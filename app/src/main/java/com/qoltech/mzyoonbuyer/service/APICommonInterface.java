package com.qoltech.mzyoonbuyer.service;

import com.qoltech.mzyoonbuyer.entity.AddToCartEntity;
import com.qoltech.mzyoonbuyer.entity.DashboardProductsEntity;
import com.qoltech.mzyoonbuyer.entity.GetMaterialSelectionModal;
import com.qoltech.mzyoonbuyer.entity.GetRecentProductEntity;
import com.qoltech.mzyoonbuyer.entity.GetTrackingEntity;
import com.qoltech.mzyoonbuyer.entity.SearchProductEntity;
import com.qoltech.mzyoonbuyer.entity.StoreAllBrandsEntity;
import com.qoltech.mzyoonbuyer.entity.StoreCatogeryEntity;
import com.qoltech.mzyoonbuyer.entity.StoreSubCatogeryEntity;
import com.qoltech.mzyoonbuyer.entity.TrackingFieldEntity;
import com.qoltech.mzyoonbuyer.entity.WishListEntity;
import com.qoltech.mzyoonbuyer.modal.*;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface APICommonInterface {


    /*Get all country api call*/
    @GET
    Call<CountryCodeResponse> getAllCountryAPI(@Url String queryUrl);

    /*Get gender api call*/
    @GET
    Call<GenderResponse> getGenderAPI(@Url String queryUrl);

    /*Get dress type api call*/
    @GET
    Call<DressTypeResponse> getDressTypeAPI(@Url String queryUrl);

    /*Get dress type api call*/
    @GET
    Call<OrderTypeResponse> getOrderTypeAPI(@Url String queryUrl);

    /*Get Measurement api call*/
    @GET
    Call<MeasurementOneResponse> getMeasurementOneAPI(@Url String queryUrl);

    /*Get CustomizationThree api call*/
    @GET
    Call<GetCustomizationThreeResponse> getCustomizationThreeAPI(@Url String queryUrl);

    /*Get CustomizationThree api call*/
    @GET
    Call<GetCustomizationThreeResponse> getNewFlowCustomizationThreeAPI(@Url String queryUrl);

    /*Get SubType api call*/
    @GET
    Call<SubTypeResponse> getSubTypeAPI(@Url String queryUrl);

    /*Get Buyer Address api call*/
    @GET
    Call<GetAddressResponse> getAddressAPI(@Url String queryUrl);

    /*Get Buyer Address api call*/
    @GET
    Call<GetAddressResponse> getBuyerAddressAPI(@Url String queryUrl);

    /*Get Measurement two api call*/
    @GET
    Call<GetMeasurementTwoResponse> getMeasurementTwoAPI(@Url String queryUrl);

    /*Get Excisted User api call*/
    @GET
    Call<IsExistedUserResponse> isExistedUserAPI(@Url String queryUrl);

    /*Get Delete api call*/
    @GET
    Call<IsExistedUserResponse> deleteUserAddressAPI(@Url String queryUrl);

    /*Manual api call*/
    @GET
    Call<ManuallyResponse> manuallyAPI(@Url String queryUrl);

    /*Delete Address*/
    @GET
    Call<DeleteAddressResponse> deldeteAddressAPI(@Url String queryUrl);

    /*Get QuotationList*/
    @GET
    Call<QuotationListResponse> quotationListAPI(@Url String queryUrl);

    /*Get OrderApprovalResponsePrice*/
    @GET
    Call<OrderApprovalPriceResponse> orderApprovalPriceAPI(@Url String queryUrl);

    /*Get OrderApprovalResponseDelivery*/
    @GET
    Call<OrderApprovalDeliveryResponse> orderApprovalDeliveryAPI(@Url String queryUrl);

    /*Get ProfileDetails*/
    @GET
    Call<GetProfileDetailsResponse> getProfileDetailsAPI(@Url String queryUrl);

    /*Get Measurement Part*/
    @GET
    Call<GetMeasurementPartsResponse> getMeasurementPartsApi(@Url String queryUrl);

    /*Get OrderRequestList*/
    @GET
    Call<OrderRequestListResponse> getOrderRequestListApi(@Url String queryUrl);

    /*Get AppointmentList*/
    @GET
    Call<AppointmentListResponse> getAppointmentListApi(@Url String queryUrl);

    /*Get OrderPendingList*/
    @GET
    Call<OrderListPendingResponse> getOrderPendingListApi(@Url String queryUrl);

    /*Get AppointmentMaterial*/
    @GET
    Call<AppointmentMaterialResponse> getAppointmentMaterialApi(@Url String queryUrl);

    /*Get AppointmentMeasurement*/
    @GET
    Call<AppointmentMeasurementResponse> getAppointmentMeasurementApi(@Url String queryUrl);

    /*Get Service Type*/
    @GET
    Call<ServiceTypeResponse> getServiceTypeApi(@Url String queryUrl);

    /*Get Order Details*/
    @GET
    Call<OrderDetailsResponse> getOrderDetailsApi(@Url String queryUrl);

    /*Get Order Details*/
    @GET
    Call<GetLanguageResponse> getLanguageApi(@Url String queryUrl);

    /*Get Rating*/
    @GET
    Call<GetRatingResponse> getRatingApi(@Url String queryUrl);

    /*Get MaterialDate*/
    @GET
    Call<GetMaterialFromDateResponse> getMaterialDateApi(@Url String queryUrl);

    /*Get MeasurementDate*/
    @GET
    Call<GetMeasurementFromDateResponse> getMeasurementDateApi(@Url String queryUrl);

    /*Get Tracking Details*/
    @GET
    Call<GetTrackingResponse> getTrackingDetailsApi(@Url String queryUrl);

    /*Get Order Delivery Details*/
    @GET
    Call<OrderDeliveryListResponse> getOrderDeliveryListApi(@Url String queryUrl);

    /*Get Shop Details*/
    @GET
    Call<ShopDetailsReponse> getShopDetailsApi(@Url String queryUrl);

    /*Get PaymentAddress Details*/
    @GET
    Call<PaymentAddressResponse> getPaymentAddressDetailsApi(@Url String queryUrl);

    /*Get View Pattern Details*/
    @GET
    Call<ViewDetailsResponse> getViewDetailsApi(@Url String queryUrl);

    /*Get View Pattern Details*/
    @GET
    Call<ViewDetailsNewFlowResponse> getNewFlowViewDetailsApi(@Url String queryUrl);

    /*Get Add Measurement*/
    @GET
    Call<AddMeasurementResponse> getAddMeasurementApi(@Url String queryUrl);

    /*Get Tailor Type*/
    @GET
    Call<TailorTypeResponse> getTailoTyepApi(@Url String queryUrl);

    /*Get Tailor Type*/
    @GET
    Call<OrderFlowTypeModal> getOrderFlowTyepApi(@Url String queryUrl);

    /*Get Tailor List*/
    @GET
    Call<TailorListResponse> getNewFlowTailorListApi(@Url String queryUrl);


    /*Get Order Type*/
    @GET
    Call<OrderTypeResponse> getNewFlowOrderTypeApi(@Url String queryUrl);

    /*Get Measurement Type*/
    @GET
    Call<MeasurementOneResponse> getNewFlowMeasurementTypeApi(@Url String queryUrl);

    /*Get Service Type*/
    @GET
    Call<ServiceTypeResponse> getNewFlowServiceTypeApi(@Url String queryUrl);

    /*Get Payment Store*/
    @GET
    Call<GetPaymentStoreModal> getPaymentStoreApi(@Url String queryUrl);

    /*Get AllCategories*/
    @GET
    Call<GetAllCategoriesResponse> getAllCategoriesApi(@Url String queryUrl);

    /*Get SortCategories*/
    @GET
    Call<GetOfferCategoriesResponse> getSortOfferCategoriesApi(@Url String queryUrl);

    /*Get OfferCategories*/
    @GET
    Call<GetOfferCategoriesResponse> getOfferCategoriesApi(@Url String queryUrl);

    /*Get Checkout*/
    @GET
    Call<CheckoutResponse> getCheckOutPriceApi(@Url String queryUrl);

    /*Get Payment Address*/
    @GET
    Call<GetBuyerAddressModal> getPaymentAddressApi(@Url String queryUrl);

    /*Get Store Order Deatils*/
    @GET
    Call<StoreOrderDetailsResponse> getStoreOrderDeatilsApi(@Url String queryUrl);

    /*Get Store Order Get Tracking*/
    @GET
    Call<GetStoreTrackingResponse> getStoreOrderTrackingDetailsApi(@Url String queryUrl);

    /*Get ScheduleType*/
    @GET
    Call<GetAppointmentForScheduleTypeResponse> getAppointmentForScheduleTypeApi(@Url String queryUrl);

    /*Get Schedul Type Date*/
    @GET
    Call<GetAppointmentDateForScheduleResponse> getAppointmentDateForScheduleTypeApi(@Url String queryUrl);

    /*Get Schedul Type Date*/
    @GET
    Call<GetNoOfAppointmentDateForScheduleResponse> getNoOfAppointmentDateForScheduleTypeApi(@Url String queryUrl);

    /*Get Order Details Customization*/
    @GET
    Call<GetOrderDetailsCustomizationResponse> getOrderDetailsCustomizationApiCall(@Url String queryUrl);

    /*Get Measurement List Parts*/
    @GET
    Call<MeasurementListGetMeasurementPartsResponse> getMeasurementListGetMeasurementPartsApiCall(@Url String queryUrl);

    /*Get Request Details*/
    @GET
    Call<GetRequestDetailsResponse> getRequestDetailsApiCall(@Url String queryUrl);

    /*Get Request Details*/
    @GET
    Call<RelatedProductsResponse> getRelatedProductsApiCall(@Url String queryUrl);

    /*Get Speical Offer*/
    @GET
    Call<GetSpecialOfferModal> getSpecialOfferApiCall(@Url String queryUrl);

    /*Get Redeem*/
    @GET
    Call<GetRedeemResponse> getRedeemApiCall(@Url String queryUrl);

    /*Get Rewards*/
    @GET
    Call<GetRewardsResponse> getRewardsApiCall(@Url String queryUrl);

    /*Get Referal Code*/
    @GET
    Call<GetReferalCodeResponse> getReferalCodeApiCall(@Url String queryUrl);

    /*Get ScrachCard */
    @GET
    Call<GetScrachCardResponse> getScrachCardApiCall(@Url String queryUrl);

    /*Get Card Items*/
    @GET
    Call<GetCardItemsResponse> getCardItemApiCall(@Url String queryUrl);

    /*Add Measurement Parts List*/
    @GET
    Call<AddMeasurementPartsListResponse> getAddMeasurementPartsList(@Url String queryUrl);

    /*Get Reward History List*/
    @GET
    Call<RewardHistoryResponse> getRewardHistory(@Url String queryUrl);

    /*Get Reward Home*/
    @GET
    Call<GetRewardsHomeResponse> getRewardHome(@Url String queryUrl);

    /*Get Notificatin list*/
    @GET
    Call<NotificationResponse> getNotificationList(@Url String queryUrl);

    /*Get Sub Color*/
    @GET
    Call<SubColorResponse> getSubColor(@Url String queryUrl);

    /*Get Material Review*/
    @GET
    Call<MaterialReviewModal> getMaterialReview(@Url String queryUrl);

    /*Get Delivery Type*/
    @GET
    Call<DeliveryTypeResponse> getDeliveryTypeApi(@Url String queryUrl);

    /*Get Dashboard Store Details*/
    @GET
    Call<GetStoreDashBoardResponse> getStoreDashBaordApi(@Url String queryUrl);

    @GET
    Call<ArrayList<StoreCatogeryEntity>> getAllStoreCatogeryApi(@Url String queryUrl);

    /*Get Store Product List*/
    @GET
    Call<GetStoreProductListModal> getStoreProductListApi(@Url String queryUrl);

    /*Get Product Details*/
    @GET
    Call<GetProductDetailsResponse> getStoreProductDetailsApi(@Url String queryUrl);

    /*Get Sub Catogery*/
    @GET
    Call<ArrayList<StoreSubCatogeryEntity>> getStoreSubCatogeryApi(@Url String queryUrl);

    /*Get View All Barnds*/
    @GET
    Call<ArrayList<StoreAllBrandsEntity>> getStoreViewAllBrandsApi(@Url String queryUrl);

    /*Store Cart List*/
    @GET
    Call<StoreCartResponse> getStoreCartListApi(@Url String queryUrl);

    /*Create Cart*/
    @GET
    Call<String> createCartApi(@Url String queryUrl);

    /*Get Search */
    @GET
    Call<ArrayList<SearchProductEntity>> getStoreSearchApi(@Url String queryUrl);

    /*Get Store Search Product List*/
    @GET
    Call<GetStoreProductListModal> getStoreSearchProductListApi(@Url String queryUrl);

    /*Get Store Tracking Field*/
    @GET
    Call<ArrayList<TrackingFieldEntity>> getStoreTrackingFieldApi(@Url String queryUrl);

    /*Get Store Tracking*/
    @GET
    Call<ArrayList<GetTrackingEntity>> getStoreTrackingApi(@Url String queryUrl);

    /*Get Order Details For Store And Stitching*/
    @GET
    Call<OrderDetailsStitchingStoreResponse> getOrderDetailsStitchingStoreApi(@Url String queryUrl);

    /*Get Store Rating Review*/
    @GET
    Call<GetStoreReviewRatingModal> getStoreRatingReviewApi(@Url String queryUrl);

    /*Get Stitching TrackingField Api*/
    @GET
    Call<TrackingFieldModal> getStitchingTrackingFieldApi(@Url String queryUrl);

    /*Get Store Review*/
    @GET
    Call<MaterialReviewModal> getStoreReview(@Url String queryUrl);


    /*Get Tailor List api call*/
    @POST("order/GetTailorlist?")
    Call<TailorListResponse> getTailorListAPI(@Body TailorListModal tailorListModal);

    /*GetCustomizationThreeApiCall*/
    @FormUrlEncoded
    @POST("Order/GetCustomization3")
    Call<CustomizationThreeGetResponse> customizationThreeAPICall(@Field("DressTypeId") String DressTypeId);

    /*GetNewFlowCustomizationThreeApiCall*/
    @FormUrlEncoded
    @POST("order/GetMaterialSelection3")
    Call<CustomizationThreeGetResponse> customizationThreeNewFlowAPICall(@Field("TailorId") String TailorId,@Field("DressSubTypeId") String DressSubTypeId);

    /*UpdateProfileApiCall*/
    @FormUrlEncoded
    @POST("Shop/InsertBuyerDetails")
    Call<UpdateProfileResponse> updateProfileAPI(@Field("Id") int Id , @Field("Email") String Email, @Field("Dob") String Dob, @Field("Gender") String Gender, @Field("ModifiedBy") String ModifiedBy, @Field("language") String language,@Field("Name") String Name,@Field("ProfilePicture") String ProfilePicture);

    /*GenerateOTP*/
    @FormUrlEncoded
    @POST("Login/GenerateOTP")
    Call<LoginOTPResponse> loginOTPGenerateAPI(@Field("CountryCode") String CountryCode , @Field("PhoneNo") String PhoneNo, @Field("DeviceId") String DeviceId,@Field("Language") String Language);

    /*ResendOTP*/
    @FormUrlEncoded
    @POST("Login/ResendOTP")
    Call<LoginOTPResponse> resendOTPGenerateAPI(@Field("CountryCode") String CountryCode , @Field("PhoneNo") String PhoneNo, @Field("DeviceId") String DeviceId,@Field("Language") String Language);

    /*ValidateOTP*/
    @FormUrlEncoded
    @POST("Login/ValidateOTP")
    Call<ValidateOTPResponse> validateOTPAPI(@Field("CountryCode") String CountryCode , @Field("PhoneNo") String PhoneNo, @Field("DeviceId") String DeviceId , @Field("OTP") String OTP, @Field("Type") String Type);

    /*ProfileIntro*/
    @FormUrlEncoded
    @POST("Shop/InsertBuyerName")
    Call<ProfileIntroResponse> profileIntroAPI(@Field("Id") int id , @Field("Name") String name,@Field("ProfilePicture") String ProfilePicture,@Field("Email") String Email,@Field("ReferenceCode") String ReferenceCode,@Field("Type") String Type);

    /*CustomizationOne*/
    @POST("Order/GetCustomization1")
    Call<GetCustomizationOneResponse> getCustomizationOneAPI(@Body CustomizationOneApiCallModal customizationOneApiCallModal);

    /*CustomizationTwo*/
    @POST("Order/GetCustomization2")
    Call<GetCustomizationTwoResponse> getCustomizationTwoAPI(@Body CustomizationTwoApiCallModal customizationTwoApiCallModal);

    /*CustomizationNew*/
    @POST("Order/GetNewCustomization")
    Call<GetNewCustomizationResponse> getNewCustomizationAPI(@Body CustomizationNewApiCallModal customizationApiCallModal);

    /*CustomizationNewFlow*/
    @POST("Order/GetNewMaterialSelection2")
    Call<GetNewCustomizationResponse> getNewCustomizationNewFlowAPI(@Body NewFlowCustomizationNewApiCallModal customizationApiCallModal);


    /*CustomizationOne*/
    @POST("Order/GetMaterialSelection1")
    Call<GetCustomizationOneResponse> getNewFlowCustomizationOneAPI(@Body NewFlowCustomizationOneApiCall customizationOneApiCallModal);

    /*CustomizationTwo*/
    @POST("Order/GetMaterialSelection2")
    Call<GetCustomizationTwoResponse> getNewFlowCustomizationTwoAPI(@Body NewFlowCustomizationTwoApiCall customizationTwoApiCallModal);

    /*InsertAddress*/
    @FormUrlEncoded
    @POST("Shop/InsertBuyerAddress")
    Call<InsertAddressResponse> insertAddressAPI(@Field("BuyerId") int BuyerId, @Field("FirstName") String FirstName , @Field("LastName") String LastName , @Field("CountryId") int CountryId,@Field("StateId") int StateId,@Field("AreaId") int Area,@Field("Floor") String Floor,@Field("LandMark") String LandMark,@Field("LocationType") String LocationType,@Field("ShippingNotes") String ShippingNotes,@Field("IsDefault") Boolean IsDefault,@Field("CountryCode") String CountryCode,@Field("PhoneNo") String PhoneNo,@Field("Longitude") Double Longitude,@Field("Lattitude") Double Latitude);

    /*DeviceDetails*/
    @FormUrlEncoded
    @POST("Login/InsertUpdateDeviceDetails")
    Call<DeviceDetailsResponse> insertUpdateDeviceDetailsAPI(@Field("DeviceId") String deviceId , @Field("Os") String os , @Field("Manufacturer") String manufacture, @Field("CountryCode") String countryCode,@Field("PhoneNumber") String phoneNumber,@Field("Model") String Model,@Field("AppVersion") String appVersion, @Field("Type") String type,@Field("Fcm") String Fcm);

    /*GetState*/
    @FormUrlEncoded
    @POST("Shop/DisplayStatebyCountry")
    Call<GetStateResponse> getStateAPI(@Field("Id") String Id );

    /*Order Summary*/
    @FormUrlEncoded
    @POST("Order/InsertOrder")
    Call<OrderSummaryResponse> orderSummaryAPI(@Field("dressType") String dressType, @Field("CustomerId") String CustomerId, @Field("AddressId") String AddressId, @Field("PatternId") String PatternId, @Field("Ordertype") String Ordertype, @Field("MeasurementId") String MeasurementId, @Field("MaterialImage[0][Image]") String MaterialImage, @Field("ReferenceImage") String ReferenceImage, @Field("OrderCustomization[0][CustomizationAttributeId]") String OrderCustomization, @Field("OrderCustomization[0][AttributeImageId]") String OrderCusomizationImage, @Field("UserMeasurementValue[0][UserMeasurementId]") String UserMeasurementId, @Field("UserMeasurementValue[0][Value]") String UserMeasurementValue, @Field(" MeasurementBy") String  MeasurementBy, @Field("CreatedBy") String CreatedBy, @Field("MeasurementName") String MeasurementName, @Field("TailorId[0][Id]") String TailorId, @Field("DeliveryTypeId") String DeliveryTypeId, @Field("MeasurmentType") String MeasurmentType, @Field("Units") String Units);

    /*Update Buyer Address*/
    @FormUrlEncoded
    @POST("Shop/UpdateBuyerAddress")
    Call<UpdateAddressResponse> updateAddressAPI(@Field("Id") String Id , @Field("BuyerId") String BuyerId, @Field("FirstName") String FirstName, @Field("LastName") String LastName, @Field("CountryId") int CountryId, @Field("StateId") int StateId, @Field("AreaId") int Area, @Field("Floor") String Floor, @Field("LandMark") String LandMark, @Field("LocationType") String LocationType, @Field("ShippingNotes") String ShippingNotes, @Field("IsDefault") Boolean IsDefault, @Field("CountryCode") String CountryCode, @Field("PhoneNo") String PhoneNo, @Field("Longitude") Double Longitude, @Field("Lattitude") Double Latitude);

    /*Insert Order Summary*/
    @POST("Order/InsertOrder")
    Call<OrderSummaryResponse> orderSummary(@Body OrderSummaryApiCallModal orderSummaryApiCallModal);

    /*Insert Measurement Value*/
    @POST("Order/InsertUserMeasurementValues")
    Call<InsertMeasurementValueResponse> insertMeasurementValue(@Body InsertMeasurementValueModal measurementValueModal);

    /*Insert Rating Value*/
    @POST("Order/InsertRating")
    Call<InsertRatingResponse> insertRatingApiCall(@Body InsertRatingApicallModal insertRatingApicallModal);

    /*Insert Rating Value*/
    @FormUrlEncoded
    @POST("Order/InsertMaterialRating")
    Call<InsertMaterialRatingResponse> insertMaterialRatingApiCall(@Field("OrderId") String OrderId, @Field("MaterialId") String MaterialId, @Field("rating") String rating, @Field("Review") String Review);

    @Multipart
    @POST("FileUpload/UploadFile")
    @Headers("Accept: */*")
    Call<FileUploadResponse> imageUpload2(@Part MultipartBody.Part image);

    @Multipart
    @POST("FileUpload/UploadFile")
    @Headers("Accept: */*")
    Call<FileUploadResponse> multiFileUpload(@Part MultipartBody.Part[] multipartTypedOutput);

    /*Insert Appointment Material*/
    @FormUrlEncoded
    @POST("Order/InsertAppointforMaterial")
    Call<InsertAppointmentMaterialResponse> insertAppointmentMaterialAPI(@Field("OrderId") String OrderId,@Field("AppointmentType") String AppointmentType,@Field("AppointmentTime") String AppointmentTime,@Field("From") String From,@Field("To") String To,@Field("TypeId") String TypeId,@Field("Type") String Type,@Field("CreatedBy") String CreatedBy);

    /*Insert Appointment Measurement*/
    @FormUrlEncoded
    @POST("Order/InsertAppointforMeasurement")
    Call<InsertAppointmentMeasurementResponse> insertAppointmentMeasurementAPI(@Field("OrderId") String OrderId, @Field("AppointmentType") String AppointmentType, @Field("AppointmentTime") String AppointmentTime, @Field("From") String From, @Field("To") String To,@Field("TypeId") String TypeId,@Field("Type") String Type, @Field("CreatedBy") String CreatedBy);

    /*Arrprove Appointment Material*/
    @FormUrlEncoded
    @POST("Order/BuyerOrderApprovalMaterial")
    Call<ApproveMaterialResponse> approveMaterialAppointmentAPI(@Field("AppointmentId") String AppointmentId, @Field("IsApproved") String IsApproved, @Field("Reason") String Reason);

    /*Arrprove Appointment Measurement*/
    @FormUrlEncoded
    @POST("Order/BuyerOrderApprovalMeasurement")
    Call<ApproveMeasurementResponse> approveMeasurementAppointmentAPI(@Field("AppointmentId") String AppointmentId, @Field("IsApproved") String IsApproved, @Field("Reason") String Reason);

    /*Payment  Status*/
    @FormUrlEncoded
    @POST("Order/UpatePaymentStatus")
    Call<PaymentStatusResponse> paymentStatus(@Field("PaymentStatus") String PaymentStatus, @Field("OrderId") String OrderId,@Field("Type") String Type);

    /*After  Payment*/
    @FormUrlEncoded
    @POST("Order/BuyerOrderApproval")
    Call<AfterPayementResponse> afterPaymentStatus(@Field("ApprovedTailorId") String ApprovedTailorId, @Field("OrderId") String OrderId);

    /*Insert  Payment Status*/
    @FormUrlEncoded
    @POST("Order/InsertPaymentStatus")
    Call<InsertPaymentStatusResponse> insertPaymentStatus(@Field("OrderId") String OrderId, @Field("Transactionid") String Transactionid,@Field("Amount") String Amount,@Field("Status") String Status,@Field("Code") String Code,@Field("message") String message,@Field("cvv") String cvv,@Field("avs") String avs,@Field("cardcode") String cardcode,@Field("cardlast4") String cardlast4,@Field("trace") String Trace,@Field("ca_Valid") String ca_Valid,@Field("UsedPoint") String UsedPoint,@Field("TotalAmount") String TotalAmount,@Field("ReduceAmount") String ReduceAmount,@Field("BalanceAmount") String BalanceAmount,@Field("CoupanReduceAmount") String CoupanReduceAmount,@Field("CoupanId") Long CoupanId,@Field("StichingOrderAmount") String StichingOrderAmount,@Field("StoreOrderAmount") String StoreOrderAmount);

    /*Get Area*/
    @FormUrlEncoded
    @POST("Shop/GetAreaByState")
    Call<GetAreaResponse> getAreaApi(@Field("StateId") String Id);

    /*Update Quantity*/
    @FormUrlEncoded
    @POST("Order/UPdateQtyInOrderApproval")
    Call<UpdateQuantityResponse> updateQtyOrderApproval(@Field("OrderId") String OrderId, @Field("Qty") String Qty);

    /*Tailor Approved*/
    @FormUrlEncoded
    @POST("shop/TailorOrderApproved")
    Call<TailorApprovedResponse> tailorApprovedApi(@Field("OrderId") String OrderId, @Field("TailorId") String TailorId , @Field("IsApproved") String IsApproved,@Field("Type") String Type);

    /*Insert Language*/
    @FormUrlEncoded
    @POST("Login/InsertLanguage")
    Call<InsertLanguageModal> insertLanguageApi(@Field("Id") String Id, @Field("language") String language,@Field("Type") String Type);

    /*GetPrice Details */
    @FormUrlEncoded
    @POST("Order/GetPriceDetails?")
    Call<GetPriceDetailsResponse> getPriceDetailsApi(@Field("TailorId") String TailorId, @Field("DressSubTypeId") String DressSubTypeId,@Field("MaterialId") String MaterialId,@Field("OrderType") String OrderType,@Field("MeasuremenType") String MeasuremenType,@Field("ServiceType") String ServiceType,@Field("DeliveryId") String DeliveryId);

    /*Insert  Payment Status*/
    @FormUrlEncoded
    @POST("Order/InsertStorePaymentStatus")
    Call<InsertPaymentStatusResponse> insertStorePaymentApi(@Field("OrderId") String OrderId, @Field("Transactionid") String Transactionid,@Field("Amount") String Amount,@Field("Status") String Status,@Field("Code") String Code,@Field("message") String message,@Field("cvv") String cvv,@Field("avs") String avs,@Field("cardcode") String cardcode,@Field("cardlast4") String cardlast4,@Field("Trace") String Trace,@Field("ca_Valid") String ca_Valid,@Field("UsedPoint") String UsedPoint,@Field("TotalAmount") String TotalAmount,@Field("ReduceAmount") String ReduceAmount,@Field("BalanceAmount") String BalanceAmount,@Field("CoupanReduceAmount") String CoupanReduceAmount,@Field("CoupanId") String CoupanId,@Field("StoreOrderAmount") String StoreOrderAmount);

    /*Store Insert Rating*/
    @FormUrlEncoded
    @POST("Weborders/InsertStoreRating")
    Call<InsertRatingAndReviewResponse> insertStoreRatingReviewApi(@Field("OrderId") String OrderId, @Field("OrderlineId") String OrderlineId, @Field("rating") String rating, @Field("review") String review);

    /*Insert Schedule Type Date*/
    @FormUrlEncoded
    @POST("Order/InsertAppointment")
    Call<InsertAppointmentForScheduleTypeResponse> insertAppointmentForScheduleTypeApi(@Field("OrderId") String OrderId, @Field("AppointmentType") String AppointmentType, @Field("AppointmentTime") String AppointmentTime, @Field("From") String From, @Field("To") String To, @Field("Type") String Type, @Field("TypeId") String TypeId, @Field("CreatedBy") String CreatedBy);

    /*Insert Error*/
    @FormUrlEncoded
    @POST("Order/InsertAppointment")
    Call<InsertErrorApiCallRespone> insertErrorApi(@Field("PageName") String PageName, @Field("MethodName") String MethodName, @Field("Error") String Error, @Field("ApiVersion") String ApiVersion, @Field("DeviceId") String DeviceId, @Field("Type") String Type);

    /*GetProductsByCoupanType*/
    @POST("Order/GetProductsByCoupanType")
    Call<GetOfferCategoriesResponse> getOfferProductByCoupanApi(@Body GetOffersModal getOffersModal);

    /*Get Coupon Code Price*/
    @FormUrlEncoded
    @POST("Order/GetDiscountsByPromocode")
    Call<GetCouponPriceResponse> getCouponPriceApiCall(@Field("couponcodeId") String couponcodeId, @Field("TailorId")String TailorId);

    /*Apply Coupon Code*/
    @FormUrlEncoded
    @POST("Order/ApplyPromocode")
    Call<ApplyPromoCodeResponse> couponPromoCodeApplyApiCall(@Field("CoupanId") int CoupanId, @Field("TailorId")String TailorId, @Field("DressTypeId") String DressTypeId, @Field("BalanceAmount") Double BalanceAmount);

    /*Apply ConvertedCashPoints*/
    @FormUrlEncoded
    @POST("Order/ConvertCashByPoint")
    Call<ConvertCashPointResponse> convertPointsApplyApiCall(@Field("UserId") String UserId, @Field("UsedPoint")int UsedPoint, @Field("OrderId") String OrderId,@Field("BalanceAmount") Double BalanceAmount);

    /*Get Points By Order*/
    @FormUrlEncoded
    @POST("Order/GetPointByOrder")
    Call<GetPointsByOrderResponse> getPointsByOrder(@Field("UserId") String UserId, @Field("OrderId")String OrderId);

    /*Get Points By Order*/
    @FormUrlEncoded
    @POST("order/UpdateRewardPoint")
    Call<UpdateRewardsPointsResponse> updateScratchCardRewardsPoints(@Field("Id") String Id);

    /*Remove Product From Card*/
    @FormUrlEncoded
    @POST("Cart/RemoveProductFromCart")
    Call<ArrayList<RemoveCardItemResponse>> removeProductFromCart(@Field("CartId") String CartId, @Field("ProductId") String ProductId, @Field("SizeId") String SizeId, @Field("ColorId") String ColorId, @Field("SellerId") String SellerId);

    /*Insert Web Order*/
    @POST("WebOrders/CreateOrder")
    Call<WebOrderResponse> insertWebOrder(@Body InsertWebOrderBody insertWebOrderBody);

    /*Update Tailor Amount By Buyer*/
    @FormUrlEncoded
    @POST("shop/UpdateTailorAmountByBuyer")
    Call<UpdateTailorAmountResponse> updateTailorAmount(@Field("OrderId") String OrderId,@Field("Amount") String Amount,@Field("Transactionno") String Transactionno);

    /*Get Material Selection*/
    @POST("order/GetMaterialSelection")
    Call<GetMaterialSelectionResponse> getMaterialSelectionResponse(@Body GetMaterialSelectionModal getMaterialSelectionModal);

    @FormUrlEncoded
    @POST("Cart/AddProductToCart")
    Call<ArrayList<AddToCartEntity>> addToCartApi(@Field("CartId") String CartId,@Field("ProductId") String ProductId,@Field("SizeId") String SizeId,@Field("ColorId") String ColorId,@Field("SellerId") String SellerId,@Field("Quantity") String Quantity);

    @FormUrlEncoded
    @POST("Cart/AddToWishList")
    Call<ArrayList<DashboardProductsEntity>> storeAddToWishListApi(@Field("CartId") String CartId,@Field("ProductId") String ProductId,@Field("SizeId") String SizeId,@Field("ColorId") String ColorId,@Field("SellerId") String SellerId);

    @FormUrlEncoded
    @POST("cart/WishListRemove")
    Call<ArrayList<DashboardProductsEntity>> storeRemoveToWishListApi(@Field("CartId") String CartId,@Field("ProductId") String ProductId,@Field("SizeId") String SizeId,@Field("ColorId") String ColorId,@Field("SellerId") String SellerId);

    @FormUrlEncoded
    @POST("Cart/GetWishList?")
    Call<ArrayList<WishListEntity>> getWishListApiCall(@Field("CartId") String CartId);

    @POST("Products/GetProductsForRecent")
    Call<ArrayList<DashboardProductsEntity>> getRecentProductList(@Body GetRecentProductEntity getRecentProductEntity);

    @FormUrlEncoded
    @POST("cart/RemoveProductFromCart?")
    Call<ArrayList<DashboardProductsEntity>> removeFromCartListApi(@Field("CartId") String CartId,@Field("ProductId") String ProductId,@Field("SizeId") String SizeId,@Field("ColorId") String ColorId,@Field("SellerId") String SellerId);

    @FormUrlEncoded
    @POST("cart/AddProductToCartQty")
    Call<ArrayList<DashboardProductsEntity>> changeQtyFromCartListApi(@Field("CartId") String CartId,@Field("ProductId") String ProductId,@Field("SizeId") String SizeId,@Field("ColorId") String ColorId,@Field("SellerId") String SellerId,@Field("Quantity") String Quantity);

    @POST("shop/GetDeliveryChargesByTailor")
    Call<CheckoutDeliveryChargesResponse> getCheckoutDeliveryChargesApi(@Body CheckoutDeliveryChargesModal checkoutDeliveryChargesModal);
}


