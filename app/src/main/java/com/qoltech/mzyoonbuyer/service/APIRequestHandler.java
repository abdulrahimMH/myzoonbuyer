package com.qoltech.mzyoonbuyer.service;

import android.os.Build;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qoltech.mzyoonbuyer.entity.GetMaterialSelectionModal;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.main.BaseFragment;
import com.qoltech.mzyoonbuyer.modal.*;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIRequestHandler {

    private static APIRequestHandler sInstance = new APIRequestHandler();
    private static Retrofit mRetrofitDialog;


    /*Init retrofit for API call*/
    private Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    /*Init retrofit for API call*/
    private OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(1000, TimeUnit.SECONDS)
            .writeTimeout(1000, TimeUnit.SECONDS)
            .build();

    private Retrofit retrofit = new Retrofit.Builder().baseUrl(AppConstants.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

    private APICommonInterface mServiceInterface = retrofit.create(APICommonInterface.class);

    public static APIRequestHandler getInstance() {
        return sInstance;
    }

    public static Retrofit getClient() {
        if (mRetrofitDialog == null) {
            mRetrofitDialog = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return mRetrofitDialog;
    }

    /*GetCountryAPICall*/
    public void getAllCountryAPICall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAllCountryAPI("Login/GetAllCountries").enqueue(new Callback<CountryCodeResponse>() {
            @Override
            public void onResponse(@NonNull Call<CountryCodeResponse> call, @NonNull Response<CountryCodeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getAllCountryAPICall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<CountryCodeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAllCountryAPICall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);
            }
        });

    }


    /*GetLoginOTPAPICall*/
    public void loginOTPGenerateAPICall(String CountryCode,String PhoneNum,String DeviceId,String Language,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.loginOTPGenerateAPI(CountryCode,PhoneNum,DeviceId,Language).enqueue(new Callback<LoginOTPResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginOTPResponse> call, @NonNull Response<LoginOTPResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","loginOTPGenerateAPICall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginOTPResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","loginOTPGenerateAPICall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }
    /*GetResendOTPAPICall*/
    public void resendOTPGenerateAPICall(String CountryCode,String PhoneNum,String DeviceId,String Language,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.resendOTPGenerateAPI(CountryCode,PhoneNum,DeviceId,Language).enqueue(new Callback<LoginOTPResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginOTPResponse> call, @NonNull Response<LoginOTPResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","resendOTPGenerateAPICall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginOTPResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","resendOTPGenerateAPICall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetValidateOTPAPICall*/
    public void validateOTPAPICall(String CountryCode,String PhoneNum,String DeviceId,String otp,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.validateOTPAPI(CountryCode,PhoneNum,DeviceId,otp,"customer").enqueue(new Callback<ValidateOTPResponse>() {
            @Override
            public void onResponse(@NonNull Call<ValidateOTPResponse> call, @NonNull Response<ValidateOTPResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","validateOTPAPICall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<ValidateOTPResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","validateOTPAPICall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*ProfileIntroAPICall*/
    public void profileIntroAPICall(int id,String userName,String ProfilePicture,String Email,String ReferenceCode,String Type,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.profileIntroAPI(id,userName,ProfilePicture,Email,ReferenceCode,Type).enqueue(new Callback<ProfileIntroResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProfileIntroResponse> call, @NonNull Response<ProfileIntroResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","profileIntroAPICall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ProfileIntroResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","profileIntroAPICall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

        /*GetGenderAPICall*/
    public void getGenderAPICall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getGenderAPI("Order/GetGenders").enqueue(new Callback<GenderResponse>() {
            @Override
            public void onResponse(@NonNull Call<GenderResponse> call, @NonNull Response<GenderResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getGenderAPICall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);


                }
            }

            @Override
            public void onFailure(@NonNull Call<GenderResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getGenderAPICall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);


            }
        });

    }

    /*GetDressTypeAPICall*/
    public void getDressTypeAPICall(final BaseActivity baseActivity , String genderId) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getDressTypeAPI("Order/GetDressTypeByGender?genderId="+genderId).enqueue(new Callback<DressTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<DressTypeResponse> call, @NonNull Response<DressTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getDressTypeAPICall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<DressTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getDressTypeAPICall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetOrderTypeAPICall*/
    public void getOrderTypeAPICall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getOrderTypeAPI("Order/DisplayOrderType").enqueue(new Callback<OrderTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderTypeResponse> call, @NonNull Response<OrderTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getOrderTypeAPICall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getOrderTypeAPICall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetMeasurementOneAPICall*/
    public void getMeasurementOneAPICall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getMeasurementOneAPI("Order/DisplayMeasurement1").enqueue(new Callback<MeasurementOneResponse>() {
            @Override
            public void onResponse(@NonNull Call<MeasurementOneResponse> call, @NonNull Response<MeasurementOneResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getMeasurementOneAPICall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<MeasurementOneResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getMeasurementOneAPICall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetCustomizationThreeAPICall*/
    public void getCustomizationThreeAPICall(String id ,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getCustomizationThreeAPI("Order/GetAttributesByAttributeId?AttributeId="+id).enqueue(new Callback<GetCustomizationThreeResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCustomizationThreeResponse> call, @NonNull Response<GetCustomizationThreeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getCustomizationThreeAPICall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCustomizationThreeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getCustomizationThreeAPICall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*Customization One Api*/
    public void getCustomizationOneApi(CustomizationOneApiCallModal customizationApiCallModal, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getCustomizationOneAPI(customizationApiCallModal).enqueue(new Callback<GetCustomizationOneResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCustomizationOneResponse> call, @NonNull Response<GetCustomizationOneResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getCustomizationOneApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCustomizationOneResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getCustomizationOneApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }


    /*Customization Two Api*/
    public void getCustomizationTwoApi(CustomizationTwoApiCallModal customizationTwoApiCallModal, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getCustomizationTwoAPI(customizationTwoApiCallModal).enqueue(new Callback<GetCustomizationTwoResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCustomizationTwoResponse> call, @NonNull Response<GetCustomizationTwoResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getCustomizationTwoApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCustomizationTwoResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getCustomizationTwoApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Customization New Api*/
    public void getNewCustomizationApi(CustomizationNewApiCallModal customizationApiCallModal, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNewCustomizationAPI(customizationApiCallModal).enqueue(new Callback<GetNewCustomizationResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetNewCustomizationResponse> call, @NonNull Response<GetNewCustomizationResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getNewCustomizationApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetNewCustomizationResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getNewCustomizationApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Customization New Api*/
    public void getNewFlowCustomizationNewApi(NewFlowCustomizationNewApiCallModal customizationApiCallModal, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNewCustomizationNewFlowAPI(customizationApiCallModal).enqueue(new Callback<GetNewCustomizationResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetNewCustomizationResponse> call, @NonNull Response<GetNewCustomizationResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getNewFlowCustomizationNewApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetNewCustomizationResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getNewFlowCustomizationNewApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*GetCountryAPICall*/
    public void getSubTypeCall(String dressSubTypeId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getSubTypeAPI("order/DisplayDressSubType?Id="+dressSubTypeId).enqueue(new Callback<SubTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<SubTypeResponse> call, @NonNull Response<SubTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getSubTypeCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<SubTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getSubTypeCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetMeasurementTwoAPICall*/
    public void getMeasurementTwoApiCall(String measurementId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getMeasurementTwoAPI("Order/GetMeasurement2?Id="+measurementId).enqueue(new Callback<GetMeasurementTwoResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetMeasurementTwoResponse> call, @NonNull Response<GetMeasurementTwoResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getMeasurementTwoApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetMeasurementTwoResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getMeasurementTwoApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }


    /*UpdateProfileAPICall*/
    public void updateProfileApiCall(int Id, String Email,String Dob,String Gender,String ModifiedBy,String language,String Name,String ProfilePicture,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.updateProfileAPI(Id,Email,Dob,Gender,ModifiedBy,language,Name,ProfilePicture).enqueue(new Callback<UpdateProfileResponse>() {
            @Override
            public void onResponse(@NonNull Call<UpdateProfileResponse> call, @NonNull Response<UpdateProfileResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","updateProfileApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<UpdateProfileResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","updateProfileApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*CustomizationThreeAPICall*/
    public void customizationThreeApiCall(String DressTypeId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.customizationThreeAPICall(DressTypeId).enqueue(new Callback<CustomizationThreeGetResponse>() {
            @Override
            public void onResponse(@NonNull Call<CustomizationThreeGetResponse> call, @NonNull Response<CustomizationThreeGetResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","customizationThreeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<CustomizationThreeGetResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","customizationThreeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetAddressByIdAPICall*/
    public void getAddressApiCall(String Id,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAddressAPI("Shop/GetBuyerAddressById?Id="+Id).enqueue(new Callback<GetAddressResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetAddressResponse> call, @NonNull Response<GetAddressResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getAddressApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetAddressResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAddressApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetBuyerAddressAPICall*/
    public void getBuyerAddressApiCall(String Id,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAddressAPI("Shop/GetBuyerAddressByBuyerId?BuyerId="+Id).enqueue(new Callback<GetAddressResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetAddressResponse> call, @NonNull Response<GetAddressResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getBuyerAddressApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetAddressResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getBuyerAddressApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*InsertAddressAPICall*/
    public void insertAddressApiCall(int BuyerId,String FirstName ,String LastName ,int CountryId,int StateId,int Area,String Floor,String LandMark,String LocationType, String ShippingNotes,Boolean IsDefault,String CountryCode,String PhoneNo,Double Longitude,Double Latitude, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertAddressAPI(BuyerId,FirstName,LastName,CountryId,StateId,Area,Floor,LandMark,LocationType,ShippingNotes,IsDefault,CountryCode,PhoneNo,Longitude,Latitude).enqueue(new Callback<InsertAddressResponse>() {
            @Override
            public void onResponse(@NonNull Call<InsertAddressResponse> call, @NonNull Response<InsertAddressResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","insertAddressApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertAddressResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertAddressApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetTailorListAPICall*/
    public void getTailorListApiCall(final BaseActivity baseActivity, TailorListModal tailorListModal) {
//        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getTailorListAPI(tailorListModal).enqueue(new Callback<TailorListResponse>() {
            @Override
            public void onResponse(@NonNull Call<TailorListResponse> call, @NonNull Response<TailorListResponse> response) {
//                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getTailorListApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<TailorListResponse> call, @NonNull Throwable t) {
//                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getTailorListApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*InsertDeviceDetailsAPICall*/
    public void insertDeviceDetailsApiCall(String deviceId, String os,String manufacture,String countryCode,String phoneNumber,String Model,String appVersion,String type,String Fcm, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertUpdateDeviceDetailsAPI(deviceId,os,manufacture,countryCode,phoneNumber,Model,appVersion,type,Fcm).enqueue(new Callback<DeviceDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<DeviceDetailsResponse> call, @NonNull Response<DeviceDetailsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","insertDeviceDetailsApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<DeviceDetailsResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertDeviceDetailsApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*IsExistedUserAPICall*/
    public void isExistedUserApiCall(String userId, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.isExistedUserAPI("Login/IsProfileCreated?UserType=customer&id="+userId).enqueue(new Callback<IsExistedUserResponse>() {
            @Override
            public void onResponse(@NonNull Call<IsExistedUserResponse> call, @NonNull Response<IsExistedUserResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","isExistedUserApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<IsExistedUserResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","isExistedUserApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }
    /*GetStateApiCall*/
    public void getStateApiCall(String Id, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getStateAPI(Id).enqueue(new Callback<GetStateResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetStateResponse> call, @NonNull Response<GetStateResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getStateApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetStateResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getStateApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetStateApiCall*/
    public void updateAddressApiCall(String Id , String BuyerId, String FirstName, String LastName, int CountryId, int StateId, int Area, String Floor, String LandMark,String LocationType,String ShippingNotes,Boolean IsDefault,String CountryCode,String PhoneNo,Double Longitude,Double Latitude, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.updateAddressAPI(Id,BuyerId,FirstName,LastName,CountryId,StateId,Area,Floor,LandMark,LocationType,ShippingNotes,IsDefault,CountryCode,PhoneNo,Longitude,Latitude).enqueue(new Callback<UpdateAddressResponse>() {
            @Override
            public void onResponse(@NonNull Call<UpdateAddressResponse> call, @NonNull Response<UpdateAddressResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","updateAddressApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<UpdateAddressResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","updateAddressApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*ManuallyApiCall*/
    public void manuallyApiCall(String dressId,String userId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.manuallyAPI("Order/GetExistingUserMeasurement?DressTypeId="+dressId+"&&UserId="+userId).enqueue(new Callback<ManuallyResponse>() {
            @Override
            public void onResponse(@NonNull Call<ManuallyResponse> call, @NonNull Response<ManuallyResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                     baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","manuallyApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<ManuallyResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","manuallyApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*DeleteAddressApiCall*/
    public void deleteAddressApiCall(String BuyerId,String Id,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.deldeteAddressAPI("Shop/DeleteBuyerAddress?BuyerId="+BuyerId+"&Id="+Id ).enqueue(new Callback<DeleteAddressResponse>() {
            @Override
            public void onResponse(@NonNull Call<DeleteAddressResponse> call, @NonNull Response<DeleteAddressResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","deleteAddressApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<DeleteAddressResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","deleteAddressApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*QuotationListApiCall*/
    public void quotationListApiCall(String OrderId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.quotationListAPI("Order/GetQuotationList?OrderId="+OrderId).enqueue(new Callback<QuotationListResponse>() {
            @Override
            public void onResponse(@NonNull Call<QuotationListResponse> call, @NonNull Response<QuotationListResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","quotationListApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<QuotationListResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","quotationListApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*OrderApprovalPriceApiCall*/
    public void orderApprovalPriceApiCall(String ResponseId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.orderApprovalPriceAPI("Order/GetTailorResponseList?TailorResponseId="+ResponseId).enqueue(new Callback<OrderApprovalPriceResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderApprovalPriceResponse> call, @NonNull Response<OrderApprovalPriceResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","orderApprovalPriceApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderApprovalPriceResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","orderApprovalPriceApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*OrderApprovalPriceApiCall*/
    public void orderApprovalDeliveryApiCall(String ResponseId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.orderApprovalDeliveryAPI("Order/GetTailorResponseList2?TailorResponseId="+ResponseId).enqueue(new Callback<OrderApprovalDeliveryResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderApprovalDeliveryResponse> call, @NonNull Response<OrderApprovalDeliveryResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","orderApprovalDeliveryApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<OrderApprovalDeliveryResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","orderApprovalDeliveryApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetProfileDetailsApiCall*/
    public void getProfileDetailsApiCall(String userId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getProfileDetailsAPI("Order/GetExistingUserProfile?Id="+userId).enqueue(new Callback<GetProfileDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetProfileDetailsResponse> call, @NonNull Response<GetProfileDetailsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getProfileDetailsApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetProfileDetailsResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getProfileDetailsApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetFragmentProfileDetailsApiCall*/
    public void getFragmentProfileDetailsApiCall(String userId,final BaseFragment baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity.getActivity());
        mServiceInterface.getProfileDetailsAPI("Order/GetExistingUserProfile?Id="+userId).enqueue(new Callback<GetProfileDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetProfileDetailsResponse> call, @NonNull Response<GetProfileDetailsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getFragmentProfileDetailsApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetProfileDetailsResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getFragmentProfileDetailsApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetMeasurementPartsApiCall*/
    public void getMeasurementPartApi(String userId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getMeasurementPartsApi("Order/GetMeasurementParts?Id="+userId).enqueue(new Callback<GetMeasurementPartsResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetMeasurementPartsResponse> call, @NonNull Response<GetMeasurementPartsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getMeasurementPartApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetMeasurementPartsResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getMeasurementPartApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetOrderRequestListApiCall*/
    public void getOrderRequestListApi(String userId,final BaseFragment baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity.getActivity());
        mServiceInterface.getOrderRequestListApi("Order/GetOrderRequest?Id="+userId).enqueue(new Callback<OrderRequestListResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderRequestListResponse> call, @NonNull Response<OrderRequestListResponse> response) {
//                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    DialogManager.getInstance().hideProgress();
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getOrderRequestListApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderRequestListResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getOrderRequestListApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*OrderSummaryApiCall*/
    public void orderSummary(OrderSummaryApiCallModal orderSummaryApiCallModal,final BaseActivity baseActivity) {

        mServiceInterface.orderSummary(orderSummaryApiCallModal).enqueue(new Callback<OrderSummaryResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderSummaryResponse> call, @NonNull Response<OrderSummaryResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","orderSummary",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderSummaryResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","orderSummary",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    public void insertMeasurementValue(InsertMeasurementValueModal measurementValueModal, final BaseActivity baseActivity) {

        mServiceInterface.insertMeasurementValue(measurementValueModal).enqueue(new Callback<InsertMeasurementValueResponse>() {
            @Override
            public void onResponse(@NonNull Call<InsertMeasurementValueResponse> call, @NonNull Response<InsertMeasurementValueResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","insertMeasurementValue",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertMeasurementValueResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertMeasurementValue",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    public void updateProfile1(MultipartBody.Part body, final BaseActivity baseActivity) {
            try {
                DialogManager.getInstance().showProgress(baseActivity);
                mServiceInterface.imageUpload2(body).enqueue(new Callback<FileUploadResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<FileUploadResponse> call, @NonNull Response<FileUploadResponse> response) {
                        DialogManager.getInstance().hideProgress();
                        if (response.isSuccessful() && response.body() != null) {
                            baseActivity.onRequestSuccess(response.body());
                        } else {
                            baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                            insertErrorApiCall("APIRequestHandler","updateProfile1",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<FileUploadResponse> call, @NonNull Throwable t) {
                        DialogManager.getInstance().hideProgress();
                        baseActivity.onRequestFailure(t);
                        insertErrorApiCall("APIRequestHandler","updateProfile1",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                    }
                });

            } catch (Exception e) {
                DialogManager.getInstance().hideProgress();
                e.printStackTrace();
            }
        }


    public void uploadMultiFile(ArrayList<String> mAddImageList,final BaseActivity baseActivity,String fileName) {
        DialogManager.getInstance().showProgress(baseActivity);
        File mUserImageFile;

        MultipartBody.Part[] multipartTypedOutput = new MultipartBody.Part[mAddImageList.size()];

        for (int index = 0; index < mAddImageList.size(); index++) {

            mUserImageFile = new File(mAddImageList.get(index));
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), mUserImageFile);
            multipartTypedOutput[index] = MultipartBody.Part.createFormData(fileName + "[" + index + "]", mUserImageFile.getName(), surveyBody);

        }

        mServiceInterface.multiFileUpload(multipartTypedOutput).enqueue(new Callback<FileUploadResponse>() {

            @Override
            public void onResponse(Call<FileUploadResponse> call, Response<FileUploadResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    DialogManager.getInstance().hideProgress();
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    DialogManager.getInstance().hideProgress();
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","uploadMultiFile",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(Call<FileUploadResponse> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","uploadMultiFile",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);


            }
        });


    }

    /*GetAppointmentListApiCall*/
    public void getAppointmentListApi(String userId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAppointmentListApi("Order/GetAppoinmentList?BuyerId="+userId).enqueue(new Callback<AppointmentListResponse>() {
            @Override
            public void onResponse(@NonNull Call<AppointmentListResponse> call, @NonNull Response<AppointmentListResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getAppointmentListApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<AppointmentListResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAppointmentListApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetOrderPendingListApiCall*/
    public void getOrderPendingListApi(String userId,final BaseFragment baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity.getActivity());
        mServiceInterface.getOrderPendingListApi("Order/ListOfOrderPending?BuyerId="+userId).enqueue(new Callback<OrderListPendingResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderListPendingResponse> call, @NonNull Response<OrderListPendingResponse> response) {
//                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    DialogManager.getInstance().hideProgress();
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getOrderPendingListApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderListPendingResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getOrderPendingListApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetAppointmentMaterialApiCall*/
    public void getAppointmentMaterialApi(String userId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAppointmentMaterialApi("Order/GetCustomerInAppoinmentMeaterial?OrderId="+userId).enqueue(new Callback<AppointmentMaterialResponse>() {
            @Override
            public void onResponse(@NonNull Call<AppointmentMaterialResponse> call, @NonNull Response<AppointmentMaterialResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getAppointmentMaterialApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<AppointmentMaterialResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAppointmentMaterialApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetAppointmentMeasurementApiCall*/
    public void getAppointmentMeasurementApi(String userId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAppointmentMeasurementApi("Order/GetCustomerInAppoinmentMeasurement?OrderId="+userId).enqueue(new Callback<AppointmentMeasurementResponse>() {
            @Override
            public void onResponse(@NonNull Call<AppointmentMeasurementResponse> call, @NonNull Response<AppointmentMeasurementResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getAppointmentMeasurementApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<AppointmentMeasurementResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAppointmentMeasurementApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*InsertAppointmentMaterialApiCall*/
    public void insertAppointmentMaterialApi(String OrderId,String AppointmentType,String AppointmentTime,String From,String To,String TypeId,String Type,String CreatedBy,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertAppointmentMaterialAPI(OrderId,AppointmentType,AppointmentTime,From,To,TypeId,Type,CreatedBy).enqueue(new Callback<InsertAppointmentMaterialResponse>() {
            @Override
            public void onResponse(@NonNull Call<InsertAppointmentMaterialResponse> call, @NonNull Response<InsertAppointmentMaterialResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","insertAppointmentMaterialApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertAppointmentMaterialResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertAppointmentMaterialApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*InsertAppointmentMeasurementApiCall*/
    public void insertAppointmentMeasurementApi(String OrderId,String AppointmentType,String AppointmentTime,String From,String To,String TypeId,String Type,String CreatedBy,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertAppointmentMeasurementAPI(OrderId,AppointmentType,AppointmentTime,From,To,TypeId,Type,CreatedBy).enqueue(new Callback<InsertAppointmentMeasurementResponse>() {
            @Override
            public void onResponse(@NonNull Call<InsertAppointmentMeasurementResponse> call, @NonNull Response<InsertAppointmentMeasurementResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","insertAppointmentMeasurementApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertAppointmentMeasurementResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertAppointmentMeasurementApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetServiceTypeApiCall*/
    public void getServiceTypeApiCall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getServiceTypeApi("Shop/GetServiceType").enqueue(new Callback<ServiceTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<ServiceTypeResponse> call, @NonNull Response<ServiceTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getServiceTypeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<ServiceTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getServiceTypeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetOrderDetalssApiCall*/
    public void getOrderDetailsApiCall(String orderId,String Type,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getOrderDetailsApi("Order/GetOrderDetails?OrderId="+orderId+"&Type="+Type).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderDetailsResponse> call, @NonNull Response<OrderDetailsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getOrderDetailsApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderDetailsResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getOrderDetailsApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }
    /*GetLanguageApiCall*/
    public void getLanguageApiCall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getLanguageApi("Login/GetAllLanguages").enqueue(new Callback<GetLanguageResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetLanguageResponse> call, @NonNull Response<GetLanguageResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getLanguageApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetLanguageResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getLanguageApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetRatingApiCall*/
    public void getRatingApiCall(String Id,String OrdId,String Type ,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getRatingApi("Order/GetRating?TailorId="+Id+"&OrdId="+OrdId+"&Type="+Type).enqueue(new Callback<GetRatingResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetRatingResponse> call, @NonNull Response<GetRatingResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getRatingApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetRatingResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getRatingApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }


    /*InsertRatingApiCall*/
    public void insertRatingApiCall(InsertRatingApicallModal insertRatingApicallModal, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertRatingApiCall(insertRatingApicallModal).enqueue(new Callback<InsertRatingResponse>() {
            @Override
            public void onResponse(@NonNull Call<InsertRatingResponse> call, @NonNull Response<InsertRatingResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","insertRatingApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertRatingResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertRatingApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*InsertMaterialRatingApiCall*/
    public void insertMaterialRatingApiCall(String OrderId,String MaterialId,String Rating,String Review, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertMaterialRatingApiCall(OrderId,MaterialId,Rating,Review).enqueue(new Callback<InsertMaterialRatingResponse>() {
            @Override
            public void onResponse(@NonNull Call<InsertMaterialRatingResponse> call, @NonNull Response<InsertMaterialRatingResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","insertMaterialRatingApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertMaterialRatingResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertMaterialRatingApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*ApproveMaterialApiCall*/
    public void approveMaterialApiCall(String AppointmentId,String IsApproved,String Reason, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.approveMaterialAppointmentAPI(AppointmentId,IsApproved,Reason).enqueue(new Callback<ApproveMaterialResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApproveMaterialResponse> call, @NonNull Response<ApproveMaterialResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","approveMaterialApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<ApproveMaterialResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","approveMaterialApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*ApproveMeasurementApiCall*/
    public void approveMeasurementApiCall(String AppointmentId,String IsApproved,String Reason, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.approveMeasurementAppointmentAPI(AppointmentId,IsApproved,Reason).enqueue(new Callback<ApproveMeasurementResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApproveMeasurementResponse> call, @NonNull Response<ApproveMeasurementResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","approveMeasurementApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<ApproveMeasurementResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","approveMeasurementApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*PaymentStatusApiCall*/
    public void payementStatusApiCall(String PaymentStatus,String OrderId, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.paymentStatus(PaymentStatus,OrderId,"Buyer").enqueue(new Callback<PaymentStatusResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaymentStatusResponse> call, @NonNull Response<PaymentStatusResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","payementStatusApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<PaymentStatusResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","payementStatusApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*AfterPaymentStatusApiCall*/
    public void afterPaymentApiCall(String ApprovedTailorId,String OrderId, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.afterPaymentStatus(ApprovedTailorId,OrderId).enqueue(new Callback<AfterPayementResponse>() {
            @Override
            public void onResponse(@NonNull Call<AfterPayementResponse> call, @NonNull Response<AfterPayementResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","afterPaymentApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<AfterPayementResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","afterPaymentApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }
    /*InsertPaymentStatusApiCall*/
    public void insertPaymentStatusApiCall(String OrderId,String TransactionId,String Amount,String Status,String Code,String message,String cvv,String avs,String cardcode,String cardlast4,String Trace,String ca_Valid,String UsedPoint,String TotalAmount,String ReduceAmount,String BalanceAmount,String CoupanReduceAmount,Long CoupanId, String StichingOrderAmount,String StoreOrderAmount ,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertPaymentStatus(OrderId,TransactionId,Amount,Status,Code,message,cvv,avs,cardcode,cardlast4,Trace,ca_Valid,UsedPoint,TotalAmount,ReduceAmount,BalanceAmount,CoupanReduceAmount,CoupanId,StichingOrderAmount,StoreOrderAmount).enqueue(new Callback<InsertPaymentStatusResponse>() {
            @Override
            public void onResponse(@NonNull Call<InsertPaymentStatusResponse> call, @NonNull Response<InsertPaymentStatusResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","insertPaymentStatusApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertPaymentStatusResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertPaymentStatusApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }
    /*GetAreaApiCall*/
    public void getAreaApiCall(String Id,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAreaApi(Id).enqueue(new Callback<GetAreaResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetAreaResponse> call, @NonNull Response<GetAreaResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getAreaApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetAreaResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAreaApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }
    /*GetMaterialDateApiCall*/
    public void getMaterialDateApiCall(String OrderId,String TypeId,String Type,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getMaterialDateApi("Order/GetAppointmentDateForMaterail?OrderId="+OrderId+"&TypeId="+TypeId+"&Type="+Type).enqueue(new Callback<GetMaterialFromDateResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetMaterialFromDateResponse> call, @NonNull Response<GetMaterialFromDateResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getMaterialDateApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetMaterialFromDateResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getMaterialDateApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }
    /*GetMeasurementDateApiCall*/
    public void getMeasurementDateApiCall(String OrderId,String TypeId,String Type,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getMeasurementDateApi("Order/GetAppointmentDateForMeasurement?OrderId="+OrderId+"&TypeId="+TypeId+"&Type="+Type).enqueue(new Callback<GetMeasurementFromDateResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetMeasurementFromDateResponse> call, @NonNull Response<GetMeasurementFromDateResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getMeasurementDateApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetMeasurementFromDateResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getMeasurementDateApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetTrackingDetailsApiCall*/
    public void getTrackingDetailsApiCall(String OrderId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getTrackingDetailsApi("Order/GetTrackingDetails?OrderId="+OrderId).enqueue(new Callback<GetTrackingResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetTrackingResponse> call, @NonNull Response<GetTrackingResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getTrackingDetailsApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetTrackingResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getTrackingDetailsApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }
    /*GetDeliveryDetailsApiCall*/
    public void getOrderDeliveryListApiCall(String userId,final BaseFragment baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity.getActivity());
        mServiceInterface.getOrderDeliveryListApi("Order/ListOfOrderDelivered?BuyerId="+userId).enqueue(new Callback<OrderDeliveryListResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderDeliveryListResponse> call, @NonNull Response<OrderDeliveryListResponse> response) {
//                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    DialogManager.getInstance().hideProgress();
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getOrderDeliveryListApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderDeliveryListResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getOrderDeliveryListApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetShopDetailsApiCall*/
    public void getShopDetailApiCall(String TailorId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getShopDetailsApi("Order/GetShopDetails?TailorId="+TailorId).enqueue(new Callback<ShopDetailsReponse>() {
            @Override
            public void onResponse(@NonNull Call<ShopDetailsReponse> call, @NonNull Response<ShopDetailsReponse> response) {
//                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getShopDetailApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<ShopDetailsReponse> call, @NonNull Throwable t) {
//                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getShopDetailApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetShopDetailsApiCall*/
    public void getShopDetailsHidePrgressBarApiCall(String TailorId,final BaseActivity baseActivity) {
//        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getShopDetailsApi("Order/GetShopDetails?TailorId="+TailorId).enqueue(new Callback<ShopDetailsReponse>() {
            @Override
            public void onResponse(@NonNull Call<ShopDetailsReponse> call, @NonNull Response<ShopDetailsReponse> response) {
//                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getShopDetailsHidePrgressBarApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<ShopDetailsReponse> call, @NonNull Throwable t) {
//                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getShopDetailsHidePrgressBarApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetShopDetailsApiCall*/
    public void getPaymentAddressDetailApiCall(String UserId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getPaymentAddressDetailsApi("order/GetPaymentAddress?BuyerId="+UserId).enqueue(new Callback<PaymentAddressResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaymentAddressResponse> call, @NonNull Response<PaymentAddressResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getPaymentAddressDetailApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<PaymentAddressResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getPaymentAddressDetailApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetShopDetailsApiCall*/
    public void updateQtyOrderApprovalApiCall(String OrderId,String QTY,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.updateQtyOrderApproval(OrderId,QTY).enqueue(new Callback<UpdateQuantityResponse>() {
            @Override
            public void onResponse(@NonNull Call<UpdateQuantityResponse> call, @NonNull Response<UpdateQuantityResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","updateQtyOrderApprovalApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<UpdateQuantityResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","updateQtyOrderApprovalApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetViewPatternDetailsApiCall*/
    public void getViewDetailsApiCall(String PatternId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getViewDetailsApi("order/ViewMaterialDetails?Id="+PatternId).enqueue(new Callback<ViewDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<ViewDetailsResponse> call, @NonNull Response<ViewDetailsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getViewDetailsApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<ViewDetailsResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getViewDetailsApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetNewFlowViewPatternDetailsApiCall*/
    public void getNewFlowViewDetailsApiCall(String TailorId,String PatternId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNewFlowViewDetailsApi("shop/GetMaterialDetails?TailorId="+TailorId+"&materialid="+PatternId).enqueue(new Callback<ViewDetailsNewFlowResponse>() {
            @Override
            public void onResponse(@NonNull Call<ViewDetailsNewFlowResponse> call, @NonNull Response<ViewDetailsNewFlowResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getNewFlowViewDetailsApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<ViewDetailsNewFlowResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getNewFlowViewDetailsApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetAddMeasurementApiCall*/
    public void getAddMeasurementApiCall(String CustomerId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAddMeasurementApi("order/MeasurementList?CustomerId="+CustomerId).enqueue(new Callback<AddMeasurementResponse>() {
            @Override
            public void onResponse(@NonNull Call<AddMeasurementResponse> call, @NonNull Response<AddMeasurementResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getAddMeasurementApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<AddMeasurementResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAddMeasurementApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetTailorTypeApiCall*/
    public void getTailorTypeApiCall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getTailoTyepApi("order/GetTailorListType?").enqueue(new Callback<TailorTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<TailorTypeResponse> call, @NonNull Response<TailorTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getTailorTypeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<TailorTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getTailorTypeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetShopDetailsApiCall*/
    public void tailorApporvedApiCall(String OrderId,String tailorId,String isApproved,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.tailorApprovedApi(OrderId,tailorId,isApproved,"Customer").enqueue(new Callback<TailorApprovedResponse>() {
            @Override
            public void onResponse(@NonNull Call<TailorApprovedResponse> call, @NonNull Response<TailorApprovedResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","tailorApporvedApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<TailorApprovedResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","tailorApporvedApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*InsertLangaugae*/
    public void insertLanguageApiCall(String Id,String Language,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertLanguageApi(Id,Language,"Customer").enqueue(new Callback<InsertLanguageModal>() {
            @Override
            public void onResponse(@NonNull Call<InsertLanguageModal> call, @NonNull Response<InsertLanguageModal> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","insertLanguageApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<InsertLanguageModal> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertLanguageApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetOrderFlowTypeApiCall*/
    public void getOrderFlowTypeApiCall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getOrderFlowTyepApi("Order/GetOrderFlowType?").enqueue(new Callback<OrderFlowTypeModal>() {
            @Override
            public void onResponse(@NonNull Call<OrderFlowTypeModal> call, @NonNull Response<OrderFlowTypeModal> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getOrderFlowTypeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<OrderFlowTypeModal> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getOrderFlowTypeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*GetNewFlowTailorListAPICall*/
    public void getNewFlowTailorListApiCall( String dressSubTyp ,String Lat,String Long ,final BaseActivity baseActivity) {
//        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNewFlowTailorListApi("order/GetTailorAll?DressSubType="+dressSubTyp+"&latitude="+Lat+"&longitude="+Long).enqueue(new Callback<TailorListResponse>() {
            @Override
            public void onResponse(@NonNull Call<TailorListResponse> call, @NonNull Response<TailorListResponse> response) {
//                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getNewFlowTailorListApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<TailorListResponse> call, @NonNull Throwable t) {
//                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getNewFlowTailorListApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }


    /*GetNewFlowTailorListAPICall*/
    public void getNewFlowOrderTypeApiCall( String tailorId ,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNewFlowOrderTypeApi("Order/GetOrderTypeByTailor?TailorId=" + tailorId).enqueue(new Callback<OrderTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderTypeResponse> call, @NonNull Response<OrderTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getNewFlowOrderTypeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getNewFlowOrderTypeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetNewFlowMeasurementAPICall*/
    public void getNewFlowMeasurementTypeApiCall( String tailorId ,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNewFlowMeasurementTypeApi("Order/GetMeasurementByTailor?TailorId=" + tailorId).enqueue(new Callback<MeasurementOneResponse>() {
            @Override
            public void onResponse(@NonNull Call<MeasurementOneResponse> call, @NonNull Response<MeasurementOneResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getNewFlowMeasurementTypeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<MeasurementOneResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getNewFlowMeasurementTypeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetNewFlowServiceAPICall*/
    public void getNewFlowServiceTypeApiCall( String tailorId ,String DressSubType,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNewFlowServiceTypeApi("Order/GetDeliveryTypeByTailorId?TailorId="+tailorId+"&DressSubTypeId="+DressSubType).enqueue(new Callback<ServiceTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<ServiceTypeResponse> call, @NonNull Response<ServiceTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getNewFlowServiceTypeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<ServiceTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getNewFlowServiceTypeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }


    /*Customization One NewFlow Api*/
    public void getNewFlowCustomizationOneApi(NewFlowCustomizationOneApiCall customizationApiCallModal, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNewFlowCustomizationOneAPI(customizationApiCallModal).enqueue(new Callback<GetCustomizationOneResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCustomizationOneResponse> call, @NonNull Response<GetCustomizationOneResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getNewFlowCustomizationOneApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCustomizationOneResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getNewFlowCustomizationOneApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }


    /*Customization Two NewFlow Api*/
    public void getNewFlowCustomizationTwoApi(NewFlowCustomizationTwoApiCall customizationTwoApiCallModal, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNewFlowCustomizationTwoAPI(customizationTwoApiCallModal).enqueue(new Callback<GetCustomizationTwoResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCustomizationTwoResponse> call, @NonNull Response<GetCustomizationTwoResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getNewFlowCustomizationTwoApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCustomizationTwoResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getNewFlowCustomizationTwoApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*CustomizationThreeAPICall*/
    public void customizationNewFlowThreeApiCall(String TailorId,String DressSubTypeId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.customizationThreeNewFlowAPICall(TailorId,DressSubTypeId).enqueue(new Callback<CustomizationThreeGetResponse>() {
            @Override
            public void onResponse(@NonNull Call<CustomizationThreeGetResponse> call, @NonNull Response<CustomizationThreeGetResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","customizationNewFlowThreeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<CustomizationThreeGetResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","customizationNewFlowThreeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetCustomizationThreeAPICall*/
    public void getCustomizationThreeNewFlowAPICall(String AttributeId,String TailorId,String DressSubType ,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getCustomizationThreeAPI("Order/GetAttributeSelectionByAttributeId?AttributeId="+AttributeId+"&TailorId="+TailorId+"&DressSubTypeId="+DressSubType).enqueue(new Callback<GetCustomizationThreeResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCustomizationThreeResponse> call, @NonNull Response<GetCustomizationThreeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getCustomizationThreeNewFlowAPICall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCustomizationThreeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getCustomizationThreeNewFlowAPICall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*Get Payment Store*/
    public void getPaymentStoreApi(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getPaymentStoreApi("Order/GetPaymentStore").enqueue(new Callback<GetPaymentStoreModal>() {
            @Override
            public void onResponse(@NonNull Call<GetPaymentStoreModal> call, @NonNull Response<GetPaymentStoreModal> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getPaymentStoreApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetPaymentStoreModal> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getPaymentStoreApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }


    /*Get Price Details Api*/
    public void getPriceDetailsApiCall(String TailorId,String DressSubTypeId,String MaterialId,String OrderTypeId,String MeasurementTypeId,String ServiceTypeId,String DeliveryId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getPriceDetailsApi(TailorId,DressSubTypeId,MaterialId,OrderTypeId,MeasurementTypeId,ServiceTypeId,DeliveryId).enqueue(new Callback<GetPriceDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetPriceDetailsResponse> call, @NonNull Response<GetPriceDetailsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getPriceDetailsApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<GetPriceDetailsResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getPriceDetailsApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*Get All Categories*/
    public void getAllCategoriesApi(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAllCategoriesApi("order/GetAllCategories").enqueue(new Callback<GetAllCategoriesResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetAllCategoriesResponse> call, @NonNull Response<GetAllCategoriesResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getAllCategoriesApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetAllCategoriesResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAllCategoriesApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Get All Categories*/
    public void getOfferCategoriesApi(String OfferId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getOfferCategoriesApi("Order/GetProductsByCoupanType?CoupanType="+OfferId).enqueue(new Callback<GetOfferCategoriesResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetOfferCategoriesResponse> call, @NonNull Response<GetOfferCategoriesResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getOfferCategoriesApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetOfferCategoriesResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getOfferCategoriesApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*InsertPaymentStatusApiCall*/
    public void insertStorePaymentApiCall(String OrderId, String TransactionId, String Amount, String Status, String Code, String message, String cvv, String avs, String cardcode, String cardlast4, String Trace, String ca_Valid,String UsedPoint,String TotalAmount,String ReduceAmount,String BalanceAmount,String CoupanReduceAmount,String CoupanId,String StoreOrderAmount , final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertStorePaymentApi(OrderId,TransactionId,Amount,Status,Code,message,cvv,avs,cardcode,cardlast4,Trace,ca_Valid,UsedPoint,TotalAmount,ReduceAmount,BalanceAmount,CoupanReduceAmount,CoupanId,StoreOrderAmount).enqueue(new Callback<InsertPaymentStatusResponse>() {
            @Override
            public void onResponse(@NonNull Call<InsertPaymentStatusResponse> call, @NonNull Response<InsertPaymentStatusResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","insertStorePaymentApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertPaymentStatusResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertStorePaymentApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*Get Sort Categories*/
    public void getSortOfferCategoriesApi(String SortCategories,String CategoriesId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getSortOfferCategoriesApi("Order/"+SortCategories+"?CoupanType="+CategoriesId).enqueue(new Callback<GetOfferCategoriesResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetOfferCategoriesResponse> call, @NonNull Response<GetOfferCategoriesResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getSortOfferCategoriesApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetOfferCategoriesResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getSortOfferCategoriesApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }
    /*Get CheckOut*/
    public void getCheckOutApi(String OrderId,String TailorId,String MaterialId,String ServiceTypeId,String MeasurementId,String OrderType,String StoreId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getCheckOutPriceApi("order/GetCheckoutDetails?OrderId="+OrderId+"&TailorId="+TailorId+"&MaterialId="+MaterialId+"&ServiceType="+ServiceTypeId+"&MeasurementId="+MeasurementId+"&OrderType="+OrderType+"&StoreId="+StoreId).enqueue(new Callback<CheckoutResponse>() {
            @Override
            public void onResponse(@NonNull Call<CheckoutResponse> call, @NonNull Response<CheckoutResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getCheckOutApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<CheckoutResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getCheckOutApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Get PaymentAddress*/
    public void getPaymentAddressApi(String OrderId, String OrderType,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);

        mServiceInterface.getPaymentAddressApi("Order/GetPaymentAddress?OrderId="+OrderId+"&OrderType="+OrderType).enqueue(new Callback<GetBuyerAddressModal>() {
            @Override
            public void onResponse(@NonNull Call<GetBuyerAddressModal> call, @NonNull Response<GetBuyerAddressModal> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getPaymentAddressApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetBuyerAddressModal> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getPaymentAddressApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Get Store Order Details*/
    public void getStoreOrderDetailsApi(String OrderLineId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getStoreOrderDeatilsApi("WebOrders/GetStoreOrderDetails?OrderLineId="+OrderLineId).enqueue(new Callback<StoreOrderDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<StoreOrderDetailsResponse> call, @NonNull Response<StoreOrderDetailsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getStoreOrderDetailsApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<StoreOrderDetailsResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getStoreOrderDetailsApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Get Store Order Tracking*/
    public void getStoreOrderTrackingDetailsApi(String OrderLineId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getStoreOrderTrackingDetailsApi("Products/GetSupplierOrderDetails?OrderLineId="+OrderLineId).enqueue(new Callback<GetStoreTrackingResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetStoreTrackingResponse> call, @NonNull Response<GetStoreTrackingResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getStoreOrderTrackingDetailsApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetStoreTrackingResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getStoreOrderTrackingDetailsApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Insert Rate And Review ApiCall*/
    public void insertStoreRateAndReviewApiCall(String OrderId,String OrderlineId,String rating,String review, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertStoreRatingReviewApi(OrderId,OrderlineId,rating,review).enqueue(new Callback<InsertRatingAndReviewResponse>() {
            @Override
            public void onResponse(@NonNull Call<InsertRatingAndReviewResponse> call, @NonNull Response<InsertRatingAndReviewResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","insertStoreRateAndReviewApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertRatingAndReviewResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertStoreRateAndReviewApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*Get Schedule Type*/
    public void getAppointmentForScheduleTypeApiCall(String OrderId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAppointmentForScheduleTypeApi("Order/GetCustomerInAppoinment?OrderId="+OrderId).enqueue(new Callback<GetAppointmentForScheduleTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetAppointmentForScheduleTypeResponse> call, @NonNull Response<GetAppointmentForScheduleTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getAppointmentForScheduleTypeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetAppointmentForScheduleTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAppointmentForScheduleTypeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Insert Schedule Type*/
    public void insertAppointmentForScheduleTypeApiCall(String OrderId,String AppointmentType,String AppointmentTime,String From,String To,String Type,String TypeId,String CreatedBy, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertAppointmentForScheduleTypeApi(OrderId,AppointmentType,AppointmentTime,From,To,Type,TypeId,CreatedBy).enqueue(new Callback<InsertAppointmentForScheduleTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<InsertAppointmentForScheduleTypeResponse> call, @NonNull Response<InsertAppointmentForScheduleTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","insertAppointmentForScheduleTypeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertAppointmentForScheduleTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertAppointmentForScheduleTypeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }


    /*Get Schedule Type Date*/
    public void getAppointmentDateForScheduleTypeApiCall(String OrderId,String TypeId,String Type,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAppointmentDateForScheduleTypeApi("Order/GetAppointmentDate?OrderId="+OrderId+"&TypeId="+TypeId+"&Type="+Type).enqueue(new Callback<GetAppointmentDateForScheduleResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetAppointmentDateForScheduleResponse> call, @NonNull Response<GetAppointmentDateForScheduleResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getAppointmentDateForScheduleTypeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetAppointmentDateForScheduleResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAppointmentDateForScheduleTypeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Get Schedule Type No Of Date*/
    public void getNoOfAppointmentDateForScheduleTypeApiCall(String OrderId,String TailorId,String DressSubTypeId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNoOfAppointmentDateForScheduleTypeApi("Order/GetTailorServiveDays?OrderId="+OrderId+"&TailorId="+TailorId+"&DressSubTypeId="+DressSubTypeId).enqueue(new Callback<GetNoOfAppointmentDateForScheduleResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetNoOfAppointmentDateForScheduleResponse> call, @NonNull Response<GetNoOfAppointmentDateForScheduleResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getNoOfAppointmentDateForScheduleTypeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetNoOfAppointmentDateForScheduleResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getNoOfAppointmentDateForScheduleTypeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }


    /*Get Order Details Customization*/
    public void getOrderDetailsCustomization(String Order,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getOrderDetailsCustomizationApiCall("Shop/GetOrderDetails?&OrderId="+Order).enqueue(new Callback<GetOrderDetailsCustomizationResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetOrderDetailsCustomizationResponse> call, @NonNull Response<GetOrderDetailsCustomizationResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getOrderDetailsCustomization",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetOrderDetailsCustomizationResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getOrderDetailsCustomization",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Insert Schedule Type*/
    public void insertErrorApiCall(String PageName,String MethodName,String Error,String ApiVersion,String DeviceId, final BaseActivity baseActivity) {
        mServiceInterface.insertErrorApi(PageName,MethodName,Error,ApiVersion,DeviceId,"Customer").enqueue(new Callback<InsertErrorApiCallRespone>() {
            @Override
            public void onResponse(@NonNull Call<InsertErrorApiCallRespone> call, @NonNull Response<InsertErrorApiCallRespone> response) {
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","insertErrorApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertErrorApiCallRespone> call, @NonNull Throwable t) {
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertErrorApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }
    /*Insert Schedule Type*/
    public void insertErrorApiCall(String PageName,String MethodName,String Error,String ApiVersion,String DeviceId, final BaseFragment baseActivity) {
        mServiceInterface.insertErrorApi(PageName,MethodName,Error,ApiVersion,DeviceId,"Customer").enqueue(new Callback<InsertErrorApiCallRespone>() {
            @Override
            public void onResponse(@NonNull Call<InsertErrorApiCallRespone> call, @NonNull Response<InsertErrorApiCallRespone> response) {
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","insertErrorApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertErrorApiCallRespone> call, @NonNull Throwable t) {
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertErrorApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*Get Measurement List Get Parts*/
    public void getMeasurementListGetMeasurementPartsApiCall(String OrderId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getMeasurementListGetMeasurementPartsApiCall("Shop/GetRequestMeasurementParts?&OrderId="+OrderId).enqueue(new Callback<MeasurementListGetMeasurementPartsResponse>() {
            @Override
            public void onResponse(@NonNull Call<MeasurementListGetMeasurementPartsResponse> call, @NonNull Response<MeasurementListGetMeasurementPartsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getOrderDetailsCustomization",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<MeasurementListGetMeasurementPartsResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getOrderDetailsCustomization",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Get Request Details*/
    public void getRequestDetailsApiCall(String OrderId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getRequestDetailsApiCall("Order/GetRequestOrderDetails?OrderId="+OrderId+"&Type=Quotation").enqueue(new Callback<GetRequestDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetRequestDetailsResponse> call, @NonNull Response<GetRequestDetailsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getRequestDetailsApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<GetRequestDetailsResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getRequestDetailsApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Get Related Products*/
    public void getRelatedProductsApiCall(String CardId,String DressTypeId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getRelatedProductsApiCall("Order/GetRelatedproduct?CartId="+CardId+"&DressSubTypeId="+DressTypeId+"&TailorId="+String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId())).enqueue(new Callback<RelatedProductsResponse>() {
            @Override
            public void onResponse(@NonNull Call<RelatedProductsResponse> call, @NonNull Response<RelatedProductsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getRelatedProductsApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<RelatedProductsResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getRelatedProductsApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Get Related Products*/
    public void getOfferProductsApiCall(GetOffersModal getOffersModal, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getOfferProductByCoupanApi(getOffersModal).enqueue(new Callback<GetOfferCategoriesResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetOfferCategoriesResponse> call, @NonNull Response<GetOfferCategoriesResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getOfferProductsApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(@NonNull Call<GetOfferCategoriesResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getOfferProductsApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Get Speical Offer*/
    public void getSpecialOfferApiCall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getSpecialOfferApiCall("Order/Getspecialoffers").enqueue(new Callback<GetSpecialOfferModal>() {
            @Override
            public void onResponse(@NonNull Call<GetSpecialOfferModal> call, @NonNull Response<GetSpecialOfferModal> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getSpecialOfferApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetSpecialOfferModal> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getSpecialOfferApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Get Redeem*/
    public void getRedeemApiCall(String UserId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getRedeemApiCall("order/GetReedem?UserId="+UserId).enqueue(new Callback<GetRedeemResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetRedeemResponse> call, @NonNull Response<GetRedeemResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getRedeemApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetRedeemResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getRedeemApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Get Redeem*/
    public void getRewardsApiCall(String UserId,final BaseFragment baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity.getActivity());
        mServiceInterface.getRewardsApiCall("order/GetRewards?UserId="+UserId).enqueue(new Callback<GetRewardsResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetRewardsResponse> call, @NonNull Response<GetRewardsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getRewardsApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetRewardsResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getRewardsApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Get Coupon Price Api Call*/
    public void getCouponPriceApiCall(String couponcode,String TailorId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getCouponPriceApiCall(couponcode,TailorId).enqueue(new Callback<GetCouponPriceResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCouponPriceResponse> call, @NonNull Response<GetCouponPriceResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getCouponPriceApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCouponPriceResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getCouponPriceApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Apply Coupon Code*/
    public void applyCouponPromoCodeApiCall(int CoupanId,String TailorId ,String DressTypeId,Double BalanceAmount,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.couponPromoCodeApplyApiCall(CoupanId,TailorId,DressTypeId,BalanceAmount).enqueue(new Callback<ApplyPromoCodeResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApplyPromoCodeResponse> call, @NonNull Response<ApplyPromoCodeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","applyCouponCodeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<ApplyPromoCodeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","applyCouponCodeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Apply Converted Points*/
    public void applyConvertedPointsApiCall(String UserId,int UsedPoint ,String OrderId,Double BalanceAmount,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.convertPointsApplyApiCall(UserId,UsedPoint,OrderId,BalanceAmount).enqueue(new Callback<ConvertCashPointResponse>() {
            @Override
            public void onResponse(@NonNull Call<ConvertCashPointResponse> call, @NonNull Response<ConvertCashPointResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","applyConvertedPointsApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<ConvertCashPointResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","applyConvertedPointsApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Get Points By Order*/
    public void getPointsByOrder(String UserId ,String OrderId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getPointsByOrder(UserId,OrderId).enqueue(new Callback<GetPointsByOrderResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetPointsByOrderResponse> call, @NonNull Response<GetPointsByOrderResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getPointsByOrder",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetPointsByOrderResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getPointsByOrder",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Get ReferalCode*/
    public void getReferalCodeApiCall(String UserId ,final BaseFragment baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity.getActivity());
        mServiceInterface.getReferalCodeApiCall("shop/GetReferenceCodeById?UserId="+UserId).enqueue(new Callback<GetReferalCodeResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetReferalCodeResponse> call, @NonNull Response<GetReferalCodeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getReferalCodeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetReferalCodeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getReferalCodeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Get ReferalCode*/
    public void getReferalCodeApiCall(String UserId ,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getReferalCodeApiCall("shop/GetReferenceCodeById?UserId="+UserId).enqueue(new Callback<GetReferalCodeResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetReferalCodeResponse> call, @NonNull Response<GetReferalCodeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getReferalCodeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetReferalCodeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getReferalCodeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }
    /*Get Scrach Card*/
    public void getScrachCardApiCall(String UserId ,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getScrachCardApiCall("Order/GetScrachCardById?UserId="+UserId).enqueue(new Callback<GetScrachCardResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetScrachCardResponse> call, @NonNull Response<GetScrachCardResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getScrachCardApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetScrachCardResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getScrachCardApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Update Scratch Card */
    public void updateScratchCardApiCall(String Id, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.updateScratchCardRewardsPoints(Id).enqueue(new Callback<UpdateRewardsPointsResponse>() {
            @Override
            public void onResponse(@NonNull Call<UpdateRewardsPointsResponse> call, @NonNull Response<UpdateRewardsPointsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","updateScratchCardApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<UpdateRewardsPointsResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","updateScratchCardApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }


    /* Get Card Items  */
    public void getCardItemsApiCall(String CardId, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getCardItemApiCall("cart/GetCartItems?CartId="+CardId).enqueue(new Callback<GetCardItemsResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCardItemsResponse> call, @NonNull Response<GetCardItemsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getCardItemsApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCardItemsResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getCardItemsApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /* Get Measurement Parts List  */
    public void getAddMeasurementPartsList(String Id, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAddMeasurementPartsList("InputInterface/GetMeasurementpartsByMeasurementId?MeasurementId="+Id).enqueue(new Callback<AddMeasurementPartsListResponse>() {
            @Override
            public void onResponse(@NonNull Call<AddMeasurementPartsListResponse> call, @NonNull Response<AddMeasurementPartsListResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getAddMeasurementPartsList",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);
                }
            }

            @Override
            public void onFailure(@NonNull Call<AddMeasurementPartsListResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getCardItemsApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*Reward History */
    public void getRewardHistory(String UserId, final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getRewardHistory("Order/GetRewardPointHistory?UserId="+UserId).enqueue(new Callback<RewardHistoryResponse>() {
            @Override
            public void onResponse(Call<RewardHistoryResponse> call, Response<RewardHistoryResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getRewardHistory",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(Call<RewardHistoryResponse> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });
    }

    /*Remove Card Item*/
    public void removeCardItem(String CartId,String ProductId,String SizeId,String ColorId,String SellerId,final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.removeProductFromCart(CartId,ProductId,SizeId,ColorId,SellerId).enqueue(new Callback<ArrayList<RemoveCardItemResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<RemoveCardItemResponse>> call, Response<ArrayList<RemoveCardItemResponse>> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","removeCardItem",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<RemoveCardItemResponse>> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });

    }

    /*Insert Web Order*/
    public void insertWebOrder(InsertWebOrderBody insertWebOrderBody,final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertWebOrder(insertWebOrderBody).enqueue(new Callback<WebOrderResponse>() {
            @Override
            public void onResponse(Call<WebOrderResponse> call, Response<WebOrderResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                  baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","insertWebOrder",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(Call<WebOrderResponse> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });
    }

    /*Update Tailor Amount*/
    public void updateTailorAmount(String OrderId,String Amount,String TransactionNo, final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.updateTailorAmount(OrderId,Amount,TransactionNo).enqueue(new Callback<UpdateTailorAmountResponse>() {
            @Override
            public void onResponse(Call<UpdateTailorAmountResponse> call, Response<UpdateTailorAmountResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() !=null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","updateTailorAmount",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(Call<UpdateTailorAmountResponse> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });
    }

    /*Get Notification List*/
    public void getNotificationList(String UserId,final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNotificationList("shop/GetBuyerNotification?Type=customer&TypeId="+UserId).enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getNotificationList",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                baseActivity.onRequestFailure(t);
            }
        });
    }

    /*Get Reward Home*/
    public void getRewarHomeApi(String UserId,final BaseFragment baseActivity){
        DialogManager.getInstance().showProgress(baseActivity.getActivity());
        mServiceInterface.getRewardHome("Order/GetRewardsAndOfferHome?UserId="+UserId).enqueue(new Callback<GetRewardsHomeResponse>() {
            @Override
            public void onResponse(Call<GetRewardsHomeResponse> call, Response<GetRewardsHomeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getRewarHomeApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);
                }
            }

            @Override
            public void onFailure(Call<GetRewardsHomeResponse> call, Throwable t) {
                baseActivity.onRequestFailure(t);
            }
        });
    }

    /*Get SubColor*/
    public void getSubColorApiCall(String ColorId,String TailorId,final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getSubColor("order/GetSubColorByColorId?ColorId="+ColorId+"&TailorId="+TailorId).enqueue(new Callback<SubColorResponse>() {
            @Override
            public void onResponse(Call<SubColorResponse> call, Response<SubColorResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getSubColorApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(Call<SubColorResponse> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });
    }

    /*Get Material Selection*/
    public void getMaterialSelection(GetMaterialSelectionModal getMaterialSelectionModal,final BaseActivity baseActivity){
      DialogManager.getInstance().showProgress(baseActivity);
      mServiceInterface.getMaterialSelectionResponse(getMaterialSelectionModal).enqueue(new Callback<GetMaterialSelectionResponse>() {
          @Override
          public void onResponse(Call<GetMaterialSelectionResponse> call, Response<GetMaterialSelectionResponse> response) {
              DialogManager.getInstance().hideProgress();
              if (response.isSuccessful() && response.body() != null){
                  baseActivity.onRequestSuccess(response.body());
              }else {
                  baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                  insertErrorApiCall("APIRequestHandler","getMaterialSelection",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);
              }
          }

          @Override
          public void onFailure(Call<GetMaterialSelectionResponse> call, Throwable t) {
              DialogManager.getInstance().hideProgress();
              baseActivity.onRequestFailure(t);

          }
      });
    }

    /*Get Material Selection*/
    public void getMaterialReview(String patternId,final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getMaterialReview("Order/GetMaterialReviewAndRating?MaterialId="+patternId).enqueue(new Callback<MaterialReviewModal>() {
            @Override
            public void onResponse(Call<MaterialReviewModal> call, Response<MaterialReviewModal> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getMaterialReview",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);
                }
            }
            @Override
            public void onFailure(Call<MaterialReviewModal> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);

            }
        });
    }

    /*Get Delivery Type*/
    public void getDeliveryType(final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getDeliveryTypeApi("Order/GetDelivery").enqueue(new Callback<DeliveryTypeResponse>() {
            @Override
            public void onResponse(Call<DeliveryTypeResponse> call, Response<DeliveryTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getDeliveryType",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);
                }
            }
            @Override
            public void onFailure(Call<DeliveryTypeResponse> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);

            }
        });
    }

    /*Get Store DashBoard */
    public void getDashBoardApiCall(String CartId,final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getStoreDashBaordApi("products/GetDisplayDashboard?CartId="+CartId).enqueue(new Callback<GetStoreDashBoardResponse>() {
            @Override
            public void onResponse(Call<GetStoreDashBoardResponse> call, Response<GetStoreDashBoardResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getDashBoardApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);
                }
            }

            @Override
            public void onFailure(Call<GetStoreDashBoardResponse> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);

            }
        });
    }

    /*Get Store Product List*/
    public void getStoreProductListApi(String BannerId,String CardId , String TypeId, final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getStoreProductListApi("Products/GetProduct_GetAllByBannerId?BannerId="+BannerId+"&CartId="+CardId+"&TypeId="+TypeId).enqueue(new Callback<GetStoreProductListModal>() {
            @Override
            public void onResponse(Call<GetStoreProductListModal> call, Response<GetStoreProductListModal> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getStoreProductListApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(Call<GetStoreProductListModal> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });

    }

    public void getStoreProductDetailsApi(String ProductId,String SellerId,String CardId,final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getStoreProductDetailsApi("Products/GetProductById?Id="+ProductId+"&SellerId="+SellerId+"&cartId="+CardId).enqueue(new Callback<GetProductDetailsResponse>() {
            @Override
            public void onResponse(Call<GetProductDetailsResponse> call, Response<GetProductDetailsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getStoreProductDetailsApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);
                }
            }

            @Override
            public void onFailure(Call<GetProductDetailsResponse> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });

    }

    public void getStoreCartListApi(String CartId,String Key,final BaseFragment baseActivity){
        DialogManager.getInstance().showProgress(baseActivity.getActivity());
        mServiceInterface.getStoreCartListApi("cart/GetCartItems?CartId="+CartId+"&Key="+Key).enqueue(new Callback<StoreCartResponse>() {
            @Override
            public void onResponse(Call<StoreCartResponse> call, Response<StoreCartResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getCartListApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);
                }
            }

            @Override
            public void onFailure(Call<StoreCartResponse> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });

    }

    public void getStoreCartListApi(String CartId,String Key,final BaseActivity baseActivity){
        mServiceInterface.getStoreCartListApi("cart/GetCartItems?CartId="+CartId+"&Key="+Key).enqueue(new Callback<StoreCartResponse>() {
            @Override
            public void onResponse(Call<StoreCartResponse> call, Response<StoreCartResponse> response) {
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getCartListApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);
                }
            }

            @Override
            public void onFailure(Call<StoreCartResponse> call, Throwable t) {
                baseActivity.onRequestFailure(t);
            }
        });

    }

    /*Get Store Product List*/
    public void getSearchStoreProductListApi(String SearchTxt,String CardId, final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getStoreSearchProductListApi("Products/GetProduct_GetAllBySearch?ProductName="+SearchTxt+"&CartId="+CardId).enqueue(new Callback<GetStoreProductListModal>() {
            @Override
            public void onResponse(Call<GetStoreProductListModal> call, Response<GetStoreProductListModal> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getSearchStoreProductListApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(Call<GetStoreProductListModal> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });

    }

    /*Get Order Details Stitching Store*/
    public void getOrderDetailsStitchingStoreApi(String OrderId,String OrderType, final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getOrderDetailsStitchingStoreApi("WebOrders/GetStoreOrderDetails?StoreOrderId="+OrderId+"&OrderTypes="+OrderType).enqueue(new Callback<OrderDetailsStitchingStoreResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsStitchingStoreResponse> call, Response<OrderDetailsStitchingStoreResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getSearchStoreProductListApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(Call<OrderDetailsStitchingStoreResponse> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });

    }

    /*Get Order Details Stitching Store*/
    public void getOrderDetailsStitchingStoreApi(CheckoutDeliveryChargesModal checkoutDeliveryChargesModal, final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getCheckoutDeliveryChargesApi(checkoutDeliveryChargesModal).enqueue(new Callback<CheckoutDeliveryChargesResponse>() {
            @Override
            public void onResponse(Call<CheckoutDeliveryChargesResponse> call, Response<CheckoutDeliveryChargesResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getOrderDetailsStitchingStoreApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(Call<CheckoutDeliveryChargesResponse> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });

    }

    /*Get Store Rating Review*/
    public void getStoreRatingReviewApi(String orderLineId, final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getStoreRatingReviewApi("WebOrders/GetStoreReview?OrderlineId="+orderLineId).enqueue(new Callback<GetStoreReviewRatingModal>() {
            @Override
            public void onResponse(Call<GetStoreReviewRatingModal> call, Response<GetStoreReviewRatingModal> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getStoreRatingReviewApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(Call<GetStoreReviewRatingModal> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });

    }

    /*Get Stitching Tracking Field*/
    public void getStitchingTrackingFieldApi(final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getStitchingTrackingFieldApi("order/GetTrackingField").enqueue(new Callback<TrackingFieldModal>() {
            @Override
            public void onResponse(Call<TrackingFieldModal> call, Response<TrackingFieldModal> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getStitchingTrackingFieldApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }
            @Override
            public void onFailure(Call<TrackingFieldModal> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });

    }

    /*Get Store Selection*/
    public void getStoreReview(String productId,final BaseActivity baseActivity){
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getStoreReview("products/GetStoreReviewByProductIdByBuyer?ProductId="+productId).enqueue(new Callback<MaterialReviewModal>() {
            @Override
            public void onResponse(Call<MaterialReviewModal> call, Response<MaterialReviewModal> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null){
                    baseActivity.onRequestSuccess(response.body());
                }else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getMaterialReview",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);
                }
            }
            @Override
            public void onFailure(Call<MaterialReviewModal> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);

            }
        });
    }
}




