package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.StoreCatogeryAdapter;
import com.qoltech.mzyoonbuyer.entity.StoreCatogeryEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.StoreCartResponse;
import com.qoltech.mzyoonbuyer.service.APICommonInterface;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreCategoryScreen extends BaseActivity {

    @BindView(R.id.store_catogery_par_lay)
    LinearLayout mStoreCatogeryParLay;

    @BindView(R.id.header_store_search_lay)
    RelativeLayout mStoreSearchLay;

    @BindView(R.id.header_store_lay)
    RelativeLayout mStoreLay;

    @BindView(R.id.header_store_catogery_icon)
    ImageView mHeaderStoreCatogeryImg;

    @BindView(R.id.header_store_catogery_txt)
    TextView mHeaderStoreCatogeryTxt;

    @BindView(R.id.store_catogery_rec_view)
    RecyclerView mStoreCatogeryRecView;

    @BindView(R.id.header_store_whis_list_icon)
    ImageView mHeaderStoreWishListImg;

    @BindView(R.id.header_store_whis_list_txt)
    TextView mHeaderStoreWishListTxt;

    @BindView(R.id.bottom_notification_lay)
            RelativeLayout mBottomBadgeLay;

    @BindView(R.id.bottom_notification_txt)
            TextView mBottomNotificationTxt;

    @BindView(R.id.store_catogery_scroll_view_img)
            ImageView mStoreCatogeryScrollViewImg;

    StoreCatogeryAdapter mStoreCatogeryAdapter;

    ArrayList<StoreCatogeryEntity> mStoreCatogeryList;

    UserDetailsEntity mUserDetailsEntityRes;

    private APICommonInterface mServiceInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_store_catogery_screen);

        initView();

    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mStoreCatogeryParLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(StoreCategoryScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        mStoreCatogeryList = new ArrayList<>();

        getLanguage();

        getStoreCatogeryApiCall();

        getStoreCartApiCall();
    }

    @OnClick({R.id.header_store_back_btn_img,R.id.header_wish_list_par_lay,R.id.store_bottom_home_img,R.id.store_bottom_cart_img,R.id.header_store_search_par_lay,R.id.header_store_icon_par_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_store_back_btn_img:
                onBackPressed();
                break;
            case R.id.header_wish_list_par_lay:
                nextScreen(StoreWishListScreen.class,true);
                break;
            case R.id.store_bottom_home_img:
                previousScreen(BottomNavigationScreen.class,true);
                break;
            case R.id.store_bottom_cart_img:
                nextScreen(CartScreen.class,true);
                break;
            case R.id.header_store_search_par_lay:
                nextScreen(StoreSearchScreen.class,true);
                break;
            case R.id.header_store_icon_par_lay:
                previousScreen(StoreDashboardScreen.class,true);
                break;
        }
    }

    public void setHeader(){

        mStoreSearchLay.setVisibility(View.GONE);
        mStoreLay.setVisibility(View.VISIBLE);

        mHeaderStoreCatogeryImg.setBackgroundResource(R.drawable.store_catogery_yellow_icon);
        mHeaderStoreCatogeryTxt.setTextColor(getResources().getColor(R.color.yellow_clr));

        mHeaderStoreWishListImg.setBackgroundResource(R.drawable.store_heart_white_icon);
        mHeaderStoreWishListTxt.setTextColor(getResources().getColor(R.color.white));

    }

    public void getStoreCatogeryApiCall(){
        if (NetworkUtil.isNetworkAvailable(this)){
//            APIRequestHandler.getInstance().getAllStoreCategoryApi(this);
            DialogManager.getInstance().showProgress(this);
            mServiceInterface =  APIRequestHandler.getClient().create(APICommonInterface.class);
            mServiceInterface.getAllStoreCatogeryApi("Products/GetCategories?").enqueue(new Callback<ArrayList<StoreCatogeryEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<StoreCatogeryEntity>> call, Response<ArrayList<StoreCatogeryEntity>> response) {
                    DialogManager.getInstance().hideProgress();
                    if (response.body() != null && response.isSuccessful()){

                        mStoreCatogeryList = new ArrayList<>();

                        for (int i=0 ; i<response.body().size(); i++){
                        if (response.body().get(i).getParentId() == 0)  {
                            mStoreCatogeryList.add(response.body().get(i));
                        }
                        }

                        setCatogeryAdapter(mStoreCatogeryList,response.body());
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<StoreCatogeryEntity>> call, Throwable t) {
                    DialogManager.getInstance().hideProgress();

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStoreCatogeryApiCall();
                }
            });
        }
    }

    public void getStoreCartApiCall() {
        if (NetworkUtil.isNetworkAvailable(this)){
            APIRequestHandler.getInstance().getStoreCartListApi(PreferenceUtil.getStringValue(this,AppConstants.CARD_ID),"List",this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStoreCartApiCall();
                }
            });
        }
    }


    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.store_catogery_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.store_catogery_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    public void setCatogeryAdapter(ArrayList<StoreCatogeryEntity> mFilterCatogeryList , ArrayList<StoreCatogeryEntity> mList){

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mStoreCatogeryAdapter = new StoreCatogeryAdapter(mFilterCatogeryList,this,mList);
        mStoreCatogeryRecView.setLayoutManager(layoutManager);
        mStoreCatogeryRecView.setAdapter(mStoreCatogeryAdapter);

        mStoreCatogeryRecView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = layoutManager.findLastVisibleItemPosition();

                if (lastPosition == mFilterCatogeryList.size()-1){
                    mStoreCatogeryScrollViewImg.setVisibility(View.GONE);
                }else {
                    mStoreCatogeryScrollViewImg.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof StoreCartResponse) {

            int countQtyCart = 0;
            StoreCartResponse mREsponse = (StoreCartResponse)resObj;

            for (int i=0 ; i<mREsponse.getResult().size(); i++){
                countQtyCart = countQtyCart + mREsponse.getResult().get(i).getQuantity();
            }

            mBottomNotificationTxt.setText(String.valueOf(countQtyCart));
            if (countQtyCart > 0){
                mBottomBadgeLay.setVisibility(View.VISIBLE);
            }else {
                mBottomBadgeLay.setVisibility(View.GONE);

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }

}
