package com.qoltech.mzyoonbuyer.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.PaymentInfoEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreWebviewScreen extends BaseActivity {

    UserDetailsEntity mUserDetailsEntityRes;

   @BindView(R.id.webView)
    WebView htmlWebView;

   String mBackHandlerStr = "false";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_store_screen);

        initView();
    }

    public void initView(){
        ButterKnife.bind(this);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(StoreWebviewScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        Toast.makeText(getApplicationContext(),getResources().getString(R.string.loading_plz_wait), Toast.LENGTH_SHORT).show();

        DialogManager.getInstance().showProgress(StoreWebviewScreen.this);
        htmlWebView.setWebViewClient(new CustomWebViewClient());
        WebSettings webSetting = htmlWebView.getSettings();
        webSetting.setUserAgentString("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36");
        webSetting.setAllowFileAccessFromFileURLs(true);
        webSetting.setAllowUniversalAccessFromFileURLs(true);
        htmlWebView.clearCache(true);
        htmlWebView.clearHistory();
        webSetting.setAllowContentAccess(true);
        webSetting.setDomStorageEnabled(true);
        webSetting.setJavaScriptEnabled(true);
        webSetting.setBuiltInZoomControls(true);
        webSetting.setSupportZoom(true);
        webSetting.setLoadWithOverviewMode(true);
        webSetting.setUseWideViewPort(true);
        webSetting.setDisplayZoomControls(false);

        if (AppConstants.LOAD_STORE_WEB.equalsIgnoreCase("STORE")){
            htmlWebView.loadUrl(AppConstants.STORE_BASE_URL+"?customerid="+mUserDetailsEntityRes.getUSER_ID());

        }else if (AppConstants.LOAD_STORE_WEB.equalsIgnoreCase("RELATED")){
            htmlWebView.loadUrl(AppConstants.STORE_BASE_URL+"/products/"+AppConstants.RELATED_PROD_ID+"/"+AppConstants.RELATED_PROD_NAME+"?type=related");

        }

        htmlWebView.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                String[] parts = url.split("\\?");
                String part1 = parts[0];
                if (AppConstants.LOAD_STORE_WEB.equalsIgnoreCase("STORE")) {

                if (part1.equalsIgnoreCase(AppConstants.STORE_BASE_URL+"/GotoPayment")) {

                    String part2 = parts[1];
                    String lastOne = parts[parts.length - 1];

                    AppConstants.STORE_PAYMENT = "STORE_PAYMENT";

                    String[] addressArr = part2.split("&");
                    AppConstants.STORE_USER_DETAILS = new PaymentInfoEntity();

                    for (int i = 0; i < addressArr.length; i++) {
                        String[] addressStr = addressArr[i].split("=");
                        if (addressStr[0].equalsIgnoreCase("OrderId")) {
                            AppConstants.REQUEST_LIST_ID = addressStr[1];
                            AppConstants.STORE_ID = addressStr[1];
                        } else if (addressStr[0].equalsIgnoreCase("Phone")) {

                        } else if (addressStr[0].equalsIgnoreCase("email")) {
                            AppConstants.STORE_USER_DETAILS.setEmail(addressStr[1]);

                        } else if (addressStr[0].equalsIgnoreCase("Address")) {
                            AppConstants.STORE_USER_DETAILS.setLineOne(addressStr[1].replace("%20", " "));
                            AppConstants.STORE_USER_DETAILS.setLineTwo("");
                            AppConstants.STORE_USER_DETAILS.setLineThree("");

                        } else if (addressStr[0].equalsIgnoreCase("Area")) {
                            AppConstants.STORE_USER_DETAILS.setRegion(addressStr[1].replace("%20", " "));

                        } else if (addressStr[0].equalsIgnoreCase("City")) {
                            AppConstants.STORE_USER_DETAILS.setCity(addressStr[1].replace("%20", " "));

                        } else if (addressStr[0].equalsIgnoreCase("Country")) {

                            List<String> PaymentCountryList = Arrays.asList(getResources().getStringArray(R.array.payment_country_list));

                            for (int j = 0; j < PaymentCountryList.size(); j++) {
                                String[] partss = PaymentCountryList.get(j).split("-");
                                String part1s = partss[0];
                                String lastOnes = partss[partss.length - 1];
                                if (part1s.trim().toLowerCase().equalsIgnoreCase(addressStr[1].replace("%20", " ").trim().toLowerCase())) {

                                    AppConstants.STORE_USER_DETAILS.setCountry(lastOnes);
                                }
                            }

                        } else if (addressStr[0].equalsIgnoreCase("Amount")) {
                            AppConstants.TRANSACTION_AMOUNT = addressStr[1];
                        }
                    }

                    htmlWebView.loadUrl(AppConstants.STORE_BASE_URL+"?customerid=" + mUserDetailsEntityRes.getUSER_ID());
                    AppConstants.CHECK_OUT_ORDER = "store";
                    AppConstants.ORDER_ID = "0";
                    AppConstants.APPROVED_TAILOR_ID = "0";
                    nextScreen(CheckoutScreen.class, true);
                }

                if (url.equalsIgnoreCase(AppConstants.STORE_BASE_URL+"/GotoHome")) {
                    mBackHandlerStr = "true";
                    onBackPressed();
                }
                if (url.equalsIgnoreCase(AppConstants.STORE_BASE_URL+"/login")) {
                    htmlWebView.loadUrl(AppConstants.STORE_BASE_URL+"?customerid=" + mUserDetailsEntityRes.getUSER_ID());
                    nextScreen(LoginScreen.class, true);

                }
            }else if (AppConstants.LOAD_STORE_WEB.equalsIgnoreCase("RELATED")){
                    if (url.equalsIgnoreCase(AppConstants.STORE_BASE_URL+"/GotoHome")) {
                        mBackHandlerStr = "true";
                        onBackPressed();
                    }
                    if (url.equalsIgnoreCase(AppConstants.STORE_BASE_URL+"/login")) {
                        htmlWebView.loadUrl(AppConstants.STORE_BASE_URL+"?customerid=" + mUserDetailsEntityRes.getUSER_ID());
                        nextScreen(LoginScreen.class, true);

                    }
                    String search = "CartId";

                    if (url.toLowerCase().indexOf(search.toLowerCase()) != -1) {
                        String[] checking = url.split("/");

                        AppConstants.CARD_ID = checking[checking.length -1];

                        mBackHandlerStr = "true";
                        onBackPressed();

                    }

                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                DialogManager.getInstance().hideProgress();
                super.onPageFinished(view, url);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);

            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }

        });

    }

    private class CustomWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    @Override
    public void onBackPressed() {
        if (mBackHandlerStr.equalsIgnoreCase("true")){
            super.onBackPressed();
            this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);
        }else {
            DialogManager.getInstance().showOptionPopup(StoreWebviewScreen.this, getResources().getString(R.string.sure_want_to_go_back), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                @Override
                public void onNegativeClick() {

                }
                @Override
                public void onPositiveClick() {
                    mBackHandlerStr = "true";
                    onBackPressed();
                }
            });
        }
    }
}
