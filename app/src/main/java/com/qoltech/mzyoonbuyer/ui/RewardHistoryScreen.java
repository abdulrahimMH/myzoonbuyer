package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.RewardHistoryAdapter;
import com.qoltech.mzyoonbuyer.entity.RewardHistoryEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.RewardHistoryResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RewardHistoryScreen extends BaseActivity {

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.reward_history_par_lay)
    LinearLayout mRewardHistoryParLay;

    @BindView(R.id.reward_available_points_txt)
    TextView mRewardAvailablePointsTxt;

    @BindView(R.id.reward_redeem_points_txt)
    TextView mRewardRedeemPointsTxt;

    @BindView(R.id.reward_earned_points_txt)
    TextView mRewardTotalEarnedPointsTxt;

    @BindView(R.id.reward_history_recycler_view)
    RecyclerView mRewardHistoryRecyclerView;

    @BindView(R.id.reward_history_empty_txt)
    TextView mRewardHistoryEmptyTxt;

    private UserDetailsEntity mUserDetailsEntityRes;

    RewardHistoryAdapter mRewardHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_reward_history_screen);
        initView();
    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mRewardHistoryParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        setHeader();

        getLanguage();

        getRewardHistory(mUserDetailsEntityRes.getUSER_ID());

    }

    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }
    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.reward_point_history));
        mRightSideImg.setVisibility(View.INVISIBLE);
        mRewardHistoryEmptyTxt.setText(getResources().getString(R.string.no_result_for_request_history));
    }

    public void getRewardHistory(String UserId){
        if (NetworkUtil.isNetworkAvailable(RewardHistoryScreen.this)){
            APIRequestHandler.getInstance().getRewardHistory(UserId,RewardHistoryScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(RewardHistoryScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getRewardHistory(UserId);
                }
            });
        }

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof RewardHistoryResponse){
            RewardHistoryResponse mResponse = (RewardHistoryResponse)resObj;
            if (mResponse.getResult().size() > 0){
                mRewardAvailablePointsTxt.setText(" : "+ String.valueOf(mResponse.getResult().get(0).getTotalPoints()));
                mRewardRedeemPointsTxt.setText(" : "+ String.valueOf(mResponse.getResult().get(0).getReedemedPoints()));
                mRewardTotalEarnedPointsTxt.setText(" : "+String.valueOf(mResponse.getResult().get(0).getTotalEarnedPoints()));
            }else {
                mRewardAvailablePointsTxt.setText(" : 0");
                mRewardRedeemPointsTxt.setText(" : 0");
                mRewardTotalEarnedPointsTxt.setText(" : 0");
            }

            setTailorListAdapter(mResponse.getResult());
        }
    }

    /*Set Adapter for the Recycler view*/
    public void setTailorListAdapter(ArrayList<RewardHistoryEntity> mRewardHistoryList) {

        if (mRewardHistoryList.size() > 0){
            mRewardHistoryEmptyTxt.setVisibility(View.GONE);
            mRewardHistoryRecyclerView.setVisibility(View.VISIBLE);
        }else {
            mRewardHistoryEmptyTxt.setVisibility(View.VISIBLE);
            mRewardHistoryRecyclerView.setVisibility(View.GONE);
        }

        mRewardHistoryAdapter = new RewardHistoryAdapter(this,mRewardHistoryList);
        mRewardHistoryRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRewardHistoryRecyclerView.setAdapter(mRewardHistoryAdapter);

    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.reward_history_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.reward_history_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }
    }
