package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.FilterTypeAdapter;
import com.qoltech.mzyoonbuyer.entity.FilterTypeEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FilterTypeScreen extends BaseActivity {

    ArrayList<FilterTypeEntity> mFilterEntity;

    @BindView(R.id.filter_type_par_lay)
    LinearLayout mFilterTypeParLay;

    @BindView(R.id.filter_type_recycler_view)
    RecyclerView mFilterRecyclerView;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    private UserDetailsEntity mUserDetailsEntityRes;

    private FilterTypeAdapter mFilterTypeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_filter_selection);

        initView();



    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mFilterTypeParLay);

        setHeader();

        setList();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(FilterTypeScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

    }

    public void setList(){
        if (AppConstants.FILTER_TYPE.equalsIgnoreCase("GENDER")){
            GenderData();
        }else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("OCCASION")){
            OccasionData();
        }
//        else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("PRICE")){
//            PriceData();
//        }
        else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("REGION")){
            RegionData();
        }
    }

    private void GenderData() {
        mFilterEntity = new ArrayList<>();

        FilterTypeEntity filterList = new FilterTypeEntity("Male",AppConstants.NEW_FILTER_GENDER.equalsIgnoreCase("") ? AppConstants.OLD_FILTER_GENDER.equalsIgnoreCase("Male") ? true : false : AppConstants.NEW_FILTER_GENDER.equalsIgnoreCase("Male") ? true :  false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Female", AppConstants.NEW_FILTER_GENDER.equalsIgnoreCase("") ? AppConstants.OLD_FILTER_GENDER.equalsIgnoreCase("Female") ? true : false : AppConstants.NEW_FILTER_GENDER.equalsIgnoreCase("Female") ? true :  false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Boy", AppConstants.NEW_FILTER_GENDER.equalsIgnoreCase("") ? AppConstants.OLD_FILTER_GENDER.equalsIgnoreCase("Boy") ? true : false : AppConstants.NEW_FILTER_GENDER.equalsIgnoreCase("Boy") ? true :  false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Girl", AppConstants.NEW_FILTER_GENDER.equalsIgnoreCase("") ? AppConstants.OLD_FILTER_GENDER.equalsIgnoreCase("Girl") ? true : false : AppConstants.NEW_FILTER_GENDER.equalsIgnoreCase("Girl") ? true :  false);
        mFilterEntity.add(filterList);

        setAdapter(mFilterEntity);

        }
    private void OccasionData() {
        mFilterEntity = new ArrayList<>();

        if (AppConstants.OLD_FILTER_OCCASION_LIST.size()>0){
            mFilterEntity = AppConstants.NEW_FILTER_OCCASION_LIST.size()>0 ? AppConstants.NEW_FILTER_OCCASION_LIST : AppConstants.OLD_FILTER_OCCASION_LIST;
            setAdapter(mFilterEntity);
        }else {
            FilterTypeEntity filterList = new FilterTypeEntity("Ceremonial Dress", false);
            mFilterEntity.add(filterList);

            filterList = new FilterTypeEntity("WaistCoat", false);
            mFilterEntity.add(filterList);

            filterList = new FilterTypeEntity("Wedding", false);
            mFilterEntity.add(filterList);

            filterList = new FilterTypeEntity("Religion Clothing", false);
            mFilterEntity.add(filterList);

            filterList = new FilterTypeEntity("Office Parties", false);
            mFilterEntity.add(filterList);

            filterList = new FilterTypeEntity("Business Lunch Meeting", false);
            mFilterEntity.add(filterList);

            filterList = new FilterTypeEntity("Engagement parties", false);
            mFilterEntity.add(filterList);

            filterList = new FilterTypeEntity("Social Event", false);
            mFilterEntity.add(filterList);

            setAdapter(mFilterEntity);
        }



    }
//    private void PriceData() {
//        mFilterEntity = new ArrayList<>();
//
//        FilterTypeEntity filterList = new FilterTypeEntity("Below AED 199", false);
//        mFilterEntity.add(filterList);
//
//        filterList = new FilterTypeEntity("AED 200-499 ", false);
//        mFilterEntity.add(filterList);
//
//        filterList = new FilterTypeEntity("AED 500-799", false);
//        mFilterEntity.add(filterList);
//
//        filterList = new FilterTypeEntity("AED 900-1199", false);
//        mFilterEntity.add(filterList);
//
//        filterList = new FilterTypeEntity("AED 1200-1499", false);
//        mFilterEntity.add(filterList);
//
//        filterList = new FilterTypeEntity("AED 1500-1799", false);
//        mFilterEntity.add(filterList);
//
//        filterList = new FilterTypeEntity("AED 1800-1999", false);
//        mFilterEntity.add(filterList);
//
//        filterList = new FilterTypeEntity("AED 2000-4999", false);
//        mFilterEntity.add(filterList);
//
//        filterList = new FilterTypeEntity("AED 5000-7999", false);
//        mFilterEntity.add(filterList);
//
//        filterList = new FilterTypeEntity("AED 8000-10000", false);
//        mFilterEntity.add(filterList);
//
//        setAdapter(mFilterEntity);
//
//    }
    private void RegionData() {
        mFilterEntity = new ArrayList<>();

        if (AppConstants.OLD_FILTER_REGION_LIST.size() > 0) {
            setAdapter(AppConstants.NEW_FILTER_REGION_LIST.size() > 0 ? AppConstants.NEW_FILTER_REGION_LIST : AppConstants.OLD_FILTER_REGION_LIST);
        } else {

            FilterTypeEntity filterList = new FilterTypeEntity("India", false);
            mFilterEntity.add(filterList);

            filterList = new FilterTypeEntity("Afghanistan", false);
            mFilterEntity.add(filterList);

            filterList = new FilterTypeEntity("Belgium", false);
            mFilterEntity.add(filterList);

            filterList = new FilterTypeEntity("China", false);
            mFilterEntity.add(filterList);

            filterList = new FilterTypeEntity("Iraq", false);
            mFilterEntity.add(filterList);

            filterList = new FilterTypeEntity("Kuwait", false);
            mFilterEntity.add(filterList);

            filterList = new FilterTypeEntity("Saudi Arabia", false);
            mFilterEntity.add(filterList);

            filterList = new FilterTypeEntity("Malaysia", false);
            mFilterEntity.add(filterList);

            setAdapter(mFilterEntity);
        }
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);

        if (AppConstants.FILTER_TYPE.equalsIgnoreCase("GENDER")){
            mHeaderTxt.setText(getResources().getString(R.string.gender).toUpperCase());
        }else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("OCCASION")){
            mHeaderTxt.setText(getResources().getString(R.string.occasion).toUpperCase());
        }
        else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("REGION")){
            mHeaderTxt.setText(getResources().getString(R.string.region).toUpperCase());
        }
        mRightSideImg.setVisibility(View.INVISIBLE);

    }


    @OnClick({R.id.header_left_side_img,R.id.filter_type_apply_btn,R.id.filter_type_reset_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.filter_type_apply_btn:
                if (AppConstants.FILTER_TYPE.equalsIgnoreCase("OCCASION")){
                    AppConstants.OCCASTION_LIST_BOOL = "NEW";
                    AppConstants.OLD_FILTER_OCCASION = "";

                }else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("REGION")){
                    AppConstants.REGION_LIST_BOOL = "NEW";
                    AppConstants.OLD_FILTER_REGION = "";

                }
                onBackPressed();
                break;
            case R.id.filter_type_reset_btn:
                if (AppConstants.FILTER_TYPE.equalsIgnoreCase("GENDER")){
                    AppConstants.NEW_FILTER_GENDER = "";

                }else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("OCCASION")){
                    AppConstants.OLD_FILTER_OCCASION = "NONE";
                    AppConstants.NEW_FILTER_OCCASION_LIST = new ArrayList<>();
                    AppConstants.OLD_FILTER_OCCASION_LIST = new ArrayList<>();
                    AppConstants.OCCASTION_LIST_BOOL = "OLD";

                }else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("REGION")){
                    AppConstants.OLD_FILTER_REGION = "NONE";
                    AppConstants.NEW_FILTER_REGION_LIST = new ArrayList<>();
                    AppConstants.OLD_FILTER_REGION_LIST = new ArrayList<>();
                    AppConstants.REGION_LIST_BOOL = "OLD";

                }

                setList();
                break;

        }

    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<FilterTypeEntity> mTailorList) {

//        if (mFilterTypeAdapter == null) {

            mFilterTypeAdapter = new FilterTypeAdapter(this,mTailorList);
            mFilterRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            mFilterRecyclerView.setAdapter(mFilterTypeAdapter);
//        } else {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    mFilterTypeAdapter.notifyDataSetChanged();
//                }
//            });
//        }

    }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.filter_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.filter_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
