package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetTrackingEntity;
import com.qoltech.mzyoonbuyer.entity.TrackingFieldEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.service.APICommonInterface;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreOrderTrackingDetailsScreen extends BaseActivity {

    @BindView(R.id.order_tracking_details_pay_lay)
    LinearLayout mOrderTrackingDetailsPayLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.order_placed_date_txt)
    TextView mOrderPlacedDateTxt;

    @BindView(R.id.order_placed_txt)
    TextView mOrderPlacedTxt;

    @BindView(R.id.payment_received_date_txt)
    TextView mPaymentReceivedDateTxt;

    @BindView(R.id.payment_received_txt)
    TextView mPaymentReceivedTxt;

    @BindView(R.id.payment_received_dotted_img)
    ImageView mPaymentReceivedDottedImg;

    @BindView(R.id.payment_received_img)
    ImageView mPaymentReceivedImg;

    @BindView(R.id.processing_date_txt)
    TextView mProcessingDateTxt;

    @BindView(R.id.processing_txt)
    TextView mProcessingTxt;

    @BindView(R.id.processing_dotted_img)
    ImageView mProcessingDottedImg;

    @BindView(R.id.processing_img)
    ImageView mProcessingImg;

    @BindView(R.id.order_packed_date_txt)
    TextView mOrderPackedDateTxt;

    @BindView(R.id.order_packed_txt)
    TextView mOrderPackedTxt;

    @BindView(R.id.order_packed_dot_img)
    ImageView mOrderPackedDottedImg;

    @BindView(R.id.order_packed_img)
    ImageView mOrderPackedImg;

    @BindView(R.id.ready_to_dispact_date_txt)
    TextView mReadyToDispactDateTxt;

    @BindView(R.id.ready_for_dispatch_txt)
    TextView mReadyToDispactTxt;

    @BindView(R.id.ready_to_dispact_dotted_img)
    ImageView mReadyToDispactDottedImg;

    @BindView(R.id.ready_to_dispact_img)
    ImageView mReadyToDispactImg;

    @BindView(R.id.dispacted_date_txt)
    TextView mDispactedDateTxt;

    @BindView(R.id.dispacted_txt)
    TextView mDispactedTxt;

    @BindView(R.id.dispacted_dotted_img)
    ImageView mDispactedDottedImg;

    @BindView(R.id.dispacted_img)
    ImageView mDispactedImg;

    @BindView(R.id.out_of_delivery_date_txt)
    TextView mOutOfDeliveryDateTxt;

    @BindView(R.id.out_of_delivery_txt)
    TextView mOutOfDeliveryTxt;

    @BindView(R.id.out_of_delivery_dotted_img)
    ImageView mOutOfDeliveryDottedImg;

    @BindView(R.id.out_of_delivery_img)
    ImageView mOutOfDeliveryImg;

    @BindView(R.id.delivered_date_txt)
    TextView mDeliveredDateTxt;

    @BindView(R.id.delivered_txt)
    TextView mDeliveredTxt;

    @BindView(R.id.delivered_dotted_img)
    ImageView mDeliveredDottedImg;

    @BindView(R.id.delivered_img)
    ImageView mDelivertedImg;

    APICommonInterface mApiCommonInterface;

    private UserDetailsEntity mUserDetailsEntityRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_store_order_tracking_details_screen);
        initView();
    }
    public void initView(){

        ButterKnife.bind(this);

        setupUI(mOrderTrackingDetailsPayLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        mApiCommonInterface = APIRequestHandler.getClient().create(APICommonInterface.class);

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(StoreOrderTrackingDetailsScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        getTrackingField();

        getStoreTrackingApiCall();

        setHeader();

    }

    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.tracking_details));
        mRightSideImg.setVisibility(View.INVISIBLE);

    }

    public void getStoreTrackingApiCall(){
        if (NetworkUtil.isNetworkAvailable(StoreOrderTrackingDetailsScreen.this)){
            mApiCommonInterface.getStoreTrackingApi("products/GetStoreTrackingDetails?OrderLineId="+AppConstants.ORDER_PENDING_LINE_ID).enqueue(new Callback<ArrayList<GetTrackingEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<GetTrackingEntity>> call, Response<ArrayList<GetTrackingEntity>> response) {
                    if (response.body() != null){

                        for (int i=0; i<response.body().size(); i++){
                            if (i == 0){
                                mOrderPlacedDateTxt.setText(response.body().get(i).getDate());
                            }else if (i == 1){
                                mPaymentReceivedDateTxt.setText(response.body().get(i).getDate());
                                mPaymentReceivedDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
                                mPaymentReceivedImg.setBackgroundResource(R.drawable.tracking_payment_received_blue);

                            }else if (i == 2){
                                mProcessingDateTxt.setText(response.body().get(i).getDate());
                                mProcessingDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
                                mProcessingImg.setBackgroundResource(R.drawable.tracking_processing_blue);

                            }else if (i == 3){
                                mOrderPackedDateTxt.setText(response.body().get(i).getDate());
                                mOrderPackedDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
                                mOrderPackedImg.setBackgroundResource(R.drawable.tracking_order_packed_blue);

                            }else if (i == 4){
                                mReadyToDispactDateTxt.setText(response.body().get(i).getDate());
                                mReadyToDispactDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
                                mReadyToDispactImg.setBackgroundResource(R.drawable.ready_for_dispatching_blue_img);

                            }else if (i == 5){
                                mDispactedDateTxt.setText(response.body().get(i).getDate());
                                mDispactedDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
                                mDispactedImg.setBackgroundResource(R.drawable.dispatched_blue_img);

                            }else if (i == 6){
                                mOutOfDeliveryDateTxt.setText(response.body().get(i).getDate());
                                mOutOfDeliveryDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
                                mOutOfDeliveryImg.setBackgroundResource(R.drawable.out_of_delivery_blue_img);

                            }else if (i == 7){
                                mDeliveredDateTxt.setText(response.body().get(i).getDate());
                                mDeliveredDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
                                mDelivertedImg.setBackgroundResource(R.drawable.delivered_blue_img);

                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetTrackingEntity>> call, Throwable t) {

                }
            });
        }else {
            getStoreTrackingApiCall();
        }
    }

    public void getTrackingField(){
        if (NetworkUtil.isNetworkAvailable(StoreOrderTrackingDetailsScreen.this)){
            mApiCommonInterface.getStoreTrackingFieldApi("Products/GetTrackingField").enqueue(new Callback<ArrayList<TrackingFieldEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<TrackingFieldEntity>> call, Response<ArrayList<TrackingFieldEntity>> response) {
                    for (int i=0; i<response.body().size(); i++){
                        if (i == 0){
                            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                                mOrderPlacedTxt.setText(response.body().get(i).getStatusInArabic());
                            }else {
                                mOrderPlacedTxt.setText(response.body().get(i).getStatus());
                            }
                        }else if (i == 1){
                            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                                mPaymentReceivedTxt.setText(response.body().get(i).getStatusInArabic());
                            }else {
                                mPaymentReceivedTxt.setText(response.body().get(i).getStatus());
                            }
                        }else if (i == 2){
                            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                                mProcessingTxt.setText(response.body().get(i).getStatusInArabic());
                            }else {
                                mProcessingTxt.setText(response.body().get(i).getStatus());
                            }
                        }else if (i == 3){
                            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                                mOrderPackedTxt.setText(response.body().get(i).getStatusInArabic());
                            }else {
                                mOrderPackedTxt.setText(response.body().get(i).getStatus());
                            }
                        }else if (i == 4){
                            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                                mReadyToDispactTxt.setText(response.body().get(i).getStatusInArabic());
                            }else {
                                mReadyToDispactTxt.setText(response.body().get(i).getStatus());
                            }
                        }else if (i == 5){
                            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                                mDispactedTxt.setText(response.body().get(i).getStatusInArabic());
                            }else {
                                mDispactedTxt.setText(response.body().get(i).getStatus());
                            }
                        }else if (i == 6){
                            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                                mOutOfDeliveryTxt.setText(response.body().get(i).getStatusInArabic());
                            }else {
                                mOutOfDeliveryTxt.setText(response.body().get(i).getStatus());
                            }
                        }else if (i == 7){
                            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                                mDeliveredTxt.setText(response.body().get(i).getStatusInArabic());
                            }else {
                                mDeliveredTxt.setText(response.body().get(i).getStatus());
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<TrackingFieldEntity>> call, Throwable t) {

                }
            });
        }else {
            getTrackingField();
        }
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.order_tracking_details_pay_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.order_tracking_details_pay_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
