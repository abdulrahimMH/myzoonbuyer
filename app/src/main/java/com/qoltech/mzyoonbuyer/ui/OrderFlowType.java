package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.OrderFlowTypeModal;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderFlowType extends BaseActivity {

    @BindView(R.id.tailor_type_par_lay)
    LinearLayout mTailorTypeParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.get_a_quote_from_tailor_img)
    ImageView mGetAQuoteFromTailorImg;

    @BindView(R.id.select_tailor_for_direct_order_img)
    ImageView mSelectTailorForDirectOrderImg;

    @BindView(R.id.get_a_quote_from_tailor_txt)
    TextView mGetAQuoteFromTailorTxt;

    @BindView(R.id.select_tailor_for_direct_order_txt)
    TextView mSelectTailorForDirectOrderTxt;

    @BindView(R.id.order_flow_type_wiz_lay)
    RelativeLayout mOrderflowTypeWizLay;

    @BindView(R.id.tailor_type_wizard_par_lay)
    RelativeLayout mTailorTypeWizLay;

    private UserDetailsEntity mUserDetailsEntityRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_tailor_type_screen);

        initView();
    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mTailorTypeParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(OrderFlowType.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        setHeader();

        getLanguage();

        getOrderFlowApiCall();
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.order_flow_type));
        mRightSideImg.setVisibility(View.INVISIBLE);

        mOrderflowTypeWizLay.setVisibility(View.VISIBLE);
        mTailorTypeWizLay.setVisibility(View.GONE);

    }

    public void getOrderFlowApiCall(){
        if (NetworkUtil.isNetworkAvailable(OrderFlowType.this)){
            APIRequestHandler.getInstance().getOrderFlowTypeApiCall(OrderFlowType.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderFlowType.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getOrderFlowApiCall();
                }
            });
        }
    }

    @OnClick({R.id.header_left_side_img,R.id.get_a_quote_from_tailor_btn,R.id.select_tailor_for_direct_order_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.get_a_quote_from_tailor_btn:
                AppConstants.DIRECT_ORDER = "TAILOR_FLOW";
                nextScreen(TailorListScreen.class, true);
                break;
            case R.id.select_tailor_for_direct_order_btn:
                AppConstants.DIRECT_ORDER = "NORMAL_FLOW";
                nextScreen(OrderTypeScreen.class, true);
                break;
        }

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        OrderFlowTypeModal mResponse = (OrderFlowTypeModal)resObj;

        if (mResponse.getResult().size() > 0){

            try {
                Glide.with(OrderFlowType.this)
                        .load(AppConstants.IMAGE_BASE_URL+"Images/orderflow/"+mResponse.getResult().get(0).getOrderTypeImage())
                        .apply(new RequestOptions().placeholder(R.drawable.gender_background_img).error(R.drawable.gender_background_img))
                        .into(mGetAQuoteFromTailorImg);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }

            try {
                Glide.with(OrderFlowType.this)
                        .load(AppConstants.IMAGE_BASE_URL+"Images/orderflow/"+mResponse.getResult().get(1).getOrderTypeImage())
                        .apply(new RequestOptions().placeholder(R.drawable.gender_background_img).error(R.drawable.gender_background_img))
                        .into(mSelectTailorForDirectOrderImg);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }

            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                mGetAQuoteFromTailorTxt.setText(mResponse.getResult().get(0).getOrderTypeInArabic());
                mSelectTailorForDirectOrderTxt.setText(mResponse.getResult().get(1).getOrderTypeInArabic());

            }else {
                mGetAQuoteFromTailorTxt.setText(mResponse.getResult().get(0).getOrderType());
                mSelectTailorForDirectOrderTxt.setText(mResponse.getResult().get(1).getOrderType());
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        previousScreen(SubTypeScreen.class,true);
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.tailor_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.tailor_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
}
