package com.qoltech.mzyoonbuyer.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.DeviceDetailsResponse;
import com.qoltech.mzyoonbuyer.modal.FileUploadResponse;
import com.qoltech.mzyoonbuyer.modal.ProfileIntroResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class ProfileIntroScreen extends BaseActivity {

    @BindView(R.id.profile_intro_par_lay)
    RelativeLayout mProfileIntroParLay;

    @BindView(R.id.profile_intro_edt_txt)
    EditText mProfileNameEdtTxt;

    @BindView(R.id.profile_intro_img_lay)
    RelativeLayout mProfileIntroImgLay;

    @BindView(R.id.profile_intro_camera_img)
    ImageView mProfileIntroCameraImg;

    @BindView(R.id.profile_referal_edt_txt)
    EditText mProfileReferalEdtTxt;

    @BindView(R.id.profile_intro_image)
    de.hdodenhof.circleimageview.CircleImageView mProfileIntroImg;

    @BindView(R.id.profile_email_edt_txt)
    EditText mProfileEmailEdtTxt;

    public final int REQUEST_CAMERA = 999;
    public final int REQUEST_GALLERY = 888;

    String IMAGE_PATH = "";
    private static final String IMAGE_DIRECTORY = "/MZYOON";

    private File mUserImageFile = null;

    private UserDetailsEntity mUserDetailsEntity;

    private String mBackStr = "false";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_profile_intro_screen);
        initView();
    }

    public void initView(){
        ButterKnife.bind(this);

        setupUI(mProfileIntroParLay);
        mUserDetailsEntity = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(ProfileIntroScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntity = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();
    }

    @OnClick({R.id.profile_intro_nxt_btn,R.id.profile_intro_img_lay})
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.header_left_side_img:
//                onBackPressed();
//                break;
            case R.id.profile_intro_nxt_btn:
                if (mProfileNameEdtTxt.getText().toString().trim().isEmpty()){
                    mProfileNameEdtTxt.clearAnimation();
                    mProfileNameEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mProfileNameEdtTxt.setError(getResources().getString(R.string.please_fill_your_name));
                }
//                else if (AppConstants.USER_IMAGE.equalsIgnoreCase("")){
//                    Toast.makeText(ProfileIntroScreen.this,getResources().getString(R.string.please_give_profile_pic),Toast.LENGTH_SHORT).show();
//                    mProfileIntroImg.clearAnimation();
//                    mProfileIntroImg.setAnimation(ShakeErrorUtils.shakeError());
//                    mProfileIntroCameraImg.clearAnimation();
//                    mProfileIntroCameraImg.setAnimation(ShakeErrorUtils.shakeError());
//                }
                else if (mProfileEmailEdtTxt.getText().toString().trim().isEmpty()){
                    mProfileEmailEdtTxt.clearAnimation();
                    mProfileEmailEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mProfileEmailEdtTxt.setError(getResources().getString(R.string.please_give_your_email_address));
                }else if (!isEmailValid(mProfileEmailEdtTxt.getText().toString().trim())){
                    mProfileEmailEdtTxt.clearAnimation();
                    mProfileEmailEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mProfileEmailEdtTxt.setError(getResources().getString(R.string.please_give_valid_email_address));
                }
                else{
                    if (!IMAGE_PATH.isEmpty()){
                        uploadProfileImageApiCall();
                    }else {
                        profileIntroApiCall("");
                    }
                    }
                break;
            case R.id.profile_intro_img_lay:
                if (checkPermission()){
                    uploadImage();
                }
                break;
        }

    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

//    public void setHeader(){
//        mHeaderTxt.setVisibility(View.VISIBLE);
//        mHeaderTxt.setText(getResources().getString(R.string.profile));
//        mRightSideImg.setVisibility(View.INVISIBLE);
//        mHeaderLeftBackBtn.setVisibility(View.INVISIBLE);
//
//    }

    public void profileIntroApiCall(final String profileImg){

        if (NetworkUtil.isNetworkAvailable(ProfileIntroScreen.this)){
            APIRequestHandler.getInstance().profileIntroAPICall(AppConstants.USER_ID,mProfileNameEdtTxt.getText().toString().trim(),profileImg,mProfileEmailEdtTxt.getText().toString(),mProfileReferalEdtTxt.getText().toString().trim(),"Profile",ProfileIntroScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ProfileIntroScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    profileIntroApiCall(profileImg);
                }
            });
        }

    }
    public void uploadProfileImageApiCall(){
        if (NetworkUtil.isNetworkAvailable(ProfileIntroScreen.this)){
            if (!IMAGE_PATH.isEmpty()) {

                mUserImageFile = new File(IMAGE_PATH);

                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), mUserImageFile);

                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("BuyerImages", mUserImageFile.getName(), requestFile);
                APIRequestHandler.getInstance().updateProfile1(body,ProfileIntroScreen.this);
            }
        }else {
            uploadProfileImageApiCall();
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof ProfileIntroResponse){
            ProfileIntroResponse mResponse = (ProfileIntroResponse)resObj;
            if (mResponse.getResult().equalsIgnoreCase("1")){
                PreferenceUtil.storeBoolPreferenceValue(ProfileIntroScreen.this,
                        AppConstants.LOGIN_STATUS, true);
                mUserDetailsEntity.setUSER_ID(String.valueOf(AppConstants.USER_ID));
                mUserDetailsEntity.setUSER_NAME(mProfileNameEdtTxt.getText().toString());
                mUserDetailsEntity.setMOBILE_NUM(AppConstants.MOBILE_NUM);
                mUserDetailsEntity.setCOUNTRY_CODE(AppConstants.COUNTRY_CODE);

                PreferenceUtil.storeUserDetails(ProfileIntroScreen.this,mUserDetailsEntity);
                if (AppConstants.SKIP_NOW.equalsIgnoreCase("CUSTOMIZATION")){
                    previousScreen(CustomizationThreeScreen.class,true);
                }else if (AppConstants.SKIP_NOW.equalsIgnoreCase("CART")){
                    nextScreen(BottomNavigationScreen.class, true);
                }else {
                    nextScreen(BottomNavigationScreen.class, true);
                }
//                nextScreen(BottomNavigationScreen.class,true);
            }
        }
        if (resObj instanceof FileUploadResponse){
            FileUploadResponse mResponse = (FileUploadResponse) resObj;
            String[] parts = mResponse.getResult().get(0).split("\\\\");
            String part1 = parts[0]; // 004
            String part2 = parts[1];
            String lastOne = parts[parts.length - 1];
            mUserDetailsEntity.setUSER_IMAGE(lastOne);

            profileIntroApiCall(lastOne);
            PreferenceUtil.storeUserDetails(ProfileIntroScreen.this,mUserDetailsEntity);
        }
        if (resObj instanceof DeviceDetailsResponse){
            DeviceDetailsResponse mRespone = (DeviceDetailsResponse)resObj;

        }
    }



    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    /* Ask for permission on Camera access*/
    private boolean checkPermission() {

        boolean addPermission = true;
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            int cameraPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
            int readStoragePermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
            int storagePermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (storagePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            addPermission = askAccessPermission(listPermissionsNeeded, 1, new InterfaceTwoBtnCallBack() {
                @Override
                public void onNegativeClick() {

                }


                @Override
                public void onPositiveClick() {
                    uploadImage();
                }


            });
        }

        return addPermission;

    }

    private void uploadImage() {
        DialogManager.getInstance().showImageUploadPopup(this, getResources().getString(R.string.select_phot_type), getResources().getString(R.string.take_camera), getResources().getString(R.string.open_gallery), new InterfaceTwoBtnCallBack() {
                    @Override
                    public void onNegativeClick() {
                        captureImage();
                    }

                    @Override
                    public void onPositiveClick() {
                        galleryImage();
                    }
                });
    }

    /*open camera Image*/
    private void captureImage() {

        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);

    }

    /*open gallery Image*/
    private void galleryImage() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, REQUEST_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            /*Open Camera Request Check*/
            case REQUEST_CAMERA:
                if (resultCode == RESULT_OK) {

                    Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//                    String path = saveImage(thumbnail);
                    IMAGE_PATH = compressImage(String.valueOf(getImageUri(getContext(),thumbnail)));
//                    IMAGE_PATH = path;
//                    saveImage(thumbnail);

                    AppConstants.USER_IMAGE = IMAGE_PATH;
//
                    Glide.with(this)
                            .load(IMAGE_PATH)
                            .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.profile_place_holder_icon).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true))
                            .into(mProfileIntroImg);

                } else {
                    if (resultCode == RESULT_CANCELED) {
                        /*Cancelling the image capture process by the user*/
                    } else {
                        /*image capture getting failed due to certail technical issues*/
                    }
                }
                break;
            /*Open Photo Gallery Request Check*/
            case REQUEST_GALLERY:
                if (resultCode == RESULT_OK) {
                    // successfully captured the image
                    // display it in image view
//                    Uri selectedImagePath = data.getData();

                    Uri contentURI = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), contentURI);
//                        String path = saveImage(bitmap);
                        IMAGE_PATH = compressImage(String.valueOf(contentURI));

                        AppConstants.USER_IMAGE = IMAGE_PATH;
                        Glide.with(this)
                                .load(IMAGE_PATH)
                                .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.profile_place_holder_icon).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true))
                                .into(mProfileIntroImg);

                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "I Can't find the location this file!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    if (resultCode == RESULT_CANCELED) {
                        /*Cancelling the image capture process by the user*/

                    } else {
                        /*image capture getting failed due to certail technical issues*/
                    }
                }
                break;

        }
    }
    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            if (checkPermission() == true) {
                wallpaperDirectory.mkdirs();

            }
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getApplicationContext(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
        }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {               imgRatio = maxHeight / actualHeight;                actualWidth = (int) (imgRatio * actualWidth);               actualHeight = (int) maxHeight;             } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;

        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height/ (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;      }       final float totalPixels = width * height;       final float totalReqPixelsCap = reqWidth * reqHeight * 2;       while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public void getLanguage(){

        if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.profile_intro_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
//            mProfileNameEdtTxt.setGravity(Gravity.END|Gravity.CENTER_VERTICAL);
        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.profile_intro_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            mProfileNameEdtTxt.setGravity(Gravity.START|Gravity.CENTER_VERTICAL);


        }
    }

    @Override
    public void onBackPressed() {
        if (mBackStr.equalsIgnoreCase("true")){
            super.onBackPressed();
            this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);
        }
    }
}
