package com.qoltech.mzyoonbuyer.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.CustomizationDressTypeAdapter;
import com.qoltech.mzyoonbuyer.adapter.CustomizationThreeAdapter;
import com.qoltech.mzyoonbuyer.adapter.CustomizationThreeDialogAdapter;
import com.qoltech.mzyoonbuyer.entity.CustomizationAttributesEntity;
import com.qoltech.mzyoonbuyer.entity.DressSubTypeImagesEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.CustomizationThreeGetResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class CustomizationThreeScreen extends BaseActivity {

    private CustomizationThreeAdapter mCustomizeAdapter;
    private CustomizationThreeDialogAdapter mCustomizationThreeDialogAdapter;
    private ArrayList<CustomizationThreeScreen> mCustomizationList;
    private ArrayList<CustomizationAttributesEntity> mCustomizationDialogList;
    CustomizationDressTypeAdapter mCustomizationDressTypeAdapter;

    @BindView(R.id.customization_three_par_lay)
    RelativeLayout mCustomizationThreeParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.customize_three_recycler_view)
    RecyclerView mCustomizatinThreeRecyclerView;

    @BindView(R.id.customiztion_three_dress_type_recycler_view)
    RecyclerView mCustomizationThreeDressTypeRecyclerView;

    @BindView(R.id.customization_three_full_img)
    public ImageView mCustomizationThreeFullImg;

    @BindView(R.id.customization_three_next_btn)
    RelativeLayout mCustomizationNextBtn;

    String mCustomizationThreeBackStr  ,mCustomizationHintLapelTxt = "",mListOneTxtStr = "",mListOneImgStr = "",mListTwoTxtStr = "",mListTwoImgStr = "" ;
    int mCountInt = 0;

    private UserDetailsEntity mUserDetailsEntityRes;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    @BindView(R.id.new_flow_customization_wiz_lay)
    RelativeLayout mNewFlowCustomizationWizLay;

    @BindView(R.id.customization_wiz_lay)
    RelativeLayout mCustomizationWizLay;

    private Dialog mCustomizationHintDialog;

    @BindView(R.id.customization_three_scroll_view_img)
    ImageView mCustomizationThreeScrollViewImg;

    int mCountHintInt = 1,showingPopUp = 1;

    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;
    final long PERIOD_MS = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_customization_three_screen);

        initView();
    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mCustomizationThreeParLay);

        setHeader();

        mCustomizationList = new ArrayList<>();

        mCustomizationDialogList = new ArrayList<>();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(CustomizationThreeScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")){
            getHintDialog();
        }

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            customizationThreeNewFlowApiCall();
            mCustomizationWizLay.setVisibility(View.GONE);
            mNewFlowCustomizationWizLay.setVisibility(View.VISIBLE);
        }else {
            customizationThreeApiCall();
            mCustomizationWizLay.setVisibility(View.VISIBLE);
            mNewFlowCustomizationWizLay.setVisibility(View.GONE);
        }
    }

    public void customizationThreeApiCall(){
        if (NetworkUtil.isNetworkAvailable(CustomizationThreeScreen.this)){
            APIRequestHandler.getInstance().customizationThreeApiCall(AppConstants.SUB_DRESS_TYPE_ID,CustomizationThreeScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationThreeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    customizationThreeApiCall();
                }
            });
        }
    }

    public void customizationThreeNewFlowApiCall(){
        if (NetworkUtil.isNetworkAvailable(CustomizationThreeScreen.this)){
            APIRequestHandler.getInstance().customizationNewFlowThreeApiCall(String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId()),AppConstants.SUB_DRESS_TYPE_ID,CustomizationThreeScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationThreeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    customizationThreeNewFlowApiCall();
                }
            });
        }
    }
    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof CustomizationThreeGetResponse){
            CustomizationThreeGetResponse mResponse = (CustomizationThreeGetResponse)resObj;

            if (mResponse.getResult().getCustomizationAttributes().size() > 0 || mResponse.getResult().getDressSubTypeImages().size() > 0 || mResponse.getResult().getAttributeImages().size()>0){
                mEmptyListLay.setVisibility(mResponse.getResult().getCustomizationAttributes().size()>0 ? View.GONE : View.VISIBLE);
                mCustomizatinThreeRecyclerView.setVisibility(mResponse.getResult().getCustomizationAttributes().size()>0 ? View.VISIBLE : View.GONE);

                for (int i=0; i<mResponse.getResult().getCustomizationAttributes().size();i++){
                    if (i == 0){
                        AppConstants.CUSTOMIZATION_NAME = mResponse.getResult().getCustomizationAttributes().get(0).getAttributeNameInEnglish();
                        AppConstants.CUSTOMIZATION_DRESS_TYPE_ID = String.valueOf(mResponse.getResult().getCustomizationAttributes().get(0).getId());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mCustomizationHintLapelTxt = mResponse.getResult().getCustomizationAttributes().get(0).getAttributeNameinArabic();

                        }else {
                            mCustomizationHintLapelTxt = mResponse.getResult().getCustomizationAttributes().get(0).getAttributeNameInEnglish();

                        }
                    }
                }
                mCustomizationDialogList = mResponse.getResult().getCustomizationAttributes();
                AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST = mCustomizationDialogList;

                mCountInt = mResponse.getResult().getCustomizationAttributes().size();

                setAdapter(mResponse.getResult().getCustomizationAttributes());

                setDressTypeAdapter(mResponse.getResult().getDressSubTypeImages());
                    if (mResponse.getResult().getDressSubTypeImages().size() > 0){
                        mCustomizationThreeBackStr = mResponse.getResult().getDressSubTypeImages().get(0).getImages();

                    }
                try {
                    Glide.with(CustomizationThreeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"images/Customazation3/"+mCustomizationThreeBackStr)
                            .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                            .into(mCustomizationThreeFullImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
            }
        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    public void setAdapter(ArrayList<CustomizationAttributesEntity> mCustomizationList){

        mEmptyListLay.setVisibility(mCustomizationList.size()>0 ? View.GONE : View.VISIBLE);
        mCustomizatinThreeRecyclerView.setVisibility(mCustomizationList.size()>0 ? View.VISIBLE : View.GONE);

        if (showingPopUp == 1){
            showingPopUp = ++showingPopUp;

            if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")){
                getHintDialog();

            }
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mCustomizationThreeDialogAdapter = new CustomizationThreeDialogAdapter(this,mCustomizationList);
        mCustomizatinThreeRecyclerView.setLayoutManager(layoutManager);
        mCustomizatinThreeRecyclerView.setAdapter(mCustomizationThreeDialogAdapter);

        mCustomizatinThreeRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = layoutManager.findLastVisibleItemPosition();

                if (lastPosition == mCustomizationList.size()-1){
                    mCustomizationThreeScrollViewImg.setVisibility(View.GONE);
                }else {
                    mCustomizationThreeScrollViewImg.setVisibility(View.VISIBLE);
                }
            }
        });

    }
    public void setDressTypeAdapter(ArrayList<DressSubTypeImagesEntity> dressSubTypeImages){

        mCustomizationDressTypeAdapter = new CustomizationDressTypeAdapter(CustomizationThreeScreen.this,dressSubTypeImages);
        mCustomizationThreeDressTypeRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        mCustomizationThreeDressTypeRecyclerView.setAdapter(mCustomizationDressTypeAdapter);

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == dressSubTypeImages.size()) {
                    currentPage = 0;
                }
                mCustomizationThreeDressTypeRecyclerView.scrollToPosition(currentPage++);

            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);

    }
    @OnClick({R.id.customization_three_next_btn,R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.customization_three_next_btn:
                if (AppConstants.CUSTOMIZATION_CLICK_ARRAY.size() != mCountInt){
                    Toast.makeText(CustomizationThreeScreen.this,getResources().getString(R.string.please_select_all_the_customization),Toast.LENGTH_SHORT).show();
                    mCustomizationNextBtn.clearAnimation();
                    mCustomizationNextBtn.setAnimation(ShakeErrorUtils.shakeError());
                }else if (mUserDetailsEntityRes.getUSER_ID().equalsIgnoreCase("0")) {
                    DialogManager.getInstance().showOptionPopup(CustomizationThreeScreen.this, getResources().getString(R.string.please_login_to_proceed), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                        @Override
                        public void onNegativeClick() {

                        }
                        @Override
                        public void onPositiveClick() {
                            AppConstants.SKIP_NOW = "CUSTOMIZATION";
                            HashMap<String,String> productIdist = new HashMap<>();
                            PreferenceUtil.storeRecentProducts(CustomizationThreeScreen.this,productIdist);
                            PreferenceUtil.storeBoolPreferenceValue(getContext(),AppConstants.LOGIN_STATUS,false);
                            nextScreen(LoginScreen.class,false);
                        }
                    });
                }else {
                    if (mCountInt == 0){
                        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
                            DialogManager.getInstance().showOptionPopup(CustomizationThreeScreen.this, getResources().getString(R.string.no_customizatin_for_selected_tailor), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                                @Override
                                public void onNegativeClick() {

                                }
                                @Override
                                public void onPositiveClick() {
                                    nextScreen(MeasurementOneScreen.class,true);
                                }
                            });
                        }else {
                            DialogManager.getInstance().showOptionPopup(CustomizationThreeScreen.this, getResources().getString(R.string.no_customization_for_selected_dress_type), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                                @Override
                                public void onNegativeClick() {

                                }
                                @Override
                                public void onPositiveClick() {
                                    nextScreen(MeasurementOneScreen.class,true);
                                }
                            });
                        }
                    }else {
                        nextScreen(MeasurementOneScreen.class,true);
                    }
                }

//                if (AppConstants.CUSTOMIZATION_CLICK_ARRAY.size() == mCountInt){
//                    if (mCountInt == 0){
//                        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
//                            DialogManager.getInstance().showOptionPopup(CustomizationThreeScreen.this, getResources().getString(R.string.no_customizatin_for_selected_tailor), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
//                                @Override
//                                public void onNegativeClick() {
//
//                                }
//                                @Override
//                                public void onPositiveClick() {
//                                    nextScreen(MeasurementOneScreen.class,true);
//                                }
//                            });
//                        }else {
//                            DialogManager.getInstance().showOptionPopup(CustomizationThreeScreen.this, getResources().getString(R.string.no_customization_for_selected_dress_type), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
//                                @Override
//                                public void onNegativeClick() {
//
//                                }
//                                @Override
//                                public void onPositiveClick() {
//                                    nextScreen(MeasurementOneScreen.class,true);
//                                }
//                            });
//                        }
//                    }else {
//                        nextScreen(MeasurementOneScreen.class,true);
//                    }
//                }else {
//                    Toast.makeText(CustomizationThreeScreen.this,getResources().getString(R.string.please_select_all_the_customization),Toast.LENGTH_SHORT).show();
//                    mCustomizationNextBtn.clearAnimation();
//                    mCustomizationNextBtn.setAnimation(ShakeErrorUtils.shakeError());
//
//                }
                break;
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.customization));
        mRightSideImg.setVisibility(View.INVISIBLE);
        mEmptyListTxt.setText(getResources().getString(R.string.no_result_found));
    }

    public void getHintDialog(){
        alertDismiss(mCustomizationHintDialog);
        mCustomizationHintDialog = getDialog(CustomizationThreeScreen.this, R.layout.pop_up_customization_three_hint);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mCustomizationHintDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mCustomizationHintDialog.findViewById(R.id.customization_three_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);


        }else {
            ViewCompat.setLayoutDirection(mCustomizationHintDialog.findViewById(R.id.customization_three_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        Button  hintSkipBtn,hintNextBtn;
        TextView mHintLapelTxt;
        /*Init view*/

        hintSkipBtn = mCustomizationHintDialog.findViewById(R.id.skip_hint_btn);
        hintNextBtn = mCustomizationHintDialog.findViewById(R.id.next_hint_btn);
        mHintLapelTxt = mCustomizationHintDialog.findViewById(R.id.customization_three_spinner_hint_txt);

        mHintLapelTxt.setText(getResources().getString(R.string.no_result_found));
        mHintLapelTxt.setText(mCustomizationHintLapelTxt);

        hintNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCustomizationHintDialog.dismiss();

            }
        });

        hintSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCustomizationHintDialog.dismiss();
            }
        });

        alertShowing(mCustomizationHintDialog);
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.customization_three_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.customization_three_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
       if ( AppConstants.ORDER_TYPE_ID.equalsIgnoreCase("3")){
           previousScreen(ViewDetailsScreen.class,true);

       }else {
           previousScreen(AddMaterialScreen.class,true);

       }
        AppConstants.CUSTOMIZATION_CLICK_ARRAY = new ArrayList<>();
        AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST = new ArrayList<>();
        AppConstants.CUSTOMIZATION_CLICKED_ARRAY = new ArrayList<>();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }

    @Override
    protected void onResume() {
        super.onResume();
        AppConstants.SKIP_NOW = "";
        if (mCustomizationThreeDialogAdapter != null){
            mCustomizationThreeDialogAdapter.notifyDataSetChanged();
        }
    }
}
