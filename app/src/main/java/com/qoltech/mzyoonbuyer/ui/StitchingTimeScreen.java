package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.ServiceTypeEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.ServiceTypeResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StitchingTimeScreen extends BaseActivity {

    @BindView(R.id.service_type_par_lay)
    LinearLayout mServiceTypeParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.service_type_urgent_bodyimg)
    ImageView mServiceTypeUrgentBodyImg;

    @BindView(R.id.service_type_normal_body_img)
    ImageView mServiceTypeNormalBodyImg;

    @BindView(R.id.service_type_appointement_body_img)
    ImageView mServiceTypeAppointmentBodyImg;

    ArrayList<ServiceTypeEntity> serviceTypeResponses;

    private UserDetailsEntity mUserDetailsEntityRes;

    @BindView(R.id.service_type_urgent_lay)
    RelativeLayout mServiceTypeUrgentLay;

    @BindView(R.id.service_type_normal_lay)
    RelativeLayout mServiceTypeNormalLay;

    @BindView(R.id.new_flow_service_wiz_lay)
    RelativeLayout mNewFlowServiceWizLay;

    @BindView(R.id.service_wiz_lay)
    RelativeLayout mServiceWizLay;

    @BindView(R.id.service_type_appointement_lay)
    RelativeLayout mServiceTypeAppointmentLay;

    @BindView(R.id.service_type_urgent_empty_lay)
    TextView mServiceTypeUrgentEmptyTxt;

    @BindView(R.id.service_type_normal_empty_lay)
    TextView mServiceTypeNormalEmtptyTxt;

    @BindView(R.id.service_type_appointement_empty_lay)
    TextView mServiceTypeAppointmentEmptyTxt;

    @BindView(R.id.service_type_urgent_msg_txt)
    TextView mServiceTypeUrgentMsgTxt;

    @BindView(R.id.service_type_normal_msg_txt)
    TextView mServiceTypeNormalMsgTxt;

    @BindView(R.id.service_type_appointment_msg_txt)
    TextView mServiceTypeAppointmentMsgTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_stitching_time_screen);
        initView();

    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mServiceTypeParLay);

        serviceTypeResponses = new ArrayList<>();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(StitchingTimeScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();
        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
            getNewFlowServiceTypeApicall();
            mServiceTypeUrgentMsgTxt.setVisibility(View.VISIBLE);
            mServiceTypeNormalMsgTxt.setVisibility(View.VISIBLE);
            mServiceTypeAppointmentMsgTxt.setVisibility(View.VISIBLE);
        } else {
            getServiceTypeApicall();
            mServiceTypeUrgentMsgTxt.setVisibility(View.GONE);
            mServiceTypeNormalMsgTxt.setVisibility(View.GONE);
            mServiceTypeAppointmentMsgTxt.setVisibility(View.GONE);
        }

        setHeader();

    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.stitching_time));
        mRightSideImg.setVisibility(View.INVISIBLE);

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
            mServiceWizLay.setVisibility(View.GONE);
            mNewFlowServiceWizLay.setVisibility(View.VISIBLE);
        } else {
            mServiceWizLay.setVisibility(View.VISIBLE);
            mNewFlowServiceWizLay.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.header_left_side_img, R.id.service_type_urgent_lay, R.id.service_type_normal_lay, R.id.service_type_appointement_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.service_type_urgent_lay:
                if (serviceTypeResponses.size() > 0){
                    if (serviceTypeResponses.get(0).isStatus()) {
                        AppConstants.DELIVERY_TYPE_ID = String.valueOf(serviceTypeResponses.get(0).getId());
                        AppConstants.SERVICE_TYPE_NAME_ENG = String.valueOf(serviceTypeResponses.get(0).getId());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                            AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(0).getDeliveryTypeInArabic());
                        } else {
                            AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(0).getDeliveryTypeInEnglish());
                        }
                        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                            nextScreen(DirectOrderPriceScreen.class, true);
                        } else {
                            nextScreen(TailorTypeScreen.class, true);
                        }
//                        nextScreen(TailorTypeScreen.class, true);
                    }
                }
//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        nextScreen(DirectOrderPriceScreen.class, true);
//
//                    } else {
//                        nextScreen(TailorTypeScreen.class, true);
//
//                    }
                break;

            case R.id.service_type_normal_lay:
                if (serviceTypeResponses.size() > 1){
                    if (serviceTypeResponses.get(1).isStatus()) {
                        AppConstants.DELIVERY_TYPE_ID = String.valueOf(serviceTypeResponses.get(1).getId());
                        AppConstants.SERVICE_TYPE_NAME_ENG = String.valueOf(serviceTypeResponses.get(1).getId());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                            AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(1).getDeliveryTypeInArabic());
                        } else {
                            AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(1).getDeliveryTypeInEnglish());
                        }
                        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                            nextScreen(DirectOrderPriceScreen.class, true);
                        } else {
                            nextScreen(TailorTypeScreen.class, true);
                        }
//                        nextScreen(TailorTypeScreen.class, true);
                    }
                }
//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        nextScreen(DirectOrderPriceScreen.class, true);
//
//                    } else {
//                        nextScreen(TailorTypeScreen.class, true);
//
//                    }

                break;
            case R.id.service_type_appointement_lay:
                if (serviceTypeResponses.size() >2){
                    if (serviceTypeResponses.get(2).isStatus()) {
                        AppConstants.DELIVERY_TYPE_ID = String.valueOf(serviceTypeResponses.get(2).getId());
                        AppConstants.SERVICE_TYPE_NAME_ENG = String.valueOf(serviceTypeResponses.get(2).getId());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                            AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(2).getDeliveryTypeInArabic());
                        } else {
                            AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(2).getDeliveryTypeInEnglish());
                        }
                        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                            nextScreen(DirectOrderPriceScreen.class, true);
                        } else {
                            nextScreen(TailorTypeScreen.class, true);
                        }
//                        nextScreen(TailorTypeScreen.class, true);
                    }
                }
//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        nextScreen(DirectOrderPriceScreen.class, true);
//
//                    } else {
//                        nextScreen(TailorTypeScreen.class, true);
//
//                    }
                break;
        }
    }

    public void getNewFlowServiceTypeApicall() {
        if (NetworkUtil.isNetworkAvailable(StitchingTimeScreen.this)) {
            APIRequestHandler.getInstance().getNewFlowServiceTypeApiCall(String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId()),AppConstants.SUB_DRESS_TYPE_ID, StitchingTimeScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(StitchingTimeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getServiceTypeApicall();
                }
            });
        }
    }

    public void getServiceTypeApicall() {
        if (NetworkUtil.isNetworkAvailable(StitchingTimeScreen.this)) {
            APIRequestHandler.getInstance().getServiceTypeApiCall(StitchingTimeScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(StitchingTimeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getServiceTypeApicall();
                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof ServiceTypeResponse) {
            ServiceTypeResponse mResponse = (ServiceTypeResponse) resObj;

            serviceTypeResponses = mResponse.getResult();

            if (!AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                for (int i = 0; i < serviceTypeResponses.size(); i++) {
                    serviceTypeResponses.get(i).setStatus(true);
                }
            }

            for (int i = 0; i < serviceTypeResponses.size(); i++) {
                if (i == 0) {

                    try {
                        Glide.with(StitchingTimeScreen.this)
                                .load(AppConstants.IMAGE_BASE_URL + "Images/ServiceType/" + serviceTypeResponses.get(0).getDeliveryImage())
                                .apply(new RequestOptions().placeholder(R.drawable.empty_img).error(R.drawable.empty_img))
                                .into(mServiceTypeUrgentBodyImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                    mServiceTypeUrgentMsgTxt.setText(serviceTypeResponses.get(0).getTailorMessage().replace("^",serviceTypeResponses.get(0).getDeliveryTime()));
                } else if (i == 1) {

                    try {
                        Glide.with(StitchingTimeScreen.this)
                                .load(AppConstants.IMAGE_BASE_URL + "Images/ServiceType/" + serviceTypeResponses.get(1).getDeliveryImage())
                                .apply(new RequestOptions().placeholder(R.drawable.empty_img).error(R.drawable.empty_img))
                                .into(mServiceTypeNormalBodyImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                    mServiceTypeNormalMsgTxt.setText(serviceTypeResponses.get(1).getTailorMessage().replace("^",serviceTypeResponses.get(1).getDeliveryTime()));
                } else if (i == 2) {

                    try {
                        Glide.with(StitchingTimeScreen.this)
                                .load(AppConstants.IMAGE_BASE_URL + "Images/ServiceType/" + serviceTypeResponses.get(2).getDeliveryImage())
                                .apply(new RequestOptions().placeholder(R.drawable.empty_img).error(R.drawable.empty_img))
                                .into(mServiceTypeAppointmentBodyImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                    mServiceTypeAppointmentMsgTxt.setText(serviceTypeResponses.get(2).getTailorMessage().replace("^",serviceTypeResponses.get(2).getDeliveryTime()));

                }
            }

            mServiceTypeUrgentLay.setVisibility(View.VISIBLE);
            mServiceTypeNormalLay.setVisibility(View.VISIBLE);
            mServiceTypeAppointmentLay.setVisibility(View.VISIBLE);
            mServiceTypeUrgentBodyImg.setVisibility(View.VISIBLE);
            mServiceTypeNormalBodyImg.setVisibility(View.VISIBLE);
            mServiceTypeAppointmentBodyImg.setVisibility(View.VISIBLE);

            if (AppConstants.DIRECT_ORDER_TYPE.equalsIgnoreCase("3")&&AppConstants.MEASUREMENT_TYPE.equalsIgnoreCase("1")) {

            } else {
                for (int i = 0; i < serviceTypeResponses.size(); i++) {
                    if (String.valueOf(serviceTypeResponses.get(i).getId()).equalsIgnoreCase("3")) {
                        serviceTypeResponses.get(i).setStatus(false);
                    }
                }

                mServiceTypeAppointmentEmptyTxt.setText(getResources().getString(R.string.service_not_available_for_appointment));
            }



            for (int i = 0; i < serviceTypeResponses.size(); i++) {
                if (serviceTypeResponses.get(i).getId() == 1){
                    if (serviceTypeResponses.get(i).getExpress() == 0) {
                        serviceTypeResponses.get(i).setStatus(false);
                    }

                }

                if (!serviceTypeResponses.get(i).isStatus()) {
                    if (i == 0) {
                        mServiceTypeUrgentEmptyTxt.setVisibility(View.VISIBLE);

                    } else if (i == 1) {
                        mServiceTypeNormalEmtptyTxt.setVisibility(View.VISIBLE);

                    } else if (i == 2) {
                        mServiceTypeAppointmentEmptyTxt.setVisibility(View.VISIBLE);

                    }
                } else {
                    if (i == 0) {
                        mServiceTypeUrgentEmptyTxt.setVisibility(View.GONE);
                    } else if (i == 1) {
                        mServiceTypeNormalEmtptyTxt.setVisibility(View.GONE);
                    } else if (i == 2) {
                        mServiceTypeAppointmentEmptyTxt.setVisibility(View.GONE);
                    }
                }


            }
        }
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.service_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.service_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
