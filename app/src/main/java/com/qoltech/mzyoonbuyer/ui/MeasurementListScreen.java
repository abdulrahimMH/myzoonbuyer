package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.AddMeasurementPartsListAdapter;
import com.qoltech.mzyoonbuyer.adapter.MeasurementPartsListAdapter;
import com.qoltech.mzyoonbuyer.entity.AddMeasurementPartsEntity;
import com.qoltech.mzyoonbuyer.entity.MeasurementListGetMeasurementPartsEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.AddMeasurementPartsListResponse;
import com.qoltech.mzyoonbuyer.modal.MeasurementListGetMeasurementPartsResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MeasurementListScreen extends BaseActivity {

    @BindView(R.id.measurement_list_par_lay)
    LinearLayout mOrderDetailsPayLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.measurement_list_recycler_view)
    RecyclerView mMeasurementListRecyclerView;

    @BindView(R.id.measurement_list_empty_txt)
    TextView mMeasurementListEmptyTxt;

    @BindView(R.id.measurement_list_img)
    de.hdodenhof.circleimageview.CircleImageView mMeasurementListImg;

    @BindView(R.id.measurement_list_measurement_name_txt)
    TextView mMeasurementNameTxt;

    @BindView(R.id.measurement_list_measurement_date_txt)
    TextView mMeasurementDateTxt;

    @BindView(R.id.measurement_list_dress_type_txt)
    TextView mMeasurementDressTypeTxt;

    @BindView(R.id.measurement_list_measurement_by_txt)
    TextView mMeasurmentByTxt;

    @BindView(R.id.measurement_list_details_par_lay)
    CardView mMeasurementListDetailsParLay;

    @BindView(R.id.measurement_list_continue)
    RelativeLayout mMeasurementListContinue;

    MeasurementPartsListAdapter mMeasurementPartsAdapter;

    AddMeasurementPartsListAdapter mAddMeasurementPartsListAdapter;

    private UserDetailsEntity mUserDetailsEntityRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_measurement_list);
        initView();

    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mOrderDetailsPayLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(MeasurementListScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        setHeader();

        if (AppConstants.ORDER_DETAILS_MEASUREMENT.equalsIgnoreCase("ORDER")){
            getMeasurementListPartsApiCall();
        } else if (AppConstants.ORDER_DETAILS_MEASUREMENT.equalsIgnoreCase("ADD_MEASUREMENT")){
            getAddMeasurementListPartsApiCall();
        }
        else {
            getMeasurementListPartsApiCall();
        }

        if (AppConstants.NEW_ORDER.equalsIgnoreCase("NEW_ORDER")){
            mMeasurementListContinue.setVisibility(View.VISIBLE);
        }else {
            mMeasurementListContinue.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.header_left_side_img,R.id.measurement_list_continue})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.measurement_list_continue:
                nextScreen(AddReferenceScreen.class,true);
                break;
        }
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.measurement_details));
        mRightSideImg.setVisibility(View.INVISIBLE);

    }


    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof MeasurementListGetMeasurementPartsResponse){
            MeasurementListGetMeasurementPartsResponse mResponse = (MeasurementListGetMeasurementPartsResponse)resObj;
            setAdapter(mResponse.getResult());
        }
        if (resObj instanceof AddMeasurementPartsListResponse){
            AddMeasurementPartsListResponse mResponse = (AddMeasurementPartsListResponse)resObj;
            setAddMeasurementPartsAdapter(mResponse.getResult());
        }
    }

    public void getMeasurementListPartsApiCall(){
        if (NetworkUtil.isNetworkAvailable(MeasurementListScreen.this)){
            APIRequestHandler.getInstance().getMeasurementListGetMeasurementPartsApiCall(AppConstants.REQUEST_LIST_ID, MeasurementListScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(MeasurementListScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getMeasurementListPartsApiCall();
                }
            });
        }
    }

    public void getAddMeasurementListPartsApiCall(){
        if (NetworkUtil.isNetworkAvailable(MeasurementListScreen.this)){
            APIRequestHandler.getInstance().getAddMeasurementPartsList(AppConstants.REQUEST_LIST_ID, MeasurementListScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(MeasurementListScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAddMeasurementListPartsApiCall();
                }
            });
        }
    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<MeasurementListGetMeasurementPartsEntity> mMeasurementPartsList) {

        mMeasurementListRecyclerView.setVisibility(mMeasurementPartsList.size()>0 ? View.VISIBLE : View.GONE);
        mMeasurementListEmptyTxt.setVisibility(mMeasurementPartsList.size() > 0 ? View.GONE : View.VISIBLE);
        mMeasurementListDetailsParLay.setVisibility(mMeasurementPartsList.size() >0 ? View.VISIBLE : View.GONE);

        if (mMeasurementPartsList.size() >0){
            try {
                Glide.with(getApplicationContext())
                        .load(AppConstants.IMAGE_BASE_URL+"Images/DressSubType/"+mMeasurementPartsList.get(0).getDressSubTypeImage())
                        .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                        .apply(RequestOptions.skipMemoryCacheOf(true))
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                        .into(mMeasurementListImg);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
            mMeasurementNameTxt.setText(mMeasurementPartsList.get(0).getName());
            mMeasurmentByTxt.setText(mMeasurementPartsList.get(0).getName());
            String[] date = mMeasurementPartsList.get(0).getCreatedOn().split("T");
            mMeasurementDateTxt.setText(date[0]);
            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                mMeasurementDressTypeTxt.setText(mMeasurementPartsList.get(0).getGender() +" - " +mMeasurementPartsList.get(0).getNameInArabic());

            }else {
                mMeasurementDressTypeTxt.setText(mMeasurementPartsList.get(0).getNameInEnglish() + " - "+mMeasurementPartsList.get(0).getGender());

            }
        }
        mMeasurementPartsAdapter = new MeasurementPartsListAdapter(this,mMeasurementPartsList);
        mMeasurementListRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        mMeasurementListRecyclerView.setAdapter(mMeasurementPartsAdapter);

    }

    /*Set Adapter for the Recycler view*/
    public void setAddMeasurementPartsAdapter(ArrayList<AddMeasurementPartsEntity> mMeasurementPartsList) {

        mMeasurementListDetailsParLay.setVisibility(mMeasurementPartsList.size() >0 ? View.VISIBLE : View.GONE);

        if (mMeasurementPartsList.size() >0){
            try {
                Glide.with(getApplicationContext())
                        .load(AppConstants.IMAGE_BASE_URL+"Images/DressSubType/"+mMeasurementPartsList.get(0).getDressSubTypeImage())
                        .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                        .apply(RequestOptions.skipMemoryCacheOf(true))
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                        .into(mMeasurementListImg);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
            mMeasurementNameTxt.setText(mMeasurementPartsList.get(0).getName());
            mMeasurementDateTxt.setText(mMeasurementPartsList.get(0).getCreatedOn());
            mMeasurmentByTxt.setText(mMeasurementPartsList.get(0).getMeasurementBy());
            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                mMeasurementDressTypeTxt.setText(mMeasurementPartsList.get(0).getGender() +" - " +mMeasurementPartsList.get(0).getNameInArabic());

            }else {
                mMeasurementDressTypeTxt.setText(mMeasurementPartsList.get(0).getNameInEnglish() + " - "+mMeasurementPartsList.get(0).getGender());

            }
        }

        mAddMeasurementPartsListAdapter = new AddMeasurementPartsListAdapter(this,mMeasurementPartsList);
        mMeasurementListRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        mMeasurementListRecyclerView.setAdapter(mAddMeasurementPartsListAdapter);

    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_list_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_list_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }

}
