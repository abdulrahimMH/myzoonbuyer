package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetRedeemResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.zhouyou.view.seekbar.SignSeekBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConvertPointsToCashScreen extends BaseActivity {
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.convert_points_to_case_par_lay)
    RelativeLayout mConvertPointsToCaseParLay;

    @BindView(R.id.convert_points_total_points_txt)
    TextView mConvertPointsTotalPointsTxt;

    @BindView(R.id.convert_points_to_cash_seek_bar)
    com.zhouyou.view.seekbar.SignSeekBar mConvertPointsSeekBar;

    @BindView(R.id.convert_to_earn_used_points_txt)
    TextView mConvertEarnUsedPointsTxt;

    @BindView(R.id.convert_earn_point_left_txt)
    TextView mConvertEarnPointsLeftTxt;

    @BindView(R.id.convert_earn_points_converted_money_txt)
    TextView mConvertEarnPointsConvertedMoneyTxt;

    private UserDetailsEntity mUserDetailsEntityRes;

    int mCurrentPointsInt, mSinglePointFloat;
    Float mSinglePointsFloat;

    String REDEEM_POINT_TO_CASH = "0",REDEEM_POINT_TO_POINTS = "0", REDEEM_SELECTED_REDEEM = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_convert_points_to_cash_screen);

        initView();
    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mConvertPointsToCaseParLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(ConvertPointsToCashScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        getRedeemApiCall(mUserDetailsEntityRes.getUSER_ID());

       if (!AppConstants.REDEEM_SELECTED_REDEEM.equalsIgnoreCase("")){
           mConvertPointsSeekBar.setProgress(Float.parseFloat(AppConstants.REDEEM_SELECTED_REDEEM));
           REDEEM_POINT_TO_CASH = AppConstants.REDEEM_POINT_TO_CASH;
           REDEEM_POINT_TO_POINTS = AppConstants.REDEEM_POINT_TO_POINTS;
           REDEEM_SELECTED_REDEEM = AppConstants.REDEEM_SELECTED_REDEEM;

       }

        mConvertPointsSeekBar.setOnProgressChangedListener(new SignSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(SignSeekBar signSeekBar, int progress, float progressFloat, boolean fromUser) {
                mConvertEarnUsedPointsTxt.setText(String.valueOf(progress  - progress % mSinglePointFloat));

                mConvertEarnPointsLeftTxt.setText(String.valueOf(mCurrentPointsInt - (progress  - progress % mSinglePointFloat)));

                mConvertEarnPointsConvertedMoneyTxt.setText(String.valueOf(progress / mSinglePointFloat));

                REDEEM_POINT_TO_CASH = String.valueOf(progress / mSinglePointFloat);

                REDEEM_POINT_TO_POINTS = String.valueOf(progress  - progress % mSinglePointFloat);

                REDEEM_SELECTED_REDEEM = String.valueOf(progress  - progress % mSinglePointFloat);

            }
            @Override
            public void getProgressOnActionUp(SignSeekBar signSeekBar, int progress, float progressFloat) {

            }
            @Override
            public void getProgressOnFinally(SignSeekBar signSeekBar, int progress, float progressFloat, boolean fromUser) {

            }
        });

    }

    @OnClick({R.id.header_left_side_img,R.id.convert_points_add_to_payment_txt})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.convert_points_add_to_payment_txt:
                AppConstants.REDEEM_POINT_TO_CASH = REDEEM_POINT_TO_CASH;
                AppConstants.REDEEM_POINT_TO_POINTS = REDEEM_POINT_TO_POINTS;
                AppConstants.REDEEM_SELECTED_REDEEM = REDEEM_SELECTED_REDEEM;
                onBackPressed();
                break;
        }
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.redeem));
        mRightSideImg.setVisibility(View.INVISIBLE);

        mConvertEarnPointsConvertedMoneyTxt.setText("0 ");
        mConvertEarnUsedPointsTxt.setText("0");
        mConvertEarnPointsLeftTxt.setText("0");

    }

    public void getRedeemApiCall(String UserId){
        if (NetworkUtil.isNetworkAvailable(ConvertPointsToCashScreen.this)){
            APIRequestHandler.getInstance().getRedeemApiCall(UserId,ConvertPointsToCashScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ConvertPointsToCashScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getRedeemApiCall(UserId);
                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetRedeemResponse){
            GetRedeemResponse mResponse = (GetRedeemResponse)resObj;

            mConvertPointsTotalPointsTxt.setText(String.valueOf(mResponse.getResult().getCurrentPoints())+"\n"+getResources().getString(R.string.points));
//            mCurrentPointsInt =(int) Math.round(Float.parseFloat(String.valueOf(mResponse.getResult().getCurrentPoints())));

            mSinglePointsFloat = Float.parseFloat(String.valueOf(mResponse.getResult().getSingleAmount()));

            mSinglePointFloat = (int) Math.round(mResponse.getResult().getSinglePoint());

            if (mResponse.getResult().getCurrentPoints() > 0.0){

                int amount = (int) Math.round(Double.parseDouble(AppConstants.TRANSACTION_AMOUNT));
                int res = (amount * mResponse.getResult().getDefaultPointAmount().get(0).getPointPercentage()) / 100;
                int pointsCovert = res * (int) Math.round(mResponse.getResult().getDefaultPointAmount().get(0).getAmount()) *(int) Math.round(mResponse.getResult().getDefaultPointAmount().get(0).getPoints());

                if (mResponse.getResult().getCurrentPoints() > pointsCovert){
                    mConvertPointsSeekBar.getConfigBuilder().showSign().max(pointsCovert).build();
                    mCurrentPointsInt = pointsCovert;

                }else {
                    mConvertPointsSeekBar.getConfigBuilder().showSign().max(Float.parseFloat(String.valueOf(mResponse.getResult().getCurrentPoints()))).build();
                    mCurrentPointsInt = (int) mResponse.getResult().getCurrentPoints();

                }
                mConvertEarnPointsLeftTxt.setText(String.valueOf(mCurrentPointsInt - Integer.parseInt(REDEEM_POINT_TO_POINTS)));

            }else {
                mConvertPointsSeekBar.setEnabled(false);

            }
        }
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.convert_points_to_case_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.convert_points_to_case_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }
}
