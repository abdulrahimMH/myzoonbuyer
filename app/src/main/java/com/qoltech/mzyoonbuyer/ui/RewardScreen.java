package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.RelativeLayout;

import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.fragment.RewardFragment;
import com.qoltech.mzyoonbuyer.main.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RewardScreen extends BaseActivity {

    @BindView(R.id.rewards_par_lay)
    RelativeLayout mRewardsParLay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_reward_screen);

        initView();
    }
    public void initView(){
        ButterKnife.bind(this);

        setupUI(mRewardsParLay);

        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment addFragment = new RewardFragment();
        fragmentTransaction.replace(R.id.rewards_frame_lay,addFragment);
        fragmentTransaction.commit();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
