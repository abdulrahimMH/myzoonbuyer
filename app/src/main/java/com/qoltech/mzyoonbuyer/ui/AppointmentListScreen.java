package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.AppointmentListAdapter;
import com.qoltech.mzyoonbuyer.entity.AppointmentListEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.AppointmentListResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AppointmentListScreen extends BaseActivity {

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.appointment_list_recycler_view)
    RecyclerView mAppointmentListRecyclerView;

    private AppointmentListAdapter mAppointmentAdapter;

    @BindView(R.id.appointment_list_par_lay)
    LinearLayout mAppointmentListParLay;

    @BindView(R.id.appoinment_list_par_lay)
    RelativeLayout mAppointmentRecListParLay;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    @BindView(R.id.appoinment_list_scroll_view_img)
    ImageView mAppointmentListScrollViewImg;

    private UserDetailsEntity mUserDetailsEntityRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_appointment_list_screen);

        initView();

    }
    public void initView(){

        ButterKnife.bind(this);

        setupUI(mAppointmentListParLay);

        setHeader();


        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(AppointmentListScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        getApponitmentListApiCall();

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof AppointmentListResponse){
            AppointmentListResponse mResponse = (AppointmentListResponse)resObj;
            setAdapter(mResponse.getResult());
        }
    }

    public void getApponitmentListApiCall(){
        if (NetworkUtil.isNetworkAvailable(AppointmentListScreen.this)){
            APIRequestHandler.getInstance().getAppointmentListApi(mUserDetailsEntityRes.getUSER_ID(),AppointmentListScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentListScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getApponitmentListApiCall();
                }
            });
        }
    }
    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.appointment_list));
        mRightSideImg.setVisibility(View.INVISIBLE);
        mEmptyListTxt.setText(getResources().getString(R.string.no_result_for_appointment_list));

    }

    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }
    
    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<AppointmentListEntity> appointmentListEntities) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mAppointmentRecListParLay.setVisibility(appointmentListEntities.size()>0 ? View.VISIBLE : View.GONE);
            mEmptyListLay.setVisibility(appointmentListEntities.size() > 0 ? View.GONE : View.VISIBLE);

            mAppointmentAdapter = new AppointmentListAdapter(this,appointmentListEntities);
            mAppointmentListRecyclerView.setLayoutManager(layoutManager);
            mAppointmentListRecyclerView.setAdapter(mAppointmentAdapter);

        mAppointmentListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = layoutManager.findLastVisibleItemPosition();

                if (lastPosition == appointmentListEntities.size()-1){
                    mAppointmentListScrollViewImg.setVisibility(View.GONE);
                }else {
                    mAppointmentListScrollViewImg.setVisibility(View.VISIBLE);
                }
            }
        });

    }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.appointment_list_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.appointment_list_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
