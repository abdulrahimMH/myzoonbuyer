package com.qoltech.mzyoonbuyer.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.GetAppointmentTimeSlotAdapter;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.OrderApprovalDeliveryResponse;
import com.qoltech.mzyoonbuyer.modal.OrderApprovalPriceResponse;
import com.qoltech.mzyoonbuyer.modal.UpdateQuantityResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class OrderApprovalScreen extends BaseActivity {

    @BindView(R.id.order_approval_par_lay)
    LinearLayout mOrderApprovalPayLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.order_appproval_img)
            ImageView mOrderApprovalImg;

    @BindView(R.id.order_approval_suit_name)
            TextView mOrderApprovalSuitName;

    @BindView(R.id.price_details_total_price)
            TextView mOrderApprovalTotalPrice;

    @BindView(R.id.order_approval_delivery_material_txt)
            TextView mOrderApprovalDeliveryAppointmentTxt;

    @BindView(R.id.order_approval_delivery_type_txt)
            TextView mOrderApprovalDeliveryTypeTxt;

    @BindView(R.id.order_approval_delivery_time_txt)
            TextView mOrderApprovalDeliveryTimeTxt;

    @BindView(R.id.order_approval_delivery_date_txt)
            TextView mOrderApprovalDeliveryDateTxt;

    @BindView(R.id.order_approval_qty_txt)
    public TextView mOrderApprovalQtyTxt;

    @BindView(R.id.price_details_measurement_price)
    TextView mPriceDetailsMeasurementPriceTxt;

    @BindView(R.id.price_details_stiching_price)
    TextView mPriceDetailsStichingPriceTxt;

    @BindView(R.id.price_details_material_price)
    TextView mPriceDetailsMaterialPriceTxt;

    @BindView(R.id.price_details_urgent_price)
    TextView mPriceDetailsUrgentPriceTxt;

    @BindView(R.id.price_details_delivery_price)
    TextView mPriceDetailsDeliveryPriceTxt;

    @BindView(R.id.price_details_material_courier_price_lay)
    RelativeLayout mPriceDetailsMaterialCourierPriceLay;

    @BindView(R.id.price_details_delivery_price_lay)
    RelativeLayout mPriceDetailsDeliveryPriceLay;

    @BindView(R.id.price_details_urgent_price_lay)
    RelativeLayout mPriceDetailsUrgentPriceLay;

    @BindView(R.id.price_details_measurement_price_lay)
    RelativeLayout mPriceDetailsMeasurementPriceLay;

    @BindView(R.id.price_details_order_details_txt)
    TextView mPriceDetailsOrderDetailsTxt;

    @BindView(R.id.order_approval_delivery_measurement_txt)
    TextView mOrderApprovalDeliveryMeasurementTxt;

    @BindView(R.id.order_approval_delivery_msg_txt)
    TextView mOrderApprovalDeliveryMsgTxt;

    private ArrayList<String> mOrderQtyList;

    Dialog mOrderQtyDialog;

    private UserDetailsEntity mUserDetailsEntityRes;

    private String mOrderTypeBoolStr ="",mMeasurementTypeBoolStr ="",mAddedAmt = "",mTotalPriceStr = "0",mAmountStr = "";

    private double mMeasurementInt = 0.0 , mUrgentInt = 0.0 ,mDeliveryInt = 0.0 ,mMaterialInt = 0.0 ;

    private GetAppointmentTimeSlotAdapter mOrderQtyAdater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_order_approval_screen);
        initView();
    }
    public void initView(){

        ButterKnife.bind(this);

        setupUI(mOrderApprovalPayLay);
        setHeader();
        OrderApprovalPriceApiCall();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(OrderApprovalScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        mOrderApprovalQtyTxt.setText("1");

        addTimeSlotList();

        getLanguage();

        mOrderApprovalQtyTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equalsIgnoreCase("")){
                    addAmount();
                }
            }
        });
    }

    public void addTimeSlotList(){
        mOrderQtyList = new ArrayList<>();
        mOrderQtyList.add("1");
        mOrderQtyList.add("2");
        mOrderQtyList.add("3");
        mOrderQtyList.add("4");
        mOrderQtyList.add("5");

    }

    @OnClick({R.id.header_left_side_img,R.id.order_approval_proceed_to_pay_txt,R.id.order_approval_qty_txt_lay,R.id.order_approval_tailor_details_txt,R.id.order_approval_tailor_detail_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.order_approval_proceed_to_pay_txt:
                if (mOrderApprovalQtyTxt.getText().toString().trim().equalsIgnoreCase("")||mOrderApprovalQtyTxt.getText().toString().trim().equalsIgnoreCase("0") ){
                    mOrderApprovalQtyTxt.clearAnimation();
                    mOrderApprovalQtyTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mOrderApprovalQtyTxt.setError(getResources().getString(R.string.please_give_qty));
                    Toast.makeText(getContext(),getResources().getString(R.string.please_give_qty),Toast.LENGTH_SHORT).show();
                }
                else if (mTotalPriceStr.equalsIgnoreCase("0")){
                    DialogManager.getInstance().showAlertPopup(OrderApprovalScreen.this, getResources().getString(R.string.payment_transaction_amount_zero), new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {

                        }
                    });

                } else if (mOrderTypeBoolStr.equalsIgnoreCase("false") && mMeasurementTypeBoolStr.equalsIgnoreCase("false")&&AppConstants.SERVICE_TYPE_NAME_ENG .equalsIgnoreCase("3")) {

                        DialogManager.getInstance().showOptionPopup(OrderApprovalScreen.this, getResources().getString(R.string.book_an_appointment_before_pay), getResources().getString(R.string.proceed), getResources().getString(R.string.cancel), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                            @Override
                            public void onNegativeClick() {

                            }

                            @Override
                            public void onPositiveClick() {
                                AppConstants.DIRECT_ORDER = "QUOTATION";
                                AppConstants.CHECK_OUT_ORDER = "Stitching";
                                AppConstants.STORE_ID = "0";
//                                AppConstants.SUB_TOTAL_CASH = "0";
//                                AppConstants.STICTHING_AMT = "0";
//                                AppConstants.TRANSACTION_AMOUNT = "0";
                                AppConstants.SUB_TOTAL_CASH = mOrderApprovalTotalPrice.getText().toString().replace("AED","").trim();
                                AppConstants.STICTHING_AMT = mOrderApprovalTotalPrice.getText().toString().replace("AED","").trim();
                                AppConstants.TRANSACTION_AMOUNT = mOrderApprovalTotalPrice.getText().toString().replace("AED","").trim();
                                        updateOrderApprovalQty();
                                        nextScreen(ScheduletDetailScreen.class,true);

                            }
                        });
                    }else if (mOrderTypeBoolStr.equalsIgnoreCase("false") && mMeasurementTypeBoolStr.equalsIgnoreCase("false")){
                    AppConstants.DIRECT_ORDER = "QUOTATION";
                    AppConstants.CHECK_OUT_ORDER = "Stitching";
                    AppConstants.STORE_ID = "0";
//                    AppConstants.SUB_TOTAL_CASH = "0";
//                    AppConstants.STICTHING_AMT = "0";
//                    AppConstants.TRANSACTION_AMOUNT = "0";
                    AppConstants.SUB_TOTAL_CASH = mOrderApprovalTotalPrice.getText().toString().replace("AED","").trim();
                    AppConstants.STICTHING_AMT = mOrderApprovalTotalPrice.getText().toString().replace("AED","").trim();
                    AppConstants.TRANSACTION_AMOUNT = mOrderApprovalTotalPrice.getText().toString().replace("AED","").trim();
                    updateOrderApprovalQty();
                    nextScreen(CheckoutScreen.class,true);

                }else {

                    DialogManager.getInstance().showOptionPopup(OrderApprovalScreen.this, getResources().getString(R.string.book_an_appointment_before_pay), getResources().getString(R.string.proceed), getResources().getString(R.string.cancel), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                        @Override
                        public void onNegativeClick() {

                        }

                        @Override
                        public void onPositiveClick() {
                            AppConstants.DIRECT_ORDER = "QUOTATION";
                            AppConstants.CHECK_OUT_ORDER = "Stitching";
                            AppConstants.STORE_ID = "0";
//                            AppConstants.SUB_TOTAL_CASH = "0";
//                            AppConstants.STICTHING_AMT = "0";
//                            AppConstants.TRANSACTION_AMOUNT = "0";
                            AppConstants.SUB_TOTAL_CASH = mOrderApprovalTotalPrice.getText().toString().replace("AED","").trim();
                            AppConstants.STICTHING_AMT = mOrderApprovalTotalPrice.getText().toString().replace("AED","").trim();
                            AppConstants.TRANSACTION_AMOUNT = mOrderApprovalTotalPrice.getText().toString().replace("AED","").trim();
                            updateOrderApprovalQty();
                            nextScreen(AppointmentDetails.class,true);
                        }
                    });


                }

                break;
            case R.id.order_approval_tailor_details_txt:
               nextScreen(ShopDetailsScreen.class,true);
                break;
            case R.id.order_approval_tailor_detail_img:
                nextScreen(ShopDetailsScreen.class,true);
                break;
        }
    }

    public void addAmount(){
        int qty =Integer.parseInt(mOrderApprovalQtyTxt.getText().toString().trim());
        double amt = Double.parseDouble(mAmountStr);
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
        DecimalFormat formatter = (DecimalFormat)nf;
        formatter.applyPattern("######.00");

        double total = amt*qty +mMeasurementInt+mUrgentInt*qty+mDeliveryInt+mMaterialInt;

        mPriceDetailsStichingPriceTxt.setText(String.valueOf(formatter.format(amt*qty)));
        mPriceDetailsUrgentPriceTxt.setText(String.valueOf(formatter.format(mUrgentInt*qty)));
        mOrderApprovalTotalPrice.setText(String.valueOf(formatter.format(total)));

        mAddedAmt = String.valueOf(total);
    }

    public void OrderApprovalPriceApiCall(){
        if (NetworkUtil.isNetworkAvailable(OrderApprovalScreen.this)){
            APIRequestHandler.getInstance().orderApprovalPriceApiCall(AppConstants.QUOTATION_LIST_ID,OrderApprovalScreen.this);
            APIRequestHandler.getInstance().orderApprovalDeliveryApiCall(AppConstants.QUOTATION_LIST_ID,OrderApprovalScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderApprovalScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    OrderApprovalPriceApiCall();
                }
            });
        }
    }

    public void updateOrderApprovalQty(){
        if (NetworkUtil.isNetworkAvailable(OrderApprovalScreen.this)){
            APIRequestHandler.getInstance().updateQtyOrderApprovalApiCall(AppConstants.QUOTATION_ORDER_ID,mOrderApprovalQtyTxt.getText().toString().trim(),OrderApprovalScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderApprovalScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    updateOrderApprovalQty();
                }
            });
        }
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.order_approval));
        mRightSideImg.setVisibility(View.INVISIBLE);
        mPriceDetailsOrderDetailsTxt.setText(getResources().getString(R.string.order_total) + " (" + getResources().getString(R.string.aed) +")");
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof OrderApprovalPriceResponse){
            OrderApprovalPriceResponse mReponse = (OrderApprovalPriceResponse)resObj;

            if (mReponse.getResult().getDressSubType().size() > 0){

                AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(mReponse.getResult().getDressSubType().get(0).getDressSubTypeId());
            try {
                Glide.with(OrderApprovalScreen.this)
                        .load(AppConstants.IMAGE_BASE_URL+"Images/DressSubType/"+mReponse.getResult().getDressSubType().get(0).getImage())
                        .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                        .apply(RequestOptions.skipMemoryCacheOf(true))
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                        .into(mOrderApprovalImg);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                mOrderApprovalSuitName.setText(mReponse.getResult().getDressSubType().get(0).getNameInArabic());

            }else{
                mOrderApprovalSuitName.setText(mReponse.getResult().getDressSubType().get(0).getNameInEnglish());
            }
            }

            mTotalPriceStr = String.valueOf(mReponse.getResult().getStichingAndMaterialCharge()+mReponse.getResult().getMeasurementCharges()+mReponse.getResult().getUrgentStichingCharges()+mReponse.getResult().getDeliveryCharge()+mReponse.getResult().getMaterialDeliveryCharges());
            mOrderApprovalTotalPrice.setText(String.valueOf(mReponse.getResult().getStichingAndMaterialCharge()+mReponse.getResult().getMeasurementCharges()+mReponse.getResult().getUrgentStichingCharges()+mReponse.getResult().getDeliveryCharge()+mReponse.getResult().getMaterialDeliveryCharges() + " "+getResources().getString(R.string.aed)));
                        mAmountStr = String.valueOf(mReponse.getResult().getStichingAndMaterialCharge());
                        mPriceDetailsStichingPriceTxt.setText(String.valueOf(mReponse.getResult().getStichingAndMaterialCharge()) );
                        mPriceDetailsMeasurementPriceTxt.setText(String.valueOf(mReponse.getResult().getMeasurementCharges()));
                        mPriceDetailsUrgentPriceTxt.setText(String.valueOf(mReponse.getResult().getUrgentStichingCharges()) );
                        mPriceDetailsDeliveryPriceTxt.setText(String.valueOf(mReponse.getResult().getDeliveryCharge()) );
                        mPriceDetailsMaterialPriceTxt.setText(String.valueOf(mReponse.getResult().getMaterialDeliveryCharges()) );

                        if (mReponse.getResult().getMeasurementCharges() == 0){
                            mPriceDetailsMeasurementPriceLay.setVisibility(View.GONE);
                        }
                        if (mReponse.getResult().getUrgentStichingCharges() == 0){
                            mPriceDetailsUrgentPriceLay.setVisibility(View.GONE);
                        }
                        if(mReponse.getResult().getDeliveryCharge() == 0){
                            mPriceDetailsDeliveryPriceLay.setVisibility(View.GONE);
                        }
                        if (mReponse.getResult().getMaterialDeliveryCharges() == 0){
                            mPriceDetailsMaterialCourierPriceLay.setVisibility(View.GONE);
                        }
                    mMeasurementInt = mReponse.getResult().getMeasurementCharges();
                    mUrgentInt = mReponse.getResult().getUrgentStichingCharges();
                    mDeliveryInt = mReponse.getResult().getDeliveryCharge();
                    mMaterialInt = mReponse.getResult().getMaterialDeliveryCharges();

        }
        if (resObj instanceof OrderApprovalDeliveryResponse){
            OrderApprovalDeliveryResponse mResponse = (OrderApprovalDeliveryResponse)resObj;

            if (mResponse.getResult().getDressSubType().size() > 0){
                AppConstants.SERVICE_TYPE_NAME_ENG = String.valueOf(mResponse.getResult().getDressSubType().get(0).getDeliveryTypeId());

                if (String.valueOf(mResponse.getResult().getDressSubType().get(0).getOrderTypeId()).equalsIgnoreCase("2")){
                    mOrderTypeBoolStr = "true";

                }
                else if (String.valueOf(mResponse.getResult().getDressSubType().get(0).getOrderTypeId()).equalsIgnoreCase("3")){
                    mOrderTypeBoolStr = "false";

                }
                else if (String.valueOf(mResponse.getResult().getDressSubType().get(0).getOrderTypeId()).equalsIgnoreCase("1")){
                    mOrderTypeBoolStr = "false";

                }
                if (String.valueOf(mResponse.getResult().getDressSubType().get(0).getMeasurementTypeId()).equalsIgnoreCase("3")){
                    mMeasurementTypeBoolStr = "true";

                }
                else if (String.valueOf(mResponse.getResult().getDressSubType().get(0).getMeasurementTypeId()).equalsIgnoreCase("1")){
                    mMeasurementTypeBoolStr = "false";

                }
                else if (String.valueOf(mResponse.getResult().getDressSubType().get(0).getMeasurementTypeId()).equalsIgnoreCase("2")) {
                    mMeasurementTypeBoolStr = "false";
                }
            }
            if (mResponse.getResult().getAppoinments().size() > 0){
                mOrderApprovalDeliveryAppointmentTxt.setText(mResponse.getResult().getAppoinments().get(0).getAppoinment());
                mOrderApprovalDeliveryMeasurementTxt.setText(mResponse.getResult().getAppoinments().get(1).getAppoinment());
            }
            if (mResponse.getResult().getDeliveryTypes().size()  > 0){
                mOrderApprovalDeliveryTypeTxt.setText(mResponse.getResult().getDeliveryTypes().get(0).getDeliveryType());
            }
            if (mResponse.getResult().getDeliveryDate().size() > 0){
                mOrderApprovalDeliveryDateTxt.setText(mResponse.getResult().getDeliveryDate().get(0).getDeliveryDate().replace("T00:00:00",""));
            }
            if (mResponse.getResult().getStichingTime().size() > 0){
                mOrderApprovalDeliveryTimeTxt.setText(mResponse.getResult().getStichingTime().get(0).getStichingTime());
            }
            if(mResponse.getResult().getDelivery().size() > 0){
                AppConstants.DELIVERY_ID = String.valueOf(mResponse.getResult().getDelivery().get(0).getId());
                mOrderApprovalDeliveryMsgTxt.setVisibility(mResponse.getResult().getDelivery().get(0).getId() == 2 ? View.VISIBLE : View.GONE);
            }
        }
        if (resObj instanceof UpdateQuantityResponse){
            UpdateQuantityResponse mResponse = (UpdateQuantityResponse)resObj;
        }
    }

    public void getLanguage(){
        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.order_approval_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.order_approval_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }


}
