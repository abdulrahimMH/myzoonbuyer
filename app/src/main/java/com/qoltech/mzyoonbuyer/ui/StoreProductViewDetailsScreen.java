package com.qoltech.mzyoonbuyer.ui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.GetAppointmentTimeSlotAdapter;
import com.qoltech.mzyoonbuyer.adapter.StoreColorAdapter;
import com.qoltech.mzyoonbuyer.adapter.StoreProductListAdapter;
import com.qoltech.mzyoonbuyer.adapter.StoreSizeAdapter;
import com.qoltech.mzyoonbuyer.adapter.ViewDetailPagerAdapter;
import com.qoltech.mzyoonbuyer.entity.AddToCartEntity;
import com.qoltech.mzyoonbuyer.entity.DashboardProductsEntity;
import com.qoltech.mzyoonbuyer.entity.GetProductColorEntity;
import com.qoltech.mzyoonbuyer.entity.GetProductSellerEntity;
import com.qoltech.mzyoonbuyer.entity.GetProductSizeEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetProductDetailsResponse;
import com.qoltech.mzyoonbuyer.modal.StoreCartResponse;
import com.qoltech.mzyoonbuyer.service.APICommonInterface;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ZoomOutPageTransformer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class StoreProductViewDetailsScreen extends BaseActivity {

    @BindView(R.id.store_product_view_details_par_lay)
    LinearLayout mStoreProductViewDetParLay;

    @BindView(R.id.header_store_search_lay)
    RelativeLayout mStoreSearchLay;

    @BindView(R.id.header_store_lay)
    RelativeLayout mStoreLay;

    @BindView(R.id.header_store_catogery_icon)
    ImageView mHeaderStoreCatogeryImg;

    @BindView(R.id.header_store_catogery_txt)
    TextView mHeaderStoreCatogeryTxt;

    @BindView(R.id.header_store_whis_list_icon)
    ImageView mHeaderStoreWishListImg;

    @BindView(R.id.header_store_whis_list_txt)
    TextView mHeaderStoreWishListTxt;

    @BindView(R.id.store_product_view_details_view_pager)
    ViewPager mStoreProdDetViewPager;

    @BindView(R.id.store_product_view_details_indicator)
    com.rd.PageIndicatorView mStoreProdDetIndicator;

    @BindView(R.id.store_prod_det_brand_txt)
            TextView mStoreProdDetBrandTxt;

    @BindView(R.id.store_prod_det_prod_name_txt)
            TextView mStoreProdDetProdNameTxt;

    @BindView(R.id.store_prod_det_shop_name_txt)
            TextView mStoreProdDetShopNameTxt;

    @BindView(R.id.store_prod_det_rating_txt)
            TextView mStoreProdDetRatingTxt;

    @BindView(R.id.store_prod_det_review_txt)
            TextView mStoreProdDetReviewTxt;

    @BindView(R.id.store_prod_det_amout_txt)
            TextView mStoreProdDetAmtTxt;

    @BindView(R.id.store_prod_det_discount_txt)
            TextView mStoreProdDetDiscountTxt;

    @BindView(R.id.store_prod_det_description_txt)
            TextView mStoreProdDetDescriptionTxt;

    @BindView(R.id.store_prod_similar_prod_txt)
            TextView mStoreProdSimilarProdTxt;

    @BindView(R.id.store_prod_similar_prod_rec_view)
    RecyclerView mStoreProdSimilarProdRecView;

    @BindView(R.id.store_prod_det_size_rec_view)
            RecyclerView mStoreProdDetSizeRecView;

    @BindView(R.id.store_prod_det_color_rec_view)
            RecyclerView mStoreProdDetColorRecView;

    @BindView(R.id.store_prod_det_empty_size_txt)
            TextView mStoreProdDetEmptySizeTxt;

    @BindView(R.id.store_prod_det_empty_color_txt)
            TextView mStoreProdDetEmptyColorTxt;

    @BindView(R.id.store_other_seller_par_lay)
            RelativeLayout mStoreOtherSellerParLay;

    @BindView(R.id.store_prod_det_other_seller_txt)
            TextView mStoreProdDetOtherSellerTxt;

    @BindView(R.id.store_prod_det_qty_txt)
          public TextView mStoreProdDetQtyTxt;

    @BindView(R.id.store_prod_det_rating_lay)
    CardView mStoreProdDetRatingLay;

    @BindView(R.id.store_product_view_details_heart_img)
    com.like.LikeButton mStoreProdViewDetHeartImg;

    @BindView(R.id.bottom_notification_lay)
    RelativeLayout mBottomBadgeLay;

    @BindView(R.id.bottom_notification_txt)
    TextView mBottomNotificationTxt;

    @BindView(R.id.store_bottom_cart_img)
            ImageView mStoreBottomCartImg;

    @BindView(R.id.store_prod_det_currently_unavailable_view)
            View mStoreProdCurrentlyUnavailableView;

    @BindView(R.id.store_prod_det_currently_unavailable_txt)
            TextView mStoreProdCurrentlyUnavailableTxt;

    @BindView(R.id.store_prod_det_no_stock_txt)
            TextView mStoreProdNoStockTxt;

    @BindView(R.id.add_to_cart_txt)
            TextView mAddToCartTxt;

    UserDetailsEntity mUserDetailsEntityRes;

    ViewDetailPagerAdapter mViewDetailAdapter;

    StoreProductListAdapter mStoreProductListAdapter;

    StoreSizeAdapter mStoreSizeAdapter;

    StoreColorAdapter mStoreColorAdapter;

    ArrayList<GetProductSellerEntity> mSellerList;

    ArrayList<String> mImageList;

    int mSellerCountInt = 0,mAvailableStockCount = 0, mReviewCountInt;

    Dialog mQuantityDialog;

    APICommonInterface mApiCommonInterface;

    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;
    final long PERIOD_MS = 3000;
    int count = 0;

    private ArrayList<String> mQuantityList;

    private GetAppointmentTimeSlotAdapter mGetAppointmentTimeSlotAdater;

    HashMap<String,String> mProductIdist = new HashMap<>();

    String mSellerId = "0",mShareLink = "",mProductName="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_store_product_view_details_screen);

        initView();
    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mStoreProductViewDetParLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        mImageList = new ArrayList<>();

        mSellerList = new ArrayList<>();

        mProductIdist = new HashMap<>();

        mQuantityList = new ArrayList<>();

        AppConstants.STORE_PRODUCT_REFRESH = "SAME_PAGE";

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(StoreProductViewDetailsScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        String idJson = PreferenceUtil.getStringValue(StoreProductViewDetailsScreen.this,AppConstants.STORE_RECENT_PRODUCT);
        Type type = new TypeToken<HashMap<String,String>>(){}.getType();
        mProductIdist = gson.fromJson(idJson,type);

        getLanguage();

        mApiCommonInterface = APIRequestHandler.getClient().create(APICommonInterface.class);

        AppConstants.STORE_SIZE_ID = "";

        AppConstants.STORE_COLOR_ID = "";

        getProductDetailsApiCall(AppConstants.STORE_PRODUCT_ID);

        getStoreCartApiCall();

        if (mProductIdist == null) {
            mProductIdist = new HashMap<>();

            mProductIdist.put(AppConstants.STORE_PRODUCT_ID,AppConstants.STORE_PRODUCT_ID);

            PreferenceUtil.storeRecentProducts(this,mProductIdist);
        }else {
            if (mProductIdist.size() < 9){
                mProductIdist.put(AppConstants.STORE_PRODUCT_ID,AppConstants.STORE_PRODUCT_ID);

                PreferenceUtil.storeRecentProducts(this,mProductIdist);
            }else {

                mProductIdist.remove(mProductIdist.size()-1);

                mProductIdist.put(AppConstants.STORE_PRODUCT_ID,AppConstants.STORE_PRODUCT_ID);

                PreferenceUtil.storeRecentProducts(this,mProductIdist);

            }
        }


        mStoreProdViewDetHeartImg.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                addToWishListApiCall(AppConstants.STORE_PRODUCT_ID,AppConstants.STORE_SELLER_ID);
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                removeToWishListApiCall(AppConstants.STORE_PRODUCT_ID,AppConstants.STORE_SELLER_ID);

            }
        });

    }

    @OnClick({R.id.header_store_back_btn_img,R.id.header_store_catogery_lay,R.id.header_wish_list_par_lay,R.id.store_prod_det_qty_par_lay,R.id.add_to_cart_txt,R.id.store_prod_det_view_all_txt,R.id.store_bottom_home_img,R.id.store_bottom_cart_img,R.id.store_prod_det_shop_name_txt,R.id.header_store_search_par_lay,R.id.header_store_icon_par_lay,R.id.store_prod_det_share_img,R.id.store_prod_det_rating_lay,R.id.store_prod_det_review_txt})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_store_back_btn_img:
                onBackPressed();
                break;
            case R.id.header_store_catogery_lay:
                nextScreen(StoreCategoryScreen.class,true);
                break;
            case R.id.header_wish_list_par_lay:
                nextScreen(StoreWishListScreen.class,true);
                break;
            case R.id.store_prod_det_qty_par_lay:
                if (mAvailableStockCount == 0){
                    Toast.makeText(StoreProductViewDetailsScreen.this,getResources().getString(R.string.currently_unavailable),Toast.LENGTH_SHORT).show();
                }else {
                    if (mQuantityList.size() > 0){
                        alertDismiss(mQuantityDialog);
                        mQuantityDialog = getDialog(StoreProductViewDetailsScreen.this, R.layout.pop_up_country_alert);

                        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                        Window window = mQuantityDialog.getWindow();

                        if (window != null) {
                            LayoutParams.copyFrom(window.getAttributes());
                            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(LayoutParams);
                            window.setGravity(Gravity.CENTER);
                        }

                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            ViewCompat.setLayoutDirection(mQuantityDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                        }else {
                            ViewCompat.setLayoutDirection(mQuantityDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                        }

                        TextView cancelTxt , headerTxt;

                        RecyclerView countryRecyclerView;

                        /*Init view*/
                        cancelTxt = mQuantityDialog.findViewById(R.id.country_text_cancel);
                        headerTxt = mQuantityDialog.findViewById(R.id.country_header_text_cancel);

                        countryRecyclerView = mQuantityDialog.findViewById(R.id.country_popup_recycler_view);
                        mGetAppointmentTimeSlotAdater = new GetAppointmentTimeSlotAdapter(StoreProductViewDetailsScreen.this, mQuantityList,mQuantityDialog,"STORE_VIEW_PRODUCT");

                        countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        countryRecyclerView.setAdapter(mGetAppointmentTimeSlotAdater);

                        /*Set data*/
                        headerTxt.setText(getResources().getString(R.string.select_your_qty));
                        cancelTxt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mQuantityDialog.dismiss();
                            }
                        });

                        alertShowing(mQuantityDialog);
                    }else {
                        Toast.makeText(StoreProductViewDetailsScreen.this,getResources().getString(R.string.loading_plz_wait),Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.add_to_cart_txt:
                if (mAvailableStockCount == 0){
                    addToWishListApiCall(AppConstants.STORE_PRODUCT_ID,AppConstants.STORE_SELLER_ID);
                }else {
                    if (AppConstants.STORE_SIZE_ID.equalsIgnoreCase("")){
                        Toast.makeText(StoreProductViewDetailsScreen.this,getResources().getString(R.string.please_select_size),Toast.LENGTH_SHORT).show();
                    }else if (AppConstants.STORE_COLOR_ID.equalsIgnoreCase("")){
                        Toast.makeText(StoreProductViewDetailsScreen.this,getResources().getString(R.string.please_select_color),Toast.LENGTH_SHORT).show();
                    }else {
                        addToCartApiCall();
                    }
                }
                break;
            case R.id.store_prod_det_view_all_txt:
                nextScreen(StoreViewSellerScreen.class,true);
                break;
            case R.id.store_bottom_home_img:
                if (AppConstants.ORDER_TYPE_CHECK.equalsIgnoreCase("Stitching")) {
                    DialogManager.getInstance().showOptionPopup(getContext(), getResources().getString(R.string.sure_want_to_go_back), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                        @Override
                        public void onNegativeClick() {

                        }

                        @Override
                        public void onPositiveClick() {
                            previousScreen(BottomNavigationScreen.class,true);
                        }
                    });
                }else {
                    previousScreen(BottomNavigationScreen.class,true);
                }
                break;
            case R.id.store_bottom_cart_img:
                nextScreen(CartScreen.class,true);
                break;
            case R.id.store_prod_det_shop_name_txt:
                nextScreen(ShopDetailsScreen.class,true);
                break;
            case R.id.header_store_search_par_lay:
                nextScreen(StoreSearchScreen.class,true);
                break;
            case R.id.header_store_icon_par_lay:
                if (AppConstants.ORDER_TYPE_CHECK.equalsIgnoreCase("Stitching")){
                    nextScreen(StoreDashboardScreen.class,true);

                }else {
                    previousScreen(StoreDashboardScreen.class,true);
                }
                break;
            case R.id.store_prod_det_share_img:
                String shareBodys = getResources().getString(R.string.found_this_amazing_product_in_mzyoon)+ " " +mShareLink +" " + getResources().getString(R.string.for_more_details) + "\n\n" + getResources().getString(R.string.click)+" "+ "http://onelink.to/mzyoon"+" " +getResources().getString(R.string.for_install_mzyoon) +"\n";
                Intent sharingIntents = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntents.setType("text/plain");
                sharingIntents.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.amazing_products_by_mzyaoon));
                sharingIntents.putExtra(android.content.Intent.EXTRA_TEXT, shareBodys);
                startActivity(Intent.createChooser(sharingIntents, getResources().getString(R.string.app_name)));
                break;
            case R.id.store_prod_det_rating_lay:
                AppConstants.STORE_ORDER_DETAILS_PRODUCT_NAME = mProductName;
                AppConstants.MATERIAL_REVIEW = "false";
                nextScreen(RatingScreen.class,true);
                break;
            case R.id.store_prod_det_review_txt:
                if (mReviewCountInt > 0){
                    AppConstants.STORE_ORDER_DETAILS_PRODUCT_NAME = mProductName;
                    AppConstants.MATERIAL_REVIEW = "false";
                    nextScreen(RatingScreen.class,true);
                }
                break;
        }
    }

    public void setHeader(){

        mStoreSearchLay.setVisibility(View.GONE);
        mStoreLay.setVisibility(View.VISIBLE);

        mHeaderStoreCatogeryImg.setBackgroundResource(R.drawable.store_catogery_white_icon);
        mHeaderStoreCatogeryTxt.setTextColor(getResources().getColor(R.color.white));

        mHeaderStoreWishListImg.setBackgroundResource(R.drawable.store_heart_white_icon);
        mHeaderStoreWishListTxt.setTextColor(getResources().getColor(R.color.white));
    }

    public void getProductDetailsApiCall(String ProductId){
        if (NetworkUtil.isNetworkAvailable(this)){
            APIRequestHandler.getInstance().getStoreProductDetailsApi(ProductId,AppConstants.STORE_SELLER_ID,PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getProductDetailsApiCall(ProductId);
                }
            });
        }
    }

    public void getStoreCartApiCall() {
        if (NetworkUtil.isNetworkAvailable(this)){
            APIRequestHandler.getInstance().getStoreCartListApi(PreferenceUtil.getStringValue(this,AppConstants.CARD_ID),"List",this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStoreCartApiCall();
                }
            });
        }
    }


    public void getLanguage(){
        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.store_product_view_details_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.store_product_view_details_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    public void addToCartApiCall(){
        DialogManager.getInstance().showProgress(this);
        if (NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.addToCartApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),AppConstants.STORE_PRODUCT_ID,AppConstants.STORE_SIZE_ID,AppConstants.STORE_COLOR_ID,mSellerId,mStoreProdDetQtyTxt.getText().toString()).enqueue(new Callback<ArrayList<AddToCartEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<AddToCartEntity>> call, Response<ArrayList<AddToCartEntity>> response) {
                    DialogManager.getInstance().hideProgress();
                    if (response.body() != null && response.isSuccessful()){
                        if (response.body().size() > 0){
                            DialogManager.getInstance().showAlertPopup(StoreProductViewDetailsScreen.this, getResources().getString(R.string.product_added_msg), new InterfaceBtnCallBack() {
                                @Override
                                public void onPositiveClick() {

                                }
                            });
                        initView();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<AddToCartEntity>> call, Throwable t) {
                    DialogManager.getInstance().hideProgress();

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    addToCartApiCall();
                }
            });
        }

    }

    public void addToWishListApiCall(String ProductId,String SellerId){
        if (NetworkUtil.isNetworkAvailable(this)){
            String SizeStr = AppConstants.STORE_SIZE_ID.equalsIgnoreCase("") ? "-1" : AppConstants.STORE_SIZE_ID;
            String ColorStr = AppConstants.STORE_COLOR_ID.equalsIgnoreCase("") ? "-1" : AppConstants.STORE_COLOR_ID;

            mApiCommonInterface.storeAddToWishListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),ProductId,SizeStr,ColorStr,SellerId).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {
                    if (response.body() != null && response.isSuccessful()){
                        initView();
                        DialogManager.getInstance().showAlertPopup(StoreProductViewDetailsScreen.this, getResources().getString(R.string.product_added_to_wishlist), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                            }
                        });
                    }

                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    addToWishListApiCall(ProductId,SellerId);
                }
            });
        }
    }

    public void removeToWishListApiCall(String ProductId,String SellerId){
        if (NetworkUtil.isNetworkAvailable(this)){
            String SizeStr = AppConstants.STORE_SIZE_ID.equalsIgnoreCase("") ? "-1" : AppConstants.STORE_SIZE_ID;
           String ColorStr = AppConstants.STORE_COLOR_ID.equalsIgnoreCase("") ? "-1" : AppConstants.STORE_COLOR_ID;

            mApiCommonInterface.storeRemoveToWishListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),ProductId,SizeStr,ColorStr,SellerId).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {
                    if (response.body() != null && response.isSuccessful()){
                        initView();
                        DialogManager.getInstance().showAlertPopup(StoreProductViewDetailsScreen.this, getResources().getString(R.string.product_removed_from_wishlist), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                            }
                        });
                    }

                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    removeToWishListApiCall(ProductId,SellerId);
                }
            });
        }
    }


    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetProductDetailsResponse){
            GetProductDetailsResponse mResponse = (GetProductDetailsResponse)resObj;
            if (mResponse.getProducts() !=  null){
                mProductName = mResponse.getProducts().getProductName();
                mStoreProdDetShopNameTxt.setText(mResponse.getProducts().getShopNameInEnglish());
                mStoreProdDetRatingTxt.setText(String.valueOf(mResponse.getProducts().getRatingsValue()));
                mStoreProdDetRatingLay.setVisibility(mResponse.getProducts().getRatingsValue() > 0 ? View.VISIBLE : View.GONE);
                mReviewCountInt = mResponse.getProducts().getRatingsCount();
                mStoreProdDetReviewTxt.setText(mResponse.getProducts().getRatingsCount() > 0 ? String.valueOf(mResponse.getProducts().getRatingsCount()+" "+getResources().getString(R.string.review)) : getResources().getString(R.string.no_reviews));
                mStoreProdDetAmtTxt.setText(String.valueOf(mResponse.getProducts().getNewPrice()));
                mStoreProdDetDiscountTxt.setText(String.valueOf(mResponse.getProducts().getAmount()));
                mStoreProdDetDiscountTxt.setVisibility(mResponse.getProducts().getDiscountType().equalsIgnoreCase("0") ? View.GONE : View.VISIBLE);
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mStoreProdDetDescriptionTxt.setText(String.valueOf(mResponse.getProducts().getProductDescriptionInArabic()));
                    mStoreProdDetBrandTxt.setText(mResponse.getProducts().getBrandNameInArabic());
                    mStoreProdDetProdNameTxt.setText(mResponse.getProducts().getProductNameInArabic());

                }else {
                    mStoreProdDetDescriptionTxt.setText(String.valueOf(mResponse.getProducts().getProductDescription()));
                    mStoreProdDetBrandTxt.setText(mResponse.getProducts().getBrandName());
                    mStoreProdDetProdNameTxt.setText(mResponse.getProducts().getProductName());

                }
                mSellerId = String.valueOf(mResponse.getProducts().getSellerId());
                mStoreProdViewDetHeartImg.setLiked(mResponse.getProducts().isProduct());
                AppConstants.TAILOR_ID = String.valueOf(mResponse.getProducts().getSellerId());
                mStoreProdDetShopNameTxt.setPaintFlags(mStoreProdDetShopNameTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                mAvailableStockCount = mResponse.getProducts().getAvailibilityCount();
                mShareLink = mResponse.getProducts().getShareUrl();
                if (mResponse.getProducts().getAvailibilityCount() == 0){
                    mStoreProdCurrentlyUnavailableView.setVisibility(View.VISIBLE);
                    mStoreProdCurrentlyUnavailableTxt.setVisibility(View.VISIBLE);
                    mStoreProdNoStockTxt.setVisibility(View.VISIBLE);
                    mAddToCartTxt.setText(getResources().getString(R.string.add_to_wishlist));
                    mStoreProdDetQtyTxt.setText("0");
                }else {
                    mStoreProdCurrentlyUnavailableView.setVisibility(View.GONE);
                    mStoreProdCurrentlyUnavailableTxt.setVisibility(View.GONE);
                    mStoreProdNoStockTxt.setVisibility(View.GONE);
                    mAddToCartTxt.setText(getResources().getString(R.string.add_to_cart));
                    mQuantityList = new ArrayList<>();
                    for(int i=1; i<=mResponse.getProducts().getAvailibilityCount(); i++){
                        mQuantityList.add(String.valueOf(i));
                    }
                    mStoreProdDetQtyTxt.setText("1");
                }
            }

            if (mResponse.getProductImages().size() >0){
                for(int i=0; i<mResponse.getProductImages().size(); i++){
                    mImageList.add(AppConstants.IMAGE_BASE_URL+"Images/Products/"+mResponse.getProductImages().get(i).getSmall());
                }
                viewPagerGet(mImageList);

            }
            setStoreSizeAdapter(mResponse.getSizes());
            setStoreColorAdapter(mResponse.getColors());
            setProductListAdapter(mResponse.getRealtedProducts());

            mSellerList = mResponse.getSellers();

            mSellerCountInt = 0;

            for (int i=0; i<mSellerList.size(); i++){
                if (mSellerList.get(i).getSellerId() == Integer.parseInt(mSellerId)){
                    mSellerList.get(i).setChecked(true);
                }else {
                    mSellerCountInt = mSellerCountInt + 1;
                }
            }
            AppConstants.PRODUCT_SELLER_LIST = mSellerList;

            mStoreOtherSellerParLay.setVisibility(mSellerCountInt > 0 ? View.VISIBLE : View.GONE);
            if (mSellerCountInt > 0){
                if (mResponse.getSellers().size() > 1){
                    mStoreProdDetOtherSellerTxt.setText(String.valueOf(mSellerCountInt + " " +getResources().getString(R.string.other_seller_txt) + " " + mResponse.getSellers().get(1).getAmount()));

                }
            }
        }
        if (resObj instanceof StoreCartResponse) {

            int countQtyCart = 0;
            StoreCartResponse mREsponse = (StoreCartResponse)resObj;

            for (int i=0 ; i<mREsponse.getResult().size(); i++){
                countQtyCart = countQtyCart + mREsponse.getResult().get(i).getQuantity();
            }

            mBottomNotificationTxt.setText(String.valueOf(countQtyCart));
            if (countQtyCart > 0){
                mBottomBadgeLay.setVisibility(View.VISIBLE);
            }else {
                mBottomBadgeLay.setVisibility(View.GONE);

            }
        }
    }

    public void setProductListAdapter(ArrayList<DashboardProductsEntity> mProductList){

        mStoreProdSimilarProdTxt.setVisibility(mProductList.size()>0 ? View.VISIBLE : View.GONE);
        mStoreProdSimilarProdRecView.setVisibility(mProductList.size() > 0 ? View.VISIBLE : View.GONE);

        mStoreProductListAdapter = new StoreProductListAdapter(this,mProductList,"ViewDetail");
        mStoreProdSimilarProdRecView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        mStoreProdSimilarProdRecView.setAdapter(mStoreProductListAdapter);

    }

    public void setStoreSizeAdapter(ArrayList<GetProductSizeEntity> mSizeList){

        AppConstants.STORE_SIZE_ID = mSizeList.size() > 0 ? "" : "-1";

        mStoreProdDetEmptySizeTxt.setVisibility(mSizeList.size() > 0 ? View.GONE : View.VISIBLE);
        mStoreProdDetSizeRecView.setVisibility(mSizeList.size() > 0 ? View.VISIBLE : View.GONE);

        mStoreSizeAdapter = new StoreSizeAdapter(mSizeList,this);
        mStoreProdDetSizeRecView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        mStoreProdDetSizeRecView.setAdapter(mStoreSizeAdapter);

    }

    public void setStoreColorAdapter(ArrayList<GetProductColorEntity> mColorList){

        AppConstants.STORE_COLOR_ID = mColorList.size() > 0 ? "" : "-1";

        mStoreProdDetEmptyColorTxt.setVisibility(mColorList.size() > 0 ? View.GONE : View.VISIBLE);
        mStoreProdDetColorRecView.setVisibility(mColorList.size() > 0 ? View.VISIBLE : View.GONE);

        mStoreColorAdapter = new StoreColorAdapter(mColorList,this);
        mStoreProdDetColorRecView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        mStoreProdDetColorRecView.setAdapter(mStoreColorAdapter);

    }


    public void viewPagerGet(ArrayList<String> image){
        mViewDetailAdapter = new ViewDetailPagerAdapter(this,image);
        mStoreProdDetViewPager.setAdapter(mViewDetailAdapter);
        mStoreProdDetViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mStoreProdDetIndicator.setViewPager(mStoreProdDetViewPager);

        if (count == 0) {
            count = 1;
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == image.size()) {
                        currentPage = 0;
                    }
                    mStoreProdDetViewPager.setCurrentItem(currentPage++, true);
                }
            };

            timer = new Timer(); // This will create a new Thread
            timer.schedule(new TimerTask() { // task to be scheduled
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, DELAY_MS, PERIOD_MS);
        }


        //page change tracker
        mStoreProdDetViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        trackScreenName("StoreProductDetails");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AppConstants.STORE_PRODUCT_REFRESH.equalsIgnoreCase("REFRESH")){
            initView();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
