package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.QuotationListAdapter;
import com.qoltech.mzyoonbuyer.entity.QuotationEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.QuotationListResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QuotationListScreen extends BaseActivity {

    @BindView(R.id.quotaion_list_par_lay)
    LinearLayout mQuotationListPayLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.quotation_list_recycler_view)
    RecyclerView mQuotationListRecyclerView;

    @BindView(R.id.quotation_list_request_id_txt)
            TextView mQuotationListRequestIdTxt;

    @BindView(R.id.quotation_list_request_on_txt)
            TextView mQuotationListRequestOnTxt;

    @BindView(R.id.quotation_list_material_type_txt)
            TextView mQuotationListMaterialTypeTxt;

    @BindView(R.id.quotation_list_service_type_txt)
            TextView mQuotationListServiceTypeTxt;

    @BindView(R.id.quotation_list_delivery_type_txt)
            TextView mQuotationListDeliveryTypeTxt;

    QuotationListAdapter mQuotationAdapter;

    private UserDetailsEntity mUserDetailsEntityRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_quotation_list_screen);

        initView();

    }
    public void initView(){

        ButterKnife.bind(this);

        setupUI(mQuotationListPayLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(QuotationListScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        setHeader();

        quotationListApiCall();
    }
    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.request_details));
        mRightSideImg.setVisibility(View.INVISIBLE);

    }

    public void quotationListApiCall(){
       if (NetworkUtil.isNetworkAvailable(QuotationListScreen.this)){
           APIRequestHandler.getInstance().quotationListApiCall(AppConstants.REQUEST_LIST_ID,QuotationListScreen.this);
       }else {
           DialogManager.getInstance().showNetworkErrorPopup(QuotationListScreen.this, new InterfaceBtnCallBack() {
               @Override
               public void onPositiveClick() {
                   quotationListApiCall();
               }
           });
       }
    }

    @OnClick({R.id.header_left_side_img,R.id.quotation_list_view_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.quotation_list_view_img:
                nextScreen(RequestViewMoreActivity.class,true);
                break;
        }
    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<QuotationEntity> quotationList) {

        if (quotationList.size()>0){
            mQuotationListRequestIdTxt.setText(String.valueOf(quotationList.get(0).getRequestId()));
            mQuotationListRequestOnTxt.setText(quotationList.get(0).getRequestedOn());
           if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                mQuotationListMaterialTypeTxt.setText(quotationList.get(0).getMaterialTypeInArabic());
                mQuotationListServiceTypeTxt.setText(quotationList.get(0).getServiceTypeInArabic());
               mQuotationListDeliveryTypeTxt.setText(quotationList.get(0).getDeliveryNameInArabic());
            }else {
                mQuotationListMaterialTypeTxt.setText(quotationList.get(0).getMaterialType());
                mQuotationListServiceTypeTxt.setText(quotationList.get(0).getServiceType());
               mQuotationListDeliveryTypeTxt.setText(quotationList.get(0).getDeliveryNameInEnglish());
           }
        }

        if (mQuotationAdapter == null) {

            mQuotationListRecyclerView.setVisibility(quotationList.size() > 0 ? View.VISIBLE : View.GONE);

            mQuotationAdapter = new QuotationListAdapter(this,quotationList);
            mQuotationListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mQuotationListRecyclerView.setAdapter(mQuotationAdapter);

        }
        else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mQuotationAdapter.notifyDataSetChanged();
                }
            });
        }

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof QuotationListResponse){
            QuotationListResponse mResponse = (QuotationListResponse)resObj;
            setAdapter(mResponse.getResult().getQuotationList());

        }
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.quotaion_list_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.quotaion_list_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
