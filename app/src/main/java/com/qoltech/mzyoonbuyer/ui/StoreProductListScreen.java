package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.StoreProductListMatchAdapter;
import com.qoltech.mzyoonbuyer.entity.DashboardProductsEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetStoreProductListModal;
import com.qoltech.mzyoonbuyer.modal.StoreCartResponse;
import com.qoltech.mzyoonbuyer.service.APICommonInterface;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class StoreProductListScreen extends BaseActivity {

    @BindView(R.id.store_product_list_par_lay)
    LinearLayout mStoreProductParLay;

    @BindView(R.id.header_store_search_lay)
    RelativeLayout mStoreSearchLay;

    @BindView(R.id.header_store_lay)
    RelativeLayout mStoreLay;

    @BindView(R.id.store_product_list_rec_view)
    RecyclerView mStoreProductListRecView;

    @BindView(R.id.header_store_catogery_icon)
    ImageView mHeaderStoreCatogeryImg;

    @BindView(R.id.header_store_catogery_txt)
    TextView mHeaderStoreCatogeryTxt;

    @BindView(R.id.header_store_whis_list_icon)
    ImageView mHeaderStoreWishListImg;

    @BindView(R.id.header_store_whis_list_txt)
    TextView mHeaderStoreWishListTxt;

    @BindView(R.id.store_product_empty_lay)
            RelativeLayout mStoreProdEmptyLay;

    @BindView(R.id.bottom_notification_lay)
    RelativeLayout mBottomBadgeLay;

    @BindView(R.id.bottom_notification_txt)
    TextView mBottomNotificationTxt;

    @BindView(R.id.store_product_list_rec_par_lay)
    RelativeLayout mStoreProductListRecParLay;

    @BindView(R.id.store_product_list_scroll_view_img)
            ImageView mStoreProductListScrollViewImg;

    StoreProductListMatchAdapter mStoreProductAdapter;

    UserDetailsEntity mUserDetailsEntityRes;

    APICommonInterface mApiCommonInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_store_product_list_screen);

        initView();

    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mStoreProductParLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        mApiCommonInterface = APIRequestHandler.getClient().create(APICommonInterface.class);

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(StoreProductListScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        if (AppConstants.STORE_SEARCH.equalsIgnoreCase("STORE_SEARCH")){
            getSearchStoreProductListApiCall(AppConstants.STORE_SEARCH_TXT);
        }else {
            getStoreProductListApiCall(AppConstants.STORE_BANNER_ID,AppConstants.STORE_TYPE_ID);

        }

        getStoreCartApiCall();

        AppConstants.STORE_PRODUCT_REFRESH = "REFRESH";

    }

        @OnClick({R.id.header_store_back_btn_img,R.id.header_store_catogery_lay,R.id.header_wish_list_par_lay,R.id.store_bottom_home_img,R.id.store_bottom_cart_img,R.id.header_store_search_par_lay,R.id.header_store_icon_par_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_store_back_btn_img:
                onBackPressed();
                break;
            case R.id.header_store_catogery_lay:
                nextScreen(StoreCategoryScreen.class,true);
                break;
            case R.id.header_wish_list_par_lay:
                nextScreen(StoreWishListScreen.class,true);
                break;
            case R.id.store_bottom_home_img:
                previousScreen(BottomNavigationScreen.class,true);
                break;
            case R.id.store_bottom_cart_img:
                nextScreen(CartScreen.class,true);
                break;
            case R.id.header_store_search_par_lay:
                nextScreen(StoreSearchScreen.class,true);
                break;
            case R.id.header_store_icon_par_lay:
                previousScreen(StoreDashboardScreen.class,true);
                break;
        }
    }

    public void setHeader(){

        mStoreSearchLay.setVisibility(View.GONE);
        mStoreLay.setVisibility(View.VISIBLE);

        mHeaderStoreCatogeryImg.setBackgroundResource(R.drawable.store_catogery_white_icon);
        mHeaderStoreCatogeryTxt.setTextColor(getResources().getColor(R.color.white));

        mHeaderStoreWishListImg.setBackgroundResource(R.drawable.store_heart_white_icon);
        mHeaderStoreWishListTxt.setTextColor(getResources().getColor(R.color.white));
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.store_product_list_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.store_product_list_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    public void getStoreProductListApiCall(String BannerId, String TypeId){

        if (NetworkUtil.isNetworkAvailable(this)){
            APIRequestHandler.getInstance().getStoreProductListApi(BannerId,PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),TypeId,this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStoreProductListApiCall(BannerId,TypeId);
                }
            });
        }
    }

    public void getSearchStoreProductListApiCall(String searchTxt){

        if (NetworkUtil.isNetworkAvailable(this)){
            APIRequestHandler.getInstance().getSearchStoreProductListApi(searchTxt,PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getSearchStoreProductListApiCall(searchTxt);
                }
            });
        }
    }


    public void addToWishListApiCall(String ProductId,String SellerId){
        if (NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.storeAddToWishListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),ProductId,"-1","-1",SellerId).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {
                    if (response.body() != null && response.isSuccessful()){
                        initView();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    addToWishListApiCall(ProductId,SellerId);
                }
            });
        }
    }

    public void removeToWishListApiCall(String ProductId,String SellerId){
        if (NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.storeRemoveToWishListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),ProductId,"-1","-1",SellerId).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {
                    if (response.body() != null && response.isSuccessful()){
                        initView();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    removeToWishListApiCall(ProductId,SellerId);
                }
            });
        }
    }

    public void getStoreCartApiCall() {
        if (NetworkUtil.isNetworkAvailable(this)){
            APIRequestHandler.getInstance().getStoreCartListApi(PreferenceUtil.getStringValue(this,AppConstants.CARD_ID),"List",this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStoreCartApiCall();
                }
            });
        }
    }


    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetStoreProductListModal){
            GetStoreProductListModal mResponse = (GetStoreProductListModal)resObj;

            setStoreProductAdapter(mResponse.getProducts());
        }
        if (resObj instanceof StoreCartResponse) {

            int countQtyCart = 0;
            StoreCartResponse mREsponse = (StoreCartResponse)resObj;

            for (int i=0 ; i<mREsponse.getResult().size(); i++){
                countQtyCart = countQtyCart + mREsponse.getResult().get(i).getQuantity();
            }

            mBottomNotificationTxt.setText(String.valueOf(countQtyCart));
            if (countQtyCart > 0){
                mBottomBadgeLay.setVisibility(View.VISIBLE);
            }else {
                mBottomBadgeLay.setVisibility(View.GONE);

            }
        }
    }

    public void setStoreProductAdapter(ArrayList<DashboardProductsEntity> mProductList){

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);

        mStoreProdEmptyLay.setVisibility(mProductList.size() > 0 ? View.GONE : View.VISIBLE);
        mStoreProductListRecParLay.setVisibility(mProductList.size() > 0 ? View.VISIBLE : View.GONE);

        mStoreProductAdapter = new StoreProductListMatchAdapter(this,mProductList);
        mStoreProductListRecView.setLayoutManager(layoutManager);
        mStoreProductListRecView.setAdapter(mStoreProductAdapter);

        mStoreProductListRecView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = layoutManager.findLastVisibleItemPosition();

                if (lastPosition == mProductList.size()-1){
                    mStoreProductListScrollViewImg.setVisibility(View.GONE);
                }else {
                    mStoreProductListScrollViewImg.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }
}
