package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.StoreProductListAdapter;
import com.qoltech.mzyoonbuyer.adapter.StoreWishListAdapter;
import com.qoltech.mzyoonbuyer.entity.DashboardProductsEntity;
import com.qoltech.mzyoonbuyer.entity.GetRecentProductEntity;
import com.qoltech.mzyoonbuyer.entity.IdStringEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.entity.WishListEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.StoreCartResponse;
import com.qoltech.mzyoonbuyer.service.APICommonInterface;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class StoreWishListScreen extends BaseActivity {

    @BindView(R.id.store_wish_list_par_lay)
    LinearLayout mStoreWishListParLay;

    @BindView(R.id.header_store_search_lay)
    RelativeLayout mStoreSearchLay;

    @BindView(R.id.header_store_lay)
    RelativeLayout mStoreLay;

    @BindView(R.id.header_store_catogery_icon)
    ImageView mHeaderStoreCatogeryImg;

    @BindView(R.id.header_store_catogery_txt)
    TextView mHeaderStoreCatogeryTxt;

    @BindView(R.id.header_store_whis_list_icon)
            ImageView mHeaderStoreWishListImg;

    @BindView(R.id.header_store_whis_list_txt)
            TextView mHeaderStoreWishListTxt;

    @BindView(R.id.store_wish_list_prod_txt)
            TextView mstoreWishRelatedProdTxt;

    @BindView(R.id.store_wish_list_prod_rec_view)
    RecyclerView mStoreWishListRelatedProdRecView;

    @BindView(R.id.store_wish_list_prod_view)
            View mStoreWishListRelatedProdView;

    @BindView(R.id.store_wish_list_empty_lay)
            LinearLayout mStoreWishListEmptyLay;

    @BindView(R.id.store_wish_list_txt)
            TextView mStoreWishListTxt;

    @BindView(R.id.store_wish_list_rec_view)
            RecyclerView mStoreWishListRecView;

    @BindView(R.id.store_wish_list_rec_par_lay)
    RelativeLayout mStoreWishListRecParLay;

    @BindView(R.id.bottom_notification_lay)
    RelativeLayout mBottomBadgeLay;

    @BindView(R.id.bottom_notification_txt)
    TextView mBottomNotificationTxt;

    @BindView(R.id.store_wish_list_scroll_view_img)
            ImageView mStoreWishListScrollViewImg;

    StoreWishListAdapter mStoreWishListAdapter;

    UserDetailsEntity mUserDetailsEntityRes;

    APICommonInterface mApiCommonInterface;

    StoreProductListAdapter mStoreProductListAdapter;

    HashMap<String,String> mProductIdist = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_store_wish_list_screen);

        initView();
    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mStoreWishListParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        mApiCommonInterface = APIRequestHandler.getClient().create(APICommonInterface.class);

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(StoreWishListScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        String idJson = PreferenceUtil.getStringValue(StoreWishListScreen.this,AppConstants.STORE_RECENT_PRODUCT);
        Type type = new TypeToken<HashMap<String,String>>(){}.getType();
        mProductIdist = gson.fromJson(idJson,type);

        getLanguage();

        getWishListApiCall();

        getRecentProductListApi();

        getStoreCartApiCall();

        AppConstants.STORE_PRODUCT_REFRESH = "REFRESH";

        setHeader();

    }

    @OnClick({R.id.header_store_back_btn_img,R.id.header_store_catogery_lay,R.id.store_wish_list_empty_shop_now_txt,R.id.store_bottom_home_img,R.id.store_bottom_cart_img,R.id.header_store_search_par_lay,R.id.header_store_icon_par_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_store_back_btn_img:
                onBackPressed();
                break;
            case R.id.header_store_catogery_lay:
                nextScreen(StoreCategoryScreen.class,true);
                break;
            case R.id.store_wish_list_empty_shop_now_txt:
                previousScreen(StoreDashboardScreen.class,true);
                break;
            case R.id.store_bottom_home_img:
                previousScreen(BottomNavigationScreen.class,true);
                break;
            case R.id.store_bottom_cart_img:
                nextScreen(CartScreen.class,true);
                break;
            case R.id.header_store_search_par_lay:
                nextScreen(StoreSearchScreen.class,true);
                break;
            case R.id.header_store_icon_par_lay:
                previousScreen(StoreDashboardScreen.class,true);
                break;
        }
    }

    public void setHeader(){

        mStoreSearchLay.setVisibility(View.GONE);
        mStoreLay.setVisibility(View.VISIBLE);

        mHeaderStoreCatogeryImg.setBackgroundResource(R.drawable.store_catogery_white_icon);
        mHeaderStoreCatogeryTxt.setTextColor(getResources().getColor(R.color.white));

        mHeaderStoreWishListImg.setBackgroundResource(R.drawable.store_heart_yellow_icon);
        mHeaderStoreWishListTxt.setTextColor(getResources().getColor(R.color.yellow_clr));
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.store_wish_list_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.store_wish_list_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    public void addToWishListApiCall(String ProductId,String SellerId){
        if (NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.storeAddToWishListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),ProductId,"-1","-1",SellerId).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {
                    if (response.body() != null && response.isSuccessful()){
                        initView();
                        DialogManager.getInstance().showAlertPopup(StoreWishListScreen.this, getResources().getString(R.string.product_added_to_wishlist), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    addToWishListApiCall(ProductId,SellerId);
                }
            });
        }
    }


    public void removeToWishListApiCall(String ProductId,String SellerId){
        if (NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.storeRemoveToWishListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),ProductId,"-1","-1",SellerId).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {
                    if (response.body() != null && response.isSuccessful()){
                        initView();
                        DialogManager.getInstance().showAlertPopup(StoreWishListScreen.this, getResources().getString(R.string.product_removed_from_wishlist), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    removeToWishListApiCall(ProductId,SellerId);
                }
            });
        }
    }

    public void removeToWishListApiCall(String ProductId,String SizeId,String ColorId,String SellerId){
        if (NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.storeRemoveToWishListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),ProductId,SizeId,ColorId,SellerId).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {
                    if (response.body() != null && response.isSuccessful()){
                        initView();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    removeToWishListApiCall(ProductId,SizeId,ColorId,SellerId);
                }
            });
        }
    }

    public void getRecentProductListApi(){

        ArrayList<IdStringEntity> idStringList = new ArrayList<>();

        GetRecentProductEntity mRecenetProductEntity = new GetRecentProductEntity();

        if (mProductIdist != null){
            Set keys = mProductIdist.keySet();
            Iterator itr = keys.iterator();

            String key;
            String value;
            while(itr.hasNext())
            {
                IdStringEntity idStringEntity = new IdStringEntity();

                key = (String)itr.next();
                value = (String)mProductIdist.get(key);
                System.out.println(key + " - "+ value);
                idStringEntity.setId(value);
                idStringList.add(idStringEntity);
            }
        }

        mRecenetProductEntity.setId(idStringList);
        mRecenetProductEntity.setCartId(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID));
        mRecenetProductEntity.setKey("List");

        if (NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.getRecentProductList(mRecenetProductEntity).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {

                    if (response.body() != null && response.isSuccessful()){
                        setRecentProductListAdapter(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getRecentProductListApi();
                }
            });
        }

    }

    public void getStoreCartApiCall() {
        if (NetworkUtil.isNetworkAvailable(this)){
            APIRequestHandler.getInstance().getStoreCartListApi(PreferenceUtil.getStringValue(this,AppConstants.CARD_ID),"List",this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStoreCartApiCall();
                }
            });
        }
    }



    public void getWishListApiCall(){
        DialogManager.getInstance().showProgress(this);
        if (NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.getWishListApiCall(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID)).enqueue(new Callback<ArrayList<WishListEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<WishListEntity>> call, Response<ArrayList<WishListEntity>> response) {
                    DialogManager.getInstance().hideProgress();
                    if (response.body() != null && response.isSuccessful()){
                        setWishListAdapter(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<WishListEntity>> call, Throwable t) {
                    DialogManager.getInstance().hideProgress();

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getWishListApiCall();
                }
            });
        }
    }

    public void setWishListAdapter(ArrayList<WishListEntity> mWishList){

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mStoreWishListRecView.setVisibility(mWishList.size() > 0 ? View.VISIBLE : View.GONE);
        mStoreWishListEmptyLay.setVisibility(mWishList.size() > 0 ? View.GONE : View.VISIBLE);

        mStoreWishListTxt.setVisibility(mWishList.size() > 0 ? View.VISIBLE : View.GONE);
        mStoreWishListRecParLay.setVisibility(mWishList.size() > 0 ? View.VISIBLE : View.GONE);

        mStoreWishListAdapter = new StoreWishListAdapter(mWishList,this);
        mStoreWishListRecView.setLayoutManager(layoutManager);
        mStoreWishListRecView.setAdapter(mStoreWishListAdapter);

        mStoreWishListRecView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = layoutManager.findLastVisibleItemPosition();

                if (lastPosition == mWishList.size()-1){
                    mStoreWishListScrollViewImg.setVisibility(View.GONE);
                }else {
                    mStoreWishListScrollViewImg.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    public void setRecentProductListAdapter(ArrayList<DashboardProductsEntity> mRecentProductList){

        mstoreWishRelatedProdTxt.setVisibility(mRecentProductList.size() > 0 ? View.VISIBLE : View.GONE);
        mStoreWishListRelatedProdRecView.setVisibility(mRecentProductList.size() > 0 ? View.VISIBLE : View.GONE);
        mStoreWishListRelatedProdView.setVisibility(mRecentProductList.size() > 0 ? View.VISIBLE : View.GONE);

        mStoreProductListAdapter = new StoreProductListAdapter(this,mRecentProductList,"WishList");
        mStoreWishListRelatedProdRecView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        mStoreWishListRelatedProdRecView.setAdapter(mStoreProductListAdapter);

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof StoreCartResponse) {

            int countQtyCart = 0;
            StoreCartResponse mREsponse = (StoreCartResponse)resObj;

            for (int i=0 ; i<mREsponse.getResult().size(); i++){
                countQtyCart = countQtyCart + mREsponse.getResult().get(i).getQuantity();
            }

            mBottomNotificationTxt.setText(String.valueOf(countQtyCart));
            if (countQtyCart > 0){
                mBottomBadgeLay.setVisibility(View.VISIBLE);
            }else {
                mBottomBadgeLay.setVisibility(View.GONE);

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }
}
