package com.qoltech.mzyoonbuyer.ui;

import android.graphics.RectF;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.PhotoView;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.OrderDetailsCustomizationAdapdter;
import com.qoltech.mzyoonbuyer.adapter.ViewImageAdapter;
import com.qoltech.mzyoonbuyer.entity.GetOrderDetailsCustomizationEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetOrderDetailsCustomizationResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;

import java.util.ArrayList;

public class ViewImageScreen extends BaseActivity {

    public static ImageView myImage;
    RecyclerView material_list_recyclerview,mCustomizationListRecyclerView;
    ViewImageAdapter viewReferenceImagesAdapter;
    OrderDetailsCustomizationAdapdter mOrderDetailsCustomizationAdapter;
    TextView mCustomiztionTxt,mEmptyTxt;
    View mImageLineView;
    RelativeLayout mEmptyLay;
    int i = 0;
    public static PhotoView photo_view;
    RectF rectf;

    TextView mHeaderTxt;

    ImageView mRightSideImg;

    ImageView mHeaderLeftBackBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_view_image_screen);
        myImage = findViewById(R.id.myImage);
        photo_view=findViewById(R.id.photo_view);
        mHeaderTxt = findViewById(R.id.header_txt);
        mRightSideImg = findViewById(R.id.header_right_side_img);
        mHeaderLeftBackBtn = findViewById(R.id.header_left_side_img);
        material_list_recyclerview = findViewById(R.id.material_list_recyclerview);
        mCustomizationListRecyclerView = findViewById(R.id.customization_list_recyclerview);
        mImageLineView = findViewById(R.id.view_img_view);
        mCustomiztionTxt = findViewById(R.id.view_image_customization_txt);
        mEmptyTxt = findViewById(R.id.view_img_emtpy_txt);
        mEmptyLay = findViewById(R.id.view_img_emtpy_lay);

        myImage = findViewById(R.id.myImage);
        rectf=new RectF(0,0,0,0);

        if (AppConstants.ORDER_DETAILS_CUSTOMIZATION.equalsIgnoreCase("CUSTOMIZATION")){
            orderDetailsCustomizationApiCall();
            mCustomiztionTxt.setVisibility(View.VISIBLE);
            mImageLineView.setVisibility(View.GONE);
        }else {
            setImageList();

            mCustomiztionTxt.setVisibility(View.GONE);
            mImageLineView.setVisibility(View.VISIBLE);
            Glide.with(getApplicationContext()).load(AppConstants.VIEW_IMAGE_LIST.get(0))
                    .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.dress_type_empty_img))
                    .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.dress_type_empty_img)).into(photo_view);
        }

        setHeader();

    }

    public void orderDetailsCustomizationApiCall(){
        if (NetworkUtil.isNetworkAvailable(ViewImageScreen.this)){
            APIRequestHandler.getInstance().getOrderDetailsCustomization(AppConstants.REQUEST_LIST_ID,ViewImageScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ViewImageScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    orderDetailsCustomizationApiCall();
                }
            });
        }
    }
    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(AppConstants.VIEW_DETAILS_HEADER);
        mRightSideImg.setVisibility(View.INVISIBLE);

        mHeaderLeftBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
    }
    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetOrderDetailsCustomizationResponse){
            GetOrderDetailsCustomizationResponse mResponse = (GetOrderDetailsCustomizationResponse)resObj;
            setImageCustomizationList(mResponse.getResult().getCustomization());

            if (mResponse.getResult().getCustomization().size() > 0){
                Glide.with(getApplicationContext()).load(AppConstants.IMAGE_BASE_URL+"images/Customazation3/"+mResponse.getResult().getCustomization().get(0).getImages())
                        .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.dress_type_empty_img))
                        .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                                .placeholder(R.drawable.dress_type_empty_img)).into(photo_view);
            }

        }
    }

    private void setImageCustomizationList(ArrayList<GetOrderDetailsCustomizationEntity> customizationList) {

        material_list_recyclerview.setVisibility(View.GONE);
        mEmptyTxt.setVisibility(customizationList.size() > 0 ? View.GONE : View.VISIBLE);
        mCustomizationListRecyclerView.setVisibility(customizationList.size() > 0 ? View.VISIBLE : View.GONE);
        mCustomizationListRecyclerView.setHasFixedSize(true);
        mCustomizationListRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        mOrderDetailsCustomizationAdapter = new OrderDetailsCustomizationAdapdter(this, customizationList);
        mCustomizationListRecyclerView.setAdapter(mOrderDetailsCustomizationAdapter);

    }

    private void setImageList() {
        mCustomizationListRecyclerView.setVisibility(View.GONE);
        material_list_recyclerview.setHasFixedSize(true);
        material_list_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        viewReferenceImagesAdapter = new ViewImageAdapter(this, AppConstants.VIEW_IMAGE_LIST);
        material_list_recyclerview.setAdapter(viewReferenceImagesAdapter);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }

}
