package com.qoltech.mzyoonbuyer.ui;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.AddMaterialAdapter;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.FileUploadResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class AddReferenceScreen extends BaseActivity {

    @BindView(R.id.add_reference_par_lay)
    LinearLayout mAddReferencePayLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    public final int REQUEST_CAMERA = 999;
    public final int REQUEST_GALLERY = 888;

    String IMAGE_PATH = "";
    private static final String IMAGE_DIRECTORY = "/MZYOON";
    private ArrayList<String> mAddImageList;

    private AddMaterialAdapter mAddReferenceAdapter;

    @BindView(R.id.add_refenece_recycler_view)
    RecyclerView mReferenceRecyclerView;

    @BindView(R.id.add_reference_img)
    public ImageView mAddReferenceImg;

    @BindView(R.id.add_reference_add_img)
    ImageView mAddImg;

    @BindView(R.id.new_flow_measurement_wizard_lay)
    RelativeLayout mNewFlowMeasurementWizLay;

    @BindView(R.id.measurement_wizard_lay)
    RelativeLayout mMeasurementWizLay;

    @BindView(R.id.no_imges_shown)
    public TextView mNoImagesTxt;

    @BindView(R.id.add_reference_comment_edt_txt)
    EditText mAddReferenceCommentEdtTxt;

    private UserDetailsEntity mUserDetailsEntityRes;

    Dialog mHintDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_add_reference_screen);

        initView();
    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mAddReferencePayLay);

        setHeader();
        mAddImageList = new ArrayList<>();

        mAddImageList = new ArrayList<>();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(AddReferenceScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")){
            getHintDialog();

        }

    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<String> addMaterialList) {

        mAddReferenceAdapter = new AddMaterialAdapter(this, addMaterialList);
        mReferenceRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mReferenceRecyclerView.setAdapter(mAddReferenceAdapter);
        mReferenceRecyclerView.scrollToPosition(addMaterialList.size()-1);

    }

    @OnClick({R.id.reference_next_btn, R.id.header_left_side_img, R.id.add_reference_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.reference_next_btn:
//                if (mUserDetailsEntityRes.getUSER_ID().equalsIgnoreCase("0")) {
//                    DialogManager.getInstance().showOptionPopup(AddReferenceScreen.this, getResources().getString(R.string.please_login_to_proceed), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
//                        @Override
//                        public void onNegativeClick() {
//
//                        }
//                        @Override
//                        public void onPositiveClick() {
//                            logOut();
//                            HashMap<String,String> productIdist = new HashMap<>();
//                            PreferenceUtil.storeRecentProducts(AddReferenceScreen.this,productIdist);
//                            PreferenceUtil.storeBoolPreferenceValue(getContext(),AppConstants.LOGIN_STATUS,false);
////                            PreferenceUtil.clearPreference(AddReferenceScreen.this);
//                            nextScreen(LoginScreen.class,false);
//
//
//                        }
//                    });
//                }else {
                    AppConstants.ADD_MATERIAL = "ADD_REFERENCE";
                    AppConstants.EDIT_ADDRESS = "";
                    AppConstants.SERVICE_TYPE = "SERVICE_TYPE";
                    uploadProfileImageApiCall();
                    AppConstants.ADD_COMMENTS = mAddReferenceCommentEdtTxt.getText().toString();
                AppConstants.ADDRESS_ON_BACK = "false";
                nextScreen(AddressScreen.class, true);
//                }
                break;

            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.add_reference_lay:
                if (mAddImageList.size() >= 10) {
                    mAddImg.clearAnimation();
                    mAddImg.setAnimation(ShakeErrorUtils.shakeError());
                    Toast.makeText(AddReferenceScreen.this, getResources().getString(R.string.maximum_img_for_add_reference), Toast.LENGTH_SHORT).show();
                } else {
                    if (checkPermission()) {
                        uploadImage();
                    }
                }
                break;
        }

    }

    public void logOut(){

        AppConstants.MEN_1 = "";
        AppConstants.MEN_2 = "";
        AppConstants.MEN_3 = "";
        AppConstants.MEN_4 = "";
        AppConstants.MEN_5 = "";
        AppConstants.MEN_6 = "";
        AppConstants.MEN_7 = "";
        AppConstants.MEN_8 = "";
        AppConstants.MEN_9 = "";
        AppConstants.MEN_10 = "";
        AppConstants.MEN_11 = "";
        AppConstants.MEN_12 = "";
        AppConstants.MEN_13 = "";
        AppConstants.MEN_14 = "";
        AppConstants.MEN_15 = "";
        AppConstants.MEN_16 = "";
        AppConstants.MEN_17 = "";
        AppConstants.MEN_18 = "";
        AppConstants.MEN_19 = "";
        AppConstants.MEN_20 = "";
        AppConstants.MEN_21 = "";
        AppConstants.MEN_22 = "";
        AppConstants.MEN_23 = "";
        AppConstants.MEN_24 = "";
        AppConstants.MEN_25 = "";
        AppConstants.MEN_26 = "";
        AppConstants.MEN_27 = "";
        AppConstants.MEN_28 = "";
        AppConstants.MEN_29 = "";
        AppConstants.MEN_30 = "";
        AppConstants.MEN_31 = "";
        AppConstants.MEN_32 = "";
        AppConstants.MEN_33 = "";
        AppConstants.MEN_34 = "";
        AppConstants.MEN_35 = "";
        AppConstants.MEN_36 = "";
        AppConstants.MEN_37 = "";
        AppConstants.MEN_38 = "";
        AppConstants.MEN_39 = "";
        AppConstants.MEN_40 = "";
        AppConstants.MEN_41 = "";
        AppConstants.MEN_42 = "";
        AppConstants.MEN_43 = "";
        AppConstants.MEN_44 = "";
        AppConstants.MEN_45 = "";
        AppConstants.MEN_46 = "";
        AppConstants.MEN_47 = "";
        AppConstants.MEN_48 = "";
        AppConstants.MEN_49 = "";
        AppConstants.MEN_50 = "";
        AppConstants.MEN_51 = "";
        AppConstants.MEN_52 = "";
        AppConstants.MEN_53 = "";
        AppConstants.MEN_54 = "";
        AppConstants.MEN_55 = "";
        AppConstants.MEN_56 = "";
        AppConstants.MEN_57 = "";
        AppConstants.MEN_58 = "";
        AppConstants.MEN_59 = "";
        AppConstants.MEN_60 = "";
        AppConstants.MEN_61 = "";
        AppConstants.MEN_62 = "";
        AppConstants.MEN_63 = "";
        AppConstants.MEN_64 = "";

        AppConstants.WOMEN_65 = "";
        AppConstants.WOMEN_66 = "";
        AppConstants.WOMEN_67 = "";
        AppConstants.WOMEN_68 = "";
        AppConstants.WOMEN_69 = "";
        AppConstants.WOMEN_70 = "";
        AppConstants.WOMEN_71 = "";
        AppConstants.WOMEN_72 = "";
        AppConstants.WOMEN_73 = "";
        AppConstants.WOMEN_74 = "";
        AppConstants.WOMEN_75 = "";
        AppConstants.WOMEN_76 = "";
        AppConstants.WOMEN_77 = "";
        AppConstants.WOMEN_78 = "";
        AppConstants.WOMEN_79 = "";
        AppConstants.WOMEN_80 = "";
        AppConstants.WOMEN_81 = "";
        AppConstants.WOMEN_82 = "";
        AppConstants.WOMEN_83 = "";
        AppConstants.WOMEN_84 = "";
        AppConstants.WOMEN_85 = "";
        AppConstants.WOMEN_86 = "";
        AppConstants.WOMEN_87 = "";
        AppConstants.WOMEN_88 = "";
        AppConstants.WOMEN_89 = "";
        AppConstants.WOMEN_90 = "";
        AppConstants.WOMEN_91 = "";
        AppConstants.WOMEN_92 = "";
        AppConstants.WOMEN_93 = "";
        AppConstants.WOMEN_94 = "";
        AppConstants.WOMEN_95 = "";
        AppConstants.WOMEN_96 = "";
        AppConstants.WOMEN_97 = "";
        AppConstants.WOMEN_98 = "";
        AppConstants.WOMEN_99 = "";
        AppConstants.WOMEN_100 = "";
        AppConstants.WOMEN_101 = "";
        AppConstants.WOMEN_102 = "";
        AppConstants.WOMEN_103 = "";
        AppConstants.WOMEN_104 = "";
        AppConstants.WOMEN_105 = "";
        AppConstants.WOMEN_106 = "";
        AppConstants.WOMEN_107 = "";
        AppConstants.WOMEN_108 = "";
        AppConstants.WOMEN_109 = "";
        AppConstants.WOMEN_110 = "";
        AppConstants.WOMEN_111 = "";
        AppConstants.WOMEN_112 = "";
        AppConstants.WOMEN_113 = "";
        AppConstants.WOMEN_114 = "";
        AppConstants.WOMEN_115 = "";
        AppConstants.WOMEN_116 = "";
        AppConstants.WOMEN_117 = "";
        AppConstants.WOMEN_118 = "";
        AppConstants.WOMEN_119 = "";
        AppConstants.WOMEN_120 = "";
        AppConstants.WOMEN_121 = "";
        AppConstants.WOMEN_122 = "";
        AppConstants.WOMEN_123 = "";
        AppConstants.WOMEN_124 = "";
        AppConstants.WOMEN_125 = "";
        AppConstants.WOMEN_126 = "";
        AppConstants.WOMEN_127 = "";
        AppConstants.WOMEN_128 = "";
        AppConstants.WOMEN_129 = "";
        AppConstants.WOMEN_130 = "";
        AppConstants.WOMEN_131 = "";
        AppConstants.WOMEN_132 = "";
        AppConstants.WOMEN_133 = "";
        AppConstants.WOMEN_134 = "";
        AppConstants.WOMEN_135 = "";
        AppConstants.WOMEN_136 = "";
        AppConstants.WOMEN_137 = "";
        AppConstants.WOMEN_138 = "";
        AppConstants.WOMEN_139 = "";
        AppConstants.WOMEN_140 = "";
        AppConstants.WOMEN_141 = "";
        AppConstants.WOMEN_142 = "";
        AppConstants.WOMEN_143 = "";
        AppConstants.WOMEN_144 = "";

        AppConstants.BOY_145 = "";
        AppConstants.BOY_146 = "";
        AppConstants.BOY_147 = "";
        AppConstants.BOY_148 = "";
        AppConstants.BOY_149 = "";
        AppConstants.BOY_150 = "";
        AppConstants.BOY_151 = "";
        AppConstants.BOY_152 = "";
        AppConstants.BOY_153 = "";
        AppConstants.BOY_154 = "";
        AppConstants.BOY_155 = "";
        AppConstants.BOY_156 = "";
        AppConstants.BOY_157 = "";
        AppConstants.BOY_158 = "";
        AppConstants.BOY_159 = "";
        AppConstants.BOY_160 = "";
        AppConstants.BOY_161 = "";
        AppConstants.BOY_162 = "";
        AppConstants.BOY_163 = "";
        AppConstants.BOY_164 = "";
        AppConstants.BOY_165 = "";
        AppConstants.BOY_166 = "";
        AppConstants.BOY_167 = "";
        AppConstants.BOY_168 = "";
        AppConstants.BOY_169 = "";
        AppConstants.BOY_170 = "";
        AppConstants.BOY_171 = "";
        AppConstants.BOY_172 = "";
        AppConstants.BOY_173 = "";
        AppConstants.BOY_174 = "";
        AppConstants.BOY_175 = "";
        AppConstants.BOY_176 = "";
        AppConstants.BOY_177 = "";
        AppConstants.BOY_178 = "";
        AppConstants.BOY_179 = "";
        AppConstants.BOY_180 = "";
        AppConstants.BOY_181 = "";
        AppConstants.BOY_182 = "";
        AppConstants.BOY_183 = "";
        AppConstants.BOY_184 = "";
        AppConstants.BOY_185 = "";
        AppConstants.BOY_186 = "";
        AppConstants.BOY_187 = "";
        AppConstants.BOY_188 = "";
        AppConstants.BOY_189 = "";
        AppConstants.BOY_190 = "";
        AppConstants.BOY_191 = "";
        AppConstants.BOY_192 = "";
        AppConstants.BOY_193 = "";
        AppConstants.BOY_194 = "";
        AppConstants.BOY_195 = "";
        AppConstants.BOY_196 = "";
        AppConstants.BOY_197 = "";
        AppConstants.BOY_198 = "";
        AppConstants.BOY_199 = "";
        AppConstants.BOY_200 = "";
        AppConstants.BOY_201 = "";
        AppConstants.BOY_202 = "";
        AppConstants.BOY_203 = "";
        AppConstants.BOY_204 = "";
        AppConstants.BOY_205 = "";
        AppConstants.BOY_206 = "";
        AppConstants.BOY_207 = "";
        AppConstants.BOY_208 = "";

        AppConstants.GIRL_209 = "";
        AppConstants.GIRL_210 = "";
        AppConstants.GIRL_211 = "";
        AppConstants.GIRL_212 = "";
        AppConstants.GIRL_213 = "";
        AppConstants.GIRL_214 = "";
        AppConstants.GIRL_215 = "";
        AppConstants.GIRL_216 = "";
        AppConstants.GIRL_217 = "";
        AppConstants.GIRL_218 = "";
        AppConstants.GIRL_219 = "";
        AppConstants.GIRL_220 = "";
        AppConstants.GIRL_221 = "";
        AppConstants.GIRL_222 = "";
        AppConstants.GIRL_223 = "";
        AppConstants.GIRL_224 = "";
        AppConstants.GIRL_225 = "";
        AppConstants.GIRL_226 = "";
        AppConstants.GIRL_227 = "";
        AppConstants.GIRL_228 = "";
        AppConstants.GIRL_229 = "";
        AppConstants.GIRL_230 = "";
        AppConstants.GIRL_231 = "";
        AppConstants.GIRL_232 = "";
        AppConstants.GIRL_233 = "";
        AppConstants.GIRL_234 = "";
        AppConstants.GIRL_235 = "";
        AppConstants.GIRL_236 = "";
        AppConstants.GIRL_237 = "";
        AppConstants.GIRL_238 = "";
        AppConstants.GIRL_239 = "";
        AppConstants.GIRL_240 = "";
        AppConstants.GIRL_241 = "";
        AppConstants.GIRL_242 = "";
        AppConstants.GIRL_243 = "";
        AppConstants.GIRL_244 = "";
        AppConstants.GIRL_245 = "";
        AppConstants.GIRL_246 = "";
        AppConstants.GIRL_247 = "";
        AppConstants.GIRL_248 = "";
        AppConstants.GIRL_249 = "";
        AppConstants.GIRL_250 = "";
        AppConstants.GIRL_251 = "";
        AppConstants.GIRL_252 = "";
        AppConstants.GIRL_253 = "";
        AppConstants.GIRL_254 = "";
        AppConstants.GIRL_255 = "";
        AppConstants.GIRL_256 = "";
        AppConstants.GIRL_257 = "";
        AppConstants.GIRL_258 = "";
        AppConstants.GIRL_259 = "";
        AppConstants.GIRL_260 = "";
        AppConstants.GIRL_261 = "";
        AppConstants.GIRL_262 = "";
        AppConstants.GIRL_263 = "";
        AppConstants.GIRL_264 = "";
        AppConstants.GIRL_265 = "";
        AppConstants.GIRL_266 = "";
        AppConstants.GIRL_267 = "";
        AppConstants.GIRL_268 = "";
        AppConstants.GIRL_269 = "";
        AppConstants.GIRL_270 = "";
        AppConstants.GIRL_271 = "";
        AppConstants.GIRL_272 = "";
        AppConstants.GIRL_273 = "";
        AppConstants.GIRL_274 = "";
        AppConstants.GIRL_275 = "";
        AppConstants.GIRL_276 = "";
        AppConstants.GIRL_277 = "";
        AppConstants.GIRL_278 = "";
        AppConstants.GIRL_279 = "";
        AppConstants.GIRL_280 = "";
        AppConstants.GIRL_281 = "";
        AppConstants.GIRL_282 = "";
        AppConstants.GIRL_283 = "";
        AppConstants.GIRL_284 = "";
        AppConstants.GIRL_285 = "";
        AppConstants.GIRL_286 = "";
        AppConstants.GIRL_287 = "";
        AppConstants.GIRL_288 = "";

        AppConstants.MEASUREMENT_VALUES = new ArrayList<>();

        AppConstants.MEASUREMENT_ID = "";

        AppConstants.MEASUREMENT_MANUALLY = "";

        AppConstants.MEASUREMENT_MAP = new HashMap<>();

        AppConstants.PATTERN_ID = "";

        AppConstants.SEASONAL_NAME = "";
        AppConstants.PLACE_OF_INDUSTRY_NAME = "";
        AppConstants.BRANDS_NAME = "";
        AppConstants.MATERIAL_TYPE_NAME = "";
        AppConstants.COLOUR_NAME = "";
        AppConstants.PATTERN_NAME = "";

        AppConstants.GET_ADDRESS_ID = "";

        AppConstants.PATTERN_ID = "";

        AppConstants.ORDER_TYPE_ID  = "" ;

        AppConstants.MEASUREMENT_ID = "";

        AppConstants.MATERIAL_IMAGES = new ArrayList<>();

        AppConstants.REFERENCE_IMAGES = new ArrayList<>();

        AppConstants.CUSTOMIZATION_CLICK_ARRAY = new ArrayList<>();

        AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();

        AppConstants.CUSTOMIZATION_CLICKED_ARRAY = new ArrayList<>();

        AppConstants.DELIVERY_TYPE_ID = "";

        AppConstants.MEASUREMENT_TYPE = "";

        AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST = new ArrayList<>();
    }

    public void getHintDialog(){
        alertDismiss(mHintDialog);
        mHintDialog = getDialog(AddReferenceScreen.this, R.layout.pop_up_add_material_hint);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mHintDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.add_material_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);


        }else {
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.add_material_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        Button hintSkipBtn;
        TextView mAddMaterialTxt,mAddMaterialHeaderTxt;

        hintSkipBtn = mHintDialog.findViewById(R.id.next_hint_btn);
        mAddMaterialTxt = mHintDialog.findViewById(R.id.material_hint_txt);
        mAddMaterialHeaderTxt = mHintDialog.findViewById(R.id.add_material_pop_up_header_txt);

        /*Set data*/
        mAddMaterialHeaderTxt.setText(getResources().getString(R.string.reference));
        mAddMaterialTxt.setText(getResources().getString(R.string.add_ref_hint));
        hintSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHintDialog.dismiss();
            }
        });

        alertShowing(mHintDialog);
    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.add_reference));
        mRightSideImg.setVisibility(View.INVISIBLE);

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            mMeasurementWizLay.setVisibility(View.GONE);
            mNewFlowMeasurementWizLay.setVisibility(View.VISIBLE);
        }else {
            mMeasurementWizLay.setVisibility(View.VISIBLE);
            mNewFlowMeasurementWizLay.setVisibility(View.GONE);
        }
    }

    private void uploadImage() {
        AppConstants.ADD_MATERIAL = "ADD_REFERENCE";

        DialogManager.getInstance().showImageUploadPopup(this,getResources().getString(R.string.select_phot_type), getResources().getString(R.string.take_camera), getResources().getString(R.string.open_gallery), new InterfaceTwoBtnCallBack() {
            @Override
            public void onNegativeClick() {
                captureImage();
            }

            @Override
            public void onPositiveClick() {
                galleryImage();
            }
        });
    }

    /*open camera Image*/
    private void captureImage() {

        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);

    }

    /*open gallery Image*/
    private void galleryImage() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, REQUEST_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            /*Open Camera Request Check*/
            case REQUEST_CAMERA:
                if (resultCode == RESULT_OK) {

                    Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//                    String path = saveImage(thumbnail);
//                    IMAGE_PATH = path;
//                    IMAGE_PATH = path;
//                    saveImage(thumbnail);

                    mAddImageList.add(compressImage(String.valueOf(getImageUri(getApplicationContext(),thumbnail))));

                    setAdapter(mAddImageList);

                    mAddReferenceAdapter.notifyDataSetChanged();

                } else {
                    if (resultCode == RESULT_CANCELED) {
                        /*Cancelling the image capture process by the user*/
                    } else {
                        /*image capture getting failed due to certail technical issues*/
                    }
                }
                break;
            /*Open Photo Gallery Request Check*/
            case REQUEST_GALLERY:
                if (resultCode == RESULT_OK) {
                    // successfully captured the image
                    // display it in image view

                    Uri contentURI = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), contentURI);
//                        String path = saveImage(bitmap);
//                        IMAGE_PATH = path;

                        mAddImageList.add(compressImage(String.valueOf(contentURI)));

                        setAdapter(mAddImageList);

                        mAddReferenceAdapter.notifyDataSetChanged();

                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "I Can't find the location this file!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    if (resultCode == RESULT_CANCELED) {
                        /*Cancelling the image capture process by the user*/

                    } else {
                        /*image capture getting failed due to certail technical issues*/
                    }
                }
                break;

        }
    }
//    public String saveImage(Bitmap myBitmap) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
//        File wallpaperDirectory = new File(
//                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
//        // have the object build the directory structure, if needed.
//        if (!wallpaperDirectory.exists()) {
//            if (checkPermission() == true) {
//                wallpaperDirectory.mkdirs();
//
//            }
//        }
//
//        try {
//            File f = new File(wallpaperDirectory, Calendar.getInstance()
//                    .getTimeInMillis() + ".jpg");
//            f.createNewFile();
//            FileOutputStream fo = new FileOutputStream(f);
//            fo.write(bytes.toByteArray());
//            MediaScannerConnection.scanFile(getApplicationContext(),
//                    new String[]{f.getPath()},
//                    new String[]{"image/jpeg"}, null);
//            fo.close();
//            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());
//
//            return f.getAbsolutePath();
//        } catch (IOException e1) {
//            e1.printStackTrace();
//        }
//        return "";
//    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
            actualWidth = (int) (imgRatio * actualWidth);
            actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;

        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height/ (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;      }
            final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    /* Ask for permission on Camera access*/
    private boolean checkPermission() {

        boolean addPermission = true;
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            int cameraPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
            int readStoragePermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
            int storagePermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (storagePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            addPermission = askAccessPermission(listPermissionsNeeded, 1, new InterfaceTwoBtnCallBack() {
                @Override
                public void onNegativeClick() {

                }


                @Override
                public void onPositiveClick() {
                    uploadImage();
                }

            });
        }

        return addPermission;

    }

    public void uploadProfileImageApiCall(){
        if (NetworkUtil.isNetworkAvailable(AddReferenceScreen.this)){
            if (mAddImageList.size()>0) {

                APIRequestHandler.getInstance().uploadMultiFile(mAddImageList,AddReferenceScreen.this,"ReferenceImages");

            }
        }else {
            uploadProfileImageApiCall();
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof FileUploadResponse){
            FileUploadResponse mResponse = (FileUploadResponse)resObj;
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success") && mResponse.getResult().size()>0){

                for (int i=0; i<mResponse.getResult().size(); i++){
                    String[] parts = mResponse.getResult().get(i).split("\\\\");
                    String part1 = parts[0]; // 004
                    String part2 = parts[1];
                    String lastOne = parts[parts.length - 1];

                    AppConstants.REFERENCE_IMAGES.add(lastOne) ;

                }
            }
        }
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.add_reference_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
//            updateResources(AddReferenceScreen.this,"ar");

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.add_reference_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            updateResources(AddReferenceScreen.this,"en");

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }
}
