package com.qoltech.mzyoonbuyer.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.PaymentGatewayResponse;
import com.qoltech.mzyoonbuyer.entity.PaymentGatewayTransactionResponse;
import com.qoltech.mzyoonbuyer.entity.PaymentInfoEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.ApplyPromoCodeResponse;
import com.qoltech.mzyoonbuyer.modal.ConvertCashPointResponse;
import com.qoltech.mzyoonbuyer.modal.GetPaymentStoreModal;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import butterknife.ButterKnife;
import fr.arnaudguyon.xmltojsonlib.XmlToJson;
import im.delight.android.webview.AdvancedWebView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PaymentWebviewScreen extends BaseActivity  implements AdvancedWebView.Listener {

    private UserDetailsEntity mUserDetailsEntityRes;

    private PaymentInfoEntity mPaymentInforRes;

    private Handler handler = new Handler(Looper.getMainLooper());

    String url = "",mStoreId = "" ,mKeyId =  "",mCodeStr = "",mEmailStr = "",mFirstNameStr = "",mSecondNameStr = "";

    private AdvancedWebView mWebView;

    String mBackHandlerStr = "false";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_payment_webview_screen);

        initView();
    }
    public void initView(){
        ButterKnife.bind(this);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(PaymentWebviewScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();
        getPaymentStoreApiCall();

        mPaymentInforRes = new PaymentInfoEntity();
        mPaymentInforRes = AppConstants.STORE_USER_DETAILS;

        mEmailStr = mPaymentInforRes.getEmail().equalsIgnoreCase("") ? mUserDetailsEntityRes.getEMAIL() : mPaymentInforRes.getEmail();

        if (mPaymentInforRes.getCountry().equalsIgnoreCase("UNITED ARAB EMIRATES")){
            mPaymentInforRes.setCountry("AE");
        } else if (mPaymentInforRes.getCountry().equalsIgnoreCase("India")){
            mPaymentInforRes.setCountry("IN");
        } else if (mPaymentInforRes.getCountry().equalsIgnoreCase("United States")){
            mPaymentInforRes.setCountry("US");
        }

        getPaymentApiCall();

        mWebView = (AdvancedWebView) findViewById(R.id.webview);
        mWebView.setListener(PaymentWebviewScreen.this, this);
        mWebView.loadUrl(url);
        WebSettings webSetting = mWebView.getSettings();
        webSetting.setUserAgentString("Mobile Safari/537.16");
        webSetting.setAllowFileAccessFromFileURLs(true);
        webSetting.setAllowUniversalAccessFromFileURLs(true);
        mWebView.clearCache(true);
        mWebView.clearHistory();
        webSetting.setAllowContentAccess(true);
        webSetting.setDomStorageEnabled(true);
        webSetting.setJavaScriptEnabled(true);
        webSetting.setBuiltInZoomControls(true);
        webSetting.setSupportZoom(true);
        webSetting.setLoadWithOverviewMode(true);
        webSetting.setUseWideViewPort(true);

        webSetting.setDisplayZoomControls(false);

        mWebView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                WebView.HitTestResult hr = ((WebView)v).getHitTestResult();

                return false;
            }
        });
    }

    public void getPaymentStoreApiCall(){
        if (NetworkUtil.isNetworkAvailable(PaymentWebviewScreen.this)){
            APIRequestHandler.getInstance().getPaymentStoreApi(PaymentWebviewScreen.this);
        }else {
            getPaymentStoreApiCall();
        }
    }

    public void getPaymentApiCall(){
        if (NetworkUtil.isNetworkAvailable(PaymentWebviewScreen.this)){
            DialogManager.getInstance().showProgress(PaymentWebviewScreen.this);
            Random rnd = new Random();
            int number = rnd.nextInt(999999);

            String RandomNum = String.format(Locale.ENGLISH,"%06d", number);

            AppConstants.DEVICE_ID = AppConstants.DEVICE_ID.equalsIgnoreCase("") ? "12345" : AppConstants.DEVICE_ID;
            mStoreId = mStoreId.equalsIgnoreCase("") ? "21552" : mStoreId;
            mKeyId = mKeyId.equalsIgnoreCase("") ? "XZCQ~9wRvD^prrJx" : mKeyId;

            String soap_string = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                    "<mobile>" +
                    "<store>"+ mStoreId+"</store>" +
                    "<key>"+mKeyId+"</key>" +
                    "<device>" +
                    "<type>"+"Android"+"</type>" +
                    "<id>"+AppConstants.DEVICE_ID+"</id>" +
                    "<agent>(DeviceAgent!)</agent>" +
                    "<accept>(DeviceAccept!)</accept>" +
                    "</device>" +
                    "<app>" +
                    "<name>"+"MZYOON"+"</name>" +
                    "<version>(AppVersion!)</version>" +
                    "<user>(AppUser!)</user>" +
                    "<id>"+mUserDetailsEntityRes.getUSER_ID()+"</id>" +
                    "</app>" +
                    "<tran>" +
                    "<test>1</test>" +
                    "<type>PayPage</type>" +
                    "<class>cont</class>" +
                    "<cartid>"+RandomNum+" - MZYOON - "+AppConstants.REQUEST_LIST_ID+"</cartid>" +
                    "<description>"+"Mzyoon Application Android"+"</description>" +
                    "<currency>AED</currency>" +
                    "<amount>"+AppConstants.TRANSACTION_AMOUNT+"</amount>" +
                    "<ref>0000000000001</ref>" +
                    "</tran>" +
                    "<card>" +
                    "<number>(CardNum!)</number>" +
                    "<expiry>" +
                    "<month>(ExpMonth!)</month>" +
                    "<year>(ExpYear!)</year>" +
                    "</expiry>" +
                    "<cvv>(CVVNum!)</cvv>" +
                    "</card>" +
                    "<billing>" +
                    "<name>" +
                    "<title></title>"+
                    "<first>"+mPaymentInforRes.getFirstName()+"</first>" +
                    "<last>"+mPaymentInforRes.getSecondName()+"</last>" +
                    "</name>" +
                    "<address>" +
                    "<line1>"+mPaymentInforRes.getLineOne()+"</line1>" +
                    "<line2>"+mPaymentInforRes.getLineTwo()+"</line2>" +
                    "<line3>"+mPaymentInforRes.getLineThree()+"</line3>" +
                    "<city>"+mPaymentInforRes.getCity()+"</city>" +
                    "<region>"+mPaymentInforRes.getRegion()+"</region>" +
                    "<country>"+mPaymentInforRes.getCountry()+"</country>" +
                    "<zip>"+""+"</zip>" +
                    "</address>" +
                    "<email>"+mEmailStr+"</email>" +
                    "</billing>" +
                    "</mobile>";


            final MediaType SOAP_MEDIA_TYPE = MediaType.parse("application/xml");
            final OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(SOAP_MEDIA_TYPE, soap_string);

            final Request request = new Request.Builder()
                    .url("https://secure.innovatepayments.com/gateway/mobile.xml")
                    .post(body)
                    .addHeader("Content-Type", "application/xml")
                    .build();


            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    DialogManager.getInstance().hideProgress();
                    String mMessage = e.getMessage().toString();
                    Log.w("failure Response", mMessage);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    String mMessage = response.body().string();

                    getResponse(mMessage, response);

                }
            });

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(PaymentWebviewScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getPaymentApiCall();
                }
            });
        }
    }
    public void getResponse(String response, Response mainRes) {

        XmlToJson xmlToJson = new XmlToJson.Builder(response).build();
        JSONObject jsonObject = xmlToJson.toJson();

        DialogManager.getInstance().hideProgress();

        Gson gson = new Gson();
        PaymentGatewayResponse paymentGatewayResponse = gson.fromJson(jsonObject.toString(), PaymentGatewayResponse.class);

        handler.post(new Runnable() {
            @Override
            public void run() {

                if (paymentGatewayResponse.getMobile().getWebview()!=null){

                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.loading_plz_wait), Toast.LENGTH_SHORT).show();
                    mWebView.loadUrl(paymentGatewayResponse.getMobile().getWebview().getStart());
                    mCodeStr = paymentGatewayResponse.getMobile().getWebview().getCode();
                }else {
                    if (AppConstants.TRANSACTION_AMOUNT.equalsIgnoreCase("0")){
                        DialogManager.getInstance().showAlertPopup(PaymentWebviewScreen.this, "Payment amount is 0! Are you sure you want to pay?",new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                AppConstants.CART_SCREEN_CHECK = "";
                                AppConstants.TRANSACTION_AMOUNT = "1";
                                getPaymentApiCall();
                            }
                        });
                    }
                    else {
                        DialogManager.getInstance().showPaymentFailurePopup(PaymentWebviewScreen.this,mUserDetailsEntityRes.getLanguage(),new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                mBackHandlerStr = "true";
//                                onBackPressed();
                                AppConstants.CART_SCREEN_CHECK = "";
                                previousScreen(BottomNavigationScreen.class,true);

                            }
                        });
                    }

                }
            }
        });

    }

    public void getPaymentResponseApiCall(){
        if (NetworkUtil.isNetworkAvailable(PaymentWebviewScreen.this)){
            DialogManager.getInstance().showProgress(PaymentWebviewScreen.this);

            AppConstants.DEVICE_ID = AppConstants.DEVICE_ID.equalsIgnoreCase("") ? "12345" : AppConstants.DEVICE_ID;
            mStoreId = mStoreId.equalsIgnoreCase("") ? "21552" : mStoreId;
            mKeyId = mKeyId.equalsIgnoreCase("") ? "XZCQ~9wRvD^prrJx" : mKeyId;

            String soap_string = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                    "<mobile>" +
                    "  <store>"+mStoreId+"</store>" +
                    "  <key>"+mKeyId+"</key>" +
                    "  <complete>"+mCodeStr+"</complete>" +
                    "</mobile>";


            final MediaType SOAP_MEDIA_TYPE = MediaType.parse("application/xml");
            final OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(SOAP_MEDIA_TYPE, soap_string);

            final Request request = new Request.Builder()
                    .url("https://secure.innovatepayments.com/gateway/mobile_complete.xml")
                    .post(body)
                    .addHeader("Content-Type", "application/xml")
                    .build();


            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    DialogManager.getInstance().hideProgress();
                    String mMessage = e.getMessage().toString();
                    Log.w("failure Response", mMessage);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    DialogManager.getInstance().hideProgress();

                    String mMessage = response.body().string();

                    XmlToJson xmlToJson = new XmlToJson.Builder(mMessage).build();
                    JSONObject jsonObject = xmlToJson.toJson();

                    DialogManager.getInstance().hideProgress();

                    Gson gson = new Gson();
                    PaymentGatewayTransactionResponse paymentGatewayTransactionResponse = gson.fromJson(jsonObject.toString(), PaymentGatewayTransactionResponse.class);

                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                    if (paymentGatewayTransactionResponse.getMobile().getAuth().getStatus().equalsIgnoreCase("A")){
                        if (AppConstants.STORE_PAYMENT.equalsIgnoreCase("STORE_PAYMENT")){
//                            if (!AppConstants.SERVICE_TYPE.equalsIgnoreCase("CART")){
                                insertStorePaymentApi(paymentGatewayTransactionResponse.getMobile().getAuth().getTranref(),paymentGatewayTransactionResponse.getMobile().getAuth().getStatus(), paymentGatewayTransactionResponse.getMobile().getAuth().getCode(), paymentGatewayTransactionResponse.getMobile().getAuth().getMessage(), paymentGatewayTransactionResponse.getMobile().getAuth().getCvv(),paymentGatewayTransactionResponse.getMobile().getAuth().getAvs(),paymentGatewayTransactionResponse.getMobile().getAuth().getCardcode(),paymentGatewayTransactionResponse.getMobile().getAuth().getCardlast4(),paymentGatewayTransactionResponse.getMobile().getTrace(),paymentGatewayTransactionResponse.getMobile().getAuth().getCa_valid());

//                            }
                        }else {
                            paymentStatus();
                            afterPaymentStatus();
                            if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER") || AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
                                tailorApprovedApiCall();
                            }
                            if (AppConstants.InitiatedBy.equalsIgnoreCase("Tailor")){
                                updateTailorAmount(paymentGatewayTransactionResponse.getMobile().getAuth().getTranref());
                            }
                            insertPaymentStatus(paymentGatewayTransactionResponse.getMobile().getAuth().getTranref(),paymentGatewayTransactionResponse.getMobile().getAuth().getStatus(),paymentGatewayTransactionResponse.getMobile().getAuth().getCode(),paymentGatewayTransactionResponse.getMobile().getAuth().getMessage(),paymentGatewayTransactionResponse.getMobile().getAuth().getCvv(),paymentGatewayTransactionResponse.getMobile().getAuth().getAvs(),paymentGatewayTransactionResponse.getMobile().getAuth().getCardcode(),paymentGatewayTransactionResponse.getMobile().getAuth().getCardlast4(),paymentGatewayTransactionResponse.getMobile().getTrace(),paymentGatewayTransactionResponse.getMobile().getAuth().getCa_valid());
                        }

                        Date c = Calendar.getInstance().getTime();

                        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                        String formattedDate = df.format(c);
                        DialogManager.getInstance().showPaymentSuccessPopup(PaymentWebviewScreen.this,AppConstants.REQUEST_LIST_ID,AppConstants.TRANSACTION_AMOUNT,paymentGatewayTransactionResponse.getMobile().getAuth().getTranref(), formattedDate,getResources().getString(R.string.receipt_sent_to_email)+" "+mUserDetailsEntityRes.getEMAIL(),mUserDetailsEntityRes.getLanguage(), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                AppConstants.CART_SCREEN_CHECK = "";
                                AppConstants.DIRECT_ORDER_TYPE = "";
                                AppConstants.DIRECT_MEASUREMENT_TYPE = "";
                                nextScreen(BottomNavigationScreen.class,true);

                            }
                        });
                    }else if (paymentGatewayTransactionResponse.getMobile().getAuth().getStatus().equalsIgnoreCase("H")){

                        if (AppConstants.STORE_PAYMENT.equalsIgnoreCase("STORE_PAYMENT")) {
//                            if (!AppConstants.SERVICE_TYPE.equalsIgnoreCase("CART")){
                                insertStorePaymentApi(paymentGatewayTransactionResponse.getMobile().getAuth().getTranref(),paymentGatewayTransactionResponse.getMobile().getAuth().getStatus(), paymentGatewayTransactionResponse.getMobile().getAuth().getCode(), paymentGatewayTransactionResponse.getMobile().getAuth().getMessage(), paymentGatewayTransactionResponse.getMobile().getAuth().getCvv(),paymentGatewayTransactionResponse.getMobile().getAuth().getAvs(),paymentGatewayTransactionResponse.getMobile().getAuth().getCardcode(),paymentGatewayTransactionResponse.getMobile().getAuth().getCardlast4(),paymentGatewayTransactionResponse.getMobile().getTrace(),paymentGatewayTransactionResponse.getMobile().getAuth().getCa_valid());

//                            }
                        }else {
                            paymentStatus();
                            afterPaymentStatus();
                            if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER") || AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
                                tailorApprovedApiCall();
                            }
                            if (!AppConstants.InitiatedBy.equalsIgnoreCase("Tailor")){
                                updateTailorAmount(paymentGatewayTransactionResponse.getMobile().getAuth().getTranref());
                            }
                            insertPaymentStatus(paymentGatewayTransactionResponse.getMobile().getAuth().getTranref(),paymentGatewayTransactionResponse.getMobile().getAuth().getStatus(), paymentGatewayTransactionResponse.getMobile().getAuth().getCode(), paymentGatewayTransactionResponse.getMobile().getAuth().getMessage(), paymentGatewayTransactionResponse.getMobile().getAuth().getCvv(),paymentGatewayTransactionResponse.getMobile().getAuth().getAvs(),paymentGatewayTransactionResponse.getMobile().getAuth().getCardcode(),paymentGatewayTransactionResponse.getMobile().getAuth().getCardlast4(),paymentGatewayTransactionResponse.getMobile().getTrace(),paymentGatewayTransactionResponse.getMobile().getAuth().getCa_valid());
                        }

                        DialogManager.getInstance().showAlertPopup(PaymentWebviewScreen.this, getResources().getString(R.string.payment_inprogress), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                AppConstants.CART_SCREEN_CHECK = "";
                                AppConstants.DIRECT_ORDER_TYPE = "";
                                AppConstants.DIRECT_MEASUREMENT_TYPE = "";
                                nextScreen(BottomNavigationScreen.class,true);

                            }
                        });
                    }

                    else if (paymentGatewayTransactionResponse.getMobile().getAuth().getStatus().equalsIgnoreCase("D")) {
                        DialogManager.getInstance().showPaymentFailurePopup(PaymentWebviewScreen.this,mUserDetailsEntityRes.getLanguage(),new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                AppConstants.CART_SCREEN_CHECK = "";
                                mBackHandlerStr = "true";
                                nextScreen(BottomNavigationScreen.class,true);


                            }
                        });
                    }
                    else if (paymentGatewayTransactionResponse.getMobile().getAuth().getStatus().equalsIgnoreCase("C")) {
                        DialogManager.getInstance().showPaymentFailurePopup(PaymentWebviewScreen.this,mUserDetailsEntityRes.getLanguage(),new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                AppConstants.CART_SCREEN_CHECK = "";
                                mBackHandlerStr = "true";
                                nextScreen(BottomNavigationScreen.class,true);

                            }
                        });
                    }
                    else if (paymentGatewayTransactionResponse.getMobile().getAuth().getStatus().equalsIgnoreCase("E")) {
                        DialogManager.getInstance().showPaymentFailurePopup(PaymentWebviewScreen.this,mUserDetailsEntityRes.getLanguage(),new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                AppConstants.CART_SCREEN_CHECK = "";
                                mBackHandlerStr = "true";
                                nextScreen(BottomNavigationScreen.class,true);

                            }
                        });
                    }
                    else if (paymentGatewayTransactionResponse.getMobile().getAuth().getStatus().equalsIgnoreCase("X")) {
                        DialogManager.getInstance().showPaymentFailurePopup(PaymentWebviewScreen.this,mUserDetailsEntityRes.getLanguage(),new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                AppConstants.CART_SCREEN_CHECK = "";
                                mBackHandlerStr = "true";
                                nextScreen(BottomNavigationScreen.class,true);

                            }
                        });
                    }

                    }

                    });
                }
            });

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(PaymentWebviewScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getPaymentApiCall();
                }
            });
        }
    }
    public void paymentStatus(){
        if (NetworkUtil.isNetworkAvailable(PaymentWebviewScreen.this)){
            APIRequestHandler.getInstance().payementStatusApiCall("1",AppConstants.REQUEST_LIST_ID,PaymentWebviewScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(PaymentWebviewScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    paymentStatus();
                }
            });
        }
    }
    public void afterPaymentStatus(){
        if (NetworkUtil.isNetworkAvailable(PaymentWebviewScreen.this)){
            APIRequestHandler.getInstance().afterPaymentApiCall(AppConstants.APPROVED_TAILOR_ID,AppConstants.REQUEST_LIST_ID,PaymentWebviewScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(PaymentWebviewScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    afterPaymentStatus();
                }
            });
        }
    }

    public void tailorApprovedApiCall(){
        if (NetworkUtil.isNetworkAvailable(PaymentWebviewScreen.this)){
            APIRequestHandler.getInstance().tailorApporvedApiCall(AppConstants.REQUEST_LIST_ID,AppConstants.APPROVED_TAILOR_ID,"1",PaymentWebviewScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(PaymentWebviewScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    tailorApprovedApiCall();
                }
            });
        }
    }

    public void insertPaymentStatus(String tranref, String status, String code, String message, String cvv,String avs, String cardcode, String cardlast4, String trace, String ca_valid){
        if (NetworkUtil.isNetworkAvailable(PaymentWebviewScreen.this)){
            String applycode = "0";
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat formatter = (DecimalFormat)nf;
            formatter.applyPattern("######.##");
            if (!AppConstants.SUB_TOTAL_CASH.equalsIgnoreCase("")){
                applycode = String.valueOf(Double.parseDouble(formatter.format(Double.parseDouble(AppConstants.SUB_TOTAL_CASH) - Double.parseDouble(AppConstants.REDEEM_POINT_TO_CASH))));
           }else {
                AppConstants.SUB_TOTAL_CASH = "0";
            }

            if (!AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")&&!AppConstants.APPLY_CODE.equalsIgnoreCase("")){
                APIRequestHandler.getInstance().insertPaymentStatusApiCall(AppConstants.REQUEST_LIST_ID,tranref,AppConstants.TRANSACTION_AMOUNT,status,code,message,cvv,avs,cardcode,cardlast4,trace,ca_valid,AppConstants.REDEEM_POINT_TO_POINTS,AppConstants.SUB_TOTAL_CASH,AppConstants.REDEEM_POINT_TO_CASH,AppConstants.TRANSACTION_AMOUNT,AppConstants.APPLY_CODE_AMT,Long.parseLong(AppConstants.APPLY_CODE_ID),AppConstants.STICTHING_AMT,AppConstants.STORE_AMT,PaymentWebviewScreen.this);

            }else if (!AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")&&AppConstants.APPLY_CODE.equalsIgnoreCase("")){

                APIRequestHandler.getInstance().insertPaymentStatusApiCall(AppConstants.REQUEST_LIST_ID,tranref,AppConstants.TRANSACTION_AMOUNT,status,code,message,cvv,avs,cardcode,cardlast4,trace,ca_valid,AppConstants.REDEEM_POINT_TO_POINTS,AppConstants.SUB_TOTAL_CASH,AppConstants.REDEEM_POINT_TO_CASH,AppConstants.TRANSACTION_AMOUNT,String.valueOf(0),Long.parseLong("0"),AppConstants.STICTHING_AMT,AppConstants.STORE_AMT,PaymentWebviewScreen.this);
            }else if (AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")&&!AppConstants.APPLY_CODE.equalsIgnoreCase("")){

                APIRequestHandler.getInstance().insertPaymentStatusApiCall(AppConstants.REQUEST_LIST_ID,tranref,AppConstants.TRANSACTION_AMOUNT,status,code,message,cvv,avs,cardcode,cardlast4,trace,ca_valid,String.valueOf(0),AppConstants.SUB_TOTAL_CASH,String.valueOf(0),AppConstants.TRANSACTION_AMOUNT,AppConstants.APPLY_CODE_AMT,Long.parseLong(AppConstants.APPLY_CODE_ID),AppConstants.STICTHING_AMT,AppConstants.STORE_AMT,PaymentWebviewScreen.this);
            }else if (AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")&&AppConstants.APPLY_CODE.equalsIgnoreCase("")){

                APIRequestHandler.getInstance().insertPaymentStatusApiCall(AppConstants.REQUEST_LIST_ID,tranref,AppConstants.TRANSACTION_AMOUNT,status,code,message,cvv,avs,cardcode,cardlast4,trace,ca_valid,String.valueOf(0),AppConstants.TRANSACTION_AMOUNT,String.valueOf(0),AppConstants.TRANSACTION_AMOUNT,String.valueOf(0),Long.parseLong("0"),AppConstants.STICTHING_AMT,AppConstants.STORE_AMT,PaymentWebviewScreen.this);
            }

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(PaymentWebviewScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {

                }
            });
        }
    }

    public void convertPointCashApplyApiCall(String balanceAmt){
        if (NetworkUtil.isNetworkAvailable(PaymentWebviewScreen.this)){
            APIRequestHandler.getInstance().applyConvertedPointsApiCall(mUserDetailsEntityRes.getUSER_ID(),Integer.parseInt(AppConstants.REDEEM_POINT_TO_POINTS),AppConstants.REQUEST_LIST_ID,Double.parseDouble(balanceAmt),PaymentWebviewScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(PaymentWebviewScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    convertPointCashApplyApiCall(balanceAmt);
                }
            });
        }
    }

    public void applyPromoCodeApiCall(String balanceAmt){
        if (NetworkUtil.isNetworkAvailable(PaymentWebviewScreen.this)){
            APIRequestHandler.getInstance().applyCouponPromoCodeApiCall(Integer.parseInt(AppConstants.APPLY_CODE_ID),AppConstants.APPROVED_TAILOR_ID,AppConstants.DRESS_TYPE_ID,Double.parseDouble(balanceAmt),PaymentWebviewScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(PaymentWebviewScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    applyPromoCodeApiCall(balanceAmt);
                }
            });
        }
    }

    public void insertStorePaymentApi(String tranref, String status, String code, String message, String cvv,String avs, String cardcode, String cardlast4, String trace, String ca_valid){
        if (NetworkUtil.isNetworkAvailable(PaymentWebviewScreen.this)){
            String applycode = "0";
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat formatter = (DecimalFormat)nf;
            formatter.applyPattern("######.##");
            if (!AppConstants.SUB_TOTAL_CASH.equalsIgnoreCase("")){
                AppConstants.STORE_AMT = AppConstants.SUB_TOTAL_CASH;
                applycode = String.valueOf(Double.parseDouble(formatter.format(Double.parseDouble(AppConstants.SUB_TOTAL_CASH) - Double.parseDouble(AppConstants.REDEEM_POINT_TO_CASH))));
            }else {
                AppConstants.SUB_TOTAL_CASH = "0";
                AppConstants.STORE_AMT = AppConstants.TRANSACTION_AMOUNT;
            }

            if (!AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")&&!AppConstants.APPLY_CODE.equalsIgnoreCase("")){
                APIRequestHandler.getInstance().insertStorePaymentApiCall(AppConstants.REQUEST_LIST_ID,tranref,AppConstants.TRANSACTION_AMOUNT,status,code,message,cvv,avs,cardcode,cardlast4,trace,ca_valid,AppConstants.REDEEM_POINT_TO_POINTS,AppConstants.SUB_TOTAL_CASH,AppConstants.REDEEM_POINT_TO_CASH,AppConstants.TRANSACTION_AMOUNT,AppConstants.APPLY_CODE_AMT,AppConstants.APPLY_CODE_ID,AppConstants.STORE_AMT,PaymentWebviewScreen.this);

            }else if (!AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")&&AppConstants.APPLY_CODE.equalsIgnoreCase("")){
                APIRequestHandler.getInstance().insertStorePaymentApiCall(AppConstants.REQUEST_LIST_ID,tranref,AppConstants.TRANSACTION_AMOUNT,status,code,message,cvv,avs,cardcode,cardlast4,trace,ca_valid,AppConstants.REDEEM_POINT_TO_POINTS,AppConstants.SUB_TOTAL_CASH,AppConstants.REDEEM_POINT_TO_CASH,AppConstants.TRANSACTION_AMOUNT,"0","0",AppConstants.STORE_AMT,PaymentWebviewScreen.this);

            }else if (AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")&&!AppConstants.APPLY_CODE.equalsIgnoreCase("")){
                APIRequestHandler.getInstance().insertStorePaymentApiCall(AppConstants.REQUEST_LIST_ID,tranref,AppConstants.TRANSACTION_AMOUNT,status,code,message,cvv,avs,cardcode,cardlast4,trace,ca_valid,"0",AppConstants.SUB_TOTAL_CASH,String.valueOf(0),AppConstants.TRANSACTION_AMOUNT,AppConstants.APPLY_CODE_AMT,AppConstants.APPLY_CODE_ID,AppConstants.STORE_AMT,PaymentWebviewScreen.this);

            }else if (AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")&&AppConstants.APPLY_CODE.equalsIgnoreCase("")){
                APIRequestHandler.getInstance().insertStorePaymentApiCall(AppConstants.REQUEST_LIST_ID,tranref,AppConstants.TRANSACTION_AMOUNT,status,code,message,cvv,avs,cardcode,cardlast4,trace,ca_valid,"0",AppConstants.TRANSACTION_AMOUNT,String.valueOf(0),AppConstants.TRANSACTION_AMOUNT,"0","0",AppConstants.STORE_AMT,PaymentWebviewScreen.this);

            }

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(PaymentWebviewScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {

                }
            });
        }
    }

    public void updateTailorAmount(String TransactionNo){
        if (NetworkUtil.isNetworkAvailable(PaymentWebviewScreen.this)){
            APIRequestHandler.getInstance().updateTailorAmount(AppConstants.REQUEST_LIST_ID,AppConstants.TRANSACTION_AMOUNT,TransactionNo,PaymentWebviewScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(PaymentWebviewScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    updateTailorAmount(TransactionNo);
                }
            });
        }
    }

    public void getPointsByOrder(){
        if (NetworkUtil.isNetworkAvailable(PaymentWebviewScreen.this)){
            APIRequestHandler.getInstance().getPointsByOrder(mUserDetailsEntityRes.getUSER_ID(),AppConstants.REQUEST_LIST_ID,PaymentWebviewScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(PaymentWebviewScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getPointsByOrder();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);

        if (resObj instanceof GetPaymentStoreModal){
            GetPaymentStoreModal mResponse = (GetPaymentStoreModal)resObj;

            mStoreId = String.valueOf(mResponse.getResult().get(0).getStoreId());
            mKeyId = mResponse.getResult().get(0).getKeyId();
        }
        if (resObj instanceof ApplyPromoCodeResponse){
            AppConstants.APPLY_CODE = "";
        }
        if (resObj instanceof ConvertCashPointResponse){
            AppConstants.REDEEM_POINT_TO_CASH = "0";
            AppConstants.REDEEM_POINT_TO_POINTS = "0";
        }

    }

    @Override
    public void onBackPressed() {
        if (mBackHandlerStr.equalsIgnoreCase("true")) {

            if (!AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER") || !AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                if (!mWebView.onBackPressed()) {
                    return;
                }
                super.onBackPressed();
                this.overridePendingTransition(R.anim.animation_f_enter,
                        R.anim.animation_f_leave);
            }
        }
    }


    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        if (url.equalsIgnoreCase("https://secure.telr.com/gateway/webview_close.html")){
            getPaymentResponseApiCall();
        }
    }

    @Override
    public void onPageFinished(String url) {

    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }
    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        mWebView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.onDestroy();
        super.onDestroy();
    }
    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.payment_web_page_screen_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
            updateResources(PaymentWebviewScreen.this,"ar");

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.payment_web_page_screen_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
            updateResources(PaymentWebviewScreen.this,"en");

        }
    }

    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }
}
