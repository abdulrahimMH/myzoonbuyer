package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.OrderApprovalTailorAdapter;
import com.qoltech.mzyoonbuyer.adapter.OrderCustomizationAdapter;
import com.qoltech.mzyoonbuyer.adapter.StoreCartInnerAdapter;
import com.qoltech.mzyoonbuyer.adapter.StoreProductListAdapter;
import com.qoltech.mzyoonbuyer.entity.ApicallidEntity;
import com.qoltech.mzyoonbuyer.entity.DashboardProductsEntity;
import com.qoltech.mzyoonbuyer.entity.IdEntity;
import com.qoltech.mzyoonbuyer.entity.InsertWebOrderDetailsEntity;
import com.qoltech.mzyoonbuyer.entity.InsertWebOrderEntity;
import com.qoltech.mzyoonbuyer.entity.OrderCustomizationIdValueEntity;
import com.qoltech.mzyoonbuyer.entity.OrderSummaryMaterialImageEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.entity.UserMeasurementValueEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetCardItemsResponse;
import com.qoltech.mzyoonbuyer.modal.InsertMeasurementValueResponse;
import com.qoltech.mzyoonbuyer.modal.InsertWebOrderBody;
import com.qoltech.mzyoonbuyer.modal.OrderSummaryApiCallModal;
import com.qoltech.mzyoonbuyer.modal.OrderSummaryResponse;
import com.qoltech.mzyoonbuyer.modal.RelatedProductsResponse;
import com.qoltech.mzyoonbuyer.modal.StoreCartResponse;
import com.qoltech.mzyoonbuyer.modal.ViewDetailsNewFlowResponse;
import com.qoltech.mzyoonbuyer.modal.ViewDetailsResponse;
import com.qoltech.mzyoonbuyer.modal.WebOrderResponse;
import com.qoltech.mzyoonbuyer.service.APICommonInterface;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class OrderSummaryScreen extends BaseActivity {

    @BindView(R.id.order_summary_par_lay)
    LinearLayout mOrderSummaryParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.order_approval_gender_txt)
    TextView mOrderApprovalGenderTxt;

    @BindView(R.id.order_approval_dress_type_txt)
    TextView mOrderApprovalDressTypeTxt;

    @BindView(R.id.order_approval_sub_type_txt)
    TextView mOrderApprovalSubTypeTxt;

    @BindView(R.id.order_approval_seasonal_txt)
    TextView mOrderApprovalSeasonalTxt;

    @BindView(R.id.order_approval_place_txt)
    TextView mOrderApprovalPlaceTxt;

    @BindView(R.id.order_approval_brands_txt)
    TextView mOrderApprovalBrandsTxt;

    @BindView(R.id.order_approval_color_txt)
    TextView mOrderApprovalColorTxt;

    @BindView(R.id.order_approval_thickness_txt)
    TextView mOrderApprovalThicknessTxt;

    @BindView(R.id.order_approval_material_txt)
    TextView mOrderApprovalMaterialTxt;

    @BindView(R.id.order_approval_pattern_txt)
    TextView mOrderApprovalPatternTxt;

    @BindView(R.id.order_approval_customization_rec_view)
    RecyclerView mOrderApprovalCustomizationRecView;

    @BindView(R.id.order_approval_tailor_rec_view)
    RecyclerView mOrderApprovalTailorRecView;

    @BindView(R.id.order_approval_tailor_list_count)
    TextView mOrderApprovalTailorListCount;

    private UserDetailsEntity mUserDetailsEntityRes;

    @BindView(R.id.customization_one_two_txt)
    TextView mCustomizationOneTwoTxt;

    @BindView(R.id.summary_measurement_txt)
    TextView mSummaryMeasurementTxt;

    @BindView(R.id.summary_service_type_txt)
    TextView mSummaryServiceTypeTxt;

    @BindView(R.id.summary_order_type_txt)
    TextView mSummaryOrderTxt;

    @BindView(R.id.order_approval_customizatin_txt)
    TextView mOrderApprovalCustomizationTxt;

    @BindView(R.id.summary_normal_type_txt)
    TextView mSummaryNormalTxt;

    @BindView(R.id.customization_one_two_par_lay)
    LinearLayout mCustomizationOneTwoParLay;

    @BindView(R.id.order_summary_submit)
    Button mOrderSummarySubmitBtn;

    @BindView(R.id.check_out_related_products_recycler_view)
    RecyclerView mCheckOutRelatedProductsRecyclerView;

    @BindView(R.id.related_product_txt)
    TextView mRelatedProductTxt;

    @BindView(R.id.selected_product_txt)
    TextView mSelectedProductTxt;

    @BindView(R.id.get_store_product_rec_lay)
    RecyclerView mGetStoreProductRecLay;

    @BindView(R.id.order_summary_i_seasonal_txt)
    TextView mOrderSummaryISeasonalTxt;

    @BindView(R.id.order_summary_i_place_of_industry_txt)
    TextView mOrderSummaryIPlceTxt;

    @BindView(R.id.order_summary_i_brand_txt)
    TextView mOrderSummaryIBrandTxt;

    @BindView(R.id.order_summary_i_color_txt)
    TextView mOrderSummaryIColorTxt;

    @BindView(R.id.order_summary_i_material_type_txt)
    TextView mOrderSummaryIMaterilTypeTxt;

    @BindView(R.id.order_summary_i_thickness_type_txt)
    TextView mOrderSummaryIThicknessTypeTxt;

    @BindView(R.id.order_summary_i_material_txt)
    TextView mOrderSummaryIMaterilTxt;

    @BindView(R.id.order_summary_i_measurement_txt)
    TextView mOrderSummaryIMeasurementTxt;

    @BindView(R.id.order_summary_i_service_txt)
    TextView mOrderSummaryIServiceTxt;

    @BindView(R.id.order_summary_i_order_txt)
    TextView mOrderSummaryIOrderTxt;

    @BindView(R.id.order_summary_i_normal_txt)
    TextView mOrderSummaryINormalTxt;

    APICommonInterface mApiCommonInterface;

    private OrderApprovalTailorAdapter mCustomizationTailorAdapter;

    private OrderCustomizationAdapter mCustomizationAdapter;

    public StoreProductListAdapter mRelatedProductsAdapter;

    public StoreCartInnerAdapter mGetItemCardAdapter;

//    public GetItemCardAdapter mGetItemCardAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_order_summary);

        initView();
    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mOrderSummaryParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        mApiCommonInterface = APIRequestHandler.getClient().create(APICommonInterface.class);

        getLanguage();

        setHeader();

        AppConstants.ORDER_TYPE_CHECK = "Stitching";

        AppConstants.STORE_ID = "0";
        AppConstants.CHECK_OUT_ORDER = "Stitching";

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER") || AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
            mOrderSummarySubmitBtn.setText(getResources().getString(R.string.proceed_to_pay));
            getRelatedProductsApiCall();
        }
    }

    @OnClick({R.id.header_left_side_img, R.id.order_summary_submit,R.id.summary_seasonal_lay,R.id.summary_place_lay,R.id.summary_brands_lay,R.id.summary_color_lay,R.id.summary_material_lay,R.id.summary_pattern_lay,R.id.summary_measurement_lay,R.id.summary_delivery_lay,R.id.summary_order_lay,R.id.summary_normal_lay,R.id.summary_thickness_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.order_summary_submit:
                AppConstants.DELIVERY_TAILOR_ID  = new ArrayList<>();
                for (int i=0; i<AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.size(); i++){
                    IdEntity id = new IdEntity();
                    id.setId(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId());
                    AppConstants.DELIVERY_TAILOR_ID.add(id);
                }
                for (int i=0; i<AppConstants.GET_ITEM_CARD_LIST.size(); i++){
                    IdEntity id = new IdEntity();
                    id.setId(AppConstants.GET_ITEM_CARD_LIST.get(0).getSellerId());
                    AppConstants.DELIVERY_TAILOR_ID.add(id);
                }
                mOrderSummarySubmitBtn.setVisibility(View.INVISIBLE);
                AppConstants.APPROVED_TAILOR_ID = String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId());
                getOrderSummaryOrders();
                break;
            case R.id.summary_seasonal_lay:
                mOrderSummaryISeasonalTxt.setVisibility(View.VISIBLE);
                break;
            case R.id.summary_place_lay:
                mOrderSummaryIPlceTxt.setVisibility(View.VISIBLE);
                break;
            case R.id.summary_brands_lay:
                mOrderSummaryIBrandTxt.setVisibility(View.VISIBLE);
                break;
            case R.id.summary_color_lay:
                mOrderSummaryIColorTxt.setVisibility(View.VISIBLE);
                break;
            case R.id.summary_material_lay:
                mOrderSummaryIMaterilTypeTxt.setVisibility(View.VISIBLE);
                break;
            case R.id.summary_pattern_lay:
                mOrderSummaryIMaterilTxt.setVisibility(View.VISIBLE);
                break;
            case R.id.summary_measurement_lay:
                mOrderSummaryIMeasurementTxt.setVisibility(View.VISIBLE);
                break;
            case R.id.summary_delivery_lay:
                mOrderSummaryIServiceTxt.setVisibility(View.VISIBLE);
                break;
            case R.id.summary_order_lay:
                mOrderSummaryIOrderTxt.setVisibility(View.VISIBLE);
                break;
            case R.id.summary_normal_lay:
                mOrderSummaryINormalTxt.setVisibility(View.VISIBLE);
                break;
            case R.id.summary_thickness_lay:
                mOrderSummaryIThicknessTypeTxt.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.order_summary));
        mRightSideImg.setVisibility(View.INVISIBLE);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            mOrderApprovalGenderTxt.setText(AppConstants.GENDER_NAME_ARABIC);
            mSummaryOrderTxt.setText(AppConstants.ORDER_TYPE_ARABIC_NAME);
            mSummaryNormalTxt.setText(AppConstants.DELIVERY_TYPE_ARABIC_NAME);

        }else {
            mOrderApprovalGenderTxt.setText(AppConstants.GENDER_NAME);
            mSummaryOrderTxt.setText(AppConstants.ORDER_TYPE_NAME);
            mSummaryNormalTxt.setText(AppConstants.DELIVERY_TYPE_ENG_NAME);

        }
        mOrderApprovalDressTypeTxt.setText(AppConstants.DRESS_TYPE_NAME);
        mOrderApprovalSubTypeTxt.setText(AppConstants.DRESS_SUB_TYPE_NAME);

        AppConstants.ORDER_SUMMARY = "ORDER_SUMMARY";
        AppConstants.RELATED_PROD_ID = "";
        AppConstants.RELATED_PROD_NAME = "";
        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("QUOTE_ORDER")) {
            mOrderApprovalTailorListCount.setText(getResources().getString(R.string.total_num_of_tailor)+ " - " + String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.size()));

        } else {
            mOrderApprovalTailorListCount.setText(getResources().getString(R.string.selected_tailor));

        }

        mSummaryMeasurementTxt.setText(AppConstants.MEASUREMENT_ONE);
        mSummaryServiceTypeTxt.setText(AppConstants.SERVICE_TYPE_NAME);

        if (AppConstants.PATTERN_NAME.equalsIgnoreCase("")) {
            mCustomizationOneTwoTxt.setVisibility(View.GONE);
            mCustomizationOneTwoParLay.setVisibility(View.GONE);
        } else {
            if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW") || AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")) {
                getNewFlowViewDetailApiCall();
            } else {
                getViewDetailApiCall();
            }

            mCustomizationOneTwoTxt.setVisibility(View.VISIBLE);
            mCustomizationOneTwoParLay.setVisibility(View.VISIBLE);
        }

        setCustomizationAdapter();
        setTailorAdapter();
    }

    /*Set Adapter for the Recycler view*/
    public void setCustomizationAdapter() {

        if (AppConstants.CUSTOMIZATION_CLICK_ARRAY.size() > 0) {

            mCustomizationAdapter = new OrderCustomizationAdapter(this, AppConstants.CUSTOMIZATION_CLICK_ARRAY);
            mOrderApprovalCustomizationRecView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            mOrderApprovalCustomizationRecView.setAdapter(mCustomizationAdapter);

        }else {
            mOrderApprovalCustomizationTxt.setVisibility(View.GONE);
            mOrderApprovalCustomizationRecView.setVisibility(View.GONE);
        }
    }

    /*Set Adapter for the Recycler view*/
    public void setTailorAdapter() {

        if (AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.size() > 0) {

            mCustomizationTailorAdapter = new OrderApprovalTailorAdapter(this, AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST);
            mOrderApprovalTailorRecView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            mOrderApprovalTailorRecView.setAdapter(mCustomizationTailorAdapter);

        }
    }

    //    /*Set Adapter for the Recycler view*/
    public void setRelatedProductAdapter(ArrayList<DashboardProductsEntity> mResponse) {

        if (mResponse.size()>0){
            mCheckOutRelatedProductsRecyclerView.setVisibility(View.VISIBLE);
            mRelatedProductTxt.setVisibility(View.VISIBLE);

        }else {
            mCheckOutRelatedProductsRecyclerView.setVisibility(View.GONE);
            mRelatedProductTxt.setVisibility(View.GONE);
        }

            mRelatedProductsAdapter = new StoreProductListAdapter(OrderSummaryScreen.this,mResponse,"OrderSummary");
            mCheckOutRelatedProductsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            mCheckOutRelatedProductsRecyclerView.setAdapter(mRelatedProductsAdapter);


    }
    public void getViewDetailApiCall() {
        if (NetworkUtil.isNetworkAvailable(OrderSummaryScreen.this)) {
            APIRequestHandler.getInstance().getViewDetailsApiCall(AppConstants.PATTERN_ID, OrderSummaryScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderSummaryScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getViewDetailApiCall();
                }
            });
        }
    }

    public void getRelatedProductsApiCall(){
        if (NetworkUtil.isNetworkAvailable(OrderSummaryScreen.this)){
            APIRequestHandler.getInstance().getRelatedProductsApiCall(PreferenceUtil.getStringValue(this,AppConstants.CARD_ID),AppConstants.SUB_DRESS_TYPE_ID,OrderSummaryScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderSummaryScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getRelatedProductsApiCall();
                }
            });
        }
    }
    public void getCardItemsApiCall(String cardId){
        if (NetworkUtil.isNetworkAvailable(OrderSummaryScreen.this)){
            APIRequestHandler.getInstance().getCardItemsApiCall(cardId,OrderSummaryScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderSummaryScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCardItemsApiCall(cardId);
                }
            });
        }
    }
    public void getNewFlowViewDetailApiCall() {
        if (NetworkUtil.isNetworkAvailable(OrderSummaryScreen.this)) {
            APIRequestHandler.getInstance().getNewFlowViewDetailsApiCall(String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId()), AppConstants.PATTERN_ID, OrderSummaryScreen.this);

        } else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderSummaryScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getNewFlowViewDetailApiCall();
                }
            });
        }
    }

    public void getOrderSummaryOrders() {
        if (NetworkUtil.isNetworkAvailable(OrderSummaryScreen.this)) {
            AppConstants.ORDER_TYPE_SKILL = "Stitching";
            AppConstants.USER_ID_STR = mUserDetailsEntityRes.getUSER_ID();
            ArrayList<OrderSummaryMaterialImageEntity> mOrderSummaMaterialEntity = new ArrayList<>();
            ArrayList<OrderSummaryMaterialImageEntity> mOrderSummaReferenceEntity = new ArrayList<>();

            for (int i = 0; i < AppConstants.MATERIAL_IMAGES.size(); i++) {
                OrderSummaryMaterialImageEntity orderSummaryMaterialImageEntity = new OrderSummaryMaterialImageEntity();

                orderSummaryMaterialImageEntity.setImage(AppConstants.MATERIAL_IMAGES.get(i));
                mOrderSummaMaterialEntity.add(orderSummaryMaterialImageEntity);

            }

            for (int i = 0; i < AppConstants.REFERENCE_IMAGES.size(); i++) {
                OrderSummaryMaterialImageEntity orderSummaryReferenceImageEntity = new OrderSummaryMaterialImageEntity();

                orderSummaryReferenceImageEntity.setImage(AppConstants.REFERENCE_IMAGES.get(i));
                mOrderSummaReferenceEntity.add(orderSummaryReferenceImageEntity);

            }

            ArrayList<OrderCustomizationIdValueEntity> orderCustomizationIdValueEntities = new ArrayList<>();

            for (int i = 0; i < AppConstants.CUSTOMIZATION_CLICK_ARRAY.size(); i++) {
                OrderCustomizationIdValueEntity orderCustomizationIdValueEntity = new OrderCustomizationIdValueEntity();

                orderCustomizationIdValueEntity.setAttributeImageId(AppConstants.CUSTOMIZATION_CLICK_ARRAY.get(i).getId());
                orderCustomizationIdValueEntity.setCustomizationAttributeId(AppConstants.CUSTOMIZATION_CLICK_ARRAY.get(i).getCustomizationName());
                orderCustomizationIdValueEntities.add(orderCustomizationIdValueEntity);

            }

            ArrayList<UserMeasurementValueEntity> userMeasurementValueEntities = new ArrayList<>();

            Set keys = AppConstants.MEASUREMENT_MAP.keySet();
            Iterator itr = keys.iterator();

            String key;
            String value;
            while (itr.hasNext()) {
                UserMeasurementValueEntity userMeasurementValueEntity = new UserMeasurementValueEntity();

                key = (String) itr.next();
                value = (String) AppConstants.MEASUREMENT_MAP.get(key);
                System.out.println(key + " - " + value);
                userMeasurementValueEntity.setUserMeasurementId(String.valueOf(key));
                userMeasurementValueEntity.setValue(value);
                userMeasurementValueEntities.add(userMeasurementValueEntity);
            }

            ArrayList<ApicallidEntity> apicallidEntities = new ArrayList<>();

            for (int i = 0; i < AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.size(); i++) {
                ApicallidEntity apicallidEntity = new ApicallidEntity();

                apicallidEntity.setId(String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(i).getTailorId()));
                apicallidEntities.add(apicallidEntity);

            }

            OrderSummaryApiCallModal orderSummaryApiCallModal = new OrderSummaryApiCallModal();
            orderSummaryApiCallModal.setDressType(AppConstants.SUB_DRESS_TYPE_ID);
            orderSummaryApiCallModal.setCustomerId(mUserDetailsEntityRes.getUSER_ID());
            orderSummaryApiCallModal.setAddressId(AppConstants.GET_ADDRESS_ID);
            orderSummaryApiCallModal.setPatternId(AppConstants.PATTERN_ID);
            orderSummaryApiCallModal.setOrdertype(AppConstants.ORDER_TYPE_ID);
            orderSummaryApiCallModal.setComments(AppConstants.ADD_COMMENTS);

            orderSummaryApiCallModal.setMaterialImages(mOrderSummaMaterialEntity);
            orderSummaryApiCallModal.setReferenceImages(mOrderSummaReferenceEntity);
            orderSummaryApiCallModal.setOrderCustomization(orderCustomizationIdValueEntities);

            if (AppConstants.MEASUREMENT_TYPE.equalsIgnoreCase("1")) {
                orderSummaryApiCallModal.setMeasurementId(AppConstants.MEASUREMENT_ID.equalsIgnoreCase("") ? "-1" : AppConstants.MEASUREMENT_ID);
                orderSummaryApiCallModal.setUserMeasurementValue(userMeasurementValueEntities);
                orderSummaryApiCallModal.setMeasurementBy("Customer");
                orderSummaryApiCallModal.setMeasurementName(AppConstants.MEASUREMENT_MANUALLY);

            } else {
                orderSummaryApiCallModal.setMeasurementId("0");
                ArrayList<UserMeasurementValueEntity> UserMeasurementValueEntities = new ArrayList<>();
                orderSummaryApiCallModal.setUserMeasurementValue(UserMeasurementValueEntities);
                orderSummaryApiCallModal.setMeasurementBy("Customer");
                orderSummaryApiCallModal.setMeasurementName(mUserDetailsEntityRes.getUSER_NAME());

            }

            orderSummaryApiCallModal.setCreatedBy(mUserDetailsEntityRes.getUSER_ID());
            orderSummaryApiCallModal.setTailorId(apicallidEntities);
            orderSummaryApiCallModal.setDeliveryTypeId(AppConstants.DELIVERY_TYPE_ID);
            orderSummaryApiCallModal.setMeasurmentType(AppConstants.MEASUREMENT_TYPE);
            orderSummaryApiCallModal.setUnits(AppConstants.UNITS);
            orderSummaryApiCallModal.setDeliveryId(AppConstants.DELIVERY_ID);
            if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("QUOTE_ORDER")) {
                orderSummaryApiCallModal.setType("Quotation");
                orderSummaryApiCallModal.setOrderSubType("Quotation");

            } else {
                orderSummaryApiCallModal.setType("Direct");
                if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
                    orderSummaryApiCallModal.setOrderSubType("TailorMaterial");
                }else {
                    orderSummaryApiCallModal.setOrderSubType("MaterialTailor");
                }
            }

            AppConstants.CHECK_OUT_MATERIAL_ID = AppConstants.PATTERN_ID;
            AppConstants.CHECK_OUT_SERVICE_TYPE_ID = AppConstants.DELIVERY_TYPE_ID;
            AppConstants.CHECK_OUT_MEASUREMENT_ID = AppConstants.MEASUREMENT_TYPE;

            APIRequestHandler.getInstance().orderSummary(orderSummaryApiCallModal, OrderSummaryScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderSummaryScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getOrderSummaryOrders();
                }
            });
        }
    }

    public void updateOrderApprovalQty() {
        if (NetworkUtil.isNetworkAvailable(OrderSummaryScreen.this)) {
            APIRequestHandler.getInstance().updateQtyOrderApprovalApiCall(AppConstants.REQUEST_LIST_ID, AppConstants.UPDATE_QUANTITY, OrderSummaryScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderSummaryScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    updateOrderApprovalQty();
                }
            });
        }
    }

    public void getStoreCartApiCall() {
        if (NetworkUtil.isNetworkAvailable(this)){
            APIRequestHandler.getInstance().getStoreCartListApi(PreferenceUtil.getStringValue(this,AppConstants.CARD_ID),"List",this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStoreCartApiCall();
                }
            });
        }
    }


    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof OrderSummaryResponse) {
            OrderSummaryResponse mResponse = (OrderSummaryResponse) resObj;

//            Toast.makeText(OrderSummaryScreen.this, "Your order Id is : " + mResponse.getResult(), Toast.LENGTH_SHORT).show();
            AppConstants.REQUEST_LIST_ID = mResponse.getResult();
            AppConstants.ORDER_ID = mResponse.getResult();
            AppConstants.APPOINTMENT_LIST_ID = mResponse.getResult();

            if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("QUOTE_ORDER")) {
                    DialogManager.getInstance().showSuccessPopup(OrderSummaryScreen.this, new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {
                            nextScreen(BottomNavigationScreen.class, true);

                            AppConstants.MEN_1 = "";
                            AppConstants.MEN_2 = "";
                            AppConstants.MEN_3 = "";
                            AppConstants.MEN_4 = "";
                            AppConstants.MEN_5 = "";
                            AppConstants.MEN_6 = "";
                            AppConstants.MEN_7 = "";
                            AppConstants.MEN_8 = "";
                            AppConstants.MEN_9 = "";
                            AppConstants.MEN_10 = "";
                            AppConstants.MEN_11 = "";
                            AppConstants.MEN_12 = "";
                            AppConstants.MEN_13 = "";
                            AppConstants.MEN_14 = "";
                            AppConstants.MEN_15 = "";
                            AppConstants.MEN_16 = "";
                            AppConstants.MEN_17 = "";
                            AppConstants.MEN_18 = "";
                            AppConstants.MEN_19 = "";
                            AppConstants.MEN_20 = "";
                            AppConstants.MEN_21 = "";
                            AppConstants.MEN_22 = "";
                            AppConstants.MEN_23 = "";
                            AppConstants.MEN_24 = "";
                            AppConstants.MEN_25 = "";
                            AppConstants.MEN_26 = "";
                            AppConstants.MEN_27 = "";
                            AppConstants.MEN_28 = "";
                            AppConstants.MEN_29 = "";
                            AppConstants.MEN_30 = "";
                            AppConstants.MEN_31 = "";
                            AppConstants.MEN_32 = "";
                            AppConstants.MEN_33 = "";
                            AppConstants.MEN_34 = "";
                            AppConstants.MEN_35 = "";
                            AppConstants.MEN_36 = "";
                            AppConstants.MEN_37 = "";
                            AppConstants.MEN_38 = "";
                            AppConstants.MEN_39 = "";
                            AppConstants.MEN_40 = "";
                            AppConstants.MEN_41 = "";
                            AppConstants.MEN_42 = "";
                            AppConstants.MEN_43 = "";
                            AppConstants.MEN_44 = "";
                            AppConstants.MEN_45 = "";
                            AppConstants.MEN_46 = "";
                            AppConstants.MEN_47 = "";
                            AppConstants.MEN_48 = "";
                            AppConstants.MEN_49 = "";
                            AppConstants.MEN_50 = "";
                            AppConstants.MEN_51 = "";
                            AppConstants.MEN_52 = "";
                            AppConstants.MEN_53 = "";
                            AppConstants.MEN_54 = "";
                            AppConstants.MEN_55 = "";
                            AppConstants.MEN_56 = "";
                            AppConstants.MEN_57 = "";
                            AppConstants.MEN_58 = "";
                            AppConstants.MEN_59 = "";
                            AppConstants.MEN_60 = "";
                            AppConstants.MEN_61 = "";
                            AppConstants.MEN_62 = "";
                            AppConstants.MEN_63 = "";
                            AppConstants.MEN_64 = "";

                            AppConstants.WOMEN_65 = "";
                            AppConstants.WOMEN_66 = "";
                            AppConstants.WOMEN_67 = "";
                            AppConstants.WOMEN_68 = "";
                            AppConstants.WOMEN_69 = "";
                            AppConstants.WOMEN_70 = "";
                            AppConstants.WOMEN_71 = "";
                            AppConstants.WOMEN_72 = "";
                            AppConstants.WOMEN_73 = "";
                            AppConstants.WOMEN_74 = "";
                            AppConstants.WOMEN_75 = "";
                            AppConstants.WOMEN_76 = "";
                            AppConstants.WOMEN_77 = "";
                            AppConstants.WOMEN_78 = "";
                            AppConstants.WOMEN_79 = "";
                            AppConstants.WOMEN_80 = "";
                            AppConstants.WOMEN_81 = "";
                            AppConstants.WOMEN_82 = "";
                            AppConstants.WOMEN_83 = "";
                            AppConstants.WOMEN_84 = "";
                            AppConstants.WOMEN_85 = "";
                            AppConstants.WOMEN_86 = "";
                            AppConstants.WOMEN_87 = "";
                            AppConstants.WOMEN_88 = "";
                            AppConstants.WOMEN_89 = "";
                            AppConstants.WOMEN_90 = "";
                            AppConstants.WOMEN_91 = "";
                            AppConstants.WOMEN_92 = "";
                            AppConstants.WOMEN_93 = "";
                            AppConstants.WOMEN_94 = "";
                            AppConstants.WOMEN_95 = "";
                            AppConstants.WOMEN_96 = "";
                            AppConstants.WOMEN_97 = "";
                            AppConstants.WOMEN_98 = "";
                            AppConstants.WOMEN_99 = "";
                            AppConstants.WOMEN_100 = "";
                            AppConstants.WOMEN_101 = "";
                            AppConstants.WOMEN_102 = "";
                            AppConstants.WOMEN_103 = "";
                            AppConstants.WOMEN_104 = "";
                            AppConstants.WOMEN_105 = "";
                            AppConstants.WOMEN_106 = "";
                            AppConstants.WOMEN_107 = "";
                            AppConstants.WOMEN_108 = "";
                            AppConstants.WOMEN_109 = "";
                            AppConstants.WOMEN_110 = "";
                            AppConstants.WOMEN_111 = "";
                            AppConstants.WOMEN_112 = "";
                            AppConstants.WOMEN_113 = "";
                            AppConstants.WOMEN_114 = "";
                            AppConstants.WOMEN_115 = "";
                            AppConstants.WOMEN_116 = "";
                            AppConstants.WOMEN_117 = "";
                            AppConstants.WOMEN_118 = "";
                            AppConstants.WOMEN_119 = "";
                            AppConstants.WOMEN_120 = "";
                            AppConstants.WOMEN_121 = "";
                            AppConstants.WOMEN_122 = "";
                            AppConstants.WOMEN_123 = "";
                            AppConstants.WOMEN_124 = "";
                            AppConstants.WOMEN_125 = "";
                            AppConstants.WOMEN_126 = "";
                            AppConstants.WOMEN_127 = "";
                            AppConstants.WOMEN_128 = "";
                            AppConstants.WOMEN_129 = "";
                            AppConstants.WOMEN_130 = "";
                            AppConstants.WOMEN_131 = "";
                            AppConstants.WOMEN_132 = "";
                            AppConstants.WOMEN_133 = "";
                            AppConstants.WOMEN_134 = "";
                            AppConstants.WOMEN_135 = "";
                            AppConstants.WOMEN_136 = "";
                            AppConstants.WOMEN_137 = "";
                            AppConstants.WOMEN_138 = "";
                            AppConstants.WOMEN_139 = "";
                            AppConstants.WOMEN_140 = "";
                            AppConstants.WOMEN_141 = "";
                            AppConstants.WOMEN_142 = "";
                            AppConstants.WOMEN_143 = "";
                            AppConstants.WOMEN_144 = "";

                            AppConstants.BOY_145 = "";
                            AppConstants.BOY_146 = "";
                            AppConstants.BOY_147 = "";
                            AppConstants.BOY_148 = "";
                            AppConstants.BOY_149 = "";
                            AppConstants.BOY_150 = "";
                            AppConstants.BOY_151 = "";
                            AppConstants.BOY_152 = "";
                            AppConstants.BOY_153 = "";
                            AppConstants.BOY_154 = "";
                            AppConstants.BOY_155 = "";
                            AppConstants.BOY_156 = "";
                            AppConstants.BOY_157 = "";
                            AppConstants.BOY_158 = "";
                            AppConstants.BOY_159 = "";
                            AppConstants.BOY_160 = "";
                            AppConstants.BOY_161 = "";
                            AppConstants.BOY_162 = "";
                            AppConstants.BOY_163 = "";
                            AppConstants.BOY_164 = "";
                            AppConstants.BOY_165 = "";
                            AppConstants.BOY_166 = "";
                            AppConstants.BOY_167 = "";
                            AppConstants.BOY_168 = "";
                            AppConstants.BOY_169 = "";
                            AppConstants.BOY_170 = "";
                            AppConstants.BOY_171 = "";
                            AppConstants.BOY_172 = "";
                            AppConstants.BOY_173 = "";
                            AppConstants.BOY_174 = "";
                            AppConstants.BOY_175 = "";
                            AppConstants.BOY_176 = "";
                            AppConstants.BOY_177 = "";
                            AppConstants.BOY_178 = "";
                            AppConstants.BOY_179 = "";
                            AppConstants.BOY_180 = "";
                            AppConstants.BOY_181 = "";
                            AppConstants.BOY_182 = "";
                            AppConstants.BOY_183 = "";
                            AppConstants.BOY_184 = "";
                            AppConstants.BOY_185 = "";
                            AppConstants.BOY_186 = "";
                            AppConstants.BOY_187 = "";
                            AppConstants.BOY_188 = "";
                            AppConstants.BOY_189 = "";
                            AppConstants.BOY_190 = "";
                            AppConstants.BOY_191 = "";
                            AppConstants.BOY_192 = "";
                            AppConstants.BOY_193 = "";
                            AppConstants.BOY_194 = "";
                            AppConstants.BOY_195 = "";
                            AppConstants.BOY_196 = "";
                            AppConstants.BOY_197 = "";
                            AppConstants.BOY_198 = "";
                            AppConstants.BOY_199 = "";
                            AppConstants.BOY_200 = "";
                            AppConstants.BOY_201 = "";
                            AppConstants.BOY_202 = "";
                            AppConstants.BOY_203 = "";
                            AppConstants.BOY_204 = "";
                            AppConstants.BOY_205 = "";
                            AppConstants.BOY_206 = "";
                            AppConstants.BOY_207 = "";
                            AppConstants.BOY_208 = "";

                            AppConstants.GIRL_209 = "";
                            AppConstants.GIRL_210 = "";
                            AppConstants.GIRL_211 = "";
                            AppConstants.GIRL_212 = "";
                            AppConstants.GIRL_213 = "";
                            AppConstants.GIRL_214 = "";
                            AppConstants.GIRL_215 = "";
                            AppConstants.GIRL_216 = "";
                            AppConstants.GIRL_217 = "";
                            AppConstants.GIRL_218 = "";
                            AppConstants.GIRL_219 = "";
                            AppConstants.GIRL_220 = "";
                            AppConstants.GIRL_221 = "";
                            AppConstants.GIRL_222 = "";
                            AppConstants.GIRL_223 = "";
                            AppConstants.GIRL_224 = "";
                            AppConstants.GIRL_225 = "";
                            AppConstants.GIRL_226 = "";
                            AppConstants.GIRL_227 = "";
                            AppConstants.GIRL_228 = "";
                            AppConstants.GIRL_229 = "";
                            AppConstants.GIRL_230 = "";
                            AppConstants.GIRL_231 = "";
                            AppConstants.GIRL_232 = "";
                            AppConstants.GIRL_233 = "";
                            AppConstants.GIRL_234 = "";
                            AppConstants.GIRL_235 = "";
                            AppConstants.GIRL_236 = "";
                            AppConstants.GIRL_237 = "";
                            AppConstants.GIRL_238 = "";
                            AppConstants.GIRL_239 = "";
                            AppConstants.GIRL_240 = "";
                            AppConstants.GIRL_241 = "";
                            AppConstants.GIRL_242 = "";
                            AppConstants.GIRL_243 = "";
                            AppConstants.GIRL_244 = "";
                            AppConstants.GIRL_245 = "";
                            AppConstants.GIRL_246 = "";
                            AppConstants.GIRL_247 = "";
                            AppConstants.GIRL_248 = "";
                            AppConstants.GIRL_249 = "";
                            AppConstants.GIRL_250 = "";
                            AppConstants.GIRL_251 = "";
                            AppConstants.GIRL_252 = "";
                            AppConstants.GIRL_253 = "";
                            AppConstants.GIRL_254 = "";
                            AppConstants.GIRL_255 = "";
                            AppConstants.GIRL_256 = "";
                            AppConstants.GIRL_257 = "";
                            AppConstants.GIRL_258 = "";
                            AppConstants.GIRL_259 = "";
                            AppConstants.GIRL_260 = "";
                            AppConstants.GIRL_261 = "";
                            AppConstants.GIRL_262 = "";
                            AppConstants.GIRL_263 = "";
                            AppConstants.GIRL_264 = "";
                            AppConstants.GIRL_265 = "";
                            AppConstants.GIRL_266 = "";
                            AppConstants.GIRL_267 = "";
                            AppConstants.GIRL_268 = "";
                            AppConstants.GIRL_269 = "";
                            AppConstants.GIRL_270 = "";
                            AppConstants.GIRL_271 = "";
                            AppConstants.GIRL_272 = "";
                            AppConstants.GIRL_273 = "";
                            AppConstants.GIRL_274 = "";
                            AppConstants.GIRL_275 = "";
                            AppConstants.GIRL_276 = "";
                            AppConstants.GIRL_277 = "";
                            AppConstants.GIRL_278 = "";
                            AppConstants.GIRL_279 = "";
                            AppConstants.GIRL_280 = "";
                            AppConstants.GIRL_281 = "";
                            AppConstants.GIRL_282 = "";
                            AppConstants.GIRL_283 = "";
                            AppConstants.GIRL_284 = "";
                            AppConstants.GIRL_285 = "";
                            AppConstants.GIRL_286 = "";
                            AppConstants.GIRL_287 = "";
                            AppConstants.GIRL_288 = "";

                            AppConstants.MEASUREMENT_VALUES = new ArrayList<>();

                            AppConstants.MEASUREMENT_ID = "";

                            AppConstants.MEASUREMENT_MANUALLY = "";

                            AppConstants.MEASUREMENT_MAP = new HashMap<>();

                            AppConstants.GET_ADDRESS_ID = "";

                            AppConstants.PATTERN_ID = "";

                            AppConstants.SEASONAL_NAME = "";
                            AppConstants.PLACE_OF_INDUSTRY_NAME = "";
                            AppConstants.BRANDS_NAME = "";
                            AppConstants.MATERIAL_TYPE_NAME = "";
                            AppConstants.COLOUR_NAME = "";
                            AppConstants.PATTERN_NAME = "";

                            AppConstants.ORDER_TYPE_ID = "";

                            AppConstants.MEASUREMENT_ID = "";

                            AppConstants.MATERIAL_IMAGES = new ArrayList<>();

                            AppConstants.REFERENCE_IMAGES = new ArrayList<>();

                            AppConstants.CUSTOMIZATION_CLICK_ARRAY = new ArrayList<>();

                            AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();

                            AppConstants.CUSTOMIZATION_CLICKED_ARRAY = new ArrayList<>();

                            AppConstants.DELIVERY_TYPE_ID = "";

                            AppConstants.MEASUREMENT_TYPE = "";

                            AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST = new ArrayList<>();

                            AppConstants.RELATED_PROD_ID = "";
                            AppConstants.RELATED_PROD_NAME = "";
                        }
                    });
                }


            if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER") || AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                updateOrderApprovalQty();
                afterPaymentStatus();
                tailorApprovedApiCall();
                if (AppConstants.GET_ITEM_CARD_LIST.size() > 0){
                    AppConstants.ORDER_TYPE_SKILL = "StitchingStore";
                    insertWebOrder();
                }else {
                    AppConstants.CHECK_BOOK_APPOINTMENT = "RequestList";
                    if (AppConstants.DIRECT_ORDER_TYPE.equalsIgnoreCase("3") && AppConstants.MEASUREMENT_TYPE.equalsIgnoreCase("1")) {
                        AppConstants.PAYMENT_STATUS = "Not Paid";
                        if (AppConstants.DELIVERY_TYPE_ID.equalsIgnoreCase("3")) {
                            DialogManager.getInstance().showAlertPopup(OrderSummaryScreen.this, getResources().getString(R.string.book_an_appointment_before_pay), new InterfaceBtnCallBack() {
                                @Override
                                public void onPositiveClick() {
                                    nextScreen(ScheduletDetailScreen.class, true);
                                }
                            });
                        } else {
                            nextScreen(CheckoutScreen.class, true);

                        }
                    } else if (AppConstants.DIRECT_ORDER_TYPE.equalsIgnoreCase("2")) {
                        AppConstants.PAYMENT_STATUS = "Not Paid";
                        DialogManager.getInstance().showAlertPopup(OrderSummaryScreen.this, getResources().getString(R.string.book_an_appointment_before_pay), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                nextScreen(AppointmentDetails.class, true);
                            }
                        });
                    } else if (AppConstants.MEASUREMENT_TYPE.equalsIgnoreCase("3")) {
                        AppConstants.PAYMENT_STATUS = "Not Paid";
                        DialogManager.getInstance().showAlertPopup(OrderSummaryScreen.this, getResources().getString(R.string.book_an_appointment_before_pay), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                nextScreen(AppointmentDetails.class, true);
                            }
                        });
                    } else {
                        AppConstants.PAYMENT_STATUS = "Not Paid";
                        nextScreen(CheckoutScreen.class, true);
                    }

                    AppConstants.MEN_1 = "";
                    AppConstants.MEN_2 = "";
                    AppConstants.MEN_3 = "";
                    AppConstants.MEN_4 = "";
                    AppConstants.MEN_5 = "";
                    AppConstants.MEN_6 = "";
                    AppConstants.MEN_7 = "";
                    AppConstants.MEN_8 = "";
                    AppConstants.MEN_9 = "";
                    AppConstants.MEN_10 = "";
                    AppConstants.MEN_11 = "";
                    AppConstants.MEN_12 = "";
                    AppConstants.MEN_13 = "";
                    AppConstants.MEN_14 = "";
                    AppConstants.MEN_15 = "";
                    AppConstants.MEN_16 = "";
                    AppConstants.MEN_17 = "";
                    AppConstants.MEN_18 = "";
                    AppConstants.MEN_19 = "";
                    AppConstants.MEN_20 = "";
                    AppConstants.MEN_21 = "";
                    AppConstants.MEN_22 = "";
                    AppConstants.MEN_23 = "";
                    AppConstants.MEN_24 = "";
                    AppConstants.MEN_25 = "";
                    AppConstants.MEN_26 = "";
                    AppConstants.MEN_27 = "";
                    AppConstants.MEN_28 = "";
                    AppConstants.MEN_29 = "";
                    AppConstants.MEN_30 = "";
                    AppConstants.MEN_31 = "";
                    AppConstants.MEN_32 = "";
                    AppConstants.MEN_33 = "";
                    AppConstants.MEN_34 = "";
                    AppConstants.MEN_35 = "";
                    AppConstants.MEN_36 = "";
                    AppConstants.MEN_37 = "";
                    AppConstants.MEN_38 = "";
                    AppConstants.MEN_39 = "";
                    AppConstants.MEN_40 = "";
                    AppConstants.MEN_41 = "";
                    AppConstants.MEN_42 = "";
                    AppConstants.MEN_43 = "";
                    AppConstants.MEN_44 = "";
                    AppConstants.MEN_45 = "";
                    AppConstants.MEN_46 = "";
                    AppConstants.MEN_47 = "";
                    AppConstants.MEN_48 = "";
                    AppConstants.MEN_49 = "";
                    AppConstants.MEN_50 = "";
                    AppConstants.MEN_51 = "";
                    AppConstants.MEN_52 = "";
                    AppConstants.MEN_53 = "";
                    AppConstants.MEN_54 = "";
                    AppConstants.MEN_55 = "";
                    AppConstants.MEN_56 = "";
                    AppConstants.MEN_57 = "";
                    AppConstants.MEN_58 = "";
                    AppConstants.MEN_59 = "";
                    AppConstants.MEN_60 = "";
                    AppConstants.MEN_61 = "";
                    AppConstants.MEN_62 = "";
                    AppConstants.MEN_63 = "";
                    AppConstants.MEN_64 = "";

                    AppConstants.WOMEN_65 = "";
                    AppConstants.WOMEN_66 = "";
                    AppConstants.WOMEN_67 = "";
                    AppConstants.WOMEN_68 = "";
                    AppConstants.WOMEN_69 = "";
                    AppConstants.WOMEN_70 = "";
                    AppConstants.WOMEN_71 = "";
                    AppConstants.WOMEN_72 = "";
                    AppConstants.WOMEN_73 = "";
                    AppConstants.WOMEN_74 = "";
                    AppConstants.WOMEN_75 = "";
                    AppConstants.WOMEN_76 = "";
                    AppConstants.WOMEN_77 = "";
                    AppConstants.WOMEN_78 = "";
                    AppConstants.WOMEN_79 = "";
                    AppConstants.WOMEN_80 = "";
                    AppConstants.WOMEN_81 = "";
                    AppConstants.WOMEN_82 = "";
                    AppConstants.WOMEN_83 = "";
                    AppConstants.WOMEN_84 = "";
                    AppConstants.WOMEN_85 = "";
                    AppConstants.WOMEN_86 = "";
                    AppConstants.WOMEN_87 = "";
                    AppConstants.WOMEN_88 = "";
                    AppConstants.WOMEN_89 = "";
                    AppConstants.WOMEN_90 = "";
                    AppConstants.WOMEN_91 = "";
                    AppConstants.WOMEN_92 = "";
                    AppConstants.WOMEN_93 = "";
                    AppConstants.WOMEN_94 = "";
                    AppConstants.WOMEN_95 = "";
                    AppConstants.WOMEN_96 = "";
                    AppConstants.WOMEN_97 = "";
                    AppConstants.WOMEN_98 = "";
                    AppConstants.WOMEN_99 = "";
                    AppConstants.WOMEN_100 = "";
                    AppConstants.WOMEN_101 = "";
                    AppConstants.WOMEN_102 = "";
                    AppConstants.WOMEN_103 = "";
                    AppConstants.WOMEN_104 = "";
                    AppConstants.WOMEN_105 = "";
                    AppConstants.WOMEN_106 = "";
                    AppConstants.WOMEN_107 = "";
                    AppConstants.WOMEN_108 = "";
                    AppConstants.WOMEN_109 = "";
                    AppConstants.WOMEN_110 = "";
                    AppConstants.WOMEN_111 = "";
                    AppConstants.WOMEN_112 = "";
                    AppConstants.WOMEN_113 = "";
                    AppConstants.WOMEN_114 = "";
                    AppConstants.WOMEN_115 = "";
                    AppConstants.WOMEN_116 = "";
                    AppConstants.WOMEN_117 = "";
                    AppConstants.WOMEN_118 = "";
                    AppConstants.WOMEN_119 = "";
                    AppConstants.WOMEN_120 = "";
                    AppConstants.WOMEN_121 = "";
                    AppConstants.WOMEN_122 = "";
                    AppConstants.WOMEN_123 = "";
                    AppConstants.WOMEN_124 = "";
                    AppConstants.WOMEN_125 = "";
                    AppConstants.WOMEN_126 = "";
                    AppConstants.WOMEN_127 = "";
                    AppConstants.WOMEN_128 = "";
                    AppConstants.WOMEN_129 = "";
                    AppConstants.WOMEN_130 = "";
                    AppConstants.WOMEN_131 = "";
                    AppConstants.WOMEN_132 = "";
                    AppConstants.WOMEN_133 = "";
                    AppConstants.WOMEN_134 = "";
                    AppConstants.WOMEN_135 = "";
                    AppConstants.WOMEN_136 = "";
                    AppConstants.WOMEN_137 = "";
                    AppConstants.WOMEN_138 = "";
                    AppConstants.WOMEN_139 = "";
                    AppConstants.WOMEN_140 = "";
                    AppConstants.WOMEN_141 = "";
                    AppConstants.WOMEN_142 = "";
                    AppConstants.WOMEN_143 = "";
                    AppConstants.WOMEN_144 = "";

                    AppConstants.BOY_145 = "";
                    AppConstants.BOY_146 = "";
                    AppConstants.BOY_147 = "";
                    AppConstants.BOY_148 = "";
                    AppConstants.BOY_149 = "";
                    AppConstants.BOY_150 = "";
                    AppConstants.BOY_151 = "";
                    AppConstants.BOY_152 = "";
                    AppConstants.BOY_153 = "";
                    AppConstants.BOY_154 = "";
                    AppConstants.BOY_155 = "";
                    AppConstants.BOY_156 = "";
                    AppConstants.BOY_157 = "";
                    AppConstants.BOY_158 = "";
                    AppConstants.BOY_159 = "";
                    AppConstants.BOY_160 = "";
                    AppConstants.BOY_161 = "";
                    AppConstants.BOY_162 = "";
                    AppConstants.BOY_163 = "";
                    AppConstants.BOY_164 = "";
                    AppConstants.BOY_165 = "";
                    AppConstants.BOY_166 = "";
                    AppConstants.BOY_167 = "";
                    AppConstants.BOY_168 = "";
                    AppConstants.BOY_169 = "";
                    AppConstants.BOY_170 = "";
                    AppConstants.BOY_171 = "";
                    AppConstants.BOY_172 = "";
                    AppConstants.BOY_173 = "";
                    AppConstants.BOY_174 = "";
                    AppConstants.BOY_175 = "";
                    AppConstants.BOY_176 = "";
                    AppConstants.BOY_177 = "";
                    AppConstants.BOY_178 = "";
                    AppConstants.BOY_179 = "";
                    AppConstants.BOY_180 = "";
                    AppConstants.BOY_181 = "";
                    AppConstants.BOY_182 = "";
                    AppConstants.BOY_183 = "";
                    AppConstants.BOY_184 = "";
                    AppConstants.BOY_185 = "";
                    AppConstants.BOY_186 = "";
                    AppConstants.BOY_187 = "";
                    AppConstants.BOY_188 = "";
                    AppConstants.BOY_189 = "";
                    AppConstants.BOY_190 = "";
                    AppConstants.BOY_191 = "";
                    AppConstants.BOY_192 = "";
                    AppConstants.BOY_193 = "";
                    AppConstants.BOY_194 = "";
                    AppConstants.BOY_195 = "";
                    AppConstants.BOY_196 = "";
                    AppConstants.BOY_197 = "";
                    AppConstants.BOY_198 = "";
                    AppConstants.BOY_199 = "";
                    AppConstants.BOY_200 = "";
                    AppConstants.BOY_201 = "";
                    AppConstants.BOY_202 = "";
                    AppConstants.BOY_203 = "";
                    AppConstants.BOY_204 = "";
                    AppConstants.BOY_205 = "";
                    AppConstants.BOY_206 = "";
                    AppConstants.BOY_207 = "";
                    AppConstants.BOY_208 = "";

                    AppConstants.GIRL_209 = "";
                    AppConstants.GIRL_210 = "";
                    AppConstants.GIRL_211 = "";
                    AppConstants.GIRL_212 = "";
                    AppConstants.GIRL_213 = "";
                    AppConstants.GIRL_214 = "";
                    AppConstants.GIRL_215 = "";
                    AppConstants.GIRL_216 = "";
                    AppConstants.GIRL_217 = "";
                    AppConstants.GIRL_218 = "";
                    AppConstants.GIRL_219 = "";
                    AppConstants.GIRL_220 = "";
                    AppConstants.GIRL_221 = "";
                    AppConstants.GIRL_222 = "";
                    AppConstants.GIRL_223 = "";
                    AppConstants.GIRL_224 = "";
                    AppConstants.GIRL_225 = "";
                    AppConstants.GIRL_226 = "";
                    AppConstants.GIRL_227 = "";
                    AppConstants.GIRL_228 = "";
                    AppConstants.GIRL_229 = "";
                    AppConstants.GIRL_230 = "";
                    AppConstants.GIRL_231 = "";
                    AppConstants.GIRL_232 = "";
                    AppConstants.GIRL_233 = "";
                    AppConstants.GIRL_234 = "";
                    AppConstants.GIRL_235 = "";
                    AppConstants.GIRL_236 = "";
                    AppConstants.GIRL_237 = "";
                    AppConstants.GIRL_238 = "";
                    AppConstants.GIRL_239 = "";
                    AppConstants.GIRL_240 = "";
                    AppConstants.GIRL_241 = "";
                    AppConstants.GIRL_242 = "";
                    AppConstants.GIRL_243 = "";
                    AppConstants.GIRL_244 = "";
                    AppConstants.GIRL_245 = "";
                    AppConstants.GIRL_246 = "";
                    AppConstants.GIRL_247 = "";
                    AppConstants.GIRL_248 = "";
                    AppConstants.GIRL_249 = "";
                    AppConstants.GIRL_250 = "";
                    AppConstants.GIRL_251 = "";
                    AppConstants.GIRL_252 = "";
                    AppConstants.GIRL_253 = "";
                    AppConstants.GIRL_254 = "";
                    AppConstants.GIRL_255 = "";
                    AppConstants.GIRL_256 = "";
                    AppConstants.GIRL_257 = "";
                    AppConstants.GIRL_258 = "";
                    AppConstants.GIRL_259 = "";
                    AppConstants.GIRL_260 = "";
                    AppConstants.GIRL_261 = "";
                    AppConstants.GIRL_262 = "";
                    AppConstants.GIRL_263 = "";
                    AppConstants.GIRL_264 = "";
                    AppConstants.GIRL_265 = "";
                    AppConstants.GIRL_266 = "";
                    AppConstants.GIRL_267 = "";
                    AppConstants.GIRL_268 = "";
                    AppConstants.GIRL_269 = "";
                    AppConstants.GIRL_270 = "";
                    AppConstants.GIRL_271 = "";
                    AppConstants.GIRL_272 = "";
                    AppConstants.GIRL_273 = "";
                    AppConstants.GIRL_274 = "";
                    AppConstants.GIRL_275 = "";
                    AppConstants.GIRL_276 = "";
                    AppConstants.GIRL_277 = "";
                    AppConstants.GIRL_278 = "";
                    AppConstants.GIRL_279 = "";
                    AppConstants.GIRL_280 = "";
                    AppConstants.GIRL_281 = "";
                    AppConstants.GIRL_282 = "";
                    AppConstants.GIRL_283 = "";
                    AppConstants.GIRL_284 = "";
                    AppConstants.GIRL_285 = "";
                    AppConstants.GIRL_286 = "";
                    AppConstants.GIRL_287 = "";
                    AppConstants.GIRL_288 = "";


                    AppConstants.MEASUREMENT_VALUES = new ArrayList<>();

                    AppConstants.MEASUREMENT_ID = "";

                    AppConstants.MEASUREMENT_MANUALLY = "";

                    AppConstants.MEASUREMENT_MAP = new HashMap<>();

                    AppConstants.GET_ADDRESS_ID = "";

                    AppConstants.PATTERN_ID = "";

                    AppConstants.SEASONAL_NAME = "";
                    AppConstants.PLACE_OF_INDUSTRY_NAME = "";
                    AppConstants.BRANDS_NAME = "";
                    AppConstants.MATERIAL_TYPE_NAME = "";
                    AppConstants.COLOUR_NAME = "";
                    AppConstants.PATTERN_NAME = "";

                    AppConstants.ORDER_TYPE_ID = "";

                    AppConstants.MEASUREMENT_ID = "";

                    AppConstants.MATERIAL_IMAGES = new ArrayList<>();

                    AppConstants.REFERENCE_IMAGES = new ArrayList<>();

                    AppConstants.CUSTOMIZATION_CLICK_ARRAY = new ArrayList<>();

                    AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();

                    AppConstants.CUSTOMIZATION_CLICKED_ARRAY = new ArrayList<>();

                    AppConstants.DELIVERY_TYPE_ID = "";

                    AppConstants.MEASUREMENT_TYPE = "";

                    AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST = new ArrayList<>();


                    AppConstants.RELATED_PROD_ID = "";
                    AppConstants.RELATED_PROD_NAME = "";
                }

            }
        }

        if (resObj instanceof ViewDetailsResponse) {
            ViewDetailsResponse mResponse = (ViewDetailsResponse) resObj;

            mOrderApprovalSeasonalTxt.setText(getResources().getString(R.string.not_available));

            if (mResponse.getResult().getGetpattternById().size() > 0) {
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mOrderApprovalPatternTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPatternInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetpattternById().get(0).getPatternInArabic());
                    mOrderApprovalPlaceTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetpattternById().get(0).getPlaceInArabic());
                    mOrderApprovalBrandsTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetpattternById().get(0).getBrandInArabic());
                    mOrderApprovalMaterialTxt.setText(mResponse.getResult().getGetpattternById().get(0).getMaterialInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetpattternById().get(0).getMaterialInArabic());
                } else {
                    mOrderApprovalPatternTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPatternInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetpattternById().get(0).getPatternInEnglish());
                    mOrderApprovalPlaceTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetpattternById().get(0).getPlaceInEnglish());
                    mOrderApprovalBrandsTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetpattternById().get(0).getBrandInEnglish());
                    mOrderApprovalMaterialTxt.setText(mResponse.getResult().getGetpattternById().get(0).getMaterialInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetpattternById().get(0).getMaterialInEnglish());
                }
            }

            if(mResponse.getResult().getGetThickness().size() > 0){
                mOrderApprovalThicknessTxt.setText(mResponse.getResult().getGetThickness().get(0).getThickness());

            }

            if (mResponse.getResult().getGetColorById().size() > 0) {
                String ColorNameArabic = "";
                String ColorNameEnglish = "";

                for (int i = 0; i < mResponse.getResult().getGetColorById().size(); i++) {
                    if (i > 0) {
                        ColorNameArabic = ColorNameArabic + ", " + mResponse.getResult().getGetColorById().get(i).getColorInArabic();
                        ColorNameEnglish = ColorNameEnglish + ", " + mResponse.getResult().getGetColorById().get(i).getColorInEnglish();
                    } else {
                        ColorNameArabic = mResponse.getResult().getGetColorById().get(i).getColorInArabic();
                        ColorNameEnglish = mResponse.getResult().getGetColorById().get(i).getColorInEnglish();

                    }
                }

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mOrderApprovalColorTxt.setText(ColorNameArabic.equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : ColorNameArabic);
                } else {
                    mOrderApprovalColorTxt.setText(ColorNameEnglish.equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : ColorNameEnglish);

                }
            }

        }

        if (resObj instanceof ViewDetailsNewFlowResponse) {
            ViewDetailsNewFlowResponse mResponse = (ViewDetailsNewFlowResponse) resObj;

            mOrderApprovalSeasonalTxt.setText(getResources().getString(R.string.not_available));

            if (mResponse.getResult().getGetpattternById().size() > 0) {
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mOrderApprovalPlaceTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetpattternById().get(0).getPlaceInArabic());
                    mOrderApprovalBrandsTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetpattternById().get(0).getBrandInArabic());
                    mOrderApprovalMaterialTxt.setText(mResponse.getResult().getGetpattternById().get(0).getMaterialInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetpattternById().get(0).getMaterialInArabic());
                } else {
                    mOrderApprovalPlaceTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetpattternById().get(0).getPlaceInEnglish());
                    mOrderApprovalBrandsTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetpattternById().get(0).getBrandInEnglish());
                    mOrderApprovalMaterialTxt.setText(mResponse.getResult().getGetpattternById().get(0).getMaterialInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetpattternById().get(0).getMaterialInEnglish());

                }
            }

            if (mResponse.getResult().getGetThickness().size() > 0){
                mOrderApprovalThicknessTxt.setText(mResponse.getResult().getGetThickness().get(0).getThickness());
            }
            if (mResponse.getResult().getGetColorById().size() > 0) {
                String ColorNameArabic = "";
                String ColorNameEnglish = "";

                for (int i = 0; i < mResponse.getResult().getGetColorById().size(); i++) {
                    if (i > 0) {
                        ColorNameArabic = ColorNameArabic + ", " + mResponse.getResult().getGetColorById().get(i).getColorInArabic();
                        ColorNameEnglish = ColorNameEnglish + ", " + mResponse.getResult().getGetColorById().get(i).getColorInEnglish();
                    } else {
                        ColorNameArabic = mResponse.getResult().getGetColorById().get(i).getColorInArabic();
                        ColorNameEnglish = mResponse.getResult().getGetColorById().get(i).getColorInEnglish();

                    }
                }

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mOrderApprovalColorTxt.setText(ColorNameArabic.equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : ColorNameArabic);
                } else {
                    mOrderApprovalColorTxt.setText(ColorNameEnglish.equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : ColorNameEnglish);

                }


            }

            if (mResponse.getResult().getGetMaterialName().size() > 0) {
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mOrderApprovalPatternTxt.setText(mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInArabic());
                } else {
                    mOrderApprovalPatternTxt.setText(mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.not_available) : mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInEnglish());
                }

            }

        }

        if (resObj instanceof InsertMeasurementValueResponse) {
            InsertMeasurementValueResponse mResponse = (InsertMeasurementValueResponse) resObj;
        }
        if (resObj instanceof RelatedProductsResponse){
            RelatedProductsResponse mResponse = (RelatedProductsResponse)resObj;
            setRelatedProductAdapter(mResponse.getResult());
        }
        if (resObj instanceof GetCardItemsResponse){
            GetCardItemsResponse mResponse = (GetCardItemsResponse)resObj;
            AppConstants.GET_ITEM_CARD_LIST = new ArrayList<>();

            AppConstants.GET_ITEM_CARD_LIST = mResponse.getResult();

            setGetItemAdapter();
        }
        if (resObj instanceof StoreCartResponse) {

            StoreCartResponse mResponse = (StoreCartResponse)resObj;

            AppConstants.GET_ITEM_CARD_LIST = new ArrayList<>();

            AppConstants.GET_ITEM_CARD_LIST = mResponse.getResult();

            setGetItemAdapter();

        }
        if (resObj instanceof WebOrderResponse){
            WebOrderResponse mResponse = (WebOrderResponse)resObj;

                AppConstants.STORE_ID =String.valueOf(mResponse.getResult().getOrderId());
                AppConstants.CHECK_OUT_ORDER = "StitchingStore";

                AppConstants.CHECK_BOOK_APPOINTMENT = "RequestList";
            if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER") || AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {

                if (AppConstants.DIRECT_ORDER_TYPE.equalsIgnoreCase("3") && AppConstants.MEASUREMENT_TYPE.equalsIgnoreCase("1")) {
                    AppConstants.PAYMENT_STATUS = "Not Paid";
                    if (AppConstants.DELIVERY_TYPE_ID.equalsIgnoreCase("3")) {
                        DialogManager.getInstance().showAlertPopup(OrderSummaryScreen.this, getResources().getString(R.string.book_an_appointment_before_pay), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                nextScreen(ScheduletDetailScreen.class, true);
                            }
                        });
                    } else {
                        nextScreen(CheckoutScreen.class, true);

                    }
                } else if (AppConstants.DIRECT_ORDER_TYPE.equalsIgnoreCase("2")) {
                    AppConstants.PAYMENT_STATUS = "Not Paid";
                    DialogManager.getInstance().showAlertPopup(OrderSummaryScreen.this, getResources().getString(R.string.book_an_appointment_before_pay), new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {
                            nextScreen(AppointmentDetails.class, true);
                        }
                    });
                } else if (AppConstants.MEASUREMENT_TYPE.equalsIgnoreCase("3")) {
                    AppConstants.PAYMENT_STATUS = "Not Paid";
                    DialogManager.getInstance().showAlertPopup(OrderSummaryScreen.this, getResources().getString(R.string.book_an_appointment_before_pay), new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {
                            nextScreen(AppointmentDetails.class, true);
                        }
                    });
                } else {
                    AppConstants.PAYMENT_STATUS = "Not Paid";
                    nextScreen(CheckoutScreen.class, true);
                }

                AppConstants.MEN_1 = "";
                AppConstants.MEN_2 = "";
                AppConstants.MEN_3 = "";
                AppConstants.MEN_4 = "";
                AppConstants.MEN_5 = "";
                AppConstants.MEN_6 = "";
                AppConstants.MEN_7 = "";
                AppConstants.MEN_8 = "";
                AppConstants.MEN_9 = "";
                AppConstants.MEN_10 = "";
                AppConstants.MEN_11 = "";
                AppConstants.MEN_12 = "";
                AppConstants.MEN_13 = "";
                AppConstants.MEN_14 = "";
                AppConstants.MEN_15 = "";
                AppConstants.MEN_16 = "";
                AppConstants.MEN_17 = "";
                AppConstants.MEN_18 = "";
                AppConstants.MEN_19 = "";
                AppConstants.MEN_20 = "";
                AppConstants.MEN_21 = "";
                AppConstants.MEN_22 = "";
                AppConstants.MEN_23 = "";
                AppConstants.MEN_24 = "";
                AppConstants.MEN_25 = "";
                AppConstants.MEN_26 = "";
                AppConstants.MEN_27 = "";
                AppConstants.MEN_28 = "";
                AppConstants.MEN_29 = "";
                AppConstants.MEN_30 = "";
                AppConstants.MEN_31 = "";
                AppConstants.MEN_32 = "";
                AppConstants.MEN_33 = "";
                AppConstants.MEN_34 = "";
                AppConstants.MEN_35 = "";
                AppConstants.MEN_36 = "";
                AppConstants.MEN_37 = "";
                AppConstants.MEN_38 = "";
                AppConstants.MEN_39 = "";
                AppConstants.MEN_40 = "";
                AppConstants.MEN_41 = "";
                AppConstants.MEN_42 = "";
                AppConstants.MEN_43 = "";
                AppConstants.MEN_44 = "";
                AppConstants.MEN_45 = "";
                AppConstants.MEN_46 = "";
                AppConstants.MEN_47 = "";
                AppConstants.MEN_48 = "";
                AppConstants.MEN_49 = "";
                AppConstants.MEN_50 = "";
                AppConstants.MEN_51 = "";
                AppConstants.MEN_52 = "";
                AppConstants.MEN_53 = "";
                AppConstants.MEN_54 = "";
                AppConstants.MEN_55 = "";
                AppConstants.MEN_56 = "";
                AppConstants.MEN_57 = "";
                AppConstants.MEN_58 = "";
                AppConstants.MEN_59 = "";
                AppConstants.MEN_60 = "";
                AppConstants.MEN_61 = "";
                AppConstants.MEN_62 = "";
                AppConstants.MEN_63 = "";
                AppConstants.MEN_64 = "";

                AppConstants.WOMEN_65 = "";
                AppConstants.WOMEN_66 = "";
                AppConstants.WOMEN_67 = "";
                AppConstants.WOMEN_68 = "";
                AppConstants.WOMEN_69 = "";
                AppConstants.WOMEN_70 = "";
                AppConstants.WOMEN_71 = "";
                AppConstants.WOMEN_72 = "";
                AppConstants.WOMEN_73 = "";
                AppConstants.WOMEN_74 = "";
                AppConstants.WOMEN_75 = "";
                AppConstants.WOMEN_76 = "";
                AppConstants.WOMEN_77 = "";
                AppConstants.WOMEN_78 = "";
                AppConstants.WOMEN_79 = "";
                AppConstants.WOMEN_80 = "";
                AppConstants.WOMEN_81 = "";
                AppConstants.WOMEN_82 = "";
                AppConstants.WOMEN_83 = "";
                AppConstants.WOMEN_84 = "";
                AppConstants.WOMEN_85 = "";
                AppConstants.WOMEN_86 = "";
                AppConstants.WOMEN_87 = "";
                AppConstants.WOMEN_88 = "";
                AppConstants.WOMEN_89 = "";
                AppConstants.WOMEN_90 = "";
                AppConstants.WOMEN_91 = "";
                AppConstants.WOMEN_92 = "";
                AppConstants.WOMEN_93 = "";
                AppConstants.WOMEN_94 = "";
                AppConstants.WOMEN_95 = "";
                AppConstants.WOMEN_96 = "";
                AppConstants.WOMEN_97 = "";
                AppConstants.WOMEN_98 = "";
                AppConstants.WOMEN_99 = "";
                AppConstants.WOMEN_100 = "";
                AppConstants.WOMEN_101 = "";
                AppConstants.WOMEN_102 = "";
                AppConstants.WOMEN_103 = "";
                AppConstants.WOMEN_104 = "";
                AppConstants.WOMEN_105 = "";
                AppConstants.WOMEN_106 = "";
                AppConstants.WOMEN_107 = "";
                AppConstants.WOMEN_108 = "";
                AppConstants.WOMEN_109 = "";
                AppConstants.WOMEN_110 = "";
                AppConstants.WOMEN_111 = "";
                AppConstants.WOMEN_112 = "";
                AppConstants.WOMEN_113 = "";
                AppConstants.WOMEN_114 = "";
                AppConstants.WOMEN_115 = "";
                AppConstants.WOMEN_116 = "";
                AppConstants.WOMEN_117 = "";
                AppConstants.WOMEN_118 = "";
                AppConstants.WOMEN_119 = "";
                AppConstants.WOMEN_120 = "";
                AppConstants.WOMEN_121 = "";
                AppConstants.WOMEN_122 = "";
                AppConstants.WOMEN_123 = "";
                AppConstants.WOMEN_124 = "";
                AppConstants.WOMEN_125 = "";
                AppConstants.WOMEN_126 = "";
                AppConstants.WOMEN_127 = "";
                AppConstants.WOMEN_128 = "";
                AppConstants.WOMEN_129 = "";
                AppConstants.WOMEN_130 = "";
                AppConstants.WOMEN_131 = "";
                AppConstants.WOMEN_132 = "";
                AppConstants.WOMEN_133 = "";
                AppConstants.WOMEN_134 = "";
                AppConstants.WOMEN_135 = "";
                AppConstants.WOMEN_136 = "";
                AppConstants.WOMEN_137 = "";
                AppConstants.WOMEN_138 = "";
                AppConstants.WOMEN_139 = "";
                AppConstants.WOMEN_140 = "";
                AppConstants.WOMEN_141 = "";
                AppConstants.WOMEN_142 = "";
                AppConstants.WOMEN_143 = "";
                AppConstants.WOMEN_144 = "";

                AppConstants.BOY_145 = "";
                AppConstants.BOY_146 = "";
                AppConstants.BOY_147 = "";
                AppConstants.BOY_148 = "";
                AppConstants.BOY_149 = "";
                AppConstants.BOY_150 = "";
                AppConstants.BOY_151 = "";
                AppConstants.BOY_152 = "";
                AppConstants.BOY_153 = "";
                AppConstants.BOY_154 = "";
                AppConstants.BOY_155 = "";
                AppConstants.BOY_156 = "";
                AppConstants.BOY_157 = "";
                AppConstants.BOY_158 = "";
                AppConstants.BOY_159 = "";
                AppConstants.BOY_160 = "";
                AppConstants.BOY_161 = "";
                AppConstants.BOY_162 = "";
                AppConstants.BOY_163 = "";
                AppConstants.BOY_164 = "";
                AppConstants.BOY_165 = "";
                AppConstants.BOY_166 = "";
                AppConstants.BOY_167 = "";
                AppConstants.BOY_168 = "";
                AppConstants.BOY_169 = "";
                AppConstants.BOY_170 = "";
                AppConstants.BOY_171 = "";
                AppConstants.BOY_172 = "";
                AppConstants.BOY_173 = "";
                AppConstants.BOY_174 = "";
                AppConstants.BOY_175 = "";
                AppConstants.BOY_176 = "";
                AppConstants.BOY_177 = "";
                AppConstants.BOY_178 = "";
                AppConstants.BOY_179 = "";
                AppConstants.BOY_180 = "";
                AppConstants.BOY_181 = "";
                AppConstants.BOY_182 = "";
                AppConstants.BOY_183 = "";
                AppConstants.BOY_184 = "";
                AppConstants.BOY_185 = "";
                AppConstants.BOY_186 = "";
                AppConstants.BOY_187 = "";
                AppConstants.BOY_188 = "";
                AppConstants.BOY_189 = "";
                AppConstants.BOY_190 = "";
                AppConstants.BOY_191 = "";
                AppConstants.BOY_192 = "";
                AppConstants.BOY_193 = "";
                AppConstants.BOY_194 = "";
                AppConstants.BOY_195 = "";
                AppConstants.BOY_196 = "";
                AppConstants.BOY_197 = "";
                AppConstants.BOY_198 = "";
                AppConstants.BOY_199 = "";
                AppConstants.BOY_200 = "";
                AppConstants.BOY_201 = "";
                AppConstants.BOY_202 = "";
                AppConstants.BOY_203 = "";
                AppConstants.BOY_204 = "";
                AppConstants.BOY_205 = "";
                AppConstants.BOY_206 = "";
                AppConstants.BOY_207 = "";
                AppConstants.BOY_208 = "";

                AppConstants.GIRL_209 = "";
                AppConstants.GIRL_210 = "";
                AppConstants.GIRL_211 = "";
                AppConstants.GIRL_212 = "";
                AppConstants.GIRL_213 = "";
                AppConstants.GIRL_214 = "";
                AppConstants.GIRL_215 = "";
                AppConstants.GIRL_216 = "";
                AppConstants.GIRL_217 = "";
                AppConstants.GIRL_218 = "";
                AppConstants.GIRL_219 = "";
                AppConstants.GIRL_220 = "";
                AppConstants.GIRL_221 = "";
                AppConstants.GIRL_222 = "";
                AppConstants.GIRL_223 = "";
                AppConstants.GIRL_224 = "";
                AppConstants.GIRL_225 = "";
                AppConstants.GIRL_226 = "";
                AppConstants.GIRL_227 = "";
                AppConstants.GIRL_228 = "";
                AppConstants.GIRL_229 = "";
                AppConstants.GIRL_230 = "";
                AppConstants.GIRL_231 = "";
                AppConstants.GIRL_232 = "";
                AppConstants.GIRL_233 = "";
                AppConstants.GIRL_234 = "";
                AppConstants.GIRL_235 = "";
                AppConstants.GIRL_236 = "";
                AppConstants.GIRL_237 = "";
                AppConstants.GIRL_238 = "";
                AppConstants.GIRL_239 = "";
                AppConstants.GIRL_240 = "";
                AppConstants.GIRL_241 = "";
                AppConstants.GIRL_242 = "";
                AppConstants.GIRL_243 = "";
                AppConstants.GIRL_244 = "";
                AppConstants.GIRL_245 = "";
                AppConstants.GIRL_246 = "";
                AppConstants.GIRL_247 = "";
                AppConstants.GIRL_248 = "";
                AppConstants.GIRL_249 = "";
                AppConstants.GIRL_250 = "";
                AppConstants.GIRL_251 = "";
                AppConstants.GIRL_252 = "";
                AppConstants.GIRL_253 = "";
                AppConstants.GIRL_254 = "";
                AppConstants.GIRL_255 = "";
                AppConstants.GIRL_256 = "";
                AppConstants.GIRL_257 = "";
                AppConstants.GIRL_258 = "";
                AppConstants.GIRL_259 = "";
                AppConstants.GIRL_260 = "";
                AppConstants.GIRL_261 = "";
                AppConstants.GIRL_262 = "";
                AppConstants.GIRL_263 = "";
                AppConstants.GIRL_264 = "";
                AppConstants.GIRL_265 = "";
                AppConstants.GIRL_266 = "";
                AppConstants.GIRL_267 = "";
                AppConstants.GIRL_268 = "";
                AppConstants.GIRL_269 = "";
                AppConstants.GIRL_270 = "";
                AppConstants.GIRL_271 = "";
                AppConstants.GIRL_272 = "";
                AppConstants.GIRL_273 = "";
                AppConstants.GIRL_274 = "";
                AppConstants.GIRL_275 = "";
                AppConstants.GIRL_276 = "";
                AppConstants.GIRL_277 = "";
                AppConstants.GIRL_278 = "";
                AppConstants.GIRL_279 = "";
                AppConstants.GIRL_280 = "";
                AppConstants.GIRL_281 = "";
                AppConstants.GIRL_282 = "";
                AppConstants.GIRL_283 = "";
                AppConstants.GIRL_284 = "";
                AppConstants.GIRL_285 = "";
                AppConstants.GIRL_286 = "";
                AppConstants.GIRL_287 = "";
                AppConstants.GIRL_288 = "";

                AppConstants.MEASUREMENT_VALUES = new ArrayList<>();

                AppConstants.MEASUREMENT_ID = "";

                AppConstants.MEASUREMENT_MANUALLY = "";

                AppConstants.MEASUREMENT_MAP = new HashMap<>();

                AppConstants.GET_ADDRESS_ID = "";

                AppConstants.PATTERN_ID = "";

                AppConstants.SEASONAL_NAME = "";
                AppConstants.PLACE_OF_INDUSTRY_NAME = "";
                AppConstants.BRANDS_NAME = "";
                AppConstants.MATERIAL_TYPE_NAME = "";
                AppConstants.COLOUR_NAME = "";
                AppConstants.PATTERN_NAME = "";

                AppConstants.ORDER_TYPE_ID = "";

                AppConstants.MEASUREMENT_ID = "";

                AppConstants.MATERIAL_IMAGES = new ArrayList<>();

                AppConstants.REFERENCE_IMAGES = new ArrayList<>();

                AppConstants.CUSTOMIZATION_CLICK_ARRAY = new ArrayList<>();

                AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();

                AppConstants.CUSTOMIZATION_CLICKED_ARRAY = new ArrayList<>();

                AppConstants.DELIVERY_TYPE_ID = "";

                AppConstants.MEASUREMENT_TYPE = "";

                AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST = new ArrayList<>();

                AppConstants.RELATED_PROD_ID = "";
                AppConstants.RELATED_PROD_NAME = "";

            }
        }
    }
    /*Set Adapter for the Recycler view*/
    public void setGetItemAdapter() {

        mGetStoreProductRecLay.setVisibility(AppConstants.GET_ITEM_CARD_LIST.size()>0 ? View.VISIBLE : View.GONE);
        mSelectedProductTxt.setVisibility(  AppConstants.GET_ITEM_CARD_LIST .size() > 0 ? View.VISIBLE : View.GONE);

        mGetItemCardAdapter = new StoreCartInnerAdapter(AppConstants.GET_ITEM_CARD_LIST,OrderSummaryScreen.this,"OrderSummary");
        mGetStoreProductRecLay.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mGetStoreProductRecLay.setAdapter(mGetItemCardAdapter);

    }
    public void tailorApprovedApiCall() {
        if (NetworkUtil.isNetworkAvailable(OrderSummaryScreen.this)) {
            APIRequestHandler.getInstance().tailorApporvedApiCall(AppConstants.REQUEST_LIST_ID, AppConstants.APPROVED_TAILOR_ID, "1", OrderSummaryScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderSummaryScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    tailorApprovedApiCall();
                }
            });
        }
    }

    public void afterPaymentStatus() {
        if (NetworkUtil.isNetworkAvailable(OrderSummaryScreen.this)) {
            APIRequestHandler.getInstance().afterPaymentApiCall(AppConstants.APPROVED_TAILOR_ID, AppConstants.REQUEST_LIST_ID, OrderSummaryScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderSummaryScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    afterPaymentStatus();
                }
            });
        }
    }

    public void removeCardItem(String ProductId,String SizeId,String ColorId,String SellerId){
        if (NetworkUtil.isNetworkAvailable(OrderSummaryScreen.this)){
            APIRequestHandler.getInstance().removeCardItem(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),ProductId,SizeId,ColorId,SellerId,OrderSummaryScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderSummaryScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    removeCardItem(ProductId,SizeId,ColorId,SellerId);
                }
            });
        }
    }

    public void removeFromCart(String productId,String sizeId,String colorId,String sellerId){
        DialogManager.getInstance().showProgress(getContext());
        if (NetworkUtil.isNetworkAvailable(getContext())){
            mApiCommonInterface.removeFromCartListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),productId,sizeId,colorId,sellerId).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {
                    DialogManager.getInstance().hideProgress();
                    onResume();
                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {
                    DialogManager.getInstance().hideProgress();

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(getContext(), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    removeFromCart(productId,sizeId,colorId,sellerId);
                }
            });
        }

    }
    public void addQtyFromCart(String productId,String sizeId,String colorId,String sellerId,String qty){
        DialogManager.getInstance().showProgress(getContext());
        if (NetworkUtil.isNetworkAvailable(getContext())){
            mApiCommonInterface.changeQtyFromCartListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),productId,sizeId,colorId,sellerId,qty).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {
                    DialogManager.getInstance().hideProgress();
                    if (response.body() != null && response.isSuccessful()){
                        onResume();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {
                    DialogManager.getInstance().hideProgress();

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(getContext(), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    addQtyFromCart(productId,sizeId,colorId,sellerId,qty);
                }
            });
        }

    }
    public void addToWishListApiCall(String ProductId,String SellerId){
        if (NetworkUtil.isNetworkAvailable(this)){
            AppConstants.STORE_SIZE_ID = AppConstants.STORE_SIZE_ID.equalsIgnoreCase("") ? "-1" : AppConstants.STORE_SIZE_ID;
            AppConstants.STORE_COLOR_ID = AppConstants.STORE_COLOR_ID.equalsIgnoreCase("") ? "-1" : AppConstants.STORE_COLOR_ID;

            mApiCommonInterface.storeAddToWishListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),ProductId,AppConstants.STORE_SIZE_ID,AppConstants.STORE_COLOR_ID,SellerId).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {
                    if (response.body() != null && response.isSuccessful()){
                        onResume();
                        DialogManager.getInstance().showAlertPopup(OrderSummaryScreen.this, getResources().getString(R.string.product_added_to_wishlist), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                            }
                        });
                    }

                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    addToWishListApiCall(ProductId,SellerId);
                }
            });
        }
    }

    public void removeToWishListApiCall(String ProductId,String SellerId){
        if (NetworkUtil.isNetworkAvailable(this)){
            AppConstants.STORE_SIZE_ID = AppConstants.STORE_SIZE_ID.equalsIgnoreCase("") ? "-1" : AppConstants.STORE_SIZE_ID;
            AppConstants.STORE_COLOR_ID = AppConstants.STORE_COLOR_ID.equalsIgnoreCase("") ? "-1" : AppConstants.STORE_COLOR_ID;

            mApiCommonInterface.storeRemoveToWishListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),ProductId,AppConstants.STORE_SIZE_ID,AppConstants.STORE_COLOR_ID,SellerId).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {
                    if (response.body() != null && response.isSuccessful()){
                        onResume();
                        DialogManager.getInstance().showAlertPopup(OrderSummaryScreen.this, getResources().getString(R.string.product_removed_from_wishlist), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                            }
                        });
                    }

                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    removeToWishListApiCall(ProductId,SellerId);
                }
            });
        }
    }



    public void  insertWebOrder(){
        if (NetworkUtil.isNetworkAvailable(OrderSummaryScreen.this)){
            InsertWebOrderBody insertWebOrderBody = new InsertWebOrderBody();
            InsertWebOrderEntity insertWebOrderEntity = new InsertWebOrderEntity();
            ArrayList<InsertWebOrderDetailsEntity> insertWebOrderDetailsEntities = new ArrayList<>();

            String ordreAmount = "0";

            for (int i=0; i<AppConstants.GET_ITEM_CARD_LIST.size(); i++){
                ordreAmount = String.valueOf(Double.parseDouble(ordreAmount) + AppConstants.GET_ITEM_CARD_LIST.get(i).getUnitTotal());
            }

            insertWebOrderEntity.setUserId(Integer.parseInt(mUserDetailsEntityRes.getUSER_ID()));
            insertWebOrderEntity.setCartId(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID));
            insertWebOrderEntity.setAddressId(Integer.parseInt(AppConstants.GET_ADDRESS_ID));
            insertWebOrderEntity.setOrderAmount(Double.parseDouble(ordreAmount));
            insertWebOrderEntity.setOrderTotal(Double.parseDouble(ordreAmount));
            insertWebOrderEntity.setCouponCode("");
            insertWebOrderEntity.setCouponDiscount(0);
            insertWebOrderEntity.setDiscount(0);
            insertWebOrderEntity.setTax(0);
            insertWebOrderEntity.setStichingOrderReferenceNo(Integer.parseInt(AppConstants.REQUEST_LIST_ID));

            for (int i=0; i<AppConstants.GET_ITEM_CARD_LIST.size(); i++){
                InsertWebOrderDetailsEntity insertWebOrderDetailsEntity = new InsertWebOrderDetailsEntity();
                insertWebOrderDetailsEntity.setColorId(AppConstants.GET_ITEM_CARD_LIST.get(i).getColorId());
                insertWebOrderDetailsEntity.setDiscount(AppConstants.GET_ITEM_CARD_LIST.get(i).getDiscount());
                insertWebOrderDetailsEntity.setPrice(AppConstants.GET_ITEM_CARD_LIST.get(i).getAmount());
                insertWebOrderDetailsEntity.setProductId(AppConstants.GET_ITEM_CARD_LIST.get(i).getProductId());
                insertWebOrderDetailsEntity.setQuantity(AppConstants.GET_ITEM_CARD_LIST.get(i).getQuantity());
                insertWebOrderDetailsEntity.setSellerId(AppConstants.GET_ITEM_CARD_LIST.get(i).getSellerId());
                insertWebOrderDetailsEntity.setSizeId(AppConstants.GET_ITEM_CARD_LIST.get(i).getSizeId());
                insertWebOrderDetailsEntity.setTotal(AppConstants.GET_ITEM_CARD_LIST.get(i).getAmount()*AppConstants.GET_ITEM_CARD_LIST.get(i).getQuantity());

                insertWebOrderDetailsEntities.add(insertWebOrderDetailsEntity);
            }

            insertWebOrderBody.setOrder(insertWebOrderEntity);
            insertWebOrderBody.setOrderDetails(insertWebOrderDetailsEntities);

            APIRequestHandler.getInstance().insertWebOrder(insertWebOrderBody,OrderSummaryScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderSummaryScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    insertWebOrder();
                }
            });
        }
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.order_summary_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.order_summary_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER") || AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            getStoreCartApiCall();
        }
//        getCardItemsApiCall(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
