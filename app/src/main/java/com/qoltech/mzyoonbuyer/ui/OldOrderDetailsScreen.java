package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.GetItemCardAdapter;
import com.qoltech.mzyoonbuyer.adapter.OrderDetailsMaterialAndReferenceAndPatternAdapter;
import com.qoltech.mzyoonbuyer.entity.StoreCartEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetRatingResponse;
import com.qoltech.mzyoonbuyer.modal.OrderDetailsResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OldOrderDetailsScreen extends BaseActivity {

    @BindView(R.id.order_details_par_lay)
    LinearLayout mOrderDetailsPayLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.order_details_order_id_txt)
    TextView mOrderDetailsOrderIdTxt;

    @BindView(R.id.order_details_order_placed_on_txt)
    TextView mOrderDetailsOrderPlacedOnTxt;

    @BindView(R.id.order_details_material_type_txt)
    TextView mOrderDetailsMaterialTyepTxt;

    @BindView(R.id.order_details_service_type_txt)
    TextView mOrderDetailsServiceTypeTxt;

    @BindView(R.id.order_details_delivery_type_txt)
    TextView mOrderDetailsDeliveryTypeTxt;

    @BindView(R.id.order_details_dress_type_txt)
    TextView mOrderDetailsDressTypeTxt;

    @BindView(R.id.order_details_sub_dress_type_txt)
    TextView mOrderDetailsSubDressTypeTxt;

    @BindView(R.id.order_details_price_txt)
    TextView mOrderDetailsPriceTxt;

    @BindView(R.id.order_details_dress_type_pic_img)
    ImageView mOrderDetailsDressTypePicImg;

    @BindView(R.id.order_details_quantity_txt)
    TextView mOrderDetailsQuantityTxt;

    @BindView(R.id.order_details_measurment_lay)
    RelativeLayout mOrderDetailsMeasurementLay;

    @BindView(R.id.order_details_customization_lay)
    RelativeLayout mOrderDetailsCustomizationLay;

    @BindView(R.id.order_details_material_appointment_lay)
    LinearLayout mOrderDetailsMaterialAppointmentLay;

    @BindView(R.id.order_details_material_order_date_lay)
    LinearLayout mOrderDetailsMaterialOrderDateLay;

    @BindView(R.id.order_details_material_order_time_lay)
    LinearLayout mOrderDetailsMaterialOrderTimeLay;

    @BindView(R.id.order_details_payment_status_lay)
    LinearLayout mOrderDetailsPaymentStatusLay;

    @BindView(R.id.order_details_material_appointment_type_txt)
    TextView mOrderDetailsMaterialAppointmentTypeTxt;

    @BindView(R.id.order_details_material_appointment_type_date_and_time_txt)
    TextView mOrderDetailsMaterialAppointmentTypeDateAndTimeTxt;

    @BindView(R.id.order_details_material_appointment_type_time_txt)
    TextView mOrderDetailsMaterialAppointmentTypeTimeTxt;

    @BindView(R.id.order_details_material_appointment_type_status_txt)
    TextView mOrderDetailsMaterialAppointmentTypeStatusTxt;

    @BindView(R.id.order_details_material_appointment_type_left_txt)
    TextView mOrderDetailsMaterialAppointmentTypeLeftTxt;

    @BindView(R.id.order_deatails_material_create_appointment_lay)
    RelativeLayout mOrderDetailsMaterialCreateAppointmentLay;

    @BindView(R.id.order_details_delivery_appointment_lay)
    LinearLayout mOrderDetailsDeliveryAppointmentLay;

    @BindView(R.id.order_details_delivery_order_date_lay)
    LinearLayout mOrderDetailsDeliveryOrderDateLay;

    @BindView(R.id.order_details_delivery_order_time_lay)
    LinearLayout mOrderDetailsDeliveryOrderTimeLay;

    @BindView(R.id.order_details_delivery_status_lay)
    LinearLayout mOrderDetailsDeliveryStatusLay;

    @BindView(R.id.order_details_delivery_appointment_type_txt)
    TextView mOrderDetailsDeliveryAppointmentTypeTxt;

    @BindView(R.id.order_details_delivery_appointment_type_date_and_time_txt)
    TextView mOrderDetailsDeliveryAppointmentTypeDateAndTimeTxt;

    @BindView(R.id.order_details_delivery_appointment_type_time_txt)
    TextView mOrderDetailsDeliveryAppointmentTypeTimeTxt;

    @BindView(R.id.order_details_delivery_appointment_type_status_txt)
    TextView mOrderDetailsDeliveryAppointmentTypeStatusTxt;

    @BindView(R.id.order_details_delivery_appointment_type_left_txt)
    TextView mOrderDetailsDeliveryAppointmentTypeLeftTxt;

    @BindView(R.id.order_deatails_delivery_create_appointment_lay)
    RelativeLayout mOrderDetailsDeliveryCreateAppointmentLay;

    @BindView(R.id.order_details_measurement_appointment_lay)
    LinearLayout mOrderDetailsMeasurementAppointmentLay;

    @BindView(R.id.order_details_measurement_appointment_type_txt)
    TextView mOrderDetailsMeasurementAppointmentTypeTxt;

    @BindView(R.id.order_details_measurement_appointment_type_date_and_time_txt)
    TextView mOrderDetailsMeasurementAppointmentTypeDateAndTimeTxt;

    @BindView(R.id.order_details_measurement_appointment_type_time_txt)
    TextView mOrderDetailsMeasurementAppointmentTypeTimeTxt;

    @BindView(R.id.order_details_measurement_appointment_type_status_txt)
    TextView mOrderDetailsMeasurementAppointmentTypeStatusTxt;

    @BindView(R.id.order_measurement_delivery_appointment_type_left_txt)
    TextView mOrderDetailsMeasurementDeliveryAppointmentTypeLeftTxt;

    @BindView(R.id.order_deatails_measurement_create_appointment_lay)
    RelativeLayout mOrderDetailsMeasurementCreateAppointmentLay;

    @BindView(R.id.order_details_companies_material_txt_lay)
    LinearLayout mOrderDetailsCompaniesMaterialTxtLay;

    @BindView(R.id.ordder_details_companies_material_txt)
    TextView mOrderDetailsCompaniesMaterialTxt;

    @BindView(R.id.order_details_material_img_recycler_view)
    RecyclerView mOrderDetailsMaterialImgRecyclerView;

    @BindView(R.id.order_details_veiw_details_par_lay)
    RelativeLayout mOrderDetailsViewDetailsParLay;

    @BindView(R.id.order_Details_reference_img_lay)
    LinearLayout mOrderDetailsReferenceImgLay;

    @BindView(R.id.order_details_reference_img_recycler_view)
    RecyclerView mOrderDetailsRefernceImgRecyclerView;

    @BindView(R.id.order_details_stiching_measurement_amt_txt)
    TextView mOrderDetailsStichingMeasurementAmtTxt;

    @BindView(R.id.order_details_measurement_amt_txt)
    TextView mOrderDetailsMeasurmentAmtTxt;

    @BindView(R.id.order_details_urgent_amt_txt)
    TextView mOrderDetailsUrgentAmtTxt;

    @BindView(R.id.order_details_delivery_amt_txt)
    TextView mOrderDetailsDeliveryAmtTxt;

    @BindView(R.id.order_details_material_delivery_amt_txt)
    TextView mOrderDetailsMaterialDeliveryAmtTxt;

    @BindView(R.id.order_details_total_amt_txt)
    TextView mOrderDetailsTotalAmtTxt;

    @BindView(R.id.order_details_pay_lay)
    RelativeLayout mOrderDetailsPaymentLay;

    @BindView(R.id.order_details_rate_and_review_lay)
    CardView mOrderDetailsRateAndReviewLay;

    @BindView(R.id.order_details_pay_txt)
    TextView mOrderDetailsPayTxt;

    @BindView(R.id.order_details_tailor_name_txt)
    TextView mOrderDetailsTailorNameTxt;

    @BindView(R.id.order_details_delivery_name_txt)
    TextView mOrderDetailsDeliveryNameTxt;

    @BindView(R.id.order_details_delivery_address_txt)
    TextView mOrderDetailsDeliveryAddressTxt;

    @BindView(R.id.order_details_tracking_details_par_lay)
    LinearLayout mOrderDetailsTrackingDetailsLay;

    @BindView(R.id.order_details_material_appointment_left_par_lay)
    LinearLayout mOrderDetailsMaterialAppointmentLeftParLay;

    @BindView(R.id.order_deatails_material_create_appointment_txt)
    TextView mOrderDetailsMaterialCreateAppointmentTxt;

    @BindView(R.id.order_details_delivery_appointment_left_par_lay)
    LinearLayout mOrderDetailsDeliveryAppointmentLeftParLay;

    @BindView(R.id.order_details_measurement_appointment_left_par_lay)
            LinearLayout mOrderDetailsMeasurementAppointmentLeftParLay;

    @BindView(R.id.order_details_measurement_order_date_lay)
    LinearLayout mOrderDetailsMeasurementOrderDetaLay;

    @BindView(R.id.order_details_measurement_order_time_lay)
    LinearLayout mOrderDetailsMeasurementOrderTimeLay;

    @BindView(R.id.order_details_measurement_status_lay)
    LinearLayout mOrderDetailsMeasurementStatusLay;

    @BindView(R.id.order_deatails_measurement_create_appointment_txt)
    TextView mOrderDetailsMeasurmentCreateAppointmentTxt;

    @BindView(R.id.order_details_tracking_end_status_txt)
            TextView mOrderDetailsTrackingEndStatusTxt;

    @BindView(R.id.order_details_measurement_amt_par_lay)
            RelativeLayout mOrderDetailsMeasurementAmtParLay;

    @BindView(R.id.order_details_urgent_amt_par_lay)
            RelativeLayout mOrderDetailsUrgentAmtParLay;

    @BindView(R.id.order_details_delivery_amt_par_lay)
            RelativeLayout mOrderDetailsDeliveryAmtParLay;

    @BindView(R.id.order_details_material_amt_par_lay)
            RelativeLayout mOrderDetailsMaterialAmtParLay;

    @BindView(R.id.order_details_price_details_lay)
            CardView mOrderDetailsPaymentDetailsLay;

    @BindView(R.id.order_details_card_number_txt)
            TextView mOrderDetailsCardNumberTxt;

    @BindView(R.id.order_details_transaction_amount_txt)
            TextView mOrderDetailsTransactionAmtTxt;

    @BindView(R.id.order_details_payment_date_txt)
            TextView mOrderDetailsPaymentDateTxt;

    @BindView(R.id.order_details_transaction_number_txt)
            TextView mOrderDetailsTransactionNumberTxt;

    @BindView(R.id.order_deatails_material_empty_txt)
    TextView mOrderDetailsMaterialEmptyTxt;

    @BindView(R.id.order_deatails_measurement_empty_txt)
            TextView mOrderDetailsMeasurementEmtptyTxt;

    @BindView(R.id.order_deatails_delivery_empty_txt)
            TextView mOrderDetailsDeliveryEmptyTxt;

    @BindView(R.id.order_details_rate_and_review_txt)
    TextView mOrderDetailsRateAndReviewTxt;

    @BindView(R.id.order_details_rating_bar)
    RatingBar mOrderDetailsRatingBar;

    @BindView(R.id.order_details_sub_total_amt_lay)
    RelativeLayout mOrderDetailsSubTotalAmtLay;

    @BindView(R.id.order_details_reduced_points_amt_lay)
            RelativeLayout mOrderDetailsReducedPointsAmtLay;

    @BindView(R.id.order_details_promo_code_amt_lay)
            RelativeLayout mOrderDetailsPromoCodeAmtLay;

    @BindView(R.id.order_details_sub_total_amt_txt)
            TextView mOrderDetailsSubTotalAmtTxt;

    @BindView(R.id.order_details_reduced_points_amt_txt)
            TextView mOrderDetailsReducedPointsAmtTxt;

    @BindView(R.id.order_details_promo_code_amt_txt)
            TextView mOrderDetailsPromoCodeAmtTxt;

    @BindView(R.id.order_details_tracking_address_txt)
            TextView mOrderDetailsTrackingAddressTxt;

    @BindView(R.id.order_details_payment_status_img)
            ImageView mOrderDetailsPaymentStatusImg;

    @BindView(R.id.order_details_store_products_recycler_view)
            RecyclerView mOrderDetailsStoreProductsRecyclerView;

    @BindView(R.id.order_details_store_products_txt)
            TextView mOrderDetailsStoreProductsTxt;

    @BindView(R.id.order_details_store_lay)
            RelativeLayout mOrderDetailsStoreLay;

    @BindView(R.id.order_details_store_txt)
            TextView mOrderDetailsStoreTxt;

    @BindView(R.id.order_details_grand_total_view)
            View mOrderDetailsGrandTotalView;

    @BindView(R.id.order_details_grand_total_lay)
            RelativeLayout mOrderDetailsGrandTotalLay;

    @BindView(R.id.order_details_grand_total_txt)
            TextView mOrderDetailsGrandTotalTxt;

     GetItemCardAdapter mGetItemCardAdapter;

     OrderDetailsMaterialAndReferenceAndPatternAdapter mMaterialAndPatternAdapter;

     OrderDetailsMaterialAndReferenceAndPatternAdapter mReferenceAdapter;

      ArrayList<String> mMaterialAndPatternList;
      ArrayList<String> mReferenceList;

      String mMaterialTypeStr = "",mMeasurementTypeStr = "",mDeliveryTypeStr = "",mMaterialTypeNameStr =  "",mMeasurementTypeNameStr = "",mDeliveryTypeNameStr = "";

      int mMaterialSizeInt = 0,mMeasurementSizeInt = 0;
    private UserDetailsEntity mUserDetailsEntityRes;

    private boolean isSchedule = false;

    double mStichingAmtInt ,mMeasurementAmtInt,mUrgentStichingAmtInt ,mDeliveryAmtInt, mMaterialCourierAmtInt,mQtyInt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_old_order_details_screen);
        initView();
    }
    public void initView(){

        ButterKnife.bind(this);

        setupUI(mOrderDetailsPayLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(OldOrderDetailsScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        getOrderDetailsApiCall();

        setHeader();

        if (AppConstants.PENDING_DELIVERY_CLICK.equalsIgnoreCase("Delivery")){
            mOrderDetailsRateAndReviewLay.setVisibility(View.VISIBLE);
        }
        if (AppConstants.PENDING_DELIVERY_CLICK.equalsIgnoreCase("Pending")){
            mOrderDetailsRateAndReviewLay.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.header_left_side_img,R.id.order_details_tracking_details_lay,R.id.order_details_rate_and_review_par_lay,R.id.order_details_view_details,R.id.order_details_pay_lay,R.id.order_deatails_material_create_appointment_txt,R.id.order_deatails_delivery_create_appointment_txt,R.id.order_deatails_measurement_create_appointment_txt,R.id.order_details_customization_lay,R.id.order_details_measurment_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.order_details_tracking_details_lay:
                nextScreen(OrderTrackingDetailsScreen.class,true);
                break;
            case R.id.order_details_rate_and_review_par_lay:
                nextScreen(RatingAndReviewScreen.class,true);
                break;
            case R.id.order_details_view_details:
                AppConstants.VIEW_DETAILS_RES = "GONE";
                nextScreen(ViewDetailsScreen.class,true);
                break;
            case R.id.order_details_pay_lay:
                if (AppConstants.PAYMENT_STATUS.equalsIgnoreCase("Not Paid")&&AppConstants.MATERIAL_TYPE.equalsIgnoreCase("2")&&mMaterialSizeInt==0||AppConstants.PAYMENT_STATUS.equalsIgnoreCase("Not Paid")&&AppConstants.MEASUREMENT_PARTS_MEASUREMENT_TYPE.equalsIgnoreCase("Tailor Come To Your Place")&&mMeasurementSizeInt==0){
                    DialogManager.getInstance().showAlertPopup(OldOrderDetailsScreen.this, getResources().getString(R.string.book_an_appointment_before_pay), new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {
                            AppConstants.CHECK_BOOK_APPOINTMENT = "RequestList";
                            nextScreen(AppointmentDetails.class,true);
                        }
                    });
                }
                else if (AppConstants.PAYMENT_STATUS.equalsIgnoreCase("Not Paid")&&mDeliveryTypeStr.equalsIgnoreCase("3")&&isSchedule){
                    AppConstants.CHECK_BOOK_APPOINTMENT = "RequestList";
                    nextScreen(ScheduletDetailScreen.class,true);
                }
                else {
                    if (AppConstants.PAYMENT_STATUS.equalsIgnoreCase("Not Paid")){
                        nextScreen(CheckoutScreen.class,true);
                    }
                }
                break;
            case R.id.order_deatails_material_create_appointment_txt:
                AppConstants.CHECK_BOOK_APPOINTMENT = "RequestList";
                nextScreen(AppointmentDetails.class,true);
                break;
            case R.id.order_deatails_delivery_create_appointment_txt:
                AppConstants.CHECK_BOOK_APPOINTMENT = "RequestList";
                nextScreen(ScheduletDetailScreen.class,true);
                break;
            case R.id.order_deatails_measurement_create_appointment_txt:
                AppConstants.CHECK_BOOK_APPOINTMENT = "RequestList";
                nextScreen(AppointmentDetails.class,true);
                break;
            case R.id.order_details_customization_lay:
                AppConstants.ORDER_DETAILS_CUSTOMIZATION = "CUSTOMIZATION";
                AppConstants.VIEW_DETAILS_HEADER = getResources().getString(R.string.customization_images);
                nextScreen(ViewImageScreen.class,true);
                break;
            case R.id.order_details_measurment_lay:
                AppConstants.NEW_ORDER = "ADD_MEASUREMENT";
                AppConstants.ORDER_DETAILS_MEASUREMENT = "ORDER";
                nextScreen(MeasurementListScreen.class,true);
                break;
        }
    }

    public void getOrderDetailsApiCall(){
        if (NetworkUtil.isNetworkAvailable(OldOrderDetailsScreen.this)){
            APIRequestHandler.getInstance().getOrderDetailsApiCall(AppConstants.ORDER_PENDING_ID,AppConstants.ORDER_TYPE_SKILL.replace("Stitching-","").trim(), OldOrderDetailsScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OldOrderDetailsScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getOrderDetailsApiCall();
                }
            });
        }
    }
    public void getGetRatingApiCall(){
        if (NetworkUtil.isNetworkAvailable(OldOrderDetailsScreen.this)){
            APIRequestHandler.getInstance().getRatingApiCall(String.valueOf(AppConstants.TAILOR_CLICK_ID),AppConstants.ORDER_PENDING_ID,"Order", OldOrderDetailsScreen.this);
        }else {
            getGetRatingApiCall();
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof OrderDetailsResponse){
            OrderDetailsResponse mResponse = (OrderDetailsResponse)resObj;

            AppConstants.GET_APPOINTMENT_LEFT_MEASUREMENT_ENTITY = mResponse.getResult().getGetAppoinmentLeftMeasurement();

            if(mResponse.getResult().getStatus().size()>0){
                mOrderDetailsTrackingDetailsLay.setVisibility(View.VISIBLE);
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOrderDetailsTrackingEndStatusTxt.setText(mResponse.getResult().getStatus().get(mResponse.getResult().getStatus().size() - 1).getStatusInArabic());

                }else {
                    mOrderDetailsTrackingEndStatusTxt.setText(mResponse.getResult().getStatus().get(mResponse.getResult().getStatus().size() - 1).getStatus());
                }
            }
            if (mResponse.getResult().getOrderDetail().size()>0){

                isSchedule = mResponse.getResult().getOrderDetail().get(0).isSchedule();
                mOrderDetailsOrderPlacedOnTxt.setText(mResponse.getResult().getOrderDetail().get(0).getOrderDt());
                mOrderDetailsDressTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getDressTypeName());
                mOrderDetailsSubDressTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getProduct_Name());
                mQtyInt = mResponse.getResult().getOrderDetail().get(0).getQty();
                AppConstants.PATTERN_ID = String.valueOf(mResponse.getResult().getOrderDetail().get(0).getPatternId());
                AppConstants.ORDER_PENDING_ID = String.valueOf(mResponse.getResult().getOrderDetail().get(0).getOrderId());
                AppConstants.TAILOR_CLICK_ID = String.valueOf(mResponse.getResult().getOrderDetail().get(0).getTailorid());
                AppConstants.APPROVED_TAILOR_ID = String.valueOf(mResponse.getResult().getOrderDetail().get(0).getTailorid());
                AppConstants.REQUEST_LIST_ID = String.valueOf(mResponse.getResult().getOrderDetail().get(0).getOrderId());
                AppConstants.ORDER_ID = String.valueOf(mResponse.getResult().getOrderDetail().get(0).getOrderId());
                AppConstants.APPOINTMENT_LIST_ID = String.valueOf(mResponse.getResult().getOrderDetail().get(0).getOrderId());
                AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(mResponse.getResult().getOrderDetail().get(0).getDressType());
                AppConstants.DRESS_TYPE_ID = String.valueOf(mResponse.getResult().getOrderDetail().get(0).getDressTypeId());
                mMaterialTypeStr = mResponse.getResult().getOrderDetail().get(0).getMaterialTypeId();
                mMeasurementTypeStr = String.valueOf(mResponse.getResult().getOrderDetail().get(0).getMeasurementType());
                AppConstants.MATERIAL_TYPE = mResponse.getResult().getOrderDetail().get(0).getMaterialTypeId();
                AppConstants.MATERIAL_ONE_TYPE_NAME = mResponse.getResult().getOrderDetail().get(0).getMaterialType();
                AppConstants.MATERIAL_ONE_TYPE_ARABIC = mResponse.getResult().getOrderDetail().get(0).getMaterialTypeInArabic();
                AppConstants.MATERIAL_TYPE_ID =String.valueOf( mResponse.getResult().getOrderDetail().get(0).getPatternId());
                AppConstants.PATTERN_ID = String.valueOf(mResponse.getResult().getOrderDetail().get(0).getPatternId());
                AppConstants.MEASUREMENT_PARTS_MEASUREMENT_TYPE = mResponse.getResult().getOrderDetail().get(0).getMeasurementInEnglish();
                mDeliveryTypeStr =String.valueOf( mResponse.getResult().getOrderDetail().get(0).getDeliveryTypeId());
                AppConstants.PAYMENT_STATUS = mResponse.getResult().getOrderDetail().get(0).getPaymentStatus();
                if (mResponse.getResult().getOrderDetail().get(0).getPaymentStatus().equalsIgnoreCase("Not Paid")){
                    mOrderDetailsPaymentLay.setBackgroundResource(R.drawable.app_clr_gradient_with_corner);
                    mOrderDetailsPayTxt.setText(getResources().getString(R.string.pay));
                    mOrderDetailsPayTxt.setTextColor(getResources().getColor(R.color.white));
                    mOrderDetailsPaymentStatusImg.setBackgroundResource(R.drawable.order_details_pending_img);
                    mOrderDetailsTrackingDetailsLay.setVisibility(View.GONE);

                }else {
                    mOrderDetailsPaymentLay.setBackgroundResource(R.drawable.yellow_gradient_with_corner);
                    mOrderDetailsPayTxt.setText(getResources().getString(R.string.paid));
                    mOrderDetailsPayTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                    mOrderDetailsPaymentStatusImg.setBackgroundResource(R.drawable.order_details_paid_img);
                    mOrderDetailsTrackingDetailsLay.setVisibility(View.VISIBLE);


                }
                try {
                    Glide.with(this)
                            .load(AppConstants.IMAGE_BASE_URL+"images/DressSubType/"+mResponse.getResult().getOrderDetail().get(0).getImage())
                            .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                            .apply(RequestOptions.skipMemoryCacheOf(true))
                            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                            .into(mOrderDetailsDressTypePicImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOrderDetailsOrderIdTxt.setText(String.valueOf(mResponse.getResult().getOrderDetail().get(0).getOrderId())+"#" );
                    mOrderDetailsMaterialTyepTxt.setText(mResponse.getResult().getOrderDetail().get(0).getMaterialTypeInArabic());
                    mOrderDetailsServiceTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getServiceTypeInArabic());
                    mOrderDetailsDeliveryTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getDeliveryNameInArabic());
                    mOrderDetailsQuantityTxt.setText(String.valueOf(mResponse.getResult().getOrderDetail().get(0).getQty()));
                    mMaterialTypeNameStr = mResponse.getResult().getOrderDetail().get(0).getMaterialTypeInArabic();
                    mMeasurementTypeNameStr = mResponse.getResult().getOrderDetail().get(0).getMeasurementInArabic();
                    AppConstants.MEASUREMENT_PARTS_MEASUREMENT_NAME_TYPE = mResponse.getResult().getOrderDetail().get(0).getMeasurementInArabic();
                    mDeliveryTypeNameStr = mResponse.getResult().getOrderDetail().get(0).getServiceTypeInArabic();
                }else {
                    mOrderDetailsOrderIdTxt.setText("#" + String.valueOf(mResponse.getResult().getOrderDetail().get(0).getOrderId()));
                    mOrderDetailsMaterialTyepTxt.setText(mResponse.getResult().getOrderDetail().get(0).getMaterialType());
                    mOrderDetailsServiceTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getServiceType());
                    mOrderDetailsDeliveryTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getDeliveryNameInEnglish());
                    mOrderDetailsQuantityTxt.setText(String.valueOf(mResponse.getResult().getOrderDetail().get(0).getQty()));
                    mMaterialTypeNameStr = mResponse.getResult().getOrderDetail().get(0).getMaterialType();
                    mMeasurementTypeNameStr = mResponse.getResult().getOrderDetail().get(0).getMeasurementInEnglish();
                    AppConstants.MEASUREMENT_PARTS_MEASUREMENT_NAME_TYPE = mResponse.getResult().getOrderDetail().get(0).getMeasurementInEnglish();
                    mDeliveryTypeNameStr = mResponse.getResult().getOrderDetail().get(0).getServiceType();

                }

                getGetRatingApiCall();

            }
            mMaterialSizeInt = mResponse.getResult().getGetAppoinmentLeftMateril().size();

            if (mResponse.getResult().getGetAppoinmentLeftMateril().size()>0){
                if(AppConstants.InitiatedBy.equalsIgnoreCase("Tailor")){
                    mOrderDetailsMaterialAppointmentLay.setVisibility(View.GONE);

                }else {

                    mOrderDetailsMaterialAppointmentLay.setVisibility(View.VISIBLE);
                    mOrderDetailsMaterialEmptyTxt.setVisibility(View.GONE);
                    if (mMaterialTypeStr.equalsIgnoreCase("2")) {
                        mOrderDetailsMaterialCreateAppointmentTxt.setText(getResources().getString(R.string.create_appointment));
                    } else if (mMaterialTypeStr.equalsIgnoreCase("1")) {
                        mOrderDetailsMaterialCreateAppointmentTxt.setText(getResources().getString(R.string.approve_appointment));
                    }
                    if (mResponse.getResult().getGetAppoinmentLeftMateril().get(0).getStatus().equalsIgnoreCase("Approved") || mResponse.getResult().getGetAppoinmentLeftMateril().get(0).getStatus().equalsIgnoreCase("Tailor Approved") || mResponse.getResult().getGetAppoinmentLeftMateril().get(0).getStatus().equalsIgnoreCase("Waiting for Tailor Approval") || mResponse.getResult().getGetAppoinmentLeftMateril().get(0).getStatus().equalsIgnoreCase("Rejected")) {
                        mOrderDetailsMaterialCreateAppointmentLay.setVisibility(View.GONE);
                        mOrderDetailsMaterialAppointmentLeftParLay.setVisibility(View.GONE);
                    }

                    if (mResponse.getResult().getGetAppoinmentLeftMateril().get(0).getAppointmentLeftMaterialCount() == 0) {
                        mOrderDetailsMaterialCreateAppointmentLay.setVisibility(View.GONE);
                        mOrderDetailsMaterialAppointmentLeftParLay.setVisibility(View.GONE);
                    }
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        mOrderDetailsMaterialAppointmentTypeTxt.setText(mResponse.getResult().getGetAppoinmentLeftMateril().get(0).getHeaderInArabic());

                    } else {
                        mOrderDetailsMaterialAppointmentTypeTxt.setText(mResponse.getResult().getGetAppoinmentLeftMateril().get(0).getHeaderInEnglish());
                    }

                    mOrderDetailsMaterialAppointmentTypeTimeTxt.setText(mResponse.getResult().getGetAppoinmentLeftMateril().get(0).getAppointmentTime());
                    mOrderDetailsMaterialAppointmentTypeDateAndTimeTxt.setText(mResponse.getResult().getGetAppoinmentLeftMateril().get(0).getOrderDt());
                    mOrderDetailsMaterialAppointmentTypeStatusTxt.setText(mResponse.getResult().getGetAppoinmentLeftMateril().get(0).getStatus());
                    mOrderDetailsMaterialAppointmentTypeLeftTxt.setText(String.valueOf(mResponse.getResult().getGetAppoinmentLeftMateril().get(0).getAppointmentLeftMaterialCount()));
                }
                }else {
                if(AppConstants.InitiatedBy.equalsIgnoreCase("Tailor")){
                    mOrderDetailsMaterialAppointmentLay.setVisibility(View.GONE);

                }else {

                mOrderDetailsMaterialAppointmentLay.setVisibility(View.GONE);

                if (mMaterialTypeStr.equalsIgnoreCase("2")){
                    mOrderDetailsMaterialAppointmentLay.setVisibility(View.VISIBLE);
                    mOrderDetailsMaterialAppointmentLeftParLay.setVisibility(View.GONE);
                    mOrderDetailsMaterialOrderDateLay.setVisibility(View.GONE);
                    mOrderDetailsMaterialOrderTimeLay.setVisibility(View.GONE);
                    mOrderDetailsPaymentStatusLay.setVisibility(View.GONE);
                    mOrderDetailsMaterialEmptyTxt.setVisibility(View.VISIBLE);
                    mOrderDetailsMaterialAppointmentTypeTxt.setText(mMaterialTypeNameStr);
                    mOrderDetailsMaterialCreateAppointmentTxt.setText(getResources().getString(R.string.create_appointment));
                    mOrderDetailsMaterialCreateAppointmentLay.setVisibility(View.VISIBLE);
                    mOrderDetailsMaterialEmptyTxt.setText(getResources().getString(R.string.appointment_details_by_yours));

                }else if (mMaterialTypeStr.equalsIgnoreCase("1")){
                    mOrderDetailsMaterialAppointmentLay.setVisibility(View.VISIBLE);
                    mOrderDetailsMaterialAppointmentLeftParLay.setVisibility(View.GONE);
                    mOrderDetailsMaterialOrderDateLay.setVisibility(View.GONE);
                    mOrderDetailsMaterialOrderTimeLay.setVisibility(View.GONE);
                    mOrderDetailsPaymentStatusLay.setVisibility(View.GONE);
                    mOrderDetailsMaterialEmptyTxt.setVisibility(View.VISIBLE);
                    mOrderDetailsMaterialAppointmentTypeTxt.setText(mMaterialTypeNameStr);
                    mOrderDetailsMaterialCreateAppointmentLay.setVisibility(View.GONE);
                    mOrderDetailsMaterialEmptyTxt.setText(getResources().getString(R.string.appointment_details_by_tailor));

                }
                }
            }
            mMeasurementSizeInt = mResponse.getResult().getGetAppoinmentLeftMeasurement().size();
            if (mResponse.getResult().getGetAppoinmentLeftMeasurement().size()>0){
                if(AppConstants.InitiatedBy.equalsIgnoreCase("Tailor")){
                    mOrderDetailsMeasurementAppointmentLay.setVisibility(View.GONE);

                }
                else {

                mOrderDetailsMeasurementAppointmentLay.setVisibility(View.VISIBLE);
                mOrderDetailsMeasurementEmtptyTxt.setVisibility(View.GONE);

                if (mMeasurementTypeStr.equalsIgnoreCase("3")){

                    mOrderDetailsMeasurmentCreateAppointmentTxt.setText(getResources().getString(R.string.create_appointment));
                }else if (mMeasurementTypeStr.equalsIgnoreCase("2")){

                    mOrderDetailsMeasurmentCreateAppointmentTxt.setText(getResources().getString(R.string.approve_appointment));
                }


                if (mResponse.getResult().getGetAppoinmentLeftMeasurement().get(0).getStatus().equalsIgnoreCase("Approved")||mResponse.getResult().getGetAppoinmentLeftMeasurement().get(0).getStatus().equalsIgnoreCase("Tailor Approved")||mResponse.getResult().getGetAppoinmentLeftMeasurement().get(0).getStatus().equalsIgnoreCase("Waiting for Tailor Approval")||mResponse.getResult().getGetAppoinmentLeftMeasurement().get(0).getStatus().equalsIgnoreCase("Rejected")){
                    mOrderDetailsMeasurementCreateAppointmentLay.setVisibility(View.GONE);
                    mOrderDetailsMeasurementAppointmentLeftParLay.setVisibility(View.GONE);
                }
                if (mResponse.getResult().getGetAppoinmentLeftMeasurement().get(0).getAppointmentLeftMeasurementCount() == 0){
                    mOrderDetailsMeasurementCreateAppointmentLay.setVisibility(View.GONE);
                    mOrderDetailsMeasurementAppointmentLeftParLay.setVisibility(View.GONE);
                }
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOrderDetailsMeasurementAppointmentTypeTxt.setText(mResponse.getResult().getGetAppoinmentLeftMeasurement().get(0).getMeasurementInArabic());

                }else {
                    mOrderDetailsMeasurementAppointmentTypeTxt.setText(mResponse.getResult().getGetAppoinmentLeftMeasurement().get(0).getMeasurementInEnglish());
                }

                mOrderDetailsMeasurementAppointmentTypeTimeTxt.setText(mResponse.getResult().getGetAppoinmentLeftMeasurement().get(0).getAppointmentTime());
                mOrderDetailsMeasurementAppointmentTypeDateAndTimeTxt.setText(mResponse.getResult().getGetAppoinmentLeftMeasurement().get(0).getOrderDt());
                mOrderDetailsMeasurementAppointmentTypeStatusTxt.setText(mResponse.getResult().getGetAppoinmentLeftMeasurement().get(0).getStatus());
                mOrderDetailsMeasurementDeliveryAppointmentTypeLeftTxt.setText(String.valueOf(mResponse.getResult().getGetAppoinmentLeftMeasurement().get(0).getAppointmentLeftMeasurementCount()));
                }
            }else {
                if(AppConstants.InitiatedBy.equalsIgnoreCase("Tailor")){
                    mOrderDetailsMeasurementAppointmentLay.setVisibility(View.GONE);

                }else {

                mOrderDetailsMeasurementAppointmentLay.setVisibility(View.GONE);
                if (mMeasurementTypeStr.equalsIgnoreCase("3")){
                    mOrderDetailsMeasurementAppointmentLay.setVisibility(View.VISIBLE);
                    mOrderDetailsMeasurementAppointmentLeftParLay.setVisibility(View.GONE);
                    mOrderDetailsMeasurementOrderDetaLay.setVisibility(View.GONE);
                    mOrderDetailsMeasurementOrderTimeLay.setVisibility(View.GONE);
                    mOrderDetailsMeasurementStatusLay.setVisibility(View.GONE);
                    mOrderDetailsMeasurementAppointmentTypeTxt.setText(mMeasurementTypeNameStr);
                    mOrderDetailsMeasurementEmtptyTxt.setVisibility(View.VISIBLE);
                    mOrderDetailsMeasurementCreateAppointmentLay.setVisibility(View.VISIBLE);
                    mOrderDetailsMeasurmentCreateAppointmentTxt.setText(getResources().getString(R.string.create_appointment));
                    mOrderDetailsMeasurementEmtptyTxt.setText(getResources().getString(R.string.appointment_details_by_yours));

                }else if (mMeasurementTypeStr.equalsIgnoreCase("2")){
                    mOrderDetailsMeasurementAppointmentLay.setVisibility(View.VISIBLE);
                    mOrderDetailsMeasurementAppointmentLeftParLay.setVisibility(View.GONE);
                    mOrderDetailsMeasurementOrderDetaLay.setVisibility(View.GONE);
                    mOrderDetailsMeasurementOrderTimeLay.setVisibility(View.GONE);
                    mOrderDetailsMeasurementStatusLay.setVisibility(View.GONE);
                    mOrderDetailsMeasurementAppointmentTypeTxt.setText(mMeasurementTypeNameStr);
                    mOrderDetailsMeasurementEmtptyTxt.setVisibility(View.VISIBLE);
                    mOrderDetailsMeasurementCreateAppointmentLay.setVisibility(View.GONE);
                    mOrderDetailsMeasurementEmtptyTxt.setText(getResources().getString(R.string.appointment_details_by_tailor));

                }
                }
            }

            if (mResponse.getResult().getGetOrderDetailScheduleType().size()>0){
                if (mDeliveryTypeStr.equalsIgnoreCase("3")){
                    mOrderDetailsDeliveryAppointmentLay.setVisibility(View.VISIBLE);
                    if (mResponse.getResult().getGetOrderDetailScheduleType().get(0).getStatus().equalsIgnoreCase("Approved")||mResponse.getResult().getGetOrderDetailScheduleType().get(0).getStatus().equalsIgnoreCase("Tailor Approved")){
                        mOrderDetailsDeliveryCreateAppointmentLay.setVisibility(View.GONE);
                        mOrderDetailsDeliveryAppointmentLeftParLay.setVisibility(View.GONE);
                    }
                    if (mResponse.getResult().getGetOrderDetailScheduleType().get(0).getAppointmentLeftScheduleCount() == 0){
                        mOrderDetailsDeliveryCreateAppointmentLay.setVisibility(View.GONE);
                        mOrderDetailsDeliveryAppointmentLeftParLay.setVisibility(View.GONE);
                    }
                }
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOrderDetailsDeliveryAppointmentTypeTxt.setText(mResponse.getResult().getGetOrderDetailScheduleType().get(0).getDeliveryTypeInArabic());

                }else {
                    mOrderDetailsDeliveryAppointmentTypeTxt.setText(mResponse.getResult().getGetOrderDetailScheduleType().get(0).getDeliveryTypeInEnglish());
                }

                mOrderDetailsDeliveryOrderDateLay.setVisibility(View.VISIBLE);
                mOrderDetailsDeliveryOrderTimeLay.setVisibility(View.VISIBLE);
                mOrderDetailsDeliveryStatusLay.setVisibility(View.VISIBLE);
                mOrderDetailsDeliveryEmptyTxt.setVisibility(View.GONE);
                mOrderDetailsDeliveryAppointmentTypeTimeTxt.setText(mResponse.getResult().getGetOrderDetailScheduleType().get(0).getAppointmentTime());
                mOrderDetailsDeliveryAppointmentTypeDateAndTimeTxt.setText(mResponse.getResult().getGetOrderDetailScheduleType().get(0).getOrderDt());
                mOrderDetailsDeliveryAppointmentTypeStatusTxt.setText(mResponse.getResult().getGetOrderDetailScheduleType().get(0).getStatus());
                mOrderDetailsDeliveryAppointmentTypeLeftTxt.setText(String.valueOf(mResponse.getResult().getGetOrderDetailScheduleType().get(0).getAppointmentLeftScheduleCount()));

            }else {
                if (mDeliveryTypeStr.equalsIgnoreCase("3")){
                    mOrderDetailsDeliveryAppointmentLay.setVisibility(View.VISIBLE);
                    mOrderDetailsDeliveryCreateAppointmentLay.setVisibility(View.VISIBLE);
                    mOrderDetailsDeliveryAppointmentLeftParLay.setVisibility(View.GONE);
                    mOrderDetailsDeliveryOrderDateLay.setVisibility(View.GONE);
                    mOrderDetailsDeliveryOrderTimeLay.setVisibility(View.GONE);
                    mOrderDetailsDeliveryStatusLay.setVisibility(View.GONE);
                    mOrderDetailsDeliveryEmptyTxt.setVisibility(View.VISIBLE);
                    mOrderDetailsDeliveryEmptyTxt.setText(getResources().getString(R.string.appointment_details_by_yours));
                    mOrderDetailsDeliveryAppointmentTypeTxt.setText(mDeliveryTypeNameStr);
                }
            }
            if (mResponse.getResult().getMaterialImage().size()>0){
                if (mMaterialTypeStr.equalsIgnoreCase("2")){
                    mOrderDetailsCompaniesMaterialTxt.setText(getResources().getString(R.string.material_details_own_material_courier_material));
                }else if (mMaterialTypeStr.equalsIgnoreCase("1")){
                    mOrderDetailsCompaniesMaterialTxt.setText(getResources().getString(R.string.material_details_own_material_direct_delivery));
                }
                mOrderDetailsViewDetailsParLay.setVisibility(View.GONE);

            }
            if (mResponse.getResult().getAdditionalMaterialImage().size()>0){
                mOrderDetailsCompaniesMaterialTxt.setText(getResources().getString(R.string.material_details_company_material));
                mOrderDetailsViewDetailsParLay.setVisibility(View.VISIBLE);
            }else{
                if (mMaterialTypeStr.equalsIgnoreCase("3")){
                    mOrderDetailsCompaniesMaterialTxt.setText(getResources().getString(R.string.material_details_company_material));
                    mOrderDetailsViewDetailsParLay.setVisibility(View.VISIBLE);
                }
            }
            if (mResponse.getResult().getReferenceImage().size()>0){
                mOrderDetailsReferenceImgLay.setVisibility(View.VISIBLE);
                mOrderDetailsRefernceImgRecyclerView.setVisibility(View.VISIBLE);
            }
            if (mResponse.getResult().getStichingAndMaterialCharge().size() > 0){
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    if (mQtyInt >=1){
                        mOrderDetailsStichingMeasurementAmtTxt.setText(String.valueOf(Double.parseDouble(mResponse.getResult().getStichingAndMaterialCharge().get(0).getStichingAndMaterialCharge())*mQtyInt));
                    }else {
                        mOrderDetailsStichingMeasurementAmtTxt.setText(String.valueOf(mResponse.getResult().getStichingAndMaterialCharge().get(0).getStichingAndMaterialCharge()));

                    }

                }else {
                    if (mQtyInt >=1) {
                        mOrderDetailsStichingMeasurementAmtTxt.setText(String.valueOf(Double.parseDouble(mResponse.getResult().getStichingAndMaterialCharge().get(0).getStichingAndMaterialCharge()) * mQtyInt) );
                    }else {
                        mOrderDetailsStichingMeasurementAmtTxt.setText(String.valueOf(mResponse.getResult().getStichingAndMaterialCharge().get(0).getStichingAndMaterialCharge()) );

                    }
                }
                mStichingAmtInt = Double.parseDouble(mResponse.getResult().getStichingAndMaterialCharge().get(0).getStichingAndMaterialCharge());
            }
            if (mResponse.getResult().getMeasurementCharges().size()>0){
                if (Double.parseDouble(mResponse.getResult().getMeasurementCharges().get(0).getMeasurementCharges())!= 0){
                    mOrderDetailsMeasurementAmtParLay.setVisibility(View.VISIBLE);
                }
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOrderDetailsMeasurmentAmtTxt.setText(String.valueOf(mResponse.getResult().getMeasurementCharges().get(0).getMeasurementCharges()));

                }else {
                    mOrderDetailsMeasurmentAmtTxt.setText(String.valueOf(mResponse.getResult().getMeasurementCharges().get(0).getMeasurementCharges()));

                }
                mMeasurementAmtInt = Double.parseDouble(mResponse.getResult().getMeasurementCharges().get(0).getMeasurementCharges());
            }
            if (mResponse.getResult().getUrgentStichingCharges().size()>0){
                if (Double.parseDouble(mResponse.getResult().getUrgentStichingCharges().get(0).getUrgentStichingCharges()) != 0){
                    mOrderDetailsUrgentAmtParLay.setVisibility(View.VISIBLE);
                }
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOrderDetailsUrgentAmtTxt.setText(String.valueOf(mResponse.getResult().getUrgentStichingCharges().get(0).getUrgentStichingCharges()));

                }else {
                    mOrderDetailsUrgentAmtTxt.setText(String.valueOf(mResponse.getResult().getUrgentStichingCharges().get(0).getUrgentStichingCharges()));

                }
                mUrgentStichingAmtInt = Double.parseDouble(mResponse.getResult().getUrgentStichingCharges().get(0).getUrgentStichingCharges());
            }
            if(mResponse.getResult().getDeliveryCharge().size()>0){
                if (Double.parseDouble(mResponse.getResult().getDeliveryCharge().get(0).getDeliveryCharge()) != 0){
                    mOrderDetailsDeliveryAmtParLay.setVisibility(View.VISIBLE);
                }
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOrderDetailsDeliveryAmtTxt.setText(String.valueOf(mResponse.getResult().getDeliveryCharge().get(0).getDeliveryCharge()));

                }else {
                    mOrderDetailsDeliveryAmtTxt.setText(String.valueOf(mResponse.getResult().getDeliveryCharge().get(0).getDeliveryCharge()));

                }
                mDeliveryAmtInt = Double.parseDouble(mResponse.getResult().getDeliveryCharge().get(0).getDeliveryCharge());
            }
            if(mResponse.getResult().getMaterialDeliveryCharges().size()>0){
                if (Double.parseDouble(mResponse.getResult().getMaterialDeliveryCharges().get(0).getMaterialDeliveryCharges()) != 0){
                    mOrderDetailsMaterialAmtParLay.setVisibility(View.VISIBLE);
                }
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOrderDetailsMaterialDeliveryAmtTxt.setText(String.valueOf(mResponse.getResult().getMaterialDeliveryCharges().get(0).getMaterialDeliveryCharges()));

                }else {
                    mOrderDetailsMaterialDeliveryAmtTxt.setText(String.valueOf(mResponse.getResult().getMaterialDeliveryCharges().get(0).getMaterialDeliveryCharges()));

                }
                mMaterialCourierAmtInt = Double.parseDouble(mResponse.getResult().getMaterialDeliveryCharges().get(0).getMaterialDeliveryCharges());
            }
            if(mResponse.getResult().getTotal().size()>0){
//                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){

                mOrderDetailsSubTotalAmtLay.setVisibility(View.VISIBLE);
                mOrderDetailsSubTotalAmtTxt.setText(String.valueOf(mResponse.getResult().getTotal().get(0).getTotal()));
                    mOrderDetailsPriceTxt.setText(String.valueOf(mResponse.getResult().getTotal().get(0).getTotal()));
//                }
//                else {
//                    mOrderDetailsTotalAmtTxt.setText(String.valueOf(mResponse.getResult().getTotal().get(0).getTotal()));
//                    mOrderDetailsPriceTxt.setText(String.valueOf(mResponse.getResult().getTotal().get(0).getTotal()));
//                }

                AppConstants.TRANSACTION_AMOUNT = String.valueOf(mResponse.getResult().getTotal().get(0).getTotal());

            }else {
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOrderDetailsTotalAmtTxt.setText(String.valueOf(mQtyInt*mStichingAmtInt+mMeasurementAmtInt+mUrgentStichingAmtInt+mDeliveryAmtInt+mMaterialCourierAmtInt));
                    mOrderDetailsPriceTxt.setText(String.valueOf(mQtyInt*mStichingAmtInt+mMeasurementAmtInt+mUrgentStichingAmtInt+mDeliveryAmtInt+mMaterialCourierAmtInt));

                }else {
                    mOrderDetailsTotalAmtTxt.setText(String.valueOf(mQtyInt*mStichingAmtInt+mMeasurementAmtInt+mUrgentStichingAmtInt+mDeliveryAmtInt+mMaterialCourierAmtInt) );
                    mOrderDetailsPriceTxt.setText(String.valueOf(mQtyInt*mStichingAmtInt+mMeasurementAmtInt+mUrgentStichingAmtInt+mDeliveryAmtInt+mMaterialCourierAmtInt));

                }
                AppConstants.TRANSACTION_AMOUNT = String.valueOf(mQtyInt*mStichingAmtInt+mMeasurementAmtInt+mUrgentStichingAmtInt+mDeliveryAmtInt+mMaterialCourierAmtInt);
            }
            if (mResponse.getResult().getGetStoreOrderAmount().size() >0){
                mOrderDetailsStoreLay.setVisibility(mResponse.getResult().getGetStoreOrderAmount().get(0).getStoreOrderAmount() > 0 ? View.VISIBLE : View.GONE);
                mOrderDetailsGrandTotalView.setVisibility(mResponse.getResult().getGetStoreOrderAmount().get(0).getStoreOrderAmount() > 0 ? View.VISIBLE : View.GONE);
                mOrderDetailsGrandTotalLay.setVisibility(mResponse.getResult().getGetStoreOrderAmount().get(0).getStoreOrderAmount() > 0 ? View.VISIBLE : View.GONE);

                mOrderDetailsStoreTxt.setText(String.valueOf(mResponse.getResult().getGetStoreOrderAmount().get(0).getStoreOrderAmount()));

                mOrderDetailsGrandTotalTxt.setText(String.valueOf( mResponse.getResult().getGetStoreOrderAmount().get(0).getStoreOrderAmount() +Double.parseDouble(mOrderDetailsTotalAmtTxt.getText().toString())));
            }
            if (mResponse.getResult().getGetTailorDetails().size()>0){
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOrderDetailsTailorNameTxt.setText(mResponse.getResult().getGetTailorDetails().get(0).getTailorNameInArabic());

                }else {
                    mOrderDetailsTailorNameTxt.setText(mResponse.getResult().getGetTailorDetails().get(0).getTailorNameInEnglish());

                }
            }
            if (mResponse.getResult().getBuyerAddress().size()>0){
                mOrderDetailsDeliveryNameTxt.setText(mResponse.getResult().getBuyerAddress().get(0).getFirstName());
                mOrderDetailsDeliveryAddressTxt.setText(mResponse.getResult().getBuyerAddress().get(0).getFloor()+", \n"+mResponse.getResult().getBuyerAddress().get(0).getArea()+", \n"+mResponse.getResult().getBuyerAddress().get(0).getStateName()+", \n"+mResponse.getResult().getBuyerAddress().get(0).getCountry_Name()+", \n"+mResponse.getResult().getBuyerAddress().get(0).getPhoneNo());
                mOrderDetailsTrackingAddressTxt.setText(mResponse.getResult().getBuyerAddress().get(0).getFloor()+", \n"+mResponse.getResult().getBuyerAddress().get(0).getArea()+", \n"+mResponse.getResult().getBuyerAddress().get(0).getStateName()+", \n"+mResponse.getResult().getBuyerAddress().get(0).getCountry_Name()+", \n"+mResponse.getResult().getBuyerAddress().get(0).getPhoneNo());

            }

            if (mResponse.getResult().getGetOrederPaymentStatus().size() > 0){
                mOrderDetailsPaymentDetailsLay.setVisibility(View.VISIBLE);
                mOrderDetailsCardNumberTxt.setText("xxxx xxxx xxxx "+mResponse.getResult().getGetOrederPaymentStatus().get(0).getCardlast4());
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOrderDetailsTransactionAmtTxt.setText(String.valueOf(mResponse.getResult().getGetOrederPaymentStatus().get(0).getAmount()));
                }else {
                    mOrderDetailsTransactionAmtTxt.setText(String.valueOf(mResponse.getResult().getGetOrederPaymentStatus().get(0).getAmount()));
                }
                String splitDate[] = mResponse.getResult().getGetOrederPaymentStatus().get(0).getPaidOn().split("T");
                mOrderDetailsPaymentDateTxt.setText(splitDate[0]);
                mOrderDetailsTransactionNumberTxt.setText(String.valueOf(mResponse.getResult().getGetOrederPaymentStatus().get(0).getTransactionId()));

                NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                DecimalFormat formatter = (DecimalFormat)nf;
                formatter.applyPattern("######.#");

                String[] coupanReduceAmt  = mResponse.getResult().getGetOrederPaymentStatus().get(0).getCoupanReduceAmount().split(Pattern.quote("("));

                if(mResponse.getResult().getGetOrederPaymentStatus().size()>0){
                    mOrderDetailsTotalAmtTxt.setText(String.valueOf(mResponse.getResult().getGetOrederPaymentStatus().get(0).getTotalAmount()));

                    if (mResponse.getResult().getGetOrederPaymentStatus().get(0).getPoints() > 0&& Double.parseDouble(coupanReduceAmt[0]) == 0){

                        mOrderDetailsReducedPointsAmtLay.setVisibility(View.VISIBLE);
                        mOrderDetailsReducedPointsAmtTxt.setText("- "+mResponse.getResult().getGetOrederPaymentStatus().get(0).getReduceAmount());

                        mOrderDetailsTotalAmtTxt.setText(String.valueOf(mResponse.getResult().getGetOrederPaymentStatus().get(0).getTotalAmount()));

//                    mOrderDetailsSubTotalAmtLay.setVisibility(View.VISIBLE);
//                    mOrderDetailsSubTotalAmtTxt.setText(String.valueOf(mResponse.getResult().getGetOrederPaymentStatus().get(0).getTotalAmount()));

                    }else if (mResponse.getResult().getGetOrederPaymentStatus().get(0).getPoints() == 0&& Double.parseDouble(coupanReduceAmt[0])  > 0){
                        mOrderDetailsPromoCodeAmtLay.setVisibility(View.VISIBLE);
                        mOrderDetailsPromoCodeAmtTxt.setText("- "+String.valueOf(mResponse.getResult().getGetOrederPaymentStatus().get(0).getCoupanReduceAmount()));

                        mOrderDetailsTotalAmtTxt.setText(String.valueOf(mResponse.getResult().getGetOrederPaymentStatus().get(0).getTotalAmount()));

//                    mOrderDetailsSubTotalAmtLay.setVisibility(View.VISIBLE);
//                    mOrderDetailsSubTotalAmtTxt.setText(String.valueOf(mResponse.getResult().getGetOrederPaymentStatus().get(0).getTotalAmount()));

                    }else if (mResponse.getResult().getGetOrederPaymentStatus().get(0).getPoints() > 0&& Double.parseDouble(coupanReduceAmt[0])  > 0){
                        mOrderDetailsReducedPointsAmtLay.setVisibility(View.VISIBLE);
                        mOrderDetailsReducedPointsAmtTxt.setText("- "+mResponse.getResult().getGetOrederPaymentStatus().get(0).getReduceAmount());

                        mOrderDetailsPromoCodeAmtLay.setVisibility(View.VISIBLE);
                        mOrderDetailsPromoCodeAmtTxt.setText("- "+String.valueOf(mResponse.getResult().getGetOrederPaymentStatus().get(0).getCoupanReduceAmount()));

                        mOrderDetailsTotalAmtTxt.setText(String.valueOf(mResponse.getResult().getGetOrederPaymentStatus().get(0).getTotalAmount()));

//                    mOrderDetailsSubTotalAmtLay.setVisibility(View.VISIBLE);
//                    mOrderDetailsSubTotalAmtTxt.setText(String.valueOf(mResponse.getResult().getGetOrederPaymentStatus().get(0).getTotalAmount()));
                    }
                }
            }

            mMaterialAndPatternList = new ArrayList<>();
            mReferenceList = new ArrayList<>();

            for (int i=0; i<mResponse.getResult().getMaterialImage().size(); i++){
                mMaterialAndPatternList.add(AppConstants.IMAGE_BASE_URL+"Images/MaterialImages/"+mResponse.getResult().getMaterialImage().get(i).getImage());
            }

            for (int i=0; i<mResponse.getResult().getReferenceImage().size(); i++){

                mReferenceList.add(AppConstants.IMAGE_BASE_URL+"Images/ReferenceImages/"+mResponse.getResult().getReferenceImage().get(i).getImage());
            }

            for (int i=0; i<mResponse.getResult().getAdditionalMaterialImage().size(); i++){

                mMaterialAndPatternList.add(AppConstants.IMAGE_BASE_URL+"Images/Pattern/"+mResponse.getResult().getAdditionalMaterialImage().get(i).getImage());
            }
            setMaterialAdapter(mMaterialAndPatternList);
            setReferenceAdapter(mReferenceList);

            setGetItemAdapter(mResponse.getResult().getGetStoreOrderDetails());
        }

        if (resObj instanceof GetRatingResponse){
            GetRatingResponse mResponse= (GetRatingResponse)resObj;
            if (mResponse.getResult().getFullStatus().size()>0){
                mOrderDetailsRatingBar.setRating(Float.parseFloat(String.valueOf(mResponse.getResult().getFullStatus().get(0).getFullStatus())));

            }
            if (mResponse.getResult().getCustomerRating().size()>0){
                mOrderDetailsRateAndReviewTxt.setText(getResources().getString(R.string.view_review));
            }

        }
    }

    /*Set Adapter for the Recycler view*/
    public void setMaterialAdapter(ArrayList<String> addMaterialList) {

        mMaterialAndPatternAdapter = new OrderDetailsMaterialAndReferenceAndPatternAdapter(this,addMaterialList,"Material");
        mOrderDetailsMaterialImgRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mOrderDetailsMaterialImgRecyclerView.setAdapter(mMaterialAndPatternAdapter);

    }

    /*Set Adapter for the Recycler view*/
    public void setReferenceAdapter(ArrayList<String> addRefenceList) {

        mReferenceAdapter = new OrderDetailsMaterialAndReferenceAndPatternAdapter(this,addRefenceList,"Reference");
        mOrderDetailsRefernceImgRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mOrderDetailsRefernceImgRecyclerView.setAdapter(mReferenceAdapter);

    }

    /*Set Adapter for the Recycler view*/
    public void setGetItemAdapter(ArrayList<StoreCartEntity> getStoreOrderDetails) {

        if (getStoreOrderDetails.size() > 0){
            AppConstants.STORE_ID = getStoreOrderDetails.get(0).getId();
            AppConstants.CHECK_OUT_ORDER = "store";
        }else {
            AppConstants.STORE_ID = "0";
            AppConstants.CHECK_OUT_ORDER = "";
        }

        mOrderDetailsStoreProductsRecyclerView.setVisibility(getStoreOrderDetails.size()>0 ? View.VISIBLE : View.GONE);
        mOrderDetailsStoreProductsTxt.setVisibility(getStoreOrderDetails.size()>0 ? View.VISIBLE : View.GONE);

        mGetItemCardAdapter = new GetItemCardAdapter(getStoreOrderDetails, OldOrderDetailsScreen.this);
        mOrderDetailsStoreProductsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mOrderDetailsStoreProductsRecyclerView.setAdapter(mGetItemCardAdapter);

    }


    public void setHeader(){
      String heading =  AppConstants.PENDING_DELIVERY_CLICK.substring(0, 1).toUpperCase() + AppConstants.PENDING_DELIVERY_CLICK.substring(1);
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(heading+ " " + getResources().getString(R.string.order_details));
        mRightSideImg.setVisibility(View.INVISIBLE);

    }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.order_details_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.order_details_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
