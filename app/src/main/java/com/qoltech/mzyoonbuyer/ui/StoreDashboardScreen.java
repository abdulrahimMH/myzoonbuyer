package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.StoreDashboardCatogeryAdapter;
import com.qoltech.mzyoonbuyer.adapter.StoreProductListAdapter;
import com.qoltech.mzyoonbuyer.adapter.StoreTopBrandsAdapter;
import com.qoltech.mzyoonbuyer.adapter.StoreViewPagerAdapter;
import com.qoltech.mzyoonbuyer.entity.DashboardCategoriesEntity;
import com.qoltech.mzyoonbuyer.entity.DashboardProductsEntity;
import com.qoltech.mzyoonbuyer.entity.GetRecentProductEntity;
import com.qoltech.mzyoonbuyer.entity.GetStoreDashBoardEntity;
import com.qoltech.mzyoonbuyer.entity.IdStringEntity;
import com.qoltech.mzyoonbuyer.entity.RecentBrandEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetStoreDashBoardResponse;
import com.qoltech.mzyoonbuyer.modal.StoreCartResponse;
import com.qoltech.mzyoonbuyer.service.APICommonInterface;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ZoomOutPageTransformer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class StoreDashboardScreen extends BaseActivity {

    @BindView(R.id.store_dashboard_par_lay)
    LinearLayout mStoreDashBoardParLay;

    @BindView(R.id.header_store_search_lay)
    RelativeLayout mStoreSearchLay;

    @BindView(R.id.header_store_lay)
    RelativeLayout mStoreLay;

    @BindView(R.id.store_dashboard_one_view_pager)
    ViewPager mStoreDashBoardOneViewPager;

    @BindView(R.id.store_dashboard_one_indigator)
    com.rd.PageIndicatorView mStoreDashBoardOneIndigator;

    @BindView(R.id.store_dashboard_two_view_pager)
    ViewPager mStoreDashBoardTwoViewPager;

    @BindView(R.id.store_dashboard_two_indigator)
    com.rd.PageIndicatorView mStoreDashBoardTwoIndigator;

    @BindView(R.id.store_dashboard_three_view_pager)
    ViewPager mStoreDashBoardThreeViewPager;

    @BindView(R.id.store_dashboard_three_indigator)
    com.rd.PageIndicatorView mStoreDashBoardThreeIndigator;

    @BindView(R.id.store_dashboard_four_view_pager)
    ViewPager mStoreDashBoardFourViewPager;

    @BindView(R.id.store_dashboard_four_indigator)
    com.rd.PageIndicatorView mStoreDashBoardFourIndigator;

    @BindView(R.id.store_dashboard_category_rec_view)
    RecyclerView mDashboardCategoryRecView;

    @BindView(R.id.store_dashboard_top_brands_rec_view)
    RecyclerView mStoreDashboardTopBrandsRecView;

    @BindView(R.id.store_dashboard_product_list_rec_view)
    RecyclerView mStoreDashboardProductListRecView;

    @BindView(R.id.store_dashboard_recent_txt_lay)
    RelativeLayout mStoreDashboardRecentTxtLay;

    @BindView(R.id.store_dashboard_recent_rec_view)
    RecyclerView mStoreDashboardRecentRecView;

    @BindView(R.id.bottom_notification_lay)
    RelativeLayout mBottomBadgeLay;

    @BindView(R.id.bottom_notification_txt)
    TextView mBottomNotificationTxt;

    UserDetailsEntity mUserDetailsEntityRes;

    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;
    final long PERIOD_MS = 3000;
    int count = 0;

//    int currentPageTwo = 0;
//
//    int currentPageThree = 0;
//
//    int currentPageFour = 0;

    StoreViewPagerAdapter mDashBoardOneAdapter;

    ArrayList<String> mDashBoardOneList;

    StoreViewPagerAdapter mDashBoardTwoAdapter;

    ArrayList<String> mDashBoardTwoList;

    StoreViewPagerAdapter mDashBoardThreeAdapter;

    ArrayList<String> mDashBoardThreeList;

    StoreViewPagerAdapter mDashBoardFourAdapter;

    ArrayList<String> mDashBoardFourList;

    StoreDashboardCatogeryAdapter mStoreCatogeryAdapter;

    StoreTopBrandsAdapter mStoreTopBrandsAdapter;

    StoreProductListAdapter mStoreProductListAdapter;

    APICommonInterface mApiCommonInterface;

    HashMap<String,String> mProductIdist = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_store_dashboard_screen);

        initView();
    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mStoreDashBoardParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        mApiCommonInterface = APIRequestHandler.getClient().create(APICommonInterface.class);

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(StoreDashboardScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        String idJson = PreferenceUtil.getStringValue(StoreDashboardScreen.this,AppConstants.STORE_RECENT_PRODUCT);
        Type type = new TypeToken<HashMap<String,String>>(){}.getType();
        mProductIdist = gson.fromJson(idJson,type);

        getLanguage();

        mDashBoardOneList = new ArrayList<>();
        mDashBoardTwoList = new ArrayList<>();
        mDashBoardThreeList = new ArrayList<>();
        mDashBoardFourList = new ArrayList<>();

        setHeader();

        getDashBoardApiCall();

        getRecentProductListApi();

        getStoreCartApiCall();

        AppConstants.STORE_PRODUCT_REFRESH = "REFRESH";
    }

    @OnClick({R.id.store_men_fashion_view_all_txt,R.id.store_top_brands_view_all_txt,R.id.header_search_heart_img,R.id.store_bottom_home_img,R.id.store_bottom_cart_img,R.id.store_header_search_par_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.store_men_fashion_view_all_txt:
                AppConstants.STORE_BANNER_ID = "8";
                AppConstants.STORE_TYPE_ID = "1";
                AppConstants.STORE_SEARCH = "";
                nextScreen(StoreProductListScreen.class,true);
                break;
            case R.id.store_top_brands_view_all_txt:
                AppConstants.STORE_BRANDS_AND_CATEGORY = "BRANDS";
                nextScreen(StoreSubCatogeryScreen.class,true);
                break;
            case R.id.header_search_heart_img:
                nextScreen(StoreWishListScreen.class,true);
                break;
            case R.id.store_bottom_home_img:
                previousScreen(BottomNavigationScreen.class,true);
                break;
            case R.id.store_bottom_cart_img:
                nextScreen(CartScreen.class,true);
                break;
            case R.id.store_header_search_par_lay:
                nextScreen(StoreSearchScreen.class,true);
                break;
        }
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.store_dashboard_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.store_dashboard_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    public void addToWishListApiCall(String ProductId,String SellerId){
        if (NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.storeAddToWishListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),ProductId,"-1","-1",SellerId).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {
                    if (response.body() != null && response.isSuccessful()){
                        initView();
                        DialogManager.getInstance().showAlertPopup(StoreDashboardScreen.this, getResources().getString(R.string.product_added_to_wishlist), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    addToWishListApiCall(ProductId,SellerId);
                }
            });
        }
    }

    public void removeToWishListApiCall(String ProductId,String SellerId){
        if (NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.storeRemoveToWishListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),ProductId,"-1","-1",SellerId).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {
                    if (response.body() != null && response.isSuccessful()){
                        initView();
                        DialogManager.getInstance().showAlertPopup(StoreDashboardScreen.this, getResources().getString(R.string.product_removed_from_wishlist), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    removeToWishListApiCall(ProductId,SellerId);
                }
            });
        }
    }

    public void getRecentProductListApi(){

        ArrayList<IdStringEntity> idStringList = new ArrayList<>();

        GetRecentProductEntity mRecenetProductEntity = new GetRecentProductEntity();

        if (mProductIdist != null){
            Set keys = mProductIdist.keySet();
            Iterator itr = keys.iterator();

            String key;
            String value;
            while(itr.hasNext())
            {
                IdStringEntity idStringEntity = new IdStringEntity();

                key = (String)itr.next();
                value = (String)mProductIdist.get(key);
                System.out.println(key + " - "+ value);
                idStringEntity.setId(value);
                idStringList.add(idStringEntity);
            }
        }

        mRecenetProductEntity.setId(idStringList);
        mRecenetProductEntity.setCartId(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID));
        mRecenetProductEntity.setKey("List");

        if (NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.getRecentProductList(mRecenetProductEntity).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {

                    if (response.body() != null && response.isSuccessful()){
                        setRecentProductListAdapter(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getRecentProductListApi();
                }
            });
        }

    }

    public void getStoreCartApiCall() {
        if (NetworkUtil.isNetworkAvailable(this)){
            APIRequestHandler.getInstance().getStoreCartListApi(PreferenceUtil.getStringValue(this,AppConstants.CARD_ID),"List",this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStoreCartApiCall();
                }
            });
        }
    }


    public void setHeader(){

        mStoreSearchLay.setVisibility(View.VISIBLE);
        mStoreLay.setVisibility(View.GONE);

    }

    public void getDashBoardApiCall(){
        if (NetworkUtil.isNetworkAvailable(StoreDashboardScreen.this)){
            APIRequestHandler.getInstance().getDashBoardApiCall(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),StoreDashboardScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(StoreDashboardScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getDashBoardApiCall();
                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetStoreDashBoardResponse){
            GetStoreDashBoardResponse mResponse = (GetStoreDashBoardResponse)resObj;

            mDashBoardOneList = new ArrayList<>();
            for(int i=0; i<mResponse.getGetStoreDashBoard().size(); i++){
                mDashBoardOneList.add(AppConstants.IMAGE_BASE_URL+"Images/DashBoardImages/"+mResponse.getGetStoreDashBoard().get(i).getBannerImage());
            }

            mDashBoardTwoList = new ArrayList<>();
            for(int i=0; i<mResponse.getStoreDashBoard2().size(); i++){
                mDashBoardTwoList.add(AppConstants.IMAGE_BASE_URL+"Images/DashBoardImages/"+mResponse.getStoreDashBoard2().get(i).getBannerImage());
            }

            mDashBoardThreeList = new ArrayList<>();
            for(int i=0; i<mResponse.getStoreDashBoard3().size(); i++){
                mDashBoardThreeList.add(AppConstants.IMAGE_BASE_URL+"Images/DashBoardImages/"+mResponse.getStoreDashBoard3().get(i).getBannerImage());
            }

            mDashBoardFourList = new ArrayList<>();
            for(int i=0; i<mResponse.getStoreDashBoard4().size(); i++){
                mDashBoardFourList.add(AppConstants.IMAGE_BASE_URL+"Images/DashBoardImages/"+mResponse.getStoreDashBoard4().get(i).getBannerImage());
            }

            viewPagerOneDashBoard(mDashBoardOneList,mResponse.getGetStoreDashBoard());
            viewPagerTwoDashBoard(mDashBoardTwoList,mResponse.getStoreDashBoard2());
            viewPagerThreeDashBoard(mDashBoardThreeList,mResponse.getStoreDashBoard3());
            viewPagerFourDashBoard(mDashBoardFourList,mResponse.getStoreDashBoard4());

            setCategoryAdapter(mResponse.getDashboardCategories());
            setProductListAdapter(mResponse.getDashboardProducts());
            setStoreTopBrandsAdapter(mResponse.getRecentBrand());
        }
        if (resObj instanceof StoreCartResponse) {

            int countQtyCart = 0;
            StoreCartResponse mREsponse = (StoreCartResponse) resObj;

            for (int i = 0; i < mREsponse.getResult().size(); i++) {
                countQtyCart = countQtyCart + mREsponse.getResult().get(i).getQuantity();
            }

            mBottomNotificationTxt.setText(String.valueOf(countQtyCart));
            if (countQtyCart > 0) {
                mBottomBadgeLay.setVisibility(View.VISIBLE);
            } else {
                mBottomBadgeLay.setVisibility(View.GONE);

            }
        }
    }



    public void viewPagerOneDashBoard(ArrayList<String> image, ArrayList<GetStoreDashBoardEntity> getStoreDashBoard){
        mDashBoardOneAdapter = new StoreViewPagerAdapter(this,image,getStoreDashBoard);
        mStoreDashBoardOneViewPager.setAdapter(mDashBoardOneAdapter);
        mStoreDashBoardOneViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mStoreDashBoardOneIndigator.setViewPager(mStoreDashBoardOneViewPager);

        if (count == 0){
            count = 1;
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == image.size()) {
                        currentPage = 0;
                    }
                    mStoreDashBoardOneViewPager.setCurrentItem(currentPage++, true);
                }
            };

            timer = new Timer(); // This will create a new Thread
            timer.schedule(new TimerTask() { // task to be scheduled
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, DELAY_MS, PERIOD_MS);
        }


        //page change tracker
        mStoreDashBoardOneViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        trackScreenName("StoreDashBaord");
    }

    public void viewPagerTwoDashBoard(ArrayList<String> image, ArrayList<GetStoreDashBoardEntity> storeDashBoard2){
        mDashBoardTwoAdapter = new StoreViewPagerAdapter(this,image,storeDashBoard2);
        mStoreDashBoardTwoViewPager.setAdapter(mDashBoardTwoAdapter);
        mStoreDashBoardTwoViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mStoreDashBoardTwoIndigator.setViewPager(mStoreDashBoardTwoViewPager);


        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
//                if (currentPageTwo == image.size()) {
//                    currentPageTwo = 0;
//                }
//                mStoreDashBoardTwoViewPager.setCurrentItem(currentPageTwo++,true);

            }
        };

//        timer = new Timer(); // This will create a new Thread
//        timer.schedule(new TimerTask() { // task to be scheduled
//            @Override
//            public void run() {
//                handler.post(Update);
//            }
//        }, DELAY_MS, PERIOD_MS);

        //page change tracker
        mStoreDashBoardTwoViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        trackScreenName("StoreDashBaord");
    }

    public void viewPagerThreeDashBoard(ArrayList<String> image, ArrayList<GetStoreDashBoardEntity> storeDashBoard3){
        mDashBoardThreeAdapter = new StoreViewPagerAdapter(this,image,storeDashBoard3);
        mStoreDashBoardThreeViewPager.setAdapter(mDashBoardThreeAdapter);
        mStoreDashBoardThreeViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mStoreDashBoardThreeIndigator.setViewPager(mStoreDashBoardThreeViewPager);


        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
//                if (currentPageThree == image.size()) {
//                    currentPageThree = 0;
//                }
//                mStoreDashBoardThreeViewPager.setCurrentItem(currentPageThree++,true);

            }
        };

//        timer = new Timer(); // This will create a new Thread
//        timer.schedule(new TimerTask() { // task to be scheduled
//            @Override
//            public void run() {
//                handler.post(Update);
//            }
//        }, DELAY_MS, PERIOD_MS);

        //page change tracker
        mStoreDashBoardThreeViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        trackScreenName("StoreDashBaord");
    }

    public void viewPagerFourDashBoard(ArrayList<String> image, ArrayList<GetStoreDashBoardEntity> storeDashBoard4){
        mDashBoardFourAdapter = new StoreViewPagerAdapter(this,image,storeDashBoard4);
        mStoreDashBoardFourViewPager.setAdapter(mDashBoardFourAdapter);
        mStoreDashBoardFourViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mStoreDashBoardFourIndigator.setViewPager(mStoreDashBoardFourViewPager);


        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
//                if (currentPageFour == image.size()) {
//                    currentPageFour = 0;
//                }
//                mStoreDashBoardFourViewPager.setCurrentItem(currentPageFour++,true);

            }
        };

//        timer = new Timer(); // This will create a new Thread
//        timer.schedule(new TimerTask() { // task to be scheduled
//            @Override
//            public void run() {
//                handler.post(Update);
//            }
//        }, DELAY_MS, PERIOD_MS);

        //page change tracker
        mStoreDashBoardFourViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        trackScreenName("StoreDashBaord");
    }

    public void setCategoryAdapter(ArrayList<DashboardCategoriesEntity> mCategoeryList){

        mStoreCatogeryAdapter = new StoreDashboardCatogeryAdapter(this,mCategoeryList);
        mDashboardCategoryRecView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mDashboardCategoryRecView.setAdapter(mStoreCatogeryAdapter);

    }

    public void setStoreTopBrandsAdapter(ArrayList<RecentBrandEntity> mTopBrandsList){

        mStoreTopBrandsAdapter = new StoreTopBrandsAdapter(this,mTopBrandsList);
        mStoreDashboardTopBrandsRecView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        mStoreDashboardTopBrandsRecView.setAdapter(mStoreTopBrandsAdapter);

    }

    public void setProductListAdapter(ArrayList<DashboardProductsEntity> mProductList){

        mStoreProductListAdapter = new StoreProductListAdapter(this,mProductList,"Dashboard");
        mStoreDashboardProductListRecView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        mStoreDashboardProductListRecView.setAdapter(mStoreProductListAdapter);

    }

    public void setRecentProductListAdapter(ArrayList<DashboardProductsEntity> mRecentProductList){

        mStoreDashboardRecentTxtLay.setVisibility(mRecentProductList.size() > 0 ? View.VISIBLE : View.GONE);
        mStoreDashboardRecentRecView.setVisibility(mRecentProductList.size() > 0 ? View.VISIBLE : View.GONE);

        mStoreProductListAdapter = new StoreProductListAdapter(this,mRecentProductList,"Dashboard");
        mStoreDashboardRecentRecView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        mStoreDashboardRecentRecView.setAdapter(mStoreProductListAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (AppConstants.ORDER_TYPE_CHECK.equalsIgnoreCase("Store")) {
            previousScreen(BottomNavigationScreen.class,true);
        }
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }
}
