package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.adapter.NotificationAdapter;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.NotificationEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.NotificationResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationScreen extends BaseActivity {

    @BindView(R.id.notification_screen_par_lay)
    LinearLayout mNotificationScreenParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.notification_empty_txt)
            TextView mNotificationEmptyTxt;

    UserDetailsEntity mUserDetailsEntity;

    @BindView(R.id.notification_recycler_view)
    RecyclerView mNotificationRecyclerView;

    NotificationAdapter mNotificationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_notification_screen);
        initView();
    }

    public void initView(){
        ButterKnife.bind(this);

        setupUI(mNotificationScreenParLay);

        setHeader();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(NotificationScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntity = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        getNotificationListApi(mUserDetailsEntity.getUSER_ID());

    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.notification));
        mRightSideImg.setVisibility(View.INVISIBLE);

    }
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }
    }

    public void getNotificationListApi(String UserId){
        if (NetworkUtil.isNetworkAvailable(NotificationScreen.this)){
            APIRequestHandler.getInstance().getNotificationList(UserId,NotificationScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(NotificationScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getNotificationListApi(UserId);
                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof NotificationResponse){
            NotificationResponse mREsponse = (NotificationResponse)resObj;
            setNotificationAdapter(mREsponse.getResult());
        }
    }

    public void getLanguage(){

        if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.notification_screen_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.notification_screen_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
        }
    }

    /*Set Adapter for the Recycler view*/
    public void setNotificationAdapter(ArrayList<NotificationEntity> mNotificationList) {

        mNotificationEmptyTxt.setVisibility(mNotificationList.size() > 0 ? View.GONE : View.VISIBLE);
        mNotificationRecyclerView.setVisibility(mNotificationList.size() > 0 ? View.VISIBLE  : View.GONE);

        mNotificationAdapter = new NotificationAdapter(this, mNotificationList);
        mNotificationRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mNotificationRecyclerView.setAdapter(mNotificationAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
