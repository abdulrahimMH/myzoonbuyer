package com.qoltech.mzyoonbuyer.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.CustomizationTwoColorAdapter;
import com.qoltech.mzyoonbuyer.adapter.CustomizationTwoMaterialAdapter;
import com.qoltech.mzyoonbuyer.adapter.CustomizationTwoSubColorAdapter;
import com.qoltech.mzyoonbuyer.entity.ApicallidEntity;
import com.qoltech.mzyoonbuyer.entity.CustomizationColorsEntity;
import com.qoltech.mzyoonbuyer.entity.CustomizationMaterialsEntity;
import com.qoltech.mzyoonbuyer.entity.SubColorEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.CustomizationTwoApiCallModal;
import com.qoltech.mzyoonbuyer.modal.GetCustomizationTwoResponse;
import com.qoltech.mzyoonbuyer.modal.NewFlowCustomizationTwoApiCall;
import com.qoltech.mzyoonbuyer.modal.SubColorResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomizationTwoScreen extends BaseActivity {


    private CustomizationTwoMaterialAdapter mMaterialAdapter;
    private CustomizationTwoColorAdapter mColorAdapter;
    private CustomizationTwoSubColorAdapter mSubColorAdapter;

    private ArrayList<CustomizationMaterialsEntity> mMateriallList;
    private ArrayList<CustomizationColorsEntity> mColorList;

    @BindView(R.id.customization_two_par_lay)
    LinearLayout mCustomizationTwoParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.customize_material_recycler_view)
    RecyclerView mMaterialRecyclerView;

    @BindView(R.id.customize_color_recycler_view)
    RecyclerView mColorRecyclerView;

    @BindView(R.id.new_flow_material_wiz_lay)
    RelativeLayout mNewFlowMaterialWizLay;

    @BindView(R.id.material_wiz_lay)
    RelativeLayout mMaterialWizLay;

    private UserDetailsEntity mUserDetailsEntityRes;

    Dialog mSubColorDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_customization_two_screen);

        initView();
    }
    public void initView(){

        ButterKnife.bind(this);

        setupUI(mCustomizationTwoParLay);

        setHeader();

        mMateriallList = new ArrayList<>();
        mColorList = new ArrayList<>();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(CustomizationTwoScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
           getNewFlowCustomizationTwoApiCall();

        }else {
            getCustomizationTwoApiCall();

        }
        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            mMaterialWizLay.setVisibility(View.GONE);
            mNewFlowMaterialWizLay.setVisibility(View.VISIBLE);
        }else {
            mMaterialWizLay.setVisibility(View.VISIBLE);
            mNewFlowMaterialWizLay.setVisibility(View.GONE);
        }

    }

    @OnClick({R.id.header_left_side_img,R.id.customization_two_apply_filter_txt,R.id.customization_two_cancel_txt})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.customization_two_apply_filter_txt:
                previousScreen(GetMaterialSelectionScreen.class,true);
                break;
            case R.id.customization_two_cancel_txt:
                AppConstants.SEASONAL_ID = "";
                AppConstants.SEASONAL_NAME = "";

                AppConstants.PLACE_INDUSTRY_ID = "";
                AppConstants.PLACE_OF_INDUSTRY_NAME = "";

                AppConstants.THICKNESS_ID = "";
                AppConstants.THICKNESS_NAME = "";

                AppConstants.BRANDS_ID = "";
                AppConstants.BRANDS_NAME = "";

                AppConstants.MATERIAL_ID = "";
                AppConstants.MATERIAL_TYPE_NAME = "";

                AppConstants.COLOR_ID = "";
                AppConstants.COLOUR_NAME = "";

                AppConstants.SEASONAL_NAME = "";
                AppConstants.PLACE_OF_INDUSTRY_NAME = "";
                AppConstants.BRANDS_NAME = "";
                previousScreen(GetMaterialSelectionScreen.class,true);
                break;
        }
    }

    public void  getCustomizationTwoApiCall(){
        if (NetworkUtil.isNetworkAvailable(CustomizationTwoScreen.this)){
            ArrayList<ApicallidEntity> mBrandId = new ArrayList<>();
            ArrayList<ApicallidEntity> mMaterialId = new ArrayList<>();
            ArrayList<ApicallidEntity> mColorId = new ArrayList<>();
            ArrayList<ApicallidEntity> mThicknessId = new ArrayList<>();

            CustomizationTwoApiCallModal mModal = new CustomizationTwoApiCallModal();


            if (AppConstants.BRANDS_ID.equalsIgnoreCase("")){
                ApicallidEntity mBrandEntityId = new ApicallidEntity();

                mBrandEntityId.setId("1");

                mBrandId.add(mBrandEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.BRANDS_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mBrandEntityId = new ApicallidEntity();

                    mBrandEntityId.setId(items.get(i));

                    mBrandId.add(mBrandEntityId);
                }

            }
            if (AppConstants.MATERIAL_ID.equalsIgnoreCase("")){
                ApicallidEntity mMaterialEntityId = new ApicallidEntity();

                mMaterialEntityId.setId("1");

                mMaterialId.add(mMaterialEntityId);


            }else {
                List<String> items = Arrays.asList(AppConstants.MATERIAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mMaterialEntityId = new ApicallidEntity();

                    mMaterialEntityId.setId(items.get(i));

                    mMaterialId.add(mMaterialEntityId);

                }

            }
            if (AppConstants.COLOR_ID.equalsIgnoreCase("")){
                ApicallidEntity mColorEntityId = new ApicallidEntity();

                mColorEntityId.setId("1");

                mColorId.add(mColorEntityId);

            }else {
                List<String> items = Arrays.asList(AppConstants.COLOR_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mColorEntityId = new ApicallidEntity();

                    mColorEntityId.setId(items.get(i));

                    mColorId.add(mColorEntityId);
                }

            }
            if (AppConstants.THICKNESS_ID.equalsIgnoreCase("")){
                ApicallidEntity mThicknessEntityId = new ApicallidEntity();

                mThicknessEntityId.setId("1");

                mThicknessId.add(mThicknessEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.THICKNESS_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mThicknessEntityId = new ApicallidEntity();

                    mThicknessEntityId.setId(items.get(i));

                    mThicknessId.add(mThicknessEntityId);
                }

            }

            mModal.setBrandId(mBrandId);
            mModal.setMaterialTypeId(mMaterialId);
            mModal.setColorId(mColorId);
            mModal.setThicknessId(mThicknessId);
            mModal.setDressType(AppConstants.SUB_DRESS_TYPE_ID);

            APIRequestHandler.getInstance().getCustomizationTwoApi(mModal,CustomizationTwoScreen.this);

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationTwoScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCustomizationTwoApiCall();
                }
            });
        }

    }

    public void  getNewFlowCustomizationTwoApiCall(){
        if (NetworkUtil.isNetworkAvailable(CustomizationTwoScreen.this)){

            ArrayList<ApicallidEntity> mBrandId = new ArrayList<>();
            ArrayList<ApicallidEntity> mMaterialId = new ArrayList<>();
            ArrayList<ApicallidEntity> mColorId = new ArrayList<>();
            ArrayList<ApicallidEntity> mThicknessId = new ArrayList<>();

            NewFlowCustomizationTwoApiCall mModal = new NewFlowCustomizationTwoApiCall();


            if (AppConstants.BRANDS_ID.equalsIgnoreCase("")){
                ApicallidEntity mBrandEntityId = new ApicallidEntity();

                mBrandEntityId.setId("1");

                mBrandId.add(mBrandEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.BRANDS_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mBrandEntityId = new ApicallidEntity();

                    mBrandEntityId.setId(items.get(i));

                    mBrandId.add(mBrandEntityId);
                }

            }
            if (AppConstants.MATERIAL_ID.equalsIgnoreCase("")){
                ApicallidEntity mMaterialEntityId = new ApicallidEntity();

                mMaterialEntityId.setId("1");

                mMaterialId.add(mMaterialEntityId);


            }else {
                List<String> items = Arrays.asList(AppConstants.MATERIAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mMaterialEntityId = new ApicallidEntity();

                    mMaterialEntityId.setId(items.get(i));

                    mMaterialId.add(mMaterialEntityId);

                }

            }
            if (AppConstants.COLOR_ID.equalsIgnoreCase("")){
                ApicallidEntity mColorEntityId = new ApicallidEntity();

                mColorEntityId.setId("1");

                mColorId.add(mColorEntityId);

            }else {
                List<String> items = Arrays.asList(AppConstants.COLOR_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mColorEntityId = new ApicallidEntity();

                    mColorEntityId.setId(items.get(i));

                    mColorId.add(mColorEntityId);
                }

            }
            if (AppConstants.THICKNESS_ID.equalsIgnoreCase("")){
                ApicallidEntity mThicknessEntityId = new ApicallidEntity();

                mThicknessEntityId.setId("1");

                mThicknessId.add(mThicknessEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.THICKNESS_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mThicknessEntityId = new ApicallidEntity();

                    mThicknessEntityId.setId(items.get(i));

                    mThicknessId.add(mThicknessEntityId);
                }

            }

            mModal.setTailorId(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId());
            mModal.setBrandId(mBrandId);
            mModal.setMaterialTypeId(mMaterialId);
            mModal.setColorId(mColorId);
            mModal.setThicknessId(mThicknessId);
            mModal.setDressType(AppConstants.DRESS_TYPE_ID);

            APIRequestHandler.getInstance().getNewFlowCustomizationTwoApi(mModal,CustomizationTwoScreen.this);

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationTwoScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getNewFlowCustomizationTwoApiCall();
                }
            });
        }
    }

    public void getSubColorApiCall(String ColorId){

        if (NetworkUtil.isNetworkAvailable(CustomizationTwoScreen.this)){
            if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                APIRequestHandler.getInstance().getSubColorApiCall(ColorId,String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId()),CustomizationTwoScreen.this);

            }else {
                APIRequestHandler.getInstance().getSubColorApiCall(ColorId,"0",CustomizationTwoScreen.this);

            }
        }else{
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationTwoScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getSubColorApiCall(ColorId);
                }
            });
        }

    }
    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.material_selection));
        mRightSideImg.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetCustomizationTwoResponse){
            GetCustomizationTwoResponse mResponse = (GetCustomizationTwoResponse)resObj;

            setMaterialAdapter(mResponse.getResult().getMaterials());
            setColorAdapter(mResponse.getResult().getColors());
        }

        if (resObj instanceof SubColorResponse) {
            SubColorResponse mREsponse = (SubColorResponse) resObj;

            getSubColorDialog(mREsponse.getResult());
        }
    }

    public void getSubColorDialog(ArrayList<SubColorEntity> result){

        if (!AppConstants.COLOR_ID.equalsIgnoreCase("")){
            List<String> items = Arrays.asList(AppConstants.COLOR_ID.split("\\s*,\\s*"));

            for (int i=0 ;i<items.size(); i++){
                for(int j=0; j<result.size(); j++){
                    if (result.get(j).getId() == Integer.parseInt(items.get(i))){
                        result.get(j).setChecked(true);
                    }
                }
            }
        }

        alertDismiss(mSubColorDialog);
        mSubColorDialog = getDialog(CustomizationTwoScreen.this, R.layout.pop_up_customization_color);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mSubColorDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mSubColorDialog.findViewById(R.id.pop_up_customization_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(mSubColorDialog.findViewById(R.id.pop_up_customization_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        Button mDoneBtn;
        RecyclerView mSubColorRecyclerView;
        TextView mEmptyTxt;

        /*Init view*/
        mDoneBtn = mSubColorDialog.findViewById(R.id.pop_up_done_btn);
        mSubColorRecyclerView = mSubColorDialog.findViewById(R.id.pop_up_color_recycler_view);
        mEmptyTxt = mSubColorDialog.findViewById(R.id.pop_up_sub_clr_txt);

        mEmptyTxt.setVisibility(result.size() > 0 ? View.GONE : View.VISIBLE);
        mSubColorRecyclerView.setVisibility(result.size() > 0 ? View.VISIBLE : View.GONE);

        mSubColorAdapter = new CustomizationTwoSubColorAdapter(this, result);
        mSubColorRecyclerView.setLayoutManager(new GridLayoutManager(this,5 ,GridLayoutManager.VERTICAL, false));
        mSubColorRecyclerView.setAdapter(mSubColorAdapter);

        mDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSubColorDialog.dismiss();
            }
        });

        alertShowing(mSubColorDialog);
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    /*Set Adapter for the Recycler view*/
    public void setMaterialAdapter(ArrayList<CustomizationMaterialsEntity> mMaterialList) {

        if (mMaterialAdapter == null) {

            if (!AppConstants.MATERIAL_ID.equalsIgnoreCase("")){
                List<String> items = Arrays.asList(AppConstants.MATERIAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    for (int j=0; j<mMaterialList.size(); j++){
                        if (mMaterialList.get(j).getId() == Integer.parseInt(items.get(i))){
                            mMaterialList.get(j).setChecked(true);
                        }
                    }
                }
                if (AppConstants.MATERIAL_TYPE_NAME.equalsIgnoreCase("all material type")){
                    for (int j=0; j<mMaterialList.size(); j++){
                            mMaterialList.get(j).setChecked(true);
                    }
                }
            }

            mMaterialAdapter = new CustomizationTwoMaterialAdapter(this,mMaterialList);
            mMaterialRecyclerView.setLayoutManager(new GridLayoutManager(this,2,GridLayoutManager.HORIZONTAL, false));
            mMaterialRecyclerView.setAdapter(mMaterialAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mMaterialAdapter.notifyDataSetChanged();
                }
            });
        }
    }
    /*Set Adapter for the Recycler view*/
    public void setColorAdapter(ArrayList<CustomizationColorsEntity> mColorList) {

        if (mColorAdapter == null) {

            mColorAdapter = new CustomizationTwoColorAdapter(this,mColorList);
            mColorRecyclerView.setLayoutManager(new GridLayoutManager(this,2,GridLayoutManager.HORIZONTAL, false));
            mColorRecyclerView.setAdapter(mColorAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mColorAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.customization_two_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.customization_two_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
}
