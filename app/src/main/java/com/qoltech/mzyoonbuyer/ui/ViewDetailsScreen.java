package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.CustomizationColorAdapter;
import com.qoltech.mzyoonbuyer.adapter.ViewDetailPagerAdapter;
import com.qoltech.mzyoonbuyer.entity.GetColorByIdEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.ViewDetailsNewFlowResponse;
import com.qoltech.mzyoonbuyer.modal.ViewDetailsResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ZoomOutPageTransformer;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewDetailsScreen extends BaseActivity {
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.view_details_par_lay)
    LinearLayout mViewDetailsParLay;

    @BindView(R.id.view_details_pattern_name_txt)
    TextView mViewDetailsPatternNameTxt;

    @BindView(R.id.view_details_seasonal_name_txt)
    TextView mViewDetailsSeasonalNameTxt;

    @BindView(R.id.view_details_place_industry_name_txt)
    TextView mViewDetailsPlaceIndustryNameTxt;

    @BindView(R.id.view_details_brands_name_txt)
    TextView mViewDetailsBrandsNameTxt;

    @BindView(R.id.view_details_material_type_name_txt)
    TextView mViewDetailsMaterialTypeNameTxt;

    @BindView(R.id.view_details_view_pager)
    ViewPager mViewDetailsViewpager;

    @BindView(R.id.pageIndicatorView)
    com.rd.PageIndicatorView mPageIndicator;

    @BindView(R.id.customization_three_view_details_btn)
    RelativeLayout mCustomizationThreeViewDetailsBtn;

    @BindView(R.id.view_details_thickness_name_txt)
            TextView mViewDetailsThicknessNameTxt;

    @BindView(R.id.customization_three_review_txt)
            TextView mCustomizationThreeReviewTxt;

    @BindView(R.id.customization_three_rating_bar)
    RatingBar mCustomizationThreeRatingBar;

    @BindView(R.id.customization_color_recycler_view)
    RecyclerView mCustomizationColorRecyclerView;

    @BindView(R.id.view_detai_material_average_rating_txt)
            TextView mViewDetailsMaterialAverageRatingTxt;

    @BindView(R.id.view_details_empty_img_txt)
            TextView mViewDetailsEmptyImgTxt;

    @BindView(R.id.view_details_empty_img)
            ImageView mViewDetailsEmtptyImt;

    ArrayList<String> mImageList;

    ViewDetailPagerAdapter mViewDetailAdapter;

    CustomizationColorAdapter mColorAdapter;

    private UserDetailsEntity mUserDetailsEntityRes;

    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;
    final long PERIOD_MS = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_view_detauils_screen);

        initView();

    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mViewDetailsParLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(ViewDetailsScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        mImageList = new ArrayList<>();

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")||AppConstants.ORDER_SUB_TYPE.equalsIgnoreCase("TailorMaterial")){
                getNewFlowViewDetailApiCall();
        }else {
                getViewDetailApiCall();
        }

        if (AppConstants.VIEW_DETAILS_RES.equalsIgnoreCase("GONE")){
            mCustomizationThreeViewDetailsBtn.setVisibility(View.GONE);

        }else {
            mCustomizationThreeViewDetailsBtn.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.customization_three_view_details_btn,R.id.header_left_side_img,R.id.view_details_rating_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.customization_three_view_details_btn:
                nextScreen(CustomizationThreeScreen.class,true);
                break;
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.view_details_rating_lay:
                AppConstants.MATERIAL_REVIEW = "true";
                nextScreen(RatingScreen.class,true);
                break;
        }
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.material_selection));
        mRightSideImg.setVisibility(View.INVISIBLE);

    }

    public void getViewDetailApiCall(){
        if (NetworkUtil.isNetworkAvailable(ViewDetailsScreen.this)){
            APIRequestHandler.getInstance().getViewDetailsApiCall(AppConstants.PATTERN_ID,ViewDetailsScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ViewDetailsScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getViewDetailApiCall();
                }
            });
        }
    }

    public void getNewFlowViewDetailApiCall(){
        if (NetworkUtil.isNetworkAvailable(ViewDetailsScreen.this)){
            if (AppConstants.APPROVED_TAILOR_ID.equalsIgnoreCase("0")){
                APIRequestHandler.getInstance().getNewFlowViewDetailsApiCall(String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId()),AppConstants.PATTERN_ID,ViewDetailsScreen.this);
            }else {
                APIRequestHandler.getInstance().getNewFlowViewDetailsApiCall(AppConstants.APPROVED_TAILOR_ID,AppConstants.PATTERN_ID,ViewDetailsScreen.this);

            }
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ViewDetailsScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getNewFlowViewDetailApiCall();
                }
            });
        }
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.view_details_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.view_details_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof ViewDetailsResponse){
            ViewDetailsResponse mResponse = (ViewDetailsResponse)resObj;

            if (mResponse.getResult().getPatternImg().size() >0){
              for(int i=0; i<mResponse.getResult().getPatternImg().size(); i++){
                  mImageList.add(AppConstants.IMAGE_BASE_URL+"Images/Pattern/"+mResponse.getResult().getPatternImg().get(i).getImageName());
              }
                viewPagerGet(mImageList);
                mViewDetailsEmptyImgTxt.setVisibility(View.GONE);
                mViewDetailsEmtptyImt.setVisibility(View.GONE);
                mPageIndicator.setVisibility(View.VISIBLE);
            }else {
                mViewDetailsEmptyImgTxt.setVisibility(View.VISIBLE);
                mViewDetailsEmtptyImt.setVisibility(View.VISIBLE);
                mPageIndicator.setVisibility(View.GONE);
            }
                if (mResponse.getResult().getGetpattternById().size()>0){
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        mViewDetailsPatternNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPatternInArabic());
                        mViewDetailsPlaceIndustryNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInArabic());
                        mViewDetailsBrandsNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInArabic());
                        mViewDetailsMaterialTypeNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getMaterialInArabic());
                    }else {
                        mViewDetailsPatternNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPatternInEnglish());
                        mViewDetailsPlaceIndustryNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInEnglish());
                        mViewDetailsBrandsNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInEnglish());
                        mViewDetailsMaterialTypeNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getMaterialInEnglish());
                    }

                }

            if(mResponse.getResult().getGetColorById().size()>0){
                setColorAdapter(mResponse.getResult().getGetColorById());

            }
            if (mResponse.getResult().getGetThickness().size() > 0){
                mViewDetailsThicknessNameTxt.setText(mResponse.getResult().getGetThickness().get(0).getThickness());
            }
            if (mResponse.getResult().getReviewCount().size() > 0){
                mCustomizationThreeReviewTxt.setText("("+String.valueOf(mResponse.getResult().getReviewCount().get(0).getReviewCount())+")"+getResources().getString(R.string.reviews));
            }
            if (mResponse.getResult().getAverageRating().size() > 0){
                NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                DecimalFormat formatter = (DecimalFormat)nf;
                formatter.applyPattern("######.#");
                mViewDetailsMaterialAverageRatingTxt.setText(String.valueOf(formatter.format(mResponse.getResult().getAverageRating().get(0).getAvgRating())));
                mCustomizationThreeRatingBar.setRating(Float.parseFloat(String.valueOf(mResponse.getResult().getAverageRating().get(0).getAvgRating())));
            }
        }

        if (resObj instanceof ViewDetailsNewFlowResponse){
            ViewDetailsNewFlowResponse mResponse = (ViewDetailsNewFlowResponse)resObj;

            if (mResponse.getResult().getPatternImg().size() >0){
                for(int i=0; i<mResponse.getResult().getPatternImg().size(); i++){
                    mImageList.add(AppConstants.IMAGE_BASE_URL+"Images/Pattern/"+mResponse.getResult().getPatternImg().get(i).getImageName());
                }
                viewPagerGet(mImageList);
                mViewDetailsEmptyImgTxt.setVisibility(View.GONE);
                mViewDetailsEmtptyImt.setVisibility(View.GONE);
                mPageIndicator.setVisibility(View.VISIBLE);
            }else {
                mViewDetailsEmptyImgTxt.setVisibility(View.VISIBLE);
                mViewDetailsEmtptyImt.setVisibility(View.VISIBLE);
                mPageIndicator.setVisibility(View.GONE);
            }

            if (mResponse.getResult().getGetpattternById().size()>0){
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mViewDetailsPlaceIndustryNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInArabic());
                    mViewDetailsBrandsNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInArabic());
                    mViewDetailsMaterialTypeNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getMaterialInArabic());
                }else {
                    mViewDetailsPlaceIndustryNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInEnglish());
                    mViewDetailsBrandsNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInEnglish());
                    mViewDetailsMaterialTypeNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getMaterialInEnglish());
                }
            }

            if(mResponse.getResult().getGetColorById().size()>0){
                setColorAdapter(mResponse.getResult().getGetColorById());
            }

            if (mResponse.getResult().getGetMaterialName().size()>0){
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mViewDetailsPatternNameTxt.setText(mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInArabic());

                }else {
                    mViewDetailsPatternNameTxt.setText(mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInEnglish());

                }
            }
            if (mResponse.getResult().getGetThickness().size() > 0){
                mViewDetailsThicknessNameTxt.setText(mResponse.getResult().getGetThickness().get(0).getThickness());
            }
            if (mResponse.getResult().getReviewCount().size() > 0){
                mCustomizationThreeReviewTxt.setText("("+String.valueOf(mResponse.getResult().getReviewCount().get(0).getReviewCount())+")"+getResources().getString(R.string.reviews));
            }
            if (mResponse.getResult().getAverageRating().size() > 0){
                NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                DecimalFormat formatter = (DecimalFormat)nf;
                formatter.applyPattern("######.#");
                mViewDetailsMaterialAverageRatingTxt.setText(String.valueOf(formatter.format(mResponse.getResult().getAverageRating().get(0).getAvgRating())));
                mCustomizationThreeRatingBar.setRating(Float.parseFloat(String.valueOf(mResponse.getResult().getAverageRating().get(0).getAvgRating())));
            }

        }
    }

    public void setColorAdapter(ArrayList<GetColorByIdEntity> mList){

        mColorAdapter  = new CustomizationColorAdapter(ViewDetailsScreen.this,mList);
        mCustomizationColorRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayout.HORIZONTAL,false));
        mCustomizationColorRecyclerView.setAdapter(mColorAdapter);

    }

    public void viewPagerGet(ArrayList<String> image){
        mViewDetailAdapter = new ViewDetailPagerAdapter(this,image);
        mViewDetailsViewpager.setAdapter(mViewDetailAdapter);
        mViewDetailsViewpager.setPageTransformer(true, new ZoomOutPageTransformer());
        mPageIndicator.setViewPager(mViewDetailsViewpager);


        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == image.size()) {
                    currentPage = 0;
                }
                mViewDetailsViewpager.setCurrentItem(currentPage++,true);

            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);

        //page change tracker
        mViewDetailsViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        trackScreenName("ViewDetail");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!AppConstants.ORDER_DETAILS_ON_BACK.equalsIgnoreCase("ORDER_DETAIL")){
            previousScreen(GetMaterialSelectionScreen.class,true);
        }
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }
}
