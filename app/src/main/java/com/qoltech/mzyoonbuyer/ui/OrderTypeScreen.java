package com.qoltech.mzyoonbuyer.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.OrderTypeEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.OrderTypeResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderTypeScreen extends BaseActivity {

    @BindView(R.id.order_type_par_lay)
    LinearLayout mOrderTypeParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.own_material_direct_txt)
    TextView mOwnMaterialDirectTxt;

    @BindView(R.id.own_courier_txt)
    TextView mOwnMaterialCourierTxt;

    @BindView(R.id.companies_material_txt)
    TextView mCompaniesMaterialTxt;

    @BindView(R.id.own_delivery_bodyimg)
    ImageView mOwnDeliveryBodyImg;

    @BindView(R.id.own_courier_body_img)
    ImageView mOwnCourierBodyImg;

    @BindView(R.id.companies_material_body_img)
    ImageView mCompaniesMaterialBodyImg;

//    @BindView(R.id.own_delviery_img)
//    ImageView mOwnDeliveryImg;
//
//    @BindView(R.id.own_courier_img)
//    ImageView mOwnCourierImg;
//
//    @BindView(R.id.company_img)
//    ImageView mCompanyImg;

    @BindView(R.id.own_material_direct_delivery_lay)
            LinearLayout mOwnMaterialDirectLay;

    @BindView(R.id.own_material_courier_lay)
            LinearLayout mOwnMaterialCourierLay;

    @BindView(R.id.companies_material_lay)
            LinearLayout mCompaniesMaterialLay;

    @BindView(R.id.new_flow_material_wiz_lay)
            RelativeLayout mNewFlowMaterialWizLay;

    @BindView(R.id.material_wiz_lay)
            RelativeLayout mMaterialWizLay;

    @BindView(R.id.own_delivery_body_empty_lay)
            TextView mOwnDeliveryBodyEmptyLay;

    @BindView(R.id.own_courier_body_empty_lay)
    TextView mOwnCourierBodyEmptyLay;

    @BindView(R.id.companies_material_body_empty_lay)
    TextView mCompaniesMaterialBodyEmptyLay;

    ArrayList<OrderTypeEntity> mOrderTypeList;

    ArrayList<OrderTypeEntity> mOrderTypeNewList;

    private UserDetailsEntity mUserDetailsEntityRes;

    @BindView(R.id.companies_material_popular_one_txt)
            TextView mCompaniesMaterialPopularOneTxt;

    @BindView(R.id.companies_material_popular_second_txt)
            TextView mCompaniesMaterialPopularSecondTxt;

    @BindView(R.id.companies_material_popular_third_txt)
            TextView mCompaniesMaterialPopularThirdTxt;

    Dialog mHintDialog;

    int mCountInt = 1;

    String mOwnDeliveryEmptyStr = "",mOwnCourierEmptyStr = "" , mCompaniesMaterialEmptyStr = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_order_type_screen);

        initView();

    }

    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setupUI(mOrderTypeParLay);

        setHeader();

        AppConstants.SEASONAL_ID = "";
        AppConstants.SEASONAL_NAME = "";

        AppConstants.PLACE_INDUSTRY_ID = "";
        AppConstants.PLACE_OF_INDUSTRY_NAME = "";

        AppConstants.THICKNESS_ID = "";
        AppConstants.THICKNESS_NAME = "";

        AppConstants.BRANDS_ID = "";
        AppConstants.BRANDS_NAME = "";

        AppConstants.MATERIAL_ID = "";
        AppConstants.MATERIAL_TYPE_NAME = "";

        AppConstants.COLOR_ID = "";
        AppConstants.SUB_COLOR_ID = "";
        AppConstants.COLOUR_NAME = "";

        AppConstants.PATTERN_ID = "0";
        AppConstants.PATTERN_NAME = "";

        AppConstants.SEASONAL_OLD_ID = "";
        AppConstants.SEASONAL_OLD_NAME = "";

        AppConstants.PLACE_INDUSTRY_OLD_ID = "";
        AppConstants.PLACE_OF_INDUSTRY_OLD_NAME = "";

        AppConstants.THICKNESS_OLD_ID = "";
        AppConstants.THICKNESS_OLD_NAME = "";

        AppConstants.BRANDS_OLD_ID = "";
        AppConstants.BRANDS_OLD_NAME = "";

        AppConstants.MATERIAL_OLD_ID = "";
        AppConstants.MATERIAL_TYPE_OLD_NAME = "";

        AppConstants.CUST_COLOR_ID = new HashMap<>();
        AppConstants.CUSTOMIZATION_CLICK_ARRAY = new ArrayList<>();
        AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST = new ArrayList<>();
        AppConstants.CUSTOMIZATION_CLICKED_ARRAY = new ArrayList<>();

        mOrderTypeList = new ArrayList<>();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(OrderTypeScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            NewFlowOrderTypeApiCall();
        }else {
            OrderTypeApiCall();
        }
    }

    @OnClick({R.id.own_material_direct_delivery_lay, R.id.own_material_courier_lay,R.id.companies_material_lay,R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.own_material_direct_delivery_lay:
                if(mOrderTypeList.size()>0){
                    if (mOrderTypeList.get(0).isStatus()){
                        AppConstants.ORDER_TYPE_ID =String.valueOf(mOrderTypeList.get(0).getId());
                        AppConstants.DIRECT_ORDER_TYPE = String.valueOf(mOrderTypeList.get(0).getId());
                        AppConstants.ORDER_TYPE = String.valueOf(mOrderTypeList.get(0).getId());
                        AppConstants.ORDER_TYPE_NAME = mOrderTypeList.get(0).getHeaderInEnglish();
                        AppConstants.ORDER_TYPE_ARABIC_NAME = mOrderTypeList.get(0).getHeaderInArabic();
                        if (String.valueOf(mOrderTypeList.get(0).getId()).equalsIgnoreCase("3")){
                            nextScreen(GetMaterialSelectionScreen.class,true);

                        }else {
                            AppConstants.PATTERN_ID = "0";
                            AppConstants.PATTERN_NAME = "";
                            nextScreen(AddMaterialScreen.class,true);
                        }
                    }
                }
                break;
            case R.id.own_material_courier_lay:
                if (mOrderTypeList.size()>1){
                    if (mOrderTypeList.get(1).isStatus()) {
                        AppConstants.ORDER_TYPE_ID = String.valueOf(mOrderTypeList.get(1).getId());
                        AppConstants.DIRECT_ORDER_TYPE = String.valueOf(mOrderTypeList.get(1).getId());
                        AppConstants.ORDER_TYPE = String.valueOf(mOrderTypeList.get(1).getId());
                        AppConstants.ORDER_TYPE_NAME = mOrderTypeList.get(1).getHeaderInEnglish();
                        AppConstants.ORDER_TYPE_ARABIC_NAME = mOrderTypeList.get(1).getHeaderInArabic();

                        if (String.valueOf(mOrderTypeList.get(1).getId()).equalsIgnoreCase("3")){
                            nextScreen(GetMaterialSelectionScreen.class, true);

                        } else {
                            AppConstants.PATTERN_ID = "0";
                            AppConstants.PATTERN_NAME = "";
                            nextScreen(AddMaterialScreen.class, true);
                        }
                    }
                }
                break;
            case R.id.companies_material_lay:
                if(mOrderTypeList.size()>2){
                    if (mOrderTypeList.get(2).isStatus()) {
                        AppConstants.ORDER_TYPE_ID = String.valueOf(mOrderTypeList.get(2).getId());

                        AppConstants.DIRECT_ORDER_TYPE = String.valueOf(mOrderTypeList.get(2).getId());
                        AppConstants.ORDER_TYPE = String.valueOf(mOrderTypeList.get(2).getId());
                        AppConstants.ORDER_TYPE_NAME = mOrderTypeList.get(2).getHeaderInEnglish();
                        AppConstants.ORDER_TYPE_ARABIC_NAME = mOrderTypeList.get(2).getHeaderInArabic();
                        if (String.valueOf(mOrderTypeList.get(2).getId()).equalsIgnoreCase("3")){
                            nextScreen(GetMaterialSelectionScreen.class, true);

                        } else {
                            AppConstants.PATTERN_ID = "0";
                            AppConstants.PATTERN_NAME = "";
                            nextScreen(AddMaterialScreen.class, true);
                        }
                    }
                }
                break;
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }
    }

    public void OrderTypeApiCall(){

        if (NetworkUtil.isNetworkAvailable(OrderTypeScreen.this)){
            APIRequestHandler.getInstance().getOrderTypeAPICall(OrderTypeScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderTypeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    OrderTypeApiCall();
                }
            });
        }

    }

    public void NewFlowOrderTypeApiCall(){

        if (NetworkUtil.isNetworkAvailable(OrderTypeScreen.this)){
            APIRequestHandler.getInstance().getNewFlowOrderTypeApiCall(String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId()),OrderTypeScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderTypeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    OrderTypeApiCall();
                }
            });
        }

    }
    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.material_type));
        mRightSideImg.setVisibility(View.INVISIBLE);

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            mMaterialWizLay.setVisibility(View.GONE);
            mNewFlowMaterialWizLay.setVisibility(View.VISIBLE);
        }else {
            mMaterialWizLay.setVisibility(View.VISIBLE);
            mNewFlowMaterialWizLay.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof OrderTypeResponse){
            OrderTypeResponse mResponse = (OrderTypeResponse) resObj;

            mOrderTypeList = mResponse.getResult();

            if (!AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                for (int i=0 ;i < mOrderTypeList.size(); i++){
                    mOrderTypeList.get(i).setStatus(true);
                }
            }
                setOrderTypeImgTitle(mOrderTypeList);
        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    public void setOrderTypeImgTitle(ArrayList<OrderTypeEntity> OrderTypeList){

        mOwnMaterialDirectLay.setVisibility(View.VISIBLE);
        mOwnMaterialCourierLay.setVisibility(View.VISIBLE);
        mCompaniesMaterialLay.setVisibility(View.VISIBLE);
        mOwnDeliveryBodyImg.setVisibility(View.VISIBLE);
        mOwnCourierBodyImg.setVisibility(View.VISIBLE);
        mCompaniesMaterialBodyImg.setVisibility(View.VISIBLE);

            for (int i=0; i<OrderTypeList.size(); i++){
                if (!OrderTypeList.get(i).isStatus()){
                    if (i == 0){
                        mOwnDeliveryBodyEmptyLay.setVisibility(View.VISIBLE);
                        mOwnDeliveryBodyImg.setAlpha(30);
                        mCompaniesMaterialPopularOneTxt.setAlpha(1);
                    }else if (i == 1){
                        mOwnCourierBodyEmptyLay.setVisibility(View.VISIBLE);
                        mOwnCourierBodyImg.setAlpha(30);
                        mCompaniesMaterialPopularSecondTxt.setAlpha(1);

                    }else if (i==2){
                        mCompaniesMaterialBodyEmptyLay.setVisibility(View.VISIBLE);
                        mCompaniesMaterialBodyImg.setAlpha(30);
                        mCompaniesMaterialPopularThirdTxt.setAlpha(1);

                    }
                }else {
                    if (i == 0){
                        mOwnDeliveryBodyEmptyLay.setVisibility(View.GONE);
                    }else if (i == 1){
                        mOwnCourierBodyEmptyLay.setVisibility(View.GONE);
                    }else if (i==2){
                        mCompaniesMaterialBodyEmptyLay.setVisibility(View.GONE);
                    }
                }

                if (i == 0){
                    if (OrderTypeList.get(i).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                        mCompaniesMaterialPopularOneTxt.setVisibility(View.VISIBLE);
                    }else{
                        mCompaniesMaterialPopularOneTxt.setVisibility(View.GONE);

                    }
                }else if (i == 1){
                    if (OrderTypeList.get(i).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                        mCompaniesMaterialPopularSecondTxt.setVisibility(View.VISIBLE);

                    }else{
                        mCompaniesMaterialPopularSecondTxt.setVisibility(View.GONE);

                    }
                }else if (i == 2){
                    if (OrderTypeList.get(i).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                        mCompaniesMaterialPopularThirdTxt.setVisibility(View.VISIBLE);

                    }else{
                        mCompaniesMaterialPopularThirdTxt.setVisibility(View.GONE);

                    }
                }

        }

      if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
          for (int i=0; i < OrderTypeList.size(); i++ ){
              if (i==0){
                  if (OrderTypeList.get(0).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                      mOwnMaterialDirectTxt.setText(getResources().getString(R.string.choose_material));
                  }else {
                      mOwnMaterialDirectTxt.setText(OrderTypeList.get(0).getHeaderInArabic());

                  }
              }else if (i==1){
                  if (OrderTypeList.get(1).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                      mOwnMaterialCourierTxt.setText(getResources().getString(R.string.choose_material));
                  }else {
                      mOwnMaterialCourierTxt.setText(OrderTypeList.get(1).getHeaderInArabic());
                  }
              }else if (i==2){
                  if (OrderTypeList.get(2).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                      mCompaniesMaterialTxt.setText(getResources().getString(R.string.choose_material));
                  }else {
                      mCompaniesMaterialTxt.setText(OrderTypeList.get(2).getHeaderInArabic());
                  }
              }

          }
        }else {
          for (int i=0; i < OrderTypeList.size(); i++ ){
              if (i == 0){
                  if (OrderTypeList.get(0).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                      mOwnMaterialDirectTxt.setText(getResources().getString(R.string.choose_material));
                  }else {
                      mOwnMaterialDirectTxt.setText(OrderTypeList.get(0).getHeaderInEnglish());
                  }
              }else if (i==1){
                  if (OrderTypeList.get(1).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                      mOwnMaterialCourierTxt.setText(getResources().getString(R.string.choose_material));
                  }else {
                      mOwnMaterialCourierTxt.setText(OrderTypeList.get(1).getHeaderInEnglish());
                  }
              }else if (i==2){
                  if (OrderTypeList.get(2).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                      mCompaniesMaterialTxt.setText(getResources().getString(R.string.choose_material));
                  }else {
                      mCompaniesMaterialTxt.setText(OrderTypeList.get(2).getHeaderInEnglish());
                  }
              }
          }
      }

        for (int i=0; i < OrderTypeList.size(); i++ ) {

            if (i==0){
                try {
                    Glide.with(OrderTypeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/OrderType/"+OrderTypeList.get(0).getBodyImage())
                            .apply(new RequestOptions().placeholder(R.drawable.gender_background_img).error(R.drawable.gender_background_img))
                            .into(mOwnDeliveryBodyImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
            }else if (i==1){

                try {
                    Glide.with(OrderTypeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/OrderType/"+OrderTypeList.get(1).getBodyImage())
                            .apply(new RequestOptions().placeholder(R.drawable.gender_background_img).error(R.drawable.gender_background_img))
                            .into(mOwnCourierBodyImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
            }else if (i==2){

                try {
                    Glide.with(OrderTypeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/OrderType/"+OrderTypeList.get(2).getBodyImage())
                            .apply(new RequestOptions().placeholder(R.drawable.gender_background_img).error(R.drawable.gender_background_img))
                            .into(mCompaniesMaterialBodyImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

            }
        }

        if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")){
            getHintDialog();
        }
    }

    public void getHintDialog(){
        alertDismiss(mHintDialog);
        mHintDialog = getDialog(OrderTypeScreen.this, R.layout.pop_up_order_type_hint);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mHintDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.order_type_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);


        }else {
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.order_type_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        Button mSkipBtn,mBackBtn,mNextBtn;
        ImageView mOrderTypeOneImg,mOrderTypeTwoImg,mOrderTypeThreeImg;
        RelativeLayout mOrderOneLay,mOrderTwoLay,mOrderThreeLay;
        TextView mOrderTypeOneTxt,mOrderTypeTwoTxt,mOrderTypeThreeTxt;

        /*Init view*/
        mSkipBtn = mHintDialog.findViewById(R.id.skip_hint_btn);
        mBackBtn = mHintDialog.findViewById(R.id.back_hint_btn);
        mNextBtn = mHintDialog.findViewById(R.id.next_hint_btn);
        mOrderTypeOneImg = mHintDialog.findViewById(R.id.order_type_one_hint_img);
        mOrderTypeTwoImg = mHintDialog.findViewById(R.id.order_type_two_hint_img);
        mOrderTypeThreeImg = mHintDialog.findViewById(R.id.order_type_three_hint_img);

        mOrderTypeOneTxt = mHintDialog.findViewById(R.id.order_type_hint_one_txt);
        mOrderTypeTwoTxt = mHintDialog.findViewById(R.id.order_type_hint_two_txt);
        mOrderTypeThreeTxt = mHintDialog.findViewById(R.id.order_type_hint_three_txt);

        mOrderOneLay = mHintDialog.findViewById(R.id.order_type_one__par_lay);
        mOrderTwoLay = mHintDialog.findViewById(R.id.order_type_two__par_lay);
        mOrderThreeLay = mHintDialog.findViewById(R.id.order_type_three__par_lay);

        for (int i=0; i < mOrderTypeList.size(); i++ ) {

            if (i==0){
                try {
                    Glide.with(OrderTypeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/OrderType/"+mOrderTypeList.get(0).getBodyImage())
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mOrderTypeOneImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

            }else if (i==1){

                try {
                    Glide.with(OrderTypeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/OrderType/"+mOrderTypeList.get(1).getBodyImage())
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mOrderTypeTwoImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
            }else if (i==2){
                try {
                    Glide.with(OrderTypeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/OrderType/"+mOrderTypeList.get(2).getBodyImage())
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mOrderTypeThreeImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
            }
        }


        if (mCountInt == 1){
            mBackBtn.setVisibility(View.INVISIBLE);
            mOrderTwoLay.setVisibility(View.INVISIBLE);
            mOrderThreeLay.setVisibility(View.INVISIBLE);
        }

        if (mOrderTypeList.size() == 1) {
            mNextBtn.setText(getResources().getString(R.string.got_it));
            if (mOrderTypeList.get(0).getHeaderInEnglish().equalsIgnoreCase("Own Material-Direct Delivery")){
                mOrderTypeOneTxt.setText(getResources().getString(R.string.order_type_direct_hint));
            }else if (mOrderTypeList.get(0).getHeaderInEnglish().equalsIgnoreCase("Own Material-Courier the Material")){
                mOrderTypeOneTxt.setText(getResources().getString(R.string.order_type_courier_hint));

            }else if (mOrderTypeList.get(0).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                mOrderTypeOneTxt.setText(getResources().getString(R.string.order_type_companies_hint));

            }

        }else if (mOrderTypeList.size() == 2){
            if (mOrderTypeList.get(0).getHeaderInEnglish().equalsIgnoreCase("Own Material-Direct Delivery")){
                mOrderTypeOneTxt.setText(getResources().getString(R.string.order_type_direct_hint));
            }else if (mOrderTypeList.get(0).getHeaderInEnglish().equalsIgnoreCase("Own Material-Courier the Material")){
                mOrderTypeOneTxt.setText(getResources().getString(R.string.order_type_courier_hint));

            }else if (mOrderTypeList.get(0).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                mOrderTypeOneTxt.setText(getResources().getString(R.string.order_type_companies_hint));

            }else if (mOrderTypeList.get(1).getHeaderInEnglish().equalsIgnoreCase("Own Material-Direct Delivery")){
                mOrderTypeTwoTxt.setText(getResources().getString(R.string.order_type_direct_hint));
            }else if (mOrderTypeList.get(1).getHeaderInEnglish().equalsIgnoreCase("Own Material-Courier the Material")){
                mOrderTypeTwoTxt.setText(getResources().getString(R.string.order_type_courier_hint));

            }else if (mOrderTypeList.get(1).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                mOrderTypeTwoTxt.setText(getResources().getString(R.string.order_type_companies_hint));

            }
        }else if (mOrderTypeList.size() == 3){
            if (mOrderTypeList.get(0).getHeaderInEnglish().equalsIgnoreCase("Own Material-Direct Delivery")){
                mOrderTypeOneTxt.setText(getResources().getString(R.string.order_type_direct_hint));
            }else if (mOrderTypeList.get(0).getHeaderInEnglish().equalsIgnoreCase("Own Material-Courier the Material")){
                mOrderTypeOneTxt.setText(getResources().getString(R.string.order_type_courier_hint));

            }else if (mOrderTypeList.get(0).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                mOrderTypeOneTxt.setText(getResources().getString(R.string.order_type_companies_hint));

            }else if (mOrderTypeList.get(1).getHeaderInEnglish().equalsIgnoreCase("Own Material-Direct Delivery")){
                mOrderTypeTwoTxt.setText(getResources().getString(R.string.order_type_direct_hint));
            }else if (mOrderTypeList.get(1).getHeaderInEnglish().equalsIgnoreCase("Own Material-Courier the Material")){
                mOrderTypeTwoTxt.setText(getResources().getString(R.string.order_type_courier_hint));

            }else if (mOrderTypeList.get(1).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                mOrderTypeTwoTxt.setText(getResources().getString(R.string.order_type_companies_hint));

            }else if (mOrderTypeList.get(2).getHeaderInEnglish().equalsIgnoreCase("Own Material-Direct Delivery")){
                mOrderTypeThreeTxt.setText(getResources().getString(R.string.order_type_direct_hint));
            }else if (mOrderTypeList.get(2).getHeaderInEnglish().equalsIgnoreCase("Own Material-Courier the Material")){
                mOrderTypeThreeTxt.setText(getResources().getString(R.string.order_type_courier_hint));

            }else if (mOrderTypeList.get(2).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                mOrderTypeThreeTxt.setText(getResources().getString(R.string.order_type_companies_hint));

            }
        }

            mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountInt = --mCountInt;
                if (mCountInt == 1){
                    mBackBtn.setVisibility(View.INVISIBLE);
                    mOrderOneLay.setVisibility(View.VISIBLE);
                    mOrderTwoLay.setVisibility(View.INVISIBLE);
                    mOrderThreeLay.setVisibility(View.INVISIBLE);
                    if (mOrderTypeList.size() == 1){
                        mNextBtn.setText(getResources().getString(R.string.got_it));

                    }else {

                        mNextBtn.setText(getResources().getString(R.string.next));

                    }
                }else if (mCountInt == 2){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mOrderOneLay.setVisibility(View.INVISIBLE);
                    mOrderTwoLay.setVisibility(View.VISIBLE);
                    mOrderThreeLay.setVisibility(View.INVISIBLE);
                    if (mOrderTypeList.size() == 2){
                        mNextBtn.setText(getResources().getString(R.string.got_it));
                    }else {
                        mNextBtn.setText(getResources().getString(R.string.next));

                    }
                }else if (mCountInt == 3){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mOrderOneLay.setVisibility(View.INVISIBLE);
                    mOrderTwoLay.setVisibility(View.INVISIBLE);
                    mOrderThreeLay.setVisibility(View.VISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.got_it));
                }
            }
        });

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountInt = ++mCountInt;
                if (mCountInt == 1){
                    mBackBtn.setVisibility(View.INVISIBLE);
                    mOrderOneLay.setVisibility(View.VISIBLE);
                    mOrderTwoLay.setVisibility(View.INVISIBLE);
                    mOrderThreeLay.setVisibility(View.INVISIBLE);
                    if (mOrderTypeList.size() == 1) {
                        mNextBtn.setText(getResources().getString(R.string.got_it));

                    }else {
                        mNextBtn.setText(getResources().getString(R.string.next));

                    }
                }else if (mCountInt == 2){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mOrderOneLay.setVisibility(View.INVISIBLE);
                    mOrderTwoLay.setVisibility(View.VISIBLE);
                    mOrderThreeLay.setVisibility(View.INVISIBLE);
                    if (mOrderTypeList.size() == 1){
                        mHintDialog.dismiss();

                    }
                    else if (mOrderTypeList.size() == 2) {
                        mNextBtn.setText(getResources().getString(R.string.got_it));
                    }else {
                        mNextBtn.setText(getResources().getString(R.string.next));

                    }
                }else if (mCountInt == 3){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mOrderOneLay.setVisibility(View.INVISIBLE);
                    mOrderTwoLay.setVisibility(View.INVISIBLE);
                    mOrderThreeLay.setVisibility(View.VISIBLE);
                    if (mOrderTypeList.size() == 2){
                        mHintDialog.dismiss();

                    }
                    mNextBtn.setText(getResources().getString(R.string.got_it));
                }else if (mCountInt >=4){
                    mHintDialog.dismiss();
                }
            }
        });

        /*Set data*/
        mSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHintDialog.dismiss();
            }
        });



        alertShowing(mHintDialog);
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.order_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.order_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
                if(AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
                    previousScreen(TailorListScreen.class,true);

                }else {
                    previousScreen(OrderFlowType.class,true);
                }
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
