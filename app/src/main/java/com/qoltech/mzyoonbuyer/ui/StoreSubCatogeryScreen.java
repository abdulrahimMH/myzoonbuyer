package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.StoreAllBrandsAdapter;
import com.qoltech.mzyoonbuyer.adapter.StoreDashboardSubCatogeryAdapter;
import com.qoltech.mzyoonbuyer.entity.StoreAllBrandsEntity;
import com.qoltech.mzyoonbuyer.entity.StoreSubCatogeryEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.StoreCartResponse;
import com.qoltech.mzyoonbuyer.service.APICommonInterface;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreSubCatogeryScreen extends BaseActivity {

    @BindView(R.id.store_sub_catogery_par_lay)
    LinearLayout mStoreSubCatogeryParLay;

    @BindView(R.id.header_store_search_lay)
    RelativeLayout mStoreSearchLay;

    @BindView(R.id.header_store_lay)
    RelativeLayout mStoreLay;

    @BindView(R.id.header_store_catogery_icon)
    ImageView mHeaderStoreCatogeryImg;

    @BindView(R.id.header_store_catogery_txt)
    TextView mHeaderStoreCatogeryTxt;

    @BindView(R.id.header_store_whis_list_icon)
    ImageView mHeaderStoreWishListImg;

    @BindView(R.id.header_store_whis_list_txt)
    TextView mHeaderStoreWishListTxt;

    @BindView(R.id.stor_sub_catogery_rec_view)
    RecyclerView mStoreSubCaotgeryRecView;

    @BindView(R.id.bottom_notification_lay)
    RelativeLayout mBottomBadgeLay;

    @BindView(R.id.bottom_notification_txt)
    TextView mBottomNotificationTxt;

    @BindView(R.id.stor_sub_catogery_scroll_view_img)
            ImageView mStoreSubCatogeryScrollViewImg;

    UserDetailsEntity mUserDetailsEntityRes;

    StoreDashboardSubCatogeryAdapter mStoreSubCatogeryAdapter;

    StoreAllBrandsAdapter mStoreAllBrandsAdapter;

    APICommonInterface mApiCommonInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_store_sub_catogery_screen);

        initView();

    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mStoreSubCatogeryParLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        mApiCommonInterface = APIRequestHandler.getClient().create(APICommonInterface.class);

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(StoreSubCatogeryScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        if (AppConstants.STORE_BRANDS_AND_CATEGORY.equalsIgnoreCase("BRANDS")){
            getAllBrandsApiCall();
        }else {
            getSubCatogeryApiCall();
        }

        getStoreCartApiCall();

        AppConstants.STORE_PRODUCT_REFRESH = "REFRESH";
    }

    @OnClick({R.id.header_store_back_btn_img,R.id.header_store_catogery_lay,R.id.header_wish_list_par_lay,R.id.store_bottom_home_img,R.id.store_bottom_cart_img,R.id.header_store_search_par_lay,R.id.header_store_icon_par_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_store_back_btn_img:
                onBackPressed();
                break;
            case R.id.header_store_catogery_lay:
                nextScreen(StoreCategoryScreen.class,true);
                break;
            case R.id.header_wish_list_par_lay:
                nextScreen(StoreWishListScreen.class,true);
                break;
            case R.id.store_bottom_home_img:
                previousScreen(BottomNavigationScreen.class,true);
                break;
            case R.id.store_bottom_cart_img:
                nextScreen(CartScreen.class,true);
                break;
            case R.id.header_store_search_par_lay:
                nextScreen(StoreSearchScreen.class,true);
                break;
            case R.id.header_store_icon_par_lay:
                previousScreen(StoreDashboardScreen.class,true);
                break;
        }
    }

    public void getSubCatogeryApiCall(){
        DialogManager.getInstance().showProgress(this);
        if (NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.getStoreSubCatogeryApi("Products/GetSubCategories?CategoryId="+AppConstants.STORE_CATOGERY_ID).enqueue(new Callback<ArrayList<StoreSubCatogeryEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<StoreSubCatogeryEntity>> call, Response<ArrayList<StoreSubCatogeryEntity>> response) {
                    DialogManager.getInstance().hideProgress();
                    if (response.body() != null && response.isSuccessful()){
                        setStoreSubCatogeryAdapter(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<StoreSubCatogeryEntity>> call, Throwable t) {
                    DialogManager.getInstance().hideProgress();

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getSubCatogeryApiCall();
                }
            });
        }
    }

    public void getAllBrandsApiCall(){
        DialogManager.getInstance().showProgress(this);
        if (NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.getStoreViewAllBrandsApi("Products/GetViewAllBrand").enqueue(new Callback<ArrayList<StoreAllBrandsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<StoreAllBrandsEntity>> call, Response<ArrayList<StoreAllBrandsEntity>> response) {
                    DialogManager.getInstance().hideProgress();
                    if (response.body() != null && response.isSuccessful()){
                        setStoreAllBradndsAdapter(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<StoreAllBrandsEntity>> call, Throwable t) {
                    DialogManager.getInstance().hideProgress();

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAllBrandsApiCall();
                }
            });
        }
    }

    public void getStoreCartApiCall() {
        if (NetworkUtil.isNetworkAvailable(this)){
            APIRequestHandler.getInstance().getStoreCartListApi(PreferenceUtil.getStringValue(this,AppConstants.CARD_ID),"List",this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStoreCartApiCall();
                }
            });
        }
    }

    public void setHeader(){

        mStoreSearchLay.setVisibility(View.GONE);
        mStoreLay.setVisibility(View.VISIBLE);

        mHeaderStoreCatogeryImg.setBackgroundResource(R.drawable.store_catogery_white_icon);
        mHeaderStoreCatogeryTxt.setTextColor(getResources().getColor(R.color.white));

        mHeaderStoreWishListImg.setBackgroundResource(R.drawable.store_heart_white_icon);
        mHeaderStoreWishListTxt.setTextColor(getResources().getColor(R.color.white));
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.store_sub_catogery_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.store_sub_catogery_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    public void setStoreSubCatogeryAdapter(ArrayList<StoreSubCatogeryEntity> mProductList){

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);

        mStoreSubCatogeryAdapter = new StoreDashboardSubCatogeryAdapter(mProductList,this);
        mStoreSubCaotgeryRecView.setLayoutManager(new GridLayoutManager(this,2));
        mStoreSubCaotgeryRecView.setAdapter(mStoreSubCatogeryAdapter);

        mStoreSubCaotgeryRecView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = layoutManager.findLastVisibleItemPosition();

                if (lastPosition == mProductList.size()-1){
                    mStoreSubCatogeryScrollViewImg.setVisibility(View.GONE);
                }else {
                    mStoreSubCatogeryScrollViewImg.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    public void setStoreAllBradndsAdapter(ArrayList<StoreAllBrandsEntity> mStoreAllBrandsList){

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);

        mStoreAllBrandsAdapter = new StoreAllBrandsAdapter(mStoreAllBrandsList,this);
        mStoreSubCaotgeryRecView.setLayoutManager(new GridLayoutManager(this,2));
        mStoreSubCaotgeryRecView.setAdapter(mStoreAllBrandsAdapter);

        mStoreSubCaotgeryRecView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = layoutManager.findLastVisibleItemPosition();

                if (lastPosition == mStoreAllBrandsList.size()-1){
                    mStoreSubCatogeryScrollViewImg.setVisibility(View.GONE);
                }else {
                    mStoreSubCatogeryScrollViewImg.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof StoreCartResponse) {

            int countQtyCart = 0;
            StoreCartResponse mREsponse = (StoreCartResponse)resObj;

            for (int i=0 ; i<mREsponse.getResult().size(); i++){
                countQtyCart = countQtyCart + mREsponse.getResult().get(i).getQuantity();
            }

            mBottomNotificationTxt.setText(String.valueOf(countQtyCart));
            if (countQtyCart > 0){
                mBottomBadgeLay.setVisibility(View.VISIBLE);
            }else {
                mBottomBadgeLay.setVisibility(View.GONE);

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
