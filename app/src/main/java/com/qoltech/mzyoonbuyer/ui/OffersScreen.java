package com.qoltech.mzyoonbuyer.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.GetSpecialOfferAdapter;
import com.qoltech.mzyoonbuyer.adapter.OfferGenderAdapter;
import com.qoltech.mzyoonbuyer.adapter.OfferListAdapter;
import com.qoltech.mzyoonbuyer.adapter.OfferLocationAdapter;
import com.qoltech.mzyoonbuyer.entity.GenderEntity;
import com.qoltech.mzyoonbuyer.entity.GetAllCategoriesEntity;
import com.qoltech.mzyoonbuyer.entity.GetOfferCateogriesEntity;
import com.qoltech.mzyoonbuyer.entity.GetSpecialOfferEntity;
import com.qoltech.mzyoonbuyer.entity.GetStateEntity;
import com.qoltech.mzyoonbuyer.entity.OfferEntity;
import com.qoltech.mzyoonbuyer.entity.OfferGenderEntity;
import com.qoltech.mzyoonbuyer.entity.OfferLocationEntity;
import com.qoltech.mzyoonbuyer.entity.TypeIdEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GenderResponse;
import com.qoltech.mzyoonbuyer.modal.GetAllCategoriesResponse;
import com.qoltech.mzyoonbuyer.modal.GetCouponPriceResponse;
import com.qoltech.mzyoonbuyer.modal.GetOfferCategoriesResponse;
import com.qoltech.mzyoonbuyer.modal.GetOffersModal;
import com.qoltech.mzyoonbuyer.modal.GetSpecialOfferModal;
import com.qoltech.mzyoonbuyer.modal.GetStateResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OffersScreen extends BaseActivity {

    @BindView(R.id.offers_par_lay)
    LinearLayout mOffersParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.offer_list_recycler_view)
    RecyclerView mOfferListRecyclerView;

    @BindView(R.id.offer_filter_par_lay)
    RelativeLayout mOfferFilterParLay;

    @BindView(R.id.offer_stitching_txt)
    TextView mOfferStitchingTxt;

    @BindView(R.id.offer_store_txt)
    TextView mOfferStoreTxt;

    @BindView(R.id.offer_stitching_par_lay)
    RelativeLayout mOfferStitchingParLay;

    @BindView(R.id.offer_store_par_lay)
    RelativeLayout mOfferStoreParLay;

    private UserDetailsEntity mUserDetailsEntityRes;

    OfferListAdapter mOfferListAdapter;

    OfferGenderAdapter mOfferGenderAdapter;

    GetSpecialOfferAdapter mGetSpecialOfferAdapter;

    OfferLocationAdapter mOfferLocationAdapter;

    ArrayList<GenderEntity> mGenderList;

    ArrayList<GetSpecialOfferEntity> mSpeiclOfferList;

    ArrayList<GetStateEntity> mGetStateEntity;

    ArrayList<GetAllCategoriesEntity> mAllCategoriesList;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    int mDiscountType = 0,mCouponAppliesTo = 0;

    String mCouponType = "0";

    double minimumamount = 0,mMaximumDiscount = 0,mDiscountValue = 0;

    Dialog mFilterDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_offers_screen);

        initView();

    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mOffersParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        setHeader();

        getLanguage();

        getAllCategoriesApiCall();

        getOfferProductsApiCall(AppConstants.OFFER_CATEGORIES_ID);

        getGenderApiCall();

        getSpeicalOfferApiCall();

        getStateApiCall("1");

    }

    public void getAllCategoriesApiCall(){
        if (NetworkUtil.isNetworkAvailable(OffersScreen.this)){
            APIRequestHandler.getInstance().getAllCategoriesApi(OffersScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OffersScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAllCategoriesApiCall();
                }
            });
        }
    }

    public void getGenderApiCall(){

        if (NetworkUtil.isNetworkAvailable(OffersScreen.this)){
            APIRequestHandler.getInstance().getGenderAPICall(OffersScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OffersScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getGenderApiCall();
                }
            });
        }

    }

    public void getOfferCategoriesApiCall(String OfferId){
        if (NetworkUtil.isNetworkAvailable(OffersScreen.this)){
            APIRequestHandler.getInstance().getOfferCategoriesApi(OfferId,OffersScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OffersScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getOfferCategoriesApiCall(OfferId);
                }
            });
        }
    }

    public void getSpeicalOfferApiCall(){
        if (NetworkUtil.isNetworkAvailable(OffersScreen.this)){
            APIRequestHandler.getInstance().getSpecialOfferApiCall(OffersScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OffersScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getSpeicalOfferApiCall();
                }
            });
        }
    }

    public void getStateApiCall(final String stateId){
        if (NetworkUtil.isNetworkAvailable(OffersScreen.this)){
            APIRequestHandler.getInstance().getStateApiCall(stateId,OffersScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OffersScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStateApiCall(stateId);
                }
            });
        }
    }
    public void getOfferProductsApiCall(String OfferId){
        GetOffersModal modal = new GetOffersModal();

        ArrayList<OfferGenderEntity> offerGenderList = new ArrayList<>();
        ArrayList<OfferLocationEntity> offerLocationList = new ArrayList<>();
        ArrayList<OfferEntity> OfferList = new ArrayList<>();
        ArrayList<TypeIdEntity> typeIdList = new ArrayList<>();

        for (int i=0; i<AppConstants.OFFER_GENDER_LIST.size(); i++){
            OfferGenderEntity offerGenderEntity = new OfferGenderEntity();

            offerGenderEntity.setId(AppConstants.OFFER_GENDER_LIST.get(i));
            offerGenderList.add(offerGenderEntity);
        }

        for (int i=0; i<AppConstants.OFFER_LOCATION_LIST.size(); i++){
            OfferLocationEntity offerLocationEntity = new OfferLocationEntity();

            offerLocationEntity.setId(AppConstants.OFFER_LOCATION_LIST.get(i));
            offerLocationList.add(offerLocationEntity);
        }

        for (int i=0; i<AppConstants.OFFER_SELECTED_LIST.size(); i++){
            OfferEntity offerEntity = new OfferEntity();

            offerEntity.setId(AppConstants.OFFER_SELECTED_LIST.get(i));
            OfferList.add(offerEntity);
        }

        if (AppConstants.OFFER_GENDER_LIST.size() == 0) {
            OfferGenderEntity offerGenderEntity = new OfferGenderEntity();

            offerGenderEntity.setId(0);
            offerGenderList.add(offerGenderEntity);
        }

        if (AppConstants.OFFER_LOCATION_LIST.size() == 0){
            OfferLocationEntity offerLocationEntity = new OfferLocationEntity();

            offerLocationEntity.setId(0);
            offerLocationList.add(offerLocationEntity);
        }

        if (AppConstants.OFFER_SELECTED_LIST.size() == 0){
            OfferEntity offerEntity = new OfferEntity();

            offerEntity.setId(0);
            OfferList.add(offerEntity);
        }

        if (AppConstants.CHECK_OUT_OFFERS.equalsIgnoreCase("Tap to apply")){
            modal.setTailorId("0");
            TypeIdEntity typeIdEntity = new TypeIdEntity();
            typeIdEntity.setType("0");
            typeIdList.add(typeIdEntity);
            modal.setTypeId(typeIdList);
        }
        else {
            modal.setTailorId(AppConstants.APPROVED_TAILOR_ID);
            if (AppConstants.OFFER_CATEGORIES_ID.equalsIgnoreCase("3")){
                for (int i=0; i< AppConstants.GET_ITEM_CARD_LIST.size(); i++){
                    TypeIdEntity typeIdEntity = new TypeIdEntity();
                    typeIdEntity.setType(AppConstants.GET_ITEM_CARD_LIST.get(i).getProductId());
                    typeIdList.add(typeIdEntity);
                }
                if (AppConstants.GET_ITEM_CARD_LIST.size() == 0){
                    TypeIdEntity typeIdEntity = new TypeIdEntity();
                    typeIdEntity.setType("0");
                    typeIdList.add(typeIdEntity);
                }

            }else {
                TypeIdEntity typeIdEntity = new TypeIdEntity();
                typeIdEntity.setType(AppConstants.SUB_DRESS_TYPE_ID);
                typeIdList.add(typeIdEntity);
            }
            modal.setTypeId(typeIdList);
        }

        modal.setCoupanType(OfferId);
        modal.setGender(offerGenderList);
        modal.setLocation(offerLocationList);
        modal.setOfferType(OfferList);

        if (NetworkUtil.isNetworkAvailable(OffersScreen.this)){
            APIRequestHandler.getInstance().getOfferProductsApiCall(modal,OffersScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OffersScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getOfferProductsApiCall(OfferId);
                }
            });
        }
    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mRightSideImg.setVisibility(View.INVISIBLE);

        if (AppConstants.CHECK_OUT_OFFERS.equalsIgnoreCase("Tap to apply")){
            mOfferFilterParLay.setVisibility(View.VISIBLE);
        }else {
            mOfferFilterParLay.setVisibility(View.INVISIBLE);
        }
        if (AppConstants.OFFER_CATEGORIES_ID.equalsIgnoreCase("3")){
            mHeaderTxt.setText(getResources().getString(R.string.store_offer));

            if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                mOfferStitchingParLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
                mOfferStitchingTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                mOfferStoreParLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
                mOfferStoreTxt.setTextColor(getResources().getColor(R.color.white));
            }else {
                mOfferStitchingParLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                mOfferStitchingTxt.setTextColor(getResources().getColor(R.color.white));
                mOfferStoreParLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                mOfferStoreTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            }

        }else {
            mHeaderTxt.setText(getResources().getString(R.string.stitching_offer));

            if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                mOfferStitchingParLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                mOfferStitchingTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                mOfferStoreParLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                mOfferStoreTxt.setTextColor(getResources().getColor(R.color.white));
            }else {
                mOfferStitchingParLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
                mOfferStitchingTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                mOfferStoreParLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
                mOfferStoreTxt.setTextColor(getResources().getColor(R.color.white));
            }

        }
    }

    @OnClick({R.id.header_left_side_img,R.id.offer_filter_par_lay,R.id.offer_sort_par_lay,R.id.offer_stitching_par_lay,R.id.offer_store_par_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.offer_filter_par_lay:
                alertDismiss(mFilterDialog);
                mFilterDialog = getDialog(OffersScreen.this, R.layout.pop_up_offer_filter);

                WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                Window window = mFilterDialog.getWindow();

                if (window != null) {
                    LayoutParams.copyFrom(window.getAttributes());
                    LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                    LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
                    window.setAttributes(LayoutParams);
                    window.setGravity(Gravity.BOTTOM);
                    window.getAttributes().windowAnimations = R.style.PopupBottomAnimation;
                }

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    ViewCompat.setLayoutDirection(mFilterDialog.findViewById(R.id.pop_up_offer_filter_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                }else {
                    ViewCompat.setLayoutDirection(mFilterDialog.findViewById(R.id.pop_up_offer_filter_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                }

                TextView mCancelTxt,mResetTxt,mSetStateTxt,mSetStateCancelTxt,mSetStateResetTxt;

                RelativeLayout mApplyFilterLay,mSetRecyclerViewLay,mFilterDoneLay,mSetStateLay;

                ScrollView mFilterParLay;

                RecyclerView mFilterGemderRecyclerViewLay,mFilterOfferRecyclerViewLay,mFilterLocationRecycleViewLay;

                mCancelTxt = mFilterDialog.findViewById(R.id.pop_up_offer_filter_cancel_txt);
                mResetTxt = mFilterDialog.findViewById(R.id.pop_up_offer_filter_reset_txt);
                mSetStateTxt = mFilterDialog.findViewById(R.id.pop_up_filter_set_state_txt);
                mApplyFilterLay = mFilterDialog.findViewById(R.id.pop_up_filter_filter_apply_lay);
                mFilterParLay = mFilterDialog.findViewById(R.id.pop_up_offer_filter_lay);
                mSetRecyclerViewLay = mFilterDialog.findViewById(R.id.offer_filter_recycler_view_lay);
                mFilterGemderRecyclerViewLay = mFilterDialog.findViewById(R.id.pop_up_filter_gender_recycler_view);
                mFilterOfferRecyclerViewLay = mFilterDialog.findViewById(R.id.pop_up_filter_offer_recycler_view);
                mFilterLocationRecycleViewLay = mFilterDialog.findViewById(R.id.offer_filter_recycler_view);
                mFilterDoneLay = mFilterDialog.findViewById(R.id.offer_filter_filter_done_lay);
                mSetStateLay = mFilterDialog.findViewById(R.id.pop_up_filter_set_state_par_lay);
                mSetStateCancelTxt = mFilterDialog.findViewById(R.id.pop_up_offer_set_state_filter_cancel_txt);
                mSetStateResetTxt = mFilterDialog.findViewById(R.id.pop_up_offer_set_state_filter_reset_txt);

                for (int i=0; i<mGenderList.size(); i++){
                    mGenderList.get(i).setChecked(false);

                }

                for (int i=0; i<mSpeiclOfferList.size(); i++){
                    mSpeiclOfferList.get(i).setChecked(false);

                }

                for (int i=0; i<mGetStateEntity.size(); i++){
                    mGetStateEntity.get(i).setChecked(false);
                }

                for (int i=0; i<AppConstants.OFFER_GENDER_LIST.size(); i++){
                    for (int j=0; j<mGenderList.size(); j++){
                        if (AppConstants.OFFER_GENDER_LIST.get(i) == mGenderList.get(j).getId()){
                            mGenderList.get(j).setChecked(true);
                        }
                    }
                }

                for (int i=0; i<AppConstants.OFFER_SELECTED_LIST.size(); i++){
                    for (int j=0; j<mSpeiclOfferList.size(); j++){
                        if (AppConstants.OFFER_SELECTED_LIST.get(i) == mSpeiclOfferList.get(j).getId()){
                            mSpeiclOfferList.get(j).setChecked(true);
                        }
                    }
                }

                for (int i=0; i<AppConstants.OFFER_LOCATION_LIST.size() ; i++){
                    for (int j=0; j<mGetStateEntity.size(); j++){
                        if (AppConstants.OFFER_LOCATION_LIST.get(i) == mGetStateEntity.get(j).getId()){
                            mGetStateEntity.get(j).setChecked(true);
                        }
                    }
                }

                if (AppConstants.OFFER_LOCATION_LIST.size() > 0){
                    mSetStateTxt.setText(getResources().getString(R.string.selected_state)+" - "+String.valueOf(AppConstants.OFFER_LOCATION_LIST.size()));
                }else {
                    mSetStateTxt.setText(getResources().getString(R.string.select_state));

                }

                mOfferGenderAdapter = new OfferGenderAdapter(this,mGenderList);
                mFilterGemderRecyclerViewLay.setLayoutManager(new GridLayoutManager(this, 2));
                mFilterGemderRecyclerViewLay.setAdapter(mOfferGenderAdapter);

                mGetSpecialOfferAdapter = new GetSpecialOfferAdapter(this,mSpeiclOfferList);
                mFilterOfferRecyclerViewLay.setLayoutManager(new GridLayoutManager(this, 2));
                mFilterOfferRecyclerViewLay.setAdapter(mGetSpecialOfferAdapter);

                mOfferLocationAdapter = new OfferLocationAdapter(this, mGetStateEntity);
                mFilterLocationRecycleViewLay.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                mFilterLocationRecycleViewLay.setAdapter(mOfferLocationAdapter);

                mCancelTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getOfferProductsApiCall(AppConstants.OFFER_CATEGORIES_ID);
                        mFilterDialog.dismiss();
                    }
                });

                mResetTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (int i=0;i<mGenderList.size(); i++){
                            mGenderList.get(i).setChecked(false);
                        }
                        for (int i=0; i<mSpeiclOfferList.size(); i++){
                            mSpeiclOfferList.get(i).setChecked(false);
                        }
                        for (int i=0;i<mGetStateEntity.size();i++){
                            mGetStateEntity.get(i).setChecked(false);
                        }
                        AppConstants.OFFER_GENDER_LIST = new ArrayList<>();
                        AppConstants.OFFER_SELECTED_LIST = new ArrayList<>();
                        AppConstants.OFFER_LOCATION_LIST = new ArrayList<>();

                        if (mOfferGenderAdapter != null){
                            mOfferGenderAdapter.notifyDataSetChanged();
                        }
                        if (mGetSpecialOfferAdapter != null){
                            mGetSpecialOfferAdapter.notifyDataSetChanged();
                        }
                        if (mOfferLocationAdapter != null){
                            mOfferLocationAdapter.notifyDataSetChanged();
                        }
                        mSetStateTxt.setText(getResources().getString(R.string.select_state));
                    }
                });

                mApplyFilterLay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AppConstants.OFFER_GENDER_LIST = new ArrayList<>();

                        for (int i=0; i<mGenderList.size(); i++){
                            if (mGenderList.get(i).isChecked()){
                                AppConstants.OFFER_GENDER_LIST.add(mGenderList.get(i).getId());
                            }
                        }
                        AppConstants.OFFER_SELECTED_LIST = new ArrayList<>();

                        for (int i=0; i<mSpeiclOfferList.size(); i++){
                            if (mSpeiclOfferList.get(i).isChecked()){
                                AppConstants.OFFER_SELECTED_LIST.add(mSpeiclOfferList.get(i).getId());
                            }
                        }
                        AppConstants.OFFER_LOCATION_LIST = new ArrayList<>();

                        for (int i=0 ;i<mGetStateEntity.size(); i++){
                            if (mGetStateEntity.get(i).isChecked()){
                                AppConstants.OFFER_LOCATION_LIST.add(mGetStateEntity.get(i).getId());
                            }
                        }

                        getOfferProductsApiCall(AppConstants.OFFER_CATEGORIES_ID);
                        mFilterDialog.dismiss();
                    }
                });

                mSetStateLay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSetRecyclerViewLay.setVisibility(View.VISIBLE);
                        mFilterParLay.setVisibility(View.GONE);
                    }
                });

                mFilterDoneLay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSetRecyclerViewLay.setVisibility(View.GONE);
                        mFilterParLay.setVisibility(View.VISIBLE);

                        int count = 0;
                        for (int i=0;i<mGetStateEntity.size();i++){
                            if (mGetStateEntity.get(i).isChecked()){
                                count = count + 1;
                            }
                        }
                        if (count > 0){
                            mSetStateTxt.setText(getResources().getString(R.string.selected_state)+"("+String.valueOf(count)+")");
                        }else {
                            mSetStateTxt.setText(getResources().getString(R.string.select_state));

                        }
                    }
                });

                mSetStateCancelTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSetRecyclerViewLay.setVisibility(View.GONE);
                        mFilterParLay.setVisibility(View.VISIBLE);

                        for (int i=0 ;i<mGetStateEntity.size(); i++){
                            mGetStateEntity.get(i).setChecked(false);
                        }

                        for (int i=0; i<AppConstants.OFFER_LOCATION_LIST.size() ; i++){
                            for (int j=0; j<mGetStateEntity.size(); j++){
                                if (AppConstants.OFFER_LOCATION_LIST.get(i) == mGetStateEntity.get(j).getId()){
                                    mGetStateEntity.get(j).setChecked(true);
                                }
                            }
                        }

                        if (AppConstants.OFFER_LOCATION_LIST.size() > 0){
                            mSetStateTxt.setText(getResources().getString(R.string.selected_state)+" - "+String.valueOf(AppConstants.OFFER_LOCATION_LIST.size()));
                        }else {
                            mSetStateTxt.setText(getResources().getString(R.string.select_state));

                        }

                        if (mOfferLocationAdapter != null){
                            mOfferLocationAdapter.notifyDataSetChanged();
                        }
                    }
                });
                mSetStateResetTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (int i=0 ;i<mGetStateEntity.size(); i++){
                            mGetStateEntity.get(i).setChecked(false);
                        }

                        AppConstants.OFFER_LOCATION_LIST = new ArrayList<>();

                        if (mOfferLocationAdapter != null){
                            mOfferLocationAdapter.notifyDataSetChanged();
                        }
                        mSetStateTxt.setText(getResources().getString(R.string.select_state));
                    }
                });

                alertShowing(mFilterDialog);
                break;
            case R.id.offer_sort_par_lay:
                DialogManager.getInstance().showOfferSortPopup(mUserDetailsEntityRes.getLanguage(),AppConstants.OFFER_CATEGORIES_ID,OffersScreen.this);
                break;
            case R.id.offer_stitching_par_lay:

                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOfferStitchingParLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                    mOfferStitchingTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                    mOfferStoreParLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                    mOfferStoreTxt.setTextColor(getResources().getColor(R.color.white));
                }else {
                    mOfferStitchingParLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
                    mOfferStitchingTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                    mOfferStoreParLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
                    mOfferStoreTxt.setTextColor(getResources().getColor(R.color.white));
                }

//                mOfferStitchingParLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
//                mOfferStitchingTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
//                mOfferStoreParLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
//                mOfferStoreTxt.setTextColor(getResources().getColor(R.color.white));
                mHeaderTxt.setText(getResources().getString(R.string.stitching_offer));

                if (mAllCategoriesList.size() > 0){
                    AppConstants.OFFER_CATEGORIES_ID = String.valueOf(mAllCategoriesList.get(0).getId());
                    getOfferProductsApiCall(AppConstants.OFFER_CATEGORIES_ID);

                }
                break;
            case R.id.offer_store_par_lay:
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOfferStitchingParLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
                    mOfferStitchingTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                    mOfferStoreParLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
                    mOfferStoreTxt.setTextColor(getResources().getColor(R.color.white));
                }else {
                    mOfferStitchingParLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                    mOfferStitchingTxt.setTextColor(getResources().getColor(R.color.white));
                    mOfferStoreParLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                    mOfferStoreTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                }
//                mOfferStitchingParLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
//                mOfferStitchingTxt.setTextColor(getResources().getColor(R.color.white));
//                mOfferStoreParLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
//                mOfferStoreTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                mHeaderTxt.setText(getResources().getString(R.string.store_offer));

                if (mAllCategoriesList.size() > 1){
                    AppConstants.OFFER_CATEGORIES_ID = String.valueOf(mAllCategoriesList.get(1).getId());
                    getOfferProductsApiCall(AppConstants.OFFER_CATEGORIES_ID);

                }
                break;
        }
    }

    public void getLanguage(){
        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.offers_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.offers_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetAllCategoriesResponse){
            GetAllCategoriesResponse mResponse = (GetAllCategoriesResponse)resObj;
            mAllCategoriesList = new ArrayList<>();
            mAllCategoriesList = mResponse.getResult().getGetAllCategories();

            for (int i=0; i<mAllCategoriesList.size(); i++){
                if (mAllCategoriesList.get(i).getCategoriesNameInEnglis().equalsIgnoreCase("SPECIAL OFFERS") || mAllCategoriesList.get(i).getCategoriesNameInEnglis().equalsIgnoreCase("BRAND")){
                    mAllCategoriesList.remove(i);
                }
            }
            if (mAllCategoriesList.size() > 0){
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mOfferStitchingTxt.setText(mAllCategoriesList.get(0).getCategoriesNameInArabic());

                }else {
                    mOfferStitchingTxt.setText(mAllCategoriesList.get(0).getCategoriesNameInEnglis());

                }
            }
            if (mAllCategoriesList.size() > 1){
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOfferStoreTxt.setText(mAllCategoriesList.get(1).getCategoriesNameInArabic());

                }else {
                    mOfferStoreTxt.setText(mAllCategoriesList.get(1).getCategoriesNameInEnglis());

                }

            }
        }
        if (resObj instanceof GetOfferCategoriesResponse){
            GetOfferCategoriesResponse mRespone = (GetOfferCategoriesResponse)resObj;
            setOfferCategoriesAdapter(mRespone.getResult());
        }
        if (resObj instanceof GenderResponse){
            GenderResponse mResponse = (GenderResponse) resObj;
            mGenderList = mResponse.getResult();
        }
        if (resObj instanceof GetSpecialOfferModal){
            GetSpecialOfferModal mResponse = (GetSpecialOfferModal)resObj;
            mSpeiclOfferList =mResponse.getResult();
        }
        if (resObj instanceof GetStateResponse){
            GetStateResponse mResponse = (GetStateResponse)resObj;
            mGetStateEntity = mResponse.getResult();
        }

        if (resObj instanceof GetCouponPriceResponse){
            GetCouponPriceResponse mResponse = (GetCouponPriceResponse)resObj;
            if (mResponse.getResult().size()>0){
                if (!mResponse.getResult().get(0).getResult().equalsIgnoreCase("")){
                    AppConstants.APPLY_CODE = "";
                    AppConstants.APPLY_CODE_ID = "";
                    Toast.makeText(this,mResponse.getResult().get(0).getResult(),Toast.LENGTH_SHORT).show();
                }else {
                    mDiscountValue = mResponse.getResult().get(0).getDiscountValue();
                    mDiscountType = mResponse.getResult().get(0).getDiscountType();
                    minimumamount = mResponse.getResult().get(0).getMinimumamount();
                    mMaximumDiscount = mResponse.getResult().get(0).getMaximumDiscount();
                    mCouponType = mResponse.getResult().get(0).getCoupanType();
                    mCouponAppliesTo = mResponse.getResult().get(0).getCouponAppliesTo();
                    AppConstants.APPLY_CODE_ID = String.valueOf(mResponse.getResult().get(0).getId());
                    CheckOutRedeemCouponConverting();
                }

            }else {
                mDiscountValue = 0;
                mDiscountType = 0;
                minimumamount = 0;
                mMaximumDiscount = 0;
                mCouponType = "0";
                mCouponAppliesTo = 0;
                CheckOutRedeemCouponConverting();
                AppConstants.APPLY_CODE = "";
                AppConstants.APPLY_CODE_ID = "";
            }
        }
    }

    public void CheckOutRedeemCouponConverting(){
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat formatter = (DecimalFormat)nf;
            formatter.applyPattern("######.00");
            if (mDiscountValue == 0){
                AppConstants.APPLY_CODE = "";
                AppConstants.APPLY_CODE_ID = "";
                Toast.makeText(this, getResources().getString(R.string.invalid_promo_code), Toast.LENGTH_SHORT).show();
            }else {
                if (mCouponAppliesTo == 1){
                    if (Double.valueOf(AppConstants.STICTHING_AMT) > minimumamount){
                        if (mDiscountType == 1){
                            AppConstants.APPLY_CODE_AMT = String.valueOf(mDiscountValue);
                        }else if (mDiscountType == 2){
                            double res = (Double.valueOf(AppConstants.STICTHING_AMT) / 100.0f) * mDiscountValue;
                            if (res > mMaximumDiscount){
                                AppConstants.APPLY_CODE_AMT = String.valueOf(mMaximumDiscount);
                            }else {
                                AppConstants.APPLY_CODE_AMT = String.valueOf(res);
                            }
                        }
                        onBackPressed();
                    }else {
                        AppConstants.APPLY_CODE = "";
                        AppConstants.APPLY_CODE_ID = "";
                        AppConstants.APPLY_CODE_AMT = "0";
                        Toast.makeText(this,getResources().getString(R.string.invalid_promo_code),Toast.LENGTH_SHORT).show();
                    }
                }else if (mCouponAppliesTo == 3){
                    double Amount = 0.0;
                    for (int i=0; i<AppConstants.GET_ITEM_CARD_LIST.size(); i++){
                        if (String.valueOf(mCouponType).equalsIgnoreCase(AppConstants.GET_ITEM_CARD_LIST.get(i).getProductId())){
                            Amount = Amount +AppConstants.GET_ITEM_CARD_LIST.get(i).getUnitTotal();
                        }
                    }
                    if (Amount == 0.0){
                        AppConstants.APPLY_CODE = "";
                        AppConstants.APPLY_CODE_ID = "";
                        AppConstants.APPLY_CODE_AMT = "0";
                        Toast.makeText(this,getResources().getString(R.string.invalid_promo_code),Toast.LENGTH_SHORT).show();
                    }else {
                        if (Amount > minimumamount){
                            if (mDiscountType == 1){
                                AppConstants.APPLY_CODE_AMT = String.valueOf(mDiscountValue);
                            }else if (mDiscountType == 2){
                                double res = (Amount / 100.0f) * mDiscountValue;
                                if (res > mMaximumDiscount){
                                    AppConstants.APPLY_CODE_AMT = String.valueOf(mMaximumDiscount);
                                }else {
                                    AppConstants.APPLY_CODE_AMT = String.valueOf(res);
                                }
                            }
                            onBackPressed();
                        }else {
                            AppConstants.APPLY_CODE = "";
                            AppConstants.APPLY_CODE_ID = "";
                            AppConstants.APPLY_CODE_AMT = "0";
                            Toast.makeText(this,getResources().getString(R.string.invalid_promo_code),Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
    }


    /*Set Adapter for the Recycler view*/
    public void setOfferCategoriesAdapter(ArrayList<GetOfferCateogriesEntity> getOfferCateogriesEntities) {

        mEmptyListTxt.setVisibility(getOfferCateogriesEntities.size()> 0 ? View.GONE : View.VISIBLE);
        mOfferListRecyclerView.setVisibility(getOfferCateogriesEntities.size()>0 ? View.VISIBLE : View.GONE);

        if (AppConstants.OFFER_CATEGORIES_ID.equalsIgnoreCase("3")) {
            mEmptyListTxt.setText(getResources().getString(R.string.no_store_offers_available));

        }else {
            mEmptyListTxt.setText(getResources().getString(R.string.no_offers_available));

        }

        mOfferListAdapter = new OfferListAdapter(getOfferCateogriesEntities,this);
        mOfferListRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mOfferListRecyclerView.setAdapter(mOfferListAdapter);

    }

    public void getCouponPriceApiCall(String CouponId){
        if (NetworkUtil.isNetworkAvailable(OffersScreen.this)){
            APIRequestHandler.getInstance().getCouponPriceApiCall(CouponId.trim(),AppConstants.APPROVED_TAILOR_ID,OffersScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OffersScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCouponPriceApiCall(CouponId);
                }
            });
        }
    }


    @Override
    public void onBackPressed() {
        AppConstants.OFFER_CATEGORIES = "";
        AppConstants.OFFER_GENDER_LIST = new ArrayList<>();
        AppConstants.OFFER_SELECTED_LIST = new ArrayList<>();
        AppConstants.OFFER_LOCATION_LIST = new ArrayList<>();
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }
}
