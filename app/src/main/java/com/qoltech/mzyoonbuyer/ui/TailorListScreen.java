package com.qoltech.mzyoonbuyer.ui;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.FilterStateAdapter;
import com.qoltech.mzyoonbuyer.adapter.ShopPhotoAdapter;
import com.qoltech.mzyoonbuyer.adapter.ShopTimingAdapter;
import com.qoltech.mzyoonbuyer.adapter.TailorListAdapter;
import com.qoltech.mzyoonbuyer.adapter.ViewDetailPagerAdapter;
import com.qoltech.mzyoonbuyer.entity.ShopTimingEntity;
import com.qoltech.mzyoonbuyer.entity.TailorListEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.floatingmarker.FloatingMarkerTitlesOverlay;
import com.qoltech.mzyoonbuyer.floatingmarker.MarkerInfo;
import com.qoltech.mzyoonbuyer.floatingmarker.SampleMarkerData;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.ShopDetailsReponse;
import com.qoltech.mzyoonbuyer.modal.TailorListCustomizationModal;
import com.qoltech.mzyoonbuyer.modal.TailorListModal;
import com.qoltech.mzyoonbuyer.modal.TailorListResponse;
import com.qoltech.mzyoonbuyer.service.APICommonInterface;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AddressUtils;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;
import com.qoltech.mzyoonbuyer.utils.ZoomOutPageTransformer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class TailorListScreen extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback, LocationListener,
        GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener {

    @BindView(R.id.tailor_scree_par_lay)
    RelativeLayout mTailorScreenParLay;

    private ArrayList<TailorListEntity> mTailorList;
    private ArrayList<TailorListEntity> mTailorStateList;
    private ArrayList<TailorListEntity> mTailorMarkerClickList;
    private ArrayList<TailorListEntity> mTailorFilterList;
    private ArrayList<TailorListEntity> mTailorOriginalList;

    final int MARKER_UPDATE_INTERVAL = 2000;
    Handler handler = new Handler();
    Runnable updateMarker;

    Handler mhandler = new Handler();
    Runnable mupdateMarker;

    @BindView(R.id.tailor_recycler_view)
    RecyclerView mTailorRecyclerView;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.tailor_selected_txt)
    public TextView mTailorSelectedTxt;

    @BindView(R.id.tailor_list_view_lay)
    RelativeLayout mTailorListViewLay;

    @BindView(R.id.tailor_map_view_lay)
    android.support.design.widget.CoordinatorLayout mTailorMapViewLay;

    @BindView(R.id.tailor_list_lay)
    RelativeLayout mTailorListlay;

    @BindView(R.id.tailor_map_lay)
    RelativeLayout mTailorMapLay;

    @BindView(R.id.tailor_list_txt)
    TextView mTailorListTxt;

    @BindView(R.id.tailor_map_txt)
    TextView mTailorMapTxt;

    @BindView(R.id.tailor_list_total_list_txt)
    TextView mTailorListTotalListTxt;

    @BindView(R.id.tailor_detail_expand_web_txt)
    TextView mTailorDetailExpandWebTxt;

    @BindView(R.id.tailor_send_req_btn)
    Button mTailorSendReqBtn;

    @BindView(R.id.tailor_filter_par_lay)
    RelativeLayout mTailorFilterParLay;

    @BindView(R.id.bottom_sheet_lay)
    LinearLayout mBottomSheetLay;

    private TailorListAdapter mTailorAdapter;

    ViewDetailPagerAdapter mViewDetailAdapter;

    private GoogleApiClient mGoogleApiClient;

    private GoogleMap mGoogleMap;

    private final int REQUEST_CHECK_SETTINGS = 300;

    double mLatitude;
    double mLongitude;

    private BottomSheetBehavior mBottomSheetBehaviour;

    private Marker previousMarker = null;

    @BindView(R.id.tailor_detail_shop_name_txt)
    TextView mTailorDetailShopNameTxt;

    @BindView(R.id.tailor_detail_shop_rating_bar)
    RatingBar mTailorDetailsShopRatingBar;

    @BindView(R.id.tailor_detail_shop_rating_txt)
    TextView mTailorDetailsShopRatingTxt;

    @BindView(R.id.tailor_detail_no_txt)
    TextView mTailorDetailsNoTxt;

    @BindView(R.id.tailor_detail_expand_shop_name_txt)
    TextView mTailorDetailExpandShopNameTxt;

    @BindView(R.id.tailor_detail_expand_shop_rating_bar)
    RatingBar mTailorDetailsExpandShopRatingBar;

    @BindView(R.id.tailor_detail_expand_shop_rating_txt)
    TextView mTailorDetailsExpandShopRatingTxt;

    @BindView(R.id.tailor_detail_expand_no_txt)
    TextView mTailorDetailsExpandNoTxt;

    @BindView(R.id.bottom_expand_par_lay)
    ScrollView mBottomExpandParLay;

    @BindView(R.id.bottom_collapse_par_lay)
    LinearLayout mBottomCollapsParLay;

    @BindView(R.id.bottom_expand_dir_lay)
    LinearLayout mBottomExpandDirLay;

    @BindView(R.id.bottom_expand_call_txt)
    TextView mBottomExpandCallTxt;

    @BindView(R.id.tailor_detail_expand_address_header_txt)
    TextView mTailorDetailsExpandAddressHeaderTxt;

    @BindView(R.id.tailor_detail_expand_address_txt)
    TextView mTailorDetailsExpandAddressTxt;

    @BindView(R.id.tailor_send_req_lay)
    RelativeLayout mTailorSendReqLay;

    @BindView(R.id.tailor_list_wiz_lay)
    RelativeLayout mTailorListWizLay;

    @BindView(R.id.new_flow_tailor_wiz_lay)
    RelativeLayout mNewFlowTailorListWizLay;

    @BindView(R.id.tailor_selection_count_par_lay)
    RelativeLayout mTailorSelectionCountParLay;

    @BindView(R.id.tailor_bottom_selecting_txt)
    TextView mTailorBottomSelectingTxt;

    @BindView(R.id.bottom_expand_view_pager)
    ViewPager mViewDetailsViewpager;

    @BindView(R.id.bottom_pageIndicatorView)
    com.rd.PageIndicatorView mPageIndicator;

    @BindView(R.id.tailor_detail_km_txt)
    TextView mTailorDetailsKmTxt;

    @BindView(R.id.tailor_detai_shop_average_rating_txt)
    TextView mTailorDetailsShopAverageRatingTxt;

    @BindView(R.id.tailor_detail_expand_shop_average_rating_txt)
    TextView mTailorDetailsExpandShopAverageRatingTxt;

    @BindView(R.id.tailor_list_scroll_view_img)
    ImageView mTailorListScrollViewImg;

    @BindView(R.id.tailor_list_loader_img)
    RelativeLayout mTailorListLoaderImg;

    Dialog mHintDialog,mFilterDialog;

    private String mWebPageSiteStr = "" ,mMobileStr = "",mCheckClickMarker = "false";

    private UserDetailsEntity mUserDetailsEntityRes;

    private ShopPhotoAdapter mShopImageAdapter;

    int mCountInt = 1,mShowingPopUp = 1,NoOfOrder = 0;

    double Km = 0;

    boolean mCheckApiCallBool = false;

    FloatingMarkerTitlesOverlay floatingMarkersOverlay;

    private FilterStateAdapter mFilterStateAdapter;

    Dialog mTimeningDialog;

    int currentPage = 0;

    private Handler mHandler;
    private Runnable mRunnable;

    ShopTimingAdapter mShopTimingAdapter;

    APICommonInterface mApiCommonInterface;

    @BindView(R.id.empty_list_lay)
            RelativeLayout mEmptyListLay;

    HashMap<String, String > mDistanceKmHashMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_tailor_list_screen);

        initView();
    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mTailorScreenParLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(TailorListScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        mApiCommonInterface = APIRequestHandler.getClient().create(APICommonInterface.class);

        mTailorList = new ArrayList<>();
        mTailorStateList = new ArrayList<>();
        mTailorMarkerClickList = new ArrayList<>();
        mTailorOriginalList = new ArrayList<>();
        mDistanceKmHashMap = new HashMap<>();
        AppConstants.ADD_MATERIAL = "";

        initGoogleAPIClient();

        SupportMapFragment fragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.tailor_map);
        fragment.getMapAsync(this);
        setCurrentLocation();

        mBottomSheetBehaviour = BottomSheetBehavior.from(mBottomSheetLay);

        mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);

        getLanguage();

        if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            mTailorListlay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
            mTailorListTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mTailorMapLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
            mTailorMapTxt.setTextColor(getResources().getColor(R.color.white));
        }else {
            mTailorListlay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
            mTailorListTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mTailorMapLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
            mTailorMapTxt.setTextColor(getResources().getColor(R.color.white));
        }

        if(AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")){
            mTailorSelectionCountParLay.setVisibility(View.GONE);

        }
        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("QUOTE_ORDER")) {
            mTailorBottomSelectingTxt.setVisibility(View.VISIBLE);

        }else {
            mTailorBottomSelectingTxt.setVisibility(View.GONE);
        }

        mBottomSheetBehaviour.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        mBottomExpandParLay.setVisibility(View.VISIBLE);
                        mBottomCollapsParLay.setVisibility(View.GONE);
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        mBottomExpandParLay.setVisibility(View.GONE);
                        mBottomCollapsParLay.setVisibility(View.VISIBLE);
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });


//        mRunnable = new Runnable() {
//            @Override
//            public void run() {
//
//                removeHandler();
//
//                DialogManager.getInstance().hideProgress();
//
//                if (mShowingPopUp == 1){
//                    mShowingPopUp = ++mShowingPopUp;
//                    if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")){
//                        getHintDialog();
//
//                    }
//                }
//            }
//        };
//
//        mHandler = new Handler();
//        mHandler.postDelayed(mRunnable, 3000);

    }

//    private void removeHandler() {
//        if (mHandler != null) {
//            mHandler.removeCallbacks(mRunnable);
//        }
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null){
            handler.removeCallbacks(updateMarker);
        }
        if (mhandler != null){
            mhandler.removeCallbacks(updateMarker);
        }
//        removeHandler();
    }
    /*Init Google API clients*/
    private void initGoogleAPIClient() {

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationManager mLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (mLocManager != null && mLocManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
                initGoogleAPIClient();
            } else {
//                TODO API CALL
                setCurrentLocation();
            }
        } else {
            LocationSettingsRequest.Builder locSettingsReqBuilder = new LocationSettingsRequest.Builder().
                    addLocationRequest(AddressUtils.createLocationRequest());
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, locSettingsReqBuilder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                    final Status status = locationSettingsResult.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // API call.

                            if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
                                initGoogleAPIClient();
                            } else {
                                setCurrentLocation();
                            }

                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied, but this can be fixed
                            // by showing the user a dialog.
                            try {
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(TailorListScreen.this, REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }

                }
            });
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
                            initGoogleAPIClient();
                        } else {
                            setCurrentLocation();
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        // user does not want to update setting. Handle it in a way that it will to affect your app functionality
                        DialogManager.getInstance().showToast(this, "User not updated");
                        break;
                }
                break;
        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsAccessLocation();
            return;
        }
        setCurrentLocation();
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
        mGoogleMap.getUiSettings().setCompassEnabled(false);
        mGoogleMap.setOnMarkerClickListener(this);
        mGoogleMap.setOnMapClickListener(this);

        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                previousMarker = marker;

                if (mTailorList.size() > 0){
                    int id = 0;

                    for (int i=0; i<mTailorList.size();i++){
                        if (mTailorList.get(i).getTailorId() == Integer.parseInt(marker.getTitle())){
                            AppConstants.TAILOR_CLICK_ID = String.valueOf(mTailorList.get(i).getTailorId());
                            id = i;
                        }
                    }
                    getShopDetailsApiCall(AppConstants.TAILOR_CLICK_ID);

                     mCheckClickMarker = "true";

                    if (mTailorList.get(id).getChecked()){
                        mTailorBottomSelectingTxt.setText(getResources().getString(R.string.un_select));
                    }else {
                        mTailorBottomSelectingTxt.setText(getResources().getString(R.string.select));
                    }
                        mTailorDetailsKmTxt.setText(mTailorList.get(id).getDistanceText().equalsIgnoreCase("N/A") ? getResources().getString(R.string.not_available) :  mTailorList.get(id).getDistanceText().replace(" km","")+getResources().getString(R.string.km_from_location));

                        mTailorMarkerClickList = new ArrayList<>();
                        mTailorMarkerClickList.add(mTailorList.get(id));

                    mBottomExpandDirLay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                    Uri.parse("http://maps.google.com/maps?saddr=" + String.valueOf(mLatitude) + "," + String.valueOf(mLongitude) + "&daddr=" + mTailorList.get(Integer.parseInt(marker.getId().replace("m", ""))).getLatitude() + "," + mTailorList.get(Integer.parseInt(marker.getId().replace("m", ""))).getLongitude()));
                            startActivity(intent);
                        }
                    });
                    mBottomSheetLay.setVisibility(View.VISIBLE);

                    mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
                return true;
            }
        });
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);

            }
        });
    }


    private void setCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsAccessLocation();
            return;
        }
        if (NetworkUtil.isNetworkAvailable(this)) {
            if (mGoogleMap != null) {

                FusedLocationProviderClient mLastLocation = LocationServices.getFusedLocationProviderClient(this);

                mLastLocation.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override

                    public void onSuccess(final Location location) {
                        if (location != null) {
                            LatLng coordinate = new LatLng(location.getLatitude(), location.getLongitude());

                            mLatitude = location.getLatitude();
                            mLongitude = location.getLongitude();
                            AppConstants.SHOP_LAT = location.getLatitude();
                            AppConstants.SHOP_LONG = location.getLongitude();
                            if (ActivityCompat.checkSelfPermission(TailorListScreen.this,
                                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                    ActivityCompat.checkSelfPermission(TailorListScreen.this,
                                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                                permissionsAccessLocation();
                            }
                            mGoogleMap.setMyLocationEnabled(true);
                            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
                            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 12));

                            if (Km == 0&&!mCheckApiCallBool){
                                mCheckApiCallBool = true;
                                if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")||AppConstants.DIRECT_ORDER.equalsIgnoreCase("NEW_TAILOR_FLOW")){
                                    getNewFlowTailorListApiCall();
                                    mTailorSelectionCountParLay.setVisibility(View.GONE);
                                }else {
                                    getTailorList();
                                }
                            }
                        }
                    }
                });

            }
        } else {
            /*Alert message will be appeared if there is no internet connection*/
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    setCurrentLocation();
                }
            });
        }

    }
    /*Ask permission on Location access*/
    private boolean permissionsAccessLocation() {
        boolean addPermission = false;

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= 23) {
            int permissionLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            int permissionCoarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

            if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (permissionCoarseLocation != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }
        }

        if (!listPermissionsNeeded.isEmpty()) {
            addPermission = askAccessPermission(listPermissionsNeeded, 1, new InterfaceTwoBtnCallBack() {
                @Override
                public void onNegativeClick() {
                    if (ActivityCompat.checkSelfPermission(TailorListScreen.this,
                            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            ActivityCompat.checkSelfPermission(TailorListScreen.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                }

                @Override
                public void onPositiveClick() {
                    setCurrentLocation();

                }
            });
        }
        return addPermission;
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    /* Ask for permission on Camera access*/
    private boolean checkPermission() {

        boolean addPermission = true;
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            int cameraPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
            int readStoragePermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
            int storagePermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (storagePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
        }

        return addPermission;

    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.tailors_list));
        mRightSideImg.setVisibility(View.INVISIBLE);

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER") || AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            mTailorSendReqLay.setVisibility(View.GONE);
        }

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            mTailorListWizLay.setVisibility(View.GONE);
            mNewFlowTailorListWizLay.setVisibility(View.VISIBLE);
        }else {
            mTailorListWizLay.setVisibility(View.VISIBLE);
            mNewFlowTailorListWizLay.setVisibility(View.GONE);
        }
    }

    public void getHintDialog(){
        alertDismiss(mHintDialog);
        mHintDialog = getDialog(TailorListScreen.this, R.layout.pop_up_tailor_list_hint);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mHintDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.tailor_list_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.tailor_list_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        Button mSkipBtn,mBackBtn,mNextBtn;
        RelativeLayout mTailorListLay,mTailorMapLay,mTailorSelectedLay,mTailorAvailableLay,mTailorFullListLay,mTailorShopNameLay,mTailorSendReqBtnLay;
        LinearLayout mTailorMapListLay,mShopListPriceLay;
        TextView mMapListHintTxt,mSeletedHintTxt,mShopNameTxt,mShopListNameTxt,mShopListOrderNoTxt,mShopListDirectionTxt,mShopListPriceTxt,mTailorListHint;
        ImageView mShopImageView;
        RatingBar mShopListRatingBar;

        /*Init view*/
        mSkipBtn = mHintDialog.findViewById(R.id.skip_hint_btn);
        mBackBtn = mHintDialog.findViewById(R.id.back_hint_btn);
        mNextBtn = mHintDialog.findViewById(R.id.next_hint_btn);

        mTailorListLay = mHintDialog.findViewById(R.id.tailor_list_par_lay);
        mTailorMapLay = mHintDialog.findViewById(R.id.tailor_map_par_lay);
        mTailorMapListLay = mHintDialog.findViewById(R.id.tailor_list_map_par_lay);
        mTailorSelectedLay = mHintDialog.findViewById(R.id.selected_total_lay);
        mTailorAvailableLay = mHintDialog.findViewById(R.id.available_total_lay);
        mSeletedHintTxt = mHintDialog.findViewById(R.id.tailor_seleted_txt);
        mTailorFullListLay = mHintDialog.findViewById(R.id.tailor_full_list_lay);
        mTailorShopNameLay = mHintDialog.findViewById(R.id.tailor_shop_name_list_lay);
        mTailorSendReqBtnLay = mHintDialog.findViewById(R.id.tailor_send_req_lay);
        mMapListHintTxt = mHintDialog.findViewById(R.id.tailor_list_map_hint_txt);
        mShopListNameTxt = mHintDialog.findViewById(R.id.tailor_list_shop_name_txt);
        mShopImageView = mHintDialog.findViewById(R.id.tailor_list_user_img);
        mShopListRatingBar = mHintDialog.findViewById(R.id.tailor_rating_bar);
        mShopListOrderNoTxt = mHintDialog.findViewById(R.id.tailor_list_order_count_txt);
        mShopListDirectionTxt = mHintDialog.findViewById(R.id.tailor_list_direction_txt);
        mShopListPriceTxt = mHintDialog.findViewById(R.id.tailor_list_price_txt);
        mShopListPriceLay = mHintDialog.findViewById(R.id.tailor_adapter_price_lay);
        mShopNameTxt = mHintDialog.findViewById(R.id.tailor_shop_name_txt);
        mTailorListHint = mHintDialog.findViewById(R.id.tailor_lis_hint_txt);

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")){
            mShopListPriceLay.setVisibility(View.VISIBLE);
        }else {
            mShopListPriceLay.setVisibility(View.GONE);

        }

        if (mTailorList.size() > 0){

            try {
                Glide.with(TailorListScreen.this)
                        .load(AppConstants.IMAGE_BASE_URL+"Images/TailorImages/"+mTailorList.get(0).getShopOwnerImageURL())
                        .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                        .into(mShopImageView);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }

            mShopListRatingBar.setRating(mTailorList.get(0).getAvgRating());
            mShopListOrderNoTxt.setText(String.valueOf(mTailorList.get(0).getNoofOrder()));
            mShopListDirectionTxt.setText(mTailorList.get(0).getDistanceText().equalsIgnoreCase("N/A") ? getResources().getString(R.string.not_available) : mTailorList.get(0).getDistanceText().replace(" km","")+" "+ getResources().getString(R.string.km_from_location));
            mShopListPriceTxt.setText(mTailorList.get(0).getPrice());

            if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                mShopListNameTxt.setText(mTailorList.get(0).getShopNameInArabic());
                mShopNameTxt.setText(mTailorList.get(0).getShopNameInArabic());
            }else {
                mShopListNameTxt.setText(mTailorList.get(0).getShopNameInEnglish());
                mShopNameTxt.setText(mTailorList.get(0).getShopNameInEnglish());
            }
        }


        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            mTailorListLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
            mTailorMapLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);

        }else {
            mTailorListLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
            mTailorMapLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);

        }

        if (mCountInt == 1){
            mBackBtn.setVisibility(View.INVISIBLE);
            mTailorMapLay.setVisibility(View.INVISIBLE);
            mMapListHintTxt.setText(getResources().getString(R.string.tailor_list_view_hint));
            mTailorShopNameLay.setVisibility(View.INVISIBLE);
            mTailorFullListLay.setVisibility(View.INVISIBLE);
            mTailorAvailableLay.setVisibility(View.INVISIBLE);
            mTailorSelectedLay.setVisibility(View.INVISIBLE);
            mTailorSendReqBtnLay.setVisibility(View.INVISIBLE);
            mSeletedHintTxt.setVisibility(View.INVISIBLE);
            mTailorListHint.setVisibility(View.INVISIBLE);
        }

        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountInt = --mCountInt;
                if (mCountInt == 1){
                    mBackBtn.setVisibility(View.INVISIBLE);
                    mTailorMapListLay.setVisibility(View.VISIBLE);
                    mTailorListLay.setVisibility(View.VISIBLE);
                    mTailorMapLay.setVisibility(View.INVISIBLE);
                    mTailorShopNameLay.setVisibility(View.INVISIBLE);
                    mTailorFullListLay.setVisibility(View.INVISIBLE);
                    mMapListHintTxt.setText(getResources().getString(R.string.tailor_list_view_hint));
                    mTailorAvailableLay.setVisibility(View.INVISIBLE);
                    mTailorSelectedLay.setVisibility(View.INVISIBLE);
                    mTailorSendReqBtnLay.setVisibility(View.INVISIBLE);
                    mSeletedHintTxt.setVisibility(View.INVISIBLE);
                    mTailorListHint.setVisibility(View.INVISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                }else if (mCountInt == 2){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mTailorMapListLay.setVisibility(View.VISIBLE);
                    mTailorListLay.setVisibility(View.INVISIBLE);
                    mTailorMapLay.setVisibility(View.VISIBLE);
                    mTailorShopNameLay.setVisibility(View.INVISIBLE);
                    mTailorFullListLay.setVisibility(View.INVISIBLE);
                    mMapListHintTxt.setText(getResources().getString(R.string.tailor_map_view_hint));
                    mTailorAvailableLay.setVisibility(View.INVISIBLE);
                    mTailorSelectedLay.setVisibility(View.INVISIBLE);
                    mTailorSendReqBtnLay.setVisibility(View.INVISIBLE);
                    mSeletedHintTxt.setVisibility(View.INVISIBLE);
                    mTailorListHint.setVisibility(View.INVISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                }else if (mCountInt == 3){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mTailorMapListLay.setVisibility(View.INVISIBLE);
                    mTailorShopNameLay.setVisibility(View.VISIBLE);
                    mTailorFullListLay.setVisibility(View.INVISIBLE);
                    mTailorAvailableLay.setVisibility(View.INVISIBLE);
                    mTailorSelectedLay.setVisibility(View.INVISIBLE);
                    mTailorSendReqBtnLay.setVisibility(View.INVISIBLE);
                    mSeletedHintTxt.setVisibility(View.INVISIBLE);
                    mTailorListHint.setVisibility(View.VISIBLE);
                    mTailorListHint.setText(getResources().getString(R.string.tailor_shop_name_hint));
                    mNextBtn.setText(getResources().getString(R.string.next));
                }else if (mCountInt == 4){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mTailorMapListLay.setVisibility(View.INVISIBLE);
                    mTailorShopNameLay.setVisibility(View.INVISIBLE);
                    mTailorFullListLay.setVisibility(View.VISIBLE);
                    mTailorAvailableLay.setVisibility(View.INVISIBLE);
                    mTailorSelectedLay.setVisibility(View.VISIBLE);
                    mTailorSendReqBtnLay.setVisibility(View.INVISIBLE);
                    mSeletedHintTxt.setVisibility(View.INVISIBLE);
                    mTailorListHint.setVisibility(View.VISIBLE);
                    mTailorListHint.setText(getResources().getString(R.string.tailor_selected_hint));
                        mNextBtn.setText(getResources().getString(R.string.next));
                }
                else if (mCountInt == 5){
                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("QUOTE_ORDER")){
                        mBackBtn.setVisibility(View.VISIBLE);
                        mTailorMapListLay.setVisibility(View.INVISIBLE);
                        mTailorShopNameLay.setVisibility(View.INVISIBLE);
                        mTailorFullListLay.setVisibility(View.INVISIBLE);
                        mTailorAvailableLay.setVisibility(View.VISIBLE);
                        mTailorSelectedLay.setVisibility(View.INVISIBLE);
                        mTailorSendReqBtnLay.setVisibility(View.INVISIBLE);
                        mSeletedHintTxt.setVisibility(View.VISIBLE);
                        mTailorListHint.setVisibility(View.INVISIBLE);
                        mSeletedHintTxt.setText(getResources().getString(R.string.tailor_available_count_hint));
                        mNextBtn.setText(getResources().getString(R.string.next));
                    }else {
                        mHintDialog.dismiss();
                    }
                }else if (mCountInt == 6){
                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("QUOTE_ORDER")) {
                        mBackBtn.setVisibility(View.VISIBLE);
                        mTailorMapListLay.setVisibility(View.INVISIBLE);
                        mTailorShopNameLay.setVisibility(View.INVISIBLE);
                        mTailorFullListLay.setVisibility(View.INVISIBLE);
                        mTailorAvailableLay.setVisibility(View.INVISIBLE);
                        mTailorSelectedLay.setVisibility(View.VISIBLE);
                        mTailorSendReqBtnLay.setVisibility(View.INVISIBLE);
                        mSeletedHintTxt.setVisibility(View.VISIBLE);
                        mTailorListHint.setVisibility(View.INVISIBLE);
                        mSeletedHintTxt.setText(getResources().getString(R.string.tailor_selection_count_hint));
                        mNextBtn.setText(getResources().getString(R.string.next));
                    }else {
                        mHintDialog.dismiss();
                    }
                }else if (mCountInt == 7){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mTailorMapListLay.setVisibility(View.INVISIBLE);
                    mTailorShopNameLay.setVisibility(View.INVISIBLE);
                    mTailorFullListLay.setVisibility(View.INVISIBLE);
                    mTailorAvailableLay.setVisibility(View.INVISIBLE);
                    mTailorSelectedLay.setVisibility(View.INVISIBLE);
                    mTailorSendReqBtnLay.setVisibility(View.VISIBLE);
                    mSeletedHintTxt.setVisibility(View.INVISIBLE);
                    mTailorListHint.setVisibility(View.VISIBLE);
                    mTailorListHint.setText(getResources().getString(R.string.tailor_confirme_hint));
                    mNextBtn.setText(getResources().getString(R.string.got_it));
                }
            }
        });

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountInt = ++mCountInt;
                if (mCountInt == 1){
                    mBackBtn.setVisibility(View.INVISIBLE);
                    mTailorMapListLay.setVisibility(View.VISIBLE);
                    mTailorListLay.setVisibility(View.VISIBLE);
                    mTailorMapLay.setVisibility(View.INVISIBLE);
                    mMapListHintTxt.setText(getResources().getString(R.string.tailor_list_view_hint));
                    mTailorShopNameLay.setVisibility(View.INVISIBLE);
                    mTailorFullListLay.setVisibility(View.INVISIBLE);
                    mTailorAvailableLay.setVisibility(View.INVISIBLE);
                    mTailorSelectedLay.setVisibility(View.INVISIBLE);
                    mTailorSendReqBtnLay.setVisibility(View.INVISIBLE);
                    mSeletedHintTxt.setVisibility(View.INVISIBLE);
                    mTailorListHint.setVisibility(View.INVISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                }else if (mCountInt == 2){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mTailorMapListLay.setVisibility(View.VISIBLE);
                    mTailorListLay.setVisibility(View.INVISIBLE);
                    mTailorMapLay.setVisibility(View.VISIBLE);
                    mMapListHintTxt.setText(getResources().getString(R.string.tailor_map_view_hint));
                    mTailorShopNameLay.setVisibility(View.INVISIBLE);
                    mTailorFullListLay.setVisibility(View.INVISIBLE);
                    mTailorAvailableLay.setVisibility(View.INVISIBLE);
                    mTailorSelectedLay.setVisibility(View.INVISIBLE);
                    mTailorSendReqBtnLay.setVisibility(View.INVISIBLE);
                    mSeletedHintTxt.setVisibility(View.INVISIBLE);
                    mTailorListHint.setVisibility(View.INVISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                }else if (mCountInt == 3){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mTailorMapListLay.setVisibility(View.INVISIBLE);
                    mTailorShopNameLay.setVisibility(View.VISIBLE);
                    mTailorFullListLay.setVisibility(View.INVISIBLE);
                    mTailorAvailableLay.setVisibility(View.INVISIBLE);
                    mTailorSelectedLay.setVisibility(View.INVISIBLE);
                    mTailorSendReqBtnLay.setVisibility(View.INVISIBLE);
                    mSeletedHintTxt.setVisibility(View.INVISIBLE);
                    mTailorListHint.setVisibility(View.VISIBLE);
                    mTailorListHint.setText(getResources().getString(R.string.tailor_shop_name_hint));
                    mNextBtn.setText(getResources().getString(R.string.next));
                }else if (mCountInt == 4){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mTailorMapListLay.setVisibility(View.INVISIBLE);
                    mTailorShopNameLay.setVisibility(View.INVISIBLE);
                    mTailorFullListLay.setVisibility(View.VISIBLE);
                    mTailorAvailableLay.setVisibility(View.INVISIBLE);
                    mTailorSelectedLay.setVisibility(View.INVISIBLE);
                    mTailorSendReqBtnLay.setVisibility(View.INVISIBLE);
                    mSeletedHintTxt.setVisibility(View.INVISIBLE);
                    mTailorListHint.setVisibility(View.VISIBLE);
                    mTailorListHint.setText(getResources().getString(R.string.tailor_selected_hint));
                    mNextBtn.setText(getResources().getString(R.string.next));
                }else if (mCountInt == 5){
                        mBackBtn.setVisibility(View.VISIBLE);
                        mTailorMapListLay.setVisibility(View.INVISIBLE);
                        mTailorShopNameLay.setVisibility(View.INVISIBLE);
                        mTailorFullListLay.setVisibility(View.INVISIBLE);
                        mTailorAvailableLay.setVisibility(View.VISIBLE);
                        mTailorSelectedLay.setVisibility(View.INVISIBLE);
                        mTailorSendReqBtnLay.setVisibility(View.INVISIBLE);
                        mSeletedHintTxt.setVisibility(View.VISIBLE);
                    mTailorListHint.setVisibility(View.INVISIBLE);
                    mSeletedHintTxt.setText(getResources().getString(R.string.tailor_available_count_hint));
                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("QUOTE_ORDER")){
                        mNextBtn.setText(getResources().getString(R.string.next));
                    }else {
                        mNextBtn.setText(getResources().getString(R.string.got_it));
                    }

                }else if (mCountInt == 6){
                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("QUOTE_ORDER")) {
                        mBackBtn.setVisibility(View.VISIBLE);
                        mTailorMapListLay.setVisibility(View.INVISIBLE);
                        mTailorShopNameLay.setVisibility(View.INVISIBLE);
                        mTailorFullListLay.setVisibility(View.INVISIBLE);
                        mTailorAvailableLay.setVisibility(View.INVISIBLE);
                        mTailorSelectedLay.setVisibility(View.VISIBLE);
                        mTailorSendReqBtnLay.setVisibility(View.INVISIBLE);
                        mSeletedHintTxt.setVisibility(View.VISIBLE);
                        mTailorListHint.setVisibility(View.INVISIBLE);
                        mSeletedHintTxt.setText(getResources().getString(R.string.tailor_selection_count_hint));
                        mNextBtn.setText(getResources().getString(R.string.next));
                    }else {
                        mHintDialog.dismiss();
                    }
                }else if (mCountInt == 7){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mTailorMapListLay.setVisibility(View.INVISIBLE);
                    mTailorShopNameLay.setVisibility(View.INVISIBLE);
                    mTailorFullListLay.setVisibility(View.INVISIBLE);
                    mTailorAvailableLay.setVisibility(View.INVISIBLE);
                    mTailorSelectedLay.setVisibility(View.INVISIBLE);
                    mTailorSendReqBtnLay.setVisibility(View.VISIBLE);
                    mSeletedHintTxt.setVisibility(View.INVISIBLE);
                    mTailorListHint.setVisibility(View.VISIBLE);
                    mTailorListHint.setText(getResources().getString(R.string.tailor_confirme_hint));
                    mNextBtn.setText(getResources().getString(R.string.got_it));
                }else if (mCountInt >= 8){
                    mHintDialog.dismiss();
                }
            }
        });

        /*Set data*/
        mSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHintDialog.dismiss();
            }
        });

        alertShowing(mHintDialog);
    }

    public void getNewFlowTailorListApiCall(){
        if (NetworkUtil.isNetworkAvailable(TailorListScreen.this)){
            String Lat = String.valueOf(mLatitude);
            String Long = String.valueOf(mLongitude);
            APIRequestHandler.getInstance().getNewFlowTailorListApiCall(AppConstants.SUB_DRESS_TYPE_ID,Lat,Long,TailorListScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(TailorListScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getNewFlowTailorListApiCall();
                }
            });
        }
    }

    public void getTailorList() {
        TailorListModal tailorListModalApiCallModal = new TailorListModal();

        ArrayList<TailorListCustomizationModal> orderCustomizationIdValueEntities = new ArrayList<>();

        for (int i=0; i< AppConstants.CUSTOMIZATION_CLICK_ARRAY.size(); i++){
            TailorListCustomizationModal orderCustomizationIdValueEntity = new TailorListCustomizationModal();

            orderCustomizationIdValueEntity.setCustomizationId(AppConstants.CUSTOMIZATION_CLICK_ARRAY.get(i).getId());
            orderCustomizationIdValueEntities.add(orderCustomizationIdValueEntity);

        }

        tailorListModalApiCallModal.setDressSubType(AppConstants.SUB_DRESS_TYPE_ID);
        tailorListModalApiCallModal.setOrderType(AppConstants.ORDER_TYPE_ID);
        tailorListModalApiCallModal.setMeasuremenType(AppConstants.MEASUREMENT_TYPE);
        tailorListModalApiCallModal.setServiceType(AppConstants.DELIVERY_TYPE_ID);
        tailorListModalApiCallModal.setAreaId(AppConstants.COUNTRY_AREA_ID);
        tailorListModalApiCallModal.setCustomization(orderCustomizationIdValueEntities);
        tailorListModalApiCallModal.setMaterialId(AppConstants.PATTERN_ID.equalsIgnoreCase("") ? "0" : AppConstants.PATTERN_ID);
        tailorListModalApiCallModal.setLatitude(mLatitude);
        tailorListModalApiCallModal.setLongitude(mLongitude);

        if (NetworkUtil.isNetworkAvailable(TailorListScreen.this)) {
            APIRequestHandler.getInstance().getTailorListApiCall(TailorListScreen.this,tailorListModalApiCallModal);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(TailorListScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getTailorList();
                }
            });
        }

    }
    public void getShopDetailsApiCall(String TailorId){
        if (NetworkUtil.isNetworkAvailable(TailorListScreen.this)){
            APIRequestHandler.getInstance().getShopDetailsHidePrgressBarApiCall(TailorId,TailorListScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(TailorListScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getShopDetailsApiCall(TailorId);
                }
            });
        }
    }

    public void viewPagerGet(ArrayList<String> image){
        mViewDetailAdapter = new ViewDetailPagerAdapter(this,image);
        mViewDetailsViewpager.setAdapter(mViewDetailAdapter);
        mViewDetailsViewpager.setPageTransformer(true, new ZoomOutPageTransformer());
        mPageIndicator.setViewPager(mViewDetailsViewpager);

        currentPage = 0;
        Timer timer;
        final long DELAY_MS = 500;
        final long PERIOD_MS = 3000;

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage >= image.size()) {
                    currentPage = 0;
                }
                mViewDetailsViewpager.setCurrentItem(currentPage++,true);

            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);

        //page change tracker
        mViewDetailsViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        trackScreenName("TailorList");
    }
    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof TailorListResponse) {
            TailorListResponse mResponse = (TailorListResponse) resObj;

            AppConstants.TAILOR_LIST = new ArrayList<>();
            mTailorStateList = new ArrayList<>();
            mTailorList = new ArrayList<>();
            AppConstants.TAILOR_LIST = mResponse.getResult().getGetTailorLists();
            for (int i=0; i<mResponse.getResult().getGetTailorLists().size(); i++){
                TailorListEntity mEntity = new TailorListEntity();
                mEntity.setStateName(mResponse.getResult().getGetTailorLists().get(i).getStateName());
                mEntity.setStateId(mResponse.getResult().getGetTailorLists().get(i).getStateId());
                mTailorStateList.add(mEntity);
            }

            mTailorList = mResponse.getResult().getGetTailorLists();
            loading(mResponse.getResult().getGetTailorLists());

        }

        if (resObj instanceof ShopDetailsReponse){
            ShopDetailsReponse mResponse = (ShopDetailsReponse)resObj;
            mBottomExpandCallTxt.setText(getResources().getString(R.string.no_mobile_num));
            mTailorDetailExpandWebTxt.setText(getResources().getString(R.string.no_web_page));
            mWebPageSiteStr = "";
            mMobileStr = "";
            if(mResponse.getResult().getGetShopDetails().size()>0){
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mTailorDetailShopNameTxt.setText(mResponse.getResult().getGetShopDetails().get(0).getShopNameInArabic());

                    mTailorDetailExpandShopNameTxt.setText(mResponse.getResult().getGetShopDetails().get(0).getShopNameInArabic());
                    mTailorDetailsExpandAddressHeaderTxt.setText(" : " + getResources().getString(R.string.address));
                    mTailorDetailsExpandAddressTxt.setText(mResponse.getResult().getGetShopDetails().get(0).getStateName()+", \n" + mResponse.getResult().getGetShopDetails().get(0).getCountryName()+", \n" + mResponse.getResult().getGetShopDetails().get(0).getAddressinArabic()+".");
                } else {
                    mTailorDetailShopNameTxt.setText(mResponse.getResult().getGetShopDetails().get(0).getShopNameInEnglish());

                    mTailorDetailExpandShopNameTxt.setText(mResponse.getResult().getGetShopDetails().get(0).getShopNameInEnglish());

                    mTailorDetailsExpandAddressHeaderTxt.setText(getResources().getString(R.string.address) +" : ");
                    mTailorDetailsExpandAddressTxt.setText(mResponse.getResult().getGetShopDetails().get(0).getStateName()+", \n" + mResponse.getResult().getGetShopDetails().get(0).getCountryName()+", \n" + mResponse.getResult().getGetShopDetails().get(0).getAddressInEnglish()+".");
                }

                mBottomExpandCallTxt.setText(mResponse.getResult().getGetShopDetails().get(0).getPhoneNumber().equalsIgnoreCase("") ? getResources().getString(R.string.no_mobile_num) : mResponse.getResult().getGetShopDetails().get(0).getPhoneNumber());
                mTailorDetailExpandWebTxt.setText(mResponse.getResult().getGetShopDetails().get(0).getWebSite().equalsIgnoreCase("") ? getResources().getString(R.string.no_web_page) : mResponse.getResult().getGetShopDetails().get(0).getWebSite());
                mWebPageSiteStr = mResponse.getResult().getGetShopDetails().get(0).getWebSite();
                mMobileStr = mResponse.getResult().getGetShopDetails().get(0).getPhoneNumber();

            }
            if (mResponse.getResult().getRating().size()>0){
                NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                DecimalFormat formatter = (DecimalFormat)nf;
                formatter.applyPattern("######.#");
                mTailorDetailsShopRatingBar.setRating(mResponse.getResult().getRating().get(0).getRating());
                mTailorDetailsExpandShopRatingBar.setRating(mResponse.getResult().getRating().get(0).getRating());
                mTailorDetailsExpandShopAverageRatingTxt.setText(String.valueOf(formatter.format(mResponse.getResult().getRating().get(0).getRating())));
                mTailorDetailsShopAverageRatingTxt.setText(String.valueOf(formatter.format(mResponse.getResult().getRating().get(0).getRating())));
            }
            if (mResponse.getResult().getOrderCount().size()>0){
                mTailorDetailsNoTxt.setText(String.valueOf(mResponse.getResult().getOrderCount().get(0).getOrderCount()));
                mTailorDetailsExpandNoTxt.setText(String.valueOf(mResponse.getResult().getOrderCount().get(0).getOrderCount()));
            }
            if (mResponse.getResult().getReviewCount().size()>0){
                mTailorDetailsShopRatingTxt.setText(String.valueOf("("+mResponse.getResult().getReviewCount().get(0).getReviewCount()+" "+getResources().getString(R.string.review) + ")"));
                mTailorDetailsExpandShopRatingTxt.setText(String.valueOf("("+mResponse.getResult().getReviewCount().get(0).getReviewCount()+" "+getResources().getString(R.string.review) + ")"));
            }
            ArrayList<String> shopImageList = new ArrayList<>();

            for (int i=0; i< mResponse.getResult().getShopImages().size(); i++){
                shopImageList.add(AppConstants.IMAGE_BASE_URL+"Images/ShopImages/"+mResponse.getResult().getShopImages().get(i).getImage());
            }
            viewPagerGet(shopImageList);

        }
    }
    public void loading(ArrayList<TailorListEntity> mTailorList){

        updateMarker = new Runnable() {
            @Override
            public void run() {

        for (int i=0; i<mTailorList.size(); i++) {
            TailorListEntity mEntitys = new TailorListEntity();
            mEntitys.setChecked(mTailorList.get(i).getChecked());
            mEntitys.setStateId(mTailorList.get(i).getStateId());
            mEntitys.setStateName(mTailorList.get(i).getStateName());
            mEntitys.setMarkerImg(AppConstants.IMAGE_BASE_URL+"Images/TailorImages/"+mTailorList.get(i).getShopOwnerImageURL());
            mEntitys.setAddressinArabic(mTailorList.get(i).getAddressinArabic());
            mEntitys.setAddressInEnglish(mTailorList.get(i).getAddressInEnglish());
            mEntitys.setAvgRating(mTailorList.get(i).getAvgRating());
            mEntitys.setCount(mTailorList.get(i).getCount());
            mEntitys.setDistance(mTailorList.get(i).getDistance());
            mEntitys.setDistanceText(mTailorList.get(i).getDistanceText());
            mEntitys.setId(mTailorList.get(i).getId());
            mEntitys.setLatitude(mTailorList.get(i).getLatitude());
            mEntitys.setLongitude(mTailorList.get(i).getLongitude());
            mEntitys.setNoofOrder(mTailorList.get(i).getNoofOrder());
            mEntitys.setOrderCount(mTailorList.get(i).getOrderCount());
            mEntitys.setPrice(mTailorList.get(i).getPrice());
            mEntitys.setRating(mTailorList.get(i).getRating());
            mEntitys.setShopNameInArabic(mTailorList.get(i).getShopNameInArabic());
            mEntitys.setShopNameInEnglish(mTailorList.get(i).getShopNameInEnglish());
            mEntitys.setShopOwnerImageURL(mTailorList.get(i).getShopOwnerImageURL());
            mEntitys.setTailorId(mTailorList.get(i).getTailorId());
            mEntitys.setTailorNameInArabic(mTailorList.get(i).getTailorNameInArabic());
            mEntitys.setTailorNameInEnglish(mTailorList.get(i).getTailorNameInEnglish());
            mTailorOriginalList.add(mEntitys);

        }

        setTailorListAdapter(mTailorList);

        mTailorListTotalListTxt.setText(String.valueOf(mTailorList.size()));

        for (int i=0; i<mTailorList.size(); i++){
            mTailorList.get(i).setMarkerImg(AppConstants.IMAGE_BASE_URL+"Images/TailorImages/"+mTailorList.get(i).getShopOwnerImageURL());
        }

        floatingMarkersOverlay = findViewById(R.id.map_floating_markers_overlay);
        floatingMarkersOverlay.setSource(mGoogleMap);


                if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")){
                    if (AppConstants.TAILOR_LIST.size()>0){
                        final List<MarkerInfo> markerInfoList = SampleMarkerData.getSampleMarkersInfo();
                        for (int i = 0; i < markerInfoList.size(); i++) {
                            final MarkerInfo mi = markerInfoList.get(i);
                            final long id = i;
                            final int Ii = i;
                            mupdateMarker = new Runnable() {
                                @Override
                                public void run() {
//                            mGoogleMap.addMarker(new MarkerOptions().position(mi.getCoordinates()).
//                                    icon(BitmapDescriptorFactory.fromBitmap(
//                                            createCustomSelectedMarker(TailorListScreen.this, mTailorList.get(Ii).getMarkerImg(),mTailorList.get(Ii).getTailorNameInEnglish()))).title(String.valueOf(mTailorList.get(Ii).getTailorId())));
//                            floatingMarkersOverlay.addMarker(id, mi);
                                    mGoogleMap.addMarker(new MarkerOptions().position(mi.getCoordinates()).
                                            icon(BitmapDescriptorFactory.fromResource(R.drawable.shop_marker))).setTitle(String.valueOf(mTailorList.get(Ii).getTailorId()));
                                    floatingMarkersOverlay.addMarker(id, mi);
                                }
                                };
                            mhandler.postDelayed(mupdateMarker, MARKER_UPDATE_INTERVAL);

                        }
                    }
                }else {
                    for (int i = 0; i < mTailorList.size(); i++) {
                        Double lat = Double.parseDouble(mTailorList.get(i).getLatitude());
                        Double lon = Double.parseDouble(mTailorList.get(i).getLongitude());

                        LatLng customMarkerLocationOne = new LatLng(lat, lon);
                        final int Ii = i;
                        mupdateMarker = new Runnable() {
                            @Override
                            public void run() {

                                mGoogleMap.addMarker(new MarkerOptions().position(customMarkerLocationOne).
                                        icon(BitmapDescriptorFactory.fromResource(R.drawable.shop_marker))).setTitle(String.valueOf(mTailorList.get(Ii).getTailorId()));
//                        mGoogleMap.addMarker(new MarkerOptions().position(customMarkerLocationOne).
//                                icon(BitmapDescriptorFactory.fromBitmap(
//                                        createCustomSelectedMarker(TailorListScreen.this, mTailorList.get(Ii).getMarkerImg(),mTailorList.get(Ii).getTailorNameInEnglish()))).title(String.valueOf(mTailorList.get(Ii).getTailorId())));
                            }
                        };
                        mhandler.postDelayed(mupdateMarker, MARKER_UPDATE_INTERVAL);
                    }
                }

                mTailorListLoaderImg.setVisibility(View.GONE);
            }
        };

        handler.postDelayed(updateMarker, MARKER_UPDATE_INTERVAL);

    }

    public static Bitmap createCustomSelectedMarker(Context context, String resource, String _name) {

                View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_selected_marker_layout, null);

                CircleImageView markerImage = (CircleImageView) marker.findViewById(R.id.user_dp);

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                try {
                    URL url = new URL(resource);
                    markerImage.setImageBitmap(BitmapFactory.decodeStream((InputStream)url.getContent()));
                } catch (IOException e) {
                    //Log.e(TAG, e.getMessage());
                }

                TextView txt_name = (TextView)marker.findViewById(R.id.name);
                txt_name.setText(_name);

                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                marker.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
                marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
                marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                marker.buildDrawingCache();
                Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                marker.draw(canvas);

                return bitmap;
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }


    @OnClick({R.id.header_left_side_img, R.id.tailor_send_req_btn, R.id.tailor_list_lay, R.id.tailor_map_lay, R.id.tailor_details_shop_view_img, R.id.bottom_expand_call_lay, R.id.bottom_expand_share_lay, R.id.bottom_expand_par_lay, R.id.bottom_expand_web_site_par_lay,R.id.tailor_detail_expand_shop_rating_bar,R.id.tailor_detai_shop_average_rating_txt,R.id.tailor_detail_shop_rating_txt,R.id.tailor_detail_shop_rating_bar,R.id.tailor_detail_expand_shop_average_rating_lay,R.id.tailor_bottom_proceed_txt,R.id.tailor_bottom_selecting_txt,R.id.tailor_filter_lay,R.id.bottom_sheet_time_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                if (BottomSheetBehavior.STATE_EXPANDED == mBottomSheetBehaviour.getState()) {
                    mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
                else {
                    onBackPressed();
                }
                break;
            case R.id.tailor_send_req_btn:
                if (AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.size() > 0) {
                    nextScreen(OrderSummaryScreen.class, true);
                } else {
                    Toast.makeText(TailorListScreen.this, getResources().getString(R.string.please_select_tailor), Toast.LENGTH_SHORT).show();
                    mTailorSendReqBtn.clearAnimation();
                    mTailorSendReqBtn.setAnimation(ShakeErrorUtils.shakeError());
                }
                break;
            case R.id.tailor_list_lay:
                mTailorListViewLay.setVisibility(View.VISIBLE);
                mTailorMapViewLay.setVisibility(View.GONE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mTailorListlay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                    mTailorListTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                    mTailorMapLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                    mTailorMapTxt.setTextColor(getResources().getColor(R.color.white));
                }else {
                    mTailorListlay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
                    mTailorListTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                    mTailorMapLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
                    mTailorMapTxt.setTextColor(getResources().getColor(R.color.white));
                }
                    mBottomSheetLay.setVisibility(View.GONE);

                break;
            case R.id.tailor_map_lay:
                mTailorListViewLay.setVisibility(View.GONE);
                mTailorMapViewLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mTailorListlay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
                    mTailorListTxt.setTextColor(getResources().getColor(R.color.white));
                    mTailorMapLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
                    mTailorMapTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                }else {
                    mTailorListlay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                    mTailorListTxt.setTextColor(getResources().getColor(R.color.white));
                    mTailorMapLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                    mTailorMapTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                }

                if (mTailorMarkerClickList.size()>0){
                    for (int i=0;i<mTailorList.size(); i++){
                        if (mTailorList.get(i).getTailorId() == mTailorMarkerClickList.get(0).getTailorId()){
                            if (mTailorList.get(i).getChecked()){
                                mTailorBottomSelectingTxt.setText(getResources().getString(R.string.unselect_tailor));
                            }else {
                                mTailorBottomSelectingTxt.setText(getResources().getString(R.string.select_tailor));
                            }
                        }
                    }
                }
                break;
            case R.id.tailor_details_shop_view_img:
                if (BottomSheetBehavior.STATE_EXPANDED == mBottomSheetBehaviour.getState()) {
                    mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);
                } else {
                    mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                break;
            case R.id.bottom_expand_call_lay:
                if (!mMobileStr.equalsIgnoreCase("")){
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + mMobileStr));
                    startActivity(intent);
                }
                break;
            case R.id.bottom_expand_share_lay:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = getResources().getString(R.string.next_milestone_str);
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "MZYOON");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case R.id.bottom_expand_par_lay:
                mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;
            case R.id.bottom_expand_web_site_par_lay:
                if (!mWebPageSiteStr.equalsIgnoreCase("")){
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mWebPageSiteStr));
                    startActivity(browserIntent);
                }
                break;
            case R.id.tailor_detail_expand_shop_rating_bar:
                AppConstants.MATERIAL_REVIEW = "";
                nextScreen(RatingScreen.class,true);
                break;
            case R.id.tailor_detail_expand_shop_average_rating_lay:
                AppConstants.MATERIAL_REVIEW = "";
                nextScreen(RatingScreen.class,true);
                break;
            case R.id.tailor_detail_shop_rating_bar:
                AppConstants.MATERIAL_REVIEW = "";
                nextScreen(RatingScreen.class,true);
                break;
            case R.id.tailor_detai_shop_average_rating_txt:
                AppConstants.MATERIAL_REVIEW = "";
                nextScreen(RatingScreen.class,true);
                break;
            case R.id.tailor_detail_shop_rating_txt:
                AppConstants.MATERIAL_REVIEW = "";
                nextScreen(RatingScreen.class,true);
                break;
            case R.id.tailor_bottom_proceed_txt:
                if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("QUOTE_ORDER")){

                    for (int i=0;i<mTailorList.size(); i++){
                        if (mTailorList.get(i).getTailorId() == mTailorMarkerClickList.get(0).getTailorId()){
                            mTailorList.get(i).setChecked(true);
                        }
                    }
                    for (int i=0;i<mTailorOriginalList.size(); i++){
                        if (mTailorOriginalList.get(i).getTailorId() == mTailorMarkerClickList.get(0).getTailorId()){
                            mTailorOriginalList.get(i).setChecked(true);
                        }
                    }
                    setTailorListAdapter(mTailorList);
                    AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();

                    for (int j=0; j<mTailorList.size(); j++){
                        if (mTailorList.get(j).getChecked()){
                            AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.add(mTailorList.get(j));
                        }
                    }

                    if (AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.size() > 0) {
                        nextScreen(OrderSummaryScreen.class, true);
                    } else {
                        Toast.makeText(TailorListScreen.this, getResources().getString(R.string.please_select_tailor), Toast.LENGTH_SHORT).show();
                        mTailorSendReqBtn.clearAnimation();
                        mTailorSendReqBtn.setAnimation(ShakeErrorUtils.shakeError());
                    }
                }else {
                    for (int i=0;i<mTailorList.size(); i++){
                        if (mTailorList.get(i).getTailorId() == mTailorMarkerClickList.get(0).getTailorId()){
                            mTailorList.get(i).setChecked(true);

                        }else {
                            mTailorList.get(i).setChecked(false);

                        }
                    }
                    for (int i=0;i<mTailorOriginalList.size(); i++){
                        if (mTailorOriginalList.get(i).getTailorId() == mTailorMarkerClickList.get(0).getTailorId()){
                            mTailorOriginalList.get(i).setChecked(true);

                        }else {
                            mTailorOriginalList.get(i).setChecked(false);

                        }
                    }
                    setTailorListAdapter(mTailorList);

                    AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();
                    AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.add(mTailorMarkerClickList.get(0));
                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")){
                        nextScreen(DirectOrderPriceScreen.class,true);
                    }
                    else if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
                        nextScreen(OrderTypeScreen.class,true);
                    }
                }

                break;
            case R.id.tailor_bottom_selecting_txt:

                    for (int i=0;i<mTailorList.size(); i++){
                        if (mTailorList.get(i).getTailorId() == mTailorMarkerClickList.get(0).getTailorId()){
                            if (mTailorList.get(i).getChecked()){
                                mTailorList.get(i).setChecked(false);
                                int count = Integer.parseInt(mTailorSelectedTxt.getText().toString().trim());
                                int sub = --count;
                                mTailorSelectedTxt.setText(String.valueOf(sub));
                                mTailorBottomSelectingTxt.setText(getResources().getString(R.string.select_tailor));
                            }else {
                                mTailorList.get(i).setChecked(true);
                                int count = Integer.parseInt(mTailorSelectedTxt.getText().toString().trim());
                                int add = ++count;
                                mTailorSelectedTxt.setText(String.valueOf(String.valueOf(add)));
                                mTailorBottomSelectingTxt.setText(getResources().getString(R.string.unselect_tailor));

                            }
                        }
                    }
                for (int i=0;i<mTailorOriginalList.size(); i++){
                    if (mTailorOriginalList.get(i).getTailorId() == mTailorMarkerClickList.get(0).getTailorId()){
                        if (mTailorOriginalList.get(i).getChecked()){
                            mTailorOriginalList.get(i).setChecked(false);
                        }else {
                            mTailorOriginalList.get(i).setChecked(true);

                        }
                    }
                }
                setTailorListAdapter(mTailorList);
                AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();

                    for (int j=0; j<mTailorList.size(); j++){
                        if (mTailorList.get(j).getChecked()){
                            AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.add(mTailorList.get(j));
                        }
                    }
                break;
            case R.id.tailor_filter_lay:
                alertDismiss(mFilterDialog);
                mFilterDialog = getDialog(TailorListScreen.this, R.layout.pop_up_tailor_filter);

                WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                Window window = mFilterDialog.getWindow();

                if (window != null) {
                    LayoutParams.copyFrom(window.getAttributes());
                    LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                    LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
                    window.setAttributes(LayoutParams);
                    window.setGravity(Gravity.BOTTOM);
                    window.getAttributes().windowAnimations = R.style.PopupBottomAnimation;
                }

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    ViewCompat.setLayoutDirection(mFilterDialog.findViewById(R.id.filter_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                }else {
                    ViewCompat.setLayoutDirection(mFilterDialog.findViewById(R.id.filter_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                }

                TextView mCancelTxt,mResetTxt,mSetStateTxt,mStateCancelTxt,mStateResetTxt;
                com.zhouyou.view.seekbar.SignSeekBar mKmSeekBar,mNoOfOrderSeekBar;
                RatingBar mFilterRatingBar;

                RelativeLayout mApplyFilterLay,mSetRecyclerViewLay,mFilterDoneLay,mSetStateLay;

                ScrollView mFilterParLay;

                RecyclerView mFilterRecyclerViewLay;

                mCancelTxt = mFilterDialog.findViewById(R.id.tailor_filter_cancel_txt);
                mResetTxt = mFilterDialog.findViewById(R.id.tailor_filter_reset_txt);
                mSetStateTxt = mFilterDialog.findViewById(R.id.filter_set_state_txt);
                mStateCancelTxt = mFilterDialog.findViewById(R.id.tailor_state_filter_cancel_txt);
                mStateResetTxt = mFilterDialog.findViewById(R.id.tailor_state_filter_reset_txt);

                mKmSeekBar = mFilterDialog.findViewById(R.id.filter_km_seek_bar);
                mNoOfOrderSeekBar = mFilterDialog.findViewById(R.id.filter_num_of_order_seek_bar);

                mFilterRatingBar = mFilterDialog.findViewById(R.id.filter_rating_bar);

                mApplyFilterLay = mFilterDialog.findViewById(R.id.filter_filter_apply_lay);

                mFilterParLay = mFilterDialog.findViewById(R.id.filter_apply_lay);

                mSetRecyclerViewLay = mFilterDialog.findViewById(R.id.filter_recycler_view_lay);

                mFilterRecyclerViewLay = mFilterDialog.findViewById(R.id.filter_recycler_view);

                mFilterDoneLay = mFilterDialog.findViewById(R.id.filter_filter_done_lay);

                mSetStateLay = mFilterDialog.findViewById(R.id.filter_set_state_lay);

                for(int i = 0; i <mTailorStateList.size(); i++) {
                    for (int j = i + 1; j < mTailorStateList.size(); j++) {
                        if (mTailorStateList.get(i).getStateId() == (mTailorStateList.get(j).getStateId())) {
                            mTailorStateList.remove(j);
                            j--;
                        }
                    }
                }
                for(int i = 0; i <mTailorStateList.size(); i++) {
                    mTailorStateList.get(i).setChecked(false);
                }

                for (int i=0;i< AppConstants.FILTER_STATE_ID_LIST.size();i++){
                    for (int j=0;j<mTailorStateList.size(); j++){
                        if ( AppConstants.FILTER_STATE_ID_LIST.get(i) == mTailorStateList.get(j).getStateId()){
                            mTailorStateList.get(j).setChecked(true);
                        }
                    }
                }

                if (mFilterStateAdapter!=null){
                    mFilterStateAdapter.notifyDataSetChanged();
                }

                if (NoOfOrder == 0 && Km == 0){
                    NoOfOrder = mTailorList.get(0).getNoofOrder();
                    Km = mTailorList.get(0).getDistance();
                for (int i=0; i<mTailorList.size(); i++){

                    if (mTailorList.get(i).getNoofOrder() > NoOfOrder){
                        NoOfOrder = mTailorList.get(i).getNoofOrder();
                    }

                    if (mTailorList.get(i).getDistance() > Km){
                        Km = mTailorList.get(i).getDistance();
                    }
                }
                }


                mNoOfOrderSeekBar.getConfigBuilder().max(NoOfOrder).build();

                mKmSeekBar.getConfigBuilder().max(Float.parseFloat(String.valueOf(Km))).build();

                mNoOfOrderSeekBar.setProgress(Integer.parseInt(String.valueOf(AppConstants.FILTER_NO_OF_ORDER)));
                mKmSeekBar.setProgress(Float.parseFloat(String.valueOf(AppConstants.FILTER_KM)));
                mFilterRatingBar.setRating(Float.parseFloat(String.valueOf(AppConstants.FILTER_RATING)));

                if (AppConstants.FILTER_STATE_ID_LIST.size() > 0){
                    mSetStateTxt.setText(getResources().getString(R.string.selected_state)+"("+String.valueOf(AppConstants.FILTER_STATE_ID_LIST.size())+")");
                }else {
                    mSetStateTxt.setText(getResources().getString(R.string.select_state));

                }

                mFilterStateAdapter = new FilterStateAdapter(this, mTailorStateList);
                mFilterRecyclerViewLay.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                mFilterRecyclerViewLay.setAdapter(mFilterStateAdapter);

                mCancelTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mFilterDialog.dismiss();
                    }
                });

                mResetTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mFilterDialog.dismiss();

                        mTailorListLoaderImg.setVisibility(View.VISIBLE);

                        if (handler != null){
                            handler.removeCallbacks(updateMarker);
                        }

                        updateMarker = new Runnable() {
                            @Override
                            public void run() {

                                AppConstants.FILTER_KM = 0;
                                AppConstants.FILTER_RATING = 0;
                                AppConstants.FILTER_NO_OF_ORDER = 0;
                                AppConstants.FILTER_STATE_ID_LIST = new ArrayList<>();

                                mKmSeekBar.setProgress(Float.parseFloat(String.valueOf(AppConstants.FILTER_KM)));
                                mNoOfOrderSeekBar.setProgress(Integer.parseInt(String.valueOf(AppConstants.FILTER_NO_OF_ORDER)));
                                mFilterRatingBar.setRating(Float.parseFloat(String.valueOf(AppConstants.FILTER_RATING)));

                                mSetStateTxt.setText(getResources().getString(R.string.select_state));

                                for (int i=0;i<mTailorStateList.size();i++){
                                    mTailorStateList.get(i).setChecked(false);
                                }
                                if (mFilterStateAdapter != null){
                                    mFilterStateAdapter.notifyDataSetChanged();
                                }
                                mTailorList =new ArrayList<>();
                                for (int i=0; i<mTailorOriginalList.size(); i++){
                                    TailorListEntity mEntitys = new TailorListEntity();
                                    mEntitys.setChecked(mTailorOriginalList.get(i).getChecked());
                                    mEntitys.setStateId(mTailorOriginalList.get(i).getStateId());
                                    mEntitys.setStateName(mTailorOriginalList.get(i).getStateName());
                                    mEntitys.setMarkerImg(AppConstants.IMAGE_BASE_URL+"Images/TailorImages/"+mTailorOriginalList.get(i).getShopOwnerImageURL());
                                    mEntitys.setAddressinArabic(mTailorOriginalList.get(i).getAddressinArabic());
                                    mEntitys.setAddressInEnglish(mTailorOriginalList.get(i).getAddressInEnglish());
                                    mEntitys.setAvgRating(mTailorOriginalList.get(i).getAvgRating());
                                    mEntitys.setCount(mTailorOriginalList.get(i).getCount());
                                    mEntitys.setDistance(mTailorOriginalList.get(i).getDistance());
                                    mEntitys.setDistanceText(mTailorOriginalList.get(i).getDistanceText());
                                    mEntitys.setId(mTailorOriginalList.get(i).getId());
                                    mEntitys.setLatitude(mTailorOriginalList.get(i).getLatitude());
                                    mEntitys.setLongitude(mTailorOriginalList.get(i).getLongitude());
                                    mEntitys.setNoofOrder(mTailorOriginalList.get(i).getNoofOrder());
                                    mEntitys.setOrderCount(mTailorOriginalList.get(i).getOrderCount());
                                    mEntitys.setPrice(mTailorOriginalList.get(i).getPrice());
                                    mEntitys.setRating(mTailorOriginalList.get(i).getRating());
                                    mEntitys.setShopNameInArabic(mTailorOriginalList.get(i).getShopNameInArabic());
                                    mEntitys.setShopNameInEnglish(mTailorOriginalList.get(i).getShopNameInEnglish());
                                    mEntitys.setShopOwnerImageURL(mTailorOriginalList.get(i).getShopOwnerImageURL());
                                    mEntitys.setTailorId(mTailorOriginalList.get(i).getTailorId());
                                    mEntitys.setTailorNameInArabic(mTailorOriginalList.get(i).getTailorNameInArabic());
                                    mEntitys.setTailorNameInEnglish(mTailorOriginalList.get(i).getTailorNameInEnglish());
                                    mTailorList.add(mEntitys);

                                }

                                mTailorFilterList = mTailorList;

                                setTailorListAdapter(mTailorFilterList);
                                mGoogleMap.clear();
                                initGoogleAPIClient();
                                setCurrentLocation();
                                onMapReady(mGoogleMap);

                                AppConstants.TAILOR_LIST = mTailorFilterList;

                                if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")){
                                    if (AppConstants.TAILOR_LIST.size()>0){
                                        final List<MarkerInfo> markerInfoList = SampleMarkerData.getSampleMarkersInfo();
                                        for (int i = 0; i < markerInfoList.size(); i++) {
                                            final MarkerInfo mi = markerInfoList.get(i);
                                            final long id = i;
                                            final int Ii = i;
                                            mupdateMarker = new Runnable() {
                                                @Override
                                                public void run() {
                                                    mGoogleMap.addMarker(new MarkerOptions().position(mi.getCoordinates()).
                                                            icon(BitmapDescriptorFactory.fromResource(R.drawable.shop_marker))).setTitle(String.valueOf(mTailorList.get(Ii).getTailorId()));
                                                    floatingMarkersOverlay.addMarker(id, mi);
                                                }
                                            };

                                            mhandler.postDelayed(mupdateMarker, MARKER_UPDATE_INTERVAL);


//                                            mGoogleMap.addMarker(new MarkerOptions().position(mi.getCoordinates()).
//                                                    icon(BitmapDescriptorFactory.fromBitmap(
//                                                            createCustomSelectedMarker(TailorListScreen.this, mTailorFilterList.get(i).getMarkerImg(),mTailorFilterList.get(i).getTailorNameInEnglish()))).title(String.valueOf(mTailorFilterList.get(i).getTailorId())));
//                                            floatingMarkersOverlay.addMarker(i, mi);
                                        }
                                    }
                                }else {
                                    for (int i = 0; i < mTailorFilterList.size(); i++) {
                                        Double lat = Double.parseDouble(mTailorFilterList.get(i).getLatitude());
                                        Double lon = Double.parseDouble(mTailorFilterList.get(i).getLongitude());

                                        LatLng customMarkerLocationOne = new LatLng(lat, lon);
                                        final int Ii = i;

                                        mupdateMarker = new Runnable() {
                                            @Override
                                            public void run() {

                                                mGoogleMap.addMarker(new MarkerOptions().position(customMarkerLocationOne).
                                                        icon(BitmapDescriptorFactory.fromResource(R.drawable.shop_marker))).setTitle(String.valueOf(mTailorList.get(Ii).getTailorId()));
                                            }
                                        };

                                        mhandler.postDelayed(mupdateMarker, MARKER_UPDATE_INTERVAL);

//                                        mGoogleMap.addMarker(new MarkerOptions().position(customMarkerLocationOne).
//                                                icon(BitmapDescriptorFactory.fromBitmap(
//                                                        createCustomSelectedMarker(TailorListScreen.this, mTailorFilterList.get(i).getMarkerImg(),mTailorFilterList.get(i).getTailorNameInEnglish()))).title(String.valueOf(mTailorFilterList.get(i).getTailorId())));

                                    }

                                }
                                mTailorListTotalListTxt.setText(String.valueOf(mTailorFilterList.size()));

                                AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();

                                for (int j=0; j<mTailorList.size(); j++){
                                    if (mTailorList.get(j).getChecked()){
                                        AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.add(mTailorList.get(j));
                                    }
                                }

                                mTailorSelectedTxt.setText(String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.size()));

                                mTailorListLoaderImg.setVisibility(View.GONE);

                            }
                        };

                        handler.postDelayed(updateMarker, MARKER_UPDATE_INTERVAL);


                    }
                });

                mApplyFilterLay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mFilterDialog.dismiss();

                        mTailorListLoaderImg.setVisibility(View.VISIBLE);

                        if (handler != null){
                            handler.removeCallbacks(updateMarker);
                        }

                        updateMarker = new Runnable() {
                            @Override
                            public void run() {

                                AppConstants.FILTER_KM = mKmSeekBar.getProgress();
                                AppConstants.FILTER_RATING = mFilterRatingBar.getRating();
                                AppConstants.FILTER_NO_OF_ORDER = mNoOfOrderSeekBar.getProgress();

                                AppConstants.FILTER_STATE_ID_LIST = new ArrayList<>();
                                for (int i=0;i<mTailorStateList.size();i++){
                                    if (mTailorStateList.get(i).getChecked()){
                                        AppConstants.FILTER_STATE_ID_LIST.add(mTailorStateList.get(i).getStateId());
                                    }
                                }

                                if (AppConstants.FILTER_STATE_ID_LIST.size() > 0){

                                    mTailorFilterList = new ArrayList<>();
                                    for (int i=0; i<mTailorList.size(); i++){
                                        for (int j=0; j<AppConstants.FILTER_STATE_ID_LIST.size(); j++){
                                            if (mTailorList.get(i).getStateId() == AppConstants.FILTER_STATE_ID_LIST.get(j)){
                                                TailorListEntity mEntitys = new TailorListEntity();
                                                mEntitys.setChecked(mTailorList.get(i).getChecked());
                                                mEntitys.setStateId(mTailorList.get(i).getStateId());
                                                mEntitys.setStateName(mTailorList.get(i).getStateName());
                                                mEntitys.setMarkerImg(AppConstants.IMAGE_BASE_URL+"Images/TailorImages/"+mTailorList.get(i).getShopOwnerImageURL());
                                                mEntitys.setAddressinArabic(mTailorList.get(i).getAddressinArabic());
                                                mEntitys.setAddressInEnglish(mTailorList.get(i).getAddressInEnglish());
                                                mEntitys.setAvgRating(mTailorList.get(i).getAvgRating());
                                                mEntitys.setCount(mTailorList.get(i).getCount());
                                                mEntitys.setDistance(mTailorList.get(i).getDistance());
                                                mEntitys.setDistanceText(mTailorList.get(i).getDistanceText());
                                                mEntitys.setId(mTailorList.get(i).getId());
                                                mEntitys.setLatitude(mTailorList.get(i).getLatitude());
                                                mEntitys.setLongitude(mTailorList.get(i).getLongitude());
                                                mEntitys.setNoofOrder(mTailorList.get(i).getNoofOrder());
                                                mEntitys.setOrderCount(mTailorList.get(i).getOrderCount());
                                                mEntitys.setPrice(mTailorList.get(i).getPrice());
                                                mEntitys.setRating(mTailorList.get(i).getRating());
                                                mEntitys.setShopNameInArabic(mTailorList.get(i).getShopNameInArabic());
                                                mEntitys.setShopNameInEnglish(mTailorList.get(i).getShopNameInEnglish());
                                                mEntitys.setShopOwnerImageURL(mTailorList.get(i).getShopOwnerImageURL());
                                                mEntitys.setTailorId(mTailorList.get(i).getTailorId());
                                                mEntitys.setTailorNameInArabic(mTailorList.get(i).getTailorNameInArabic());
                                                mEntitys.setTailorNameInEnglish(mTailorList.get(i).getTailorNameInEnglish());
                                                mTailorFilterList.add(mEntitys);
                                            }
                                        }
                                    }

                                }else {
                                    mTailorList = new ArrayList<>();
                                    for (int i=0; i<mTailorOriginalList.size(); i++){
                                        TailorListEntity mEntitys = new TailorListEntity();
                                        mEntitys.setChecked(mTailorOriginalList.get(i).getChecked());
                                        mEntitys.setStateId(mTailorOriginalList.get(i).getStateId());
                                        mEntitys.setStateName(mTailorOriginalList.get(i).getStateName());
                                        mEntitys.setMarkerImg(AppConstants.IMAGE_BASE_URL+"Images/TailorImages/"+mTailorOriginalList.get(i).getShopOwnerImageURL());
                                        mEntitys.setAddressinArabic(mTailorOriginalList.get(i).getAddressinArabic());
                                        mEntitys.setAddressInEnglish(mTailorOriginalList.get(i).getAddressInEnglish());
                                        mEntitys.setAvgRating(mTailorOriginalList.get(i).getAvgRating());
                                        mEntitys.setCount(mTailorOriginalList.get(i).getCount());
                                        mEntitys.setDistance(mTailorOriginalList.get(i).getDistance());
                                        mEntitys.setDistanceText(mTailorOriginalList.get(i).getDistanceText());
                                        mEntitys.setId(mTailorOriginalList.get(i).getId());
                                        mEntitys.setLatitude(mTailorOriginalList.get(i).getLatitude());
                                        mEntitys.setLongitude(mTailorOriginalList.get(i).getLongitude());
                                        mEntitys.setNoofOrder(mTailorOriginalList.get(i).getNoofOrder());
                                        mEntitys.setOrderCount(mTailorOriginalList.get(i).getOrderCount());
                                        mEntitys.setPrice(mTailorOriginalList.get(i).getPrice());
                                        mEntitys.setRating(mTailorOriginalList.get(i).getRating());
                                        mEntitys.setShopNameInArabic(mTailorOriginalList.get(i).getShopNameInArabic());
                                        mEntitys.setShopNameInEnglish(mTailorOriginalList.get(i).getShopNameInEnglish());
                                        mEntitys.setShopOwnerImageURL(mTailorOriginalList.get(i).getShopOwnerImageURL());
                                        mEntitys.setTailorId(mTailorOriginalList.get(i).getTailorId());
                                        mEntitys.setTailorNameInArabic(mTailorOriginalList.get(i).getTailorNameInArabic());
                                        mEntitys.setTailorNameInEnglish(mTailorOriginalList.get(i).getTailorNameInEnglish());
                                        mTailorList.add(mEntitys);

                                    }
                                    mTailorFilterList = mTailorList;
                                }

                                if (AppConstants.FILTER_KM != 0){
                                    for (int i=0; i<mTailorFilterList.size(); i++){
                                        if (mTailorFilterList.get(i).getDistance() > AppConstants.FILTER_KM){
                                            mTailorFilterList.remove(i);
                                            i--;
                                        }
                                    }

                                    for (int i=0; i<mTailorFilterList.size(); i++){
                                        if (mTailorFilterList.get(i).getDistanceText().equalsIgnoreCase("N/A")){
                                            mTailorFilterList.remove(i);
                                            i--;
                                        }
                                    }
                                }

                                if (AppConstants.FILTER_NO_OF_ORDER != 0){
                                    for (int i=0; i<mTailorFilterList.size(); i++){
                                        if (mTailorFilterList.get(i).getNoofOrder() < AppConstants.FILTER_NO_OF_ORDER){
                                            mTailorFilterList.remove(i);
                                            i--;
                                        }
                                    }
                                }

                                if (AppConstants.FILTER_RATING != 0){
                                    for (int i=0; i<mTailorFilterList.size(); i++){
                                        if (mTailorFilterList.get(i).getAvgRating() < AppConstants.FILTER_RATING){
                                            mTailorFilterList.remove(i);
                                            i--;
                                        }
                                    }
                                }

                                setTailorListAdapter(mTailorFilterList);
                                mGoogleMap.clear();
                                initGoogleAPIClient();
                                setCurrentLocation();
                                onMapReady(mGoogleMap);

                                AppConstants.TAILOR_LIST = mTailorFilterList;

                                if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")){
                                    if (AppConstants.TAILOR_LIST.size()>0){
                                        final List<MarkerInfo> markerInfoList = SampleMarkerData.getSampleMarkersInfo();
                                        for (int i = 0; i < markerInfoList.size(); i++) {
                                            final MarkerInfo mi = markerInfoList.get(i);
                                            final long id = i;
                                            final int Ii = i;
                                            mupdateMarker = new Runnable() {
                                                @Override
                                                public void run() {
                                                    mGoogleMap.addMarker(new MarkerOptions().position(mi.getCoordinates()).
                                                            icon(BitmapDescriptorFactory.fromResource(R.drawable.shop_marker))).setTitle(String.valueOf(mTailorList.get(Ii).getTailorId()));
                                                    floatingMarkersOverlay.addMarker(id, mi);
                                                }
                                            };
                                            mhandler.postDelayed(mupdateMarker, MARKER_UPDATE_INTERVAL);
//                                            mGoogleMap.addMarker(new MarkerOptions().position(mi.getCoordinates()).
//                                                    icon(BitmapDescriptorFactory.fromBitmap(
//                                                            createCustomSelectedMarker(TailorListScreen.this, mTailorFilterList.get(i).getMarkerImg(),mTailorFilterList.get(i).getTailorNameInEnglish()))).title(String.valueOf(mTailorFilterList.get(i).getTailorId())));
//                                            floatingMarkersOverlay.addMarker(i, mi);
                                        }
                                    }
                                }else {
                                    for (int i = 0; i < mTailorFilterList.size(); i++) {
                                        Double lat = Double.parseDouble(mTailorFilterList.get(i).getLatitude());
                                        Double lon = Double.parseDouble(mTailorFilterList.get(i).getLongitude());

                                        LatLng customMarkerLocationOne = new LatLng(lat, lon);
                                        final int Ii = i;

                                        mupdateMarker = new Runnable() {
                                            @Override
                                            public void run() {

                                                mGoogleMap.addMarker(new MarkerOptions().position(customMarkerLocationOne).
                                                        icon(BitmapDescriptorFactory.fromResource(R.drawable.shop_marker))).setTitle(String.valueOf(mTailorList.get(Ii).getTailorId()));
                                            }
                                        };
                                        mhandler.postDelayed(mupdateMarker, MARKER_UPDATE_INTERVAL);

//                                        mGoogleMap.addMarker(new MarkerOptions().position(customMarkerLocationOne).
//                                                icon(BitmapDescriptorFactory.fromBitmap(
//                                                        createCustomSelectedMarker(TailorListScreen.this, mTailorFilterList.get(i).getMarkerImg(),mTailorFilterList.get(i).getTailorNameInEnglish()))).title(String.valueOf(mTailorFilterList.get(i).getTailorId())));

                                    }

                                }

                                mTailorListTotalListTxt.setText(String.valueOf(mTailorFilterList.size()));

                                AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();

                                for (int j=0; j<mTailorList.size(); j++){
                                    if (mTailorList.get(j).getChecked()){
                                        AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.add(mTailorList.get(j));
                                    }
                                }
                                mTailorSelectedTxt.setText(String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.size()));

                                mTailorListLoaderImg.setVisibility(View.GONE);

                            }
                        };

                        handler.postDelayed(updateMarker, MARKER_UPDATE_INTERVAL);

                    }
                });

                mSetStateLay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSetRecyclerViewLay.setVisibility(View.VISIBLE);
                        mFilterParLay.setVisibility(View.GONE);
                    }
                });

                mStateCancelTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSetRecyclerViewLay.setVisibility(View.GONE);
                        mFilterParLay.setVisibility(View.VISIBLE);
                    }
                });

                mStateResetTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (int i=0; i<mTailorStateList.size(); i++){
                            mTailorStateList.get(i).setChecked(false);
                        }
                        if (mFilterStateAdapter!=null){
                            mFilterStateAdapter.notifyDataSetChanged();
                        }

                        mSetStateTxt.setText(getResources().getString(R.string.select_state));

                    }
                });

                mFilterDoneLay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSetRecyclerViewLay.setVisibility(View.GONE);
                        mFilterParLay.setVisibility(View.VISIBLE);
                        int count = 0;
                        for (int i=0;i<mTailorStateList.size();i++){
                            if (mTailorStateList.get(i).getChecked()){
                                count = count + 1;
                            }
                        }
                        if (count > 0){
                            mSetStateTxt.setText(getResources().getString(R.string.selected_state)+"("+String.valueOf(count)+")");
                        }else {
                            mSetStateTxt.setText(getResources().getString(R.string.select_state));
                        }
                    }
                });

                alertShowing(mFilterDialog);
                break;
            case R.id.bottom_sheet_time_lay:
                getTimeingDialog();
                break;
        }

    }

    /*Set Adapter for the Recycler view*/
    public void setTailorListAdapter(ArrayList<TailorListEntity> mTailorList) {

        mEmptyListLay.setVisibility(mTailorList.size() > 0 ? View.GONE : View.VISIBLE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mTailorAdapter = new TailorListAdapter(this, mTailorList, mLatitude, mLongitude);
        mTailorRecyclerView.setLayoutManager(layoutManager);
        mTailorRecyclerView.setAdapter(mTailorAdapter);

        mTailorRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = layoutManager.findLastVisibleItemPosition();

                if (lastPosition == mTailorList.size()-1){
                    mTailorListScrollViewImg.setVisibility(View.GONE);
                }else {
                    mTailorListScrollViewImg.setVisibility(View.VISIBLE);
                }
            }
        });

        if (mShowingPopUp == 1){
            mShowingPopUp = ++mShowingPopUp;
            if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")){
                getHintDialog();

            }
        }
    }

    public void getTimeingDialog(){
        alertDismiss(mTimeningDialog);
        mTimeningDialog = getDialog(TailorListScreen.this, R.layout.pop_up_country_alert);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mTimeningDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.BOTTOM);
            window.getAttributes().windowAnimations = R.style.PopupBottomAnimation;
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mTimeningDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(mTimeningDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        TextView mSetHeaderTxt,mCancelTxt;

        RecyclerView mShopDetailsRecyclerView;

        mSetHeaderTxt = mTimeningDialog.findViewById(R.id.country_header_text_cancel);
        mShopDetailsRecyclerView = mTimeningDialog.findViewById(R.id.country_popup_recycler_view);
        mCancelTxt = mTimeningDialog.findViewById(R.id.country_text_cancel);

        mSetHeaderTxt.setText(getResources().getString(R.string.shop_timeing));

        ArrayList<ShopTimingEntity> mShopTimeList = new ArrayList<>();
        ShopTimingEntity shopTimeEntity =  new ShopTimingEntity();
        shopTimeEntity.setChecked(false);
        shopTimeEntity.setShopDay("Sunday");
        shopTimeEntity.setShopTime("9.00am - 9.00 pm");
        shopTimeEntity.setChecked(false);
        mShopTimeList.add(shopTimeEntity);
        shopTimeEntity = new ShopTimingEntity();
        shopTimeEntity.setShopDay("Monday");
        shopTimeEntity.setShopTime("9.00am - 9.00 pm");
        mShopTimeList.add(shopTimeEntity);
        shopTimeEntity = new ShopTimingEntity();
        shopTimeEntity.setChecked(false);
        shopTimeEntity.setShopDay("Tuesday");
        shopTimeEntity.setShopTime("9.00am - 9.00 pm");
        mShopTimeList.add(shopTimeEntity);
        shopTimeEntity = new ShopTimingEntity();
        shopTimeEntity.setChecked(false);
        shopTimeEntity.setShopDay("Wednesday");
        shopTimeEntity.setShopTime("9.00am - 9.00 pm");
        mShopTimeList.add(shopTimeEntity);
        shopTimeEntity = new ShopTimingEntity();
        shopTimeEntity.setChecked(false);
        shopTimeEntity.setShopDay("Thrusday");
        shopTimeEntity.setShopTime("9.00am - 9.00 pm");
        mShopTimeList.add(shopTimeEntity);
        shopTimeEntity = new ShopTimingEntity();
        shopTimeEntity.setChecked(false);
        shopTimeEntity.setShopDay("Friday");
        shopTimeEntity.setShopTime("2.00pm - 10.00 pm");
        mShopTimeList.add(shopTimeEntity);
        shopTimeEntity = new ShopTimingEntity();
        shopTimeEntity.setChecked(false);
        shopTimeEntity.setShopDay("Saturday");
        shopTimeEntity.setShopTime("9.00am - 9.00 pm");
        mShopTimeList.add(shopTimeEntity);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE", Locale.ENGLISH);
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);

        for (int i=0; i<mShopTimeList.size(); i++){
            if (dayOfTheWeek.equals(mShopTimeList.get(i).getShopDay())){
                mShopTimeList.get(i).setChecked(true);
            }
        }

        mShopTimingAdapter = new ShopTimingAdapter(this,mShopTimeList );
        mShopDetailsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mShopDetailsRecyclerView.setAdapter(mShopTimingAdapter);

        mCancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTimeningDialog.dismiss();
            }
        });

        alertShowing(mTimeningDialog);
    }
    @Override
    public void onBackPressed() {
        if (BottomSheetBehavior.STATE_EXPANDED == mBottomSheetBehaviour.getState()) {
            mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);
        }else {
            AppConstants.FILTER_KM = 0;
            AppConstants.FILTER_RATING = 0;
            AppConstants.FILTER_NO_OF_ORDER = 0;
            AppConstants.FILTER_STATE_ID_LIST = new ArrayList<>();
            AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();
            super.onBackPressed();
            if(AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
                previousScreen(OrderFlowType.class,true);
            }
            this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);
        }
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.tailor_scree_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.tailor_scree_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
}
