package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.OrderDetailsMaterialAndReferenceAndPatternAdapter;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetRequestDetailsResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RequestViewMoreActivity extends BaseActivity {

    @BindView(R.id.request_details_par_lay)
    LinearLayout mOrderDetailsPayLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.request_details_order_id_txt)
    TextView mRequestDetailsOrderIdTxt;

    @BindView(R.id.request_details_order_placed_on_txt)
    TextView mRequestDetailsOrderPlacedOnTxt;

    @BindView(R.id.request_details_material_type_txt)
    TextView mRequestDetailsMaterialTypeTxt;

    @BindView(R.id.request_details_service_type_txt)
    TextView mRequestDetailsServiceTypeTxt;

    @BindView(R.id.request_details_delivery_type_txt)
    TextView mRequestDetailsDeliveryTypeTxt;

    @BindView(R.id.request_details_dress_type_txt)
    TextView mRequestDetailsDressTypeTxt;

    @BindView(R.id.request_details_sub_dress_type_txt)
    TextView mRequestDetailsSubDressTypeTxt;

    @BindView(R.id.request_details_dress_type_pic_img)
    ImageView mRequestDetailsDressTypePicImg;

    @BindView(R.id.ordder_details_companies_material_txt)
    TextView mRequestDetailsCompaniesMaterialTxt;

    @BindView(R.id.request_details_material_img_recycler_view)
    RecyclerView mRequestDetailsMaterialImgRecyclerView;

    @BindView(R.id.request_details_veiw_details_par_lay)
    RelativeLayout mRequestDetailsViewDetailsParLay;

    @BindView(R.id.request_details_reference_img_txt)
    TextView mRequestDetailsReferenceImgTxt;

    @BindView(R.id.request_details_reference_img_recycler_view)
    RecyclerView mRequestDetailsReferenceImgRecyclerView;

    @BindView(R.id.request_details_delivery_name_txt)
    TextView mRequestDetailsDeliveryNameTxt;

    @BindView(R.id.request_details_delivery_address_txt)
    TextView mRequestDetailsDeliveryAddressTxt;

    ArrayList<String> mMaterialAndPatternList;
    ArrayList<String> mReferenceList;

    OrderDetailsMaterialAndReferenceAndPatternAdapter mMaterialAndPatternAdapter;
    OrderDetailsMaterialAndReferenceAndPatternAdapter mReferenceAdapter;

    private UserDetailsEntity mUserDetailsEntityRes;

    String mMeasurementStr = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_request_view_more);

        initView();

    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mOrderDetailsPayLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(RequestViewMoreActivity.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        setHeader();

        AppConstants.DIRECT_ORDER ="Quotation";

        AppConstants.ORDER_DETAILS_ON_BACK = "ORDER_DETAIL";

        getRequestDetailsApiCall();

    }

    @OnClick({R.id.header_left_side_img,R.id.request_details_customization_lay,R.id.request_details_measurment_lay,R.id.request_details_view_details})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.request_details_customization_lay:
                AppConstants.ORDER_DETAILS_CUSTOMIZATION = "CUSTOMIZATION";
                AppConstants.VIEW_DETAILS_HEADER = getResources().getString(R.string.customization_images);
                nextScreen(ViewImageScreen.class, true);
                break;
            case R.id.request_details_measurment_lay:
                AppConstants.NEW_ORDER = "ADD_MEASUREMENT";
                AppConstants.ORDER_DETAILS_MEASUREMENT = "REQUEST";
                nextScreen(MeasurementListScreen.class,true);
                break;
            case R.id.request_details_view_details:
                AppConstants.VIEW_DETAILS_RES = "GONE";
                nextScreen(ViewDetailsScreen.class,true);
                break;

        }

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetRequestDetailsResponse){
            GetRequestDetailsResponse mResponse =(GetRequestDetailsResponse)resObj;

            if (mResponse.getResult().getOrderDetail().size()>0){
                AppConstants.PATTERN_ID = String.valueOf(mResponse.getResult().getOrderDetail().get(0).getPatternId());
                mRequestDetailsOrderIdTxt.setText(String.valueOf(mResponse.getResult().getOrderDetail().get(0).getOrderId()));
                mRequestDetailsOrderPlacedOnTxt.setText(mResponse.getResult().getOrderDetail().get(0).getOrderDt());

                try {
                    Glide.with(this)
                            .load(AppConstants.IMAGE_BASE_URL+"images/DressSubType/"+mResponse.getResult().getOrderDetail().get(0).getImage())
                            .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                            .apply(RequestOptions.skipMemoryCacheOf(true))
                            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                            .into(mRequestDetailsDressTypePicImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mRequestDetailsMaterialTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getMaterialTypeInArabic());
                    mMeasurementStr = mResponse.getResult().getOrderDetail().get(0).getMaterialTypeId();
                    mRequestDetailsServiceTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getServiceTypeInArabic());
                    mRequestDetailsDressTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getDressTypeName());
                    mRequestDetailsSubDressTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getNameInArabic());
                    mRequestDetailsDeliveryTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getDeliveryNameInArabic());

                }else {
                    mRequestDetailsMaterialTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getMaterialType());
                    mMeasurementStr = mResponse.getResult().getOrderDetail().get(0).getMaterialTypeId();
                    mRequestDetailsServiceTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getServiceType());
                    mRequestDetailsDressTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getDressTypeName());
                    mRequestDetailsSubDressTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getProduct_Name());
                    mRequestDetailsDeliveryTypeTxt.setText(mResponse.getResult().getOrderDetail().get(0).getDeliveryNameInEnglish());

                }
                if (mMeasurementStr.equalsIgnoreCase("3")){
                    mRequestDetailsViewDetailsParLay.setVisibility(View.VISIBLE);
                }
            }
            if (mResponse.getResult().getBuyerAddress().size()>0){
                mRequestDetailsDeliveryNameTxt.setText(mResponse.getResult().getBuyerAddress().get(0).getFirstName());
                mRequestDetailsDeliveryAddressTxt.setText(mResponse.getResult().getBuyerAddress().get(0).getFloor()+", "+mResponse.getResult().getBuyerAddress().get(0).getArea()+", \n"+mResponse.getResult().getBuyerAddress().get(0).getStateName()+", \n"+mResponse.getResult().getBuyerAddress().get(0).getCountry_Name()+", \n"+mResponse.getResult().getBuyerAddress().get(0).getPhoneNo());
            }
            if (mResponse.getResult().getAdditionalMaterialImage().size()>0){
                mRequestDetailsCompaniesMaterialTxt.setText(getResources().getString(R.string.material_details_company_material));
                mRequestDetailsViewDetailsParLay.setVisibility(View.VISIBLE);
            }
            if (mResponse.getResult().getMaterialImage().size()>0){
                if (mMeasurementStr.equalsIgnoreCase("1")){
                    mRequestDetailsCompaniesMaterialTxt.setText(getResources().getString(R.string.material_details_own_material_direct_delivery));

                }else if (mMeasurementStr.equalsIgnoreCase("2")){
                    mRequestDetailsCompaniesMaterialTxt.setText(getResources().getString(R.string.material_details_own_material_courier_material));

                }
                mRequestDetailsViewDetailsParLay.setVisibility(View.GONE);
            }
            if (mResponse.getResult().getReferenceImage().size()>0){
                mRequestDetailsReferenceImgTxt.setVisibility(View.VISIBLE);
                mRequestDetailsReferenceImgRecyclerView.setVisibility(View.VISIBLE);
            }
            mMaterialAndPatternList = new ArrayList<>();
            mReferenceList = new ArrayList<>();

            for (int i=0; i<mResponse.getResult().getMaterialImage().size(); i++){
                mMaterialAndPatternList.add(AppConstants.IMAGE_BASE_URL+"Images/MaterialImages/"+mResponse.getResult().getMaterialImage().get(i).getImage());
            }

            for (int i=0; i<mResponse.getResult().getReferenceImage().size(); i++){
                mReferenceList.add(AppConstants.IMAGE_BASE_URL+"Images/ReferenceImages/"+mResponse.getResult().getReferenceImage().get(i).getImage());
            }

            for (int i=0; i<mResponse.getResult().getAdditionalMaterialImage().size(); i++){
                mMaterialAndPatternList.add(AppConstants.IMAGE_BASE_URL+"Images/Pattern/"+mResponse.getResult().getAdditionalMaterialImage().get(i).getImage());
            }
            setMaterialAdapter(mMaterialAndPatternList);
            setReferenceAdapter(mReferenceList);

        }
    }

    /*Set Adapter for the Recycler view*/
    public void setMaterialAdapter(ArrayList<String> addMaterialList) {

        mMaterialAndPatternAdapter = new OrderDetailsMaterialAndReferenceAndPatternAdapter(this,addMaterialList,"Material");
        mRequestDetailsMaterialImgRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRequestDetailsMaterialImgRecyclerView.setAdapter(mMaterialAndPatternAdapter);

    }

    /*Set Adapter for the Recycler view*/
    public void setReferenceAdapter(ArrayList<String> addRefenceList) {

        mRequestDetailsReferenceImgTxt.setVisibility(addRefenceList.size()>0 ? View.VISIBLE : View.GONE);
        mReferenceAdapter = new OrderDetailsMaterialAndReferenceAndPatternAdapter(this,addRefenceList,"Reference");
        mRequestDetailsReferenceImgRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRequestDetailsReferenceImgRecyclerView.setAdapter(mReferenceAdapter);

    }


    public void getRequestDetailsApiCall(){
        if (NetworkUtil.isNetworkAvailable(RequestViewMoreActivity.this)){
            APIRequestHandler.getInstance().getRequestDetailsApiCall(AppConstants.REQUEST_LIST_ID,RequestViewMoreActivity.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(RequestViewMoreActivity.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getRequestDetailsApiCall();
                }
            });
        }
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.request_details));
        mRightSideImg.setVisibility(View.INVISIBLE);

    }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.request_details_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.request_details_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
