package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.StoreSellerAvailableAdapter;
import com.qoltech.mzyoonbuyer.entity.GetProductSellerEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.StoreCartResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StoreViewSellerScreen extends BaseActivity {

    @BindView(R.id.store_view_sellers_par_lay)
    LinearLayout mStoreViewSellerParLay;

    @BindView(R.id.header_store_search_lay)
    RelativeLayout mStoreSearchLay;

    @BindView(R.id.header_store_lay)
    RelativeLayout mStoreLay;

    @BindView(R.id.header_store_catogery_icon)
    ImageView mHeaderStoreCatogeryImg;

    @BindView(R.id.header_store_catogery_txt)
    TextView mHeaderStoreCatogeryTxt;

    @BindView(R.id.header_store_whis_list_icon)
    ImageView mHeaderStoreWishListImg;

    @BindView(R.id.header_store_whis_list_txt)
    TextView mHeaderStoreWishListTxt;

    @BindView(R.id.store_view_seller_available_products_txt)
    TextView mStoreViewSellerAvailableProductsTxt;

    @BindView(R.id.store_view_seller_available_rec_view)
    RecyclerView mStoreViewSellerAvailableRecView;

    @BindView(R.id.bottom_notification_lay)
    RelativeLayout mBottomBadgeLay;

    @BindView(R.id.bottom_notification_txt)
    TextView mBottomNotificationTxt;

    UserDetailsEntity mUserDetailsEntityRes;

    StoreSellerAvailableAdapter mStoreSellerAvailableAdapter;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_store_view_seller_screen);

        initView();

    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mStoreViewSellerParLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(StoreViewSellerScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        setStoreSellerAdapter(AppConstants.PRODUCT_SELLER_LIST);

        getStoreCartApiCall();

        AppConstants.STORE_PRODUCT_REFRESH = "REFRESH";
    }

    @OnClick({R.id.header_store_back_btn_img,R.id.header_store_catogery_lay,R.id.header_wish_list_par_lay,R.id.store_bottom_home_img,R.id.store_bottom_cart_img,R.id.header_store_search_par_lay,R.id.header_store_icon_par_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_store_back_btn_img:
                onBackPressed();
                break;
            case R.id.header_store_catogery_lay:
                nextScreen(StoreCategoryScreen.class,true);
                break;
            case R.id.header_wish_list_par_lay:
                nextScreen(StoreWishListScreen.class,true);
                break;
            case R.id.store_bottom_home_img:
                previousScreen(BottomNavigationScreen.class,true);
                break;
            case R.id.store_bottom_cart_img:
                nextScreen(CartScreen.class,true);
                break;
            case R.id.header_store_search_par_lay:
                nextScreen(StoreSearchScreen.class,true);
                break;
            case R.id.header_store_icon_par_lay:
                previousScreen(StoreDashboardScreen.class,true);
                break;
        }
    }

    public void setHeader(){

        mStoreSearchLay.setVisibility(View.GONE);
        mStoreLay.setVisibility(View.VISIBLE);

        mHeaderStoreCatogeryImg.setBackgroundResource(R.drawable.store_catogery_white_icon);
        mHeaderStoreCatogeryTxt.setTextColor(getResources().getColor(R.color.white));

        mHeaderStoreWishListImg.setBackgroundResource(R.drawable.store_heart_white_icon);
        mHeaderStoreWishListTxt.setTextColor(getResources().getColor(R.color.white));

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof StoreCartResponse) {

            int countQtyCart = 0;
            StoreCartResponse mREsponse = (StoreCartResponse)resObj;

            for (int i=0 ; i<mREsponse.getResult().size(); i++){
                countQtyCart = countQtyCart + mREsponse.getResult().get(i).getQuantity();
            }

            mBottomNotificationTxt.setText(String.valueOf(countQtyCart));
            if (countQtyCart > 0){
                mBottomBadgeLay.setVisibility(View.VISIBLE);
            }else {
                mBottomBadgeLay.setVisibility(View.GONE);

            }
        }
    }

    public void getStoreCartApiCall() {
        if (NetworkUtil.isNetworkAvailable(this)){
            APIRequestHandler.getInstance().getStoreCartListApi(PreferenceUtil.getStringValue(this,AppConstants.CARD_ID),"List",this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStoreCartApiCall();
                }
            });
        }
    }



    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.store_view_sellers_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.store_view_sellers_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }

    public void setStoreSellerAdapter(ArrayList<GetProductSellerEntity> mList){

        mStoreViewSellerAvailableProductsTxt.setText(String.valueOf(mList.size()) + " " +getResources().getString(R.string.sellers_available));

        mStoreSellerAvailableAdapter = new StoreSellerAvailableAdapter(mList,this);
        mStoreViewSellerAvailableRecView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        mStoreViewSellerAvailableRecView.setAdapter(mStoreSellerAvailableAdapter);

    }

}
