package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetStoreReviewRatingModal;
import com.qoltech.mzyoonbuyer.modal.InsertRatingAndReviewResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StoreWriteAnReviewScreen extends BaseActivity {

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.store_write_an_review_par_lay)
    LinearLayout mStoreWriteAnReviewParLay;

    @BindView(R.id.store_rate_and_review_img)
    ImageView mStoreRateAndReviewImg;

    @BindView(R.id.store_rate_and_review_name_txt)
    TextView mStoreRateAndReviewNameTxt;

    @BindView(R.id.store_rate_and_review_rating_bar)
    RatingBar mStoreRateAndReviewRatingBar;

    @BindView(R.id.store_order_review_edt_txt)
    EditText mStoreOrderReviewEdtTxt;

    @BindView(R.id.store_rating_and_review_submit_lay)
    RelativeLayout mStoreRatingAndReviewSubmitLay;


    private UserDetailsEntity mUserDetailsEntityRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_store_write_an_review_screen);

        initView();

    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mStoreWriteAnReviewParLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(StoreWriteAnReviewScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        getStoreApiCall();
    }

    public void getStoreApiCall(){
        if (NetworkUtil.isNetworkAvailable(StoreWriteAnReviewScreen.this)){
            APIRequestHandler.getInstance().getStoreRatingReviewApi(AppConstants.ORDER_PENDING_LINE_ID,this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStoreApiCall();
                }
            });
        }
    }

    @OnClick({R.id.header_left_side_img,R.id.store_rate_and_review_submit_txt})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.store_rate_and_review_submit_txt:
                if (mStoreOrderReviewEdtTxt.getText().toString().equalsIgnoreCase("")){
                    mStoreOrderReviewEdtTxt.clearAnimation();
                    mStoreOrderReviewEdtTxt.setError(getResources().getString(R.string.please_give_feedback));
                }else {
                    insertStoreRateAndReviewApiCall();
                }
                break;

        }
    }

    public void insertStoreRateAndReviewApiCall(){
        if (NetworkUtil.isNetworkAvailable(StoreWriteAnReviewScreen.this)){
            APIRequestHandler.getInstance().insertStoreRateAndReviewApiCall(AppConstants.ORDER_PENDING_ID,AppConstants.ORDER_PENDING_LINE_ID,String.valueOf(mStoreRateAndReviewRatingBar.getRating()),mStoreOrderReviewEdtTxt.getText().toString(),StoreWriteAnReviewScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(StoreWriteAnReviewScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    insertStoreRateAndReviewApiCall();
                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof InsertRatingAndReviewResponse){
            InsertRatingAndReviewResponse mREspone = (InsertRatingAndReviewResponse)resObj;
            DialogManager.getInstance().showAlertPopup(StoreWriteAnReviewScreen.this, getResources().getString(R.string.thank_you_for_your_feedback), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    onBackPressed();
                }
            });
        }
        if (resObj instanceof GetStoreReviewRatingModal) {
            GetStoreReviewRatingModal mREspone = (GetStoreReviewRatingModal)resObj;
            if (mREspone.getResult().size() > 0){
                mStoreRatingAndReviewSubmitLay.setVisibility(View.GONE);
                mStoreOrderReviewEdtTxt.setEnabled(false);
                mStoreRateAndReviewRatingBar.setIsIndicator(true);
                mHeaderTxt.setText(getResources().getString(R.string.view_review));
                mStoreOrderReviewEdtTxt.setText(mREspone.getResult().get(0).getReview());
                mStoreRateAndReviewRatingBar.setRating(Float.parseFloat(String.valueOf(mREspone.getResult().get(0).getRating())));
            }

        }
        }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.write_a_review));
        mRightSideImg.setVisibility(View.INVISIBLE);

        try {
            Glide.with(StoreWriteAnReviewScreen.this)
                    .load(AppConstants.IMAGE_BASE_URL + "Images/Products/" + AppConstants.STORE_ORDER_DETAILS_PRODUCT_IMG)
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(mStoreRateAndReviewImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        mStoreRateAndReviewNameTxt.setText(AppConstants.STORE_ORDER_DETAILS_PRODUCT_NAME);

    }


    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.store_write_an_review_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.store_write_an_review_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }
}
