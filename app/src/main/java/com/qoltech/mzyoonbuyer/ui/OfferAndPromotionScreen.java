package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.ViewDetailPagerAdapter;
import com.qoltech.mzyoonbuyer.entity.GetAllCategoriesEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetAllCategoriesResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ZoomOutPageTransformer;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OfferAndPromotionScreen extends BaseActivity {

    @BindView(R.id.offer_and_promotion_par_lay)
    LinearLayout mOfferAndPromotionPayLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.offer_and_promotion_txt)
    TextView mOfferAndPromotionAdvertismentTxt;

    @BindView(R.id.offer_and_promotion_stitching_img)
    ImageView mOfferAndPromotionStitchingImg;

    @BindView(R.id.offer_and_promotion_stitching_txt)
    TextView mOfferAndPromotionStitchingTxt;

    @BindView(R.id.offer_and_promotion_store_img)
    ImageView mOfferAndPromotionStoreImg;

    @BindView(R.id.offer_and_promotion_store_txt)
    TextView mOfferAndPromotionStoreTxt;

    @BindView(R.id.offer_view_pager)
    ViewPager mOfferViewpager;

    @BindView(R.id.offer_pageIndicatorView)
    com.rd.PageIndicatorView mPageIndicator;

    ViewDetailPagerAdapter mOfferAdapter;

    private UserDetailsEntity mUserDetailsEntityRes;

    ArrayList<GetAllCategoriesEntity> mList = new ArrayList<>();

    ArrayList<String> mBannerStr = new ArrayList<>();

    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;
    final long PERIOD_MS = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_offer_and_promotion);

        initView();
    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mOfferAndPromotionPayLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        getAllCategoriesApiCall();

    }

    public void getAllCategoriesApiCall(){
        if (NetworkUtil.isNetworkAvailable(OfferAndPromotionScreen.this)){
            APIRequestHandler.getInstance().getAllCategoriesApi(OfferAndPromotionScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OfferAndPromotionScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAllCategoriesApiCall();
                }
            });
        }
    }

    @OnClick({R.id.header_left_side_img,R.id.offer_and_promotion_stitching_lay,R.id.offer_and_promotion_store_lay})

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.offer_and_promotion_stitching_lay:
                AppConstants.OFFER_CATEGORIES_ID = String.valueOf(mList.get(0).getId());
                nextScreen(OffersScreen.class,true);
                break;
            case R.id.offer_and_promotion_store_lay:
                AppConstants.OFFER_CATEGORIES_ID = String.valueOf(mList.get(1).getId());
                    nextScreen(OffersScreen.class,true);
                break;
        }
    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.offer_and_promotion));
        mRightSideImg.setVisibility(View.INVISIBLE);
    }

    public void getLanguage(){
        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.offer_and_promotion_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.offer_and_promotion_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetAllCategoriesResponse){
            GetAllCategoriesResponse mResponse = (GetAllCategoriesResponse)resObj;

            mList = mResponse.getResult().getGetAllCategories();
            for (int i=0; i<mList.size(); i++){
                if (mList.get(i).getCategoriesNameInEnglis().equalsIgnoreCase("SPECIAL OFFERS") || mList.get(i).getCategoriesNameInEnglis().equalsIgnoreCase("BRAND")){
                    mList.remove(i);
                }
            }

            if (mResponse.getResult().getGetadvertisement().size() > 0){
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOfferAndPromotionAdvertismentTxt.setText(mResponse.getResult().getGetadvertisement().get(0).getAdvertisementInArabic());

                }else {
                    mOfferAndPromotionAdvertismentTxt.setText(mResponse.getResult().getGetadvertisement().get(0).getAdvertisement());

                }
            }
            for (int i=0; i<mResponse.getResult().getGetSliderImage().size(); i++){
                mBannerStr.add(AppConstants.IMAGE_BASE_URL+"Images/PromotionSliderImage/"+mResponse.getResult().getGetSliderImage().get(i).getSliderImage());

            }
            viewPagerGet(mBannerStr);
            if (mList.size() > 0){
                try {
                    Glide.with(getApplicationContext())
                            .load(AppConstants.IMAGE_BASE_URL+"Images/categories/"+mList.get(0).getImage())
                            .apply(new RequestOptions().placeholder(R.drawable.empty_img).error(R.drawable.empty_img))
                            .into(mOfferAndPromotionStitchingImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOfferAndPromotionStitchingTxt.setText(mList.get(0).getCategoriesNameInArabic());

                }else {
                    mOfferAndPromotionStitchingTxt.setText(mList.get(0).getCategoriesNameInEnglis());

                }
            }
            if (mList.size() > 1){
                try {
                    Glide.with(getApplicationContext())
                            .load(AppConstants.IMAGE_BASE_URL+"Images/categories/"+mList.get(1).getImage())
                            .apply(new RequestOptions().placeholder(R.drawable.empty_img).error(R.drawable.empty_img))
                            .into(mOfferAndPromotionStoreImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mOfferAndPromotionStoreTxt.setText(mList.get(1).getCategoriesNameInEnglis());

                }else {
                    mOfferAndPromotionStoreTxt.setText(mList.get(1).getCategoriesNameInEnglis());

                }
            }
        }
    }
    public void viewPagerGet(ArrayList<String> image){
        mOfferAdapter = new ViewDetailPagerAdapter(this,image);
        mOfferViewpager.setAdapter(mOfferAdapter);
        mOfferViewpager.setPageTransformer(true, new ZoomOutPageTransformer());
        mPageIndicator.setViewPager(mOfferViewpager);

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == image.size()) {
                    currentPage = 0;
                }
                mOfferViewpager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);

        //page change tracker
        mOfferViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        trackScreenName("ViewDetail");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
