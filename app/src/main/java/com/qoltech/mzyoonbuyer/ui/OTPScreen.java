package com.qoltech.mzyoonbuyer.ui;

import android.animation.ObjectAnimator;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.view.ViewCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.main.MyFirebaseInstanceIdService;
import com.qoltech.mzyoonbuyer.modal.IsExistedUserResponse;
import com.qoltech.mzyoonbuyer.modal.ValidateOTPResponse;
import com.qoltech.mzyoonbuyer.service.APICommonInterface;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPScreen extends BaseActivity {

@BindView(R.id.otp_par_lay)
    LinearLayout mOtpParLay;

//@BindView(R.id.header_left_side_img)
//    ImageView mHeaderLeftBackImg;

@BindView(R.id.otp_first_edt_txt)
    EditText mOtpFirstEdtTxt;

@BindView(R.id.otp_second_edt_txt)
EditText mOtpSecondEdtTxt;

@BindView(R.id.otp_third_edt_txt)
EditText mOtpThirdEdtTxt;

@BindView(R.id.otp_fourth_edt_txt)
EditText mOtpFourthEdtTxt;

@BindView(R.id.otp_fivth_edt_txt)
EditText mOtpFivthEdtTxt;

@BindView(R.id.otp_sixth_edt_txt)
EditText mOtpSixthEdtTxt;

@BindView(R.id.otp_resent_btn)
RelativeLayout mOtpResendBtn;

@BindView(R.id.otp_resent_txt)
TextView mOtpResentTxt;

@BindView(R.id.otp_timer_txt)
    TextView mOtpTimerTxt;

    @BindView(R.id.circularProgressBar)
    ProgressBar mCircularProgressBar;

    APICommonInterface mApiCommonInterface;

    MyFirebaseInstanceIdService mMyFirebaseInstanceIdService = new MyFirebaseInstanceIdService();

    private UserDetailsEntity mUserDetailsEntity;
    CountDownTimer countDownTimer;
    public int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_otpscreen);

        initView();

    }
    public void initView(){
        ButterKnife.bind(this);

        setupUI(mOtpParLay);

        setHeader();
        textNextFocusOTP();
        mUserDetailsEntity = new UserDetailsEntity();
        mApiCommonInterface = APIRequestHandler.getClient().create(APICommonInterface.class);
        getLanguage();
        mOtpFirstEdtTxt.requestFocus();
        mOtpFirstEdtTxt.setCursorVisible(true);
    }

    @OnClick({R.id.otp_change_num_btn,R.id.otp_resent_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.otp_change_num_btn:
                countDownTimer.cancel();
                counter = 0;
                onBackPressed();
                break;
//            case R.id.header_left_side_img:
//                onBackPressed();
//                break;
            case R.id.otp_resent_btn:
                mOtpResendBtn.setEnabled(false);
//                mOtpResendBtn.setAlpha(0.5f);
                mOtpResendBtn.setBackground(getResources().getDrawable(R.drawable.grey_border_with_corner));
                mOtpResentTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                countDownTimer.cancel();
                counter = 0;
                countDownTimer.start();
                resendOTPGeneraterApiCall();
                break;
        }

    }


    public void textNextFocusOTP(){

        mOtpFirstEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1){
                    mOtpFirstEdtTxt.clearFocus();
                    mOtpSecondEdtTxt.requestFocus();
                    mOtpSecondEdtTxt.setCursorVisible(true);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        mOtpSecondEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mOtpSecondEdtTxt.getText().toString().trim().length() >= 1){
                    mOtpSecondEdtTxt.clearFocus();
                    mOtpThirdEdtTxt.requestFocus();
                    mOtpThirdEdtTxt.setCursorVisible(true);
                }else {
                    mOtpSecondEdtTxt.clearFocus();
                    mOtpFirstEdtTxt.requestFocus();
                    mOtpFirstEdtTxt.setCursorVisible(true);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mOtpSecondEdtTxt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if (mOtpSecondEdtTxt.getText().toString().trim().length() == 0){
                        mOtpSecondEdtTxt.clearFocus();
                        mOtpFirstEdtTxt.requestFocus();
                        mOtpFirstEdtTxt.setCursorVisible(true);
                    }

                }
                return false;
            }
        });

        mOtpThirdEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count >= 1){
                    mOtpThirdEdtTxt.clearFocus();
                    mOtpFourthEdtTxt.requestFocus();
                    mOtpFourthEdtTxt.setCursorVisible(true);
                }else {
                    mOtpThirdEdtTxt.clearFocus();
                    mOtpSecondEdtTxt.requestFocus();
                    mOtpSecondEdtTxt.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mOtpThirdEdtTxt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if (mOtpThirdEdtTxt.getText().toString().trim().length() == 0){
                        mOtpThirdEdtTxt.clearFocus();
                        mOtpSecondEdtTxt.requestFocus();
                        mOtpSecondEdtTxt.setCursorVisible(true);
                    }

                }
                return false;
            }
        });
        mOtpFourthEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1){
                    mOtpFourthEdtTxt.clearFocus();
                    mOtpFivthEdtTxt.requestFocus();
                    mOtpFivthEdtTxt.setCursorVisible(true);
                }else {
                    mOtpFourthEdtTxt.clearFocus();
                    mOtpThirdEdtTxt.requestFocus();
                    mOtpThirdEdtTxt.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mOtpFourthEdtTxt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if (mOtpFourthEdtTxt.getText().toString().trim().length() == 0){
                        mOtpFourthEdtTxt.clearFocus();
                        mOtpThirdEdtTxt.requestFocus();
                        mOtpThirdEdtTxt.setCursorVisible(true);
                    }

                }
                return false;
            }
        });
        mOtpFivthEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1){
                    mOtpFivthEdtTxt.clearFocus();
                    mOtpSixthEdtTxt.requestFocus();
                    mOtpSixthEdtTxt.setCursorVisible(true);
                }else {
                    mOtpFivthEdtTxt.clearFocus();
                    mOtpFourthEdtTxt.requestFocus();
                    mOtpFourthEdtTxt.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mOtpFivthEdtTxt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if (mOtpFivthEdtTxt.getText().toString().trim().length() == 0){
                        mOtpFivthEdtTxt.clearFocus();
                        mOtpFourthEdtTxt.requestFocus();
                        mOtpFourthEdtTxt.setCursorVisible(true);
                    }

                }
                return false;
            }
        });
        mOtpSixthEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count >= 1) {
                        validateOTPApiCall();
                }else {
                    mOtpSixthEdtTxt.clearFocus();
                    mOtpFivthEdtTxt.requestFocus();
                    mOtpFivthEdtTxt.setCursorVisible(true);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mOtpSixthEdtTxt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if (mOtpSixthEdtTxt.getText().toString().trim().length() == 0){
                        mOtpSixthEdtTxt.clearFocus();
                        mOtpFivthEdtTxt.requestFocus();
                        mOtpFivthEdtTxt.setCursorVisible(true);
                    }

                }
                return false;
            }
        });
    }

    public void resendOTPGeneraterApiCall(){
        if (NetworkUtil.isNetworkAvailable(OTPScreen.this)){
            APIRequestHandler.getInstance().resendOTPGenerateAPICall(AppConstants.COUNTRY_CODE,AppConstants.MOBILE_NUM,AppConstants.DEVICE_ID,mUserDetailsEntity.getLanguage(),OTPScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OTPScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    resendOTPGeneraterApiCall();
                }
            });
        }
    }

    public void validateOTPApiCall(){
        if (NetworkUtil.isNetworkAvailable(OTPScreen.this)){
            String OTP = mOtpFirstEdtTxt.getText().toString()+mOtpSecondEdtTxt.getText().toString()+mOtpThirdEdtTxt.getText().toString()+mOtpFourthEdtTxt.getText().toString()+mOtpFivthEdtTxt.getText().toString()+mOtpSixthEdtTxt.getText().toString();
            APIRequestHandler.getInstance().validateOTPAPICall(AppConstants.COUNTRY_CODE,AppConstants.MOBILE_NUM,AppConstants.DEVICE_ID,OTP,OTPScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OTPScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    validateOTPApiCall();
                }
            });
        }
    }

    public void isExistedUserApiCall(final String userId){
        if (NetworkUtil.isNetworkAvailable(OTPScreen.this)){
            APIRequestHandler.getInstance().isExistedUserApiCall(userId,OTPScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OTPScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    isExistedUserApiCall(userId);
                }
            });
        }
    }

    public void setHeader(){
//        mHeaderLeftBackImg.setVisibility(View.VISIBLE);
        mOtpResendBtn.setEnabled(false);
//        mOtpResendBtn.setAlpha(0.5f);
        mOtpResendBtn.setBackground(getResources().getDrawable(R.drawable.grey_border_with_corner));
        mOtpResentTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
        Drawable drawable = getResources().getDrawable(R.drawable.progressbar);
        mCircularProgressBar.setProgress(0);   // Main Progress
        mCircularProgressBar.setSecondaryProgress(0); // Secondary Progress
        mCircularProgressBar.setMax(30); // Maximum Progress
        mCircularProgressBar.setProgressDrawable(drawable);
        final ObjectAnimator animation = ObjectAnimator.ofInt(mCircularProgressBar, "progress", 0, 30);
        animation.setDuration(mCircularProgressBar.getProgress());
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();

        int millsec = 30 * 1000;
        countDownTimer = new CountDownTimer(millsec, 1000) {
            public void onTick(long millisUntilFinished) {

                String formattedTime = String.format("%02d S",

                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));
                mOtpTimerTxt.setText(formattedTime);
                counter++;
                mCircularProgressBar.setProgress(counter + 1);
            }

            public void onFinish() {
                mOtpTimerTxt.setText("00 S");
                mOtpResendBtn.setEnabled(true);
//                mOtpResendBtn.setAlpha(1f);
                mOtpResendBtn.setBackground(getResources().getDrawable(R.drawable.app_clr_gradient_with_corner));
                mOtpResentTxt.setTextColor(getResources().getColor(R.color.white));
            }
        }.start();
//        startTimer();

    }


    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof ValidateOTPResponse) {
            ValidateOTPResponse mResponse = (ValidateOTPResponse) resObj;
            if (!mResponse.getResult().equalsIgnoreCase("-1")) {
                isExistedUserApiCall(mResponse.getUserId());
                AppConstants.USER_ID = Integer.parseInt(mResponse.getUserId());
                AppConstants.USER_ID_STR = mResponse.getUserId();
//                if (PreferenceUtil.getStringValue(OTPScreen.this,AppConstants.CARD_ID).equalsIgnoreCase("")||PreferenceUtil.getStringValue(OTPScreen.this,AppConstants.CARD_ID) == null ){
//
//
//                    (mResponse.getUserId());
//                }
                InsertLanguageApiCall(mResponse.getUserId());
                countDownTimer.cancel();
                counter = 0;
            } else {
                Toast.makeText(OTPScreen.this, getResources().getString(R.string.invalid_otp), Toast.LENGTH_SHORT).show();
            }

        }


        if (resObj instanceof IsExistedUserResponse) {
            IsExistedUserResponse mResponse = (IsExistedUserResponse) resObj;
            if (mResponse.getResult().equalsIgnoreCase("Existing User")) {
                PreferenceUtil.storeBoolPreferenceValue(OTPScreen.this,
                        AppConstants.LOGIN_STATUS, true);

                mUserDetailsEntity.setUSER_ID(AppConstants.USER_ID_STR);
                mUserDetailsEntity.setMOBILE_NUM(AppConstants.MOBILE_NUM);
                mUserDetailsEntity.setCOUNTRY_CODE(AppConstants.COUNTRY_CODE);
                if (mUserDetailsEntity.getTOKEN().equalsIgnoreCase("")){
                    mMyFirebaseInstanceIdService.onTokenRefresh();
                    mUserDetailsEntity.setTOKEN(AppConstants.TOKEN);
                }else {
                    mUserDetailsEntity.setTOKEN(mUserDetailsEntity.getTOKEN());
                }
                PreferenceUtil.storeUserDetails(OTPScreen.this, mUserDetailsEntity);

                if (AppConstants.SKIP_NOW.equalsIgnoreCase("CUSTOMIZATION")){
                    previousScreen(CustomizationThreeScreen.class,true);
                }else if (AppConstants.SKIP_NOW.equalsIgnoreCase("CART")){
                    nextScreen(BottomNavigationScreen.class, true);
                }else {
                    nextScreen(BottomNavigationScreen.class, true);
                }
            } else {
                nextScreen(ProfileIntroScreen.class, true);

            }
        }

    }
    public void getLanguage(){
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(OTPScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntity = gson.fromJson(json, UserDetailsEntity.class);

        if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.otp_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.otp_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
        }
    }

    public void InsertLanguageApiCall(String userId) {
        if (NetworkUtil.isNetworkAvailable(OTPScreen.this)) {
            APIRequestHandler.getInstance().insertLanguageApiCall(userId, mUserDetailsEntity.getLanguage(), OTPScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(OTPScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    InsertLanguageApiCall(userId);
                }
            });
        }
    }
    public void createCartCustomerId(String UserId){
        DialogManager.getInstance().showProgress(this);
        if (NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.createCartApi("cart/CreateCart?CustomerId="+UserId).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    DialogManager.getInstance().hideProgress();
                    if (response.isSuccessful() && response.body() != null){
                        PreferenceUtil.storeStringValue(OTPScreen.this,AppConstants.CARD_ID,response.body());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    DialogManager.getInstance().hideProgress();

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    createCartCustomerId(UserId);
                }
            });
        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
        countDownTimer.cancel();
        counter = 0;
        countDownTimer.start();
        Toast.makeText(OTPScreen.this, getResources().getString(R.string.invalid_otp), Toast.LENGTH_SHORT).show();
    }
//    private BroadcastReceiver receiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if (intent.getAction().equalsIgnoreCase("otp")) {
//                final String message = intent.getStringExtra("message");
//
//                String number = message.replaceAll("[^0-9]", "");
//
//                String[] otp = number.split("");
//
//                for (int i=0; i<otp.length; i++){
//                    if (i == 1){
//                        mOtpFirstEdtTxt.setText(String.valueOf(otp[1]));
//                    }else if (i == 2){
//                        mOtpSecondEdtTxt.setText(String.valueOf(otp[2]));
//                    }else if (i == 3){
//                        mOtpThirdEdtTxt.setText(String.valueOf(otp[3]));
//                    }else if (i == 4){
//                        mOtpFourthEdtTxt.setText(String.valueOf(otp[4]));
//                    }else if (i == 5){
//                        mOtpFivthEdtTxt.setText(String.valueOf(otp[5]));
//                    }else if (i == 6){
//                        mOtpSixthEdtTxt.setText(String.valueOf(otp[6]));
//                    }
//                }
//
////                Toast.makeText(OTPScreen.this,number,Toast.LENGTH_SHORT).show();
//            }
//        }
//    };
    @Override
    protected void onResume() {
//        LocalBroadcastManager.getInstance(this).
//                registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
        countDownTimer.cancel();
        counter = 0;
        countDownTimer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
