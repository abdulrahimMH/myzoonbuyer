package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.DeliveryTypeEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.DeliveryTypeResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DeliveryTypeScreen extends BaseActivity {

    @BindView(R.id.delivery_type_par_lay)
    LinearLayout mDeliveryTypeParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.delivery_type_collect_from_store_body_img)
    ImageView mDeliveryTypeCollectFromBodytBodyImg;

    @BindView(R.id.delivery_type_normal_body_img)
    ImageView mDeliveryTypeNormalBodyImg;

    ArrayList<DeliveryTypeEntity> mDeliveryTypeList;

    private UserDetailsEntity mUserDetailsEntityRes;

    @BindView(R.id.delivery_type_collect_from_store_lay)
    LinearLayout mDeliveryTypeUrgentLay;

    @BindView(R.id.delivery_type_normal_lay)
    LinearLayout mDeliveryTypeNormalLay;

    @BindView(R.id.new_flow_delivery_wiz_lay)
    RelativeLayout mNewFlowDeliveryWizLay;

    @BindView(R.id.delivery_wiz_lay)
    RelativeLayout mDeliveryWizLay;

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_delivery_type_screen);
        initView();

    }
    public void initView() {

        ButterKnife.bind(this);

        setupUI(mDeliveryTypeParLay);

        mDeliveryTypeList = new ArrayList<>();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(DeliveryTypeScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        setHeader();

        getDeliveryApiCall();

    }

    public void getDeliveryApiCall(){
        if (NetworkUtil.isNetworkAvailable(DeliveryTypeScreen.this)){
            APIRequestHandler.getInstance().getDeliveryType(DeliveryTypeScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(DeliveryTypeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getDeliveryApiCall();
                }
            });
        }
    }

    @OnClick({R.id.header_left_side_img, R.id.delivery_type_collect_from_store_lay, R.id.delivery_type_normal_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.delivery_type_collect_from_store_lay:
                if (mDeliveryTypeList.size() > 0) {
                    AppConstants.DELIVERY_ID = String.valueOf(mDeliveryTypeList.get(0).getId());
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        AppConstants.DELIVERY_TYPE_ARABIC_NAME = mDeliveryTypeList.get(0).getNameInArabic();
                    }else {
                        AppConstants.DELIVERY_TYPE_ENG_NAME = mDeliveryTypeList.get(0).getNameInEnglish();

                    }
//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        nextScreen(DirectOrderPriceScreen.class, true);
//                    } else {
                        nextScreen(StitchingTimeScreen.class, true);
//                    }
                }
                break;

            case R.id.delivery_type_normal_lay:
                if (mDeliveryTypeList.size() > 1) {
                    AppConstants.DELIVERY_ID = String.valueOf(mDeliveryTypeList.get(1).getId());
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        AppConstants.DELIVERY_TYPE_ARABIC_NAME = mDeliveryTypeList.get(1).getNameInArabic();
                    }else {
                        AppConstants.DELIVERY_TYPE_ENG_NAME = mDeliveryTypeList.get(1).getNameInEnglish();

                    }
//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        nextScreen(DirectOrderPriceScreen.class, true);
//                    } else {
                        nextScreen(StitchingTimeScreen.class, true);
//                    }
                }
                break;

        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof DeliveryTypeResponse) {
            DeliveryTypeResponse mResponse = (DeliveryTypeResponse) resObj;

            mDeliveryTypeList = mResponse.getResult();

            for (int i = 0; i < mResponse.getResult().size(); i++) {
                if (i == 0) {

                    try {
                        Glide.with(DeliveryTypeScreen.this)
                                .load(AppConstants.IMAGE_BASE_URL + "Images/Delivery/" + mResponse.getResult().get(0).getImage())
                                .apply(new RequestOptions().placeholder(R.drawable.gender_background_img).error(R.drawable.gender_background_img))
                                .into(mDeliveryTypeCollectFromBodytBodyImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                } else if (i == 1) {

                    try {
                        Glide.with(DeliveryTypeScreen.this)
                                .load(AppConstants.IMAGE_BASE_URL + "Images/Delivery/" + mResponse.getResult().get(1).getImage())
                                .apply(new RequestOptions().placeholder(R.drawable.gender_background_img).error(R.drawable.gender_background_img))
                                .into(mDeliveryTypeNormalBodyImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                }

            }
        }
    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.delivery_type));
        mRightSideImg.setVisibility(View.INVISIBLE);

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
            mDeliveryWizLay.setVisibility(View.GONE);
            mNewFlowDeliveryWizLay.setVisibility(View.VISIBLE);
        } else {
            mDeliveryWizLay.setVisibility(View.VISIBLE);
            mNewFlowDeliveryWizLay.setVisibility(View.GONE);
        }
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.delivery_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.delivery_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
