package com.qoltech.mzyoonbuyer.ui;

import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoView;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.CustomizationThreeAdapter;
import com.qoltech.mzyoonbuyer.entity.GetCustomizationThreeEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetCustomizationThreeResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseCustomizationScreen extends BaseActivity {

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.customization_wiz_lay)
    RelativeLayout mCustomizationWizLay;

    @BindView(R.id.new_flow_customization_wiz_lay)
    RelativeLayout mNewFlowCustomizationWizLay;

    @BindView(R.id.choose_chustomization_par_lay)
    LinearLayout mChooseCustomizationParLay;

    UserDetailsEntity mUserDetailsEntityRes = new UserDetailsEntity();

    @BindView(R.id.choose_customization_recycler_view)
    RecyclerView mChooseCustomizationRecyclerView;

    private CustomizationThreeAdapter mCustomizeAdapter;

    @BindView(R.id.choose_customization_img_lay)
    public RelativeLayout mChooseCustomizatoinImgLay;

    @BindView(R.id.choose_customization_list_lay)
    public LinearLayout mChooseCustomizationListLay;

    @BindView(R.id.choose_customization_scroll_view_img)
    ImageView mChooseCustomizationScrollViewImg;

    public static ImageView myImages;

    public static PhotoView photo_views;
    RectF rectf;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_choose_customization_screen);
        initView();

    }
    public void initView() {
        myImages = findViewById(R.id.myImage);

        photo_views=findViewById(R.id.photo_view);
        rectf=new RectF(0,0,0,0);

        ButterKnife.bind(this);

        setupUI(mChooseCustomizationListLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(ChooseCustomizationScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            getCustomizationThreeNewFlowApiCall();
            mCustomizationWizLay.setVisibility(View.GONE);
            mNewFlowCustomizationWizLay.setVisibility(View.VISIBLE);
        }else {
            mCustomizationWizLay.setVisibility(View.VISIBLE);
            mNewFlowCustomizationWizLay.setVisibility(View.GONE);
            getCustomizationThreeApiCall();
        }


    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(AppConstants.CUSTOMIZATION_NAME);
        mRightSideImg.setVisibility(View.INVISIBLE);

        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);

    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.choose_chustomization_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.choose_chustomization_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
    @OnClick({R.id.header_left_side_img,R.id.choose_customization_zoom_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.choose_customization_zoom_img:
                mChooseCustomizatoinImgLay.setVisibility(View.GONE);
                mChooseCustomizationListLay.setVisibility(View.VISIBLE);
                break;
        }
        }
    public void getCustomizationThreeApiCall(){
        if (NetworkUtil.isNetworkAvailable(ChooseCustomizationScreen.this)){
            APIRequestHandler.getInstance().getCustomizationThreeAPICall(AppConstants.CUSTOMIZATION_DRESS_TYPE_ID,ChooseCustomizationScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ChooseCustomizationScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCustomizationThreeApiCall();
                }
            });
        }
    }
    public void getCustomizationThreeNewFlowApiCall(){
        if (NetworkUtil.isNetworkAvailable(ChooseCustomizationScreen.this)){
            APIRequestHandler.getInstance().getCustomizationThreeNewFlowAPICall(AppConstants.CUSTOMIZATION_DRESS_TYPE_ID,String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId()),AppConstants.SUB_DRESS_TYPE_ID,ChooseCustomizationScreen.this);
        }else {

            DialogManager.getInstance().showNetworkErrorPopup(ChooseCustomizationScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCustomizationThreeNewFlowApiCall();
                }
            });
        }
    }
    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetCustomizationThreeResponse) {
            GetCustomizationThreeResponse mResponse = (GetCustomizationThreeResponse) resObj;

            setAdapter(mResponse.getResult());

        }
    }

    public void setAdapter(ArrayList<GetCustomizationThreeEntity> customizationThreeEntities){

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);

        mCustomizeAdapter = new CustomizationThreeAdapter(ChooseCustomizationScreen.this,customizationThreeEntities);
        mChooseCustomizationRecyclerView.setLayoutManager(layoutManager);
        mChooseCustomizationRecyclerView.setAdapter(mCustomizeAdapter);

        mChooseCustomizationRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = layoutManager.findLastVisibleItemPosition();

                if (lastPosition == customizationThreeEntities.size()-1){
                    mChooseCustomizationScrollViewImg.setVisibility(View.GONE);
                }else {
                    mChooseCustomizationScrollViewImg.setVisibility(View.VISIBLE);
                }
            }
        });


    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }

}
