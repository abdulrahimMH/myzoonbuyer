package com.qoltech.mzyoonbuyer.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.AddMeasurementListAdapter;
import com.qoltech.mzyoonbuyer.adapter.ManuallyAdapter;
import com.qoltech.mzyoonbuyer.entity.AddMeasurementEntity;
import com.qoltech.mzyoonbuyer.entity.ManuallyEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.AddMeasurementResponse;
import com.qoltech.mzyoonbuyer.modal.ManuallyResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddMeasurementScreen extends BaseActivity {

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.add_measurement_par_lay)
    RelativeLayout mAddMeasurementParLay;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    @BindView(R.id.add_new_measurement_recycler_view)
    RecyclerView mAddNewMeasurementRecyclerView;

    @BindView(R.id.add_new_measurement_list_par_lay)
    RelativeLayout mAddNewMeasurementListparLay;

    @BindView(R.id.add_measurement_scroll_view_img)
    ImageView mAddMeasurementScrollViewImg;

    private AddMeasurementListAdapter mMeasurementListAdapter;

    private ManuallyAdapter mMmanuallyAdapter;

    String mManuallyStr = "false";

    private ArrayList<ManuallyEntity> mManullyEntity;

    private UserDetailsEntity mUserDetailsEntityRes;
    Dialog mEditTxtDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_add_measurement_screen);

        initView();
    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mAddMeasurementParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(AddMeasurementScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        setHeader();

        getLanguage();

        if (AppConstants.NEW_ORDER.equalsIgnoreCase("NEW_ORDER")){
            mManullyEntity = new ArrayList<>();
            getManuallyApiCall();
        }else {
            getAddMeasurementListApiCall();

        }

    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.measurement_list));
        mRightSideImg.setVisibility(View.INVISIBLE);

        mEmptyListTxt.setText(getResources().getString(R.string.no_result_for_measurement_list));

    }

    public void getAddMeasurementListApiCall(){
        if (NetworkUtil.isNetworkAvailable(AddMeasurementScreen.this)){
            APIRequestHandler.getInstance().getAddMeasurementApiCall(mUserDetailsEntityRes.getUSER_ID(),AddMeasurementScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(AddMeasurementScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAddMeasurementListApiCall();
                }
            });
        }
    }

    public void getManuallyApiCall(){
        if (NetworkUtil.isNetworkAvailable(AddMeasurementScreen.this)){
            APIRequestHandler.getInstance().manuallyApiCall(AppConstants.SUB_DRESS_TYPE_ID,mUserDetailsEntityRes.getUSER_ID(),AddMeasurementScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(AddMeasurementScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getManuallyApiCall();
                }
            });
        }
    }

    @OnClick({R.id.header_left_side_img,R.id.add_new_measurement_par_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.add_new_measurement_par_lay:
                if (AppConstants.NEW_ORDER.equalsIgnoreCase("NEW_ORDER")){
//                    AppConstants.ADD_NEW_MEASUREMENT = "ADD_NEW_MEASUREMENT";
//                    nextScreen(AddReferenceScreen.class,true);
                    EditTextDialog();
                }else {
                    AppConstants.ADD_NEW_MEASUREMENT = "ADD_NEW_MEASUREMENT";
                    nextScreen(GenderScreen.class,true);
                }

                break;
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof AddMeasurementResponse){
            AddMeasurementResponse mResponse = (AddMeasurementResponse)resObj;

            setAdapter(mResponse.getResult());
        }
        if (resObj instanceof ManuallyResponse){
            ManuallyResponse mREsponse = (ManuallyResponse)resObj;

            mManullyEntity =mREsponse.getResult();
            setManullyAdapter(mREsponse.getResult());

        }
    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<AddMeasurementEntity> measurementListEntities) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mAddNewMeasurementListparLay.setVisibility(measurementListEntities.size()>0 ? View.VISIBLE : View.GONE);
        mEmptyListLay.setVisibility(measurementListEntities.size() > 0 ? View.GONE : View.VISIBLE);

        mMeasurementListAdapter = new AddMeasurementListAdapter(this,measurementListEntities);
        mAddNewMeasurementRecyclerView.setLayoutManager(layoutManager);
        mAddNewMeasurementRecyclerView.setAdapter(mMeasurementListAdapter);

        mAddNewMeasurementRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = layoutManager.findLastVisibleItemPosition();

                if (lastPosition == measurementListEntities.size()-1){
                    mAddMeasurementScrollViewImg.setVisibility(View.GONE);
                }else {
                    mAddMeasurementScrollViewImg.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    /*Set Adapter for the Recycler view*/
    public void setManullyAdapter(ArrayList<ManuallyEntity> measurementListEntities) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mAddNewMeasurementListparLay.setVisibility(measurementListEntities.size()>0 ? View.VISIBLE : View.GONE);
            mEmptyListLay.setVisibility(measurementListEntities.size() > 0 ? View.GONE : View.VISIBLE);

            mMmanuallyAdapter = new ManuallyAdapter(this,measurementListEntities);
            mAddNewMeasurementRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mAddNewMeasurementRecyclerView.setAdapter(mMmanuallyAdapter);

        mAddNewMeasurementRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = layoutManager.findLastVisibleItemPosition();

                if (lastPosition == measurementListEntities.size()-1){
                    mAddMeasurementScrollViewImg.setVisibility(View.GONE);
                }else {
                    mAddMeasurementScrollViewImg.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void EditTextDialog(){
        alertDismiss(mEditTxtDialog);
        mEditTxtDialog = getDialog(AddMeasurementScreen.this, R.layout.pop_up_edit_txt);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mEditTxtDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mEditTxtDialog.findViewById(R.id.pop_up_measurement_manually_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(mEditTxtDialog.findViewById(R.id.pop_up_measurement_manually_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
        TextView cancelTxt , okTxt,mHeaderTxt;
        final EditText manuallyEdtTxt;

        /*Init view*/

        cancelTxt = mEditTxtDialog.findViewById(R.id.pop_up_cancel_txt);
        okTxt = mEditTxtDialog.findViewById(R.id.pop_up_ok_txt);
        manuallyEdtTxt = mEditTxtDialog.findViewById(R.id.measurement_manually_edt_txt);
        mHeaderTxt = mEditTxtDialog.findViewById(R.id.popup_measurement_name_manually_txt);

        mHeaderTxt.setText(getResources().getString(R.string.give_you_measurement_name)+"\n"+AppConstants.DRESS_SUB_TYPE_NAME);

        /*Set data*/
        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditTxtDialog.dismiss();
            }
        });
        okTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!manuallyEdtTxt.getText().toString().trim().equalsIgnoreCase("")){
                    mManuallyStr = "false";

                    for (int i=0; i<mManullyEntity.size(); i++){
                        String[] parts = mManullyEntity.get(i).getName().split("-");
                        String part1 = parts[0]; // 004
                        String part2 = parts[1];
                        String lastOne = parts[parts.length - 1];

                        if (part2.trim().toLowerCase().equalsIgnoreCase(manuallyEdtTxt.getText().toString().trim().toLowerCase())){
                            mManuallyStr = "true";
                        }
                    }
                    if (mManuallyStr.equalsIgnoreCase("true")){
                        manuallyEdtTxt.clearAnimation();
                        manuallyEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                        manuallyEdtTxt.setError(getResources().getString(R.string.measurement_name_already_exist));
                    }else if (mManuallyStr.equalsIgnoreCase("false")){
                        mEditTxtDialog.dismiss();
                        AppConstants.MEASUREMENT_MANUALLY = manuallyEdtTxt.getText().toString();
                        AppConstants.MEASUREMENT_ID = "-1";
                        nextScreen(MeasurementTwoScreen.class,true);
                    }
                }else {
                    manuallyEdtTxt.clearAnimation();
                    manuallyEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    manuallyEdtTxt.setError(getResources().getString(R.string.please_fill_measurement_name));
                }
            }
        });

        alertShowing(mEditTxtDialog);
    }


    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.add_measurement_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.add_measurement_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!AppConstants.NEW_ORDER.equalsIgnoreCase("NEW_ORDER")){
            previousScreen(BottomNavigationScreen.class,true);
        }
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
    }
