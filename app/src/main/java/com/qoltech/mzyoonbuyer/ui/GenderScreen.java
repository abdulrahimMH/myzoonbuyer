package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.GenderAdapter;
import com.qoltech.mzyoonbuyer.entity.GenderEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GenderResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GenderScreen extends BaseActivity {

    private GenderAdapter mGenderAdapter;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.gender_par_lay)
    LinearLayout mGenderParLay;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    private ArrayList<GenderEntity> mGenderList;

    @BindView(R.id.gender_recycler_view)
    RecyclerView mGenderRecyclerView;

    @BindView(R.id.gender_wizard_lay)
    RelativeLayout mGenderWizardLay;

    @BindView(R.id.gender_measurement_wizard_lay)
    RelativeLayout mGenderMeasurementWizardLay;

    private UserDetailsEntity mUserDetailsEntityRes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_gender_screen);

        initView();

    }

    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setupUI(mGenderParLay);

        setHeader();

        getGenderApiCall();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(GenderScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

    }


    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.gender_selection));
        mRightSideImg.setVisibility(View.INVISIBLE);

        if (AppConstants.ADD_NEW_MEASUREMENT.equalsIgnoreCase("ADD_NEW_MEASUREMENT")){
            mGenderWizardLay.setVisibility(View.GONE);
            mGenderMeasurementWizardLay.setVisibility(View.VISIBLE);
        }else {
            mGenderWizardLay.setVisibility(View.VISIBLE);
            mGenderMeasurementWizardLay.setVisibility(View.GONE);

        }
    }

    public void getGenderApiCall(){

        if (NetworkUtil.isNetworkAvailable(GenderScreen.this)){
            APIRequestHandler.getInstance().getGenderAPICall(GenderScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(GenderScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getGenderApiCall();
                }
            });
        }

    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<GenderEntity> genderList) {

        if (mGenderAdapter == null) {

            mGenderAdapter = new GenderAdapter(this,genderList);
            mGenderRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            mGenderRecyclerView.setAdapter(mGenderAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mGenderAdapter.notifyDataSetChanged();
                }
            });
        }

    }

    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GenderResponse){
            GenderResponse mResponse = (GenderResponse) resObj;

            mGenderList = new ArrayList<>();
            mGenderList.addAll(mResponse.getResult());

            setAdapter(mGenderList);
        }
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.gender_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.gender_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        previousScreen(BottomNavigationScreen.class,true);
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
