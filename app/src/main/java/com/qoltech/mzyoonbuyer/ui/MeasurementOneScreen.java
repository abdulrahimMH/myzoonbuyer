package com.qoltech.mzyoonbuyer.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.ManuallyAdapter;
import com.qoltech.mzyoonbuyer.entity.ManuallyEntity;
import com.qoltech.mzyoonbuyer.entity.MeasurementOneEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.ManuallyResponse;
import com.qoltech.mzyoonbuyer.modal.MeasurementOneResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class MeasurementOneScreen extends BaseActivity {

    @BindView(R.id.measurement_one_par_lay)
    LinearLayout mMeasurementOneParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.measurement_manully_img)
    ImageView mMeasurementManuallyImg;

    @BindView(R.id.measurement_tailor_img)
    ImageView mMeasurementTailorImg;

    @BindView(R.id.measurement_tailor_come_img)
    ImageView mMeasurementTailorComeImg;

    @BindView(R.id.measurement_manully_txt)
    TextView mMeasurementManulllyTxt;

    @BindView(R.id.measurement_tailor_txt)
    TextView mMeasurementTailorTxt;

    @BindView(R.id.measurement_tailor_come_txt)
    TextView mMeasurementTailorComeTxt;

    private Dialog mManuallyDialog , mEditTxtDialog;

    private ManuallyAdapter mManuallyAdapter;

    private ArrayList<ManuallyEntity> mManualEntity;

    private UserDetailsEntity mUserDetailsEntityRes;

    ArrayList<MeasurementOneEntity> mMeasurementList;

    @BindView(R.id.measurement_manully_lay)
            LinearLayout mMeasurementManuallyLay;

    @BindView(R.id.measurement_to_tailor_lay)
            LinearLayout mMeasurementTailorLay;

    @BindView(R.id.measurement_tailor_come_to_place_lay)
            LinearLayout mMeasurementTailorComeLay;

    @BindView(R.id.new_flow_measurement_wizard_lay)
    RelativeLayout mNewFlowMeasurementWizLay;

    @BindView(R.id.measurement_wizard_lay)
    RelativeLayout mMeasurementWizLay;
    
    @BindView(R.id.measurement_manully_empty_lay)
    TextView mMeasuremetnManullyEmptyLay;

    @BindView(R.id.measurement_tailor_empty_lay)
    TextView mMeasurementTailorEmptyLay;
    
    @BindView(R.id.measurement_tailor_come_empty_lay)
            TextView mMeasurementTailorComeEmptyLay;

    Dialog mHintDialog;

    int mCountInt = 1;

    String mManuallyStr = "false";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_measurement_one_screen);

        initView();
    }
    public void initView(){

        ButterKnife.bind(this);

        setupUI(mMeasurementOneParLay);

        setHeader();

        mManualEntity = new ArrayList<>();
        mUserDetailsEntityRes = new UserDetailsEntity();
        mMeasurementList = new ArrayList<>();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(getContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

//        getManuallyApiCall();

        getLanguage();

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
            getNewFlowMeasuementApiCall();
        }else {
            getMeasuementApiCall();

        }
        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            mMeasurementWizLay.setVisibility(View.GONE);
            mNewFlowMeasurementWizLay.setVisibility(View.VISIBLE);
        }else {
            mMeasurementWizLay.setVisibility(View.VISIBLE);
            mNewFlowMeasurementWizLay.setVisibility(View.GONE);
        }
        }

    @OnClick({R.id.measurement_manully_lay,R.id.measurement_to_tailor_lay,R.id.measurement_tailor_come_to_place_lay,R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.measurement_manully_lay:
                if (mMeasurementList.get(0).isStatus()) {
                    AppConstants.DIRECT_MEASUREMENT_TYPE = mMeasurementList.get(0).getMeasurementInEnglish();
                    AppConstants.MEASUREMENT_TYPE = String.valueOf(mMeasurementList.get(0).getId());
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        AppConstants.MEASUREMENT_ONE = String.valueOf(mMeasurementList.get(0).getMeasurementInArabic());
                    } else {
                        AppConstants.MEASUREMENT_ONE = String.valueOf(mMeasurementList.get(0).getMeasurementInEnglish());

                    }
                    if (mMeasurementList.get(0).getMeasurementInEnglish().equalsIgnoreCase("Manually")) {
//                        manuallyDialogCall();
                        nextScreen(AddMeasurementScreen.class,true);
                    } else {

                        nextScreen(AddReferenceScreen.class, true);
                    }
                }
                break;
            case R.id.measurement_to_tailor_lay:
                if (mMeasurementList.get(1).isStatus()) {
                    AppConstants.MEASUREMENT_TYPE = String.valueOf(mMeasurementList.get(1).getId());
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        AppConstants.MEASUREMENT_ONE = String.valueOf(mMeasurementList.get(1).getMeasurementInArabic());
                    } else {
                        AppConstants.MEASUREMENT_ONE = String.valueOf(mMeasurementList.get(1).getMeasurementInEnglish());

                    }
                    AppConstants.DIRECT_MEASUREMENT_TYPE = mMeasurementList.get(1).getMeasurementInEnglish();
                    if (mMeasurementList.get(1).getMeasurementInEnglish().equalsIgnoreCase("Manually")) {
//                        manuallyDialogCall();
                        nextScreen(AddMeasurementScreen.class,true);
                    } else {

                        nextScreen(AddReferenceScreen.class, true);
                    }
                }
                break;
            case R.id.measurement_tailor_come_to_place_lay:
                if (mMeasurementList.get(2).isStatus()) {
                    AppConstants.MEASUREMENT_TYPE = String.valueOf(mMeasurementList.get(2).getId());
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        AppConstants.MEASUREMENT_ONE = String.valueOf(mMeasurementList.get(2).getMeasurementInArabic());
                    } else {
                        AppConstants.MEASUREMENT_ONE = String.valueOf(mMeasurementList.get(2).getMeasurementInEnglish());

                    }
                    AppConstants.DIRECT_MEASUREMENT_TYPE = mMeasurementList.get(2).getMeasurementInEnglish();
                    if (mMeasurementList.get(2).getMeasurementInEnglish().equalsIgnoreCase("Manually")) {
//                        manuallyDialogCall();
                        nextScreen(AddMeasurementScreen.class,true);
                    } else {

                        nextScreen(AddReferenceScreen.class, true);
                    }
                }
                break;
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    public void getNewFlowMeasuementApiCall(){

        if (NetworkUtil.isNetworkAvailable(MeasurementOneScreen.this)){
            APIRequestHandler.getInstance().getNewFlowMeasurementTypeApiCall(String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId()),MeasurementOneScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(MeasurementOneScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getMeasuementApiCall();
                }
            });
        }

    }

    public void getMeasuementApiCall(){

        if (NetworkUtil.isNetworkAvailable(MeasurementOneScreen.this)){
            APIRequestHandler.getInstance().getMeasurementOneAPICall(MeasurementOneScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(MeasurementOneScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getMeasuementApiCall();
                }
            });
        }

    }

    public void getManuallyApiCall(){
        if (NetworkUtil.isNetworkAvailable(MeasurementOneScreen.this)){
            APIRequestHandler.getInstance().manuallyApiCall(AppConstants.SUB_DRESS_TYPE_ID,mUserDetailsEntityRes.getUSER_ID(),MeasurementOneScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(MeasurementOneScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getManuallyApiCall();
                }
            });
        }
    }

    public void manuallyDialogCall(){
        AppConstants.MEASUREMENT_TYPE = String.valueOf(mMeasurementList.get(0).getId());
        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic"))  {
            AppConstants.MEASUREMENT_ONE = String.valueOf(mMeasurementList.get(0).getMeasurementInArabic());
        }else {
            AppConstants.MEASUREMENT_ONE = String.valueOf(mMeasurementList.get(0).getMeasurementInEnglish());

        }
        alertDismiss(mManuallyDialog);
        mManuallyDialog = getDialog(MeasurementOneScreen.this, R.layout.pop_up_manually);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mManuallyDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mManuallyDialog.findViewById(R.id.pop_up_manaully_list_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(mManuallyDialog.findViewById(R.id.pop_up_manaully_list_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
        TextView cancelTxt,addTxt ;

        RecyclerView countryRecyclerView;

        LinearLayout emptyLay;

        /*Init view*/
        emptyLay = mManuallyDialog.findViewById(R.id.manually_empty_cancel_add_lay);
        cancelTxt = mManuallyDialog.findViewById(R.id.pop_up_cancel_txt);
        addTxt = mManuallyDialog.findViewById(R.id.pop_up_new_txt);

        emptyLay.setVisibility(View.VISIBLE);

        countryRecyclerView = mManuallyDialog.findViewById(R.id.pop_up_manually_recycler_view);
//        mManuallyAdapter = new ManuallyAdapter(MeasurementOneScreen.this, mManualEntity,mManuallyDialog);

        countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        countryRecyclerView.setAdapter(mManuallyAdapter);

        /*Set data*/
        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mManuallyDialog.dismiss();
            }
        });


        addTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mManuallyDialog.dismiss();
                EditTextDialog();
            }
        });

        alertShowing(mManuallyDialog);
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.measurement_type));
        mRightSideImg.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof MeasurementOneResponse){
            MeasurementOneResponse mResponse = (MeasurementOneResponse)resObj;

            mMeasurementList = mResponse.getResult();
            if (!AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                for (int i=0 ;i < mMeasurementList.size(); i++){
                    mMeasurementList.get(i).setStatus(true);
                }
            }
            setImagesAndTitle(mMeasurementList);
        }
        if (resObj instanceof ManuallyResponse){
            ManuallyResponse mResponse = (ManuallyResponse)resObj;
            mManualEntity = mResponse.getResult();
        }
    }

    public void setImagesAndTitle(ArrayList<MeasurementOneEntity> MeasurementList){

        mMeasurementManuallyLay.setVisibility(View.VISIBLE);
        mMeasurementTailorLay.setVisibility(View.VISIBLE);
        mMeasurementTailorComeLay.setVisibility(View.VISIBLE);
        mMeasurementManuallyImg.setVisibility(View.VISIBLE);
        mMeasurementTailorImg.setVisibility(View.VISIBLE);
        mMeasurementTailorComeImg.setVisibility(View.VISIBLE);

        for (int i=0; i<MeasurementList.size(); i++){
            if (!MeasurementList.get(i).isStatus()){
                if (i == 0){
                    mMeasuremetnManullyEmptyLay.setVisibility(View.VISIBLE);
                    mMeasurementManuallyImg.setAlpha(30);

                }else if (i == 1){
                    mMeasurementTailorEmptyLay.setVisibility(View.VISIBLE);
                    mMeasurementTailorImg.setAlpha(30);

                }else if (i==2){
                    mMeasurementTailorComeEmptyLay.setVisibility(View.VISIBLE);
                    mMeasurementTailorComeImg.setAlpha(30);

                }
            }else {
                if (i == 0){
                    mMeasuremetnManullyEmptyLay.setVisibility(View.GONE);
                }else if (i == 1){
                    mMeasurementTailorEmptyLay.setVisibility(View.GONE);
                }else if (i==2){
                    mMeasurementTailorComeEmptyLay.setVisibility(View.GONE);
                }
            }
        }

            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                for (int i=0; i<MeasurementList.size(); i++){
                    if (i==0){
                        mMeasurementManulllyTxt.setText(MeasurementList.get(0).getMeasurementInArabic());

                    }else if (i==1){
                        mMeasurementTailorTxt.setText(MeasurementList.get(1).getMeasurementInArabic());

                    }else if (i==2){
                        mMeasurementTailorComeTxt.setText(MeasurementList.get(2).getMeasurementInArabic());

                    }
                }
            }else {
                for (int i = 0; i < MeasurementList.size(); i++) {
                    if (i == 0) {
                        mMeasurementManulllyTxt.setText(MeasurementList.get(0).getMeasurementInEnglish());

                    } else if (i == 1) {
                        mMeasurementTailorTxt.setText(MeasurementList.get(1).getMeasurementInEnglish());

                    } else if (i == 2) {
                        mMeasurementTailorComeTxt.setText(MeasurementList.get(2).getMeasurementInEnglish());

                    }
                }
            }
                for (int i = 0; i < MeasurementList.size(); i++) {

                    if (i == 0) {
                        try {
                            Glide.with(MeasurementOneScreen.this)
                                    .load(AppConstants.IMAGE_BASE_URL + "Images/Measurement1/" + MeasurementList.get(0).getBodyImage())
                                    .apply(new RequestOptions().placeholder(R.drawable.gender_background_img).error(R.drawable.gender_background_img))
                                    .into(mMeasurementManuallyImg);

                        } catch (Exception ex) {
                            Log.e(AppConstants.TAG, ex.getMessage());
                        }


                    } else if (i == 1) {

                        try {

                            Glide.with(MeasurementOneScreen.this)
                                    .load(Uri.parse(AppConstants.IMAGE_BASE_URL + "Images/Measurement1/" + MeasurementList.get(1).getBodyImage()))
                                    .apply(new RequestOptions().placeholder(R.drawable.gender_background_img).error(R.drawable.gender_background_img))
                                    .into(mMeasurementTailorImg);

                        } catch (Exception ex) {
                            Log.e(AppConstants.TAG, ex.getMessage());
                        }

                    } else if (i == 2) {

                        try {
                            Glide.with(MeasurementOneScreen.this)
                                    .load(AppConstants.IMAGE_BASE_URL + "Images/Measurement1/" + MeasurementList.get(2).getBodyImage())
                                    .apply(new RequestOptions().placeholder(R.drawable.gender_background_img).error(R.drawable.gender_background_img))
                                    .into(mMeasurementTailorComeImg);

                        } catch (Exception ex) {
                            Log.e(AppConstants.TAG, ex.getMessage());
                        }

                    }
                }


            if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")){
                getHintDialog();

            }


    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    public void getHintDialog(){
        alertDismiss(mHintDialog);
        mHintDialog = getDialog(MeasurementOneScreen.this, R.layout.pop_up_measurement_one_hint);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mHintDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.measurement_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.measurement_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        Button mSkipBtn,mBackBtn,mNextBtn;
        ImageView mMeasurementOneImg,mMeasurementTwoImg,mMeasurementThreeImg;
        RelativeLayout mMeasurementOneLay,mMeasurementTwoLay,mMeasurementThreeLay;
        TextView mMeasurementTypeOneTxt,mMeasurementTypeTwoTxt,mMeasurementTypeThreeTxt;

        /*Init view*/
        mSkipBtn = mHintDialog.findViewById(R.id.skip_hint_btn);
        mBackBtn = mHintDialog.findViewById(R.id.back_hint_btn);
        mNextBtn = mHintDialog.findViewById(R.id.next_hint_btn);
        mMeasurementOneImg = mHintDialog.findViewById(R.id.measurement_one_hint_img);
        mMeasurementTwoImg = mHintDialog.findViewById(R.id.measurement_two_hint_img);
        mMeasurementThreeImg = mHintDialog.findViewById(R.id.measurement_three_hint_img);

        mMeasurementTypeOneTxt = mHintDialog.findViewById(R.id.measurement_one_hint_txt);
        mMeasurementTypeTwoTxt = mHintDialog.findViewById(R.id.measurement_two_hint_txt);
        mMeasurementTypeThreeTxt = mHintDialog.findViewById(R.id.measurement_three_hint_txt);

        mMeasurementOneLay = mHintDialog.findViewById(R.id.measurement_one__par_lay);
        mMeasurementTwoLay = mHintDialog.findViewById(R.id.measurement_two__par_lay);
        mMeasurementThreeLay = mHintDialog.findViewById(R.id.measurement_three__par_lay);

        try {
            Glide.with(MeasurementOneScreen.this)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/Measurement1/"+mMeasurementList.get(0).getBodyImage())
                    .apply(new RequestOptions().placeholder(R.drawable.gender_background_img).error(R.drawable.gender_background_img))
                    .into(mMeasurementOneImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        try {
            Glide.with(MeasurementOneScreen.this)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/Measurement1/"+mMeasurementList.get(1).getBodyImage())
                    .apply(new RequestOptions().placeholder(R.drawable.gender_background_img).error(R.drawable.gender_background_img))
                    .into(mMeasurementTwoImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }
        try {
            Glide.with(MeasurementOneScreen.this)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/Measurement1/"+mMeasurementList.get(2).getBodyImage())
                    .apply(new RequestOptions().placeholder(R.drawable.gender_background_img).error(R.drawable.gender_background_img))
                    .into(mMeasurementThreeImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }



        if (mCountInt == 1){
            mBackBtn.setVisibility(View.INVISIBLE);
            mMeasurementTwoLay.setVisibility(View.INVISIBLE);
            mMeasurementThreeLay.setVisibility(View.INVISIBLE);
        }

        if (mMeasurementList.size() == 1) {
            mNextBtn.setText(getResources().getString(R.string.got_it));
            if (mMeasurementList.get(0).getMeasurementInEnglish().equalsIgnoreCase("Manually")){
                mMeasurementTypeOneTxt.setText(getResources().getString(R.string.measurement_manually_hint));
            }else if (mMeasurementList.get(0).getMeasurementInEnglish().equalsIgnoreCase("Tailor Come To Your Place")){
                mMeasurementTypeOneTxt.setText(getResources().getString(R.string.measurement_tailor_come_hint));

            }else if (mMeasurementList.get(0).getMeasurementInEnglish().equalsIgnoreCase("Go to Tailor Shop")){
                mMeasurementTypeOneTxt.setText(getResources().getString(R.string.measurement_go_to_tailor_hint));

            }

        }else if (mMeasurementList.size() == 2){
            if (mMeasurementList.get(0).getMeasurementInEnglish().equalsIgnoreCase("Manually")){
                mMeasurementTypeOneTxt.setText(getResources().getString(R.string.measurement_manually_hint));
            }else if (mMeasurementList.get(0).getMeasurementInEnglish().equalsIgnoreCase("Tailor Come To Your Place")){
                mMeasurementTypeOneTxt.setText(getResources().getString(R.string.measurement_tailor_come_hint));

            }else if (mMeasurementList.get(0).getMeasurementInEnglish().equalsIgnoreCase("Go to Tailor Shop")){
                mMeasurementTypeOneTxt.setText(getResources().getString(R.string.measurement_go_to_tailor_hint));

            }else if (mMeasurementList.get(1).getMeasurementInEnglish().equalsIgnoreCase("Manually")){
                mMeasurementTypeTwoTxt.setText(getResources().getString(R.string.measurement_manually_hint));
            }else if (mMeasurementList.get(1).getMeasurementInEnglish().equalsIgnoreCase("Tailor Come To Your Place")){
                mMeasurementTypeTwoTxt.setText(getResources().getString(R.string.measurement_tailor_come_hint));

            }else if (mMeasurementList.get(1).getMeasurementInEnglish().equalsIgnoreCase("Go to Tailor Shop")){
                mMeasurementTypeTwoTxt.setText(getResources().getString(R.string.measurement_go_to_tailor_hint));

            }
        }else if (mMeasurementList.size() == 3){
            if (mMeasurementList.get(0).getMeasurementInEnglish().equalsIgnoreCase("Manually")){
                mMeasurementTypeOneTxt.setText(getResources().getString(R.string.measurement_manually_hint));
            }else if (mMeasurementList.get(0).getMeasurementInEnglish().equalsIgnoreCase("Tailor Come To Your Place")){
                mMeasurementTypeOneTxt.setText(getResources().getString(R.string.measurement_tailor_come_hint));

            }else if (mMeasurementList.get(0).getMeasurementInEnglish().equalsIgnoreCase("Go to Tailor Shop")){
                mMeasurementTypeOneTxt.setText(getResources().getString(R.string.measurement_go_to_tailor_hint));

            }else if (mMeasurementList.get(1).getMeasurementInEnglish().equalsIgnoreCase("Manually")){
                mMeasurementTypeTwoTxt.setText(getResources().getString(R.string.measurement_manually_hint));
            }else if (mMeasurementList.get(1).getMeasurementInEnglish().equalsIgnoreCase("Tailor Come To Your Place")){
                mMeasurementTypeTwoTxt.setText(getResources().getString(R.string.measurement_tailor_come_hint));

            }else if (mMeasurementList.get(1).getMeasurementInEnglish().equalsIgnoreCase("Go to Tailor Shop")){
                mMeasurementTypeTwoTxt.setText(getResources().getString(R.string.measurement_go_to_tailor_hint));

            }else if (mMeasurementList.get(2).getMeasurementInEnglish().equalsIgnoreCase("Manually")){
                mMeasurementTypeThreeTxt.setText(getResources().getString(R.string.measurement_manually_hint));
            }else if (mMeasurementList.get(2).getMeasurementInEnglish().equalsIgnoreCase("Tailor Come To Your Place")){
                mMeasurementTypeThreeTxt.setText(getResources().getString(R.string.measurement_tailor_come_hint));

            }else if (mMeasurementList.get(2).getMeasurementInEnglish().equalsIgnoreCase("Go to Tailor Shop")){
                mMeasurementTypeThreeTxt.setText(getResources().getString(R.string.measurement_go_to_tailor_hint));

            }
        }

        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountInt = --mCountInt;
                if (mCountInt == 1){
                    mBackBtn.setVisibility(View.INVISIBLE);
                    mMeasurementOneLay.setVisibility(View.VISIBLE);
                    mMeasurementTwoLay.setVisibility(View.INVISIBLE);
                    mMeasurementThreeLay.setVisibility(View.INVISIBLE);
                    if (mMeasurementList.size() == 1){
                        mNextBtn.setText(getResources().getString(R.string.got_it));

                    }else {
                        mNextBtn.setText(getResources().getString(R.string.next));

                    }
                }else if (mCountInt == 2){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mMeasurementOneLay.setVisibility(View.INVISIBLE);
                    mMeasurementTwoLay.setVisibility(View.VISIBLE);
                    mMeasurementThreeLay.setVisibility(View.INVISIBLE);
                   if (mMeasurementList.size() == 2){
                        mNextBtn.setText(getResources().getString(R.string.got_it));

                    }else {
                        mNextBtn.setText(getResources().getString(R.string.next));

                    }
                }else if (mCountInt == 3){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mMeasurementOneLay.setVisibility(View.INVISIBLE);
                    mMeasurementTwoLay.setVisibility(View.INVISIBLE);
                    mMeasurementThreeLay.setVisibility(View.VISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.got_it));
                }
            }
        });

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountInt = ++mCountInt;
                if (mCountInt == 1){
                    mBackBtn.setVisibility(View.INVISIBLE);
                    mMeasurementOneLay.setVisibility(View.VISIBLE);
                    mMeasurementTwoLay.setVisibility(View.INVISIBLE);
                    mMeasurementThreeLay.setVisibility(View.INVISIBLE);
                    if (mMeasurementList.size() == 1){
                        mNextBtn.setText(getResources().getString(R.string.got_it));

                    }else {
                        mNextBtn.setText(getResources().getString(R.string.next));

                    }
                }else if (mCountInt == 2){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mMeasurementOneLay.setVisibility(View.INVISIBLE);
                    mMeasurementTwoLay.setVisibility(View.VISIBLE);
                    mMeasurementThreeLay.setVisibility(View.INVISIBLE);
                    if (mMeasurementList.size() == 1){
                        mHintDialog.dismiss();

                    }else if (mMeasurementList.size() == 2){
                        mNextBtn.setText(getResources().getString(R.string.got_it));

                    }
                    else {
                        mNextBtn.setText(getResources().getString(R.string.next));

                    }
                }else if (mCountInt == 3){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mMeasurementOneLay.setVisibility(View.INVISIBLE);
                    mMeasurementTwoLay.setVisibility(View.INVISIBLE);
                    mMeasurementThreeLay.setVisibility(View.VISIBLE);
                    if (mMeasurementList.size() == 2){
                        mHintDialog.dismiss();
                    }
                    mNextBtn.setText(getResources().getString(R.string.got_it));
                }else if (mCountInt >=4){
                    mHintDialog.dismiss();
                }
            }
        });

        /*Set data*/
        mSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHintDialog.dismiss();
            }
        });



        alertShowing(mHintDialog);
    }

    public void EditTextDialog(){
        alertDismiss(mEditTxtDialog);
        mEditTxtDialog = getDialog(MeasurementOneScreen.this, R.layout.pop_up_edit_txt);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mEditTxtDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mEditTxtDialog.findViewById(R.id.pop_up_measurement_manually_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(mEditTxtDialog.findViewById(R.id.pop_up_measurement_manually_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
        TextView cancelTxt , okTxt,mHeaderTxt;
        final EditText manuallyEdtTxt;

        /*Init view*/

        cancelTxt = mEditTxtDialog.findViewById(R.id.pop_up_cancel_txt);
        okTxt = mEditTxtDialog.findViewById(R.id.pop_up_ok_txt);
        manuallyEdtTxt = mEditTxtDialog.findViewById(R.id.measurement_manually_edt_txt);
        mHeaderTxt = mEditTxtDialog.findViewById(R.id.popup_measurement_name_manually_txt);

        mHeaderTxt.setText(getResources().getString(R.string.give_you_measurement_name)+"\n"+AppConstants.DRESS_SUB_TYPE_NAME);

        /*Set data*/
        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditTxtDialog.dismiss();
            }
        });
        okTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!manuallyEdtTxt.getText().toString().trim().equalsIgnoreCase("")){
                    mManuallyStr = "false";

                    for (int i=0; i<mManualEntity.size(); i++){
                        String[] parts = mManualEntity.get(i).getName().split("-");
                        String part1 = parts[0]; // 004
                        String part2 = parts[1];
                        String lastOne = parts[parts.length - 1];

                        if (part2.trim().toLowerCase().equalsIgnoreCase(manuallyEdtTxt.getText().toString().trim().toLowerCase())){
                            mManuallyStr = "true";
                        }
                    }
                    if (mManuallyStr.equalsIgnoreCase("true")){
                        manuallyEdtTxt.clearAnimation();
                        manuallyEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                        manuallyEdtTxt.setError(getResources().getString(R.string.measurement_name_already_exist));
                    }else if (mManuallyStr.equalsIgnoreCase("false")){
                        mEditTxtDialog.dismiss();
                        AppConstants.MEASUREMENT_MANUALLY = manuallyEdtTxt.getText().toString();
                        AppConstants.MEASUREMENT_ID = "-1";
                        nextScreen(MeasurementTwoScreen.class,true);
                    }
                }else {
                    manuallyEdtTxt.clearAnimation();
                    manuallyEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    manuallyEdtTxt.setError(getResources().getString(R.string.please_fill_measurement_name));
                }
            }
        });

        alertShowing(mEditTxtDialog);
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_one_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
//            updateResources(MeasurementOneScreen.this,"ar");

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_one_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            updateResources(MeasurementOneScreen.this,"en");

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }
}
