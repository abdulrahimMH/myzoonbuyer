package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MeasurementEditTextScreen extends BaseActivity {

    @BindView(R.id.measurement_edt_txt_par_lay)
    RelativeLayout mMeasurementEdtTxtParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.scale_cm_edt_txt)
    EditText mScaleCmEdtTxt;

    @BindView(R.id.measurement_scale_body_img)
    ImageView mMeasurementScalseBodyImg;

    @BindView(R.id.scale_measurement_ok_btn)
    Button mScaleOkBtn;

    @BindView(R.id.scale_measurement_cancel_btn)
    Button mScaleCancelBtn;

    private UserDetailsEntity mUserDetailsEntityRes;

    String mBackHandleStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_measurement_edit_txt_screen);

        initView();
    }
    public void initView() {

        ButterKnife.bind(this);

        setupUI(mMeasurementEdtTxtParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(MeasurementEditTextScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        setHeader();

    }


    @OnClick({R.id.header_left_side_img,R.id.scale_measurement_ok_btn,R.id.scale_measurement_cancel_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
//                mBackHandleStr = "true";
//                DialogManager.getInstance().showOptionPopup(MeasurementEditTextScreen.this, getResources().getString(R.string.sure_want_to_go_back), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
//                    @Override
//                    public void onNegativeClick() {
//                        mBackHandleStr = "false";
//
//                    }
//                    @Override
//                    public void onPositiveClick() {
                        onBackPressed();
//                    }
//                });
                break;
            case R.id.scale_measurement_ok_btn:
                if (isEmailValid(mScaleCmEdtTxt.getText().toString().trim())){
                    NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                    DecimalFormat formatter = (DecimalFormat)nf;
                    formatter.applyPattern("######.#");

                    Float values = Float.parseFloat(mScaleCmEdtTxt.getText().toString().trim());
                    Float multiplyValue = Float.parseFloat("2.5");
                    if (AppConstants.SelectedPartsId.equalsIgnoreCase("1")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_1 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_1_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_1 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_1_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("2")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_2 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_2_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_2 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_2_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("3")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_3 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_3_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_3 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_3_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("4")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_4 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_4_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_4 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_4_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("5")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_5 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_5_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_5 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_5_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("7")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_7 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_7_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_7 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_7_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("6")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_6 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_6_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_6 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_6_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("8")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_8 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_8_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_8 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_8_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("9")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_9 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_9_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_9 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_9_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("10")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_10 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_10_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_10 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_10_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("11")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_11 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_11_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_11 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_11_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("12")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_12 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_12_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_12 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_12_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("13")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_13 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_13_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_13 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_13_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("14")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_14 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_14_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_14 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_14_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("15")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_15 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_15_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_15 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_15_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("16")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_16 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_16_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_16 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_16_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("17")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_17 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_17_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_17 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_17_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("18")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_18 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_18_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_18 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_18_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("19")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_19 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_19_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_19 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_19_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("20")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_20 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_20_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_20 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_20_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("21")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_21 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_21_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_21 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_21_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("22")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_22 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_22_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_22 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_22_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("23")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_23 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_23_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_23 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_23_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("24")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_24 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_24_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_24 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_24_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("25")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_25 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_25_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_25 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_25_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("26")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_26 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_26_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_26 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_26_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("27")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_27 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_27_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_27 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_27_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("28")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_28 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_28_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_28 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_28_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("29")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_29 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_29_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_29 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_29_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("30")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_30 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_30_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_30 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_30_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("31")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_31 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_31_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_31 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_31_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("32")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_32 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_32_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_32 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_32_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("33")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_33 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_33_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_33 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_33_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("34")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_34 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_34_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_34 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_34_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("35")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_35 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_35_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_35 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_35_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("36")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_36 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_36_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_36 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_36_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("37")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_37 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_37_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_37 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_37_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("38")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_38 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_38_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_38 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_38_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("39")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_39 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_39_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_39 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_39_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("40")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_40 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_40_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_40 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_40_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("41")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_41 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_41_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_41 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_41_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("42")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_42 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_42_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_42 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_42_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("43")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_43 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_43_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_43 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_43_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("44")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_44 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_44_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_44 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_44_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("45")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_45 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_45_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_45 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_45_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("46")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_46 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_46_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_46 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_46_INCHES = mScaleCmEdtTxt.getText().toString().trim();

                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("47")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_47 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_47_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_47 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_47_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("48")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_48 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_48_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_48 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_48_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("49")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_49 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_49_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_49 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_49_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("50")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_50 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_50_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_50 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_50_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("51")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_51 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_51_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_51 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_51_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("52")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_52 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_52_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_52 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_52_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("53")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_53 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_53_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_53 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_53_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("54")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_54 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_54_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_54 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_54_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("55")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_55 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_55_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_55 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_55_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("56")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_56 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_56_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_56 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_56_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("57")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_57 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_57_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_57 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_57_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("58")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_58 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_58_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_58 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_58_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("59")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_59 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_59_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_59 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_59_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("60")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_60 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_60_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_60 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_60_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("61")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_61 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_61_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_61 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_61_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("62")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_62 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_62_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_62 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_62_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("63")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_63 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_63_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_63 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_63_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("64")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.MEN_64 = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.MEN_64_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.MEN_64 = String.valueOf(formatter.format(values * multiplyValue));
                            AppConstants.MEN_64_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }

                    AppConstants.SelectedPartsEditValue = "";
                    mBackHandleStr = "true";
                    onBackPressed();
                }else {
                    mScaleCmEdtTxt.clearAnimation();
                    mScaleCmEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        mScaleCmEdtTxt.setError("يرجى إعطاء سم مناسب أو IN (EX: 00.0)");
                    }else {
                        mScaleCmEdtTxt.setError(getResources().getString(R.string.please_give_proper_cm_or_in));
                    }
                }

                break;
            case R.id.scale_measurement_cancel_btn:
//                mBackHandleStr = "true";
//
//                DialogManager.getInstance().showOptionPopup(MeasurementEditTextScreen.this, getResources().getString(R.string.sure_want_to_cancel), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
//                    @Override
//                    public void onNegativeClick() {
//                        mBackHandleStr = "false";
//
//                    }
//
//                    @Override
//                    public void onPositiveClick() {
                        onBackPressed();
//                    }
//                });
                break;
        }

    }

    public static boolean isEmailValid(String measurement) {
        String expression = "([0-9]{1,3}+\\.)+[0-9]{1}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(measurement);
        return matcher.matches();
    }


    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(AppConstants.SelectedPartsName);
        mRightSideImg.setVisibility(View.INVISIBLE);

        mBackHandleStr = "false";

        mScaleCmEdtTxt.setText( AppConstants.SelectedPartsEditValue);
        try {
            Glide.with(MeasurementEditTextScreen.this)
                    .load(AppConstants.IMAGE_BASE_URL+"images/Measurement2/"+AppConstants.SelectedPartsImage)
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(mMeasurementScalseBodyImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            mScaleOkBtn.setText("حفظ");
            mScaleCancelBtn.setText("إلغاء");
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);
        AppConstants.SelectedPartsEditValue = "";
//        onBackPressed();


    }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
}