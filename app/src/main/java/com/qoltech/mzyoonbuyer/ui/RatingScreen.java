package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.MaterialRatingAdapter;
import com.qoltech.mzyoonbuyer.adapter.RatingAdapter;
import com.qoltech.mzyoonbuyer.entity.CustomerRatingEntity;
import com.qoltech.mzyoonbuyer.entity.MaterialGetReviewDetailsEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetRatingResponse;
import com.qoltech.mzyoonbuyer.modal.MaterialReviewModal;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RatingScreen extends BaseActivity {

    @BindView(R.id.rating_screen_par_lay)
    LinearLayout mRatingScreenParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    private UserDetailsEntity mUserDetailsEntity;

    @BindView(R.id.rating_total_rating_txt)
    TextView mRatingTotalRatingTxt;

    @BindView(R.id.rating_total_based_txt)
    TextView mRatingTotalBasedTxt;

    @BindView(R.id.tailor_service_rating_bar)
    RatingBar mRatingServiceRatingBar;

    @BindView(R.id.tailor_stiching_rating_bar)
    RatingBar mRatingStichingRatingBar;

    @BindView(R.id.tailor_customer_rating_bar)
    RatingBar mRatingCustomerRatingBar;

    @BindView(R.id.all_service_rating_bar)
    RatingBar mAllServiceRatingBar;

    @BindView(R.id.rating_total_rating_review_txt)
    TextView mRatingTotalRatingReviewTxt;

    @BindView(R.id.rating_service_review_par_lay)
    LinearLayout mRatingServiceReviewParLay;

    RatingAdapter mRatingAdapter;

    MaterialRatingAdapter mMaterialRatingAdapter;

    @BindView(R.id.rating_star_recycler_view)
    RecyclerView mRatingStarRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_rating_screen);
        initView();
    }
    public void initView(){
        ButterKnife.bind(this);

        setupUI(mRatingScreenParLay);

        setHeader();

        getLanguage();

        if (AppConstants.MATERIAL_REVIEW.equalsIgnoreCase("true")){
            mRatingServiceReviewParLay.setVisibility(View.GONE);
            getGetMaterialReivewApiCall();
        }else if (AppConstants.MATERIAL_REVIEW.equalsIgnoreCase("false")){
            mRatingServiceReviewParLay.setVisibility(View.GONE);
            getStoreReviewApiCall();
        }
        else {
            getGetRatingApiCall();
        }
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        if (AppConstants.MATERIAL_REVIEW.equalsIgnoreCase("true")){
            mHeaderTxt.setText(AppConstants.PATTERN_NAME);
        }else if (AppConstants.MATERIAL_REVIEW.equalsIgnoreCase("false")) {
            mHeaderTxt.setText(AppConstants.STORE_ORDER_DETAILS_PRODUCT_NAME);
        }
        else {
            mHeaderTxt.setText(AppConstants.TAILOR_CLICK_NAME);

        }
        mRightSideImg.setVisibility(View.INVISIBLE);

    }
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }



    public void getGetRatingApiCall(){
        if (NetworkUtil.isNetworkAvailable(RatingScreen.this)){
            APIRequestHandler.getInstance().getRatingApiCall(String.valueOf(AppConstants.TAILOR_CLICK_ID),"0","Tailor",RatingScreen.this);
        }else {
            getGetRatingApiCall();
        }
    }

    public void getGetMaterialReivewApiCall(){
        if (NetworkUtil.isNetworkAvailable(RatingScreen.this)){
            APIRequestHandler.getInstance().getMaterialReview(AppConstants.PATTERN_ID,RatingScreen.this);
        } else {
            getGetMaterialReivewApiCall();
        }
    }

    public void getStoreReviewApiCall(){
        if (NetworkUtil.isNetworkAvailable(RatingScreen.this)){
            APIRequestHandler.getInstance().getStoreReview(AppConstants.STORE_PRODUCT_ID,RatingScreen.this);
        }else {
            getStoreReviewApiCall();
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetRatingResponse){
            GetRatingResponse mResponse= (GetRatingResponse)resObj;
            if (mResponse.getResult().getCustomerRating().size()>0){
                setAdapter(mResponse.getResult().getCustomerRating());
            }
            if (mResponse.getResult().getRatings().size()>0){
                NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                DecimalFormat formatter = (DecimalFormat)nf;
                formatter.applyPattern("######.#");
                for (int i=0; i<mResponse.getResult().getRatings().size(); i++){
                    if (i == 0){
                        mRatingServiceRatingBar.setRating(Float.parseFloat(String.valueOf(mResponse.getResult().getRatings().get(0).getRating())));
                    }
                    else if (i == 1){
                        mRatingStichingRatingBar.setRating(Float.parseFloat(String.valueOf(mResponse.getResult().getRatings().get(1).getRating())));
                    }
                    else if (i == 2){
                        mRatingCustomerRatingBar.setRating(Float.parseFloat(String.valueOf(mResponse.getResult().getRatings().get(2).getRating())));
                    }
                }
            }
            if (mResponse.getResult().getRatingStatus().size()>0){
                mRatingTotalBasedTxt.setText(getResources().getString(R.string.basec_on_review).replace("0",String.valueOf(mResponse.getResult().getRatingStatus().get(0).getReviewCount())));

                mRatingTotalRatingReviewTxt.setText(getResources().getString(R.string.reviews)+"(" + mResponse.getResult().getRatingStatus().get(0).getReviewCount()+")");
            }
            if (mResponse.getResult().getFullStatus().size()>0){
                NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                DecimalFormat formatter = (DecimalFormat)nf;
                formatter.applyPattern("######.#");
                mRatingTotalRatingTxt.setText(String.valueOf(Double.valueOf(formatter.format(mResponse.getResult().getFullStatus().get(0).getFullStatus()))));

                mAllServiceRatingBar.setRating(Float.parseFloat(String.valueOf(mResponse.getResult().getFullStatus().get(0).getFullStatus())));

            }
        }
        if (resObj instanceof MaterialReviewModal){
            MaterialReviewModal mResponses = (MaterialReviewModal)resObj;

            if (mResponses.getResult().getReviewCount().size() > 0){
                mRatingTotalBasedTxt.setText(getResources().getString(R.string.basec_on_review).replace("0",String.valueOf(mResponses.getResult().getReviewCount().get(0).getReviewCount())));
                mRatingTotalRatingReviewTxt.setText(getResources().getString(R.string.reviews)+"(" + mResponses.getResult().getReviewCount().get(0).getReviewCount()+")");

            }
            if (mResponses.getResult().getAverageRating().size() > 0){
                NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                DecimalFormat formatter = (DecimalFormat)nf;
                formatter.applyPattern("######.#");
                mRatingTotalRatingTxt.setText(String.valueOf(Double.valueOf(formatter.format(mResponses.getResult().getAverageRating().get(0).getAverageRating()))));

                mAllServiceRatingBar.setRating(Float.parseFloat(String.valueOf(mResponses.getResult().getAverageRating().get(0).getAverageRating())));

            }
            setMaterialAdapter(mResponses.getResult().getGetReviewDetails());
        }
    }

    public void getLanguage(){
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(RatingScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntity = gson.fromJson(json, UserDetailsEntity.class);

        if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.rating_screen_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.rating_screen_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
        }
    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<CustomerRatingEntity> customerListeEntities) {
            mRatingAdapter = new RatingAdapter(this,customerListeEntities);
            mRatingStarRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mRatingStarRecyclerView.setAdapter(mRatingAdapter);

    }


    /*Set Adapter for the Recycler view*/
    public void setMaterialAdapter(ArrayList<MaterialGetReviewDetailsEntity> customerListeEntities) {
        mMaterialRatingAdapter = new MaterialRatingAdapter(this,customerListeEntities);
        mRatingStarRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRatingStarRecyclerView.setAdapter(mMaterialRatingAdapter);

    }
    @Override
    protected void onResume() {
        super.onResume();
        if (AppConstants.MATERIAL_REVIEW.equalsIgnoreCase("true")){
            mRatingServiceReviewParLay.setVisibility(View.GONE);
            getGetMaterialReivewApiCall();
        }else if (AppConstants.MATERIAL_REVIEW.equalsIgnoreCase("false")){
            mRatingServiceReviewParLay.setVisibility(View.GONE);
            getStoreReviewApiCall();
        }
        else {
            getGetRatingApiCall();

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
