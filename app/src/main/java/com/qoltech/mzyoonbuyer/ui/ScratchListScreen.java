package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.ScratchCardAdapter;
import com.qoltech.mzyoonbuyer.entity.GetScrachCardEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetScrachCardResponse;
import com.qoltech.mzyoonbuyer.modal.UpdateRewardsPointsResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class ScratchListScreen extends BaseActivity {

    @BindView(R.id.scratch_card_list_par_lay)
    RelativeLayout mScratchCardListParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.scratch_card_empty_txt)
    TextView mEmptyListTxt;

    @BindView(R.id.scratch_card_recycler_view)
    RecyclerView mScratchCardRecyclerView;

    private UserDetailsEntity mUserDetailsEntityRes;

    ScratchCardAdapter mAdapter;

    ArrayList<GetScrachCardEntity> mGetScratchCardList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_scratch_list_screen);
    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mScratchCardListParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        setHeader();

        getLanguage();

        getScrachCardApiCall(mUserDetailsEntityRes.getUSER_ID());

    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.scratch_card));
        mRightSideImg.setVisibility(View.INVISIBLE);
        mEmptyListTxt.setText(getResources().getString(R.string.no_scratch_card));
    }

    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }
    }

    public void getScrachCardApiCall(String UserId) {
        if (NetworkUtil.isNetworkAvailable(getContext())) {
            APIRequestHandler.getInstance().getScrachCardApiCall(UserId, ScratchListScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(getContext(), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getScrachCardApiCall(UserId);
                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);

        if (resObj instanceof GetScrachCardResponse) {
            GetScrachCardResponse mREsponse = (GetScrachCardResponse) resObj;
            mGetScratchCardList = new ArrayList<>();
            mGetScratchCardList = mREsponse.getResult();
            setAdapter(mGetScratchCardList);
        }
        if (resObj instanceof UpdateRewardsPointsResponse) {
            UpdateRewardsPointsResponse mREsponse = (UpdateRewardsPointsResponse) resObj;
            if (mREsponse.ResponseMsg.equalsIgnoreCase("Success")) {

            }
        }
    }


    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<GetScrachCardEntity> getScrachCardEntities) {

        if (getScrachCardEntities.size() > 0) {
            mScratchCardRecyclerView.setVisibility(View.VISIBLE);
            mEmptyListTxt.setVisibility(View.GONE);

        } else {
            mScratchCardRecyclerView.setVisibility(View.GONE);
            mEmptyListTxt.setVisibility(View.VISIBLE);
        }

        mAdapter = new ScratchCardAdapter(ScratchListScreen.this, getScrachCardEntities, ScratchListScreen.this);
        mScratchCardRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mScratchCardRecyclerView.setAdapter(mAdapter);

    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.scratch_card_list_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.scratch_card_list_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }

    @Override
    public void onResume() {
        super.onResume();
        initView();
    }
}
