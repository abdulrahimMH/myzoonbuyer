package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.CustomizationTwoPatternAdapter;
import com.qoltech.mzyoonbuyer.entity.ApicallidEntity;
import com.qoltech.mzyoonbuyer.entity.GetMaterialSelectionEntity;
import com.qoltech.mzyoonbuyer.entity.GetMaterialSelectionModal;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetMaterialSelectionResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GetMaterialSelectionScreen extends BaseActivity {

    @BindView(R.id.get_material_selection_par_lay)
    LinearLayout mGetMaterialSelectionParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.new_flow_material_wiz_lay)
    RelativeLayout mNewFlowMaterialWizLay;

    @BindView(R.id.material_wiz_lay)
    RelativeLayout mMaterialWizLay;

    @BindView(R.id.get_material_num_txt)
    TextView mGetMaterialNumTxt;

    @BindView(R.id.get_material_recycler_view)
    RecyclerView mGetMaterialRecyclerView;

    @BindView(R.id.get_material_price_txt)
    TextView mGetMaterialPriceTxt;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    @BindView(R.id.get_material_available_tailor_lay)
    LinearLayout mGetMaterialAvailableTailorLay;

    @BindView(R.id.get_material_recycler_par_lay)
    RelativeLayout mGetMaterialRecyclerParLay;

    @BindView(R.id.get_material_scroll_view_img)
    ImageView mGetMaterialScrollViewImg;

    private CustomizationTwoPatternAdapter mPattrnAdapter;

    private UserDetailsEntity mUserDetailsEntityRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_get_material_selection_screen);

        initView();
    }
    public void initView(){

        ButterKnife.bind(this);

        setupUI(mGetMaterialSelectionParLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(GetMaterialSelectionScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        AppConstants.ORDER_SUB_TYPE = "";
        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            mMaterialWizLay.setVisibility(View.GONE);
            mNewFlowMaterialWizLay.setVisibility(View.VISIBLE);
            mGetMaterialPriceTxt.setVisibility(View.GONE);
        }else {
            mMaterialWizLay.setVisibility(View.VISIBLE);
            mNewFlowMaterialWizLay.setVisibility(View.GONE);
            mGetMaterialPriceTxt.setVisibility(View.VISIBLE);
        }


        getMaterialSelectedApiCall();
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.material_selection));
        mRightSideImg.setVisibility(View.INVISIBLE);

        mEmptyListTxt.setText(getResources().getString(R.string.no_result_found));

    }

    public void getMaterialSelectedApiCall(){
        if (NetworkUtil.isNetworkAvailable(GetMaterialSelectionScreen.this)){

            ArrayList<ApicallidEntity> mColorId = new ArrayList<>();
            ArrayList<ApicallidEntity> mThicknessId = new ArrayList<>();
            ArrayList<ApicallidEntity> mOrginId = new ArrayList<>();
            ArrayList<ApicallidEntity> mBrandId = new ArrayList<>();
            ArrayList<ApicallidEntity> mMaterialId = new ArrayList<>();

            if (AppConstants.PLACE_INDUSTRY_ID.equalsIgnoreCase("")){
                ApicallidEntity mOrginEntityId = new ApicallidEntity();

                mOrginEntityId.setId("0");

                mOrginId.add(mOrginEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.PLACE_INDUSTRY_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mOrginEntityId = new ApicallidEntity();

                    mOrginEntityId.setId(items.get(i));
                    mOrginId.add(mOrginEntityId);
                }
            }

            if (AppConstants.BRANDS_ID.equalsIgnoreCase("")){
                ApicallidEntity mBrandEntityId = new ApicallidEntity();

                mBrandEntityId.setId("0");

                mBrandId.add(mBrandEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.BRANDS_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mBrandEntityId = new ApicallidEntity();

                    mBrandEntityId.setId(items.get(i));

                    mBrandId.add(mBrandEntityId);
                }
            }
            if (AppConstants.MATERIAL_ID.equalsIgnoreCase("")){
                ApicallidEntity mMaterialEntityId = new ApicallidEntity();

                mMaterialEntityId.setId("0");

                mMaterialId.add(mMaterialEntityId);


            }else {
                List<String> items = Arrays.asList(AppConstants.MATERIAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mMaterialEntityId = new ApicallidEntity();

                    mMaterialEntityId.setId(items.get(i));

                    mMaterialId.add(mMaterialEntityId);

                }
            }

            if (AppConstants.CUST_COLOR_ID.size() > 0){
                Set keys = AppConstants.CUST_COLOR_ID.keySet();
                Iterator itr = keys.iterator();

                String key;
                String value;
                while(itr.hasNext())
                {
                    key = (String) itr.next();
                    value = (String) AppConstants.CUST_COLOR_ID.get(key);

                    ApicallidEntity mColorEntityId = new ApicallidEntity();

                    mColorEntityId.setId(key);

                    mColorId.add(mColorEntityId);
                }

            }else {
                ApicallidEntity mColorEntityId = new ApicallidEntity();

                mColorEntityId.setId("0");

                mColorId.add(mColorEntityId);

            }
            if (AppConstants.THICKNESS_ID.equalsIgnoreCase("")){
                ApicallidEntity mThicknessEntityId = new ApicallidEntity();

                mThicknessEntityId.setId("0");

                mThicknessId.add(mThicknessEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.THICKNESS_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mThicknessEntityId = new ApicallidEntity();

                    mThicknessEntityId.setId(items.get(i));

                    mThicknessId.add(mThicknessEntityId);
                }
            }

            GetMaterialSelectionModal mGetMaterialSelectionResponse = new GetMaterialSelectionModal();

            if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                mGetMaterialSelectionResponse.setType("direct");
                mGetMaterialSelectionResponse.setTailorid(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId());

            }else {
                mGetMaterialSelectionResponse.setType("quotation");
                mGetMaterialSelectionResponse.setTailorid(0);

            }

            mGetMaterialSelectionResponse.setDressSubTypeId(AppConstants.SUB_DRESS_TYPE_ID);
            mGetMaterialSelectionResponse.setThickness(mThicknessId);
            mGetMaterialSelectionResponse.setSubColor(mColorId);
            mGetMaterialSelectionResponse.setPlaceofOrginId(mOrginId);
            mGetMaterialSelectionResponse.setBrandId(mBrandId);
            mGetMaterialSelectionResponse.setMaterialTypeId(mMaterialId);

                APIRequestHandler.getInstance().getMaterialSelection(mGetMaterialSelectionResponse,GetMaterialSelectionScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(GetMaterialSelectionScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getMaterialSelectedApiCall();
                }
            });
        }

    }
    @OnClick({R.id.header_left_side_img,R.id.get_material_filter_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.get_material_filter_lay:
                nextScreen(CustomizationOneScreen.class, true);
                break;
        }
    }


    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetMaterialSelectionResponse){
            GetMaterialSelectionResponse mResponse = (GetMaterialSelectionResponse)resObj;
            setPatternAdapter(mResponse.getResult());
            AppConstants.MATERIAL_AVAILABLE_COUNT = String.valueOf(mResponse.getResult().size());
        }
    }

    /*Set Adapter for the Recycler view*/
    public void setPatternAdapter(ArrayList<GetMaterialSelectionEntity> mPatternList) {

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);

        mGetMaterialNumTxt.setText(String.valueOf(mPatternList.size()));
        mGetMaterialRecyclerParLay.setVisibility(mPatternList.size()>0 ? View.VISIBLE : View.GONE);
        mEmptyListLay.setVisibility(mPatternList.size() > 0 ? View.GONE : View.VISIBLE);

        mPattrnAdapter = new CustomizationTwoPatternAdapter(this,mPatternList);
        mGetMaterialRecyclerView.setLayoutManager(layoutManager);
        mGetMaterialRecyclerView.setAdapter(mPattrnAdapter);

        mGetMaterialRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = layoutManager.findLastVisibleItemPosition();

                if (lastPosition == mPatternList.size()-1){
                    mGetMaterialScrollViewImg.setVisibility(View.GONE);
                }else {
                    mGetMaterialScrollViewImg.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.get_material_selection_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.get_material_selection_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppConstants.PATTERN_ID = "0";
        AppConstants.PATTERN_NAME = "";

        AppConstants.SEASONAL_ID = "";
        AppConstants.SEASONAL_NAME = "";

        AppConstants.PLACE_INDUSTRY_ID = "";
        AppConstants.PLACE_OF_INDUSTRY_NAME = "";

        AppConstants.THICKNESS_ID = "";
        AppConstants.THICKNESS_NAME = "";

        AppConstants.BRANDS_ID = "";
        AppConstants.BRANDS_NAME = "";

        AppConstants.MATERIAL_ID = "";
        AppConstants.MATERIAL_TYPE_NAME = "";

        AppConstants.COLOR_ID = "";
        AppConstants.SUB_COLOR_ID = "";
        AppConstants.COLOUR_NAME = "";

        AppConstants.SEASONAL_OLD_ID = "";
        AppConstants.SEASONAL_OLD_NAME = "";

        AppConstants.PLACE_INDUSTRY_OLD_ID = "";
        AppConstants.PLACE_OF_INDUSTRY_OLD_NAME = "";

        AppConstants.THICKNESS_OLD_ID = "";
        AppConstants.THICKNESS_OLD_NAME = "";

        AppConstants.BRANDS_OLD_ID = "";
        AppConstants.BRANDS_OLD_NAME = "";

        AppConstants.MATERIAL_OLD_ID = "";
        AppConstants.MATERIAL_TYPE_OLD_NAME = "";

        AppConstants.CUST_COLOR_ID = new HashMap<>();
        previousScreen(OrderTypeScreen.class,true);
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (!AppConstants.CUSTOMIZATION_CANCEL.equalsIgnoreCase("CANCEL")){
            getMaterialSelectedApiCall();
//        }
    }
}
