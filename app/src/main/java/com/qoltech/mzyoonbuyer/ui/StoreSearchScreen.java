package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.StoreSearchAdapter;
import com.qoltech.mzyoonbuyer.entity.SearchProductEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.service.APICommonInterface;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreSearchScreen extends BaseActivity {

    @BindView(R.id.store_search_par_lay)
    LinearLayout mStoreSearchParLay;

    @BindView(R.id.store_search_edt_txt)
    EditText mSearchEdtTxt;

    @BindView(R.id.store_search_recycler_view)
    RecyclerView mStoreSearchRecyclerView;

    StoreSearchAdapter mStoreSearchAdapter;

    UserDetailsEntity mUserDetailsEntityRes;

    APICommonInterface mApiCommonInterface;

    HashMap<String,String> mSearchedList = new HashMap<>();

    HashMap<String,String> mSearchHistoryList = new HashMap<>();

    ArrayList<SearchProductEntity> mList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_store_search_screen);

        initView();
    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mStoreSearchParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        mSearchedList = new HashMap<>();
        mSearchHistoryList = new HashMap<>();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(StoreSearchScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        mApiCommonInterface = APIRequestHandler.getClient().create(APICommonInterface.class);

        getLanguage();

        AppConstants.STORE_PRODUCT_REFRESH = "REFRESH";

        mSearchEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().equalsIgnoreCase("")){
                    ArrayList<SearchProductEntity> List = new ArrayList<>();
                    setSearchAdapter(List);
                } else {
                    storeSearchApiCall(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mSearchEdtTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!mSearchEdtTxt.getText().toString().trim().equalsIgnoreCase("")){
                        int searchCount = 0;
                        for (int i=0; i<mList.size(); i++){
                            if (!mList.get(i).isChecked()){
                                searchCount = searchCount + 1;
                            }
                        }

                        if (searchCount  != 0){
                            AppConstants.STORE_SEARCH_TXT = mSearchEdtTxt.getText().toString();
                            AppConstants.STORE_SEARCH = "STORE_SEARCH";
                            nextScreen(StoreProductListScreen.class, true);
                        }
                    }
                    return true;
                }
                return false;
            }
        });
    }

    @OnClick({R.id.store_search_back_btn_img,R.id.store_search_close_btn_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.store_search_back_btn_img:
                onBackPressed();
                break;
            case R.id.store_search_close_btn_img:
                mSearchEdtTxt.setText("");
                break;

        }
    }


    public void storeSearchApiCall(String search){
        if(NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.getStoreSearchApi("products/SearchStoreProduct?ProductName="+search).enqueue(new Callback<ArrayList<SearchProductEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<SearchProductEntity>> call, Response<ArrayList<SearchProductEntity>> response) {
                    if (response.body() != null && response.isSuccessful()){
                        setSearchAdapter(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<SearchProductEntity>> call, Throwable t) {

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    storeSearchApiCall(search);
                }
            });
        }
    }

    public void setSearchAdapter(ArrayList<SearchProductEntity> list){

        ArrayList<SearchProductEntity> List = new ArrayList<>();
        mList = new ArrayList<>();

        Gson gson = new Gson();
        String idJson = PreferenceUtil.getStringValue(StoreSearchScreen.this,AppConstants.STORE_SEARCH_HISTORY);
        Type type = new TypeToken<HashMap<String,String>>(){}.getType();
        mSearchHistoryList = gson.fromJson(idJson,type);

        if (mSearchHistoryList == null){
            mSearchHistoryList = new HashMap<>();
        }

        if (mSearchedList == null){
            mSearchedList = new HashMap<>();
        }

        Set keys = mSearchHistoryList.keySet();
        Iterator itr = keys.iterator();

        String key;
        String value;
        while(itr.hasNext())
        {
            key = (String) itr.next();
            value = (String) mSearchHistoryList.get(key);

            SearchProductEntity entity = new SearchProductEntity();
            entity.setProductName(key);
            entity.setTitle(value);
            entity.setChecked(true);
            entity.setSearchChecked(false);
            List.add(entity);
        }

        List.addAll(list);

        if(mSearchEdtTxt.getText().toString().trim().equalsIgnoreCase("")){
            mList =  new ArrayList<>();
        }else {
            for (SearchProductEntity s : List) {
                //if the existing elements contains the search input
                if (s.getProductName().toLowerCase().contains(mSearchEdtTxt.getText().toString().toLowerCase())) {
                    //adding the element to filtered list
                    mList.add(s);
                }
            }

            int searchCount = 0;
            for (int i=0; i<mList.size(); i++){
                if (!mList.get(i).isChecked()){
                    searchCount = searchCount + 1;
                }
            }

            if (searchCount  == 0){
                SearchProductEntity mEntity = new SearchProductEntity();
                mEntity.setProductName(mSearchEdtTxt.getText().toString());
                mEntity.setTitle(mSearchEdtTxt.getText().toString());
                mEntity.setChecked(false);
                mEntity.setSearchChecked(true);
                mList.add(mEntity);
            }
        }

        mStoreSearchAdapter = new StoreSearchAdapter(mList,mSearchHistoryList,this);
        mStoreSearchRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        mStoreSearchRecyclerView.setAdapter(mStoreSearchAdapter);
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.store_search_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.store_search_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppConstants.STORE_SEARCH = "";
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }

    @Override
    protected void onResume() {
        super.onResume();
       if (!mSearchEdtTxt.getText().toString().trim().equalsIgnoreCase("")){
           storeSearchApiCall(mSearchEdtTxt.getText().toString());
       }
    }
}
