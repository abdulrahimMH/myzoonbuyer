package com.qoltech.mzyoonbuyer.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.GetAppointmentTimeSlotAdapter;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetPriceDetailsResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DirectOrderPriceScreen extends BaseActivity {

    @BindView(R.id.direct_order_price_par_lay)
    RelativeLayout mDirectOrderPriceParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.price_details_qty)
    public EditText mPriceDetailsQtyTxt;

    @BindView(R.id.price_details_total_price)
    TextView mPriceDetailsTotalTxt;

    @BindView(R.id.price_details_stiching_price)
    TextView mStichingChargesTxt;

    @BindView(R.id.price_details_measurement_price)
    TextView mMeasurementChargesTxt;

    @BindView(R.id.price_details_urgent_price)
    TextView mUrgentChargesTxt;

    @BindView(R.id.price_details_delivery_price)
    TextView mDeliveryChargesTxt;

    @BindView(R.id.price_details_material_courier_price)
    TextView mCourierCharges;

    @BindView(R.id.price_details_material_courier_price_lay)
    RelativeLayout mPriceDetailsMaterialCourierPriceLay;

    @BindView(R.id.price_details_delivery_price_lay)
    RelativeLayout mPriceDetailsDeliveryPriceLay;

    @BindView(R.id.price_details_urgent_price_lay)
    RelativeLayout mPriceDetailsUrgentPriceLay;

    @BindView(R.id.price_details_measurement_price_lay)
    RelativeLayout mPriceDetailsMeasurementPriceLay;

    @BindView(R.id.direct_order_delivery_charges_txt)
    TextView mDirectOrderDeliveryChargesTxt;

    private UserDetailsEntity mUserDetailsEntityRes;

    public String mAddedAmt = "",mAmoutStr = "";

    private double mMeasurementChargesInt = 0.0, mUrgentStichingChargesInt = 0.0,mDeliveryChargesInt = 0.0,mMaterialDeliveryChargesInt = 0.0;

    private ArrayList<String> mOrderQtyList;

    private Dialog mOrderQtyDialog;

    private GetAppointmentTimeSlotAdapter mOrderQtyAdater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_direct_order_price_screen);

        initView();

    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mDirectOrderPriceParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(DirectOrderPriceScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        mPriceDetailsQtyTxt.setText("1");

        getPriceDetailsApiCall();

        addTimeSlotList();

        setHeader();

        getLanguage();

        if ( AppConstants.DELIVERY_ID.equalsIgnoreCase("2")){
            mDirectOrderDeliveryChargesTxt.setVisibility(View.VISIBLE);
        }else {
            mDirectOrderDeliveryChargesTxt.setVisibility(View.GONE);
        }

        mPriceDetailsQtyTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().trim().equalsIgnoreCase("")){
                    addAmount();
                }
            }
        });
    }

    public void getPriceDetailsApiCall(){

        if (NetworkUtil.isNetworkAvailable(DirectOrderPriceScreen.this)){
            APIRequestHandler.getInstance().getPriceDetailsApiCall(String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId()),AppConstants.SUB_DRESS_TYPE_ID,AppConstants.PATTERN_ID,AppConstants.ORDER_TYPE_ID,AppConstants.MEASUREMENT_TYPE,AppConstants.DELIVERY_TYPE_ID,AppConstants.DELIVERY_ID,DirectOrderPriceScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(DirectOrderPriceScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getPriceDetailsApiCall();
                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetPriceDetailsResponse){
            GetPriceDetailsResponse mResponse = (GetPriceDetailsResponse)resObj;

            if(mResponse.getResult().getTotal().size() > 0){
                mPriceDetailsTotalTxt.setText(String.valueOf(mResponse.getResult().getTotal().get(0).getTotal()) );
            }
             if (mResponse.getResult().getMeasurementCharges().size() > 0){
                 mPriceDetailsMeasurementPriceLay.setVisibility(Double.parseDouble(mResponse.getResult().getMeasurementCharges().get(0).getMeasurementCharges()) > 0 ? View.VISIBLE : View.GONE);

                 mMeasurementChargesTxt.setText(String.valueOf(mResponse.getResult().getMeasurementCharges().get(0).getMeasurementCharges()));
                mMeasurementChargesInt = Double.parseDouble(mResponse.getResult().getMeasurementCharges().get(0).getMeasurementCharges());
            }
             if (mResponse.getResult().getUrgentStichingCharges().size() > 0){
                 mPriceDetailsUrgentPriceLay.setVisibility(Double.parseDouble(mResponse.getResult().getUrgentStichingCharges().get(0).getUrgentStichingCharges()) > 0 ? View.VISIBLE : View.GONE);

                 mUrgentChargesTxt.setText(String.valueOf(mResponse.getResult().getUrgentStichingCharges().get(0).getUrgentStichingCharges()) );
                mUrgentStichingChargesInt = Double.parseDouble( mResponse.getResult().getUrgentStichingCharges().get(0).getUrgentStichingCharges());
            }
             if (mResponse.getResult().getDeliveryCharges().size() > 0){
                 mPriceDetailsDeliveryPriceLay.setVisibility(Double.parseDouble(mResponse.getResult().getDeliveryCharges().get(0).getDeliveryCharge()) > 0 ? View.VISIBLE : View.GONE);

                 mDeliveryChargesTxt.setText(String.valueOf(mResponse.getResult().getDeliveryCharges().get(0).getDeliveryCharge()));
                mDeliveryChargesInt = Double.parseDouble(mResponse.getResult().getDeliveryCharges().get(0).getDeliveryCharge());
            }
             if (mResponse.getResult().getMaterialDeliveryCharges().size() > 0){
                 mPriceDetailsMaterialCourierPriceLay.setVisibility(Double.parseDouble(mResponse.getResult().getMaterialDeliveryCharges().get(0).getMaterialDeliveryCharges()) > 0 ? View.VISIBLE : View.GONE);

                 mCourierCharges.setText(String.valueOf(mResponse.getResult().getMaterialDeliveryCharges().get(0).getMaterialDeliveryCharges() ));
                mMaterialDeliveryChargesInt = Double.parseDouble(mResponse.getResult().getMaterialDeliveryCharges().get(0).getMaterialDeliveryCharges());
            }
             if (mResponse.getResult().getStichingAndMaterialCharges().size() > 0){
                 mAmoutStr = String.valueOf(mResponse.getResult().getStichingAndMaterialCharges().get(0).getStichingAndMaterialCharge());
                 mStichingChargesTxt.setText(String.valueOf(mResponse.getResult().getStichingAndMaterialCharges().get(0).getStichingAndMaterialCharge()));
             }
        }
    }

    @OnClick({R.id.header_left_side_img,R.id.direct_order_proceed_to_pay_txt,R.id.price_details_qty_lay})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.direct_order_proceed_to_pay_txt:
                if (mPriceDetailsQtyTxt.getText().toString().trim().equalsIgnoreCase("")||mPriceDetailsQtyTxt.getText().toString().trim().equalsIgnoreCase("0")){
                    mPriceDetailsQtyTxt.clearAnimation();
                    mPriceDetailsQtyTxt.setError(getResources().getString(R.string.please_give_qty));
                    mPriceDetailsQtyTxt.setAnimation(ShakeErrorUtils.shakeError());
                }else {
                    AppConstants.TRANSACTION_AMOUNT = mPriceDetailsTotalTxt.getText().toString();
                    AppConstants.UPDATE_QUANTITY = mPriceDetailsQtyTxt.getText().toString().trim();
                    nextScreen(OrderSummaryScreen.class,true);
                }
                break;

//            case R.id.price_details_qty_lay:
//
////                        if (mOrderQtyList.size() > 0){
//                            alertDismiss(mOrderQtyDialog);
//                            mOrderQtyDialog = getDialog(DirectOrderPriceScreen.this, R.layout.pop_up_country_alert);
//
//                            WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
//                            Window window = mOrderQtyDialog.getWindow();
//
//                            if (window != null) {
//                                LayoutParams.copyFrom(window.getAttributes());
//                                LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
//                                LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                                window.setAttributes(LayoutParams);
//                                window.setGravity(Gravity.CENTER);
//                            }
//
//                            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
//                                ViewCompat.setLayoutDirection(mOrderQtyDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
//
//                            }else {
//                                ViewCompat.setLayoutDirection(mOrderQtyDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//
//                            }
//
//                            TextView cancelTxt , headerTxt;
//
//                            RecyclerView countryRecyclerView;
//
//                            /*Init view*/
//                            cancelTxt = mOrderQtyDialog.findViewById(R.id.country_text_cancel);
//                            headerTxt = mOrderQtyDialog.findViewById(R.id.country_header_text_cancel);
//
//                            countryRecyclerView = mOrderQtyDialog.findViewById(R.id.country_popup_recycler_view);
//                            mOrderQtyAdater = new GetAppointmentTimeSlotAdapter(DirectOrderPriceScreen.this, mOrderQtyList,mOrderQtyDialog,"DIRECT_ORDER");
//
//                            countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//                            countryRecyclerView.setAdapter(mOrderQtyAdater);
//
//                            /*Set data*/
//                            headerTxt.setText(getResources().getString(R.string.choose_your_qty));
//                            cancelTxt.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    mOrderQtyDialog.dismiss();
//                                }
//                            });
//
//                            alertShowing(mOrderQtyDialog);
//                        break;

        }

    }

    public void addTimeSlotList(){
        mOrderQtyList = new ArrayList<>();
        mOrderQtyList.add("1");
        mOrderQtyList.add("2");
        mOrderQtyList.add("3");
        mOrderQtyList.add("4");
        mOrderQtyList.add("5");

    }


    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.price_details));
        mRightSideImg.setVisibility(View.INVISIBLE);

    }

    public void addAmount(){
        int qty =Integer.parseInt(mPriceDetailsQtyTxt.getText().toString().trim());
        double amt = Double.parseDouble(mAmoutStr);
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
        DecimalFormat formatter = (DecimalFormat)nf;
        formatter.applyPattern("######.00");

        double total = amt*qty+mMeasurementChargesInt+mUrgentStichingChargesInt*qty+mDeliveryChargesInt+mMaterialDeliveryChargesInt;

        mStichingChargesTxt.setText(String.valueOf(formatter.format(amt*qty)));
        mUrgentChargesTxt.setText(String.valueOf(formatter.format(mUrgentStichingChargesInt*qty)));
        mPriceDetailsTotalTxt.setText(String.valueOf(formatter.format(total)));

        mAddedAmt = String.valueOf(formatter.format(total));

        AppConstants.TRANSACTION_AMOUNT = mAddedAmt;
    }

    @Override
    public void onBackPressed() {
            super.onBackPressed();

            this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);

        }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.direct_order_price_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.direct_order_price_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);


        }
    }

}
