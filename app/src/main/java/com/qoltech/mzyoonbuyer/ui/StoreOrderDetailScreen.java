package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.StoreOtherItemAdapter;
import com.qoltech.mzyoonbuyer.entity.GetStoreProductDetailsEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetStoreTrackingResponse;
import com.qoltech.mzyoonbuyer.modal.StoreOrderDetailsResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StoreOrderDetailScreen extends BaseActivity {

    @BindView(R.id.store_order_details_par_lay)
    LinearLayout mStoreOrderDetailPayLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.store_order_detail_order_id_txt)
    TextView mStoreOrderDetailOrderIdTxt;

    @BindView(R.id.store_details_product_name_txt)
    TextView mStoreDetailProductNameTxt;

    @BindView(R.id.store_details_seller_name_txt)
    TextView mStoreDetailsSellerNameTxt;

    @BindView(R.id.store_details_total_txt)
    TextView mStoreDetailsTotalTxt;

    @BindView(R.id.store_details_product_img)
    ImageView mStoreDetailsProductImg;

    @BindView(R.id.store_details_qty_txt)
    TextView mStoreDetailsQtyTxt;

    @BindView(R.id.store_details_size_txt)
    TextView mStoreDetailsSizeTxt;

    @BindView(R.id.store_details_clr_txt)
    TextView mStoreDetailsClrTxt;

    @BindView(R.id.store_details_name_txt)
    TextView mStoreDetailsNameTxt;

    @BindView(R.id.store_details_address_txt)
    TextView mStoreDetailsAddressTxt;

    @BindView(R.id.order_details_tracking_end_status_txt)
    TextView mStoreOrderDetailsTrackingOneNameTxt;

    @BindView(R.id.store_order_details_tracking_one_name_txt)
    TextView mStoreOrderTrackingAddressTxt;

    @BindView(R.id.store_details_rating_bar)
    RatingBar mStoreDetailsRatingBar;

    @BindView(R.id.store_write_an_review_txt)
    TextView mStoreWriteAnReviewTxt;

    @BindView(R.id.store_order_details_payment_status_img)
    ImageView mStoreOrderDeatailsPaymentStatusLay;

    @BindView(R.id.store_order_details_pay_lay)
    RelativeLayout mStoreOrderDetailsPayLay;

    @BindView(R.id.store_order_details_pay_txt)
    TextView mStoreOrderDetailsPayTxt;

    @BindView(R.id.store_order_other_items_txt)
    TextView mOrderOtherItemsTxt;

    @BindView(R.id.store_other_items_recycler_view)
    RecyclerView mStoreOrderItemsRecyclerView;

    @BindView(R.id.store_payment_details_lay)
    CardView mStorePaymentDetailsLay;

    @BindView(R.id.store_order_card_number_txt)
    TextView mStorePaymentDetailsCarNumTxt;

    @BindView(R.id.store_order_transactin_amt_txt)
    TextView mStoreOrderTransactionAmtTxt;

    @BindView(R.id.store_order_payment_date_txt)
    TextView mStoreOrderPaymentDateTxt;

    @BindView(R.id.store_order_transaction_number)
    TextView mStoreOrderTransactionNumTxt;

    @BindView(R.id.order_details_tracking_details_par_lay)
    LinearLayout mOrderDetailsTrackingDetailsParLay;

    @BindView(R.id.order_details_rate_and_review_lay)
    CardView mStoreDetailsRateAndReviewLay;

    @BindView(R.id.store_details_payment_status_lay)
    CardView mStoreDetailsPaymentStatusLay;

    @BindView(R.id.store_details_list_price_txt)
    TextView mStoreDetailsListPriceTxt;

    @BindView(R.id.store_details_order_total_txt)
            TextView mStoreDetailsOrderTotalTxt;

    @BindView(R.id.store_order_details_other_product_price_lay)
            RelativeLayout mStoreOrderDetailsOterhProductPriceLay;

    @BindView(R.id.store_order_details_other_product_price_txt)
            TextView mStoreOrderDetailsOtherProductPriceTxt;

    @BindView(R.id.store_order_details_stitching_lay)
            RelativeLayout mStoreOrderDetailsStitchingLay;

    @BindView(R.id.store_order_details_stitching_txt)
            TextView mStoreOrderDetailsStitchingTxt;

    @BindView(R.id.store_order_details_grand_total_view)
            View mStoreOrderDetailsGrandTotalView;

    @BindView(R.id.store_order_details_grand_total_lay)
            RelativeLayout mStoreOrderDetailsGrandTotalLay;

    @BindView(R.id.store_order_details_grand_total_txt)
            TextView mStoreOrderDetailsGrandTotalTxt;

    @BindView(R.id.store_order_details_cash_card_lay)
            RelativeLayout mStoreOrderDetailsCashCardLay;

    @BindView(R.id.store_order_details_dicount_lay)
            RelativeLayout mStoreOrderDetailsDiscountLay;

    @BindView(R.id.store_order_details_paid_by_points_lay)
            RelativeLayout mStoreOrderDetailsPaidByPointsLay;

    StoreOtherItemAdapter mStoreOtherItemAdapter;

    private UserDetailsEntity mUserDetailsEntityRes;

    double total = 0;

    String paymentStatus = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_store_order_detail_screen);

        initView();

    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mStoreOrderDetailPayLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        AppConstants.STORE_PAYMENT = "STORE_PAYMENT";

        getOrderDetailsApiCall();
        getStoreTreackingDetailsApiCall();

    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.Store_order_detail));
        mRightSideImg.setVisibility(View.INVISIBLE);
    }

    @OnClick({R.id.header_left_side_img,R.id.store_tracking_details_lay,R.id.order_details_rate_and_review_par_lay,R.id.store_order_details_pay_txt})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.store_tracking_details_lay:
                nextScreen(OrderTrackingDetailsScreen.class,true);
                break;
            case R.id.order_details_rate_and_review_par_lay:
                nextScreen(StoreWriteAnReviewScreen.class,true);
                break;
            case R.id.store_order_details_pay_txt:
                if (paymentStatus.equalsIgnoreCase("Pending")){
                    nextScreen(CheckoutScreen.class,true);
                }
                break;
        }
    }

    public void getOrderDetailsApiCall(){
        if (NetworkUtil.isNetworkAvailable(StoreOrderDetailScreen.this)){
            APIRequestHandler.getInstance().getStoreOrderDetailsApi(AppConstants.ORDER_PENDING_LINE_ID,StoreOrderDetailScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(StoreOrderDetailScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getOrderDetailsApiCall();
                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof StoreOrderDetailsResponse){
            StoreOrderDetailsResponse mResponse = (StoreOrderDetailsResponse)resObj;
            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                if (mResponse.getResult().getStoreOrderDetails().size() > 0) {

                    AppConstants.ORDER_ID = String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getStichingOrderReferenceNo());
                    AppConstants.APPROVED_TAILOR_ID = String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getSellerId());
                    AppConstants.REQUEST_LIST_ID = String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getStichingOrderReferenceNo());
                    AppConstants.STORE_ID = String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getId());
                    AppConstants.TRANSACTION_AMOUNT = String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getOrderTotal());
                    mStoreOrderDetailOrderIdTxt.setText(String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getId()));
                    mStoreDetailProductNameTxt.setText(mResponse.getResult().getStoreOrderDetails().get(0).getNameInArabic());
                    AppConstants.STORE_ORDER_DETAILS_PRODUCT_NAME = mResponse.getResult().getStoreOrderDetails().get(0).getNameInArabic();
                    mStoreDetailsSellerNameTxt.setText(mResponse.getResult().getStoreOrderDetails().get(0).getShopNameInArabic());
                    mStoreDetailsListPriceTxt.setText(String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getAmount()*mResponse.getResult().getStoreOrderDetails().get(0).getQuantity()));
                    mStoreDetailsOrderTotalTxt.setText(String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getAmount()*mResponse.getResult().getStoreOrderDetails().get(0).getQuantity()));
                    mStoreDetailsTotalTxt.setText(String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getAmount()*mResponse.getResult().getStoreOrderDetails().get(0).getQuantity()));
                    mStoreDetailsQtyTxt.setText(String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getQuantity()+" : "+getResources().getString(R.string.quantity)));
                    mStoreDetailsClrTxt.setText(mResponse.getResult().getStoreOrderDetails().get(0).getColorName());
                    mStoreDetailsSizeTxt.setText(mResponse.getResult().getStoreOrderDetails().get(0).getSize());
                    AppConstants.CHECK_OUT_ORDER = mResponse.getResult().getStoreOrderDetails().get(0).getOrderType();
                    AppConstants.CHECK_OUT_ORDER = "store";
                }
            }else {
                if (mResponse.getResult().getStoreOrderDetails().size() > 0){
                    AppConstants.ORDER_ID = String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getStichingOrderReferenceNo());
                    AppConstants.APPROVED_TAILOR_ID = String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getSellerId());
                    AppConstants.REQUEST_LIST_ID = String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getStichingOrderReferenceNo());
                    AppConstants.STORE_ID = String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getId());
                    AppConstants.TRANSACTION_AMOUNT = String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getOrderTotal());
                    mStoreOrderDetailOrderIdTxt.setText(String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getId()));
                    mStoreDetailProductNameTxt.setText(mResponse.getResult().getStoreOrderDetails().get(0).getProduct_Name());
                    AppConstants.STORE_ORDER_DETAILS_PRODUCT_NAME = mResponse.getResult().getStoreOrderDetails().get(0).getProduct_Name();
                    mStoreDetailsSellerNameTxt.setText(mResponse.getResult().getStoreOrderDetails().get(0).getShopNameInArabic() );
                    mStoreDetailsListPriceTxt.setText(String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getAmount()*mResponse.getResult().getStoreOrderDetails().get(0).getQuantity()));
                    mStoreDetailsOrderTotalTxt.setText(String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getAmount()*mResponse.getResult().getStoreOrderDetails().get(0).getQuantity()));
                    mStoreDetailsTotalTxt.setText( String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getAmount()*mResponse.getResult().getStoreOrderDetails().get(0).getQuantity()));
                    mStoreDetailsQtyTxt.setText(getResources().getString(R.string.quantity) +" : "+ String.valueOf(mResponse.getResult().getStoreOrderDetails().get(0).getQuantity()));
                    mStoreDetailsClrTxt.setText(mResponse.getResult().getStoreOrderDetails().get(0).getColorName());
                    mStoreDetailsSizeTxt.setText(mResponse.getResult().getStoreOrderDetails().get(0).getSize());
                    AppConstants.CHECK_OUT_ORDER = mResponse.getResult().getStoreOrderDetails().get(0).getOrderType();
                    AppConstants.CHECK_OUT_ORDER = "store";

                }
            }
            if (mResponse.getResult().getStoreOrderDetails().size() > 0) {
                AppConstants.STORE_ORDER_DETAILS_PRODUCT_IMG = mResponse.getResult().getStoreOrderDetails().get(0).getImage();
                try {
                    Glide.with(StoreOrderDetailScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL + "images/Products/" + mResponse.getResult().getStoreOrderDetails().get(0).getImage())
                            .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                            .into(mStoreDetailsProductImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
            }
            if (mResponse.getResult().getGetShowBuyerAddress().size() >0){
                mStoreDetailsNameTxt.setText(mResponse.getResult().getGetShowBuyerAddress().get(0).getFirstName());
                mStoreDetailsAddressTxt.setText(mResponse.getResult().getGetShowBuyerAddress().get(0).getFloor()+ ", \n"+ mResponse.getResult().getGetShowBuyerAddress().get(0).getLandMark()+ ", \n"+mResponse.getResult().getGetShowBuyerAddress().get(0).getArea()+", \n"+mResponse.getResult().getGetShowBuyerAddress().get(0).getStateName()+", \n"+mResponse.getResult().getGetShowBuyerAddress().get(0).getName()+".");
                mStoreOrderTrackingAddressTxt.setText(mResponse.getResult().getGetShowBuyerAddress().get(0).getFloor()+ ", \n"+ mResponse.getResult().getGetShowBuyerAddress().get(0).getLandMark()+ ", \n"+mResponse.getResult().getGetShowBuyerAddress().get(0).getArea()+", \n"+mResponse.getResult().getGetShowBuyerAddress().get(0).getStateName()+", \n"+mResponse.getResult().getGetShowBuyerAddress().get(0).getName()+".");

            }

            if (mResponse.getResult().getPaymentstatus().size() > 0){
                paymentStatus = mResponse.getResult().getPaymentstatus().get(0).getPaymentstatus();
                if (mResponse.getResult().getPaymentstatus().get(0).getPaymentstatus().equalsIgnoreCase("Pending")){
                    mStoreDetailsPaymentStatusLay.setVisibility(View.VISIBLE);
                    mStoreOrderDetailsPayTxt.setText(getResources().getString(R.string.pay));
                    mStoreOrderDetailsPayTxt.setTextColor(getResources().getColor(R.color.white));
                    mStoreOrderDeatailsPaymentStatusLay.setBackgroundResource(R.drawable.order_details_pending_img);
                    mStoreOrderDetailsPayLay.setBackgroundResource(R.drawable.app_clr_gradient_with_corner);
                    mStoreOrderDetailsCashCardLay.setVisibility(View.GONE);
                    mStoreOrderDetailsDiscountLay.setVisibility(View.GONE);
                    mStoreOrderDetailsPaidByPointsLay.setVisibility(View.GONE);
                }else {
                    mStoreDetailsPaymentStatusLay.setVisibility(View.VISIBLE);
                    mStoreOrderDeatailsPaymentStatusLay.setBackgroundResource(R.drawable.order_details_paid_img);
                    mStoreOrderDetailsPayTxt.setText(getResources().getString(R.string.paid));
                    mStoreOrderDetailsPayTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                    mStoreOrderDetailsPayLay.setBackgroundResource(R.drawable.yellow_gradient_with_corner);
                    mStoreOrderDetailsCashCardLay.setVisibility(View.VISIBLE);
                    mStoreOrderDetailsDiscountLay.setVisibility(View.VISIBLE);
                    mStoreOrderDetailsPaidByPointsLay.setVisibility(View.VISIBLE);

                }

            }

            if (mResponse.getResult().getGetStoreProductDetails().size() > 0){
                mOrderOtherItemsTxt.setVisibility(View.VISIBLE);
                mStoreOrderItemsRecyclerView.setVisibility(View.VISIBLE);
                setStoreOrder(mResponse.getResult().getGetStoreProductDetails());
            }
            if (mResponse.getResult().getGetStorePaymentStatus().size() > 0){
                mStorePaymentDetailsLay.setVisibility(View.VISIBLE);

                mStorePaymentDetailsCarNumTxt.setText("xxxx xxxx xxxx "+mResponse.getResult().getGetStorePaymentStatus().get(0).getCardlast4());
                mStoreOrderTransactionAmtTxt.setText(String.valueOf(mResponse.getResult().getGetStorePaymentStatus().get(0).getAmount()));

                String splitDate[] = mResponse.getResult().getGetStorePaymentStatus().get(0).getPaidOn().split("T");
                mStoreOrderPaymentDateTxt.setText(splitDate[0]);
                mStoreOrderTransactionNumTxt.setText(String.valueOf(mResponse.getResult().getGetStorePaymentStatus().get(0).getTransactionid()));

                mStoreDetailsRateAndReviewLay.setVisibility(View.VISIBLE);
                mOrderDetailsTrackingDetailsParLay.setVisibility(View.VISIBLE);

            }else {
                mStorePaymentDetailsLay.setVisibility(View.GONE);
                mStoreDetailsRateAndReviewLay.setVisibility(View.GONE);
                mOrderDetailsTrackingDetailsParLay.setVisibility(View.GONE);

            }
            AppConstants.STORE_REVIEW_RATING = new ArrayList<>();
            AppConstants.STORE_REVIEW_RATING = mResponse.getResult().getGetStoreReview();
            if (mResponse.getResult().getGetStoreReview().size()>0){
                mStoreDetailsRatingBar.setRating(Float.parseFloat(String.valueOf(mResponse.getResult().getGetStoreReview().get(0).getRating())));
                mStoreWriteAnReviewTxt.setText(getResources().getString(R.string.view_review));
            }
//            if (mResponse.getResult().getProductPrice().size() > 0){
//                mStoreDetailsListPriceTxt.setText(String.valueOf(mResponse.getResult().getProductPrice().get(0).getProductPrice()));
//                mStoreDetailsOrderTotalTxt.setText(String.valueOf(mResponse.getResult().getProductPrice().get(0).getProductPrice()));
//            }
            if (mResponse.getResult().getOtherProductPrice().size() > 0){

                mStoreOrderDetailsOterhProductPriceLay.setVisibility(mResponse.getResult().getOtherProductPrice().get(0).getStichingPrice() > 0 ? View.VISIBLE : View.GONE);

                mStoreOrderDetailsStitchingLay.setVisibility(View.GONE);
                double total = mResponse.getResult().getOtherProductPrice().get(0).getOtherProductPrice() + mResponse.getResult().getOtherProductPrice().get(0).getStichingPrice();

                mStoreOrderDetailsGrandTotalView.setVisibility(total > 0 ? View.VISIBLE : View.GONE);
                mStoreOrderDetailsGrandTotalLay.setVisibility(total > 0 ? View.VISIBLE : View.GONE);

                mStoreOrderDetailsGrandTotalTxt.setText(String.valueOf(total + Double.parseDouble(mStoreDetailsListPriceTxt.getText().toString())));

                mStoreOrderDetailsOtherProductPriceTxt.setText(String.valueOf(total));
                if (mResponse.getResult().getOtherProductPrice().size() > 0)
                {
                    AppConstants.STICTHING_AMT = String.valueOf(mResponse.getResult().getOtherProductPrice().get(0).getStichingPrice());
                    AppConstants.TRANSACTION_AMOUNT = String.valueOf(mResponse.getResult().getOtherProductPrice().get(0).getStichingPrice());

                }

            }
        }
        if (resObj instanceof GetStoreTrackingResponse){
            GetStoreTrackingResponse mRespone = (GetStoreTrackingResponse)resObj;

          for(int i=0; i<mRespone.getGetTracking().size(); i++){
              if (i==0){
                  if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                      mStoreOrderDetailsTrackingOneNameTxt.setText(mRespone.getGetTracking().get(0).getStatusInArabic());

                  }else {
                      mStoreOrderDetailsTrackingOneNameTxt.setText(mRespone.getGetTracking().get(0).getStatus());

                  }
              }
          }
        }

    }

    /*Set Adapter for the Recycler view*/
    public void setStoreOrder(ArrayList<GetStoreProductDetailsEntity> mGetStoreProductList) {

        mStoreOtherItemAdapter = new StoreOtherItemAdapter(this, mGetStoreProductList);
        mStoreOrderItemsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mStoreOrderItemsRecyclerView.setAdapter(mStoreOtherItemAdapter);

    }

    public void getStoreTreackingDetailsApiCall(){
        if (NetworkUtil.isNetworkAvailable(StoreOrderDetailScreen.this)){
            APIRequestHandler.getInstance().getStoreOrderTrackingDetailsApi(AppConstants.ORDER_PENDING_LINE_ID,StoreOrderDetailScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(StoreOrderDetailScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStoreTreackingDetailsApiCall();
                }
            });
        }
    }


    public void getLanguage(){
        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.store_order_details_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.store_order_details_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }

    @Override
    public void onBackPressed() {
        AppConstants.DATE_FOR_RESTRICT_MATERIAL = "";
        AppConstants.DATE_FOR_RESTRICT_MEASUREMENT = "";

        if (!AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")||!AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            super.onBackPressed();
            this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);
        }
    }
}
