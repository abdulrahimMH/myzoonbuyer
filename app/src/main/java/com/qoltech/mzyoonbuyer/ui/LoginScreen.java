package com.qoltech.mzyoonbuyer.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.GetCountryAdapter;
import com.qoltech.mzyoonbuyer.adapter.GetLanguageAdapter;
import com.qoltech.mzyoonbuyer.entity.GetCountryEntity;
import com.qoltech.mzyoonbuyer.entity.GetLanguageEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.main.MyFirebaseInstanceIdService;
import com.qoltech.mzyoonbuyer.modal.CountryCodeResponse;
import com.qoltech.mzyoonbuyer.modal.GetLanguageResponse;
import com.qoltech.mzyoonbuyer.modal.LoginOTPResponse;
import com.qoltech.mzyoonbuyer.service.APICommonInterface;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class LoginScreen extends BaseActivity {

    private GetCountryAdapter mGetCountryAdapter;

    private GetLanguageAdapter mLanguageAdapter;

    private ArrayList<GetCountryEntity> mCountryList;

    ArrayList<GetLanguageEntity> mLanguageEntity;

   private Dialog mDialog,mChooseLanguageDialog;

   @BindView(R.id.flag_img)
    public ImageView mFlagImg;

   @BindView(R.id.choose_language_txt)
   public TextView mChooseLanguageTxt;

   @BindView(R.id.country_code_txt)
   public TextView mCountryCodeTxt;

   @BindView(R.id.mobile_num_edt_txt)
    EditText mMobileNumEdtTxt;

   @BindView(R.id.login_continue_btn)
    Button mLoginContinueBtn;

   @BindView(R.id.login_choose_language_txt)
   TextView mLoginChooseLanguageTxt;

   private UserDetailsEntity mUserDetailsEntity;

   @BindView(R.id.login_par_lay)
    LinearLayout mParLay;

    private String android_id ;

    APICommonInterface mApiCommonInterface;

    MyFirebaseInstanceIdService mMyFirebaseInstanceIdService = new MyFirebaseInstanceIdService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_login_screen);
        initView();

    }

public void initView(){
    ButterKnife.bind(this);

    setupUI(mParLay);

    android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
            Settings.Secure.ANDROID_ID);
    AppConstants.DEVICE_ID = android_id;

    mCountryList = new ArrayList<>();
    mLanguageEntity = new ArrayList<>();

    mApiCommonInterface = APIRequestHandler.getClient().create(APICommonInterface.class);

    getLanguage();

    getCountryApiCall();

    getLanguageApiCall();

}
    @OnClick({R.id.login_continue_btn,R.id.country_code_lay,R.id.login_choose_language_lay,R.id.login_skip_now_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_continue_btn:
                if (mCountryCodeTxt.getText().toString().trim().isEmpty()){
                    mCountryCodeTxt.clearAnimation();
                    mCountryCodeTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mCountryCodeTxt.setError(getResources().getString(R.string.please_select_country_code));
                }
                else if (mMobileNumEdtTxt.getText().toString().trim().isEmpty()){
                    mMobileNumEdtTxt.clearAnimation();
                    mMobileNumEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mMobileNumEdtTxt.setError(getResources().getString(R.string.please_give_mobile_num));
                }else if (mMobileNumEdtTxt.getText().toString().trim().length()<6){
                    mMobileNumEdtTxt.clearAnimation();
                    mMobileNumEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mMobileNumEdtTxt.setError(getResources().getString(R.string.please_give_valid_mobile_num));
                }
                else {
                    loginApiCall();
                }
                break;
            case R.id.country_code_lay:
                if (mCountryList.size() > 0){
                    alertDismiss(mDialog);
                    mDialog = getDialog(LoginScreen.this, R.layout.pop_up_country_alert);

                    WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                    Window window = mDialog.getWindow();

                    if (window != null) {
                        LayoutParams.copyFrom(window.getAttributes());
                        LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
                        window.setAttributes(LayoutParams);
                        window.setGravity(Gravity.CENTER);
                    }

                    if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
                    }else {
                        ViewCompat.setLayoutDirection(mDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }

                    TextView cancelTxt , headerTxt;

                    RecyclerView countryRecyclerView;

                    /*Init view*/
                    cancelTxt = mDialog.findViewById(R.id.country_text_cancel);
                    headerTxt = mDialog.findViewById(R.id.country_header_text_cancel);

                    countryRecyclerView = mDialog.findViewById(R.id.country_popup_recycler_view);
                    mGetCountryAdapter = new GetCountryAdapter(LoginScreen.this, mCountryList,mDialog);

                    countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    countryRecyclerView.setAdapter(mGetCountryAdapter);

                    /*Set data*/
                    headerTxt.setText(getResources().getString(R.string.choose_your_country_code));
                    cancelTxt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    });

                    alertShowing(mDialog);
                }else {
                    Toast.makeText(LoginScreen.this,getResources().getString(R.string.sorry_no_result_found) , Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.login_choose_language_lay:
                if (mLanguageEntity.size() > 0){
                    alertDismiss(mChooseLanguageDialog);
                    mChooseLanguageDialog = getDialog(LoginScreen.this, R.layout.pop_up_country_alert);

                    WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                    Window window = mChooseLanguageDialog.getWindow();

                    if (window != null) {
                        LayoutParams.copyFrom(window.getAttributes());
                        LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(LayoutParams);
                        window.setGravity(Gravity.CENTER);
                    }

                    if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mChooseLanguageDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
                    }else {
                        ViewCompat.setLayoutDirection(mChooseLanguageDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }

                    TextView cancelTxt , headerTxt;

                    RecyclerView countryRecyclerView;

                    /*Init view*/
                    cancelTxt = mChooseLanguageDialog.findViewById(R.id.country_text_cancel);
                    headerTxt = mChooseLanguageDialog.findViewById(R.id.country_header_text_cancel);

                    countryRecyclerView = mChooseLanguageDialog.findViewById(R.id.country_popup_recycler_view);
                    mLanguageAdapter = new GetLanguageAdapter(LoginScreen.this, mLanguageEntity,mChooseLanguageDialog);

                    countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    countryRecyclerView.setAdapter(mLanguageAdapter);

                    /*Set data*/
                    headerTxt.setText(getResources().getString(R.string.choose_your_language));
                    cancelTxt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mChooseLanguageDialog.dismiss();
                        }
                    });

                    alertShowing(mChooseLanguageDialog);
                }else {
                    Toast.makeText(LoginScreen.this,getResources().getString(R.string.sorry_no_result_found) , Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.login_skip_now_lay:
                createCartApi();
                break;
        }

    }

    public void loginApiCall(){
        if (NetworkUtil.isNetworkAvailable(LoginScreen.this)){
            AppConstants.MOBILE_NUM = mMobileNumEdtTxt.getText().toString().trim();
            AppConstants.COUNTRY_CODE = mCountryCodeTxt.getText().toString().trim();
            APIRequestHandler.getInstance().loginOTPGenerateAPICall(mCountryCodeTxt.getText().toString().trim(),mMobileNumEdtTxt.getText().toString().trim(),AppConstants.DEVICE_ID,mUserDetailsEntity.getLanguage(),LoginScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(LoginScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    loginApiCall();
                }
            });
        }
    }

    public void getLanguageApiCall(){
        if (NetworkUtil.isNetworkAvailable(this)){
            APIRequestHandler.getInstance().getLanguageApiCall(LoginScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(LoginScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getLanguageApiCall();
                }
            });
        }
    }

    public void getCountryApiCall(){
        if (NetworkUtil.isNetworkAvailable(this)){
            APIRequestHandler.getInstance().getAllCountryAPICall(LoginScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(LoginScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCountryApiCall();
                }
            });
        }

    }

    public void getLanguage(){
        mUserDetailsEntity = new UserDetailsEntity();

        mMyFirebaseInstanceIdService.onTokenRefresh();

        mUserDetailsEntity.setLanguage(mChooseLanguageTxt.getText().toString());

        if (AppConstants.TOKEN.equalsIgnoreCase("")){
            if (mUserDetailsEntity.getTOKEN().equalsIgnoreCase("")){
                mUserDetailsEntity.setTOKEN(FirebaseInstanceId.getInstance().getToken());
            }else {
                mUserDetailsEntity.setTOKEN(mUserDetailsEntity.getTOKEN());
            }
        }else {
            mUserDetailsEntity.setTOKEN(AppConstants.TOKEN);
        }

        PreferenceUtil.storeUserDetails(LoginScreen.this, mUserDetailsEntity);

        if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.login_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
            updateResources(LoginScreen.this,"ar");

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.login_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
            updateResources(LoginScreen.this,"en");

        }
        mMobileNumEdtTxt.setHint(getResources().getString(R.string.mobile_num));
        mLoginContinueBtn.setText(getResources().getString(R.string.contin));
        mLoginChooseLanguageTxt.setText(getResources().getString(R.string.choose_your_language_login));
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof CountryCodeResponse){
            CountryCodeResponse mResponse = (CountryCodeResponse) resObj;
            mCountryList = new ArrayList<>();
            mCountryList.addAll(mResponse.getResult());
        }
        if (resObj instanceof LoginOTPResponse){
            LoginOTPResponse mResponse = (LoginOTPResponse)resObj;
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")){
                DialogManager.getInstance().showAlertPopup(LoginScreen.this, getResources().getString(R.string.otp_has_been_sent_to)+" "+AppConstants.MOBILE_NUM, new InterfaceBtnCallBack() {
                    @Override
                    public void onPositiveClick() {
                        nextScreen(OTPScreen.class,true);
                    }
                });
            }
        }
        if (resObj instanceof GetLanguageResponse){
            GetLanguageResponse mResponse = (GetLanguageResponse)resObj;
            mLanguageEntity = mResponse.getResult();
        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }
    @Override
    public void onBackPressed() {
        exitFromApp();
    }

    /*App exit popup*/
    private void exitFromApp() {
        DialogManager.getInstance().showOptionPopup(this, getString(R.string.exit_msg),
                getString(R.string.yes), getString(R.string.no),mUserDetailsEntity.getLanguage(), new InterfaceTwoBtnCallBack() {
                    @Override
                    public void onPositiveClick() {
                        ActivityCompat.finishAffinity(LoginScreen.this);
                    }

                    @Override
                    public void onNegativeClick() {

                    }
                });

    }

    public void createCartApi(){
        DialogManager.getInstance().showProgress(this);
        if (NetworkUtil.isNetworkAvailable(this)){
            mApiCommonInterface.createCartApi("cart/CreateCart").enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    DialogManager.getInstance().hideProgress();
                    if (response.body() != null && response.isSuccessful()){

                        PreferenceUtil.storeStringValue(LoginScreen.this,AppConstants.CARD_ID,response.body());

                        mUserDetailsEntity.setUSER_ID("0");

                        PreferenceUtil.storeUserDetails(LoginScreen.this, mUserDetailsEntity);

                        PreferenceUtil.storeBoolPreferenceValue(LoginScreen.this,
                                AppConstants.LOGIN_STATUS, true);

                        nextScreen(BottomNavigationScreen.class, true);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    DialogManager.getInstance().hideProgress();

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    createCartApi();
                }
            });
        }
    }

    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
