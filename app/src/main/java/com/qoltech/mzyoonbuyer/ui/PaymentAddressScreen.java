package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.PaymentInfoEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.PaymentAddressResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentAddressScreen extends BaseActivity {

    @BindView(R.id.payment_address_par_lay)
    RelativeLayout mPaymentAddressParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.payment_first_txt)
    EditText mPaymentFirstNameEdtTxt;

    @BindView(R.id.payment_second_txt)
    EditText mPaymentSecondNameEdtTxt;

    @BindView(R.id.payment_email_edt_txt)
    EditText mPaymentEmailEdtTxt;

    @BindView(R.id.payment_line_one_txt)
    EditText mPaymentLineOneTxt;

    @BindView(R.id.payment_line_two_txt)
    EditText mPaymentLineTwoTxt;

    @BindView(R.id.payment_line_three_txt)
    EditText mPaymentLineThreeTxt;

    @BindView(R.id.payment_city_txt)
    EditText mPaymentCityTxt;

    @BindView(R.id.payment_region_txt)
    EditText mPaymentRegionTxt;

    @BindView(R.id.payment_country_txt)
    EditText mPaymentCountryTxt;

    @BindView(R.id.payment_zip_txt)
    EditText mPaymentZipTxt;

    private UserDetailsEntity mUserDetailsEntity;

    private PaymentInfoEntity mPaymentInfoEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_payment_address_screen);
        initView();

    }

    public void initView(){
        ButterKnife.bind(this);

        setupUI(mPaymentAddressParLay);
        mUserDetailsEntity = new UserDetailsEntity();
        mPaymentInfoEntity = new PaymentInfoEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(PaymentAddressScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntity = gson.fromJson(json, UserDetailsEntity.class);


        String jsons = PreferenceUtil.getStringValue(PaymentAddressScreen.this, AppConstants.PAYMENT_DETAILS);
        mPaymentInfoEntity = gson.fromJson(jsons, PaymentInfoEntity.class);

        setHeader();
        getLanguage();

        if (mPaymentInfoEntity != null){
            setPaymentInfo(mPaymentInfoEntity);
        }else {
            getPaymentDetailsApiCall();
        }

    }

    private void setPaymentInfo(PaymentInfoEntity mPaymentInfoEntity) {
        mPaymentFirstNameEdtTxt.setText(mPaymentInfoEntity.getFirstName());
        mPaymentSecondNameEdtTxt.setText(mPaymentInfoEntity.getSecondName());
    mPaymentEmailEdtTxt.setText(mPaymentInfoEntity.getEmail());
    mPaymentLineOneTxt.setText(mPaymentInfoEntity.getLineOne());
    mPaymentLineTwoTxt.setText(mPaymentInfoEntity.getLineTwo());
    mPaymentLineThreeTxt.setText(mPaymentInfoEntity.getLineThree());
    mPaymentCityTxt.setText(mPaymentInfoEntity.getCity());
    mPaymentRegionTxt.setText(mPaymentInfoEntity.getRegion());
    mPaymentCountryTxt.setText(convert(mPaymentInfoEntity.getCountry()));
    mPaymentZipTxt.setText(mPaymentInfoEntity.getZip());
    getPaymentDetailsApiCall();
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.payment_type));
        mRightSideImg.setVisibility(View.INVISIBLE);

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")|| AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            mHeaderLeftBackBtn.setVisibility(View.GONE);
        }
        }

    @OnClick({R.id.header_left_side_img,R.id.payment_save_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.payment_save_btn :
                if (mPaymentFirstNameEdtTxt.getText().toString().trim().equalsIgnoreCase("")){
                    mPaymentFirstNameEdtTxt.clearAnimation();
                    mPaymentFirstNameEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mPaymentFirstNameEdtTxt.setError(getResources().getString(R.string.please_fill_your_name));
                }else if (mPaymentSecondNameEdtTxt.getText().toString().trim().equalsIgnoreCase("")){
                    mPaymentSecondNameEdtTxt.clearAnimation();
                    mPaymentSecondNameEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mPaymentSecondNameEdtTxt.setError(getResources().getString(R.string.please_fill_your_name));
                }else if (mPaymentEmailEdtTxt.getText().toString().trim().equalsIgnoreCase("")){
                    mPaymentEmailEdtTxt.clearAnimation();
                    mPaymentEmailEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mPaymentEmailEdtTxt.setError(getResources().getString(R.string.please_give_your_email_address));
                }else if (!isEmailValid(mPaymentEmailEdtTxt.getText().toString().trim())){
                    mPaymentEmailEdtTxt.clearAnimation();
                    mPaymentEmailEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mPaymentEmailEdtTxt.setError(getResources().getString(R.string.please_give_valid_email_address));
                 } else  if (mPaymentLineOneTxt.getText().toString().trim().equalsIgnoreCase("")){
                    mPaymentLineOneTxt.clearAnimation();
                    mPaymentLineOneTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mPaymentLineOneTxt.setError("");
                }else  if (mPaymentLineTwoTxt.getText().toString().trim().equalsIgnoreCase("")){
                    mPaymentLineTwoTxt.clearAnimation();
                    mPaymentLineTwoTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mPaymentLineTwoTxt.setError("");
                }else  if (mPaymentLineThreeTxt.getText().toString().trim().equalsIgnoreCase("")){
                    mPaymentLineThreeTxt.clearAnimation();
                    mPaymentLineThreeTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mPaymentLineThreeTxt.setError("");
                }else  if (mPaymentCityTxt.getText().toString().trim().equalsIgnoreCase("")){
                    mPaymentCityTxt.clearAnimation();
                    mPaymentCityTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mPaymentCityTxt.setError("");
                }else  if (mPaymentRegionTxt.getText().toString().trim().equalsIgnoreCase("")){
                    mPaymentRegionTxt.clearAnimation();
                    mPaymentRegionTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mPaymentRegionTxt.setError("");
                }else  if (mPaymentCountryTxt.getText().toString().trim().equalsIgnoreCase("")){
                    mPaymentCountryTxt.clearAnimation();
                    mPaymentCountryTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mPaymentCountryTxt.setError("");
                }else  if (mPaymentZipTxt.getText().toString().trim().equalsIgnoreCase("")){
                    mPaymentZipTxt.clearAnimation();
                    mPaymentZipTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mPaymentZipTxt.setError("");
                }else {
                    mPaymentInfoEntity = new PaymentInfoEntity();

                    mPaymentInfoEntity.setFirstName(mPaymentFirstNameEdtTxt.getText().toString().trim());
                    mPaymentInfoEntity.setSecondName(mPaymentSecondNameEdtTxt.getText().toString().trim());
                    mPaymentInfoEntity.setEmail(mPaymentEmailEdtTxt.getText().toString().trim());
                    mPaymentInfoEntity.setLineOne(mPaymentLineOneTxt.getText().toString());
                    mPaymentInfoEntity.setLineTwo(mPaymentLineTwoTxt.getText().toString());
                    mPaymentInfoEntity.setLineThree(mPaymentLineThreeTxt.getText().toString());
                    mPaymentInfoEntity.setCity(mPaymentCityTxt.getText().toString());
                    mPaymentInfoEntity.setRegion(mPaymentRegionTxt.getText().toString());
                    mPaymentInfoEntity.setCountry(mPaymentCountryTxt.getText().toString().equalsIgnoreCase("United Arab Emirates") ? "UAE" : mPaymentCountryTxt.getText().toString());
                    mPaymentInfoEntity.setZip(mPaymentZipTxt.getText().toString().trim());

                    PreferenceUtil.storePaymentDetails(PaymentAddressScreen.this, mPaymentInfoEntity);
                    nextScreen(CheckoutScreen.class, true);
                }
                break;

        }

    }
    public String convert(String str) {
        // Create a char array of given String
        char ch[] = str.toCharArray();
        for (int i = 0; i < str.length(); i++) {

            // If first character of a word is found
            if (i == 0 && ch[i] != ' ' ||
                    ch[i] != ' ' && ch[i - 1] == ' ') {

                // If it is in lower-case
                if (ch[i] >= 'a' && ch[i] <= 'z') {

                    // Convert into Upper-case
                    ch[i] = (char)(ch[i] - 'a' + 'A');
                }
            }

            // If apart from first character
            // Any one is in Upper-case
            else if (ch[i] >= 'A' && ch[i] <= 'Z')

                // Convert into Lower-Case
                ch[i] = (char)(ch[i] + 'a' - 'A');
        }

        // Convert the char array to equivalent String
        String st = new String(ch);
        return st;
    }
    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public void getPaymentDetailsApiCall(){
        if (NetworkUtil.isNetworkAvailable(PaymentAddressScreen.this)){
            APIRequestHandler.getInstance().getPaymentAddressDetailApiCall(mUserDetailsEntity.getUSER_ID(),PaymentAddressScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(PaymentAddressScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getPaymentDetailsApiCall();
                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof PaymentAddressResponse){
            PaymentAddressResponse mResponse =(PaymentAddressResponse)resObj;
           if (mResponse.getResult().size()>0){
               mPaymentEmailEdtTxt.setText(mResponse.getResult().get(0).getEmail());
               mPaymentLineOneTxt.setText(mResponse.getResult().get(0).getFloor());
               mPaymentLineTwoTxt.setText(mResponse.getResult().get(0).getLandmark());
               mPaymentLineThreeTxt.setText(mResponse.getResult().get(0).getLocationType());
               mPaymentCityTxt.setText(mResponse.getResult().get(0).getStateName());
               mPaymentRegionTxt.setText(mResponse.getResult().get(0).getArea());
               mPaymentCountryTxt.setText(convert(mResponse.getResult().get(0).getCountryName()));
           }

        }
    }

    public void getLanguage(){

        if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.payment_address_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.payment_address_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
        }
    }
    @Override
    public void onBackPressed() {
        if (!AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")|| !AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){

            super.onBackPressed();
            this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);
        }
    }
}
