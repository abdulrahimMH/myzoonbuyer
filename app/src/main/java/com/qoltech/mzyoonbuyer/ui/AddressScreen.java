package com.qoltech.mzyoonbuyer.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.GetAddressAdapter;
import com.qoltech.mzyoonbuyer.entity.GetAddressEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.DeleteAddressResponse;
import com.qoltech.mzyoonbuyer.modal.GetAddressResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddressScreen extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    @BindView(R.id.address_par_lay)
    LinearLayout mAddressPayLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    private GetAddressAdapter mGetAddressAdapter;

    private LocationManager  locationManager;

    private boolean GpsStatus ;

    ArrayList<GetAddressResponse> mGetAddressEntity;

    @BindView(R.id.get_address_recycler_view)
    RecyclerView mGetAddressRecyclerView;

    @BindView(R.id.get_address_empty_img_lay)
    RelativeLayout mGetAddressEmptyImgLay;

    @BindView(R.id.recycler_view_add_address_btn)
    RelativeLayout mRecyclerViewAddAddressBtn;

    @BindView(R.id.address_add_btn)
    Button mAddressAddBtn;

    @BindView(R.id.measurement_address_wizard_lay)
    RelativeLayout mMeasurementAddressWizardLay;

    @BindView(R.id.new_flow_address_wiz_lay)
    RelativeLayout mNewFlowAddressWizLay;

    private UserDetailsEntity mUserDetailsEntityRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_address_screen);

        initView();
    }
    public void initView(){
        ButterKnife.bind(this);

        setupUI(mAddressPayLay);

        mGetAddressEntity = new ArrayList<>();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(AddressScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();
        getAddressApiCall();
        setHeader();
        CheckGpsStatus();

        if (AppConstants.SERVICE_TYPE.equalsIgnoreCase("SERVICE_TYPE")){
            mMeasurementAddressWizardLay.setVisibility(View.VISIBLE);
            mNewFlowAddressWizLay.setVisibility(View.GONE);
        }else {
            mMeasurementAddressWizardLay.setVisibility(View.VISIBLE);
            mNewFlowAddressWizLay.setVisibility(View.GONE);
        }
        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            mMeasurementAddressWizardLay.setVisibility(View.GONE);
            mNewFlowAddressWizLay.setVisibility(View.VISIBLE);
        }else {
            mMeasurementAddressWizardLay.setVisibility(View.VISIBLE);
            mNewFlowAddressWizLay.setVisibility(View.GONE);
        }
        if ( AppConstants.EDIT_ADDRESS.equalsIgnoreCase("EDIT_ADDRESS")){
            mMeasurementAddressWizardLay.setVisibility(View.GONE);
            mNewFlowAddressWizLay.setVisibility(View.GONE);
        }
        if (AppConstants.SERVICE_TYPE.equalsIgnoreCase("CART")){
            mMeasurementAddressWizardLay.setVisibility(View.GONE);
            mNewFlowAddressWizLay.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.address_add_btn,R.id.header_left_side_img,R.id.recyclerview_address_add_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.address_add_btn:
                AppConstants.CHECK_ADD_ADDRESS = "";
                if (checkPermission()){
                    CheckGpsStatus();
                    if (GpsStatus){
                        AppConstants.EDIT_ADDRESS = "";
                        nextScreen(ConfirmAddressScreen.class,true);
                    }else {
                        EnableGPSAutoMatically();
                    }
                }
                break;
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.recyclerview_address_add_btn:
                AppConstants.CHECK_ADD_ADDRESS = "";
                if (checkPermission()) {
                    CheckGpsStatus();
                    if (GpsStatus){
                        AppConstants.EDIT_ADDRESS = "";
                        nextScreen(ConfirmAddressScreen.class, true);
                    }else {
                        EnableGPSAutoMatically();
                    }
                }
                break;
        }

    }


    public void getAddressApiCall(){
        if (NetworkUtil.isNetworkAvailable(AddressScreen.this)){
            APIRequestHandler.getInstance().getBuyerAddressApiCall(mUserDetailsEntityRes.getUSER_ID(),AddressScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(AddressScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAddressApiCall();
                }
            });
        }
    }
    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.address));
        mRightSideImg.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetAddressResponse){
            GetAddressResponse mResponse = (GetAddressResponse)resObj;
            mGetAddressEmptyImgLay.setVisibility(mResponse.getResult().size()>0 ? View.GONE : View.VISIBLE);
            mRecyclerViewAddAddressBtn.setVisibility(mResponse.getResult().size()>0 ? View.VISIBLE : View.GONE);
            setAdapter(mResponse.getResult());
        }
        if (resObj instanceof DeleteAddressResponse){
            DeleteAddressResponse mResponse = (DeleteAddressResponse)resObj;
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")){
                getAddressApiCall();
            }
        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<GetAddressEntity> mAddressList) {

            mGetAddressAdapter = new GetAddressAdapter(this,mAddressList,AddressScreen.this,mUserDetailsEntityRes.getUSER_ID());
            mGetAddressRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            mGetAddressRecyclerView.setAdapter(mGetAddressAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getAddressApiCall();
    }

    /* Ask for permission on Camera access*/
    private boolean checkPermission() {

        boolean addPermission = true;
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            int fineLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            int coarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

            if (fineLocation != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            else if (coarseLocation != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }

        }
        if (!listPermissionsNeeded.isEmpty()) {
            addPermission = askAccessPermission(listPermissionsNeeded, 1, new InterfaceTwoBtnCallBack() {
                @Override
                public void onNegativeClick() {

                }

                @Override
                public void onPositiveClick() {
                    nextScreen(ConfirmAddressScreen.class,true);                }

            });
        }

        return addPermission;

    }

    public void CheckGpsStatus(){

        locationManager = (LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);
            builder.setNeedBle(true);
            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
//                            toast("Success");
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                            toast("GPS is not on");
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(AddressScreen.this, 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                            toast("Setting change not allowed");
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1000) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    public void getLanguage(){
        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.address_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.address_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (AppConstants.ADDRESS_ON_BACK.equalsIgnoreCase("true")){
            previousScreen(BottomNavigationScreen.class,true);
        }
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
