package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.StitchingStoreOrderDetailsAdapter;
import com.qoltech.mzyoonbuyer.entity.IdEntity;
import com.qoltech.mzyoonbuyer.entity.StichingOrderDetailsEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.CheckoutDeliveryChargesResponse;
import com.qoltech.mzyoonbuyer.modal.OrderDetailsStitchingStoreModal;
import com.qoltech.mzyoonbuyer.modal.OrderDetailsStitchingStoreResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderDetailsScreen extends BaseActivity {

    @BindView(R.id.stitch_store_order_details_par_lay)
    LinearLayout mStitchStoreOrderDetailsParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.order_details_date_txt)
    TextView mOrderDetailsDateTxt;

    @BindView(R.id.order_details_order_id_txt)
    TextView mOrderDetailsOrderIdTxt;

    @BindView(R.id.order_details_order_total_amt_txt)
    TextView mOrderDetailsOrderTotalAmtTxt;

    @BindView(R.id.order_details_stitching_charges_lay)
    RelativeLayout mOrderDetailsStitchingChargesParLay;

    @BindView(R.id.order_details_stiching_measurement_amt_txt)
    TextView mOrderDetailsStitchingMeasurementAmtTxt;

    @BindView(R.id.order_details_measurement_amt_par_lay)
    RelativeLayout mOrderDetailsMeasurementAmtParLay;

    @BindView(R.id.order_details_measurement_amt_txt)
    TextView mOrderDetailsMeasurementAmtTxt;

    @BindView(R.id.order_details_urgent_amt_par_lay)
    RelativeLayout mOrderDetailsUrgenAmtParLay;

    @BindView(R.id.order_details_urgent_amt_txt)
    TextView mOrderDetailsUrgentAmtTxt;

    @BindView(R.id.order_details_delivery_amt_par_lay)
    RelativeLayout mOrderDetailsDeliveryAmtParLay;

    @BindView(R.id.order_details_delivery_amt_txt)
    TextView mOrderDetailsDeliveryAmtTxt;

    @BindView(R.id.order_details_material_amt_par_lay)
    RelativeLayout mOrderDetailsMaterialAmtParLay;

    @BindView(R.id.order_details_material_delivery_amt_txt)
    TextView mOrderDetailsMaterialDeliveryAmtTxt;

    @BindView(R.id.order_details_total_amt_txt)
    TextView mOrderDetailsTotalAmtTxt;

    @BindView(R.id.order_details_total_lay)
    RelativeLayout mOrderDetailsTotalLay;

    @BindView(R.id.order_details_store_lay)
    RelativeLayout mOrderDetailsStoreAmtParLay;

    @BindView(R.id.order_details_store_txt)
    TextView mOrderDetailsStoreAmtTxt;

    @BindView(R.id.order_details_grand_total_view)
    View mOrderDetailsGrandTotalView;

    @BindView(R.id.order_details_grand_total_lay)
    RelativeLayout mOrderDetailsGrandTotalParLay;

    @BindView(R.id.order_details_grand_total_txt)
    TextView mOrderDetailsGrandTotalAmtTxt;

    @BindView(R.id.order_details_delivery_address_txt)
    TextView mOrderDetailsDeliveryAddressTxt;

    @BindView(R.id.order_details_delivery_name_txt)
    TextView mOrderDetailsDeliveryNameTxt;

    @BindView(R.id.order_details_payment_status_img)
    ImageView mOrderDetailsPaymentStatusImg;

    @BindView(R.id.order_details_pay_lay)
    RelativeLayout mOrderDetailsPaymentParLay;

    @BindView(R.id.order_details_pay_txt)
    TextView mOrderDetailsPaymentTxt;

    @BindView(R.id.order_details_card_cash_par_lay)
    RelativeLayout mOrderDetailsCardCasParLay;

    @BindView(R.id.order_details_card_cash_txt)
    TextView mOrderDetailsCardOrCashTxt;

    @BindView(R.id.order_details_stitching_store_rec_view)
    RecyclerView mOrderDetailsStitchingRecView;

    @BindView(R.id.order_details_promo_code_amt_lay)
    RelativeLayout mOrderDetailsPromoCodeAmtLay;

    @BindView(R.id.order_details_promo_code_amt_txt)
    TextView mOrderDetailsPromoCodeAmtTxt;

    @BindView(R.id.order_details_reduced_points_amt_lay)
    RelativeLayout mOrderDetailsReducedPointsAmtLay;

    @BindView(R.id.order_details_reduced_points_amt_txt)
    TextView mOrderDetailsReducedPointAmtTxt;

    @BindView(R.id.order_details_payment_details_par_lay)
    LinearLayout mOrderDetailsPaymentDetailsParLay;

    @BindView(R.id.order_details_card_number_txt)
    TextView mOrderDetailsCardNumTxt;

    @BindView(R.id.order_details_transaction_number_txt)
    TextView mOrderDetailsTransactionNumberTxt;

    @BindView(R.id.order_details_payment_date_txt)
    TextView mOrderDetailsPaymentDateTxt;

    @BindView(R.id.order_details_transaction_amount_txt)
    TextView mOrderDetailsTransactionAmtTxt;

    @BindView(R.id.order_details_delivery_type_par_lay)
    LinearLayout mOrderDetailsDeliveryTypeParLay;

    @BindView(R.id.order_detail_delivery_type_txt)
    TextView mOrderDetailsDeliveryTypeTxt;

    @BindView(R.id.order_details_total_view)
            View mOrderDetailsTotalView;

    StitchingStoreOrderDetailsAdapter mStitchingOrderDetailsAdapter;

    HashMap<String,String> mSellerList = new HashMap<>();

    int mMaterialSizeInt = 0,mMeasurementSizeInt = 0,mDeliveryTypeInt = 0,mMeasurementTypeInt = 0,mOrderTypeInt = 0;

    String mOrderTypeNameStr = "STO", mTotalAmtDou = "0",mGrandTotalAmtDou = "0";

    UserDetailsEntity mUserDetailsEntityRes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_order_details_screen);

        initView();
    }

    public void initView() {
        ButterKnife.bind(this);

        setupUI(mStitchStoreOrderDetailsParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();
        mSellerList = new HashMap<>();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(OrderDetailsScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        AppConstants.DELIVERY_AREA_ID = "-0";

        getOrderDetailsStitchingStoreApiCall();

        AppConstants.ORDER_DETAILS_ON_BACK = "ORDER_DETAIL";

        AppConstants.CHECK_OUT_ORDER = AppConstants.ORDER_TYPE_SKILL.equalsIgnoreCase("StitchingStore") ? "StitchingStore" : AppConstants.ORDER_TYPE_SKILL.equalsIgnoreCase("Stitching") ? "Stitching" :"store";

        AppConstants.STORE_PAYMENT = AppConstants.ORDER_TYPE_SKILL.equalsIgnoreCase("store") ? "STORE_PAYMENT" : "";

        AppConstants.DIRECT_ORDER = "";

        setHeader();

        getLanguage();

    }

    public void setHeader(){
        String heading =  AppConstants.PENDING_DELIVERY_CLICK.substring(0, 1).toUpperCase() + AppConstants.PENDING_DELIVERY_CLICK.substring(1);
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(heading+ " " + getResources().getString(R.string.order_details));
        mRightSideImg.setVisibility(View.INVISIBLE);
    }

    @OnClick({R.id.header_left_side_img,R.id.order_details_pay_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.order_details_pay_lay:
                if (AppConstants.PAYMENT_STATUS.equalsIgnoreCase("Not paid")&&AppConstants.MATERIAL_TYPE.equalsIgnoreCase("2")&&mMaterialSizeInt==0||AppConstants.PAYMENT_STATUS.equalsIgnoreCase("Not paid")&&mMeasurementTypeInt == 3&&mMeasurementSizeInt==0){
                    DialogManager.getInstance().showAlertPopup(OrderDetailsScreen.this, getResources().getString(R.string.book_an_appointment_before_pay), new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {
                            AppConstants.CHECK_BOOK_APPOINTMENT = "RequestList";
                            nextScreen(AppointmentDetails.class,true);
                        }
                    });
                }
                else if (AppConstants.PAYMENT_STATUS.equalsIgnoreCase("Not paid")&&mDeliveryTypeInt == 3){
                    DialogManager.getInstance().showAlertPopup(OrderDetailsScreen.this, getResources().getString(R.string.book_an_appointment_before_pay), new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {
                            AppConstants.CHECK_BOOK_APPOINTMENT = "RequestList";
                            nextScreen(ScheduletDetailScreen.class,true);
                        }
                    });
                }
                else {
                    if (AppConstants.PAYMENT_STATUS.equalsIgnoreCase("Not paid")){
                        AppConstants.SERVICE_TYPE = "";
                        nextScreen(CheckoutScreen.class,true);
                    }
                }
                break;
        }
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.stitch_store_order_details_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.stitch_store_order_details_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    public void getOrderDetailsStitchingStoreApiCall(){
        if (NetworkUtil.isNetworkAvailable(OrderDetailsScreen.this)){
            APIRequestHandler.getInstance().getOrderDetailsStitchingStoreApi(AppConstants.ORDER_DETAILS_PENDING_ID,AppConstants.ORDER_TYPE_SKILL.replace("-Direct","").trim(), OrderDetailsScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderDetailsScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getOrderDetailsStitchingStoreApiCall();
                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof OrderDetailsStitchingStoreResponse){
            OrderDetailsStitchingStoreResponse mREsponse = (OrderDetailsStitchingStoreResponse)resObj;

            mMaterialSizeInt = mREsponse.getResult().getGetAppoinmentLeftMateril().size();
            mMeasurementSizeInt = mREsponse.getResult().getGetAppoinmentLeftMeasurement().size();

            for (int i=0 ; i<mREsponse.getResult().getStichingOrderDetails().size(); i++){
                if (mREsponse.getResult().getStichingOrderDetails().get(i).getOrderType().equalsIgnoreCase("store")){
                    AppConstants.STORE_ID = String.valueOf(mREsponse.getResult().getStichingOrderDetails().get(i).getOrderId());
                }
                String StitchingStr = mREsponse.getResult().getStichingOrderDetails().get(i).getOrderType();
                StitchingStr = StitchingStr.replace("-Direct","");
                StitchingStr = StitchingStr.replace("-Quotation","");
                if (StitchingStr.equalsIgnoreCase("Stitching")) {
                    mOrderTypeNameStr = "STI";
                }
                mSellerList.put(String.valueOf(mREsponse.getResult().getStichingOrderDetails().get(i).getSellerId()),mREsponse.getResult().getStichingOrderDetails().get(i).getTailorNameInEnglish());
            }


            mOrderDetailsDeliveryTypeParLay.setVisibility(AppConstants.ORDER_TYPE_SKILL.equalsIgnoreCase("store") ? View.GONE : View.VISIBLE);

            if (mREsponse.getResult().getStichingOrderDetails().size() > 0){
                AppConstants.APPOINTMENT_LIST_ID = String.valueOf(mREsponse.getResult().getStichingOrderDetails().get(0).getOrderId());
                AppConstants.ORDER_ID = String.valueOf(mREsponse.getResult().getStichingOrderDetails().get(0).getOrderId());
                AppConstants.REQUEST_LIST_ID = String.valueOf(mREsponse.getResult().getStichingOrderDetails().get(0).getOrderId());
                mOrderDetailsOrderIdTxt.setText(String.valueOf(mREsponse.getResult().getStichingOrderDetails().get(0).getOrderId() + " - " +mOrderTypeNameStr));
                AppConstants.ORDER_PENDING_ID = String.valueOf(mREsponse.getResult().getStichingOrderDetails().get(0).getOrderId());
                AppConstants.APPROVED_TAILOR_ID = String.valueOf(mREsponse.getResult().getStichingOrderDetails().get(0).getSellerId());
                AppConstants.TAILOR_CLICK_ID = String.valueOf(mREsponse.getResult().getStichingOrderDetails().get(0).getSellerId());
                mOrderTypeInt = mREsponse.getResult().getStichingOrderDetails().get(0).getOrderTypeId();
                mMeasurementTypeInt = mREsponse.getResult().getStichingOrderDetails().get(0).getMeasurementTypeId();
                mDeliveryTypeInt = mREsponse.getResult().getStichingOrderDetails().get(0).getDeliveryTypeId();
                mOrderDetailsDateTxt.setText(String.valueOf(mREsponse.getResult().getStichingOrderDetails().get(0).getCreatedOn()));
                AppConstants.PATTERN_ID = String.valueOf(mREsponse.getResult().getStichingOrderDetails().get(0).getPatternId());
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOrderDetailsDeliveryTypeTxt.setText(mREsponse.getResult().getStichingOrderDetails().get(0).getDeliveryNameInArabic());
                }else {
                    mOrderDetailsDeliveryTypeTxt.setText(mREsponse.getResult().getStichingOrderDetails().get(0).getDeliveryNameInEnglish());
                }
//                if (mREsponse.getResult().getStichingOrderDetails().get(0).getOrderType().equalsIgnoreCase("store")){
//                    AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(mREsponse.getResult().getStichingOrderDetails().get(0).getOrderLineId());
//
//                }else {
                AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(mREsponse.getResult().getStichingOrderDetails().get(0).getDressSubTypeId());
//                }
                AppConstants.DRESS_TYPE_ID = String.valueOf(mREsponse.getResult().getStichingOrderDetails().get(0).getDressTypeId());
                AppConstants.MATERIAL_TYPE = String.valueOf(mREsponse.getResult().getStichingOrderDetails().get(0).getOrderTypeId());
                AppConstants.MATERIAL_ONE_TYPE_NAME = mREsponse.getResult().getStichingOrderDetails().get(0).getMaterialTypeInEnglish();
                AppConstants.MATERIAL_ONE_TYPE_ARABIC = mREsponse.getResult().getStichingOrderDetails().get(0).getMaterialTypeInArabic();
                AppConstants.MATERIAL_TYPE_ID =String.valueOf( mREsponse.getResult().getStichingOrderDetails().get(0).getPatternId());
                AppConstants.MEASUREMENT_PARTS_MEASUREMENT_TYPE =  mREsponse.getResult().getStichingOrderDetails().get(0).getMeasurementInEnglish();
            }

            if (mREsponse.getResult().getGetGrandTotal().size() > 0) {
                mGrandTotalAmtDou = mREsponse.getResult().getGetGrandTotal().get(0).getGrandTotal();
                AppConstants.TRANSACTION_AMOUNT = String.valueOf(mREsponse.getResult().getGetGrandTotal().get(0).getGrandTotal());
                mOrderDetailsOrderTotalAmtTxt.setText(String.valueOf(mREsponse.getResult().getGetGrandTotal().get(0).getGrandTotal()));
                mOrderDetailsGrandTotalAmtTxt.setText(String.valueOf(mREsponse.getResult().getGetGrandTotal().get(0).getGrandTotal()));
//                mOrderDetailsCardOrCashTxt.setText(String.valueOf(mREsponse.getResult().getGetGrandTotal().get(0).getGrandTotal()));
            }

            if (mREsponse.getResult().getStichingAndMaterialCharge().size() > 0){
                mOrderDetailsStitchingChargesParLay.setVisibility(Double.parseDouble(mREsponse.getResult().getStichingAndMaterialCharge().get(0).getStichingAndMaterialCharge()) > 0 ? View.VISIBLE  : View.GONE);
                mOrderDetailsStitchingMeasurementAmtTxt.setText(String.valueOf(mREsponse.getResult().getStichingAndMaterialCharge().get(0).getStichingAndMaterialCharge()));
            }

            if (mREsponse.getResult().getMeasurementCharges().size() > 0){
                mOrderDetailsMeasurementAmtParLay.setVisibility(Double.parseDouble(mREsponse.getResult().getMeasurementCharges().get(0).getMeasurementCharges()) > 0 ? View.VISIBLE : View.GONE);
                mOrderDetailsMeasurementAmtTxt.setText(String.valueOf(mREsponse.getResult().getMeasurementCharges().get(0).getMeasurementCharges()));
            }

            if (mREsponse.getResult().getUrgentStichingCharges().size() > 0){
                mOrderDetailsUrgenAmtParLay.setVisibility(Double.parseDouble(mREsponse.getResult().getUrgentStichingCharges().get(0).getUrgentStichingCharges()) > 0 ? View.VISIBLE : View.GONE);
                mOrderDetailsUrgentAmtTxt.setText(String.valueOf(mREsponse.getResult().getUrgentStichingCharges().get(0).getUrgentStichingCharges()));
            }

            if (mREsponse.getResult().getDeliveryCharge().size() > 0){
                mOrderDetailsDeliveryAmtParLay.setVisibility(Double.parseDouble(mREsponse.getResult().getDeliveryCharge().get(0).getDeliveryCharge()) > 0 ? View.VISIBLE : View.GONE);
                mOrderDetailsDeliveryAmtTxt.setText(String.valueOf(mREsponse.getResult().getDeliveryCharge().get(0).getDeliveryCharge()));
            }

            if (mREsponse.getResult().getMaterialDeliveryCharges().size() > 0) {
                mOrderDetailsMaterialAmtParLay.setVisibility(Double.parseDouble(mREsponse.getResult().getMaterialDeliveryCharges().get(0).getMaterialDeliveryCharges()) > 0 ? View.VISIBLE : View.GONE);
                mOrderDetailsMaterialDeliveryAmtTxt.setText(String.valueOf(mREsponse.getResult().getMaterialDeliveryCharges().get(0).getMaterialDeliveryCharges()));
            }

            if (mREsponse.getResult().getTotal().size() > 0){
                mTotalAmtDou = mREsponse.getResult().getTotal().get(0).getTotal();
                mOrderDetailsTotalView.setVisibility(Double.parseDouble(mREsponse.getResult().getTotal().get(0).getTotal()) > 0 ? View.VISIBLE : View.GONE);
                mOrderDetailsTotalLay.setVisibility(Double.parseDouble(mREsponse.getResult().getTotal().get(0).getTotal()) > 0 ? View.VISIBLE : View.GONE);
                mOrderDetailsTotalAmtTxt.setText(String.valueOf(mREsponse.getResult().getTotal().get(0).getTotal()));
            }else {
                mOrderDetailsTotalLay.setVisibility(View.GONE);
                mOrderDetailsTotalView.setVisibility(View.GONE);
            }

            if (mREsponse.getResult().getGetstoreProductPrice().size() > 0) {
                mOrderDetailsStoreAmtParLay.setVisibility(Double.parseDouble(mREsponse.getResult().getGetstoreProductPrice().get(0).getStoreProductPrice()) > 0 ? View.VISIBLE : View.GONE);
                mOrderDetailsStoreAmtTxt.setText(String.valueOf(mREsponse.getResult().getGetstoreProductPrice().get(0).getStoreProductPrice()));
                mOrderDetailsGrandTotalView.setVisibility(Double.parseDouble(mREsponse.getResult().getGetstoreProductPrice().get(0).getStoreProductPrice()) > 0 ? View.VISIBLE : View.GONE);
                mOrderDetailsGrandTotalParLay.setVisibility(Double.parseDouble(mREsponse.getResult().getGetstoreProductPrice().get(0).getStoreProductPrice()) > 0 ? View.VISIBLE : View.GONE);

            }

            if (mREsponse.getResult().getGetPaymentReceivedStatus().size() > 0){
                AppConstants.PAYMENT_STATUS = mREsponse.getResult().getGetPaymentReceivedStatus().get(0).getPaymentReceivedStatus();
                if (mREsponse.getResult().getGetPaymentReceivedStatus().get(0).getPaymentReceivedStatus().equalsIgnoreCase("Not paid")){
                    mOrderDetailsPaymentStatusImg.setBackground(getResources().getDrawable(R.drawable.payment_pending_img));
                    mOrderDetailsPaymentParLay.setBackgroundResource(R.drawable.red_clr_with_lite_corner);
                    mOrderDetailsPaymentTxt.setText(getResources().getString(R.string.pay));
                    mOrderDetailsPaymentTxt.setTextColor(getResources().getColor(R.color.white));
                    mOrderDetailsPromoCodeAmtLay.setVisibility(View.GONE);

                }else {
                    mOrderDetailsPaymentStatusImg.setBackground(getResources().getDrawable(R.drawable.payment_paid_img));
                    mOrderDetailsPaymentParLay.setBackgroundResource(R.drawable.green_clr_with_lite_corner);
                    mOrderDetailsPaymentTxt.setText(getResources().getString(R.string.paid));
                    mOrderDetailsPaymentTxt.setTextColor(getResources().getColor(R.color.white));
                }
            }

            if (mREsponse.getResult().getGetPaymentStatus().size() > 0){
                mOrderDetailsPaymentDetailsParLay.setVisibility(View.VISIBLE);
                mOrderDetailsCardNumTxt.setText(String.valueOf("xxxx xxxx xxxx "+mREsponse.getResult().getGetPaymentStatus().get(0).getCardlast4()));
                mOrderDetailsTransactionAmtTxt.setText(String.valueOf(mREsponse.getResult().getGetPaymentStatus().get(0).getAmount()));
                mOrderDetailsPaymentDateTxt.setText(String.valueOf(mREsponse.getResult().getGetPaymentStatus().get(0).getPaidOn()));
                mOrderDetailsTransactionNumberTxt.setText(String.valueOf(mREsponse.getResult().getGetPaymentStatus().get(0).getTransactionid()));
            }

            if (mREsponse.getResult().getGetPaymentDetails().size() > 0) {
                mOrderDetailsCardCasParLay.setVisibility(View.VISIBLE);
                mOrderDetailsReducedPointsAmtLay.setVisibility(View.VISIBLE);
                mOrderDetailsPromoCodeAmtLay.setVisibility(View.VISIBLE);
                mOrderDetailsCardOrCashTxt.setText(String.valueOf(mREsponse.getResult().getGetPaymentDetails().get(0).getAmount()));
                mOrderDetailsPromoCodeAmtTxt.setText(String.valueOf(mREsponse.getResult().getGetPaymentDetails().get(0).getCoupanReduceAmount()));
                mOrderDetailsReducedPointAmtTxt.setText(String.valueOf(mREsponse.getResult().getGetPaymentDetails().get(0).getReduceAmount()));
            }

            if (mREsponse.getResult().getGetBuyerAddress().size() > 0){
                AppConstants.DELIVERY_AREA_ID = String.valueOf(mREsponse.getResult().getGetBuyerAddress().get(0).getAreaId());
                mOrderDetailsDeliveryNameTxt.setText(mREsponse.getResult().getGetBuyerAddress().get(0).getFirstName());
                mOrderDetailsDeliveryAddressTxt.setText(mREsponse.getResult().getGetBuyerAddress().get(0).getFloor()+", "+mREsponse.getResult().getGetBuyerAddress().get(0).getArea()+", \n"+mREsponse.getResult().getGetBuyerAddress().get(0).getStateName()+", \n"+mREsponse.getResult().getGetBuyerAddress().get(0).getCountryName()+", \n"+mREsponse.getResult().getGetBuyerAddress().get(0).getPhoneNo());

            }

            setOrderDetailsAdapter(mSellerList,mREsponse.getResult().getStichingOrderDetails(),mREsponse.getResult());
//            if (!AppConstants.DELIVERY_AREA_ID.equalsIgnoreCase("-0")){
//            getDeliveryChargesApiCall();
//            }
        }
        if (resObj instanceof CheckoutDeliveryChargesResponse){
            CheckoutDeliveryChargesResponse mResponse = (CheckoutDeliveryChargesResponse)resObj;

            if (mResponse.getResult().size() > 0){
                mOrderDetailsDeliveryAmtParLay.setVisibility(mResponse.getResult().get(0).getAmount() > 0 ? View.VISIBLE : View.GONE);
                mOrderDetailsDeliveryAmtTxt.setText(String.valueOf(mResponse.getResult().get(0).getAmount()));

                if (mResponse.getResult().get(0).getAmount() > 0){
                    mOrderDetailsTotalLay.setVisibility(View.VISIBLE);
                    mOrderDetailsTotalView.setVisibility(View.VISIBLE);
                    mOrderDetailsTotalAmtTxt.setText(String.valueOf( Double.parseDouble(mTotalAmtDou) + mResponse.getResult().get(0).getAmount()));
                    AppConstants.TRANSACTION_AMOUNT = String.valueOf(Double.parseDouble(mGrandTotalAmtDou) + mResponse.getResult().get(0).getAmount());
                    mOrderDetailsOrderTotalAmtTxt.setText(String.valueOf(Double.parseDouble(mGrandTotalAmtDou) + mResponse.getResult().get(0).getAmount()));
                    mOrderDetailsGrandTotalAmtTxt.setText(String.valueOf(Double.parseDouble(mGrandTotalAmtDou) + mResponse.getResult().get(0).getAmount()));
                }
            }
        }
    }


    public void setOrderDetailsAdapter(HashMap<String, String> sellerList, ArrayList<StichingOrderDetailsEntity> stichingOrderDetails, OrderDetailsStitchingStoreModal result){

        ArrayList<String> sellerIdStr = new ArrayList<>();
        ArrayList<String> sellerName = new ArrayList<>();
        AppConstants.DELIVERY_TAILOR_ID = new ArrayList<>();

        Set keys = sellerList.keySet();
        Iterator itr = keys.iterator();

        String key;
        String value;
        while(itr.hasNext()) {
            key = (String) itr.next();
            value = (String) sellerList.get(key);

            sellerIdStr.add(key);
            sellerName.add(value);
            IdEntity id = new IdEntity();
            id.setId(Integer.parseInt(key));
            AppConstants.DELIVERY_TAILOR_ID.add(id);
        }

            mStitchingOrderDetailsAdapter = new StitchingStoreOrderDetailsAdapter(stichingOrderDetails,this,sellerIdStr,sellerName,result);
            mOrderDetailsStitchingRecView.setLayoutManager(new LinearLayoutManager(this,LinearLayout.VERTICAL,false));
            mOrderDetailsStitchingRecView.setAdapter(mStitchingOrderDetailsAdapter);
            mOrderDetailsStitchingRecView.setNestedScrollingEnabled(false);

        }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
