package com.qoltech.mzyoonbuyer.ui;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.ShopTimingAdapter;
import com.qoltech.mzyoonbuyer.adapter.ViewDetailPagerAdapter;
import com.qoltech.mzyoonbuyer.entity.ShopTimingEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.ShopDetailsReponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ZoomOutPageTransformer;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShopDetailsScreen extends BaseActivity {

    @BindView(R.id.shop_details_par_lay)
    LinearLayout mShopDetailsParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.bottom_expand_call_txt)
    TextView mBottomExpandCallTxt;

    @BindView(R.id.tailor_detail_expand_web_txt)
    TextView mTailorDetailExpandWebTxt;

    @BindView(R.id.tailor_detail_expand_shop_name_txt)
    TextView mTailorDetailExpandShopNameTxt;

    @BindView(R.id.tailor_detail_expand_address_header_txt)
    TextView mTailorDetailsExpandAddressHeaderTxt;

    @BindView(R.id.tailor_detail_expand_address_txt)
    TextView mTailorDetailsExpandAddressTxt;

    @BindView(R.id.tailor_detail_expand_shop_rating_bar)
    RatingBar mTailorDetailsExpandShopRatingBar;

    @BindView(R.id.tailor_detail_expand_no_txt)
    TextView mTailorDetailsExpandNoTxt;

    @BindView(R.id.tailor_detail_expand_shop_rating_txt)
    TextView mTailorDetailsExpandShopRatingTxt;

    @BindView(R.id.tailor_detail_expand_shop_average_rating_txt)
    TextView mTailorDetailsExpandShopAverageRatingTxt;

    @BindView(R.id.shop_details_view_pager)
    ViewPager mViewDetailsViewpager;

    @BindView(R.id.shop_details_pageIndicatorView)
    com.rd.PageIndicatorView mPageIndicator;

    private String mWebPageSiteStr = "" ,mMobileStr = "";

    private UserDetailsEntity mUserDetailsEntity;

    ViewDetailPagerAdapter mViewDetailAdapter;

    ShopTimingAdapter mShopTimingAdapter;

    Dialog mTimeningDialog;

    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;
    final long PERIOD_MS = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_shop_details);

        initView();
    }
    public void initView(){
        ButterKnife.bind(this);

        setupUI(mShopDetailsParLay);

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(ShopDetailsScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntity = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        setHeader();

        getShopDetailsApiCall();
        AppConstants.TAILOR_CLICK_ID = AppConstants.TAILOR_ID;
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.shop_details));
        mRightSideImg.setVisibility(View.INVISIBLE);

    }

    @OnClick({R.id.header_left_side_img,R.id.bottom_expand_web_site_par_lay,R.id.bottom_expand_call_lay,R.id.bottom_expand_share_lay,R.id.bottom_expand_dir_lay,R.id.shop_details_rating_and_review_lay,R.id.shop_details_timeing_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.bottom_expand_web_site_par_lay:
                if (!mWebPageSiteStr.equalsIgnoreCase("")){
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mWebPageSiteStr));
                    startActivity(browserIntent);
                }
                break;
            case R.id.bottom_expand_call_lay:
                if (!mMobileStr.equalsIgnoreCase("")){
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + mMobileStr));
                    startActivity(intent);
                }
                break;
            case R.id.bottom_expand_share_lay:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = getResources().getString(R.string.next_milestone_str);
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "MZYOON");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case R.id.bottom_expand_dir_lay:
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr=" + String.valueOf(AppConstants.SHOP_LAT) + "," + String.valueOf(AppConstants.SHOP_LONG) + "&daddr=" + AppConstants.SHOP_OWN_LAT + "," + AppConstants.SHOP_OWN_LONG));
                    startActivity(intent);
                    break;
            case R.id.shop_details_rating_and_review_lay:
                AppConstants.MATERIAL_REVIEW = "";
                nextScreen(RatingScreen.class,true);
                break;
            case R.id.shop_details_timeing_lay:
                getTimeingDialog();
                break;
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof ShopDetailsReponse){
            ShopDetailsReponse mResponse = (ShopDetailsReponse)resObj;
            mBottomExpandCallTxt.setText(getResources().getString(R.string.no_mobile_num));
            mTailorDetailExpandWebTxt.setText(getResources().getString(R.string.no_web_page));
            mWebPageSiteStr = "";
            mMobileStr = "";

            if(mResponse.getResult().getGetShopDetails().size()>0){
                if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")) {

                    mTailorDetailExpandShopNameTxt.setText(mResponse.getResult().getGetShopDetails().get(0).getShopNameInArabic());
                    mTailorDetailsExpandAddressHeaderTxt.setText(" : " + getResources().getString(R.string.address));
                    mTailorDetailsExpandAddressTxt.setText(mResponse.getResult().getGetShopDetails().get(0).getStateName()+", \n" + mResponse.getResult().getGetShopDetails().get(0).getCountryName()+", \n" + mResponse.getResult().getGetShopDetails().get(0).getAddressinArabic()+".");
                } else {
                    mTailorDetailExpandShopNameTxt.setText(mResponse.getResult().getGetShopDetails().get(0).getShopNameInEnglish());
                    mTailorDetailsExpandAddressHeaderTxt.setText(getResources().getString(R.string.address) +" : ");
                    mTailorDetailsExpandAddressTxt.setText(mResponse.getResult().getGetShopDetails().get(0).getStateName()+", \n" + mResponse.getResult().getGetShopDetails().get(0).getCountryName()+", \n" + mResponse.getResult().getGetShopDetails().get(0).getAddressInEnglish()+".");
                }

                mBottomExpandCallTxt.setText(mResponse.getResult().getGetShopDetails().get(0).getPhoneNumber().equalsIgnoreCase("") ? getResources().getString(R.string.no_mobile_num) : mResponse.getResult().getGetShopDetails().get(0).getPhoneNumber());
                mTailorDetailExpandWebTxt.setText(mResponse.getResult().getGetShopDetails().get(0).getWebSite().equalsIgnoreCase("") ? getResources().getString(R.string.no_web_page) : mResponse.getResult().getGetShopDetails().get(0).getWebSite());
                mWebPageSiteStr = mResponse.getResult().getGetShopDetails().get(0).getWebSite();
                mMobileStr = mResponse.getResult().getGetShopDetails().get(0).getPhoneNumber();
                AppConstants.SHOP_OWN_LAT = mResponse.getResult().getGetShopDetails().get(0).getLatitude();
                AppConstants.SHOP_OWN_LONG = mResponse.getResult().getGetShopDetails().get(0).getLongitude();

            }
            if (mResponse.getResult().getRating().size()>0){
                NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                DecimalFormat formatter = (DecimalFormat)nf;
                formatter.applyPattern("######.#");
                mTailorDetailsExpandShopAverageRatingTxt.setText(String.valueOf(formatter.format(mResponse.getResult().getRating().get(0).getRating())));
                mTailorDetailsExpandShopRatingBar.setRating(mResponse.getResult().getRating().get(0).getRating());
            }
            if (mResponse.getResult().getOrderCount().size()>0){
                mTailorDetailsExpandNoTxt.setText(String.valueOf(mResponse.getResult().getOrderCount().get(0).getOrderCount()));
            }
            if (mResponse.getResult().getReviewCount().size()>0){
                mTailorDetailsExpandShopRatingTxt.setText(String.valueOf("("+mResponse.getResult().getReviewCount().get(0).getReviewCount()+getResources().getString(R.string.review)+")"));
            }

            ArrayList<String> shopImageList = new ArrayList<>();

            for (int i=0; i< mResponse.getResult().getShopImages().size(); i++){
                shopImageList.add(AppConstants.IMAGE_BASE_URL+"Images/ShopImages/"+mResponse.getResult().getShopImages().get(i).getImage());
            }
            viewPagerGet(shopImageList);

        }
    }
    public void viewPagerGet(ArrayList<String> image){
        mViewDetailAdapter = new ViewDetailPagerAdapter(this,image);
        mViewDetailsViewpager.setAdapter(mViewDetailAdapter);
        mViewDetailsViewpager.setPageTransformer(true, new ZoomOutPageTransformer());
        mPageIndicator.setViewPager(mViewDetailsViewpager);

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == image.size()) {
                    currentPage = 0;
                }
                mViewDetailsViewpager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);

        //page change tracker
        mViewDetailsViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        trackScreenName("ShopDetails");
    }

    public void getTimeingDialog(){
        alertDismiss(mTimeningDialog);
        mTimeningDialog = getDialog(ShopDetailsScreen.this, R.layout.pop_up_country_alert);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mTimeningDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.BOTTOM);
            window.getAttributes().windowAnimations = R.style.PopupBottomAnimation;
        }

        if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mTimeningDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(mTimeningDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        TextView mSetHeaderTxt,mCancelTxt;

        RecyclerView mShopDetailsRecyclerView;

        mSetHeaderTxt = mTimeningDialog.findViewById(R.id.country_header_text_cancel);
        mShopDetailsRecyclerView = mTimeningDialog.findViewById(R.id.country_popup_recycler_view);
        mCancelTxt = mTimeningDialog.findViewById(R.id.country_text_cancel);

        mSetHeaderTxt.setText(getResources().getString(R.string.shop_timeing));

        ArrayList<ShopTimingEntity> mShopTimeList = new ArrayList<>();
        ShopTimingEntity shopTimeEntity =  new ShopTimingEntity();
        shopTimeEntity.setChecked(false);
        shopTimeEntity.setShopDay("Sunday");
        shopTimeEntity.setShopTime("9.00am - 9.00 pm");
        mShopTimeList.add(shopTimeEntity);
        shopTimeEntity = new ShopTimingEntity();
        shopTimeEntity.setChecked(false);
        shopTimeEntity.setShopDay("Monday");
        shopTimeEntity.setShopTime("9.00am - 9.00 pm");
        mShopTimeList.add(shopTimeEntity);
        shopTimeEntity = new ShopTimingEntity();
        shopTimeEntity.setChecked(false);
        shopTimeEntity.setShopDay("Tuesday");
        shopTimeEntity.setShopTime("9.00am - 9.00 pm");
        mShopTimeList.add(shopTimeEntity);
        shopTimeEntity = new ShopTimingEntity();
        shopTimeEntity.setChecked(false);
        shopTimeEntity.setShopDay("Wednesday");
        shopTimeEntity.setShopTime("9.00am - 9.00 pm");
        mShopTimeList.add(shopTimeEntity);
        shopTimeEntity = new ShopTimingEntity();
        shopTimeEntity.setChecked(false);
        shopTimeEntity.setShopDay("Thursday");
        shopTimeEntity.setShopTime("9.00am - 9.00 pm");
        mShopTimeList.add(shopTimeEntity);
        shopTimeEntity = new ShopTimingEntity();
        shopTimeEntity.setChecked(false);
        shopTimeEntity.setShopDay("Friday");
        shopTimeEntity.setShopTime("2.00pm - 10.00 pm");
        mShopTimeList.add(shopTimeEntity);
        shopTimeEntity = new ShopTimingEntity();
        shopTimeEntity.setChecked(false);
        shopTimeEntity.setShopDay("Saturday");
        shopTimeEntity.setShopTime("9.00am - 9.00 pm");
        mShopTimeList.add(shopTimeEntity);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE", Locale.ENGLISH);
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);

        for (int i=0; i<mShopTimeList.size(); i++){
            if (dayOfTheWeek.equals(mShopTimeList.get(i).getShopDay())){
                mShopTimeList.get(i).setChecked(true);
            }
        }

        mShopTimingAdapter = new ShopTimingAdapter(this,mShopTimeList );
        mShopDetailsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mShopDetailsRecyclerView.setAdapter(mShopTimingAdapter);

        mCancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTimeningDialog.dismiss();
            }
        });

        alertShowing(mTimeningDialog);
    }

    public void getShopDetailsApiCall(){
        if (NetworkUtil.isNetworkAvailable(ShopDetailsScreen.this)){
            APIRequestHandler.getInstance().getShopDetailsHidePrgressBarApiCall(AppConstants.TAILOR_ID, ShopDetailsScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ShopDetailsScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getShopDetailsApiCall();
                }
            });
        }
    }

    public void getLanguage(){

        if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.shop_details_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.shop_details_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
