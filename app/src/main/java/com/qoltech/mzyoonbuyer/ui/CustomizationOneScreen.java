package com.qoltech.mzyoonbuyer.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.CustomizationOneBrandAdapter;
import com.qoltech.mzyoonbuyer.adapter.CustomizationOnePlaceAdapter;
import com.qoltech.mzyoonbuyer.adapter.CustomizationOneSessionAdapter;
import com.qoltech.mzyoonbuyer.adapter.CustomizationOneThicknessAdapter;
import com.qoltech.mzyoonbuyer.adapter.CustomizationTwoColorAdapter;
import com.qoltech.mzyoonbuyer.adapter.CustomizationTwoMaterialAdapter;
import com.qoltech.mzyoonbuyer.adapter.CustomizationTwoSubColorAdapter;
import com.qoltech.mzyoonbuyer.entity.ApicallidEntity;
import com.qoltech.mzyoonbuyer.entity.CustomizationBrandEntity;
import com.qoltech.mzyoonbuyer.entity.CustomizationColorsEntity;
import com.qoltech.mzyoonbuyer.entity.CustomizationMaterialsEntity;
import com.qoltech.mzyoonbuyer.entity.CustomizationPlaceEntity;
import com.qoltech.mzyoonbuyer.entity.CustomizationSessionEntity;
import com.qoltech.mzyoonbuyer.entity.CustomizationThicknessEntity;
import com.qoltech.mzyoonbuyer.entity.SubColorEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.CustomizationNewApiCallModal;
import com.qoltech.mzyoonbuyer.modal.CustomizationOneApiCallModal;
import com.qoltech.mzyoonbuyer.modal.GetCustomizationOneResponse;
import com.qoltech.mzyoonbuyer.modal.GetNewCustomizationResponse;
import com.qoltech.mzyoonbuyer.modal.NewFlowCustomizationNewApiCallModal;
import com.qoltech.mzyoonbuyer.modal.NewFlowCustomizationOneApiCall;
import com.qoltech.mzyoonbuyer.modal.SubColorResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomizationOneScreen extends BaseActivity {

    private CustomizationOneSessionAdapter mSeasonalAdapter;
    private CustomizationOnePlaceAdapter mIndustryAdapter;
    private CustomizationOneBrandAdapter mBrandAdapter;
    private CustomizationOneThicknessAdapter mThicknessAdapter;
    private CustomizationTwoMaterialAdapter mMaterialAdapter;
    private CustomizationTwoColorAdapter mColorAdapter;
    private CustomizationTwoSubColorAdapter mSubColorAdapter;
//
//    private ArrayList<CustomizationSessionEntity> mSeasonalList;
//    private ArrayList<CustomizationPlaceEntity> mIndustryList;
//    private ArrayList<CustomizationBrandEntity> mBrandList;
//    private ArrayList<CustomizationThicknessEntity> mThicknessList;

    @BindView(R.id.customization_one_par_lay)
    LinearLayout mCustomizationOneParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.customize_seaonal_recycler_view)
    RecyclerView mSeasonalRecyclerView;

    @BindView(R.id.customize_industry_recycler_view)
    RecyclerView mIndustryRecyclerView;

    @BindView(R.id.customize_brands_recycler_view)
    RecyclerView mBrandRecyclerView;

    @BindView(R.id.customize_thickness_recycler_view)
    RecyclerView mCustomizationThicknessRecyclerView;

    @BindView(R.id.thickness_empty_list_txt)
    TextView mThicknessEmptyListTxt;

    @BindView(R.id.customize_material_recycler_view)
    RecyclerView mMaterialRecyclerView;

    @BindView(R.id.customize_color_recycler_view)
    RecyclerView mColorRecyclerView;

    @BindView(R.id.customization_one_available_count_txt)
    TextView mCustomizationOneAvailableCountTxt;

//    @BindView(R.id.new_flow_material_wiz_lay)
//    RelativeLayout mNewFlowMaterialWizLay;
//
//    @BindView(R.id.material_wiz_lay)
//    RelativeLayout mMaterialWizLay;
//
//    @BindView(R.id.customize_one_nxt_lay)
//    RelativeLayout mCustomizationOneNxtLay;

    @BindView(R.id.customize_brand_empty_txt)
    TextView mCustomizeBrandEmptyTxt;

    private UserDetailsEntity mUserDetailsEntityRes;

    Dialog mSubColorDialog;

    ArrayList<String> mColorIdList = new ArrayList<>();

    ArrayList<String> mSubColorIdList = new ArrayList<>();

    ArrayList<CustomizationColorsEntity> mCutsomizationColorEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_customization_one_screen);

        initView();
    }
    public void initView(){

        ButterKnife.bind(this);

        setupUI(mCustomizationOneParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(CustomizationOneScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        setHeader();

//        mSeasonalList = new ArrayList<>();
//        mIndustryList = new ArrayList<>();
//        mBrandList = new ArrayList<>();
//        mThicknessList = new ArrayList<>();
        mCutsomizationColorEntity = new ArrayList<>();

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
            getNewFlowCustomizationNewApiCall();

        }else {
            getNewCustomizationApiCall();

        }
//        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
//            mMaterialWizLay.setVisibility(View.GONE);
//            mNewFlowMaterialWizLay.setVisibility(View.VISIBLE);
//        }else {
//            mMaterialWizLay.setVisibility(View.VISIBLE);
//            mNewFlowMaterialWizLay.setVisibility(View.GONE);
//        }
        getLanguage();
    }

    @OnClick({R.id.header_left_side_img,R.id.customization_one_apply_filter_txt,R.id.customization_one_reset_txt,R.id.customization_one_cancel_txt})
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.customize_one_nxt_lay:
//                if ( AppConstants.THICKNESS_ID.equalsIgnoreCase("")){
//                    mCustomizationOneNxtLay.clearAnimation();
//                    mCustomizationOneNxtLay.setAnimation(ShakeErrorUtils.shakeError());
//                    Toast.makeText(CustomizationOneScreen.this,getResources().getString(R.string.please_select_thickness),Toast.LENGTH_SHORT).show();
//                }else {
//                    nextScreen(CustomizationTwoScreen.class,true);
//                }
//                break;
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.customization_one_apply_filter_txt:
                AppConstants.SEASONAL_OLD_ID = AppConstants.SEASONAL_ID ;
                AppConstants.SEASONAL_OLD_NAME =  AppConstants.SEASONAL_NAME;

                AppConstants.PLACE_INDUSTRY_OLD_ID =  AppConstants.PLACE_INDUSTRY_ID;
                AppConstants.PLACE_OF_INDUSTRY_OLD_NAME = AppConstants.PLACE_OF_INDUSTRY_NAME;

                AppConstants.THICKNESS_OLD_ID = AppConstants.THICKNESS_ID;
                AppConstants.THICKNESS_OLD_NAME = AppConstants.THICKNESS_NAME;

                AppConstants.BRANDS_OLD_ID =  AppConstants.BRANDS_ID ;
                AppConstants.BRANDS_OLD_NAME = AppConstants.BRANDS_NAME;

                AppConstants.MATERIAL_OLD_ID = AppConstants.MATERIAL_ID;
                AppConstants.MATERIAL_TYPE_OLD_NAME = AppConstants.MATERIAL_TYPE_NAME;

//                AppConstants.CUST_COLOR_OLD_ID = new HashMap<>();
//                AppConstants.CUST_COLOR_OLD_ID = AppConstants.CUST_COLOR_ID;
                AppConstants.COLOR_ID = "";
                AppConstants.SUB_COLOR_ID = "";

                Set keys = AppConstants.CUST_COLOR_ID.keySet();
                Iterator itr = keys.iterator();

                String key;
                String value;
                while(itr.hasNext())
                {
                    key = (String) itr.next();
                    value = (String) AppConstants.CUST_COLOR_ID.get(key);

                    AppConstants.SUB_COLOR_ID = AppConstants.SUB_COLOR_ID.equalsIgnoreCase("") ? key : AppConstants.SUB_COLOR_ID + ","+ key;
                    AppConstants.COLOR_ID = AppConstants.COLOR_ID.equalsIgnoreCase("") ? value : AppConstants.COLOR_ID + ","+ value;
                }


                previousScreen(GetMaterialSelectionScreen.class,true);
                break;
            case R.id.customization_one_reset_txt:
                AppConstants.SEASONAL_ID = "";
                AppConstants.SEASONAL_NAME = "";

                AppConstants.PLACE_INDUSTRY_ID = "";
                AppConstants.PLACE_OF_INDUSTRY_NAME = "";

                AppConstants.THICKNESS_ID = "";
                AppConstants.THICKNESS_NAME = "";

                AppConstants.BRANDS_ID = "";
                AppConstants.BRANDS_NAME = "";

                AppConstants.MATERIAL_ID = "";
                AppConstants.MATERIAL_TYPE_NAME = "";

                AppConstants.COLOR_ID = "";
                AppConstants.SUB_COLOR_ID = "";
                AppConstants.COLOUR_NAME = "";

                AppConstants.SEASONAL_OLD_ID = "";
                AppConstants.SEASONAL_OLD_NAME = "";

                AppConstants.PLACE_INDUSTRY_OLD_ID = "";
                AppConstants.PLACE_OF_INDUSTRY_OLD_NAME = "";

                AppConstants.THICKNESS_OLD_ID = "";
                AppConstants.THICKNESS_OLD_NAME = "";

                AppConstants.BRANDS_OLD_ID = "";
                AppConstants.BRANDS_OLD_NAME = "";

                AppConstants.MATERIAL_OLD_ID = "";
                AppConstants.MATERIAL_TYPE_OLD_NAME = "";

                AppConstants.CUST_COLOR_ID = new HashMap<>();
//                AppConstants.CUST_COLOR_OLD_ID = new HashMap<>();
                previousScreen(GetMaterialSelectionScreen.class,true);
                break;
            case R.id.customization_one_cancel_txt:

                AppConstants.SEASONAL_ID = AppConstants.SEASONAL_OLD_ID ;
                AppConstants.SEASONAL_NAME =  AppConstants.SEASONAL_OLD_NAME;

                AppConstants.PLACE_INDUSTRY_ID =  AppConstants.PLACE_INDUSTRY_OLD_ID;
                AppConstants.PLACE_OF_INDUSTRY_NAME = AppConstants.PLACE_OF_INDUSTRY_OLD_NAME;

                AppConstants.THICKNESS_ID = AppConstants.THICKNESS_OLD_ID;
                AppConstants.THICKNESS_NAME = AppConstants.THICKNESS_OLD_NAME;

                AppConstants.BRANDS_ID =  AppConstants.BRANDS_OLD_ID ;
                AppConstants.BRANDS_NAME = AppConstants.BRANDS_OLD_NAME;

                AppConstants.MATERIAL_ID = AppConstants.MATERIAL_OLD_ID;
                AppConstants.MATERIAL_TYPE_NAME = AppConstants.MATERIAL_TYPE_OLD_NAME;

                AppConstants.CUST_COLOR_ID = new HashMap<>();

                if (!AppConstants.SUB_COLOR_ID.equalsIgnoreCase("")) {
                    List<String> items = Arrays.asList(AppConstants.SUB_COLOR_ID.split("\\s*,\\s*"));
                    List<String> item = Arrays.asList(AppConstants.COLOR_ID.split("\\s*,\\s*"));

                    for (int i=0 ;i<items.size(); i++){
                        AppConstants.CUST_COLOR_ID.put(items.get(i),item.get(i));
                    }
                }

//                AppConstants.CUST_COLOR_ID = AppConstants.CUST_COLOR_OLD_ID;
                previousScreen(GetMaterialSelectionScreen.class,true);
                break;
        }
    }

    public void getCustomizationApiCall(){
        if (NetworkUtil.isNetworkAvailable(CustomizationOneScreen.this)){

            ArrayList<ApicallidEntity> mOrginId = new ArrayList<>();
            ArrayList<ApicallidEntity> mSeasonalId = new ArrayList<>();
            CustomizationOneApiCallModal mModal = new CustomizationOneApiCallModal();


            if (AppConstants.PLACE_INDUSTRY_ID.equalsIgnoreCase("")){
                ApicallidEntity mOrginEntityId = new ApicallidEntity();

                mOrginEntityId.setId("1");

                mOrginId.add(mOrginEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.PLACE_INDUSTRY_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mOrginEntityId = new ApicallidEntity();

                    mOrginEntityId.setId(items.get(i));
                    mOrginId.add(mOrginEntityId);
                }

            }
            if (AppConstants.SEASONAL_ID.equalsIgnoreCase("")){
                ApicallidEntity mSeasonEntityId = new ApicallidEntity();

                mSeasonEntityId.setId("1");

                mSeasonalId.add(mSeasonEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.SEASONAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mSeasonEntityId = new ApicallidEntity();

                    mSeasonEntityId.setId(items.get(i));
                    mSeasonalId.add(mSeasonEntityId);
                }
            }

            mModal.setPlaceofOrginId(mOrginId);
            mModal.setSeasonId(mSeasonalId);

            APIRequestHandler.getInstance().getCustomizationOneApi(mModal,CustomizationOneScreen.this);

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationOneScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCustomizationApiCall();
                }
            });
        }
    }

    public void getNewCustomizationApiCall(){
        if (NetworkUtil.isNetworkAvailable(CustomizationOneScreen.this)){

            ArrayList<ApicallidEntity> mOrginId = new ArrayList<>();
            ArrayList<ApicallidEntity> mSeasonalId = new ArrayList<>();
            ArrayList<ApicallidEntity> mBrandId = new ArrayList<>();
            ArrayList<ApicallidEntity> mMaterialId = new ArrayList<>();
            ArrayList<ApicallidEntity> mColorId = new ArrayList<>();
            ArrayList<ApicallidEntity> mThicknessId = new ArrayList<>();

            CustomizationNewApiCallModal mModal = new CustomizationNewApiCallModal();

            if (AppConstants.PLACE_INDUSTRY_ID.equalsIgnoreCase("")){
                ApicallidEntity mOrginEntityId = new ApicallidEntity();

                mOrginEntityId.setId("0");

                mOrginId.add(mOrginEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.PLACE_INDUSTRY_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mOrginEntityId = new ApicallidEntity();

                    mOrginEntityId.setId(items.get(i));
                    mOrginId.add(mOrginEntityId);
                }

            }
            if (AppConstants.SEASONAL_ID.equalsIgnoreCase("")){
                ApicallidEntity mSeasonEntityId = new ApicallidEntity();

                mSeasonEntityId.setId("0");

                mSeasonalId.add(mSeasonEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.SEASONAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mSeasonEntityId = new ApicallidEntity();

                    mSeasonEntityId.setId(items.get(i));
                    mSeasonalId.add(mSeasonEntityId);
                }
            }

            if (AppConstants.BRANDS_ID.equalsIgnoreCase("")){
                ApicallidEntity mBrandEntityId = new ApicallidEntity();

                mBrandEntityId.setId("0");

                mBrandId.add(mBrandEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.BRANDS_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mBrandEntityId = new ApicallidEntity();

                    mBrandEntityId.setId(items.get(i));

                    mBrandId.add(mBrandEntityId);
                }

            }
            if (AppConstants.MATERIAL_ID.equalsIgnoreCase("")){
                ApicallidEntity mMaterialEntityId = new ApicallidEntity();

                mMaterialEntityId.setId("0");

                mMaterialId.add(mMaterialEntityId);


            }else {
                List<String> items = Arrays.asList(AppConstants.MATERIAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mMaterialEntityId = new ApicallidEntity();

                    mMaterialEntityId.setId(items.get(i));

                    mMaterialId.add(mMaterialEntityId);

                }

            }
            if (AppConstants.COLOR_ID.equalsIgnoreCase("")){
                ApicallidEntity mColorEntityId = new ApicallidEntity();

                mColorEntityId.setId("0");

                mColorId.add(mColorEntityId);

            }else {
//                List<String> items = Arrays.asList(AppConstants.COLOR_ID.split("\\s*,\\s*"));
//
//                for (int i=0 ;i<items.size(); i++){
//                    ApicallidEntity mColorEntityId = new ApicallidEntity();
//
//                    mColorEntityId.setId(items.get(i));
//
//                    mColorId.add(mColorEntityId);
//                }
                Set keys = AppConstants.CUST_COLOR_ID.keySet();
                Iterator itr = keys.iterator();

                String key;
                String value;
                while(itr.hasNext())
                {
                    key = (String) itr.next();
                    value = (String) AppConstants.CUST_COLOR_ID.get(key);

                    ApicallidEntity mColorEntityId = new ApicallidEntity();

                    mColorEntityId.setId(key);

                    mColorId.add(mColorEntityId);
                }
            }
            if (AppConstants.THICKNESS_ID.equalsIgnoreCase("")){
                ApicallidEntity mThicknessEntityId = new ApicallidEntity();

                mThicknessEntityId.setId("0");

                mThicknessId.add(mThicknessEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.THICKNESS_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mThicknessEntityId = new ApicallidEntity();

                    mThicknessEntityId.setId(items.get(i));

                    mThicknessId.add(mThicknessEntityId);
                }

            }

            mModal.setPlaceofOrginId(mOrginId);
            mModal.setSeasonId(mSeasonalId);
            mModal.setBrandId(mBrandId);
            mModal.setMaterialTypeId(mMaterialId);
            mModal.setColorId(mColorId);
            mModal.setThicknessId(mThicknessId);
            mModal.setDressType(AppConstants.SUB_DRESS_TYPE_ID);

            APIRequestHandler.getInstance().getNewCustomizationApi(mModal,CustomizationOneScreen.this);

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationOneScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getNewCustomizationApiCall();
                }
            });
        }
    }


    public void getNewFlowCustomizationNewApiCall(){
        if (NetworkUtil.isNetworkAvailable(CustomizationOneScreen.this)){

            ArrayList<ApicallidEntity> mOrginId = new ArrayList<>();
            ArrayList<ApicallidEntity> mSeasonalId = new ArrayList<>();
            ArrayList<ApicallidEntity> mBrandId = new ArrayList<>();
            ArrayList<ApicallidEntity> mMaterialId = new ArrayList<>();
            ArrayList<ApicallidEntity> mColorId = new ArrayList<>();
            ArrayList<ApicallidEntity> mThicknessId = new ArrayList<>();

            NewFlowCustomizationNewApiCallModal mModal = new NewFlowCustomizationNewApiCallModal();


            if (AppConstants.PLACE_INDUSTRY_ID.equalsIgnoreCase("")){
                ApicallidEntity mOrginEntityId = new ApicallidEntity();

                mOrginEntityId.setId("0");

                mOrginId.add(mOrginEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.PLACE_INDUSTRY_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mOrginEntityId = new ApicallidEntity();

                    mOrginEntityId.setId(items.get(i));
                    mOrginId.add(mOrginEntityId);
                }

            }
            if (AppConstants.SEASONAL_ID.equalsIgnoreCase("")){
                ApicallidEntity mSeasonEntityId = new ApicallidEntity();

                mSeasonEntityId.setId("0");

                mSeasonalId.add(mSeasonEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.SEASONAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mSeasonEntityId = new ApicallidEntity();

                    mSeasonEntityId.setId(items.get(i));
                    mSeasonalId.add(mSeasonEntityId);
                }
            }

            if (AppConstants.BRANDS_ID.equalsIgnoreCase("")){
                ApicallidEntity mBrandEntityId = new ApicallidEntity();

                mBrandEntityId.setId("0");

                mBrandId.add(mBrandEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.BRANDS_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mBrandEntityId = new ApicallidEntity();

                    mBrandEntityId.setId(items.get(i));

                    mBrandId.add(mBrandEntityId);
                }

            }
            if (AppConstants.MATERIAL_ID.equalsIgnoreCase("")){
                ApicallidEntity mMaterialEntityId = new ApicallidEntity();

                mMaterialEntityId.setId("0");

                mMaterialId.add(mMaterialEntityId);


            }else {
                List<String> items = Arrays.asList(AppConstants.MATERIAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mMaterialEntityId = new ApicallidEntity();

                    mMaterialEntityId.setId(items.get(i));

                    mMaterialId.add(mMaterialEntityId);

                }

            }
            if (AppConstants.COLOR_ID.equalsIgnoreCase("")){
                ApicallidEntity mColorEntityId = new ApicallidEntity();

                mColorEntityId.setId("0");

                mColorId.add(mColorEntityId);

            }else {
//                List<String> items = Arrays.asList(AppConstants.COLOR_ID.split("\\s*,\\s*"));
//
//                for (int i=0 ;i<items.size(); i++){
//                    ApicallidEntity mColorEntityId = new ApicallidEntity();
//
//                    mColorEntityId.setId(items.get(i));
//
//                    mColorId.add(mColorEntityId);
//                }
                Set keys = AppConstants.CUST_COLOR_ID.keySet();
                Iterator itr = keys.iterator();

                String key;
                String value;
                while(itr.hasNext())
                {
                    key = (String) itr.next();
                    value = (String) AppConstants.CUST_COLOR_ID.get(key);

                    ApicallidEntity mColorEntityId = new ApicallidEntity();

                    mColorEntityId.setId(key);

                    mColorId.add(mColorEntityId);
                }
            }
            if (AppConstants.THICKNESS_ID.equalsIgnoreCase("")){
                ApicallidEntity mThicknessEntityId = new ApicallidEntity();

                mThicknessEntityId.setId("0");

                mThicknessId.add(mThicknessEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.THICKNESS_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mThicknessEntityId = new ApicallidEntity();

                    mThicknessEntityId.setId(items.get(i));

                    mThicknessId.add(mThicknessEntityId);
                }
            }

            mModal.setPlaceofOrginId(mOrginId);
            mModal.setSeasonId(mSeasonalId);
            mModal.setBrandId(mBrandId);
            mModal.setMaterialTypeId(mMaterialId);
            mModal.setColorId(mColorId);
            mModal.setThicknessId(mThicknessId);
            mModal.setDressType(AppConstants.SUB_DRESS_TYPE_ID);
            mModal.setTailorId(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId());

            APIRequestHandler.getInstance().getNewFlowCustomizationNewApi(mModal,CustomizationOneScreen.this);

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationOneScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getNewFlowCustomizationNewApiCall();
                }
            });
        }
    }

    public void getNewFlowCustomizationApiCall(){
        if (NetworkUtil.isNetworkAvailable(CustomizationOneScreen.this)){

            ArrayList<ApicallidEntity> mOrginId = new ArrayList<>();
            ArrayList<ApicallidEntity> mSeasonalId = new ArrayList<>();
            NewFlowCustomizationOneApiCall mModal = new NewFlowCustomizationOneApiCall();


            if (AppConstants.PLACE_INDUSTRY_ID.equalsIgnoreCase("")){
                ApicallidEntity mOrginEntityId = new ApicallidEntity();

                mOrginEntityId.setId("1");

                mOrginId.add(mOrginEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.PLACE_INDUSTRY_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mOrginEntityId = new ApicallidEntity();

                    mOrginEntityId.setId(items.get(i));
                    mOrginId.add(mOrginEntityId);
                }

            }
            if (AppConstants.SEASONAL_ID.equalsIgnoreCase("")){
                ApicallidEntity mSeasonEntityId = new ApicallidEntity();

                mSeasonEntityId.setId("1");

                mSeasonalId.add(mSeasonEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.SEASONAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mSeasonEntityId = new ApicallidEntity();

                    mSeasonEntityId.setId(items.get(i));
                    mSeasonalId.add(mSeasonEntityId);
                }

            }
            mModal.setTailorId(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId());
            mModal.setPlaceofOrginId(mOrginId);
            mModal.setSeasonId(mSeasonalId);

            APIRequestHandler.getInstance().getNewFlowCustomizationOneApi(mModal,CustomizationOneScreen.this);

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationOneScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getNewFlowCustomizationApiCall();
                }
            });
        }
    }



    public void getSubColorApiCall(String ColorId){

        if (NetworkUtil.isNetworkAvailable(CustomizationOneScreen.this)){
            if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                APIRequestHandler.getInstance().getSubColorApiCall(ColorId,String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(0).getTailorId()),CustomizationOneScreen.this);

            }else {
                APIRequestHandler.getInstance().getSubColorApiCall(ColorId,"0",CustomizationOneScreen.this);

            }
        }else{
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationOneScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getSubColorApiCall(ColorId);
                }
            });
        }

    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.filter));
        mRightSideImg.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetCustomizationOneResponse){
            GetCustomizationOneResponse mResponse = (GetCustomizationOneResponse)resObj;

            setSeasonalAdapter(mResponse.getResult().getSeasons());
            setIndustryAdapter(mResponse.getResult().getPlaceofIndustrys());
            setBrandAdapter(mResponse.getResult().getMaterialBrand());
            setThicknessAdapter(mResponse.getResult().getThickness());

        }
        if (resObj instanceof GetNewCustomizationResponse){
            GetNewCustomizationResponse mResponse = (GetNewCustomizationResponse)resObj;

            setSeasonalAdapter(mResponse.getResult().getSeasons());
            setIndustryAdapter(mResponse.getResult().getPlaceofIndustrys());
            setBrandAdapter(mResponse.getResult().getMaterialBrand());
            setThicknessAdapter(mResponse.getResult().getThickness());
            setMaterialAdapter(mResponse.getResult().getMaterials());
            setColorAdapter(mResponse.getResult().getColors());
            mCutsomizationColorEntity = mResponse.getResult().getColors();

            mCustomizationOneAvailableCountTxt.setText(String.valueOf(AppConstants.MATERIAL_AVAILABLE_COUNT) + " " + getResources().getString(R.string.materials_available));
        }
        if (resObj instanceof SubColorResponse) {
            SubColorResponse mREsponse = (SubColorResponse) resObj;

            getSubColorDialog(mREsponse.getResult());
        }
    }



    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    /*Set Adapter for the Recycler view*/
    public void setSeasonalAdapter(ArrayList<CustomizationSessionEntity> mSeasonalList) {

        if (mSeasonalAdapter == null) {

            if (!AppConstants.SEASONAL_ID.equalsIgnoreCase("")){
                List<String> items = Arrays.asList(AppConstants.SEASONAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    for (int j=0; j<mSeasonalList.size(); j++){
                        if (mSeasonalList.get(j).getId() == Integer.parseInt(items.get(i))){
                            mSeasonalList.get(j).setChecked(true);
                        }
                    }
                }
                if (AppConstants.SEASONAL_NAME.equalsIgnoreCase("All Season")){
                    for (int j=0; j<mSeasonalList.size(); j++){
                            mSeasonalList.get(j).setChecked(true);

                    }
                }
            }

            mSeasonalAdapter = new CustomizationOneSessionAdapter(this,mSeasonalList);
            mSeasonalRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            mSeasonalRecyclerView.setAdapter(mSeasonalAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSeasonalAdapter.notifyDataSetChanged();
                }
            });
        }
    }
    /*Set Adapter for the Recycler view*/
    public void setIndustryAdapter(ArrayList<CustomizationPlaceEntity> mIndustryList) {

        if (mIndustryAdapter == null) {

            if (!AppConstants.PLACE_INDUSTRY_ID.equalsIgnoreCase("")){
                List<String> items = Arrays.asList(AppConstants.PLACE_INDUSTRY_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    for (int j=0; j<mIndustryList.size(); j++){
                        if (mIndustryList.get(j).getId() == Integer.parseInt(items.get(i))){
                            mIndustryList.get(j).setChecked(true);
                        }
                    }
                }
                if (AppConstants.PLACE_OF_INDUSTRY_NAME.equalsIgnoreCase("All Industry")){
                    for (int j=0; j<mIndustryList.size(); j++){
                            mIndustryList.get(j).setChecked(true);

                    }
                }
            }

            mIndustryAdapter = new CustomizationOnePlaceAdapter(this,mIndustryList);
            mIndustryRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            mIndustryRecyclerView.setAdapter(mIndustryAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mIndustryAdapter.notifyDataSetChanged();
                }
            });
        }

    }
    /*Set Adapter for the Recycler view*/
    public void setBrandAdapter(ArrayList<CustomizationBrandEntity> mBrandList) {

        mBrandRecyclerView.setVisibility(mBrandList.size()>0 ? View.VISIBLE : View.GONE);
        mCustomizeBrandEmptyTxt.setVisibility(mBrandList.size() > 0 ? View.GONE : View.VISIBLE);

        if (!AppConstants.BRANDS_ID.equalsIgnoreCase("")){
            List<String> items = Arrays.asList(AppConstants.BRANDS_ID.split("\\s*,\\s*"));
            for (int i=0 ;i<items.size(); i++){
                for (int j=0; j<mBrandList.size(); j++){
                    if (mBrandList.get(j).getId() == Integer.parseInt(items.get(i))){
                        mBrandList.get(j).setChecked(true);
                    }
                }
            }
        }

            mBrandAdapter = new CustomizationOneBrandAdapter(this,mBrandList);
            mBrandRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            mBrandRecyclerView.setAdapter(mBrandAdapter);
    }

    /*Set Adapter for the Recycler view*/
    public void setThicknessAdapter(ArrayList<CustomizationThicknessEntity> mThicknessList) {

        mCustomizationThicknessRecyclerView.setVisibility(mThicknessList.size()>0 ? View.VISIBLE : View.GONE);
        mThicknessEmptyListTxt.setVisibility(mThicknessList.size() > 0 ? View.GONE : View.VISIBLE);

        if (!AppConstants.THICKNESS_ID.equalsIgnoreCase("")){
            List<String> items = Arrays.asList(AppConstants.THICKNESS_ID.split("\\s*,\\s*"));

            for (int i=0 ;i<items.size(); i++) {
                for (int j=0; j<mThicknessList.size(); j++){
                    if (mThicknessList.get(j).getId() == Integer.parseInt(items.get(i))){
                        mThicknessList.get(j).setChecked(true);
                    }
                }
            }
        }

        mThicknessAdapter = new CustomizationOneThicknessAdapter(this,mThicknessList);
        mCustomizationThicknessRecyclerView.setLayoutManager(new GridLayoutManager(this,2,GridLayoutManager.HORIZONTAL,false));
        mCustomizationThicknessRecyclerView.setAdapter(mThicknessAdapter);
    }

    /*Set Adapter for the Recycler view*/
    public void setMaterialAdapter(ArrayList<CustomizationMaterialsEntity> mMaterialList) {

        if (mMaterialAdapter == null) {

            if (!AppConstants.MATERIAL_ID.equalsIgnoreCase("")){
                List<String> items = Arrays.asList(AppConstants.MATERIAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    for (int j=0; j<mMaterialList.size(); j++){
                        if (mMaterialList.get(j).getId() == Integer.parseInt(items.get(i))){
                            mMaterialList.get(j).setChecked(true);
                        }
                    }
                }
                if (AppConstants.MATERIAL_TYPE_NAME.equalsIgnoreCase("all material type")){
                    for (int j=0; j<mMaterialList.size(); j++){
                        mMaterialList.get(j).setChecked(true);
                    }
                }
                if (AppConstants.MATERIAL_TYPE_NAME.equalsIgnoreCase("All Material")){
                    for (int j=0; j<mMaterialList.size(); j++){
                        mMaterialList.get(j).setChecked(true);
                    }
                }
            }

            mMaterialAdapter = new CustomizationTwoMaterialAdapter(this,mMaterialList);
            mMaterialRecyclerView.setLayoutManager(new GridLayoutManager(this,2,GridLayoutManager.HORIZONTAL, false));
            mMaterialRecyclerView.setAdapter(mMaterialAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mMaterialAdapter.notifyDataSetChanged();
                }
            });
        }
    }
    /*Set Adapter for the Recycler view*/
    public void setColorAdapter(ArrayList<CustomizationColorsEntity> mColorList) {

        for (int i=0; i < mColorList.size(); i++) {
            mColorList.get(i).setChecked(false);
        }

            mColorIdList = new ArrayList<>();

        Set keys = AppConstants.CUST_COLOR_ID.keySet();
        Iterator itr = keys.iterator();

        String key;
        String value;
        while(itr.hasNext())
        {
            key = (String) itr.next();
            value = (String) AppConstants.CUST_COLOR_ID.get(key);
            mColorIdList.add(value);
        }

        for (int i=0; i < mColorList.size(); i++){
            for (int j=0; j < mColorIdList.size(); j++){
                if (mColorList.get(i).getId() == Integer.parseInt(mColorIdList.get(j))){
                    mColorList.get(i).setChecked(true);
                }
            }
        }

            mColorAdapter = new CustomizationTwoColorAdapter(this,mColorList);
            mColorRecyclerView.setLayoutManager(new GridLayoutManager(this,2,GridLayoutManager.HORIZONTAL, false));
            mColorRecyclerView.setAdapter(mColorAdapter);
    }


    public void getSubColorDialog(ArrayList<SubColorEntity> result){

//        if (!AppConstants.COLOR_ID.equalsIgnoreCase("")){
//            List<String> items = Arrays.asList(AppConstants.COLOR_ID.split("\\s*,\\s*"));
//
//            for (int i=0 ;i<items.size(); i++){
//                for(int j=0; j<result.size(); j++){
//                    if (result.get(j).getId() == Integer.parseInt(items.get(i))){
//                        result.get(j).setChecked(true);
//                    }
//                }
//            }
//        }

        mSubColorIdList = new ArrayList<>();
        Set keys = AppConstants.CUST_COLOR_ID.keySet();
        Iterator itr = keys.iterator();

        String key;
        String value;
        while(itr.hasNext())
        {
            key = (String) itr.next();
            value = (String) AppConstants.CUST_COLOR_ID.get(key);
            mSubColorIdList.add(key);
        }

        for (int i=0 ;i<mSubColorIdList.size(); i++){
            for(int j=0; j<result.size(); j++){
                if (result.get(j).getId() == Integer.parseInt(mSubColorIdList.get(i))){
                    result.get(j).setChecked(true);
                }
            }
        }

        alertDismiss(mSubColorDialog);
        mSubColorDialog = getDialog(CustomizationOneScreen.this, R.layout.pop_up_customization_color);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mSubColorDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mSubColorDialog.findViewById(R.id.pop_up_customization_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(mSubColorDialog.findViewById(R.id.pop_up_customization_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        Button mDoneBtn;
        RecyclerView mSubColorRecyclerView;
        TextView mEmptyTxt;

        /*Init view*/
        mDoneBtn = mSubColorDialog.findViewById(R.id.pop_up_done_btn);
        mSubColorRecyclerView = mSubColorDialog.findViewById(R.id.pop_up_color_recycler_view);
        mEmptyTxt = mSubColorDialog.findViewById(R.id.pop_up_sub_clr_txt);

        mEmptyTxt.setVisibility(result.size() > 0 ? View.GONE : View.VISIBLE);
        mSubColorRecyclerView.setVisibility(result.size() > 0 ? View.VISIBLE : View.GONE);

        mSubColorAdapter = new CustomizationTwoSubColorAdapter(this, result);
        mSubColorRecyclerView.setLayoutManager(new GridLayoutManager(this,5 ,GridLayoutManager.VERTICAL, false));
        mSubColorRecyclerView.setAdapter(mSubColorAdapter);

        mDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mColorIdList = new ArrayList<>();
//
//                Set keys = AppConstants.CUST_COLOR_ID.keySet();
//                Iterator itr = keys.iterator();
//
//                String key;
//                String value;
//                while(itr.hasNext())
//                {
//                    key = (String) itr.next();
//                    value = (String) AppConstants.CUST_COLOR_ID.get(key);
//                    mColorIdList.add(value);
//                }
//
//                for (int i=0; i < mCutsomizationColorEntity.size(); i++){
//                    for (int j=0; j < mColorIdList.size(); j++){
//                        if (mCutsomizationColorEntity.get(i).getId() == Integer.parseInt(mColorIdList.get(j))){
//                            mCutsomizationColorEntity.get(i).setChecked(true);
//                        }
//                    }
//                }

                setColorAdapter(mCutsomizationColorEntity);

                mSubColorDialog.dismiss();
            }
        });

        alertShowing(mSubColorDialog);
    }


    @Override
    public void onBackPressed() {

        AppConstants.SEASONAL_ID = AppConstants.SEASONAL_OLD_ID ;
        AppConstants.SEASONAL_NAME =  AppConstants.SEASONAL_OLD_NAME;

        AppConstants.PLACE_INDUSTRY_ID =  AppConstants.PLACE_INDUSTRY_OLD_ID;
        AppConstants.PLACE_OF_INDUSTRY_NAME = AppConstants.PLACE_OF_INDUSTRY_OLD_NAME;

        AppConstants.THICKNESS_ID = AppConstants.THICKNESS_OLD_ID;
        AppConstants.THICKNESS_NAME = AppConstants.THICKNESS_OLD_NAME;

        AppConstants.BRANDS_ID =  AppConstants.BRANDS_OLD_ID ;
        AppConstants.BRANDS_NAME = AppConstants.BRANDS_OLD_NAME;

        AppConstants.MATERIAL_ID = AppConstants.MATERIAL_OLD_ID;
        AppConstants.MATERIAL_TYPE_NAME = AppConstants.MATERIAL_TYPE_OLD_NAME;
        AppConstants.CUST_COLOR_ID = new HashMap<>();

        if (!AppConstants.SUB_COLOR_ID.equalsIgnoreCase("")){
            List<String> items = Arrays.asList(AppConstants.SUB_COLOR_ID.split("\\s*,\\s*"));
            List<String> item = Arrays.asList(AppConstants.COLOR_ID.split("\\s*,\\s*"));

            for (int i=0 ;i<items.size(); i++){
                AppConstants.CUST_COLOR_ID.put(items.get(i),item.get(i));
            }
        }

//        AppConstants.CUST_COLOR_ID = AppConstants.CUST_COLOR_OLD_ID;

        super.onBackPressed();

        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.customization_one_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.customization_one_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
}
