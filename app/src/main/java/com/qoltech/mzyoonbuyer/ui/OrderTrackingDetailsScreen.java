package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.GetTrackingDetailsAdapter;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetTrackingResponse;
import com.qoltech.mzyoonbuyer.modal.TrackingFieldModal;
import com.qoltech.mzyoonbuyer.service.APICommonInterface;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderTrackingDetailsScreen extends BaseActivity {

    @BindView(R.id.order_tracking_details_pay_lay)
    LinearLayout mOrderTrackingDetailsPayLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.order_placed_date_txt)
    TextView mOrderPlacedDateTxt;

    @BindView(R.id.order_placed_txt)
    TextView mOrderPlacedTxt;

//    @BindView(R.id.order_placed_time_txt)
//    TextView mOrderPlacedTimeTxt;

    @BindView(R.id.material_received_date_txt)
    TextView mMaterialReceivedDateTxt;

    @BindView(R.id.material_received_txt)
    TextView mMaterialReceivedTxt;

//    @BindView(R.id.material_received_time_txt)
//    TextView mMaterialReceivedTimeTxt;

    @BindView(R.id.material_received_dotted_img)
    ImageView mMaterialReceivedDottedImg;

    @BindView(R.id.material_received_img)
    ImageView mMaterialReceivedImg;

    @BindView(R.id.measurement_done_date_txt)
    TextView mMeasurementDoneDateTxt;

    @BindView(R.id.measurement_done_txt)
    TextView mMeasurementDoneTxt;

//    @BindView(R.id.measurement_done_time_txt)
//    TextView mMeasurementDoneTimeTxt;

    @BindView(R.id.measurement_done_dotted_img)
    ImageView mMeasurementDoneDottedImg;

    @BindView(R.id.measurement_done_img)
    ImageView mMeasurementDoneImg;

    @BindView(R.id.stitching_started_date_txt)
    TextView mStitchingStartedDateTxt;

    @BindView(R.id.stitching_started_txt)
    TextView mStitchingStartedTxt;

//    @BindView(R.id.stitching_started_time_txt)
//    TextView mStitchingStartedTimeTxt;

    @BindView(R.id.stitching_started_dotted_img)
    ImageView mStitchingStartedDottedImg;

    @BindView(R.id.stitching_started_img)
    ImageView mStitchingStartedImg;

    @BindView(R.id.ready_to_dispact_date_txt)
    TextView mReadyToDispactDateTxt;

    @BindView(R.id.ready_for_dispatch_txt)
    TextView mReadyToDispactTxt;

//    @BindView(R.id.ready_to_dispact_time_txt)
//    TextView mReadyToDispactTimeTxt;

    @BindView(R.id.ready_to_dispact_dotted_img)
    ImageView mReadyToDispactDottedImg;

    @BindView(R.id.ready_to_dispact_img)
    ImageView mReadyToDispactImg;

    @BindView(R.id.dispacted_date_txt)
    TextView mDispactedDateTxt;

    @BindView(R.id.dispacted_txt)
    TextView mDispactedTxt;

//    @BindView(R.id.dispacted_time_txt)
//    TextView mDispactedTimeTxt;

    @BindView(R.id.dispacted_dotted_img)
    ImageView mDispactedDottedImg;

    @BindView(R.id.dispacted_img)
    ImageView mDispactedImg;

    @BindView(R.id.out_of_delivery_date_txt)
    TextView mOutOfDeliveryDateTxt;

    @BindView(R.id.out_of_delivery_txt)
    TextView mOutOfDeliveryTxt;

//    @BindView(R.id.out_of_delivery_time_txt)
//    TextView mOutOfDeliveryTimeTxt;

    @BindView(R.id.out_of_delivery_dotted_img)
    ImageView mOutOfDeliveryDottedImg;

    @BindView(R.id.out_of_delivery_img)
    ImageView mOutOfDeliveryImg;

    @BindView(R.id.delivered_date_txt)
    TextView mDeliveredDateTxt;

    @BindView(R.id.delivered_txt)
    TextView mDeliveredTxt;

//    @BindView(R.id.delivered_time_txt)
//    TextView mDelivertedTimeTxt;

    @BindView(R.id.delivered_dotted_img)
    ImageView mDeliveredDottedImg;

    @BindView(R.id.delivered_img)
    ImageView mDelivertedImg;

    APICommonInterface mApiCommonInterface;

    private UserDetailsEntity mUserDetailsEntityRes;

    private GetTrackingDetailsAdapter mGetTrackingAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_order_tracking_details_screen);
        initView();
    }
    public void initView(){

        ButterKnife.bind(this);

        setupUI(mOrderTrackingDetailsPayLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        mApiCommonInterface = APIRequestHandler.getClient().create(APICommonInterface.class);

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(OrderTrackingDetailsScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        getStitchingTrackingFieldApiCall();
        getTreackingDetailsApiCall();

        setHeader();

    }

    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.tracking_details));
        mRightSideImg.setVisibility(View.INVISIBLE);

    }

    public void getTreackingDetailsApiCall(){
        if (NetworkUtil.isNetworkAvailable(OrderTrackingDetailsScreen.this)){
            APIRequestHandler.getInstance().getTrackingDetailsApiCall(AppConstants.ORDER_PENDING_ID,OrderTrackingDetailsScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderTrackingDetailsScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getTreackingDetailsApiCall();
                }
            });
        }
    }

    public void getStitchingTrackingFieldApiCall(){
        if (NetworkUtil.isNetworkAvailable(OrderTrackingDetailsScreen.this)){
            APIRequestHandler.getInstance().getStitchingTrackingFieldApi(this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStitchingTrackingFieldApiCall();
                }
            });
        }
    }


    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.order_tracking_details_pay_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.order_tracking_details_pay_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetTrackingResponse){
            GetTrackingResponse mResponse = (GetTrackingResponse)resObj;

            for (int i=0; i<mResponse.getResult().size(); i++){
                if (i == 0){
                    mOrderPlacedDateTxt.setText(mResponse.getResult().get(i).getDate());
                }else if (i == 1){
                    mMaterialReceivedDateTxt.setText(mResponse.getResult().get(i).getDate());
                    mMaterialReceivedDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
                    mMaterialReceivedImg.setBackgroundResource(R.drawable.material_received_blue_img);

                }else if (i == 2){
                    mMeasurementDoneDateTxt.setText(mResponse.getResult().get(i).getDate());
                    mMeasurementDoneDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
                    mMeasurementDoneImg.setBackgroundResource(R.drawable.measurement_done_blue_img);

                }else if (i == 3){
                    mStitchingStartedDateTxt.setText(mResponse.getResult().get(i).getDate());
                    mStitchingStartedDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
                    mStitchingStartedImg.setBackgroundResource(R.drawable.stitching_started_blue_img);

                }else if (i == 4){
                    mReadyToDispactDateTxt.setText(mResponse.getResult().get(i).getDate());
                    mReadyToDispactDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
                    mReadyToDispactImg.setBackgroundResource(R.drawable.ready_for_dispatching_blue_img);

                }else if (i == 5){
                    mDispactedDateTxt.setText(mResponse.getResult().get(i).getDate());
                    mDispactedDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
                    mDispactedImg.setBackgroundResource(R.drawable.dispatched_blue_img);

                }else if (i == 6){
                    mOutOfDeliveryDateTxt.setText(mResponse.getResult().get(i).getDate());
                    mOutOfDeliveryDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
                    mOutOfDeliveryImg.setBackgroundResource(R.drawable.out_of_delivery_blue_img);

                }else if (i == 7){
                    mDeliveredDateTxt.setText(mResponse.getResult().get(i).getDate());
                    mDeliveredDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
                    mDelivertedImg.setBackgroundResource(R.drawable.delivered_blue_img);

                }
            }
        }
        if (resObj instanceof TrackingFieldModal){
            TrackingFieldModal mResponse = (TrackingFieldModal)resObj;
            for (int i=0; i<mResponse.getResult().size(); i++){
                if (i == 0){
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        mOrderPlacedTxt.setText(mResponse.getResult().get(i).getStatusInArabic());
                    }else {
                        mOrderPlacedTxt.setText(mResponse.getResult().get(i).getStatusInEnglish());
                    }
                }else if (i == 1){
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        mMaterialReceivedTxt.setText(mResponse.getResult().get(i).getStatusInArabic());
                    }else {
                        mMaterialReceivedTxt.setText(mResponse.getResult().get(i).getStatusInEnglish());
                    }
                }else if (i == 2){
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        mMeasurementDoneTxt.setText(mResponse.getResult().get(i).getStatusInArabic());
                    }else {
                        mMeasurementDoneTxt.setText(mResponse.getResult().get(i).getStatusInEnglish());
                    }
                }else if (i == 3){
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        mStitchingStartedTxt.setText(mResponse.getResult().get(i).getStatusInArabic());
                    }else {
                        mStitchingStartedTxt.setText(mResponse.getResult().get(i).getStatusInEnglish());
                    }
                }else if (i == 4){
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        mReadyToDispactTxt.setText(mResponse.getResult().get(i).getStatusInArabic());
                    }else {
                        mReadyToDispactTxt.setText(mResponse.getResult().get(i).getStatusInEnglish());
                    }
                }else if (i == 5){
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        mDispactedTxt.setText(mResponse.getResult().get(i).getStatusInArabic());
                    }else {
                        mDispactedTxt.setText(mResponse.getResult().get(i).getStatusInEnglish());
                    }
                }else if (i == 6){
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        mOutOfDeliveryTxt.setText(mResponse.getResult().get(i).getStatusInArabic());
                    }else {
                        mOutOfDeliveryTxt.setText(mResponse.getResult().get(i).getStatusInEnglish());
                    }
                }else if (i == 7){
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        mDeliveredTxt.setText(mResponse.getResult().get(i).getStatusInArabic());
                    }else {
                        mDeliveredTxt.setText(mResponse.getResult().get(i).getStatusInEnglish());
                    }
                }
            }
        }
//        if (resObj instanceof GetStoreTrackingResponse){
//            GetStoreTrackingResponse mResponse = (GetStoreTrackingResponse)resObj;
//
//            for (int i=0; i<mResponse.getGetTracking().size(); i++){
//                if (i == 0){
//                    mOrderPlacedDateTxt.setText(mResponse.getGetTracking().get(i).getDate());
//                }else if (i == 1){
//                    mMaterialReceivedDateTxt.setText(mResponse.getGetTracking().get(i).getDate());
//                    mMaterialReceivedDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
//                    mMaterialReceivedImg.setBackgroundResource(R.drawable.material_received_blue_img);
//
//                }else if (i == 2){
//                    mMeasurementDoneDateTxt.setText(mResponse.getGetTracking().get(i).getDate());
//                    mMeasurementDoneDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
//                    mMeasurementDoneImg.setBackgroundResource(R.drawable.measurement_done_blue_img);
//
//                }else if (i == 3){
//                    mStitchingStartedDateTxt.setText(mResponse.getGetTracking().get(i).getDate());
//                    mStitchingStartedDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
//                    mStitchingStartedImg.setBackgroundResource(R.drawable.stitching_started_blue_img);
//
//                }else if (i == 4){
//                    mReadyToDispactDateTxt.setText(mResponse.getGetTracking().get(i).getDate());
//                    mReadyToDispactDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
//                    mReadyToDispactImg.setBackgroundResource(R.drawable.ready_for_dispatching_blue_img);
//
//                }else if (i == 5){
//                    mDispactedDateTxt.setText(mResponse.getGetTracking().get(i).getDate());
//                    mDispactedDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
//                    mDispactedImg.setBackgroundResource(R.drawable.dispatched_blue_img);
//
//                }else if (i == 6){
//                    mOutOfDeliveryDateTxt.setText(mResponse.getGetTracking().get(i).getDate());
//                    mOutOfDeliveryDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
//                    mOutOfDeliveryImg.setBackgroundResource(R.drawable.out_of_delivery_blue_img);
//
//                }else if (i == 7){
//                    mDeliveredDateTxt.setText(mResponse.getGetTracking().get(i).getDate());
//                    mDeliveredDottedImg.setBackgroundResource(R.drawable.tracking_blue_img);
//                    mDelivertedImg.setBackgroundResource(R.drawable.delivered_blue_img);
//
//                }
//            }
//        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
