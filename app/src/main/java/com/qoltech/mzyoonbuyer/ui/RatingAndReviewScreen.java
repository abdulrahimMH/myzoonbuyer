package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.InserRatingApicallEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetRatingResponse;
import com.qoltech.mzyoonbuyer.modal.InsertMaterialRatingResponse;
import com.qoltech.mzyoonbuyer.modal.InsertRatingApicallModal;
import com.qoltech.mzyoonbuyer.modal.InsertRatingResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RatingAndReviewScreen extends BaseActivity {

    @BindView(R.id.rating_and_review_par_lay)
    LinearLayout mRatingAndReviewScreenParLay;

    @BindView(R.id.material_rating_review_par_lay)
    LinearLayout mMaterialRatingReviewParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.tailor_service_rating_bar)
    RatingBar mRatingReviewRatingBar;

    @BindView(R.id.tailor_stching_rating_bar)
    RatingBar mRatingStichingRatingBar;

    @BindView(R.id.tailor_cusomter_rating_bar)
    RatingBar mRatingCustomerRatingBar;

    @BindView(R.id.material_rating_bar)
    RatingBar mMaterialRatingBar;

    @BindView(R.id.material_rating_edt_Txt)
    EditText mMaterialRatingEdtTxt;

    @BindView(R.id.rating_reive_edt_Txt)
    EditText mRatingReviewEdtTxt;

    @BindView(R.id.rating_and_review_submit_btn)
    Button mSubmitBtn;

    private UserDetailsEntity mUserDetailsEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_rating_and_review_screen);
        initView();
    }
    public void initView(){
        ButterKnife.bind(this);

        setupUI(mRatingAndReviewScreenParLay);

        setHeader();

        getLanguage();

        getGetRatingApiCall();

    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.rating_and_write));
        mRightSideImg.setVisibility(View.INVISIBLE);

        if (AppConstants.MATERIAL_TYPE.equalsIgnoreCase("3")){
            mMaterialRatingReviewParLay.setVisibility(View.VISIBLE);
        }else {
            mMaterialRatingReviewParLay.setVisibility(View.GONE);
        }
    }
    @OnClick({R.id.header_left_side_img,R.id.rating_and_review_submit_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.rating_and_review_submit_btn:
                if (AppConstants.MATERIAL_TYPE.equalsIgnoreCase("3")){
                    if (mRatingReviewEdtTxt.getText().toString().trim().equalsIgnoreCase("")){
                        mRatingReviewEdtTxt.clearAnimation();
                        mRatingReviewEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                        mRatingReviewEdtTxt.setError(getResources().getString(R.string.please_give_feedback));
                    }else if (mMaterialRatingEdtTxt.getText().toString().trim().equalsIgnoreCase("")){
                        mMaterialRatingEdtTxt.clearAnimation();
                        mMaterialRatingEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                        mMaterialRatingEdtTxt.setError(getResources().getString(R.string.please_give_feedback));
                    }
                    else {
                        insertRatingApiCall();
                        insertMaterialRatingApiCall();
                    }
                }else {
                    if (mRatingReviewEdtTxt.getText().toString().trim().equalsIgnoreCase("")){
                        mRatingReviewEdtTxt.clearAnimation();
                        mRatingReviewEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                        mRatingReviewEdtTxt.setError(getResources().getString(R.string.please_give_feedback));

                    }else {
                        insertRatingApiCall();

                    }
                }
                break;
        }

    }
    public void getLanguage(){
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(RatingAndReviewScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntity = gson.fromJson(json, UserDetailsEntity.class);

        if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.rating_and_review_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.rating_and_review_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
        }
    }
    public void insertRatingApiCall(){
        if (NetworkUtil.isNetworkAvailable(RatingAndReviewScreen.this)){
            InsertRatingApicallModal mRespone = new InsertRatingApicallModal();
            mRespone.setOrderId(AppConstants.ORDER_PENDING_ID);
            mRespone.setReview(mRatingReviewEdtTxt.getText().toString().replaceAll("(?m)^[ \t]*\r?\n", ""));
            mRespone.setTailorId(String.valueOf(AppConstants.TAILOR_CLICK_ID));

            ArrayList<InserRatingApicallEntity> inserRatingApicallEntities = new ArrayList<>();

            for (int i=0; i<=3;i++){
                InserRatingApicallEntity inserRatingApicallEntity = new InserRatingApicallEntity();
                if (i==1){
                    inserRatingApicallEntity.setId(String.valueOf(i));
                    inserRatingApicallEntity.setRating(String.valueOf(mRatingReviewRatingBar.getRating()));
                    inserRatingApicallEntities.add(inserRatingApicallEntity);

                }
                if (i==2){
                    inserRatingApicallEntity.setId(String.valueOf(i));
                    inserRatingApicallEntity.setRating(String.valueOf(mRatingStichingRatingBar.getRating()));
                    inserRatingApicallEntities.add(inserRatingApicallEntity);

                }
                if (i==3){
                    inserRatingApicallEntity.setId(String.valueOf(i));
                    inserRatingApicallEntity.setRating(String.valueOf(mRatingCustomerRatingBar.getRating()));
                    inserRatingApicallEntities.add(inserRatingApicallEntity);
                }
            }
            mRespone.setCategory(inserRatingApicallEntities);

            APIRequestHandler.getInstance().insertRatingApiCall(mRespone,RatingAndReviewScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(RatingAndReviewScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    insertRatingApiCall();
                }
            });
        }
    }

    public void insertMaterialRatingApiCall(){
        if (NetworkUtil.isNetworkAvailable(RatingAndReviewScreen.this)){
            APIRequestHandler.getInstance().insertMaterialRatingApiCall(AppConstants.ORDER_PENDING_ID,AppConstants.MATERIAL_TYPE_ID,String.valueOf(mMaterialRatingBar.getRating()),mMaterialRatingEdtTxt.getText().toString(),RatingAndReviewScreen.this);

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(RatingAndReviewScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    insertMaterialRatingApiCall();
                }
            });
        }
    }

    public void getGetRatingApiCall(){
        if (NetworkUtil.isNetworkAvailable(RatingAndReviewScreen.this)){
            APIRequestHandler.getInstance().getRatingApiCall(String.valueOf(AppConstants.TAILOR_CLICK_ID),AppConstants.ORDER_PENDING_ID,"Order",RatingAndReviewScreen.this);
        }else {
            getGetRatingApiCall();
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof InsertRatingResponse){
            InsertRatingResponse mRespone = (InsertRatingResponse)resObj;
            DialogManager.getInstance().showAlertPopup(RatingAndReviewScreen.this, getResources().getString(R.string.thank_you_for_your_feedback), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    onBackPressed();
                }
            });

//            DialogManager.getInstance().showAlertPopup(RatingAndReviewScreen.this, mRespone.getResult(), new InterfaceBtnCallBack() {
//                @Override
//                public void onPositiveClick() {
//
//                }
//            });
        }
        if (resObj instanceof InsertMaterialRatingResponse){
            InsertMaterialRatingResponse mREsponse = (InsertMaterialRatingResponse)resObj;
        }

        if (resObj instanceof GetRatingResponse){
            GetRatingResponse mResponse= (GetRatingResponse)resObj;
            if (mResponse.getResult().getCustomerRating().size()>0){
                mRatingReviewEdtTxt.setText(String.valueOf(mResponse.getResult().getCustomerRating().get(0).getReview()));
                mSubmitBtn.setVisibility(View.GONE);
                mRatingReviewEdtTxt.setEnabled(false);
                mRatingReviewRatingBar.setIsIndicator(true);
                mRatingStichingRatingBar.setIsIndicator(true);
                mRatingCustomerRatingBar.setIsIndicator(true);
                mMaterialRatingBar.setIsIndicator(true);
                mMaterialRatingEdtTxt.setEnabled(false);
            }

            if (mResponse.getResult().getRatings().size()>0){
                NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                DecimalFormat formatter = (DecimalFormat)nf;
                formatter.applyPattern("######.#");
                for (int i=0; i<mResponse.getResult().getRatings().size(); i++){
                    if (i == 0){
                        mRatingReviewRatingBar.setRating(Float.parseFloat(String.valueOf(mResponse.getResult().getRatings().get(0).getRating())));
                    }
                    else if (i == 1){
                        mRatingStichingRatingBar.setRating(Float.parseFloat(String.valueOf(mResponse.getResult().getRatings().get(1).getRating())));
                    }
                    else if (i == 2){
                        mRatingCustomerRatingBar.setRating(Float.parseFloat(String.valueOf(mResponse.getResult().getRatings().get(2).getRating())));
                    }
                }

            }
            if (mResponse.getResult().getMaterialReview().size() > 0){
                mMaterialRatingEdtTxt.setText(mResponse.getResult().getMaterialReview().get(0).getReview());
            }

            if (mResponse.getResult().getMaterialRating().size() > 0){
                mMaterialRatingBar.setRating(Float.parseFloat(String.valueOf(mResponse.getResult().getMaterialRating().get(0).getRating())));

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
