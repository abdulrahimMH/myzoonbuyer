package com.qoltech.mzyoonbuyer.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.GetCheckoutProductAdapter;
import com.qoltech.mzyoonbuyer.adapter.GetItemCardAdapter;
import com.qoltech.mzyoonbuyer.entity.IdEntity;
import com.qoltech.mzyoonbuyer.entity.InsertWebOrderDetailsEntity;
import com.qoltech.mzyoonbuyer.entity.InsertWebOrderEntity;
import com.qoltech.mzyoonbuyer.entity.PaymentInfoEntity;
import com.qoltech.mzyoonbuyer.entity.StoreCartEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.CheckoutDeliveryChargesModal;
import com.qoltech.mzyoonbuyer.modal.CheckoutDeliveryChargesResponse;
import com.qoltech.mzyoonbuyer.modal.CheckoutResponse;
import com.qoltech.mzyoonbuyer.modal.GetBuyerAddressModal;
import com.qoltech.mzyoonbuyer.modal.GetCouponPriceResponse;
import com.qoltech.mzyoonbuyer.modal.InsertWebOrderBody;
import com.qoltech.mzyoonbuyer.modal.WebOrderResponse;
import com.qoltech.mzyoonbuyer.service.APICommonInterface;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class CheckoutScreen extends BaseActivity {

    @BindView(R.id.check_out_par_lay)
    RelativeLayout mCheckOutParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.check_out_total_amt_txt)
    TextView mCheckOurTotalAmtTxt;

    @BindView(R.id.check_out_promo_code_edt_txt)
    EditText mCheckOutPromoCodeEdtTxt;

    @BindView(R.id.checkout_promo_code_apply_lay)
    RelativeLayout mCheckOutPromoCodeApplyLay;

    @BindView(R.id.checkout_shipping_address_txt)
    TextView mCheckoutShippingAddressTxt;

    @BindView(R.id.checkout_dress_img)
    ImageView mCheckOutDressImg;

    @BindView(R.id.check_out_dress_name_txt)
    TextView mCheckOutDressNameTxt;

    @BindView(R.id.check_out_qty_txt)
    TextView mCheckOutQtyTxt;

    @BindView(R.id.check_out_Price_txt)
    TextView mCheckOutPriceTxt;

    @BindView(R.id.cheout_sub_total_lay)
    RelativeLayout mCheckOutSubTotalLay;

    @BindView(R.id.check_out_sub_total_txt)
    TextView mCheckOutSubTotalTxt;

    @BindView(R.id.cheout_redeem_amt_lay)
    RelativeLayout mCheckOutRedeemAmtLay;

    @BindView(R.id.check_out_redeem_amt_txt)
    TextView mCheckOutRedeemAmtTxt;

    @BindView(R.id.cheout_promo_code_amt_lay)
    RelativeLayout mCheckOutPromoCodeAmtLay;

    @BindView(R.id.check_out_promo_code_amt_txt)
    TextView mCheckOutPromoCodeAmtTxt;

    @BindView(R.id.get_store_product_rec_lay)
    RecyclerView mGetStoreProductRecLay;

    @BindView(R.id.product_added_from_store_txt)
    TextView mProductAddedFromStoreTxt;

    @BindView(R.id.check_out_recycler_view)
    RecyclerView mCheckOutRecyclerView;

    @BindView(R.id.check_out_screen_your_order_txt)
            TextView mCheckOutScreenYourOrderTxt;

    @BindView(R.id.check_out_screen_your_order_lay)
    CardView mCheckOutScreenYourOrderLay;

    @BindView(R.id.cheout_store_total_lay)
            RelativeLayout mCheckOutStoreTotalLay;

    @BindView(R.id.check_out_store_total_txt)
            TextView mCheckOutStoreTotalTxt;

    @BindView(R.id.check_out_stitching_lay)
            RelativeLayout mCheckOutStitchingLay;

    @BindView(R.id.check_out_stitching_amt_txt)
            TextView mCheckOutStitchingAmtTxt;

    @BindView(R.id.cheout_delivery_price_lay)
            RelativeLayout mCheckoutDeliveryPriceLay;

    @BindView(R.id.cheout_delivery_price)
    TextView mCheckoutDeliveryPriceTxt;

    @BindView(R.id.check_out_payment_lay)
            RelativeLayout mCheckOutPaymentLay;

    @BindView(R.id.checkout_prome_code_apply_txt)
            TextView mCheckoutPromoCodeApplyTxt;

    GetCheckoutProductAdapter mCheckoutProductAdapter;

    public GetItemCardAdapter mGetItemCardAdapter;
    private UserDetailsEntity mUserDetailsEntityRes;

    int mDiscountType = 0,mCouponAppliesTo = 0;

    String mCouponType = "0";

    double minimumamount = 0,mMaximumDiscount = 0,mDiscountValue = 0;

    APICommonInterface mApiCommonInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_checkout_screen);

        initView();

    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mCheckOutParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(CheckoutScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        mApiCommonInterface = APIRequestHandler.getClient().create(APICommonInterface.class);

        setHeader();

        AppConstants.APPLY_CODE = "";
        AppConstants.APPLY_CODE_ID = "";
        AppConstants.REDEEM_POINT_TO_CASH = "0";
        AppConstants.REDEEM_POINT_TO_POINTS  = "0";
        AppConstants.REDEEM_SELECTED_REDEEM = "";
        AppConstants.APPLY_CODE_AMT = "0";
        AppConstants.STORE_AMT = "0";

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("QUOTATION")){
            mCheckOutStitchingLay.setVisibility(View.VISIBLE);
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat formatter = (DecimalFormat)nf;
            formatter.applyPattern("######.00");
            double deliveryCharges = 50.00;

            AppConstants.TRANSACTION_AMOUNT = AppConstants.DELIVERY_ID.equalsIgnoreCase("2") ? String.valueOf(formatter.format(Double.parseDouble(AppConstants.TRANSACTION_AMOUNT) + deliveryCharges))  : AppConstants.TRANSACTION_AMOUNT;
            mCheckOutStitchingAmtTxt.setText(AppConstants.STICTHING_AMT);
            mCheckOurTotalAmtTxt.setText(AppConstants.TRANSACTION_AMOUNT);
            mCheckOutSubTotalTxt.setText(AppConstants.TRANSACTION_AMOUNT);
            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                mCheckOutPriceTxt.setText(AppConstants.STICTHING_AMT + " : " + getResources().getString(R.string.price));
            }else {
                mCheckOutPriceTxt.setText(getResources().getString(R.string.price) + " : "+AppConstants.STICTHING_AMT);
            }
            mCheckoutDeliveryPriceLay.setVisibility(AppConstants.DELIVERY_ID.equalsIgnoreCase("2") ? View.VISIBLE : View.GONE);
            mCheckoutDeliveryPriceTxt.setText(AppConstants.DELIVERY_ID.equalsIgnoreCase("2") ?  "50.00" : "0");
        }else {
            AppConstants.SUB_TOTAL_CASH = "0";
            AppConstants.STICTHING_AMT = "0";
            AppConstants.TRANSACTION_AMOUNT = "0";
        }

        if (AppConstants.SERVICE_TYPE.equalsIgnoreCase("CART")) {
            AppConstants.STORE_PAYMENT = "STORE_PAYMENT";
            mCheckoutShippingAddressTxt.setText(AppConstants.GET_ADDRESS);
            setCartGetItemAdapter(AppConstants.GET_ITEM_CARD_LIST);
            getDeliveryChargesApiCall(0,0);
            AppConstants.SUB_DRESS_TYPE_ID = "0";
            AppConstants.OFFER_CATEGORIES_ID = "3";
            if(AppConstants.GET_ITEM_CARD_LIST.size() > 0){
                AppConstants.APPROVED_TAILOR_ID = String.valueOf(AppConstants.GET_ITEM_CARD_LIST.get(0).getSellerId());
            }
        }else {
            AppConstants.STORE_PAYMENT = AppConstants.ORDER_TYPE_SKILL.equalsIgnoreCase("store") ? "STORE_PAYMENT" : "";
            AppConstants.SUB_DRESS_TYPE_ID = AppConstants.ORDER_TYPE_SKILL.equalsIgnoreCase("store") ? "0" : AppConstants.SUB_DRESS_TYPE_ID;
            AppConstants.OFFER_CATEGORIES_ID = AppConstants.ORDER_TYPE_SKILL.equalsIgnoreCase("store") ? "3" : "1";
            checkOutApiCall();
            AppConstants.GET_ITEM_CARD_LIST = new ArrayList<>();
            getPaymentAddressApiCall();
        }

        getLanguage();

        if (!AppConstants.TAP_TO_APPLY.equalsIgnoreCase("")){
            DialogManager.getInstance().showOptionPopup(CheckoutScreen.this, getResources().getString(R.string.already_coupon_code), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                @Override
                public void onNegativeClick() {

                }

                @Override
                public void onPositiveClick() {
                    mCheckOutPromoCodeEdtTxt.setText(AppConstants.TAP_TO_APPLY);
                    AppConstants.APPLY_CODE = AppConstants.TAP_TO_APPLY;
                    AppConstants.APPLY_CODE_ID = AppConstants.TAP_TO_APPLY_CODE_ID;
                    getCouponPriceApiCall(AppConstants.TAP_TO_APPLY);
                    AppConstants.TAP_TO_APPLY = "";
                }
            });
        }

        mCheckOutPromoCodeEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count !=0){
                    mCheckOutPromoCodeEdtTxt.setError(null);
                }else {
                    CheckOutRedeemCouponConverting();
                }
                AppConstants.APPLY_CODE = mCheckOutPromoCodeEdtTxt.getText().toString().trim();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setHeader() {
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.check_out));
        mRightSideImg.setVisibility(View.INVISIBLE);
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        AppConstants.ORDER_SUMMARY = "CHECKOUT";

    }

    @OnClick({R.id.header_left_side_img,R.id.check_out_payment_lay,R.id.checkout_promo_code_apply_lay,R.id.check_out_point_to_cash_lay,R.id.check_out_view_all_promo_code})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                    onBackPressed();
                break;
            case R.id.check_out_payment_lay:
                if (mDiscountValue == 0){
                    AppConstants.APPLY_CODE = "";
                    AppConstants.APPLY_CODE_ID = "";
                    AppConstants.APPLY_CODE_AMT = "0";

                }
                if (mDiscountValue != 0 || !AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")){
                    AppConstants.TRANSACTION_AMOUNT = mCheckOurTotalAmtTxt.getText().toString().trim();
                    AppConstants.SUB_TOTAL_CASH = mCheckOutSubTotalTxt.getText().toString().trim();
                }
                if (AppConstants.SERVICE_TYPE.equalsIgnoreCase("CART")){
                    insertWebOrder();
                }else {
                    nextScreen(PaymentWebviewScreen.class, true);

                }
                mCheckOutPaymentLay.setVisibility(View.GONE);
                break;
            case R.id.checkout_promo_code_apply_lay:
                if (mCheckoutPromoCodeApplyTxt.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.remove))){
                    mCheckOutPromoCodeEdtTxt.setText("");
                    mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.apply));
                    CheckOutRedeemCouponConverting();
                }else if (mCheckOutPromoCodeEdtTxt.getText().toString().trim().equalsIgnoreCase("")){
                    mCheckOutPromoCodeApplyLay.clearAnimation();
                    mCheckOutPromoCodeApplyLay.setAnimation(ShakeErrorUtils.shakeError());
                    mCheckOutPromoCodeEdtTxt.setError(getResources().getString(R.string.please_type_your_promo_code));
                    CheckOutRedeemCouponConverting();
                }else {
                    mCheckOutPromoCodeEdtTxt.setError(null);
                    getCouponPriceApiCall(mCheckOutPromoCodeEdtTxt.getText().toString().trim());
                    CheckOutRedeemCouponConverting();
                }
                break;
            case R.id.check_out_point_to_cash_lay:
                nextScreen(ConvertPointsToCashScreen.class,true);
                break;
            case R.id.check_out_view_all_promo_code:
                AppConstants.CHECK_OUT_OFFERS = "Apply";
                nextScreen(OffersScreen.class,true);
                break;

        }
    }

    public void getLanguage(){
        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.check_out_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.check_out_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
        }
    }

    public void  insertWebOrder(){
        if (NetworkUtil.isNetworkAvailable(CheckoutScreen.this)){
            InsertWebOrderBody insertWebOrderBody = new InsertWebOrderBody();
            InsertWebOrderEntity insertWebOrderEntity = new InsertWebOrderEntity();
            ArrayList<InsertWebOrderDetailsEntity> insertWebOrderDetailsEntities = new ArrayList<>();

            String ordreAmount = "0";

            for (int i=0; i<AppConstants.GET_ITEM_CARD_LIST.size(); i++){
                ordreAmount = String.valueOf(Double.parseDouble(ordreAmount) + AppConstants.GET_ITEM_CARD_LIST.get(i).getUnitTotal());
            }

            insertWebOrderEntity.setUserId(Integer.parseInt(mUserDetailsEntityRes.getUSER_ID()));
            insertWebOrderEntity.setCartId(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID));
            insertWebOrderEntity.setAddressId(Integer.parseInt(AppConstants.GET_ADDRESS_ID));
            insertWebOrderEntity.setOrderAmount(Double.parseDouble(ordreAmount));
            insertWebOrderEntity.setOrderTotal(Double.parseDouble(ordreAmount));
            insertWebOrderEntity.setCouponCode(AppConstants.APPLY_CODE);
            insertWebOrderEntity.setCouponDiscount(Double.parseDouble(AppConstants.APPLY_CODE_AMT));
            insertWebOrderEntity.setDiscount(AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("") ? 0 : Integer.parseInt(AppConstants.REDEEM_POINT_TO_CASH));
            insertWebOrderEntity.setTax(0);
            insertWebOrderEntity.setStichingOrderReferenceNo(0);

            for (int i=0; i<AppConstants.GET_ITEM_CARD_LIST.size(); i++){
                InsertWebOrderDetailsEntity insertWebOrderDetailsEntity = new InsertWebOrderDetailsEntity();
                insertWebOrderDetailsEntity.setColorId(AppConstants.GET_ITEM_CARD_LIST.get(i).getColorId());
                insertWebOrderDetailsEntity.setDiscount(AppConstants.GET_ITEM_CARD_LIST.get(i).getDiscount());
                insertWebOrderDetailsEntity.setPrice(AppConstants.GET_ITEM_CARD_LIST.get(i).getAmount());
                insertWebOrderDetailsEntity.setProductId(AppConstants.GET_ITEM_CARD_LIST.get(i).getProductId());
                insertWebOrderDetailsEntity.setQuantity(AppConstants.GET_ITEM_CARD_LIST.get(i).getQuantity());
                insertWebOrderDetailsEntity.setSellerId(AppConstants.GET_ITEM_CARD_LIST.get(i).getSellerId());
                insertWebOrderDetailsEntity.setSizeId(AppConstants.GET_ITEM_CARD_LIST.get(i).getSizeId());
                insertWebOrderDetailsEntity.setTotal(AppConstants.GET_ITEM_CARD_LIST.get(i).getUnitTotal());

                insertWebOrderDetailsEntities.add(insertWebOrderDetailsEntity);
            }

            insertWebOrderBody.setOrder(insertWebOrderEntity);
            insertWebOrderBody.setOrderDetails(insertWebOrderDetailsEntities);

            APIRequestHandler.getInstance().insertWebOrder(insertWebOrderBody,CheckoutScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CheckoutScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    insertWebOrder();
                }
            });
        }
    }

    public void checkOutApiCall(){
        if (NetworkUtil.isNetworkAvailable(CheckoutScreen.this)){
            if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
                if (AppConstants.CHECK_OUT_ORDER.equalsIgnoreCase("StitchingStore")){
                    APIRequestHandler.getInstance().getCheckOutApi(AppConstants.ORDER_ID,AppConstants.APPROVED_TAILOR_ID,AppConstants.CHECK_OUT_MATERIAL_ID,AppConstants.CHECK_OUT_SERVICE_TYPE_ID,AppConstants.CHECK_OUT_MEASUREMENT_ID,"SkillUpdate",AppConstants.STORE_ID,CheckoutScreen.this);

                }else {
                    APIRequestHandler.getInstance().getCheckOutApi(AppConstants.ORDER_ID,AppConstants.APPROVED_TAILOR_ID,AppConstants.CHECK_OUT_MATERIAL_ID,AppConstants.CHECK_OUT_SERVICE_TYPE_ID,AppConstants.CHECK_OUT_MEASUREMENT_ID,"SkillUpdate","0",CheckoutScreen.this);

                }
            }else {
                if (AppConstants.CHECK_OUT_ORDER.equalsIgnoreCase("store")) {
                    APIRequestHandler.getInstance().getCheckOutApi("0",AppConstants.APPROVED_TAILOR_ID,"0","0","0","Quotation",AppConstants.STORE_ID,CheckoutScreen.this);

                }else if (AppConstants.CHECK_OUT_ORDER.equalsIgnoreCase("StitchingStore")){
                    APIRequestHandler.getInstance().getCheckOutApi(AppConstants.ORDER_ID,AppConstants.APPROVED_TAILOR_ID,"0","0","0","SkillUpdate",AppConstants.STORE_ID,CheckoutScreen.this);

                }else {
                    APIRequestHandler.getInstance().getCheckOutApi(AppConstants.ORDER_ID,AppConstants.APPROVED_TAILOR_ID,"0","0","0","Quotation","0",CheckoutScreen.this);

                }
            }
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CheckoutScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    checkOutApiCall();
                }
            });
        }
    }

    public void getCouponPriceApiCall(String CouponId){
        if (NetworkUtil.isNetworkAvailable(CheckoutScreen.this)){
            APIRequestHandler.getInstance().getCouponPriceApiCall(CouponId.trim(),AppConstants.APPROVED_TAILOR_ID,CheckoutScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CheckoutScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCouponPriceApiCall(CouponId);
                }
            });
        }
    }

    public void getPaymentAddressApiCall(){
        if(NetworkUtil.isNetworkAvailable(CheckoutScreen.this)){
            APIRequestHandler.getInstance().getPaymentAddressApi(AppConstants.ORDER_ID,AppConstants.ORDER_TYPE_SKILL,CheckoutScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CheckoutScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getPaymentAddressApiCall();
                }
            });
        }
    }

    public void getDeliveryChargesApiCall(int OrderId, int DeliveryId){
        if (NetworkUtil.isNetworkAvailable(CheckoutScreen.this)){
            CheckoutDeliveryChargesModal checkoutDeliveryChargesModal =  new CheckoutDeliveryChargesModal();
            ArrayList<IdEntity> idList = new ArrayList<>();
            idList = AppConstants.DELIVERY_TAILOR_ID;

            checkoutDeliveryChargesModal.setOrderId(OrderId);
            checkoutDeliveryChargesModal.setDeliveryId(DeliveryId);
            checkoutDeliveryChargesModal.setTailorId(idList);
            checkoutDeliveryChargesModal.setAreaId(Integer.parseInt(AppConstants.DELIVERY_AREA_ID));
            APIRequestHandler.getInstance().getOrderDetailsStitchingStoreApi(checkoutDeliveryChargesModal,CheckoutScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CheckoutScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getDeliveryChargesApiCall(OrderId,DeliveryId);
                }
            });
        }
    }

    /*Set Adapter for the Recycler view*/
    public void setGetItemAdapter(ArrayList<StoreCartEntity> getStoreOrderDetails) {

        mGetStoreProductRecLay.setVisibility(getStoreOrderDetails.size() > 0 ? View.VISIBLE : View.GONE);
        mProductAddedFromStoreTxt.setVisibility(getStoreOrderDetails.size() > 0 ? View.VISIBLE : View.GONE);

        mGetItemCardAdapter = new GetItemCardAdapter(getStoreOrderDetails,CheckoutScreen.this);
        mGetStoreProductRecLay.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mGetStoreProductRecLay.setAdapter(mGetItemCardAdapter);

    }

    /*Set Adapter for the Recycler view*/
    public void setCartGetItemAdapter(ArrayList<StoreCartEntity> getStoreOrderDetails) {

        double storeAmt = 0;

        for (int i=0; i<getStoreOrderDetails.size(); i++){
            double total = 0;
            total = getStoreOrderDetails.get(i).getUnitTotal();
            storeAmt = storeAmt + total;
        }

        if (storeAmt > 0){
            mCheckOutStoreTotalLay.setVisibility(View.VISIBLE);
        }else {
            mCheckOutStoreTotalLay.setVisibility(View.GONE);
        }

        mCheckOutStoreTotalTxt.setText(String.valueOf(storeAmt));
        mCheckOurTotalAmtTxt.setText(String.valueOf(storeAmt));
        mCheckOutSubTotalTxt.setText(String.valueOf(storeAmt));
        AppConstants.SUB_TOTAL_CASH = String.valueOf(storeAmt);
        AppConstants.TRANSACTION_AMOUNT = String.valueOf(storeAmt);
        AppConstants.STORE_AMT = String.valueOf(storeAmt);
        AppConstants.STICTHING_AMT = "0";

        mGetStoreProductRecLay.setVisibility(getStoreOrderDetails.size() > 0 ? View.VISIBLE : View.GONE);
        mProductAddedFromStoreTxt.setVisibility(getStoreOrderDetails.size() > 0 ? View.VISIBLE : View.GONE);

        mGetItemCardAdapter = new GetItemCardAdapter(getStoreOrderDetails,CheckoutScreen.this);
        mGetStoreProductRecLay.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mGetStoreProductRecLay.setAdapter(mGetItemCardAdapter);

    }

    public void setCheckoutProductAdapter(ArrayList<StoreCartEntity> getStoreOrderDetails){

        mCheckOutRecyclerView.setVisibility(getStoreOrderDetails.size() > 0 ? View.VISIBLE : View.GONE);

        mCheckoutProductAdapter = new GetCheckoutProductAdapter(getStoreOrderDetails,CheckoutScreen.this);
        mCheckOutRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
        mCheckOutRecyclerView.setAdapter(mCheckoutProductAdapter);

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof CheckoutResponse){
            CheckoutResponse mResponse = (CheckoutResponse)resObj;
            if (mResponse.getResult().getGetDressSubtype().size() > 0){
                mCheckOutScreenYourOrderTxt.setVisibility(View.VISIBLE);
                mCheckOutScreenYourOrderLay.setVisibility(View.VISIBLE);
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mCheckOutDressNameTxt.setText(mResponse.getResult().getGetDressSubtype().get(0).getNameInArabic());
                    mCheckOutQtyTxt.setText(mResponse.getResult().getGetDressSubtype().get(0).getQty()+ " : "+getResources().getString(R.string.qty));

                }else {
                    mCheckOutDressNameTxt.setText(mResponse.getResult().getGetDressSubtype().get(0).getNameInEnglish());
                    mCheckOutQtyTxt.setText(getResources().getString(R.string.qty) + " : "+mResponse.getResult().getGetDressSubtype().get(0).getQty());

                }
                try {
                    Glide.with(CheckoutScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"images/DressSubType/"+mResponse.getResult().getGetDressSubtype().get(0).getImage())
                            .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                            .apply(RequestOptions.skipMemoryCacheOf(true))
                            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                            .into(mCheckOutDressImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
            }
            AppConstants.GET_ITEM_CARD_LIST = new ArrayList<>();
            AppConstants.GET_ITEM_CARD_LIST = mResponse.getResult().getGetStoreOrderDetails();
            setGetItemAdapter(mResponse.getResult().getGetStoreOrderDetails());

            if (mResponse.getResult().getGetStoreOrderAmount().size() > 0){
                mCheckOutStoreTotalLay.setVisibility(View.VISIBLE);
                mCheckOutStoreTotalTxt.setText(String.valueOf(mResponse.getResult().getGetStoreOrderAmount().get(0).getStoreOrderAmount()));
                AppConstants.STORE_AMT = String.valueOf(mResponse.getResult().getGetStoreOrderAmount().get(0).getStoreOrderAmount());
            }else {
                AppConstants.STORE_AMT = "0";
                mCheckOutStoreTotalTxt.setText("0");
                mCheckOutStoreTotalLay.setVisibility(View.GONE);
            }

            if (!AppConstants.DIRECT_ORDER.equalsIgnoreCase("QUOTATION")) {

                AppConstants.STICTHING_AMT = String.valueOf(mResponse.getResult().getPrice());

                mCheckOutStitchingAmtTxt.setText(String.valueOf(mResponse.getResult().getPrice()));
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mCheckOutPriceTxt.setText(String.valueOf(mResponse.getResult().getPrice()) + " : " + getResources().getString(R.string.price));
                }else {
                    mCheckOutPriceTxt.setText(getResources().getString(R.string.price) + " : "+String.valueOf(mResponse.getResult().getPrice()));
                }

                if (mResponse.getResult().getPrice() > 0){
                    mCheckOutStitchingLay.setVisibility(View.VISIBLE);

                }else {
                    mCheckOutStitchingLay.setVisibility(View.GONE);
                }

                AppConstants.SUB_TOTAL_CASH = String.valueOf(mResponse.getResult().getGrandTotal());
                AppConstants.TRANSACTION_AMOUNT = String.valueOf(mResponse.getResult().getGrandTotal());

                mCheckoutDeliveryPriceLay.setVisibility(mResponse.getResult().getDeliveryCharges() > 0 ? View.VISIBLE : View.GONE);
                mCheckoutDeliveryPriceTxt.setText(String.valueOf(mResponse.getResult().getDeliveryCharges()));

                mCheckOurTotalAmtTxt.setText(String.valueOf(mResponse.getResult().getGrandTotal()));
                mCheckOutSubTotalTxt.setText(String.valueOf(mResponse.getResult().getGrandTotal()));

            }

            mCheckOutSubTotalLay.setVisibility(View.GONE);

        }
        if (resObj instanceof GetBuyerAddressModal){
            GetBuyerAddressModal mREspone = (GetBuyerAddressModal)resObj;
            if (mREspone.getResult().size() > 0){
                AppConstants.STORE_USER_DETAILS = new PaymentInfoEntity();
                AppConstants.STORE_USER_DETAILS.setFirstName(mREspone.getResult().get(0).getFirstName());
                AppConstants.STORE_USER_DETAILS.setSecondName(mREspone.getResult().get(0).getLastName());
                AppConstants.STORE_USER_DETAILS.setLineOne(mREspone.getResult().get(0).getLandmark());
                AppConstants.STORE_USER_DETAILS.setLineTwo("");
                AppConstants.STORE_USER_DETAILS.setLineThree("");
                AppConstants.STORE_USER_DETAILS.setCity(mREspone.getResult().get(0).getStateName());
                AppConstants.STORE_USER_DETAILS.setRegion(mREspone.getResult().get(0).getArea());
                AppConstants.STORE_USER_DETAILS.setCountry(mREspone.getResult().get(0).getCountryName());

                if (!AppConstants.SERVICE_TYPE.equalsIgnoreCase("CART")) {
                    mCheckoutShippingAddressTxt.setText(mREspone.getResult().get(0).getFirstName()+", "+mREspone.getResult().get(0).getLandmark()+","+mREspone.getResult().get(0).getStateName()+","+mREspone.getResult().get(0).getArea()+", "+mREspone.getResult().get(0).getCountryName()+".");
                }

            }
        }
        if (resObj instanceof GetCouponPriceResponse){
            GetCouponPriceResponse mResponse = (GetCouponPriceResponse)resObj;
            if (mResponse.getResult().size()>0){
                if (!mResponse.getResult().get(0).getResult().equalsIgnoreCase("")){
                    Toast.makeText(CheckoutScreen.this,mResponse.getResult().get(0).getResult(),Toast.LENGTH_SHORT).show();
                    mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.remove));
                }else {
                    mDiscountValue = mResponse.getResult().get(0).getDiscountValue();
                    mDiscountType = mResponse.getResult().get(0).getDiscountType();
                    minimumamount = mResponse.getResult().get(0).getMinimumamount();
                    mMaximumDiscount = mResponse.getResult().get(0).getMaximumDiscount();
                    mCouponType = mResponse.getResult().get(0).getCoupanType();
                    mCouponAppliesTo = mResponse.getResult().get(0).getCouponAppliesTo();
                    AppConstants.APPLY_CODE_ID = String.valueOf(mResponse.getResult().get(0).getId());
                    CheckOutRedeemCouponConverting();
                }

            }else {
                mDiscountValue = 0;
                mDiscountType = 0;
                minimumamount = 0;
                mMaximumDiscount = 0;
                mCouponType = "0";
                mCouponAppliesTo = 0;
                CheckOutRedeemCouponConverting();
                mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.remove));
            }
        }
        if (resObj instanceof WebOrderResponse) {
            WebOrderResponse mResponse = (WebOrderResponse) resObj;
            AppConstants.REQUEST_LIST_ID = String.valueOf(mResponse.getResult().getOrderId());
            AppConstants.STORE_PAYMENT = "STORE_PAYMENT";
            nextScreen(PaymentWebviewScreen.class, true);

        }
        if (resObj instanceof CheckoutDeliveryChargesResponse){
            CheckoutDeliveryChargesResponse mResponse = (CheckoutDeliveryChargesResponse)resObj;
            if (mResponse.getResult().size() > 0){
                mCheckoutDeliveryPriceLay.setVisibility(mResponse.getResult().get(0).getAmount() > 0 ? View.VISIBLE : View.GONE);

                if (mResponse.getResult().get(0).getAmount() > 0){
                    mCheckoutDeliveryPriceTxt.setText(String.valueOf(mResponse.getResult().get(0).getAmount()));
                    mCheckOurTotalAmtTxt.setText(String.valueOf(Double.parseDouble(mCheckOurTotalAmtTxt.getText().toString())+mResponse.getResult().get(0).getAmount()));
                    mCheckOutSubTotalTxt.setText(String.valueOf(Double.parseDouble(mCheckOutSubTotalTxt.getText().toString())+mResponse.getResult().get(0).getAmount()));
                    AppConstants.SUB_TOTAL_CASH = String.valueOf(Double.parseDouble(AppConstants.SUB_TOTAL_CASH)+mResponse.getResult().get(0).getAmount());
                    AppConstants.TRANSACTION_AMOUNT = String.valueOf(Double.parseDouble(AppConstants.TRANSACTION_AMOUNT)+mResponse.getResult().get(0).getAmount());
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!AppConstants.APPLY_CODE.equalsIgnoreCase("")){
            mCheckOutPromoCodeEdtTxt.setText(AppConstants.APPLY_CODE);
        }
        if (!mCheckOutPromoCodeEdtTxt.getText().toString().trim().equalsIgnoreCase("")){
            getCouponPriceApiCall(mCheckOutPromoCodeEdtTxt.getText().toString().trim());
        }else {
            CheckOutRedeemCouponConverting();
        }
    }

    public void CheckOutRedeemCouponConverting(){
        if (mCheckOutPromoCodeEdtTxt.getText().toString().trim().equalsIgnoreCase("")&&AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")){
            mCheckOutRedeemAmtLay.setVisibility(View.GONE);
            mCheckOutSubTotalLay.setVisibility(View.GONE);
            mCheckOutPromoCodeAmtLay.setVisibility(View.GONE);
            mCheckOurTotalAmtTxt.setText(AppConstants.TRANSACTION_AMOUNT);
            mDiscountValue = 0;
            mDiscountType = 0;
            minimumamount = 0;
            mMaximumDiscount = 0;
            mCouponType = "0";
            mCouponAppliesTo = 0;
            mCheckOutPromoCodeAmtTxt.setText("- "+mDiscountValue);
            AppConstants.APPLY_CODE = "";
            AppConstants.APPLY_CODE_ID = "";
            AppConstants.APPLY_CODE_AMT = "0";
            mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.apply));
        }else if (!mCheckOutPromoCodeEdtTxt.getText().toString().trim().equalsIgnoreCase("")&&!AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")){
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat formatter = (DecimalFormat)nf;
            formatter.applyPattern("######.00");

            String dedicatedPointStr = String.valueOf(Double.valueOf(formatter.format(Double.parseDouble(AppConstants.TRANSACTION_AMOUNT) - Double.parseDouble(AppConstants.REDEEM_POINT_TO_CASH))));
            Double dedicatedPointAmt = Double.valueOf(formatter.format(Double.parseDouble(AppConstants.TRANSACTION_AMOUNT) - Double.parseDouble(AppConstants.REDEEM_POINT_TO_CASH)));
            mCheckOutRedeemAmtLay.setVisibility(View.VISIBLE);
            mCheckOutSubTotalLay.setVisibility(View.VISIBLE);
            mCheckOutSubTotalTxt.setText(AppConstants.TRANSACTION_AMOUNT);
            mCheckOutRedeemAmtTxt.setText("- "+String.valueOf(Double.valueOf(formatter.format(Double.parseDouble(AppConstants.REDEEM_POINT_TO_CASH)))));
            mCheckOurTotalAmtTxt.setText(String.valueOf(formatter.format(Double.parseDouble(dedicatedPointStr))));

            if (mDiscountValue == 0){
                AppConstants.APPLY_CODE = "";
                AppConstants.APPLY_CODE_ID = "";
                AppConstants.APPLY_CODE_AMT = String.valueOf(mDiscountValue);
                mCheckOutPromoCodeAmtLay.setVisibility(View.GONE);
                mCheckOutPromoCodeAmtTxt.setText("- "+String.valueOf(Double.parseDouble(formatter.format(mDiscountValue))));
                Toast.makeText(CheckoutScreen.this, getResources().getString(R.string.invalid_promo_code), Toast.LENGTH_SHORT).show();
                mCheckOutPromoCodeEdtTxt.clearAnimation();
                mCheckOutPromoCodeEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                mCheckOutPromoCodeEdtTxt.setError(getResources().getString(R.string.invalid_promo_code));
                mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.remove));
            }else {
                if (mCouponAppliesTo == 1){
                    if (Double.valueOf(AppConstants.STICTHING_AMT) > minimumamount){
                        if (mDiscountType == 1){
                            mCheckOutPromoCodeAmtTxt.setText("- "+String.valueOf(Double.parseDouble(formatter.format(mDiscountValue))));
                            mCheckOurTotalAmtTxt.setText(String.valueOf(formatter.format(dedicatedPointAmt - mDiscountValue)));
                            mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
                            AppConstants.APPLY_CODE_AMT = String.valueOf(mDiscountValue);
                        }else if (mDiscountType == 2){
                            double res = (Double.valueOf(AppConstants.STICTHING_AMT) / 100.0f) * mDiscountValue;
                            if (res > mMaximumDiscount){
                                mCheckOutPromoCodeAmtTxt.setText("- "+String.valueOf(Double.parseDouble(formatter.format(mMaximumDiscount))));
                                mCheckOurTotalAmtTxt.setText(String.valueOf(formatter.format(dedicatedPointAmt - mMaximumDiscount)));
                                mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
                                AppConstants.APPLY_CODE_AMT = String.valueOf(mMaximumDiscount);
                            }else {
                                mCheckOutPromoCodeAmtTxt.setText("- "+String.valueOf(Double.parseDouble(formatter.format(res))));
                                mCheckOurTotalAmtTxt.setText(String.valueOf(formatter.format(dedicatedPointAmt - res)));
                                mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
                                AppConstants.APPLY_CODE_AMT = String.valueOf(res);
                            }
                        }
                        mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.remove));
                    }else {
                        AppConstants.APPLY_CODE = "";
                        AppConstants.APPLY_CODE_ID = "";
                        AppConstants.APPLY_CODE_AMT = "0";
                        mCheckOutPromoCodeAmtLay.setVisibility(View.GONE);
                        mCheckOutPromoCodeAmtTxt.setText("0");
                        Toast.makeText(CheckoutScreen.this,getResources().getString(R.string.invalid_promo_code),Toast.LENGTH_SHORT).show();
                        mCheckOutPromoCodeEdtTxt.clearAnimation();
                        mCheckOutPromoCodeEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                        mCheckOutPromoCodeEdtTxt.setError(getResources().getString(R.string.invalid_promo_code));
                        mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.remove));
                    }
                }else if (mCouponAppliesTo == 3){
                    double Amount = 0.0;
                    for (int i=0; i<AppConstants.GET_ITEM_CARD_LIST.size(); i++){
                        if (String.valueOf(mCouponType).equalsIgnoreCase(AppConstants.GET_ITEM_CARD_LIST.get(i).getProductId())){
                            Amount = Amount +AppConstants.GET_ITEM_CARD_LIST.get(i).getUnitTotal();
                        }
                    }
                    if (Amount == 0.0){
                        AppConstants.APPLY_CODE = "";
                        AppConstants.APPLY_CODE_ID = "";
                        AppConstants.APPLY_CODE_AMT = "0";
                        mCheckOutPromoCodeAmtLay.setVisibility(View.GONE);
                        mCheckOutPromoCodeAmtTxt.setText("0");
                        Toast.makeText(CheckoutScreen.this,getResources().getString(R.string.invalid_promo_code),Toast.LENGTH_SHORT).show();
                        mCheckOutPromoCodeEdtTxt.clearAnimation();
                        mCheckOutPromoCodeEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                        mCheckOutPromoCodeEdtTxt.setError(getResources().getString(R.string.invalid_promo_code));
                        mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.remove));
                    }else {
                        if (Amount > minimumamount){
                            if (mDiscountType == 1){
                                mCheckOutPromoCodeAmtTxt.setText("- "+String.valueOf(Double.parseDouble(formatter.format(mDiscountValue))));
                                mCheckOurTotalAmtTxt.setText(String.valueOf(formatter.format(dedicatedPointAmt - mDiscountValue)));
                                mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
                                AppConstants.APPLY_CODE_AMT = String.valueOf(mDiscountValue);
                            }else if (mDiscountType == 2){
                                double res = (Amount / 100.0f) * mDiscountValue;
                                if (res > mMaximumDiscount){
                                    mCheckOutPromoCodeAmtTxt.setText("- "+String.valueOf(Double.parseDouble(formatter.format(mMaximumDiscount))));
                                    mCheckOurTotalAmtTxt.setText(String.valueOf(formatter.format(dedicatedPointAmt - mMaximumDiscount)));
                                    mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
                                    AppConstants.APPLY_CODE_AMT = String.valueOf(mMaximumDiscount);
                                }else {
                                    mCheckOutPromoCodeAmtTxt.setText("- "+String.valueOf(Double.parseDouble(formatter.format(res))));
                                    mCheckOurTotalAmtTxt.setText(String.valueOf(formatter.format(dedicatedPointAmt - res)));
                                    mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
                                    AppConstants.APPLY_CODE_AMT = String.valueOf(res);
                                }
                            }
                            mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.remove));
                        }else {
                            AppConstants.APPLY_CODE = "";
                            AppConstants.APPLY_CODE_ID = "";
                            AppConstants.APPLY_CODE_AMT = "0";
                            mCheckOutPromoCodeAmtLay.setVisibility(View.GONE);
                            mCheckOutPromoCodeAmtTxt.setText("0");
                            Toast.makeText(CheckoutScreen.this,getResources().getString(R.string.invalid_promo_code),Toast.LENGTH_SHORT).show();
                            mCheckOutPromoCodeEdtTxt.clearAnimation();
                            mCheckOutPromoCodeEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                            mCheckOutPromoCodeEdtTxt.setError(getResources().getString(R.string.invalid_promo_code));
                            mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.remove));
                        }
                    }
                }
//                else if (mCouponAppliesTo == 4){
//                    double Amount = 0.0;
//                    for (int i=0; i<AppConstants.GET_ITEM_CARD_LIST.size(); i++){
//                        if (mCouponType.equalsIgnoreCase(String.valueOf(AppConstants.GET_ITEM_CARD_LIST.get(i).getBrandId()))){
//                            Amount = Amount + AppConstants.GET_ITEM_CARD_LIST.get(i).getAmount()*AppConstants.GET_ITEM_CARD_LIST.get(i).getQuantity();
//                        }
//                    }
//                    if (Amount == 0.0){
//                        AppConstants.APPLY_CODE = "";
//                        AppConstants.APPLY_CODE_ID = "";
//                        AppConstants.APPLY_CODE_AMT = "0";
//                        mCheckOutPromoCodeAmtLay.setVisibility(View.GONE);
//                        mCheckOutPromoCodeAmtTxt.setText("0");
//                        Toast.makeText(CheckoutScreen.this,getResources().getString(R.string.invalid_promo_code),Toast.LENGTH_SHORT).show();
//                        mCheckOutPromoCodeEdtTxt.clearAnimation();
//                        mCheckOutPromoCodeEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
//                        mCheckOutPromoCodeEdtTxt.setError(getResources().getString(R.string.invalid_promo_code));
//                    }else {
//                        if (Amount > minimumamount){
//                            if (mDiscountType == 1){
//                                mCheckOutPromoCodeAmtTxt.setText("- "+String.valueOf(Double.parseDouble(formatter.format(mDiscountValue))));
//                                mCheckOurTotalAmtTxt.setText(String.valueOf(Double.valueOf(formatter.format(dedicatedPointAmt - mDiscountValue))));
//                                mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
//                                AppConstants.APPLY_CODE_AMT = String.valueOf(mDiscountValue);
//                            }else if (mDiscountType == 2){
//                                double res = (Amount / 100.0f) * mDiscountValue;
//                                if (res > mMaximumDiscount){
//                                    mCheckOutPromoCodeAmtTxt.setText("- "+String.valueOf(Double.parseDouble(formatter.format(mMaximumDiscount))));
//                                    mCheckOurTotalAmtTxt.setText(String.valueOf(Double.valueOf(formatter.format(dedicatedPointAmt - mMaximumDiscount))));
//                                    mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
//                                    AppConstants.APPLY_CODE_AMT = String.valueOf(mMaximumDiscount);
//                                }else {
//                                    mCheckOutPromoCodeAmtTxt.setText("- "+String.valueOf(Double.parseDouble(formatter.format(res))));
//                                    mCheckOurTotalAmtTxt.setText(String.valueOf(Double.valueOf(formatter.format(dedicatedPointAmt - res))));
//                                    mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
//                                    AppConstants.APPLY_CODE_AMT = String.valueOf(res);
//                                }
//                            }
//                        }else {
//                            AppConstants.APPLY_CODE = "";
//                            AppConstants.APPLY_CODE_ID = "";
//                            AppConstants.APPLY_CODE_AMT = "0";
//                            mCheckOutPromoCodeAmtLay.setVisibility(View.GONE);
//                            mCheckOutPromoCodeAmtTxt.setText("0");
//                            Toast.makeText(CheckoutScreen.this,getResources().getString(R.string.invalid_promo_code),Toast.LENGTH_SHORT).show();
//                            mCheckOutPromoCodeEdtTxt.clearAnimation();
//                            mCheckOutPromoCodeEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
//                            mCheckOutPromoCodeEdtTxt.setError(getResources().getString(R.string.invalid_promo_code));
//                        }
//                    }
//                }
            }

        }else if (!mCheckOutPromoCodeEdtTxt.getText().toString().trim().equalsIgnoreCase("")&&AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")){
            mCheckOutRedeemAmtLay.setVisibility(View.GONE);
            mCheckOutSubTotalLay.setVisibility(View.VISIBLE);
            mCheckOutSubTotalTxt.setText(AppConstants.TRANSACTION_AMOUNT);
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat formatter = (DecimalFormat)nf;
            formatter.applyPattern("######.00");
            Double dedicatedPointAmt = Double.valueOf(formatter.format(Double.parseDouble(AppConstants.TRANSACTION_AMOUNT)));
            if (mDiscountValue == 0) {
                AppConstants.APPLY_CODE = "";
                AppConstants.APPLY_CODE_ID = "";
                AppConstants.APPLY_CODE_AMT = "0";
                mCheckOutSubTotalLay.setVisibility(View.GONE);
                mCheckOutPromoCodeAmtLay.setVisibility(View.GONE);
                mCheckOutPromoCodeAmtTxt.setText("0");
                mCheckOurTotalAmtTxt.setText(AppConstants.TRANSACTION_AMOUNT);
                Toast.makeText(CheckoutScreen.this, getResources().getString(R.string.invalid_promo_code), Toast.LENGTH_SHORT).show();
                mCheckOutPromoCodeEdtTxt.clearAnimation();
                mCheckOutPromoCodeEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                mCheckOutPromoCodeEdtTxt.setError(getResources().getString(R.string.invalid_promo_code));
                mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.remove));
            } else {
                if (mCouponAppliesTo == 1) {
                    if (Double.valueOf(AppConstants.STICTHING_AMT) > minimumamount) {
                        if (mDiscountType == 1) {
                            mCheckOutPromoCodeAmtTxt.setText("- " + String.valueOf(Double.parseDouble(formatter.format(mDiscountValue))));
                            mCheckOurTotalAmtTxt.setText(String.valueOf(formatter.format(dedicatedPointAmt - mDiscountValue)));
                            mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
                            AppConstants.APPLY_CODE_AMT = String.valueOf(mDiscountValue);
                        } else if (mDiscountType == 2) {
                            double res = (Double.valueOf(AppConstants.STICTHING_AMT) / 100.0f) * mDiscountValue;
                            if (res > mMaximumDiscount) {
                                mCheckOutPromoCodeAmtTxt.setText("- " + String.valueOf(Double.parseDouble(formatter.format(mMaximumDiscount))));
                                mCheckOurTotalAmtTxt.setText(String.valueOf(formatter.format(dedicatedPointAmt - mMaximumDiscount)));
                                mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
                                AppConstants.APPLY_CODE_AMT = String.valueOf(mMaximumDiscount);
                            } else {
                                mCheckOutPromoCodeAmtTxt.setText("- " + String.valueOf(Double.parseDouble(formatter.format(res))));
                                mCheckOurTotalAmtTxt.setText(String.valueOf(formatter.format(dedicatedPointAmt - res)));
                                mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
                                AppConstants.APPLY_CODE_AMT = String.valueOf(res);
                            }
                        }
                        mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.remove));
                    } else {
                        AppConstants.APPLY_CODE = "";
                        AppConstants.APPLY_CODE_ID = "";
                        AppConstants.APPLY_CODE_AMT = "0";
                        mCheckOutSubTotalLay.setVisibility(View.GONE);
                        mCheckOutPromoCodeAmtLay.setVisibility(View.GONE);
                        mCheckOutPromoCodeAmtTxt.setText("0");
                        mCheckOurTotalAmtTxt.setText(AppConstants.TRANSACTION_AMOUNT);
                        Toast.makeText(CheckoutScreen.this, getResources().getString(R.string.invalid_promo_code), Toast.LENGTH_SHORT).show();
                        mCheckOutPromoCodeEdtTxt.clearAnimation();
                        mCheckOutPromoCodeEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                        mCheckOutPromoCodeEdtTxt.setError(getResources().getString(R.string.invalid_promo_code));
                        mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.remove));
                    }
                } else if (mCouponAppliesTo == 3) {
                    double Amount = 0.0;
                    for (int i = 0; i < AppConstants.GET_ITEM_CARD_LIST.size(); i++) {
                        if (String.valueOf(mCouponType).equalsIgnoreCase(AppConstants.GET_ITEM_CARD_LIST.get(i).getProductId())) {
                            Amount = Amount + AppConstants.GET_ITEM_CARD_LIST.get(i).getUnitTotal();
                        }
                    }
                    if (Amount == 0.0) {
                        AppConstants.APPLY_CODE = "";
                        AppConstants.APPLY_CODE_ID = "";
                        AppConstants.APPLY_CODE_AMT = "0";
                        mCheckOutSubTotalLay.setVisibility(View.GONE);
                        mCheckOutPromoCodeAmtLay.setVisibility(View.GONE);
                        mCheckOutPromoCodeAmtTxt.setText("0");
                        mCheckOurTotalAmtTxt.setText(AppConstants.TRANSACTION_AMOUNT);
                        Toast.makeText(CheckoutScreen.this, getResources().getString(R.string.invalid_promo_code), Toast.LENGTH_SHORT).show();
                        mCheckOutPromoCodeEdtTxt.clearAnimation();
                        mCheckOutPromoCodeEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                        mCheckOutPromoCodeEdtTxt.setError(getResources().getString(R.string.invalid_promo_code));
                        mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.remove));
                    } else {
                        if (Amount > minimumamount) {
                            if (mDiscountType == 1) {
                                mCheckOutPromoCodeAmtTxt.setText("- " + String.valueOf(Double.parseDouble(formatter.format(mDiscountValue))));
                                mCheckOurTotalAmtTxt.setText(String.valueOf(dedicatedPointAmt - mDiscountValue));
                                mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
                                AppConstants.APPLY_CODE_AMT = String.valueOf(mDiscountValue);
                            } else if (mDiscountType == 2) {
                                double res = (Amount / 100.0f) * mDiscountValue;
                                if (res > mMaximumDiscount) {
                                    mCheckOutPromoCodeAmtTxt.setText("- " + String.valueOf(Double.parseDouble(formatter.format(mMaximumDiscount))));
                                    mCheckOurTotalAmtTxt.setText(String.valueOf(dedicatedPointAmt - mMaximumDiscount));
                                    mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
                                    AppConstants.APPLY_CODE_AMT = String.valueOf(mMaximumDiscount);
                                } else {
                                    mCheckOutPromoCodeAmtTxt.setText("- " + String.valueOf(Double.parseDouble(formatter.format(res))));
                                    mCheckOurTotalAmtTxt.setText(String.valueOf(dedicatedPointAmt - res));
                                    mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
                                    AppConstants.APPLY_CODE_AMT = String.valueOf(res);
                                }
                            }
                            mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.remove));
                        } else {
                            AppConstants.APPLY_CODE = "";
                            AppConstants.APPLY_CODE_ID = "";
                            AppConstants.APPLY_CODE_AMT = "0";
                            mCheckOutSubTotalLay.setVisibility(View.GONE);
                            mCheckOutPromoCodeAmtLay.setVisibility(View.GONE);
                            mCheckOutPromoCodeAmtTxt.setText("0");
                            mCheckOurTotalAmtTxt.setText(AppConstants.TRANSACTION_AMOUNT);
                            Toast.makeText(CheckoutScreen.this, getResources().getString(R.string.invalid_promo_code), Toast.LENGTH_SHORT).show();
                            mCheckOutPromoCodeEdtTxt.clearAnimation();
                            mCheckOutPromoCodeEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                            mCheckOutPromoCodeEdtTxt.setError(getResources().getString(R.string.invalid_promo_code));
                            mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.remove));
                        }
                    }
                }
//                else if (mCouponAppliesTo == 4) {
//                    double Amount = 0.0;
//                    for (int i = 0; i < AppConstants.GET_ITEM_CARD_LIST.size(); i++) {
//                        if (mCouponType.equalsIgnoreCase(String.valueOf(AppConstants.GET_ITEM_CARD_LIST.get(i).getBrandId()))) {
//                            Amount = Amount + AppConstants.GET_ITEM_CARD_LIST.get(i).getAmount()*AppConstants.GET_ITEM_CARD_LIST.get(i).getQuantity();
//                        }
//                    }
//                    if (Amount == 0.0) {
//                        AppConstants.APPLY_CODE = "";
//                        AppConstants.APPLY_CODE_ID = "";
//                        AppConstants.APPLY_CODE_AMT = "0";
//                        mCheckOutSubTotalLay.setVisibility(View.GONE);
//                        mCheckOutPromoCodeAmtLay.setVisibility(View.GONE);
//                        mCheckOutPromoCodeAmtTxt.setText("0");
//                        mCheckOurTotalAmtTxt.setText(AppConstants.TRANSACTION_AMOUNT);
//                        Toast.makeText(CheckoutScreen.this, getResources().getString(R.string.invalid_promo_code), Toast.LENGTH_SHORT).show();
//                        mCheckOutPromoCodeEdtTxt.clearAnimation();
//                        mCheckOutPromoCodeEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
//                        mCheckOutPromoCodeEdtTxt.setError(getResources().getString(R.string.invalid_promo_code));
//                    } else {
//                        if (Amount > minimumamount) {
//                            if (mDiscountType == 1) {
//                                mCheckOutPromoCodeAmtTxt.setText("- " + String.valueOf(Double.parseDouble(formatter.format(mDiscountValue))));
//                                mCheckOurTotalAmtTxt.setText(String.valueOf(Double.valueOf(formatter.format(dedicatedPointAmt - mDiscountValue))));
//                                mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
//                                AppConstants.APPLY_CODE_AMT = String.valueOf(mDiscountValue);
//                            } else if (mDiscountType == 2) {
//                                double res = (Amount / 100.0f) * mDiscountValue;
//                                if (res > mMaximumDiscount) {
//                                    mCheckOutPromoCodeAmtTxt.setText("- " + String.valueOf(Double.parseDouble(formatter.format(mMaximumDiscount))));
//                                    mCheckOurTotalAmtTxt.setText(String.valueOf(Double.valueOf(formatter.format(dedicatedPointAmt - mMaximumDiscount))));
//                                    mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
//                                    AppConstants.APPLY_CODE_AMT = String.valueOf(mMaximumDiscount);
//                                } else {
//                                    mCheckOutPromoCodeAmtTxt.setText("- " + String.valueOf(Double.parseDouble(formatter.format(res))));
//                                    mCheckOurTotalAmtTxt.setText(String.valueOf(Double.valueOf(formatter.format(dedicatedPointAmt - res))));
//                                    mCheckOutPromoCodeAmtLay.setVisibility(View.VISIBLE);
//                                    AppConstants.APPLY_CODE_AMT = String.valueOf(res);
//                                }
//                            }
//                        } else {
//                            AppConstants.APPLY_CODE = "";
//                            AppConstants.APPLY_CODE_ID = "";
//                            AppConstants.APPLY_CODE_AMT = "0";
//                            mCheckOutSubTotalLay.setVisibility(View.GONE);
//                            mCheckOutPromoCodeAmtLay.setVisibility(View.GONE);
//                            mCheckOutPromoCodeAmtTxt.setText("0");
//                            mCheckOurTotalAmtTxt.setText(AppConstants.TRANSACTION_AMOUNT);
//                            Toast.makeText(CheckoutScreen.this, getResources().getString(R.string.invalid_promo_code), Toast.LENGTH_SHORT).show();
//                            mCheckOutPromoCodeEdtTxt.clearAnimation();
//                            mCheckOutPromoCodeEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
//                            mCheckOutPromoCodeEdtTxt.setError(getResources().getString(R.string.invalid_promo_code));
//                        }
//                    }
//                }
            }

        }else if (mCheckOutPromoCodeEdtTxt.getText().toString().trim().equalsIgnoreCase("")&&!AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")){
            mDiscountValue = 0;
            mDiscountType = 0;
            minimumamount = 0;
            mMaximumDiscount = 0;
            mCouponType = "0";
            mCouponAppliesTo = 0;
            AppConstants.APPLY_CODE_AMT = "0";
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat formatter = (DecimalFormat)nf;
            formatter.applyPattern("######.00");
            mCheckOutPromoCodeAmtTxt.setText("- " + String.valueOf(Double.parseDouble(formatter.format(mDiscountValue))));

            mCheckoutPromoCodeApplyTxt.setText(getResources().getString(R.string.apply));
            String dedicatedPointStr = String.valueOf(Double.valueOf(formatter.format(Double.parseDouble(AppConstants.TRANSACTION_AMOUNT) - Double.parseDouble(AppConstants.REDEEM_POINT_TO_CASH))));
            mCheckOutRedeemAmtLay.setVisibility(View.VISIBLE);
            mCheckOutSubTotalLay.setVisibility(View.VISIBLE);
            mCheckOutPromoCodeAmtLay.setVisibility(View.GONE);
            mCheckOutSubTotalTxt.setText(AppConstants.TRANSACTION_AMOUNT);
            mCheckOutRedeemAmtTxt.setText("- "+String.valueOf(Double.valueOf(formatter.format(Double.parseDouble(AppConstants.REDEEM_POINT_TO_CASH)))));
            mCheckOurTotalAmtTxt.setText(dedicatedPointStr);
        }
    }

    @Override
    public void onBackPressed() {
            if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")||AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
               DialogManager.getInstance().showOptionPopup(CheckoutScreen.this, getResources().getString(R.string.sure_want_to_go_back), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                   @Override
                   public void onNegativeClick() {

                   }
                   @Override
                   public void onPositiveClick() {
                       AppConstants.APPLY_CODE = "";
                       AppConstants.APPLY_CODE_ID = "";
                       AppConstants.REDEEM_POINT_TO_CASH = "0";
                       AppConstants.REDEEM_SELECTED_REDEEM = "";
                       AppConstants.SUB_TOTAL_CASH = "0";
                       AppConstants.REDEEM_POINT_TO_POINTS  = "0";
                       AppConstants.APPLY_CODE_AMT = "0";
                       AppConstants.STORE_AMT = "0";
                       AppConstants.STICTHING_AMT = "0";
                       AppConstants.TRANSACTION_AMOUNT = "0";
                       previousScreen(BottomNavigationScreen.class,true);
                   }
               });
            }
            else {
//                if (!AppConstants.DIRECT_ORDER.equalsIgnoreCase("QUOTATION")){
//                    AppConstants.APPLY_CODE = "";
//                    AppConstants.APPLY_CODE_ID = "";
//                    AppConstants.REDEEM_POINT_TO_CASH = "0";
//                    AppConstants.REDEEM_SELECTED_REDEEM = "";
//                    AppConstants.SUB_TOTAL_CASH = "0";
//                    AppConstants.REDEEM_POINT_TO_POINTS  = "0";
//                    AppConstants.APPLY_CODE_AMT = "0";
//                    AppConstants.STORE_AMT = "0";
//                    AppConstants.STICTHING_AMT = "0";
//                    AppConstants.TRANSACTION_AMOUNT = "0";
//                }
                super.onBackPressed();
            }

            this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);
    }
}
