package com.qoltech.mzyoonbuyer.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.GetAppointmentTimeSlotAdapter;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.AppointmentMaterialResponse;
import com.qoltech.mzyoonbuyer.modal.AppointmentMeasurementResponse;
import com.qoltech.mzyoonbuyer.modal.ApproveMaterialResponse;
import com.qoltech.mzyoonbuyer.modal.ApproveMeasurementResponse;
import com.qoltech.mzyoonbuyer.modal.GetMaterialFromDateResponse;
import com.qoltech.mzyoonbuyer.modal.GetMeasurementFromDateResponse;
import com.qoltech.mzyoonbuyer.modal.InsertAppointmentMaterialResponse;
import com.qoltech.mzyoonbuyer.modal.InsertAppointmentMeasurementResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.SelectDateFragment;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class AppointmentDetails extends BaseActivity {

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.appointment_material_status_txt)
    TextView mAppointmentMaterialStatusTxt;

    @BindView(R.id.appointment_material_type_header_txt)
    TextView mAppointmentMaterialTypeHeaderTxt;

    @BindView(R.id.appointment_material_type_body_img)
    ImageView mAppointmentMaterialBodyImg;

    @BindView(R.id.material_par_lay)
    LinearLayout mMaterialParLay;

    @BindView(R.id.appointment_measurement_status_txt)
    TextView mAppointmentMeasurementStatusTxt;

    @BindView(R.id.appointment_measurment_type_header_txt)
    TextView mAppointmentMeasurementTypeHeaderTxt;

    @BindView(R.id.appointment_measurement_type_body_img)
    ImageView mAppointmentMeasurementBodyImg;

    @BindView(R.id.measurement_par_lay)
    LinearLayout mMeasurementParLay;

    @BindView(R.id.appointment_details_par_lay)
    RelativeLayout mAppointmentDetailsPayLay;

    @BindView(R.id.material_approve_reject_lay)
    LinearLayout mMaterialApproveRejectLay;

    @BindView(R.id.measurement_approve_reject_lay)
    LinearLayout mMeasurementApproveRejectLay;

    @BindView(R.id.material_save_btn_lay)
    RelativeLayout mMaterialSaveBtnLay;

    @BindView(R.id.measurement_save_btn_lay)
    RelativeLayout mMeasurementSaveBtnLay;

    @BindView(R.id.material_type_time_slot_txt)
    public TextView mMaterialTypeTimeSlotTxt;

    @BindView(R.id.measurement_type_time_slot_txt)
    public TextView mMeasurementTypeTimeSlotTxt;

    @BindView(R.id.appointment_type_fromt_date_txt)
    public TextView mMaterialTypeFromDateTxt;

    @BindView(R.id.measurement_type_from_date_txt)
    public TextView mMeasurementTypeFromDateTxt;

    @BindView(R.id.order_type_txt_lay)
    RelativeLayout mOrderTypeTxtLay;

    @BindView(R.id.order_type_txt)
    TextView mOrderTypeTxt;

    @BindView(R.id.measurement_type_txt__lay)
    RelativeLayout mMeasurementTypeTxtLay;

    @BindView(R.id.measurement_type_txt)
    TextView mMeasurementTypeTxt;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    @BindView(R.id.material_rejected_reason_heading_txt)
    TextView mMaterialRejectedReasonHeadingTxt;

    @BindView(R.id.material_rejected_reason_lay)
    CardView mMaterialRejectedReasonLay;

    @BindView(R.id.material_rejected_tailor_name_txt)
    TextView mMaterialRejectedTailorNameTxt;

    @BindView(R.id.material_rejected_date_txt)
    TextView mMaterialRejectedDateTxt;

    @BindView(R.id.material_rejected_time_txt)
    TextView mMaterialRejectedTimeTxt;

    @BindView(R.id.material_rejected_on_txt)
    TextView mMaterialRejectedOnTxt;

    @BindView(R.id.material_rejected_reason_txt)
    TextView mMaterialRejectedReasonTxt;

    @BindView(R.id.measurement_rejected_reason_heading_txt)
    TextView mMeasurementRejectedReasonHeadingTxt;

    @BindView(R.id.measurement_rejected_reason_lay)
    CardView mMeasurementRejectedReasonLay;

    @BindView(R.id.measurement_rejected_tailor_name_txt)
    TextView mMeasurementRejectedTailorNameTxt;

    @BindView(R.id.measurement_rejected_date_txt)
    TextView mMeasurementRejectedDateTxt;

    @BindView(R.id.measurement_rejected_time_txt)
    TextView mMeasurementRejectedTimeTxt;

    @BindView(R.id.measurement_rejected_on_txt)
    TextView mMeasurementRejectedOnTxt;

    @BindView(R.id.measurement_rejected_reason_txt)
    TextView mMeasurementRejectedReasonTxt;

    private UserDetailsEntity mUserDetailsEntityRes;

    Dialog mAppointmentReschedule,mAppointmentReject,mAppointmentTimeDialog;

    private GetAppointmentTimeSlotAdapter mGetAppointmentTimeSlotAdater;

    private ArrayList<String> mAppointmentTimeSlotList;

    private String mOrderTypeBoolStr="",mMeasurementTypeBoolStr="",mMaterialEdit="",mMeasurementEdit="",mOrderTypeViewBool = "",mMeasurementTypeViewBool = "",mOrderTypeFromDateStr = "",mOrderTypeToDateStr = "",mOrderTimeStr = "",mMeasurementFromDateStr = "",mMeasurementToDateStr="",mMeasurementTimeStr = "",mOrderStatusStr = "",mMeasurementStatusStr= "",mOrderStr = "",mMeasurementStr="",mBackHandleBool = "true";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_appointment_details);
        initView();
    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mAppointmentDetailsPayLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(AppointmentDetails.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        addTimeSlotList();

        getAppointmentMaterialApiCall();

        getAppointmentMeasurementApiCall();

        getLanguage();

        AppConstants.DATE_FOR_RESTRICT_MATERIAL = "";
        AppConstants.DATE_FOR_RESTRICT_MEASUREMENT = "";

        mOrderTypeTxtLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
        mOrderTypeTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
        mMeasurementTypeTxtLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
        mMeasurementTypeTxt.setTextColor(getResources().getColor(R.color.white));
        if (mOrderTypeViewBool.equalsIgnoreCase("true")){
            mMaterialParLay.setVisibility(View.VISIBLE);
            mEmptyListLay.setVisibility(View.GONE);
        }else {
            mMaterialParLay.setVisibility(View.GONE);
            mEmptyListLay.setVisibility(View.VISIBLE);
            if (mOrderStr.equalsIgnoreCase("1")){
                mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_update_soon));
            }else {
                mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_for_order));
            }
        }
    }

    public void addTimeSlotList(){
        mAppointmentTimeSlotList = new ArrayList<>();
        mAppointmentTimeSlotList.add("09:00 am to 11:00 am");
        mAppointmentTimeSlotList.add("02:00 pm to 03:00 pm");
        mAppointmentTimeSlotList.add("04:00 pm to 05:00 pm");
    }

    @OnClick({R.id.header_left_side_img,R.id.appointment_material_approve_lay,R.id.appointment_material_reject_lay,R.id.appointment_material_save_btn,R.id.appointment_measurement_approve_lay,R.id.appointment_measurement_reject_lay,R.id.appointment_measurement_save_btn,R.id.material_type_time_slot_par_lay,R.id.measurement_type_time_slot_lay,R.id.material_type_calender_from_par_lay,R.id.measurement_calender_from_date_par_lay,R.id.order_type_txt_lay,R.id.measurement_type_txt__lay,R.id.appointment_material_status_txt,R.id.appointment_measurement_status_txt,R.id.appointment_continue_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                    onBackPressed();
                break;
            case R.id.appointment_material_approve_lay:
                if (mMaterialTypeFromDateTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.appointment_date_hint))||mMaterialTypeTimeSlotTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.appointment_time_hint))){
                    DialogManager.getInstance().showAlertPopup(AppointmentDetails.this, getResources().getString(R.string.no_appointment_to_approve), new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {

                        }
                    });
                }else {
                    approveMaterialApiCall(AppConstants.APPOINTMENT_MATERIAL_ID,"1","Approve");
                }
                break;
            case R.id.appointment_material_reject_lay:
                if (mMaterialTypeFromDateTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.appointment_date_hint))||mMaterialTypeTimeSlotTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.appointment_time_hint))){
                    DialogManager.getInstance().showAlertPopup(AppointmentDetails.this, getResources().getString(R.string.no_appointment_to_reject), new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {

                        }
                    });
                }else {
                    alertDismiss(mAppointmentReject);
                    mAppointmentReject = getDialog(AppointmentDetails.this, R.layout.pop_up_appointment);
                    WindowManager.LayoutParams LayoutParam = new WindowManager.LayoutParams();
                    Window windows = mAppointmentReject.getWindow();
                    if (windows != null) {
                        LayoutParam.copyFrom(windows.getAttributes());
                        LayoutParam.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParam.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        windows.setAttributes(LayoutParam);
                        windows.setGravity(Gravity.CENTER);
                    }
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mAppointmentReject.findViewById(R.id.pop_up_appointment_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                    } else {
                        ViewCompat.setLayoutDirection(mAppointmentReject.findViewById(R.id.pop_up_appointment_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }
                    RelativeLayout cancelLays, saveLay;
                    EditText mEditText;
                    cancelLays = mAppointmentReject.findViewById(R.id.appointment_popup_cancel_lay);
                    saveLay = mAppointmentReject.findViewById(R.id.appointment_popup_save_lay);
                    mEditText = mAppointmentReject.findViewById(R.id.appointment_popup_edt_txt);
                    cancelLays.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAppointmentReject.dismiss();
                        }
                    });

                    saveLay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mEditText.getText().toString().equalsIgnoreCase("")) {
                                mEditText.clearAnimation();
                                mEditText.setAnimation(ShakeErrorUtils.shakeError());
                                mEditText.setError(getResources().getString(R.string.please_give_reason));
                            } else {
                                approveMaterialApiCall(AppConstants.APPOINTMENT_MATERIAL_ID, "2", mEditText.getText().toString());
                            }

                            mAppointmentReject.dismiss();
                        }
                    });

                    alertShowing(mAppointmentReject);
                }
                break;
            case R.id.appointment_material_save_btn:
                if (mMaterialTypeFromDateTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.appointment_date_hint))){
               mMaterialTypeFromDateTxt.clearAnimation();
               mMaterialTypeFromDateTxt.setAnimation(ShakeErrorUtils.shakeError());
               mMaterialTypeFromDateTxt.setError(getResources().getString(R.string.please_give_date));
                }

                else if (mMaterialTypeTimeSlotTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.appointment_time_hint))){
                    mMaterialTypeTimeSlotTxt.clearAnimation();
                    mMaterialTypeTimeSlotTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mMaterialTypeTimeSlotTxt.setError(getResources().getString(R.string.please_give_time));
                }
                else {
                    if (mOrderStatusStr.equalsIgnoreCase("Rejected")){
                        if (mOrderTypeFromDateStr.equalsIgnoreCase(mMaterialTypeFromDateTxt.getText().toString().replace("/"," "))&&
                                mOrderTimeStr.equalsIgnoreCase(mMaterialTypeTimeSlotTxt.getText().toString())||mOrderTypeFromDateStr.equalsIgnoreCase(mMaterialTypeFromDateTxt.getText().toString().replace("/"," "))){
                            DialogManager.getInstance().showAlertPopup(AppointmentDetails.this, getResources().getString(R.string.appointment_date_and_time_already_rejected), new InterfaceBtnCallBack() {
                                @Override
                                public void onPositiveClick() {

                                }
                            });
                        }else {
                            insertAppointmentMaterialApiCall();
                        }
                    }else {
                        insertAppointmentMaterialApiCall();
                    }
                }
                break;
            case R.id.appointment_measurement_approve_lay:
                if (mMeasurementTypeFromDateTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.appointment_date_hint))||mMeasurementTypeTimeSlotTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.appointment_time_hint))){
                    DialogManager.getInstance().showAlertPopup(AppointmentDetails.this, getResources().getString(R.string.no_appointment_to_approve), new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {

                        }
                    });
                }else {
                    approveMeasurementApiCall(AppConstants.APPOINTMENT_MEASUREMENT_ID, "1", "Approve");
                }
                break;

            case R.id.appointment_measurement_reject_lay:
                if (mMeasurementTypeFromDateTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.appointment_date_hint))||mMeasurementTypeTimeSlotTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.appointment_time_hint))){
                    DialogManager.getInstance().showAlertPopup(AppointmentDetails.this, getResources().getString(R.string.no_appointment_to_reject), new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {

                        }
                    });
                }else {
                    alertDismiss(mAppointmentReject);
                    mAppointmentReject = getDialog(AppointmentDetails.this, R.layout.pop_up_appointment);
                    WindowManager.LayoutParams LayoutParamRe = new WindowManager.LayoutParams();
                    Window windowsRe = mAppointmentReject.getWindow();
                    if (windowsRe != null) {
                        LayoutParamRe.copyFrom(windowsRe.getAttributes());
                        LayoutParamRe.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParamRe.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        windowsRe.setAttributes(LayoutParamRe);
                        windowsRe.setGravity(Gravity.CENTER);
                    }
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mAppointmentReject.findViewById(R.id.pop_up_appointment_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                    } else {
                        ViewCompat.setLayoutDirection(mAppointmentReject.findViewById(R.id.pop_up_appointment_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }
                    RelativeLayout cancelLaysRe, saveLayRe;
                    EditText mEditTextRe;
                    cancelLaysRe = mAppointmentReject.findViewById(R.id.appointment_popup_cancel_lay);
                    saveLayRe = mAppointmentReject.findViewById(R.id.appointment_popup_save_lay);
                    mEditTextRe = mAppointmentReject.findViewById(R.id.appointment_popup_edt_txt);

                    cancelLaysRe.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAppointmentReject.dismiss();
                        }
                    });

                    saveLayRe.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mEditTextRe.getText().toString().equalsIgnoreCase("")) {
                                mEditTextRe.clearAnimation();
                                mEditTextRe.setAnimation(ShakeErrorUtils.shakeError());
                                mEditTextRe.setError(getResources().getString(R.string.please_give_reason));
                            } else {
                                approveMeasurementApiCall(AppConstants.APPOINTMENT_MEASUREMENT_ID, "2", mEditTextRe.getText().toString());
                            }
                            mAppointmentReject.dismiss();
                        }
                    });

                    alertShowing(mAppointmentReject);
                }
                break;
            case R.id.appointment_measurement_save_btn:
                if (mMeasurementTypeFromDateTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.appointment_date_hint))){
                    mMeasurementTypeFromDateTxt.clearAnimation();
                    mMeasurementTypeFromDateTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mMeasurementTypeFromDateTxt.setError(getResources().getString(R.string.please_give_date));
                }
                else if (mMeasurementTypeTimeSlotTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.appointment_time_hint))){
                    mMeasurementTypeTimeSlotTxt.clearAnimation();
                    mMeasurementTypeTimeSlotTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mMeasurementTypeTimeSlotTxt.setError(getResources().getString(R.string.please_give_time));
                }
                else {
                    if (mMeasurementStatusStr.equalsIgnoreCase("Rejected")){
                        if (mMeasurementFromDateStr.equalsIgnoreCase(mMeasurementTypeFromDateTxt.getText().toString().replace("/"," "))&& mMeasurementTimeStr.equalsIgnoreCase(mMeasurementTypeTimeSlotTxt.getText().toString())||mMeasurementFromDateStr.equalsIgnoreCase(mMeasurementTypeFromDateTxt.getText().toString().replace("/"," "))){
                            DialogManager.getInstance().showAlertPopup(AppointmentDetails.this, getResources().getString(R.string.appointment_date_and_time_already_rejected), new InterfaceBtnCallBack() {
                                @Override
                                public void onPositiveClick() {

                                }
                            });
                        }else {
                            insertAppointmentMeasurementApiCall();
                        }
                    }else {
                        insertAppointmentMeasurementApiCall();
                    }
                }
                break;
            case R.id.material_type_time_slot_par_lay:
                if(!mMaterialEdit.equalsIgnoreCase("")){
                if (mAppointmentTimeSlotList.size() > 0){
                    alertDismiss(mAppointmentTimeDialog);
                    mAppointmentTimeDialog = getDialog(AppointmentDetails.this, R.layout.pop_up_country_alert);

                    WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                    Window window = mAppointmentTimeDialog.getWindow();

                    if (window != null) {
                        LayoutParams.copyFrom(window.getAttributes());
                        LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(LayoutParams);
                        window.setGravity(Gravity.CENTER);
                    }

                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        ViewCompat.setLayoutDirection(mAppointmentTimeDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                    }else {
                        ViewCompat.setLayoutDirection(mAppointmentTimeDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }

                    TextView cancelTxt , headerTxt;

                    RecyclerView countryRecyclerView;

                    /*Init view*/
                    cancelTxt = mAppointmentTimeDialog.findViewById(R.id.country_text_cancel);
                    headerTxt = mAppointmentTimeDialog.findViewById(R.id.country_header_text_cancel);

                    countryRecyclerView = mAppointmentTimeDialog.findViewById(R.id.country_popup_recycler_view);
                    mGetAppointmentTimeSlotAdater = new GetAppointmentTimeSlotAdapter(AppointmentDetails.this, mAppointmentTimeSlotList,mAppointmentTimeDialog,"ORDER_TYPE");

                    countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    countryRecyclerView.setAdapter(mGetAppointmentTimeSlotAdater);

                    /*Set data*/
                    headerTxt.setText(getResources().getString(R.string.choose_your_time_slot));
                    cancelTxt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAppointmentTimeDialog.dismiss();
                        }
                    });

                    alertShowing(mAppointmentTimeDialog);
                }else {
                    Toast.makeText(AppointmentDetails.this,getResources().getString(R.string.sorry_no_result_found) , Toast.LENGTH_SHORT).show();
                }
                }
                break;
            case R.id.measurement_type_time_slot_lay:
                if (!mMeasurementEdit.equalsIgnoreCase("")) {
                    if (mAppointmentTimeSlotList.size() > 0) {
                        alertDismiss(mAppointmentTimeDialog);
                        mAppointmentTimeDialog = getDialog(AppointmentDetails.this, R.layout.pop_up_country_alert);

                        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                        Window window = mAppointmentTimeDialog.getWindow();

                        if (window != null) {
                            LayoutParams.copyFrom(window.getAttributes());
                            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(LayoutParams);
                            window.setGravity(Gravity.CENTER);
                        }

                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                            ViewCompat.setLayoutDirection(mAppointmentTimeDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                        } else {
                            ViewCompat.setLayoutDirection(mAppointmentTimeDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                        }

                        TextView cancelTxt, headerTxt;

                        RecyclerView countryRecyclerView;

                        /*Init view*/
                        cancelTxt = mAppointmentTimeDialog.findViewById(R.id.country_text_cancel);
                        headerTxt = mAppointmentTimeDialog.findViewById(R.id.country_header_text_cancel);

                        countryRecyclerView = mAppointmentTimeDialog.findViewById(R.id.country_popup_recycler_view);
                        mGetAppointmentTimeSlotAdater = new GetAppointmentTimeSlotAdapter(AppointmentDetails.this, mAppointmentTimeSlotList, mAppointmentTimeDialog, "MEASUREMENT_TYPE");

                        countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        countryRecyclerView.setAdapter(mGetAppointmentTimeSlotAdater);

                        /*Set data*/
                        headerTxt.setText(getResources().getString(R.string.choose_your_time_slot));
                        cancelTxt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mAppointmentTimeDialog.dismiss();
                            }
                        });

                        alertShowing(mAppointmentTimeDialog);
                    } else {
                        Toast.makeText(AppointmentDetails.this, getResources().getString(R.string.sorry_no_result_found), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.material_type_calender_from_par_lay:
                if(!mMaterialEdit.equalsIgnoreCase("")) {
                    AppConstants.DATE_PICKER_COND = "APPOINTMENT_MATERIAL_FROM";
                    DialogFragment newFragment = new SelectDateFragment();
                    newFragment.show(getSupportFragmentManager(), "DatePicker");
                }
                break;
            case R.id.measurement_calender_from_date_par_lay:
                if(!mMeasurementEdit.equalsIgnoreCase("")) {
                    AppConstants.DATE_PICKER_COND = "APPOINTMENT_MEASUREMENT_FROM";
                    DialogFragment newFragmentm = new SelectDateFragment();
                    newFragmentm.show(getSupportFragmentManager(), "DatePicker");
                }
                break;
            case R.id.order_type_txt_lay:
                mOrderTypeTxtLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
                mOrderTypeTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                mMeasurementTypeTxtLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
                mMeasurementTypeTxt.setTextColor(getResources().getColor(R.color.white));
                if (mOrderTypeViewBool.equalsIgnoreCase("true")){
                    mMaterialParLay.setVisibility(View.VISIBLE);
                    mEmptyListLay.setVisibility(View.GONE);
                }else {
                    mMaterialParLay.setVisibility(View.GONE);
                    mEmptyListLay.setVisibility(View.VISIBLE);
                    if (mOrderStr.equalsIgnoreCase("1")){
                        mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_update_soon));
                    }else {
                        mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_for_order));
                    }
                }
                mMeasurementParLay.setVisibility(View.GONE);
                break;
            case R.id.measurement_type_txt__lay:
                mOrderTypeTxtLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                mOrderTypeTxt.setTextColor(getResources().getColor(R.color.white));
                mMeasurementTypeTxtLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                mMeasurementTypeTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                if (mMeasurementTypeViewBool.equalsIgnoreCase("true")){
                    mMeasurementParLay.setVisibility(View.VISIBLE);
                    mEmptyListLay.setVisibility(View.GONE);
                }else {
                    mMeasurementParLay.setVisibility(View.GONE);
                    mEmptyListLay.setVisibility(View.VISIBLE);
                    if (mMeasurementStr.equalsIgnoreCase("2")){
                        mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_update_soon));
                    }else {
                        mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_for_measurement));
                    }

                }
                mMaterialParLay.setVisibility(View.GONE);
                break;
            case R.id.appointment_material_status_txt:
                if (mOrderStatusStr.equalsIgnoreCase("Rejected")){
                    alertDismiss(mAppointmentReschedule);
                    mAppointmentReschedule = getDialog(AppointmentDetails.this, R.layout.pop_up_appointment_reschedule);

                    WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                    Window window = mAppointmentReschedule.getWindow();

                    if (window != null) {
                        LayoutParams.copyFrom(window.getAttributes());
                        LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(LayoutParams);
                        window.setGravity(Gravity.CENTER);
                    }

                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mAppointmentReschedule.findViewById(R.id.pop_up_appointment_reschedule_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                    } else {
                        ViewCompat.setLayoutDirection(mAppointmentReschedule.findViewById(R.id.pop_up_appointment_reschedule_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }

                    RelativeLayout mOkRal,mResRal;

                    /*Init view*/
                    mOkRal = mAppointmentReschedule.findViewById(R.id.appointment_reschedule_ok_lay);
                    mResRal = mAppointmentReschedule.findViewById(R.id.pop_up_appointment_reschedule_par_lay);

                    mOkRal.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAppointmentReschedule.dismiss();
                        }
                    });
                    mResRal.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAppointmentReschedule.dismiss();

                        }
                    });

                    alertShowing(mAppointmentReschedule);
                }
                break;
            case R.id.appointment_measurement_status_txt:
                if (mMeasurementStatusStr.equalsIgnoreCase("Rejected")){
                    alertDismiss(mAppointmentReschedule);
                    mAppointmentReschedule = getDialog(AppointmentDetails.this, R.layout.pop_up_appointment_reschedule);

                    WindowManager.LayoutParams LayoutParamss = new WindowManager.LayoutParams();
                    Window windowss = mAppointmentReschedule.getWindow();

                    if (windowss != null) {
                        LayoutParamss.copyFrom(windowss.getAttributes());
                        LayoutParamss.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParamss.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        windowss.setAttributes(LayoutParamss);
                        windowss.setGravity(Gravity.CENTER);
                    }

                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mAppointmentReschedule.findViewById(R.id.pop_up_appointment_reschedule_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                    } else {
                        ViewCompat.setLayoutDirection(mAppointmentReschedule.findViewById(R.id.pop_up_appointment_reschedule_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }

                    RelativeLayout mOkRals,mResRals;

                    /*Init view*/
                    mOkRals = mAppointmentReschedule.findViewById(R.id.appointment_reschedule_ok_lay);
                    mResRals = mAppointmentReschedule.findViewById(R.id.appointment_reschedule_check_lay);

                    mOkRals.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAppointmentReschedule.dismiss();
                        }
                    });
                    mResRals.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAppointmentReschedule.dismiss();

                        }
                    });

                    alertShowing(mAppointmentReschedule);
                }
                break;
            case R.id.appointment_continue_btn:
                if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                    if (mOrderTypeBoolStr.equalsIgnoreCase("true")&&mMeasurementTypeBoolStr.equalsIgnoreCase("true")) {
                        if (AppConstants.PAYMENT_STATUS.equalsIgnoreCase("Not Paid")) {
                            nextScreen(CheckoutScreen.class, true);
                        } else {
                            onBackPressed();
                        }
                    }else if (!mOrderTypeBoolStr.equalsIgnoreCase("true")){
                        Toast.makeText(AppointmentDetails.this,getResources().getString(R.string.material_continue_btn_txt),Toast.LENGTH_SHORT).show();
                    }else if (!mMeasurementTypeBoolStr.equalsIgnoreCase("true")){
                        Toast.makeText(AppointmentDetails.this,getResources().getString(R.string.measurement_continue_btn_txt),Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    if (mOrderTypeBoolStr.equalsIgnoreCase("true")&&mMeasurementTypeBoolStr.equalsIgnoreCase("true")) {
                        nextScreen(BottomNavigationScreen.class, true);
                    }else if (!mOrderTypeBoolStr.equalsIgnoreCase("true")){
                        Toast.makeText(AppointmentDetails.this,getResources().getString(R.string.material_continue_btn_txt),Toast.LENGTH_SHORT).show();
                    }else if (!mMeasurementTypeBoolStr.equalsIgnoreCase("true")){
                        Toast.makeText(AppointmentDetails.this,getResources().getString(R.string.measurement_continue_btn_txt),Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    public void approveMaterialApiCall(String AppointmentId,String IsApproved,String Reason){
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)){
            APIRequestHandler.getInstance().approveMaterialApiCall(AppointmentId,IsApproved,Reason,AppointmentDetails.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    approveMaterialApiCall(AppointmentId,IsApproved,Reason);
                }
            });
        }
    }

    public void approveMeasurementApiCall(String AppointmentId,String IsApproved,String Reason){
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)){
            APIRequestHandler.getInstance().approveMeasurementApiCall(AppointmentId,IsApproved,Reason,AppointmentDetails.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    approveMeasurementApiCall(AppointmentId,IsApproved,Reason);
                }
            });
        }
    }
    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.appointment));
        mRightSideImg.setVisibility(View.INVISIBLE);

        mMaterialParLay.setVisibility(View.GONE);
        mMeasurementParLay.setVisibility(View.GONE);

        mEmptyListTxt.setText(getResources().getString(R.string.no_result_found));
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);

        }

    public void insertAppointmentMaterialApiCall(){
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)){

            if ( AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")){
                APIRequestHandler.getInstance().insertAppointmentMaterialApi(AppConstants.APPOINTMENT_LIST_ID,"1",mMaterialTypeTimeSlotTxt.getText().toString(),mMaterialTypeFromDateTxt.getText().toString(),mMaterialTypeFromDateTxt.getText().toString(),mUserDetailsEntityRes.getUSER_ID(),"Buyer",mUserDetailsEntityRes.getUSER_NAME(),AppointmentDetails.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().insertAppointmentMaterialApi(AppConstants.REQUEST_LIST_ID,"1",mMaterialTypeTimeSlotTxt.getText().toString(),mMaterialTypeFromDateTxt.getText().toString(),mMaterialTypeFromDateTxt.getText().toString(),mUserDetailsEntityRes.getUSER_ID(),"Buyer",mUserDetailsEntityRes.getUSER_NAME(),AppointmentDetails.this);

            }

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    insertAppointmentMaterialApiCall();
                }
            });
        }
    }

    public void insertAppointmentMeasurementApiCall(){
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)){

            if ( AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")){
                APIRequestHandler.getInstance().insertAppointmentMeasurementApi(AppConstants.APPOINTMENT_LIST_ID,"2",mMeasurementTypeTimeSlotTxt.getText().toString(),mMeasurementTypeFromDateTxt.getText().toString(),mMeasurementTypeFromDateTxt.getText().toString(),mUserDetailsEntityRes.getUSER_ID(),"Buyer",mUserDetailsEntityRes.getUSER_NAME(),AppointmentDetails.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().insertAppointmentMeasurementApi(AppConstants.REQUEST_LIST_ID,"2",mMeasurementTypeTimeSlotTxt.getText().toString(),mMeasurementTypeFromDateTxt.getText().toString(),mMeasurementTypeFromDateTxt.getText().toString(),mUserDetailsEntityRes.getUSER_ID(),"Buyer",mUserDetailsEntityRes.getUSER_NAME(),AppointmentDetails.this);

            }

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    insertAppointmentMeasurementApiCall();
                }
            });
        }
    }

    public void getAppointmentMaterialApiCall(){
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)){
            if ( AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")){
                APIRequestHandler.getInstance().getAppointmentMaterialApi(AppConstants.APPOINTMENT_LIST_ID,AppointmentDetails.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().getAppointmentMaterialApi(AppConstants.REQUEST_LIST_ID,AppointmentDetails.this);

            }

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentMaterialApiCall();
                }
            });
        }
    }

    public void getAppointmentMeasurementApiCall(){
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)){
            if ( AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")) {
                APIRequestHandler.getInstance().getAppointmentMeasurementApi(AppConstants.APPOINTMENT_LIST_ID,AppointmentDetails.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")){
                APIRequestHandler.getInstance().getAppointmentMeasurementApi(AppConstants.REQUEST_LIST_ID,AppointmentDetails.this);

            }

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentMeasurementApiCall();
                }
            });
        }
    }

    public void getAppointmentMaterialDateApiCall(String TypeId,String Type){
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)){
            if ( AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")){
                APIRequestHandler.getInstance().getMaterialDateApiCall(AppConstants.APPOINTMENT_LIST_ID,TypeId,Type,AppointmentDetails.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().getMaterialDateApiCall(AppConstants.REQUEST_LIST_ID,TypeId,Type,AppointmentDetails.this);

            }

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentMaterialDateApiCall(TypeId,Type);
                }
            });
        }
    }

    public void getAppointmentMeasurementDateApiCall(String TypeId ,String Type){
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)){
            if ( AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")) {
                APIRequestHandler.getInstance().getMeasurementDateApiCall(AppConstants.APPOINTMENT_LIST_ID,TypeId,Type,AppointmentDetails.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")){
                APIRequestHandler.getInstance().getMeasurementDateApiCall(AppConstants.REQUEST_LIST_ID,TypeId,Type,AppointmentDetails.this);

            }

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentMeasurementDateApiCall(TypeId,Type);
                }
            });
        }
    }
    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }

    }
    /*Default dialog init method*/
    public Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        AppConstants.DATE_FOR_RESTRICT_MATERIAL = "";
        AppConstants.DATE_FOR_RESTRICT_MEASUREMENT = "";
        if(resObj instanceof AppointmentMaterialResponse){
            AppointmentMaterialResponse mResponse = (AppointmentMaterialResponse)resObj;
            if (mResponse.getResult().size() > 0){
                    AppConstants.APPOINTMENT_MATERIAL_ID = String.valueOf(mResponse.getResult().get(mResponse.getResult().size()-1).getAppointmentId());
                    mOrderStatusStr = mResponse.getResult().get(mResponse.getResult().size()-1).getStatus();
                mOrderStr = String.valueOf(mResponse.getResult().get(mResponse.getResult().size()-1).getOrderTypeId());
                if (String.valueOf(mResponse.getResult().get(mResponse.getResult().size()-1).getOrderTypeId()).equalsIgnoreCase("2")){
                    mMaterialParLay.setVisibility(View.VISIBLE);
                    mMaterialApproveRejectLay.setVisibility(View.GONE);
                    mMaterialSaveBtnLay.setVisibility(View.VISIBLE);
                    mMaterialEdit = "Edit";
                    mOrderTypeViewBool = "true";
                    getAppointmentMaterialDateApiCall(mUserDetailsEntityRes.getUSER_ID(),"Buyer");
                    if (mResponse.getResult().get(mResponse.getResult().size() -1).getStatus().equalsIgnoreCase("Approved")){
                        mMaterialSaveBtnLay.setVisibility(View.GONE);
                        mMaterialEdit = "";
                        mOrderTypeBoolStr = "true";
                    }else if (mResponse.getResult().get(mResponse.getResult().size() -1).getStatus().equalsIgnoreCase("Tailor Approved")){
                        mMaterialSaveBtnLay.setVisibility(View.GONE);
                        mMaterialEdit = "";
                        mOrderTypeBoolStr = "true";

                    }

                }
                else if (String.valueOf(mResponse.getResult().get(mResponse.getResult().size()-1).getOrderTypeId()).equalsIgnoreCase("3")){
                    mMaterialParLay.setVisibility(View.GONE);
                    mOrderTypeBoolStr = "true";
                    mOrderTypeViewBool = "false";

                }
                else if (String.valueOf(mResponse.getResult().get(mResponse.getResult().size()-1).getOrderTypeId()).equalsIgnoreCase("1")){

                    if (AppConstants.DIRECT_ORDER_TYPE.equalsIgnoreCase("1")){
                        mMaterialParLay.setVisibility(View.GONE);
                        mOrderTypeBoolStr = "true";
                        mOrderTypeViewBool = "false";
                    }else {
                        mMaterialParLay.setVisibility(View.VISIBLE);
                        mMaterialApproveRejectLay.setVisibility(View.VISIBLE);
                        mMaterialSaveBtnLay.setVisibility(View.GONE);
                        mMaterialEdit = "";
                        mOrderTypeViewBool = "true";
                        getAppointmentMaterialDateApiCall(AppConstants.APPROVED_TAILOR_ID,"Tailor");

                        if (mResponse.getResult().get(mResponse.getResult().size() -1).getStatus().equalsIgnoreCase("Approved")){
                            if (AppConstants.APPROVE_REJECT_BTN.equalsIgnoreCase("true")){
                                mMaterialApproveRejectLay.setVisibility(View.VISIBLE);

                            }else {
                                mMaterialApproveRejectLay.setVisibility(View.GONE);

                            }                            mOrderTypeBoolStr = "true";
                        }else if (mResponse.getResult().get(mResponse.getResult().size() -1).getStatus().equalsIgnoreCase("Tailor Approved")){
                           if (AppConstants.APPROVE_REJECT_BTN.equalsIgnoreCase("true")){
                               mMaterialApproveRejectLay.setVisibility(View.VISIBLE);

                           }else {
                               mMaterialApproveRejectLay.setVisibility(View.GONE);

                           }
                            mOrderTypeBoolStr = "true";
                        }else if (mResponse.getResult().get(mResponse.getResult().size() -1).getStatus().equalsIgnoreCase("Rejected")){
                            if (AppConstants.APPROVE_REJECT_BTN.equalsIgnoreCase("true")){
                                mMaterialApproveRejectLay.setVisibility(View.VISIBLE);

                            }else {
                                mMaterialApproveRejectLay.setVisibility(View.GONE);

                            }
                            mOrderTypeBoolStr = "true";
                        }
                    }
                }
                mAppointmentMaterialStatusTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getStatus());

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mAppointmentMaterialTypeHeaderTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getHeaderInArabic());
                }else {
                    mAppointmentMaterialTypeHeaderTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getHeaderInEnglish());
                }

                try {
                    Glide.with(AppointmentDetails.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/OrderType/"+mResponse.getResult().get(mResponse.getResult().size()-1).getBodyImage())
                            .apply(new RequestOptions().placeholder(R.drawable.empty_img).error(R.drawable.empty_img))
                            .into(mAppointmentMaterialBodyImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

            }

            if (mOrderTypeViewBool.equalsIgnoreCase("true")){
                mMeasurementParLay.setVisibility(View.GONE);
                mMaterialParLay.setVisibility(View.VISIBLE);
                mEmptyListLay.setVisibility(View.GONE);
            }else if (mOrderTypeViewBool.equalsIgnoreCase("false")&&mMeasurementTypeViewBool.equalsIgnoreCase("true")){
                mMeasurementParLay.setVisibility(View.VISIBLE);
                mMaterialParLay.setVisibility(View.GONE);
                mEmptyListLay.setVisibility(View.GONE);
                mOrderTypeTxtLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                mOrderTypeTxt.setTextColor(getResources().getColor(R.color.white));
                mMeasurementTypeTxtLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                mMeasurementTypeTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            }
            else {
                mMaterialParLay.setVisibility(View.GONE);
                mEmptyListLay.setVisibility(View.VISIBLE);
                mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_for_order));

            }
        }
        if (resObj instanceof AppointmentMeasurementResponse){
            AppointmentMeasurementResponse mResponse = (AppointmentMeasurementResponse)resObj;
            if(mResponse.getResult().size() > 0){
                    AppConstants.APPOINTMENT_MEASUREMENT_ID = String.valueOf(mResponse.getResult().get(mResponse.getResult().size()-1).getAppointmentId());
                    mMeasurementStatusStr = mResponse.getResult().get(mResponse.getResult().size()-1).getStatus();
                    mMeasurementStr = String.valueOf(mResponse.getResult().get(mResponse.getResult().size()-1).getMeasurementTypeId());

                    if (String.valueOf(mResponse.getResult().get(mResponse.getResult().size()-1).getMeasurementTypeId()).equalsIgnoreCase("3")){
                    mMeasurementParLay.setVisibility(View.VISIBLE);
                    mMeasurementSaveBtnLay.setVisibility(View.VISIBLE);
                    mMeasurementApproveRejectLay.setVisibility(View.GONE);
                    mMeasurementEdit = "Edit";
                    mMeasurementTypeViewBool = "true";
                    getAppointmentMeasurementDateApiCall(mUserDetailsEntityRes.getUSER_ID(),"Buyer");
                        if (mResponse.getResult().get(mResponse.getResult().size() -1).getStatus().equalsIgnoreCase("Approved")){
                            mMeasurementSaveBtnLay.setVisibility(View.GONE);
                            mMeasurementEdit = "";
                            mMeasurementTypeBoolStr = "true";
                        }else if (mResponse.getResult().get(mResponse.getResult().size() -1).getStatus().equalsIgnoreCase("Tailor Approved")){
                            mMeasurementSaveBtnLay.setVisibility(View.GONE);
                            mMeasurementEdit = "";
                            mMeasurementTypeBoolStr = "true";
                        }
                }
                else if (String.valueOf(mResponse.getResult().get(mResponse.getResult().size()-1).getMeasurementTypeId()).equalsIgnoreCase("1")){
                    mMeasurementParLay.setVisibility(View.GONE);
                    mMeasurementTypeBoolStr = "true";
                    mMeasurementTypeViewBool = "false";

                }
                else if (String.valueOf(mResponse.getResult().get(mResponse.getResult().size()-1).getMeasurementTypeId()).equalsIgnoreCase("2")){
                    if (AppConstants.MEASUREMENT_TYPE.equalsIgnoreCase("2")){
                        mMeasurementParLay.setVisibility(View.GONE);
                        mMeasurementTypeBoolStr = "true";
                        mMeasurementTypeViewBool = "false";
                    }else {
                        mMeasurementParLay.setVisibility(View.VISIBLE);
                        mMeasurementApproveRejectLay.setVisibility(View.VISIBLE);
                        mMeasurementSaveBtnLay.setVisibility(View.GONE);
                        mMeasurementEdit = "";
                        mMeasurementTypeViewBool = "true";
                        getAppointmentMeasurementDateApiCall(AppConstants.APPROVED_TAILOR_ID,"Tailor");

                        if (mResponse.getResult().get(mResponse.getResult().size() -1).getStatus().equalsIgnoreCase("Approved")){
                            if (AppConstants.APPROVE_REJECT_BTN.equalsIgnoreCase("true")) {
                                mMeasurementApproveRejectLay.setVisibility(View.VISIBLE);

                            }else {
                                mMeasurementApproveRejectLay.setVisibility(View.GONE);

                            }
                                mMeasurementTypeBoolStr = "true";
                        }else if (mResponse.getResult().get(mResponse.getResult().size() -1).getStatus().equalsIgnoreCase("Tailor Approved")){
                            if (AppConstants.APPROVE_REJECT_BTN.equalsIgnoreCase("true")) {
                                mMeasurementApproveRejectLay.setVisibility(View.VISIBLE);

                            }else {
                                mMeasurementApproveRejectLay.setVisibility(View.GONE);

                            }
                            mMeasurementTypeBoolStr = "true";
                        }else if (mResponse.getResult().get(mResponse.getResult().size() -1).getStatus().equalsIgnoreCase("Rejected")){
                            if (AppConstants.APPROVE_REJECT_BTN.equalsIgnoreCase("true")) {
                                mMeasurementApproveRejectLay.setVisibility(View.VISIBLE);

                            }else {
                                mMeasurementApproveRejectLay.setVisibility(View.GONE);

                            }
                            mMeasurementTypeBoolStr = "true";
                        }
                    }
                }

                mAppointmentMeasurementStatusTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getStatus());

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mAppointmentMeasurementTypeHeaderTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getMeasurementInArabic());

                }else {
                    mAppointmentMeasurementTypeHeaderTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getMeasurementInEnglish());

                }

                try {
                    Glide.with(AppointmentDetails.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/Measurement1/"+mResponse.getResult().get(mResponse.getResult().size()-1).getBodyImage())
                            .apply(new RequestOptions().placeholder(R.drawable.empty_img).error(R.drawable.empty_img))
                            .into(mAppointmentMeasurementBodyImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

            }

            if (mOrderTypeViewBool.equalsIgnoreCase("true")){
                mMeasurementParLay.setVisibility(View.GONE);
                mMaterialParLay.setVisibility(View.VISIBLE);
                mEmptyListLay.setVisibility(View.GONE);
            }else if (mOrderTypeViewBool.equalsIgnoreCase("false")&&mMeasurementTypeViewBool.equalsIgnoreCase("true")){
                mMeasurementParLay.setVisibility(View.VISIBLE);
                mMaterialParLay.setVisibility(View.GONE);
                mEmptyListLay.setVisibility(View.GONE);
                mOrderTypeTxtLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                mOrderTypeTxt.setTextColor(getResources().getColor(R.color.white));
                mMeasurementTypeTxtLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                mMeasurementTypeTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            }
            else {
                mMaterialParLay.setVisibility(View.GONE);
                mEmptyListLay.setVisibility(View.VISIBLE);
                mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_for_order));

            }
        }
        if(resObj instanceof InsertAppointmentMaterialResponse){
            InsertAppointmentMaterialResponse mResponse = (InsertAppointmentMaterialResponse)resObj;
//            Toast.makeText(AppointmentDetails.this,mResponse.getResult(),Toast.LENGTH_SHORT).show();
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")){
                mOrderTypeBoolStr = "true";
                mMaterialSaveBtnLay.setVisibility(View.GONE);
                if (!mMeasurementTypeBoolStr.equalsIgnoreCase("true")) {
                    mOrderTypeTxtLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                    mOrderTypeTxt.setTextColor(getResources().getColor(R.color.white));
                    mMeasurementTypeTxtLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                    mMeasurementTypeTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                    if (mMeasurementTypeViewBool.equalsIgnoreCase("true")){
                        mMeasurementParLay.setVisibility(View.VISIBLE);
                        mEmptyListLay.setVisibility(View.GONE);
                    }else {
                        mMeasurementParLay.setVisibility(View.GONE);
                        mEmptyListLay.setVisibility(View.VISIBLE);
                        if (mMeasurementStr.equalsIgnoreCase("2")){
                            mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_update_soon));
                        }else {
                            mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_for_measurement));
                        }

                    }
                    mMaterialParLay.setVisibility(View.GONE);
                }
                if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                    if (mMeasurementTypeBoolStr.equalsIgnoreCase("true")) {
                        DialogManager.getInstance().showAlertPopup(AppointmentDetails.this,getResources().getString(R.string.appointment_created_successsfully), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                if (AppConstants.PAYMENT_STATUS.equalsIgnoreCase("Not Paid")) {
                                    nextScreen(CheckoutScreen.class, true);
                                } else {
                                    onBackPressed();
                                }
                            }
                        });
                    }
                }
                else {
                    if (mMeasurementTypeBoolStr.equalsIgnoreCase("true")) {
                        DialogManager.getInstance().showAlertPopup(AppointmentDetails.this,getResources().getString(R.string.appointment_created_successsfully), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                nextScreen(BottomNavigationScreen.class, true);
                            }
                        });
                    }
                }
            }


        }

        if (resObj instanceof InsertAppointmentMeasurementResponse){
            InsertAppointmentMeasurementResponse mResponse = (InsertAppointmentMeasurementResponse)resObj;
//            Toast.makeText(AppointmentDetails.this,mResponse.getResult(),Toast.LENGTH_SHORT).show();
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")){
                mMeasurementTypeBoolStr = "true";
                mMeasurementSaveBtnLay.setVisibility(View.GONE);
                if(!mOrderTypeBoolStr.equalsIgnoreCase("true")){
                    if (mOrderTypeViewBool.equalsIgnoreCase("true")){
                        mMaterialParLay.setVisibility(View.VISIBLE);
                        mEmptyListLay.setVisibility(View.GONE);
                    }else {
                        mMaterialParLay.setVisibility(View.GONE);
                        mEmptyListLay.setVisibility(View.VISIBLE);
                        if (mOrderStr.equalsIgnoreCase("1")){
                            mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_update_soon));
                        }else {
                            mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_for_order));
                        }
                    }
                    mOrderTypeTxtLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
                    mOrderTypeTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                    mMeasurementTypeTxtLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
                    mMeasurementTypeTxt.setTextColor(getResources().getColor(R.color.white));
                    mMeasurementParLay.setVisibility(View.GONE);
                }
                if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                    if (mOrderTypeBoolStr.equalsIgnoreCase("true")) {
                        DialogManager.getInstance().showAlertPopup(AppointmentDetails.this,getResources().getString(R.string.appointment_created_successsfully), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                if (AppConstants.PAYMENT_STATUS.equalsIgnoreCase("Not Paid")) {
                                    nextScreen(CheckoutScreen.class, true);
                                } else {
                                    onBackPressed();
                                }
                            }
                        });
                    }
                }
                else {
                    if (mOrderTypeBoolStr.equalsIgnoreCase("true")) {
                        DialogManager.getInstance().showAlertPopup(AppointmentDetails.this,getResources().getString(R.string.appointment_created_successsfully), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                nextScreen(BottomNavigationScreen.class, true);
                            }
                        });
                    }
                }
            }
        }
        if (resObj instanceof ApproveMeasurementResponse){
            ApproveMeasurementResponse mResponse = (ApproveMeasurementResponse)resObj;
//            Toast.makeText(AppointmentDetails.this,mResponse.getResult(),Toast.LENGTH_SHORT).show();
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")){
                mMeasurementTypeBoolStr = "true";
                mMeasurementApproveRejectLay.setVisibility(View.GONE);
                if(!mOrderTypeBoolStr.equalsIgnoreCase("true")){

                    if (mOrderTypeViewBool.equalsIgnoreCase("true")){
                        mMaterialParLay.setVisibility(View.VISIBLE);
                        mEmptyListLay.setVisibility(View.GONE);
                    }else {
                        mMaterialParLay.setVisibility(View.GONE);
                        mEmptyListLay.setVisibility(View.VISIBLE);
                        if (mOrderStr.equalsIgnoreCase("1")){
                            mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_update_soon));
                        }else {
                            mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_for_order));
                        }
                    }
                    mOrderTypeTxtLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
                    mOrderTypeTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                    mMeasurementTypeTxtLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
                    mMeasurementTypeTxt.setTextColor(getResources().getColor(R.color.white));
                    mMeasurementParLay.setVisibility(View.GONE);
                }
                if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                    if (mOrderTypeBoolStr.equalsIgnoreCase("true")) {
                        DialogManager.getInstance().showAlertPopup(AppointmentDetails.this,getResources().getString(R.string.appointment_created_successsfully), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                if (AppConstants.PAYMENT_STATUS.equalsIgnoreCase("Not Paid")) {
                                    nextScreen(CheckoutScreen.class, true);
                                } else {
                                    onBackPressed();
                                }
                            }
                        });
                    }
                }
                else {
                    if (mOrderTypeBoolStr.equalsIgnoreCase("true")) {
                        DialogManager.getInstance().showAlertPopup(AppointmentDetails.this,getResources().getString(R.string.appointment_created_successsfully), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                nextScreen(BottomNavigationScreen.class, true);
                            }
                        });
                    }
                }
            }
        }
        if (resObj instanceof ApproveMaterialResponse){
            ApproveMaterialResponse mResponse = (ApproveMaterialResponse)resObj;
//            Toast.makeText(AppointmentDetails.this,mResponse.getResult(),Toast.LENGTH_SHORT).show();
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")){
                mOrderTypeBoolStr = "true";
                mMaterialApproveRejectLay.setVisibility(View.GONE);
                if (!mMeasurementTypeBoolStr.equalsIgnoreCase("true")) {
                    if (mMeasurementTypeViewBool.equalsIgnoreCase("true")){
                        mMeasurementParLay.setVisibility(View.VISIBLE);
                        mEmptyListLay.setVisibility(View.GONE);
                    }else {
                        mMeasurementParLay.setVisibility(View.GONE);
                        mEmptyListLay.setVisibility(View.VISIBLE);
                        if (mMeasurementStr.equalsIgnoreCase("2")){
                            mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_update_soon));
                        }else {
                            mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_for_measurement));
                        }

                    }
                    mOrderTypeTxtLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                    mOrderTypeTxt.setTextColor(getResources().getColor(R.color.white));
                    mMeasurementTypeTxtLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                    mMeasurementTypeTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                    mMaterialParLay.setVisibility(View.GONE);
                }
                if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                    if (mMeasurementTypeBoolStr.equalsIgnoreCase("true")) {
                        DialogManager.getInstance().showAlertPopup(AppointmentDetails.this,getResources().getString(R.string.appointment_created_successsfully), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                if (AppConstants.PAYMENT_STATUS.equalsIgnoreCase("Not Paid")) {
                                    nextScreen(CheckoutScreen.class, true);
                                } else {
                                    onBackPressed();
                                }
                            }
                        });
                    }
                }
                else {
                    if (mMeasurementTypeBoolStr.equalsIgnoreCase("true")) {
                        DialogManager.getInstance().showAlertPopup(AppointmentDetails.this,getResources().getString(R.string.appointment_created_successsfully), new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                nextScreen(BottomNavigationScreen.class, true);
                            }
                        });
                    }
                }
            }
        }
        if (resObj instanceof GetMaterialFromDateResponse){
            GetMaterialFromDateResponse mResponse = (GetMaterialFromDateResponse)resObj;
            if (mResponse.getResult().size() >0 ){
                mOrderTypeFromDateStr = mResponse.getResult().get(mResponse.getResult().size()-1).getFromDt();
                mOrderTypeToDateStr = mResponse.getResult().get(mResponse.getResult().size()-1).getToDt();
                mOrderTimeStr = mResponse.getResult().get(mResponse.getResult().size()-1).getAppointmentTime();
                mMaterialTypeFromDateTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getFromDt());
                AppConstants.DATE_FOR_RESTRICT_MATERIAL = mResponse.getResult().get(mResponse.getResult().size()-1).getFromDt();
                mMaterialTypeTimeSlotTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getAppointmentTime());

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mMaterialRejectedTailorNameTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getNameInArabic());
                }else {
                    mMaterialRejectedTailorNameTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getNameInEnglish());
                }
                mMaterialRejectedDateTxt.setText(getResources().getString(R.string.appointment_time) +" : " + mResponse.getResult().get(mResponse.getResult().size()-1).getFromDt());
                mMaterialRejectedTimeTxt.setText(getResources().getString(R.string.appointment_date) +" : " +mResponse.getResult().get(mResponse.getResult().size()-1).getAppointmentTime());
                mMaterialRejectedOnTxt.setText(getResources().getString(R.string.rejected_on) +" : " +mResponse.getResult().get(mResponse.getResult().size()-1).getRejectDt());
                mMaterialRejectedReasonTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getReason());

                if (mOrderStatusStr.equalsIgnoreCase("Rejected")) {
                    mMaterialRejectedReasonHeadingTxt.setVisibility(View.VISIBLE);
                    mMaterialRejectedReasonLay.setVisibility(View.VISIBLE);

                }else {
                    mMaterialRejectedReasonHeadingTxt.setVisibility(View.GONE);
                    mMaterialRejectedReasonLay.setVisibility(View.GONE);

                }

                if (mOrderStr.equalsIgnoreCase("2")&&mOrderStatusStr.equalsIgnoreCase("Pending")){
                    mMaterialSaveBtnLay.setVisibility(View.GONE);
                    mMaterialEdit = "";
                    mOrderTypeBoolStr = "true";
                }
            }
            else {
                if(mOrderStr.equalsIgnoreCase("1")){
                    mMaterialParLay.setVisibility(View.GONE);
                    mOrderTypeBoolStr = "true";
                    mOrderTypeViewBool = "false";
                    mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_update_soon));
                }
            }

            if (mOrderTypeViewBool.equalsIgnoreCase("true")){
                mMeasurementParLay.setVisibility(View.GONE);
                mMaterialParLay.setVisibility(View.VISIBLE);
                mEmptyListLay.setVisibility(View.GONE);
            }else if (mOrderTypeViewBool.equalsIgnoreCase("false")&&mMeasurementTypeViewBool.equalsIgnoreCase("true")){
                mMeasurementParLay.setVisibility(View.VISIBLE);
                mMaterialParLay.setVisibility(View.GONE);
                mEmptyListLay.setVisibility(View.GONE);
                mOrderTypeTxtLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                mOrderTypeTxt.setTextColor(getResources().getColor(R.color.white));
                mMeasurementTypeTxtLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                mMeasurementTypeTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            }
            else {
                mMaterialParLay.setVisibility(View.GONE);
                mEmptyListLay.setVisibility(View.VISIBLE);
                mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_for_order));

            }
        }
        if (resObj instanceof GetMeasurementFromDateResponse){
            GetMeasurementFromDateResponse mResponse = (GetMeasurementFromDateResponse)resObj;
            if (mResponse.getResult().size() >0 ){
                mMeasurementFromDateStr = mResponse.getResult().get(mResponse.getResult().size()-1).getFromDt();
                mMeasurementToDateStr = mResponse.getResult().get(mResponse.getResult().size()-1).getToDt();
                mMeasurementTimeStr = mResponse.getResult().get(mResponse.getResult().size()-1).getAppointmentTime();
                mMeasurementTypeFromDateTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getFromDt());
                AppConstants.DATE_FOR_RESTRICT_MEASUREMENT = mResponse.getResult().get(mResponse.getResult().size()-1).getFromDt();
                mMeasurementTypeTimeSlotTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getAppointmentTime());

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mMeasurementRejectedTailorNameTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getNameInArabic());
                }else {
                    mMeasurementRejectedTailorNameTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getNameInEnglish());
                }
                mMeasurementRejectedDateTxt.setText(getResources().getString(R.string.appointment_time) +" : " + mResponse.getResult().get(mResponse.getResult().size()-1).getFromDt());
                mMeasurementRejectedTimeTxt.setText(getResources().getString(R.string.appointment_date) +" : " +mResponse.getResult().get(mResponse.getResult().size()-1).getAppointmentTime());
                mMeasurementRejectedOnTxt.setText(getResources().getString(R.string.rejected_on) +" : " +mResponse.getResult().get(mResponse.getResult().size()-1).getRejectDt());
                mMeasurementRejectedReasonTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getReason());

                if (mMeasurementStatusStr.equalsIgnoreCase("Rejected")) {
                    mMeasurementRejectedReasonHeadingTxt.setVisibility(View.VISIBLE);
                    mMeasurementRejectedReasonLay.setVisibility(View.VISIBLE);

                }else {
                    mMeasurementRejectedReasonHeadingTxt.setVisibility(View.GONE);
                    mMeasurementRejectedReasonLay.setVisibility(View.GONE);

                }

                if (mMeasurementStr.equalsIgnoreCase("3")&&mMeasurementStatusStr.equalsIgnoreCase("Pending")){
                    mMeasurementSaveBtnLay.setVisibility(View.GONE);
                    mMeasurementEdit = "";
                    mMeasurementTypeBoolStr = "true";
                }
            }
              else {

                if(mMeasurementStr.equalsIgnoreCase("2")){
                    mMeasurementParLay.setVisibility(View.GONE);
                    mMeasurementTypeBoolStr = "true";
                    mMeasurementTypeViewBool = "false";
                }
            }
            if (mOrderTypeViewBool.equalsIgnoreCase("true")){
                mMeasurementParLay.setVisibility(View.GONE);
                mMaterialParLay.setVisibility(View.VISIBLE);
                mEmptyListLay.setVisibility(View.GONE);
            }else if (mOrderTypeViewBool.equalsIgnoreCase("false")&&mMeasurementTypeViewBool.equalsIgnoreCase("true")){
                mMeasurementParLay.setVisibility(View.VISIBLE);
                mMaterialParLay.setVisibility(View.GONE);
                mEmptyListLay.setVisibility(View.GONE);
                mOrderTypeTxtLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                mOrderTypeTxt.setTextColor(getResources().getColor(R.color.white));
                mMeasurementTypeTxtLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                mMeasurementTypeTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            }
            else {
                mMaterialParLay.setVisibility(View.GONE);
                mEmptyListLay.setVisibility(View.VISIBLE);
                mEmptyListTxt.setText(getResources().getString(R.string.no_appointment_for_order));

            }
        }
    }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.appointment_details_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.appointment_details_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        AppConstants.DATE_FOR_RESTRICT_MATERIAL = "";
        AppConstants.DATE_FOR_RESTRICT_MEASUREMENT = "";

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")||AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            DialogManager.getInstance().showOptionPopup(AppointmentDetails.this, getResources().getString(R.string.sure_want_to_go_back), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                @Override
                public void onNegativeClick() {

                }

                @Override
                public void onPositiveClick() {
                    previousScreen(BottomNavigationScreen.class,true);

                }
            });

        }else {
            super.onBackPressed();

        }

        this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);
    }
}
