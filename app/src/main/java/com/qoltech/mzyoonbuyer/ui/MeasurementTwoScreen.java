package com.qoltech.mzyoonbuyer.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.MeasurementTwoPartsAdapter;
import com.qoltech.mzyoonbuyer.adapter.SliderImageAdapter;
import com.qoltech.mzyoonbuyer.entity.GetMeasurementImageEntity;
import com.qoltech.mzyoonbuyer.entity.GetMeasurementPartEntity;
import com.qoltech.mzyoonbuyer.entity.InsertMeasurmentValueEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetMeasurementTwoResponse;
import com.qoltech.mzyoonbuyer.modal.InsertMeasurementValueModal;
import com.qoltech.mzyoonbuyer.modal.InsertMeasurementValueResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;
import com.zhouyou.view.seekbar.SignSeekBar;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MeasurementTwoScreen extends BaseActivity  {

    @BindView(R.id.measurement_two_par_lay)
    RelativeLayout mMeasurementTwoParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    @BindView(R.id.measurement_two_slider_one)
    RelativeLayout mMeasurementTwoSliderOne;

    @BindView(R.id.measurement_two_slider_two)
    RelativeLayout mMeasurementTwoSliderTwo;

    @BindView(R.id.measurement_two_slider_three)
    RelativeLayout mMeasurementTwoSliderThree;

    @BindView(R.id.measurement_two_slider_four)
    RelativeLayout mMeasurementTwoSliderFour;

    @BindView(R.id.measurement_two_slider_five)
    RelativeLayout mMeasurementTwoSliderFive;

    @BindView(R.id.measurement_two_slider_five_img)
    ImageView mMeasurementTwoSliderfiveImg;

    @BindView(R.id.measurement_two_part_recy_view)
    RecyclerView mMeasurementTwoPartRecyView;

    @BindView(R.id.view_pager_lay)
    LinearLayout mViewPageLay;

    @BindView(R.id.measurement_two_img_lay)
    RelativeLayout mMeasurementTwoImgLay;

    @BindView(R.id.measurement_two_parts_lay)
    RelativeLayout mMeasurementTwoPartsLay;

    @BindView(R.id.measurement_two_img_txt)
    TextView mMeasurementTwoImgTxt;

    @BindView(R.id.measurement_two_part_txt)
    TextView mMeasurementTwoPartTxt;

    @BindView(R.id.measurement_two_cm_lay)
    RelativeLayout mMeasurementTwoCmLay;

//    @BindView(R.id.measurement_two_in_lay)
//    RelativeLayout mMeasurementTwoInLay;

    @BindView(R.id.measurement_two_cm_txt)
    TextView mMeasurementTwoCmTxt;

//    @BindView(R.id.measurement_two_in_txt)
//    TextView mMeasurementTwoInTxt;

    @BindView(R.id.measurement_two_nxt_lay)
    RelativeLayout mMeasurementTwoNxtLay;

    @BindView(R.id.measurement_wizard_lay)
    RelativeLayout mMeasurementWizardLay;

    @BindView(R.id.measurement_add_measurement_wizard_lay)
    RelativeLayout mMeasurementAddMeasurementWizardLay;

    @BindView(R.id.new_flow_measurement_wizard_lay)
    RelativeLayout mNewFlowMeasurementWizLay;

    @BindView(R.id.measurement_two_slider_one_txt)
    TextView mMeasurementTwoSliderOneTxt;

    @BindView(R.id.measurement_two_slider_two_txt)
    TextView mMeasurementTwoSliderTwoTxt;

    @BindView(R.id.measurement_two_slider_three_txt)
    TextView mMeasurementTwoSliderThreeTxt;

    @BindView(R.id.measurement_two_slider_four_txt)
    TextView mMeasurementTwoSliderFourTxt;

    @BindView(R.id.measurement_two_slider_five_txt)
    TextView mMeasurementTwoSliderFiveTxt;

    @BindView(R.id.measurement_two_scroll_view_img)
    ImageView mMeasurementTwoScrollViewImg;

    private MeasurementTwoPartsAdapter mMeasurementPartsAdapter;

    private SliderImageAdapter mSliderAdapter;

    private ArrayList<GetMeasurementPartEntity> mGetMeasurementPartEntity;

    private ArrayList<GetMeasurementImageEntity> mGetMeasurementImgeEntity;

    private UserDetailsEntity mUserDetailsEntityRes;

    HashMap<String,String> parts = new HashMap<>();

    HashMap<String,String> mUpdateParts = new HashMap<>();

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    Dialog mHintDialog,mMeasurementTwoEdtTxtDialog;

    int mCountInt = 1;

    String mBackHandleString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_measurement_two_screen);
        initView();
        AppConstants.MEASUREMENT_SLIDER_POSITION = 0;

    }
    public void initView(){

        ButterKnife.bind(this);

        setupUI(mMeasurementTwoParLay);

        mGetMeasurementPartEntity = new ArrayList<>();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(MeasurementTwoScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        getMeasurementTwoApiCall();

        if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")){
            getHintDialog();

        }

        setHeader();

        if (AppConstants.GENDER_ID.equalsIgnoreCase("2")||AppConstants.GENDER_ID.equalsIgnoreCase("4")){
            mMeasurementTwoSliderFive.setVisibility(View.VISIBLE);
            mMeasurementTwoSliderfiveImg.setVisibility(View.VISIBLE);
        }else {
            mMeasurementTwoSliderFive.setVisibility(View.GONE);
            mMeasurementTwoSliderfiveImg.setVisibility(View.GONE);
        }

    }


    public void getMeasurementTwoApiCall(){
        if (NetworkUtil.isNetworkAvailable(MeasurementTwoScreen.this)){
            APIRequestHandler.getInstance().getMeasurementTwoApiCall(AppConstants.SUB_DRESS_TYPE_ID,MeasurementTwoScreen.this);
        }else{
            DialogManager.getInstance().showNetworkErrorPopup(MeasurementTwoScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getMeasurementTwoApiCall();
                }
            });
        }
    }


    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.measurement));
        mRightSideImg.setVisibility(View.INVISIBLE);
        mEmptyListTxt.setText(getResources().getString(R.string.no_result_found));

        mBackHandleString = "false";

        if (AppConstants.ADD_NEW_MEASUREMENT.equalsIgnoreCase("ADD_NEW_MEASUREMENT")){
            mMeasurementWizardLay.setVisibility(View.GONE);
            mMeasurementAddMeasurementWizardLay.setVisibility(View.VISIBLE);
            mNewFlowMeasurementWizLay.setVisibility(View.GONE);
        }
        else {
            mMeasurementWizardLay.setVisibility(View.VISIBLE);
            mMeasurementAddMeasurementWizardLay.setVisibility(View.GONE);
            mNewFlowMeasurementWizLay.setVisibility(View.GONE);

        }
        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            mMeasurementWizardLay.setVisibility(View.GONE);
            mNewFlowMeasurementWizLay.setVisibility(View.VISIBLE);
            mMeasurementAddMeasurementWizardLay.setVisibility(View.GONE);

        }
    }



    @OnClick({R.id.header_left_side_img,R.id.measurement_two_nxt_lay,R.id.measurement_two_img_lay,R.id.measurement_two_parts_lay,R.id.measurement_two_cm_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                parts = new HashMap<>();

                parts = AppConstants.MEASUREMENT_MAP;

                String MapCounts ="0";

                Set keyss = AppConstants.MEASUREMENT_MAP.keySet();
                Iterator itrs = keyss.iterator();

                String keysss;
                String values;
                while(itrs.hasNext())
                {
                    keysss = (String)itrs.next();
                    values = (String)AppConstants.MEASUREMENT_MAP.get(keysss);
                    if (!values.equalsIgnoreCase("")){
                        MapCounts =String.valueOf(Integer.parseInt(MapCounts) + 1);

//                    MapCounts = String.valueOf(Integer.parseInt(MapCounts) - 1);
                    }

                }

                int count = Integer.parseInt(MapCounts);

                if (count >= 1){
                    DialogManager.getInstance().showOptionPopup(MeasurementTwoScreen.this, getResources().getString(R.string.if_you_go_back_value_will_cleared), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                        @Override
                        public void onNegativeClick() {

                        }

                        @Override
                        public void onPositiveClick() {
                            mBackHandleString = "true";
                            getLanguage();
                            onBackPressed();

                        }
                    });
                }else {
                    mBackHandleString = "true";
                    onBackPressed();
                }

                break;
            case R.id.measurement_two_nxt_lay:
                parts = new HashMap<>();

                parts = AppConstants.MEASUREMENT_MAP;

                String MapCount ="0";

                Set keys = AppConstants.MEASUREMENT_MAP.keySet();
                Iterator itr = keys.iterator();

                String key;
                String value;
                while(itr.hasNext())
                {
                    key = (String)itr.next();
                    value = (String)AppConstants.MEASUREMENT_MAP.get(key);
                    if (value.equalsIgnoreCase("")){
                        MapCount = String.valueOf(Integer.parseInt(MapCount) - 1);
                    }else {
                        MapCount =String.valueOf(Integer.parseInt(MapCount) + 1);
                    }
                }

                if (MapCount.equalsIgnoreCase(String.valueOf(mGetMeasurementPartEntity.size()))){
//                    if (AppConstants.ADD_NEW_MEASUREMENT.equalsIgnoreCase("ADD_NEW_MEASUREMENT")){
                        insertMeasaurementValue();

//                    }else {
//                        nextScreen(AddReferenceScreen.class,true);
//                    }
                }else {
                    mMeasurementTwoNxtLay.clearAnimation();
                    mMeasurementTwoNxtLay.setAnimation(ShakeErrorUtils.shakeError());
                    Toast.makeText(MeasurementTwoScreen.this,getResources().getString(R.string.please_select_all_the_measurement),Toast.LENGTH_LONG).show();
                }


                break;
            case R.id.measurement_two_img_lay:
                mMeasurementTwoImgLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
                mMeasurementTwoImgTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                mMeasurementTwoPartsLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
                mMeasurementTwoPartTxt.setTextColor(getResources().getColor(R.color.white));
                mMeasurementTwoPartRecyView.setVisibility(View.GONE);
                mMeasurementTwoScrollViewImg.setVisibility(View.GONE);

                mEmptyListLay.setVisibility(View.GONE);
                if (mSliderAdapter != null) {
                    mSliderAdapter = new SliderImageAdapter(this,mGetMeasurementImgeEntity,mGetMeasurementPartEntity,1);
                    mViewPager.setAdapter(mSliderAdapter);
                    mViewPager.setCurrentItem(AppConstants.MEASUREMENT_SLIDER_POSITION,true);
                    bottomSliderChanger(AppConstants.MEASUREMENT_SLIDER_POSITION);
                }
                if (mMeasurementPartsAdapter != null){
                    mMeasurementPartsAdapter.notifyDataSetChanged();
                }
                mViewPageLay.setVisibility(View.VISIBLE);
                break;
            case R.id.measurement_two_parts_lay:
                mMeasurementTwoImgLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                mMeasurementTwoImgTxt.setTextColor(getResources().getColor(R.color.white));
                mMeasurementTwoPartsLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                mMeasurementTwoPartTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                mViewPageLay.setVisibility(View.GONE);
                mMeasurementTwoPartRecyView.setVisibility(mGetMeasurementPartEntity.size()>0 ? View.VISIBLE : View.GONE);
                mMeasurementTwoScrollViewImg.setVisibility(mGetMeasurementPartEntity.size()>0 ? View.VISIBLE : View.GONE);


                mEmptyListLay.setVisibility(mGetMeasurementPartEntity.size() > 0 ? View.GONE : View.VISIBLE);

                if (mMeasurementPartsAdapter != null){
                    mMeasurementPartsAdapter.notifyDataSetChanged();
                }
                bottomSliderChanger(AppConstants.MEASUREMENT_SLIDER_POSITION);

                break;
//            case R.id.measurement_two_cm_lay:
//                AppConstants.UNITS = "CM";
//                mMeasurementTwoCmLay.setBackgroundResource(R.drawable.app_clr_gradient_with_corner);
//                mMeasurementTwoInLay.setBackgroundResource(R.drawable.measurement_two_switch_img);
//
//                if (mMeasurementPartsAdapter != null){
//                    mMeasurementPartsAdapter.notifyDataSetChanged();
//                }
//                if (mSliderAdapter != null) {
//                    mSliderAdapter = new SliderImageAdapter(this,mGetMeasurementImgeEntity,mGetMeasurementPartEntity);
//                    mViewPager.setAdapter(mSliderAdapter);
//                    mViewPager.setCurrentItem(AppConstants.MEASUREMENT_SLIDER_POSITION,true);
//                    bottomSliderChanger(AppConstants.MEASUREMENT_SLIDER_POSITION);
//
//                }
//
//                mMeasurementTwoCmTxt.setTextColor(getResources().getColor(R.color.white));
//                mMeasurementTwoInTxt.setTextColor(getResources().getColor(R.color.white));
//                mMeasurementTwoCmTxt.setVisibility(View.VISIBLE);
//                mMeasurementTwoInTxt.setVisibility(View.GONE);
//                break;
//            case R.id.measurement_two_in_lay:
//                AppConstants.UNITS = "IN";
//
//                mMeasurementTwoCmLay.setBackgroundResource(R.drawable.measurement_two_switch_img);
//                mMeasurementTwoInLay.setBackgroundResource(R.drawable.app_clr_gradient_with_corner);
//
//                if (mMeasurementPartsAdapter != null){
//                    mMeasurementPartsAdapter.notifyDataSetChanged();
//                }
//                if (mSliderAdapter != null) {
//                    mSliderAdapter = new SliderImageAdapter(this,mGetMeasurementImgeEntity,mGetMeasurementPartEntity);
//                    mViewPager.setAdapter(mSliderAdapter);
//                    mViewPager.setCurrentItem(AppConstants.MEASUREMENT_SLIDER_POSITION,true);
//                    bottomSliderChanger(AppConstants.MEASUREMENT_SLIDER_POSITION);
//
//                }
//
//                mMeasurementTwoCmTxt.setTextColor(getResources().getColor(R.color.black));
//                mMeasurementTwoInTxt.setTextColor(getResources().getColor(R.color.white));
//                mMeasurementTwoCmTxt.setVisibility(View.GONE);
//                mMeasurementTwoInTxt.setVisibility(View.VISIBLE);
//                break;
        }
    }

    public void insertMeasaurementValue(){
        if (NetworkUtil.isNetworkAvailable(MeasurementTwoScreen.this)){
            AppConstants.USER_ID_STR = mUserDetailsEntityRes.getUSER_ID();

            ArrayList<InsertMeasurmentValueEntity> mResponse = new ArrayList<>();

            Set keys = AppConstants.MEASUREMENT_MAP.keySet();
            Iterator itr = keys.iterator();

            String key;
            String value;
            while(itr.hasNext())
            {
                InsertMeasurmentValueEntity measurmentValueEntity = new InsertMeasurmentValueEntity();

                key = (String)itr.next();
                value = (String)AppConstants.MEASUREMENT_MAP.get(key);
                System.out.println(key + " - "+ value);
                measurmentValueEntity.setMeasurementId(String.valueOf(key));
                measurmentValueEntity.setValue(value);
                mResponse.add(measurmentValueEntity);
            }

            InsertMeasurementValueModal measurementValueModal = new InsertMeasurementValueModal();
            measurementValueModal.setUserId(mUserDetailsEntityRes.getUSER_ID());
            measurementValueModal.setDressTypeId(AppConstants.SUB_DRESS_TYPE_ID);
            measurementValueModal.setMeasurementValue(mResponse);
            measurementValueModal.setUnits(AppConstants.UNITS);

            measurementValueModal.setMeasurementBy(AppConstants.MEASUREMENT_MANUALLY.equalsIgnoreCase("") ? "" : AppConstants.MEASUREMENT_MANUALLY);
            measurementValueModal.setCreatedBy("Customer");
            measurementValueModal.setName(AppConstants.MEASUREMENT_MANUALLY.equalsIgnoreCase("") ? "" : AppConstants.MEASUREMENT_MANUALLY);

            APIRequestHandler.getInstance().insertMeasurementValue(measurementValueModal,MeasurementTwoScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(MeasurementTwoScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    insertMeasaurementValue();
                }
            });
        }
    }


    public void bottomSliderChanger(int position){

        if (position == 0){
            mMeasurementTwoSliderOne.setBackgroundResource(R.drawable.app_clr_gradient_circle);
            mMeasurementTwoSliderTwo.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderThree.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderFour.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderFive.setBackgroundResource(R.drawable.grey_clr_circle);

            mMeasurementTwoSliderOneTxt.setTextColor(getResources().getColor(R.color.white));
            mMeasurementTwoSliderTwoTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderThreeTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderFourTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderFiveTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
        }else if (position == 1){
            mMeasurementTwoSliderOne.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderTwo.setBackgroundResource(R.drawable.app_clr_gradient_circle);
            mMeasurementTwoSliderThree.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderFour.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderFive.setBackgroundResource(R.drawable.grey_clr_circle);

            mMeasurementTwoSliderOneTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderTwoTxt.setTextColor(getResources().getColor(R.color.white));
            mMeasurementTwoSliderThreeTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderFourTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderFiveTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
        }else if (position == 2){
            mMeasurementTwoSliderOne.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderTwo.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderThree.setBackgroundResource(R.drawable.app_clr_gradient_circle);
            mMeasurementTwoSliderFour.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderFive.setBackgroundResource(R.drawable.grey_clr_circle);

            mMeasurementTwoSliderOneTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderTwoTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderThreeTxt.setTextColor(getResources().getColor(R.color.white));
            mMeasurementTwoSliderFourTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderFiveTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
        }else if (position == 3){
            mMeasurementTwoSliderOne.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderTwo.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderThree.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderFour.setBackgroundResource(R.drawable.app_clr_gradient_circle);
            mMeasurementTwoSliderFive.setBackgroundResource(R.drawable.grey_clr_circle);

            mMeasurementTwoSliderOneTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderTwoTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderThreeTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderFourTxt.setTextColor(getResources().getColor(R.color.white));
            mMeasurementTwoSliderFiveTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
        }else if (position == 4){
            mMeasurementTwoSliderOne.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderTwo.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderThree.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderFour.setBackgroundResource(R.drawable.grey_clr_circle);
            mMeasurementTwoSliderFive.setBackgroundResource(R.drawable.app_clr_gradient_circle);

            mMeasurementTwoSliderOneTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderTwoTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderThreeTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderFourTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mMeasurementTwoSliderFiveTxt.setTextColor(getResources().getColor(R.color.white));
        }


    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);

    if (resObj instanceof GetMeasurementTwoResponse){
        GetMeasurementTwoResponse mResponse = (GetMeasurementTwoResponse)resObj;
        mGetMeasurementPartEntity = mResponse.getResult().getMeasurements();
        mGetMeasurementImgeEntity = mResponse.getResult().getImage();

        setPartsAdapter(mGetMeasurementPartEntity);

        viewPagerGet(mResponse.getResult().getImage(),mResponse.getResult().getMeasurements());

        for (int i=0 ; i<mGetMeasurementPartEntity.size(); i++){
            mUpdateParts.put(String.valueOf(mGetMeasurementPartEntity.get(i).getId()),String.valueOf(""));
        }

    }
        if (resObj instanceof InsertMeasurementValueResponse){
            InsertMeasurementValueResponse mResponse = (InsertMeasurementValueResponse)resObj;
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")){

                AppConstants.MEN_1 = "";
                AppConstants.MEN_2 = "";
                AppConstants.MEN_3 = "";
                AppConstants.MEN_4 = "";
                AppConstants.MEN_5 = "";
                AppConstants.MEN_6 = "";
                AppConstants.MEN_7 = "";
                AppConstants.MEN_8 = "";
                AppConstants.MEN_9 = "";
                AppConstants.MEN_10 = "";
                AppConstants.MEN_11 = "";
                AppConstants.MEN_12 = "";
                AppConstants.MEN_13 = "";
                AppConstants.MEN_14 = "";
                AppConstants.MEN_15 = "";
                AppConstants.MEN_16 = "";
                AppConstants.MEN_17 = "";
                AppConstants.MEN_18 = "";
                AppConstants.MEN_19 = "";
                AppConstants.MEN_20 = "";
                AppConstants.MEN_21 = "";
                AppConstants.MEN_22 = "";
                AppConstants.MEN_23 = "";
                AppConstants.MEN_24 = "";
                AppConstants.MEN_25 = "";
                AppConstants.MEN_26 = "";
                AppConstants.MEN_27 = "";
                AppConstants.MEN_28 = "";
                AppConstants.MEN_29 = "";
                AppConstants.MEN_30 = "";
                AppConstants.MEN_31 = "";
                AppConstants.MEN_32 = "";
                AppConstants.MEN_33 = "";
                AppConstants.MEN_34 = "";
                AppConstants.MEN_35 = "";
                AppConstants.MEN_36 = "";
                AppConstants.MEN_37 = "";
                AppConstants.MEN_38 = "";
                AppConstants.MEN_39 = "";
                AppConstants.MEN_40 = "";
                AppConstants.MEN_41 = "";
                AppConstants.MEN_42 = "";
                AppConstants.MEN_43 = "";
                AppConstants.MEN_44 = "";
                AppConstants.MEN_45 = "";
                AppConstants.MEN_46 = "";
                AppConstants.MEN_47 = "";
                AppConstants.MEN_48 = "";
                AppConstants.MEN_49 = "";
                AppConstants.MEN_50 = "";
                AppConstants.MEN_51 = "";
                AppConstants.MEN_52 = "";
                AppConstants.MEN_53 = "";
                AppConstants.MEN_54 = "";
                AppConstants.MEN_55 = "";
                AppConstants.MEN_56 = "";
                AppConstants.MEN_57 = "";
                AppConstants.MEN_58 = "";
                AppConstants.MEN_59 = "";
                AppConstants.MEN_60 = "";
                AppConstants.MEN_61 = "";
                AppConstants.MEN_62 = "";
                AppConstants.MEN_63 = "";
                AppConstants.MEN_64 = "";

                AppConstants.WOMEN_65 = "";
                AppConstants.WOMEN_66 = "";
                AppConstants.WOMEN_67 = "";
                AppConstants.WOMEN_68 = "";
                AppConstants.WOMEN_69 = "";
                AppConstants.WOMEN_70 = "";
                AppConstants.WOMEN_71 = "";
                AppConstants.WOMEN_72 = "";
                AppConstants.WOMEN_73 = "";
                AppConstants.WOMEN_74 = "";
                AppConstants.WOMEN_75 = "";
                AppConstants.WOMEN_76 = "";
                AppConstants.WOMEN_77 = "";
                AppConstants.WOMEN_78 = "";
                AppConstants.WOMEN_79 = "";
                AppConstants.WOMEN_80 = "";
                AppConstants.WOMEN_81 = "";
                AppConstants.WOMEN_82 = "";
                AppConstants.WOMEN_83 = "";
                 AppConstants.WOMEN_84 = "";
                 AppConstants.WOMEN_85 = "";
                 AppConstants.WOMEN_86 = "";
                 AppConstants.WOMEN_87 = "";
                 AppConstants.WOMEN_88 = "";
                 AppConstants.WOMEN_89 = "";
                 AppConstants.WOMEN_90 = "";
                 AppConstants.WOMEN_91 = "";
                 AppConstants.WOMEN_92 = "";
                 AppConstants.WOMEN_93 = "";
                 AppConstants.WOMEN_94 = "";
                 AppConstants.WOMEN_95 = "";
                 AppConstants.WOMEN_96 = "";
                 AppConstants.WOMEN_97 = "";
                 AppConstants.WOMEN_98 = "";
                 AppConstants.WOMEN_99 = "";
                 AppConstants.WOMEN_100 = "";
                 AppConstants.WOMEN_101 = "";
                 AppConstants.WOMEN_102 = "";
                 AppConstants.WOMEN_103 = "";
                 AppConstants.WOMEN_104 = "";
                 AppConstants.WOMEN_105 = "";
                 AppConstants.WOMEN_106 = "";
                 AppConstants.WOMEN_107 = "";
                 AppConstants.WOMEN_108 = "";
                 AppConstants.WOMEN_109 = "";
                 AppConstants.WOMEN_110 = "";
                 AppConstants.WOMEN_111 = "";
                 AppConstants.WOMEN_112 = "";
                 AppConstants.WOMEN_113 = "";
                 AppConstants.WOMEN_114 = "";
                 AppConstants.WOMEN_115 = "";
                 AppConstants.WOMEN_116 = "";
                 AppConstants.WOMEN_117 = "";
                 AppConstants.WOMEN_118 = "";
                 AppConstants.WOMEN_119 = "";
                 AppConstants.WOMEN_120 = "";
                 AppConstants.WOMEN_121 = "";
                 AppConstants.WOMEN_122 = "";
                 AppConstants.WOMEN_123 = "";
                 AppConstants.WOMEN_124 = "";
                 AppConstants.WOMEN_125 = "";
                 AppConstants.WOMEN_126 = "";
                 AppConstants.WOMEN_127 = "";
                 AppConstants.WOMEN_128 = "";
                 AppConstants.WOMEN_129 = "";
                 AppConstants.WOMEN_130 = "";
                 AppConstants.WOMEN_131 = "";
                 AppConstants.WOMEN_132 = "";
                 AppConstants.WOMEN_133 = "";
                 AppConstants.WOMEN_134 = "";
                 AppConstants.WOMEN_135 = "";
                 AppConstants.WOMEN_136 = "";
                 AppConstants.WOMEN_137 = "";
                 AppConstants.WOMEN_138 = "";
                 AppConstants.WOMEN_139 = "";
                 AppConstants.WOMEN_140 = "";
                 AppConstants.WOMEN_141 = "";
                 AppConstants.WOMEN_142 = "";
                 AppConstants.WOMEN_143 = "";
                 AppConstants.WOMEN_144 = "";

                AppConstants.BOY_145 = "";
                AppConstants.BOY_146 = "";
                AppConstants.BOY_147 = "";
                AppConstants.BOY_148 = "";
                AppConstants.BOY_149 = "";
                AppConstants.BOY_150 = "";
                AppConstants.BOY_151 = "";
                AppConstants.BOY_152 = "";
                AppConstants.BOY_153 = "";
                AppConstants.BOY_154 = "";
                AppConstants.BOY_155 = "";
                AppConstants.BOY_156 = "";
                AppConstants.BOY_157 = "";
                AppConstants.BOY_158 = "";
                AppConstants.BOY_159 = "";
                AppConstants.BOY_160 = "";
                AppConstants.BOY_161 = "";
                AppConstants.BOY_162 = "";
                AppConstants.BOY_163 = "";
                AppConstants.BOY_164 = "";
                AppConstants.BOY_165 = "";
                AppConstants.BOY_166 = "";
                AppConstants.BOY_167 = "";
                AppConstants.BOY_168 = "";
                AppConstants.BOY_169 = "";
                AppConstants.BOY_170 = "";
                AppConstants.BOY_171 = "";
                AppConstants.BOY_172 = "";
                AppConstants.BOY_173 = "";
                AppConstants.BOY_174 = "";
                AppConstants.BOY_175 = "";
                AppConstants.BOY_176 = "";
                AppConstants.BOY_177 = "";
                AppConstants.BOY_178 = "";
                AppConstants.BOY_179 = "";
                AppConstants.BOY_180 = "";
                AppConstants.BOY_181 = "";
                AppConstants.BOY_182 = "";
                AppConstants.BOY_183 = "";
                AppConstants.BOY_184 = "";
                AppConstants.BOY_185 = "";
                AppConstants.BOY_186 = "";
                AppConstants.BOY_187 = "";
                AppConstants.BOY_188 = "";
                AppConstants.BOY_189 = "";
                AppConstants.BOY_190 = "";
                AppConstants.BOY_191 = "";
                AppConstants.BOY_192 = "";
                AppConstants.BOY_193 = "";
                AppConstants.BOY_194 = "";
                AppConstants.BOY_195 = "";
                AppConstants.BOY_196 = "";
                AppConstants.BOY_197 = "";
                AppConstants.BOY_198 = "";
                AppConstants.BOY_199 = "";
                AppConstants.BOY_200 = "";
                AppConstants.BOY_201 = "";
                AppConstants.BOY_202 = "";
                AppConstants.BOY_203 = "";
                AppConstants.BOY_204 = "";
                AppConstants.BOY_205 = "";
                AppConstants.BOY_206 = "";
                AppConstants.BOY_207 = "";
                AppConstants.BOY_208 = "";

                AppConstants.GIRL_209 = "";
                AppConstants.GIRL_210 = "";
                AppConstants.GIRL_211 = "";
                AppConstants.GIRL_212 = "";
                AppConstants.GIRL_213 = "";
                AppConstants.GIRL_214 = "";
                AppConstants.GIRL_215 = "";
                AppConstants.GIRL_216 = "";
                AppConstants.GIRL_217 = "";
                AppConstants.GIRL_218 = "";
                AppConstants.GIRL_219 = "";
                AppConstants.GIRL_220 = "";
                AppConstants.GIRL_221 = "";
                AppConstants.GIRL_222 = "";
                AppConstants.GIRL_223 = "";
                AppConstants.GIRL_224 = "";
                AppConstants.GIRL_225 = "";
                AppConstants.GIRL_226 = "";
                AppConstants.GIRL_227 = "";
                AppConstants.GIRL_228 = "";
                AppConstants.GIRL_229 = "";
                AppConstants.GIRL_230 = "";
                AppConstants.GIRL_231 = "";
                AppConstants.GIRL_232 = "";
                AppConstants.GIRL_233 = "";
                AppConstants.GIRL_234 = "";
                AppConstants.GIRL_235 = "";
                AppConstants.GIRL_236 = "";
                AppConstants.GIRL_237 = "";
                AppConstants.GIRL_238 = "";
                AppConstants.GIRL_239 = "";
                AppConstants.GIRL_240 = "";
                AppConstants.GIRL_241 = "";
                AppConstants.GIRL_242 = "";
                AppConstants.GIRL_243 = "";
                AppConstants.GIRL_244 = "";
                AppConstants.GIRL_245 = "";
                AppConstants.GIRL_246 = "";
                AppConstants.GIRL_247 = "";
                AppConstants.GIRL_248 = "";
                AppConstants.GIRL_249 = "";
                AppConstants.GIRL_250 = "";
                AppConstants.GIRL_251 = "";
                AppConstants.GIRL_252 = "";
                AppConstants.GIRL_253 = "";
                AppConstants.GIRL_254 = "";
                AppConstants.GIRL_255 = "";
                AppConstants.GIRL_256 = "";
                AppConstants.GIRL_257 = "";
                AppConstants.GIRL_258 = "";
                AppConstants.GIRL_259 = "";
                AppConstants.GIRL_260 = "";
                AppConstants.GIRL_261 = "";
                AppConstants.GIRL_262 = "";
                AppConstants.GIRL_263 = "";
                AppConstants.GIRL_264 = "";
                AppConstants.GIRL_265 = "";
                AppConstants.GIRL_266 = "";
                AppConstants.GIRL_267 = "";
                AppConstants.GIRL_268 = "";
                AppConstants.GIRL_269 = "";
                AppConstants.GIRL_270 = "";
                AppConstants.GIRL_271 = "";
                AppConstants.GIRL_272 = "";
                AppConstants.GIRL_273 = "";
                AppConstants.GIRL_274 = "";
                AppConstants.GIRL_275 = "";
                AppConstants.GIRL_276 = "";
                AppConstants.GIRL_277 = "";
                AppConstants.GIRL_278 = "";
                AppConstants.GIRL_279 = "";
                AppConstants.GIRL_280 = "";
                AppConstants.GIRL_281 = "";
                AppConstants.GIRL_282 = "";
                AppConstants.GIRL_283 = "";
                AppConstants.GIRL_284 = "";
                AppConstants.GIRL_285 = "";
                AppConstants.GIRL_286 = "";
                AppConstants.GIRL_287 = "";
                AppConstants.GIRL_288 = "";

                AppConstants.MEASUREMENT_MAP = new HashMap<>();

                if (!AppConstants.NEW_ORDER.equalsIgnoreCase("NEW_ORDER")) {
                    AppConstants.SUB_DRESS_TYPE_ID = "";

                }

                previousScreen(AddMeasurementScreen.class,true);
            }
        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    public void viewPagerGet(ArrayList<GetMeasurementImageEntity> image,ArrayList<GetMeasurementPartEntity> parts){
        mSliderAdapter = new SliderImageAdapter(this,image,parts,0);
        mViewPager.setAdapter(mSliderAdapter);

        //page change tracker
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                AppConstants.MEASUREMENT_SLIDER_POSITION = position;
            }

            @Override
            public void onPageSelected(int position) {
                AppConstants.MEASUREMENT_SLIDER_POSITION = position;
                bottomSliderChanger(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        trackScreenName("MeasurementTwo");
    }

    public void getHintDialog(){
        alertDismiss(mHintDialog);
        mHintDialog = getDialog(MeasurementTwoScreen.this, R.layout.pop_up_measurement_two_hint);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mHintDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.measurement_two_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.measurement_two_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        Button mSkipBtn,mBackBtn,mNextBtn;
        RelativeLayout mImageBgLay,mPartsBgLay;
        LinearLayout mArrowBgLay,mCmToInBgLay,mImagePartsBgLay;
        TextView mMeasurementTwoPartImgHintTxt;

        /*Init view*/
        mSkipBtn = mHintDialog.findViewById(R.id.measurement_skip_hint_btn);
        mBackBtn = mHintDialog.findViewById(R.id.measurement_back_hint_btn);
        mNextBtn = mHintDialog.findViewById(R.id.measurement_next_hint_btn);

        mImageBgLay = mHintDialog.findViewById(R.id.measurement_img_par_lay);
        mPartsBgLay = mHintDialog.findViewById(R.id.measurement_part_par_lay);

        mArrowBgLay = mHintDialog.findViewById(R.id.measurement_arrow_par_lay);
        mCmToInBgLay = mHintDialog.findViewById(R.id.measurement_cm_to_in_par_lay);

        mImagePartsBgLay = mHintDialog.findViewById(R.id.measeurement_img_part_par_lay);

        mMeasurementTwoPartImgHintTxt = mHintDialog.findViewById(R.id.measurement_two_part_img_hint);

        if (mCountInt == 1){
            mBackBtn.setVisibility(View.INVISIBLE);
            mPartsBgLay.setVisibility(View.INVISIBLE);
            mArrowBgLay.setVisibility(View.INVISIBLE);
            mCmToInBgLay.setVisibility(View.INVISIBLE);
            mMeasurementTwoPartImgHintTxt.setText(getResources().getString(R.string.measurement_image_tab_hint));
        }

        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountInt = --mCountInt;
                if (mCountInt == 1){
                    mBackBtn.setVisibility(View.INVISIBLE);
                    mImageBgLay.setVisibility(View.VISIBLE);
                    mPartsBgLay.setVisibility(View.INVISIBLE);
                    mArrowBgLay.setVisibility(View.INVISIBLE);
                    mCmToInBgLay.setVisibility(View.INVISIBLE);
                    mImagePartsBgLay.setVisibility(View.VISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                    mMeasurementTwoPartImgHintTxt.setText(getResources().getString(R.string.measurement_image_tab_hint));

                }else if (mCountInt == 2){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mImageBgLay.setVisibility(View.INVISIBLE);
                    mPartsBgLay.setVisibility(View.VISIBLE);
                    mArrowBgLay.setVisibility(View.INVISIBLE);
                    mCmToInBgLay.setVisibility(View.INVISIBLE);
                    mImagePartsBgLay.setVisibility(View.VISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                    mMeasurementTwoPartImgHintTxt.setText(getResources().getString(R.string.measurement_parts_tab_hint));

                }else if (mCountInt == 3){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mImageBgLay.setVisibility(View.INVISIBLE);
                    mPartsBgLay.setVisibility(View.INVISIBLE);
                    mArrowBgLay.setVisibility(View.VISIBLE);
                    mCmToInBgLay.setVisibility(View.INVISIBLE);
                    mImagePartsBgLay.setVisibility(View.INVISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                }else if (mCountInt == 4){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mImageBgLay.setVisibility(View.INVISIBLE);
                    mPartsBgLay.setVisibility(View.INVISIBLE);
                    mArrowBgLay.setVisibility(View.INVISIBLE);
                    mCmToInBgLay.setVisibility(View.VISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.got_it));
                    mImagePartsBgLay.setVisibility(View.INVISIBLE);

                }
            }
        });

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountInt = ++mCountInt;
                if (mCountInt == 1){
                    mBackBtn.setVisibility(View.INVISIBLE);
                    mImageBgLay.setVisibility(View.VISIBLE);
                    mPartsBgLay.setVisibility(View.INVISIBLE);
                    mArrowBgLay.setVisibility(View.INVISIBLE);
                    mCmToInBgLay.setVisibility(View.INVISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                    mImagePartsBgLay.setVisibility(View.VISIBLE);
                    mMeasurementTwoPartImgHintTxt.setText(getResources().getString(R.string.measurement_image_tab_hint));

                }else if (mCountInt == 2){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mImageBgLay.setVisibility(View.INVISIBLE);
                    mPartsBgLay.setVisibility(View.VISIBLE);
                    mArrowBgLay.setVisibility(View.INVISIBLE);
                    mCmToInBgLay.setVisibility(View.INVISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                    mImagePartsBgLay.setVisibility(View.VISIBLE);
                    mMeasurementTwoPartImgHintTxt.setText(getResources().getString(R.string.measurement_parts_tab_hint));

                }else if (mCountInt == 3){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mImageBgLay.setVisibility(View.INVISIBLE);
                    mPartsBgLay.setVisibility(View.INVISIBLE);
                    mArrowBgLay.setVisibility(View.VISIBLE);
                    mCmToInBgLay.setVisibility(View.INVISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                    mImagePartsBgLay.setVisibility(View.INVISIBLE);
                }else if (mCountInt == 4){
                    mBackBtn.setVisibility(View.VISIBLE);
                    mImageBgLay.setVisibility(View.INVISIBLE);
                    mPartsBgLay.setVisibility(View.INVISIBLE);
                    mArrowBgLay.setVisibility(View.INVISIBLE);
                    mCmToInBgLay.setVisibility(View.VISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.got_it));
                    mImagePartsBgLay.setVisibility(View.INVISIBLE);
                }else if (mCountInt >= 5){
                    mHintDialog.dismiss();
                }
            }
        });

        /*Set data*/
        mSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHintDialog.dismiss();
            }
        });

        alertShowing(mHintDialog);
    }

    public void getMeasurmentTwoEdtTxtPopUp(){
        alertDismiss(mMeasurementTwoEdtTxtDialog);
        mMeasurementTwoEdtTxtDialog = getDialog(MeasurementTwoScreen.this, R.layout.pop_up_measurement_parts_edit_txt);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mMeasurementTwoEdtTxtDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
            window.getAttributes().windowAnimations = R.style.PopupBottomAnimation;
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mMeasurementTwoEdtTxtDialog.findViewById(R.id.pop_up_measurement_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(mMeasurementTwoEdtTxtDialog.findViewById(R.id.pop_up_measurement_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        TextView mCancleTxt,mSaveTxt;
        EditText mMeasurementEdtTxt;
        pl.droidsonroids.gif.GifImageView mMeasurementTwoImg;
        com.zhouyou.view.seekbar.SignSeekBar mSeekBar;

        mCancleTxt = mMeasurementTwoEdtTxtDialog.findViewById(R.id.pop_up_measurement_cancel_txt);
        mMeasurementTwoImg = mMeasurementTwoEdtTxtDialog.findViewById(R.id.pop_up_measurement_two_parts_img);
        mSaveTxt = mMeasurementTwoEdtTxtDialog.findViewById(R.id.pop_up_measurement_save_txt);
        mMeasurementEdtTxt = mMeasurementTwoEdtTxtDialog.findViewById(R.id.pop_up_measurement_edt_txt);
        mSeekBar = mMeasurementTwoEdtTxtDialog.findViewById(R.id.measurement_parts_seek_bar);

        mSeekBar.getConfigBuilder().showSign().max(Float.parseFloat(AppConstants.SEEK_MAX)).build();
        mSeekBar.getConfigBuilder().showSign().min(Float.parseFloat(AppConstants.SEEK_MIN)).build();

        AppConstants.SelectedPartsEditValue = AppConstants.SelectedPartsEditValue.equalsIgnoreCase("") ? "0" : AppConstants.SelectedPartsEditValue;

        if(AppConstants.SelectedPartsEditValue.equalsIgnoreCase("0")){
            mSeekBar.setProgress(Float.parseFloat(AppConstants.SEEK_MIN));

        }else {
            mSeekBar.setProgress(Float.parseFloat(AppConstants.SelectedPartsEditValue));

        }
        mSeekBar.setOnProgressChangedListener(new SignSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(SignSeekBar signSeekBar, int progress, float progressFloat, boolean fromUser) {
                mMeasurementEdtTxt.setText(String.valueOf(progressFloat));
            }

            @Override
            public void getProgressOnActionUp(SignSeekBar signSeekBar, int progress, float progressFloat) {

            }

            @Override
            public void getProgressOnFinally(SignSeekBar signSeekBar, int progress, float progressFloat, boolean fromUser) {

            }
        });

        mMeasurementEdtTxt.setText(AppConstants.SelectedPartsEditValue);
        try {
            Glide.with(MeasurementTwoScreen.this)
                    .load(AppConstants.IMAGE_BASE_URL+"images/Measurement2/"+AppConstants.SelectedPartsImage)
                    .apply(new RequestOptions().placeholder(R.drawable.empty_img).error(R.drawable.empty_img))
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(mMeasurementTwoImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        mCancleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.SelectedPartsEditValue = "";
                mMeasurementTwoEdtTxtDialog.dismiss();
            }
        });

        mSaveTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Float.parseFloat(mMeasurementEdtTxt.getText().toString().trim()) == 0){
                    mMeasurementEdtTxt.clearAnimation();
                    mMeasurementEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                }else {
                    if (isCheckDotValue(mMeasurementEdtTxt.getText().toString().trim())) {
                        NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                        DecimalFormat formatter = (DecimalFormat) nf;
                        formatter.applyPattern("######.#");

                        Float values = Float.parseFloat(mMeasurementEdtTxt.getText().toString().trim());
                        Float multiplyValue = Float.parseFloat("2.5");
                        if (AppConstants.SelectedPartsId.equalsIgnoreCase("1") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_1 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_1_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_1 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_1_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("2") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_2 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_2_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_2 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_2_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("3") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_3 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_3_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_3 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_3_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("4") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_4 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_4_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_4 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_4_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("5") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_5 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_5_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_5 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_5_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("7") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_7 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_7_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_7 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_7_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("6") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_6 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_6_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_6 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_6_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("8") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_8 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_8_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_8 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_8_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("9") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_9 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_9_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_9 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_9_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("10") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_10 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_10_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_10 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_10_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("11") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_11 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_11_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_11 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_11_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("12") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_12 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_12_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_12 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_12_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("13") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_13 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_13_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_13 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_13_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("14") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_14 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_14_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_14 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_14_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("15") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_15 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_15_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_15 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_15_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("16") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_16 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_16_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_16 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_16_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("17") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_17 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_17_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_17 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_17_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("18") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_18 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_18_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_18 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_18_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("19") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_19 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_19_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_19 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_19_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("20") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_20 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_20_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_20 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_20_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("21") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_21 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_21_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_21 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_21_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("22") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_22 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_22_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_22 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_22_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("23") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_23 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_23_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_23 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_23_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("24") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_24 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_24_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_24 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_24_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("25") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_25 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_25_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_25 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_25_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("26") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_26 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_26_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_26 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_26_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("27") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_27 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_27_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_27 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_27_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("28") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_28 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_28_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_28 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_28_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("29") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_29 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_29_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_29 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_29_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("30") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_30 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_30_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_30 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_30_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("31") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_31 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_31_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_31 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_31_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("32") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_32 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_32_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_32 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_32_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("33") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_33 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_33_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_33 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_33_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("34") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_34 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_34_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_34 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_34_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("35") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_35 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_35_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_35 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_35_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("36") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_36 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_36_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_36 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_36_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("37") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_37 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_37_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_37 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_37_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("38") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_38 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_38_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_38 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_38_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("39") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_39 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_39_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_39 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_39_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("40") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_40 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_40_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_40 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_40_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("41") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_41 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_41_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_41 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_41_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("42") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_42 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_42_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_42 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_42_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("43") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_43 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_43_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_43 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_43_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("44") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_44 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_44_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_44 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_44_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("45") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_45 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_45_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_45 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_45_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("46") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_46 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_46_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_46 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_46_INCHES = mMeasurementEdtTxt.getText().toString().trim();

                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("47") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_47 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_47_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_47 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_47_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("48") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_48 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_48_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_48 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_48_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("49") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_49 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_49_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_49 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_49_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("50") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_50 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_50_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_50 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_50_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("51") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_51 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_51_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_51 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_51_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("52") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_52 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_52_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_52 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_52_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("53") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_53 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_53_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_53 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_53_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("54") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_54 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_54_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_54 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_54_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("55") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_55 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_55_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_55 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_55_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("56") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_56 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_56_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_56 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_56_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("57") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_57 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_57_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_57 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_57_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("58") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_58 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_58_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_58 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_58_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("59") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_59 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_59_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_59 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_59_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("60") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_60 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_60_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_60 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_60_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("61") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_61 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_61_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_61 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_61_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("62") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_62 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_62_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_62 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_62_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("63") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_63 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_63_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_63 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_63_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("64") && AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.MEN_64 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.MEN_64_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.MEN_64 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.MEN_64_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("65") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_65 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_65_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_65 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_65_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("66") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_66 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_66_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_66 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_66_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("67") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_67 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_67_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_67 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_67_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("68") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_68 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_68_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_68 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_68_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("69") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_69 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_69_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_69 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_69_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("70") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_70 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_70_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_70 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_70_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("71") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_71 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_71_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_71 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_71_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("72") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_72 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_72_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_72 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_72_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("73") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_73 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_73_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_73 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_73_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("74") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_74 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_74_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_74 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_74_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("75") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_75 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_75_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_75 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_75_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("76") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_76 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_76_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_76 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_76_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("77") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_77 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_77_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_77 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_77_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("78") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_78 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_78_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_78 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_78_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("79") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_79 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_79_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_79 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_79_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("80") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_80 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_80_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_80 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_80_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("81") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_81 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_81_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_81 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_81_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("82") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_82 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_82_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_82 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_82_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("83") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_83 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_83_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_83 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_83_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("84") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_84 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_84_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_84 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_84_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("85") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_85 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_85_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_85 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_85_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("86") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_86 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_86_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_86 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_86_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("87") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_87 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_87_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_87 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_87_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("88") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_88 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_88_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_88 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_88_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("89") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_89 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_89_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_89 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_89_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("90") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_90 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_90_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_90 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_90_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("91") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_91 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_91_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_91 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_91_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("92") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_92 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_92_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_92 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_92_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("93") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_93 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_93_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_93 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_93_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("94") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_94 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_94_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_94 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_94_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("95") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_95 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_95_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_95 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_95_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("96") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_96 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_96_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_96 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_96_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("97") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_97 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_97_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_97 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_97_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("98") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_98 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_98_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_98 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_98_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("99") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_99 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_99_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_99 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_99_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("100") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_100 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_100_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_100 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_100_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("101") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_101 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_101_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_101 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_101_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("102") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_102 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_102_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_102 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_102_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("103") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_103 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_103_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_103 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_103_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("104") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_104 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_104_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_104 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_104_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("105") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_105 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_105_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_105 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_105_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("106") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_106 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_106_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_106 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_106_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("107") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_107 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_107_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_107 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_107_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("108") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_108 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_108_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_108 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_108_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("109") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_109 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_109_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_109 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_109_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("110") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_110 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_110_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_110 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_110_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("111") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_111 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_111_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_111 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_111_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("112") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_112 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_112_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_112 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_112_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("113") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_113 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_113_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_113 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_113_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("114") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_114 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_114_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_114 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_114_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("115") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_115 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_115_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_115 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_115_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("116") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_116 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_116_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_116 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_116_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("117") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_117 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_117_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_117 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_117_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("118") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_118 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_118_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_118 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_118_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("119") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_119 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_119_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_119 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_119_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("120") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_120 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_120_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_120 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_120_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("121") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_121 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_121_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_121 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_121_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("122") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_122 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_122_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_122 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_122_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("123") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_123 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_123_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_123 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_123_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("124") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_124 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_124_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_124 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_124_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("125") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_125 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_125_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_125 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_125_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("126") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_126 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_126_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_126 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_126_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("127") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_127 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_127_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_127 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_127_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("128") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_128 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_128_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_128 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_128_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("129") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_129 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_129_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_129 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_129_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("130") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_130 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_130_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_130 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_130_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("131") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_131 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_131_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_131 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_131_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("132") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_132 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_132_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_132 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_132_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("133") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_133 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_133_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_133 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_133_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("134") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_134 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_134_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_134 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_134_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("135") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_135 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_135_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_135 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_135_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("136") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_136 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_136_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_136 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_136_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("137") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_137 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_137_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_137 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_137_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("138") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_138 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_138_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_138 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_138_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("139") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_139 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_139_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_139 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_139_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("140") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_140 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_140_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_140 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_140_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("141") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_141 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_141_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_141 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_141_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("142") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_142 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_142_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_142 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_142_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("143") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_143 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_143_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_143 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_143_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("144") && AppConstants.GENDER_ID.equalsIgnoreCase("2")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.WOMEN_144 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.WOMEN_144_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.WOMEN_144 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.WOMEN_144_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }
                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("145") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_145 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_145_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_145 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_145_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("146") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_146 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_146_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_146 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_146_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("147") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_147 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_147_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_147 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_147_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("148") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_148 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_148_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_148 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_148_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("149") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_149 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_149_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_149 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_149_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("150") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_150 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_150_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_150 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_150_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("151") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_151 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_151_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_151 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_151_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("152") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_152 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_152_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_152 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_152_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("153") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_153 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_153_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_153 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_153_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("154") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_154 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_154_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_154 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_154_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("155") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_155 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_155_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_155 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_155_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("156") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_156 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_156_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_156 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_156_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("157") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_157 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_157_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_157 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_157_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("158") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_158 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_158_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_158 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_158_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("159") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_159 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_159_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_159 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_159_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("160") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_160 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_160_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_160 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_160_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("161") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_161 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_161_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_161 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_161_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("162") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_162 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_162_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_162 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_162_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("163") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_163 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_163_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_163 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_163_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("164") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_164 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_164_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_164 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_164_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("165") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_165 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_165_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_165 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_165_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("166") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_166 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_166_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_166 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_166_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("167") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_167 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_167_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_167 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_167_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("168") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_168 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_168_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_168 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_168_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("169") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_169 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_169_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_169 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_169_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("170") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_170 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_170_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_170 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_170_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("171") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_171 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_171_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_171 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_171_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("172") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_172 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_172_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_172 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_172_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("173") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_173 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_173_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_173 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_173_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("174") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_174 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_174_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_174 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_174_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("175") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_175 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_175_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_175 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_175_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("176") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_176 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_176_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_176 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_176_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("177") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_177 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_177_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_177 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_177_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("178") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_178 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_178_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_178 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_178_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("179") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_179 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_179_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_179 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_179_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("180") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_180 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_180_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_180 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_180_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("181") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_181 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_181_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_181 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_181_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("182") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_182 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_182_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_182 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_182_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("183") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_183 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_183_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_183 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_183_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("184") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_184 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_184_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_184 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_184_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("185") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_185 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_185_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_185 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_185_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("186") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_186 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_186_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_186 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_186_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("187") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_187 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_187_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_187 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_187_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("188") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_188 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_188_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_188 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_188_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("189") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_189 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_189_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_189 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_189_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("190") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_190 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_190_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_190 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_190_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("191") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_191 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_191_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_191 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_191_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("192") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_192 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_192_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_192 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_192_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("193") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_193 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_193_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_193 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_193_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("194") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_194 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_194_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_194 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_194_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("195") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_195 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_195_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_195 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_195_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("196") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_196 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_196_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_196 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_196_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("197") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_197 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_197_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_197 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_197_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("198") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_198 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_198_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_198 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_198_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("199") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_199 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_199_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_199 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_199_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("200") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_200 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_200_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_200 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_200_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("201") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_201 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_201_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_201 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_201_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("202") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_202 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_202_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_202 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_202_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("203") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_203 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_203_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_203 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_203_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("204") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_204 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_204_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_204 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_204_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("205") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_205 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_205_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_205 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_205_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("206") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_206 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_206_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_206 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_206_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("207") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_207 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_207_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_207 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_207_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("208") && AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.BOY_208 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.BOY_208_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.BOY_208 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.BOY_208_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("209") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_209 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_209_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_209 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_209_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("210") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_210 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_210_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_210 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_210_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("211") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_211 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_211_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_211 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_211_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("212") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_212 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_212_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_212 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_212_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("213") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_213 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_213_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_213 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_213_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("214") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_214 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_214_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_214 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_214_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("215") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_215 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_215_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_215 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_215_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("216") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_216 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_216_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_216 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_216_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("217") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_217 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_217_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_217 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_217_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("218") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_218 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_218_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_218 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_218_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("219") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_219 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_219_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_219 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_219_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("220") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_220 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_220_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_220 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_220_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("221") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_221 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_221_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_221 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_221_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("222") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_222 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_222_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_222 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_222_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("223") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_223 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_223_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_223 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_223_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("224") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_224 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_224_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_224 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_224_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("225") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_225 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_225_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_225 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_225_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("226") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_226 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_226_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_226 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_226_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("227") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_227 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_227_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_227 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_227_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("228") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_228 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_228_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_228 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_228_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("229") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_229 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_229_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_229 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_229_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("230") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_230 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_230_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_230 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_230_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("231") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_231 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_231_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_231 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_231_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("232") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_232 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_232_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_232 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_232_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("233") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_233 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_233_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_233 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_233_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("234") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_234 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_234_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_234 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_234_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("235") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_235 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_235_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_235 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_235_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("236") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_236 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_236_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_236 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_236_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("237") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_237 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_237_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_237 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_237_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("238") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_238 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_238_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_238 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_238_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("239") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_239 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_239_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_239 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_239_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("240") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_240 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_240_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_240 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_240_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("241") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_241 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_241_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_241 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_241_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("242") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_242 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_242_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_242 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_242_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("243") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_243 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_243_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_243 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_243_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("244") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_244 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_244_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_244 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_244_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("245") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_245 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_245_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_245 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_245_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("246") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_246 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_246_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_246 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_246_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("247") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_247 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_247_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_247 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_247_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("248") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_248 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_248_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_248 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_248_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("249") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_249 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_249_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_249 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_249_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("250") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_250 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_250_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_250 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_250_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("251") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_251 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_251_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_251 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_251_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("252") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_252 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_252_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_252 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_252_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("253") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_253 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_253_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_253 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_253_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("254") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_254 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_254_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_254 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_254_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("255") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_255 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_255_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_255 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_255_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("256") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_256 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_256_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_256 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_256_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("257") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_257 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_257_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_257 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_257_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("258") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_258 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_258_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_258 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_258_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("259") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_259 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_259_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_259 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_259_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("260") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_260 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_260_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_260 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_260_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("261") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_261 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_261_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_261 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_261_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("262") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_262 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_262_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_262 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_262_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("263") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_263 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_263_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_263 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_263_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("264") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_264 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_264_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_264 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_264_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("265") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_265 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_265_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_265 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_265_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("266") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_266 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_266_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_266 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_266_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("267") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_267 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_267_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_267 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_267_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("268") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_268 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_268_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_268 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_268_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("269") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_269 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_269_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_269 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_269_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("270") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_270 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_270_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_270 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_270_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("271") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_271 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_271_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_271 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_271_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("272") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_272 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_272_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_272 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_272_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("273") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_273 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_273_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_273 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_273_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("274") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_274 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_274_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_274 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_274_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("275") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_275 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_275_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_275 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_275_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("276") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_276 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_276_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_276 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_276_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("277") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_277 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_277_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_277 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_277_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("278") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_278 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_278_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_278 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_278_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("279") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_279 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_279_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_279 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_279_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("280") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_280 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_280_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_280 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_280_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("281") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_281 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_281_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_281 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_281_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("282") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_282 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_282_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_282 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_282_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("283") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_283 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_283_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_283 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_283_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("284") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_284 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_284_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_284 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_284_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("285") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_285 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_285_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_285 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_285_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("286") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_286 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_286_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_286 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_286_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("287") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_287 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_287_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_287 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_287_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        } else if (AppConstants.SelectedPartsId.equalsIgnoreCase("288") && AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                            if (AppConstants.UNITS.equalsIgnoreCase("CM")) {
                                AppConstants.GIRL_288 = mMeasurementEdtTxt.getText().toString().trim();
                                AppConstants.GIRL_288_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                            } else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                                AppConstants.GIRL_288 = String.valueOf(formatter.format(values * multiplyValue));
                                AppConstants.GIRL_288_INCHES = mMeasurementEdtTxt.getText().toString().trim();
                            }

                        }
                        mUpdateParts.put(AppConstants.SelectedPartsId,mMeasurementEdtTxt.getText().toString().trim());
                        AppConstants.MEASUREMENT_MAP = mUpdateParts;

                        AppConstants.SelectedPartsEditValue = "";
                        mMeasurementTwoEdtTxtDialog.dismiss();
                        onResume();

                    } else {
                        mMeasurementEdtTxt.clearAnimation();
                        mMeasurementEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
//                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
//                        mMeasurementEdtTxt.setError("يرجى إعطاء سم مناسب أو IN (EX: 00.0)");
//                    }else {
                        mMeasurementEdtTxt.setError(getResources().getString(R.string.please_give_proper_cm_or_in));
//                    }
                    }
                }
            }
        });

        alertShowing(mMeasurementTwoEdtTxtDialog);
    }

    public static boolean isCheckDotValue(String measurement) {
        String expression = "([0-9]{1,3}+\\.)+[0-9]{1}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(measurement);
        return matcher.matches();
    }

    @Override
    public void onBackPressed() {

        if (mBackHandleString.equalsIgnoreCase("true")){
            super.onBackPressed();

            AppConstants.MEN_1 = "";
            AppConstants.MEN_2 = "";
            AppConstants.MEN_3 = "";
            AppConstants.MEN_4 = "";
            AppConstants.MEN_5 = "";
            AppConstants.MEN_6 = "";
            AppConstants.MEN_7 = "";
            AppConstants.MEN_8 = "";
            AppConstants.MEN_9 = "";
            AppConstants.MEN_10 = "";
            AppConstants.MEN_11 = "";
            AppConstants.MEN_12 = "";
            AppConstants.MEN_13 = "";
            AppConstants.MEN_14 = "";
            AppConstants.MEN_15 = "";
            AppConstants.MEN_16 = "";
            AppConstants.MEN_17 = "";
            AppConstants.MEN_18 = "";
            AppConstants.MEN_19 = "";
            AppConstants.MEN_20 = "";
            AppConstants.MEN_21 = "";
            AppConstants.MEN_22 = "";
            AppConstants.MEN_23 = "";
            AppConstants.MEN_24 = "";
            AppConstants.MEN_25 = "";
            AppConstants.MEN_26 = "";
            AppConstants.MEN_27 = "";
            AppConstants.MEN_28 = "";
            AppConstants.MEN_29 = "";
            AppConstants.MEN_30 = "";
            AppConstants.MEN_31 = "";
            AppConstants.MEN_32 = "";
            AppConstants.MEN_33 = "";
            AppConstants.MEN_34 = "";
            AppConstants.MEN_35 = "";
            AppConstants.MEN_36 = "";
            AppConstants.MEN_37 = "";
            AppConstants.MEN_38 = "";
            AppConstants.MEN_39 = "";
            AppConstants.MEN_40 = "";
            AppConstants.MEN_41 = "";
            AppConstants.MEN_42 = "";
            AppConstants.MEN_43 = "";
            AppConstants.MEN_44 = "";
            AppConstants.MEN_45 = "";
            AppConstants.MEN_46 = "";
            AppConstants.MEN_47 = "";
            AppConstants.MEN_48 = "";
            AppConstants.MEN_49 = "";
            AppConstants.MEN_50 = "";
            AppConstants.MEN_51 = "";
            AppConstants.MEN_52 = "";
            AppConstants.MEN_53 = "";
            AppConstants.MEN_54 = "";
            AppConstants.MEN_55 = "";
            AppConstants.MEN_56 = "";
            AppConstants.MEN_57 = "";
            AppConstants.MEN_58 = "";
            AppConstants.MEN_59 = "";
            AppConstants.MEN_60 = "";
            AppConstants.MEN_61 = "";
            AppConstants.MEN_62 = "";
            AppConstants.MEN_63 = "";
            AppConstants.MEN_64 = "";

            AppConstants.MEN_1_INCHES = "";
            AppConstants.MEN_2_INCHES = "";
            AppConstants.MEN_3_INCHES = "";
            AppConstants.MEN_4_INCHES = "";
            AppConstants.MEN_5_INCHES = "";
            AppConstants.MEN_6_INCHES = "";
            AppConstants.MEN_7_INCHES = "";
            AppConstants.MEN_8_INCHES = "";
            AppConstants.MEN_9_INCHES = "";
            AppConstants.MEN_10_INCHES = "";
            AppConstants.MEN_11_INCHES = "";
            AppConstants.MEN_12_INCHES = "";
            AppConstants.MEN_13_INCHES = "";
            AppConstants.MEN_14_INCHES = "";
            AppConstants.MEN_15_INCHES = "";
            AppConstants.MEN_16_INCHES = "";
            AppConstants.MEN_17_INCHES = "";
            AppConstants.MEN_18_INCHES = "";
            AppConstants.MEN_19_INCHES = "";
            AppConstants.MEN_20_INCHES = "";
            AppConstants.MEN_21_INCHES = "";
            AppConstants.MEN_22_INCHES = "";
            AppConstants.MEN_23_INCHES = "";
            AppConstants.MEN_24_INCHES = "";
            AppConstants.MEN_25_INCHES = "";
            AppConstants.MEN_26_INCHES = "";
            AppConstants.MEN_27_INCHES = "";
            AppConstants.MEN_28_INCHES = "";
            AppConstants.MEN_29_INCHES = "";
            AppConstants.MEN_30_INCHES = "";
            AppConstants.MEN_31_INCHES = "";
            AppConstants.MEN_32_INCHES = "";
            AppConstants.MEN_33_INCHES = "";
            AppConstants.MEN_34_INCHES = "";
            AppConstants.MEN_35_INCHES = "";
            AppConstants.MEN_36_INCHES = "";
            AppConstants.MEN_37_INCHES = "";
            AppConstants.MEN_38_INCHES = "";
            AppConstants.MEN_39_INCHES = "";
            AppConstants.MEN_40_INCHES = "";
            AppConstants.MEN_41_INCHES = "";
            AppConstants.MEN_42_INCHES = "";
            AppConstants.MEN_43_INCHES = "";
            AppConstants.MEN_44_INCHES = "";
            AppConstants.MEN_45_INCHES = "";
            AppConstants.MEN_46_INCHES = "";
            AppConstants.MEN_47_INCHES = "";
            AppConstants.MEN_48_INCHES = "";
            AppConstants.MEN_49_INCHES = "";
            AppConstants.MEN_50_INCHES = "";
            AppConstants.MEN_51_INCHES = "";
            AppConstants.MEN_52_INCHES = "";
            AppConstants.MEN_53_INCHES = "";
            AppConstants.MEN_54_INCHES = "";
            AppConstants.MEN_55_INCHES = "";
            AppConstants.MEN_56_INCHES = "";
            AppConstants.MEN_57_INCHES = "";
            AppConstants.MEN_58_INCHES = "";
            AppConstants.MEN_59_INCHES = "";
            AppConstants.MEN_60_INCHES = "";
            AppConstants.MEN_61_INCHES = "";
            AppConstants.MEN_62_INCHES = "";
            AppConstants.MEN_63_INCHES = "";
            AppConstants.MEN_64_INCHES = "";

            AppConstants.WOMEN_65 = "";
            AppConstants.WOMEN_66 = "";
            AppConstants.WOMEN_67 = "";
            AppConstants.WOMEN_68 = "";
            AppConstants.WOMEN_69 = "";
            AppConstants.WOMEN_70 = "";
            AppConstants.WOMEN_71 = "";
            AppConstants.WOMEN_72 = "";
            AppConstants.WOMEN_73 = "";
            AppConstants.WOMEN_74 = "";
            AppConstants.WOMEN_75 = "";
            AppConstants.WOMEN_76 = "";
            AppConstants.WOMEN_77 = "";
            AppConstants.WOMEN_78 = "";
            AppConstants.WOMEN_79 = "";
            AppConstants.WOMEN_80 = "";
            AppConstants.WOMEN_81 = "";
            AppConstants.WOMEN_82 = "";
            AppConstants.WOMEN_83 = "";
            AppConstants.WOMEN_84 = "";
            AppConstants.WOMEN_85 = "";
            AppConstants.WOMEN_86 = "";
            AppConstants.WOMEN_87 = "";
            AppConstants.WOMEN_88 = "";
            AppConstants.WOMEN_89 = "";
            AppConstants.WOMEN_90 = "";
            AppConstants.WOMEN_91 = "";
            AppConstants.WOMEN_92 = "";
            AppConstants.WOMEN_93 = "";
            AppConstants.WOMEN_94 = "";
            AppConstants.WOMEN_95 = "";
            AppConstants.WOMEN_96 = "";
            AppConstants.WOMEN_97 = "";
            AppConstants.WOMEN_98 = "";
            AppConstants.WOMEN_99 = "";
            AppConstants.WOMEN_100 = "";
            AppConstants.WOMEN_101 = "";
            AppConstants.WOMEN_102 = "";
            AppConstants.WOMEN_103 = "";
            AppConstants.WOMEN_104 = "";
            AppConstants.WOMEN_105 = "";
            AppConstants.WOMEN_106 = "";
            AppConstants.WOMEN_107 = "";
            AppConstants.WOMEN_108 = "";
            AppConstants.WOMEN_109 = "";
            AppConstants.WOMEN_110 = "";
            AppConstants.WOMEN_111 = "";
            AppConstants.WOMEN_112 = "";
            AppConstants.WOMEN_113 = "";
            AppConstants.WOMEN_114 = "";
            AppConstants.WOMEN_115 = "";
            AppConstants.WOMEN_116 = "";
            AppConstants.WOMEN_117 = "";
            AppConstants.WOMEN_118 = "";
            AppConstants.WOMEN_119 = "";
            AppConstants.WOMEN_120 = "";
            AppConstants.WOMEN_121 = "";
            AppConstants.WOMEN_122 = "";
            AppConstants.WOMEN_123 = "";
            AppConstants.WOMEN_124 = "";
            AppConstants.WOMEN_125 = "";
            AppConstants.WOMEN_126 = "";
            AppConstants.WOMEN_127 = "";
            AppConstants.WOMEN_128 = "";
            AppConstants.WOMEN_129 = "";
            AppConstants.WOMEN_130 = "";
            AppConstants.WOMEN_131 = "";
            AppConstants.WOMEN_132 = "";
            AppConstants.WOMEN_133 = "";
            AppConstants.WOMEN_134 = "";
            AppConstants.WOMEN_135 = "";
            AppConstants.WOMEN_136 = "";
            AppConstants.WOMEN_137 = "";
            AppConstants.WOMEN_138 = "";
            AppConstants.WOMEN_139 = "";
            AppConstants.WOMEN_140 = "";
            AppConstants.WOMEN_141 = "";
            AppConstants.WOMEN_142 = "";
            AppConstants.WOMEN_143 = "";
            AppConstants.WOMEN_144 = "";

            AppConstants.BOY_145 = "";
            AppConstants.BOY_146 = "";
            AppConstants.BOY_147 = "";
            AppConstants.BOY_148 = "";
            AppConstants.BOY_149 = "";
            AppConstants.BOY_150 = "";
            AppConstants.BOY_151 = "";
            AppConstants.BOY_152 = "";
            AppConstants.BOY_153 = "";
            AppConstants.BOY_154 = "";
            AppConstants.BOY_155 = "";
            AppConstants.BOY_156 = "";
            AppConstants.BOY_157 = "";
            AppConstants.BOY_158 = "";
            AppConstants.BOY_159 = "";
            AppConstants.BOY_160 = "";
            AppConstants.BOY_161 = "";
            AppConstants.BOY_162 = "";
            AppConstants.BOY_163 = "";
            AppConstants.BOY_164 = "";
            AppConstants.BOY_165 = "";
            AppConstants.BOY_166 = "";
            AppConstants.BOY_167 = "";
            AppConstants.BOY_168 = "";
            AppConstants.BOY_169 = "";
            AppConstants.BOY_170 = "";
            AppConstants.BOY_171 = "";
            AppConstants.BOY_172 = "";
            AppConstants.BOY_173 = "";
            AppConstants.BOY_174 = "";
            AppConstants.BOY_175 = "";
            AppConstants.BOY_176 = "";
            AppConstants.BOY_177 = "";
            AppConstants.BOY_178 = "";
            AppConstants.BOY_179 = "";
            AppConstants.BOY_180 = "";
            AppConstants.BOY_181 = "";
            AppConstants.BOY_182 = "";
            AppConstants.BOY_183 = "";
            AppConstants.BOY_184 = "";
            AppConstants.BOY_185 = "";
            AppConstants.BOY_186 = "";
            AppConstants.BOY_187 = "";
            AppConstants.BOY_188 = "";
            AppConstants.BOY_189 = "";
            AppConstants.BOY_190 = "";
            AppConstants.BOY_191 = "";
            AppConstants.BOY_192 = "";
            AppConstants.BOY_193 = "";
            AppConstants.BOY_194 = "";
            AppConstants.BOY_195 = "";
            AppConstants.BOY_196 = "";
            AppConstants.BOY_197 = "";
            AppConstants.BOY_198 = "";
            AppConstants.BOY_199 = "";
            AppConstants.BOY_200 = "";
            AppConstants.BOY_201 = "";
            AppConstants.BOY_202 = "";
            AppConstants.BOY_203 = "";
            AppConstants.BOY_204 = "";
            AppConstants.BOY_205 = "";
            AppConstants.BOY_206 = "";
            AppConstants.BOY_207 = "";
            AppConstants.BOY_208 = "";

            AppConstants.GIRL_209 = "";
            AppConstants.GIRL_210 = "";
            AppConstants.GIRL_211 = "";
            AppConstants.GIRL_212 = "";
            AppConstants.GIRL_213 = "";
            AppConstants.GIRL_214 = "";
            AppConstants.GIRL_215 = "";
            AppConstants.GIRL_216 = "";
            AppConstants.GIRL_217 = "";
            AppConstants.GIRL_218 = "";
            AppConstants.GIRL_219 = "";
            AppConstants.GIRL_220 = "";
            AppConstants.GIRL_221 = "";
            AppConstants.GIRL_222 = "";
            AppConstants.GIRL_223 = "";
            AppConstants.GIRL_224 = "";
            AppConstants.GIRL_225 = "";
            AppConstants.GIRL_226 = "";
            AppConstants.GIRL_227 = "";
            AppConstants.GIRL_228 = "";
            AppConstants.GIRL_229 = "";
            AppConstants.GIRL_230 = "";
            AppConstants.GIRL_231 = "";
            AppConstants.GIRL_232 = "";
            AppConstants.GIRL_233 = "";
            AppConstants.GIRL_234 = "";
            AppConstants.GIRL_235 = "";
            AppConstants.GIRL_236 = "";
            AppConstants.GIRL_237 = "";
            AppConstants.GIRL_238 = "";
            AppConstants.GIRL_239 = "";
            AppConstants.GIRL_240 = "";
            AppConstants.GIRL_241 = "";
            AppConstants.GIRL_242 = "";
            AppConstants.GIRL_243 = "";
            AppConstants.GIRL_244 = "";
            AppConstants.GIRL_245 = "";
            AppConstants.GIRL_246 = "";
            AppConstants.GIRL_247 = "";
            AppConstants.GIRL_248 = "";
            AppConstants.GIRL_249 = "";
            AppConstants.GIRL_250 = "";
            AppConstants.GIRL_251 = "";
            AppConstants.GIRL_252 = "";
            AppConstants.GIRL_253 = "";
            AppConstants.GIRL_254 = "";
            AppConstants.GIRL_255 = "";
            AppConstants.GIRL_256 = "";
            AppConstants.GIRL_257 = "";
            AppConstants.GIRL_258 = "";
            AppConstants.GIRL_259 = "";
            AppConstants.GIRL_260 = "";
            AppConstants.GIRL_261 = "";
            AppConstants.GIRL_262 = "";
            AppConstants.GIRL_263 = "";
            AppConstants.GIRL_264 = "";
            AppConstants.GIRL_265 = "";
            AppConstants.GIRL_266 = "";
            AppConstants.GIRL_267 = "";
            AppConstants.GIRL_268 = "";
            AppConstants.GIRL_269 = "";
            AppConstants.GIRL_270 = "";
            AppConstants.GIRL_271 = "";
            AppConstants.GIRL_272 = "";
            AppConstants.GIRL_273 = "";
            AppConstants.GIRL_274 = "";
            AppConstants.GIRL_275 = "";
            AppConstants.GIRL_276 = "";
            AppConstants.GIRL_277 = "";
            AppConstants.GIRL_278 = "";
            AppConstants.GIRL_279 = "";
            AppConstants.GIRL_280 = "";
            AppConstants.GIRL_281 = "";
            AppConstants.GIRL_282 = "";
            AppConstants.GIRL_283 = "";
            AppConstants.GIRL_284 = "";
            AppConstants.GIRL_285 = "";
            AppConstants.GIRL_286 = "";
            AppConstants.GIRL_287 = "";
            AppConstants.GIRL_288 = "";

            AppConstants.WOMEN_65_INCHES = "";
            AppConstants.WOMEN_66_INCHES = "";
            AppConstants.WOMEN_67_INCHES = "";
            AppConstants.WOMEN_68_INCHES = "";
            AppConstants.WOMEN_69_INCHES = "";
            AppConstants.WOMEN_70_INCHES = "";
            AppConstants.WOMEN_71_INCHES = "";
            AppConstants.WOMEN_72_INCHES = "";
            AppConstants.WOMEN_73_INCHES = "";
            AppConstants.WOMEN_74_INCHES = "";
            AppConstants.WOMEN_75_INCHES = "";
            AppConstants.WOMEN_76_INCHES = "";
            AppConstants.WOMEN_77_INCHES = "";
            AppConstants.WOMEN_78_INCHES = "";
            AppConstants.WOMEN_79_INCHES = "";
            AppConstants.WOMEN_80_INCHES = "";
            AppConstants.WOMEN_81_INCHES = "";
            AppConstants.WOMEN_82_INCHES = "";
            AppConstants.WOMEN_83_INCHES = "";
            AppConstants.WOMEN_84_INCHES = "";
            AppConstants.WOMEN_85_INCHES = "";
            AppConstants.WOMEN_86_INCHES = "";
            AppConstants.WOMEN_87_INCHES = "";
            AppConstants.WOMEN_88_INCHES = "";
            AppConstants.WOMEN_89_INCHES = "";
            AppConstants.WOMEN_90_INCHES = "";
            AppConstants.WOMEN_91_INCHES = "";
            AppConstants.WOMEN_92_INCHES = "";
            AppConstants.WOMEN_93_INCHES = "";
            AppConstants.WOMEN_94_INCHES = "";
            AppConstants.WOMEN_95_INCHES = "";
            AppConstants.WOMEN_96_INCHES = "";
            AppConstants.WOMEN_97_INCHES = "";
            AppConstants.WOMEN_98_INCHES = "";
            AppConstants.WOMEN_99_INCHES = "";
            AppConstants.WOMEN_100_INCHES = "";
            AppConstants.WOMEN_101_INCHES = "";
            AppConstants.WOMEN_102_INCHES = "";
            AppConstants.WOMEN_103_INCHES = "";
            AppConstants.WOMEN_104_INCHES = "";
            AppConstants.WOMEN_105_INCHES = "";
            AppConstants.WOMEN_106_INCHES= "";
            AppConstants.WOMEN_107_INCHES = "";
            AppConstants.WOMEN_108_INCHES = "";
            AppConstants.WOMEN_109_INCHES = "";
            AppConstants.WOMEN_110_INCHES = "";
            AppConstants.WOMEN_111_INCHES = "";
            AppConstants.WOMEN_112_INCHES = "";
            AppConstants.WOMEN_113_INCHES = "";
            AppConstants.WOMEN_114_INCHES = "";
            AppConstants.WOMEN_115_INCHES = "";
            AppConstants.WOMEN_116_INCHES = "";
            AppConstants.WOMEN_117_INCHES = "";
            AppConstants.WOMEN_118_INCHES = "";
            AppConstants.WOMEN_119_INCHES = "";
            AppConstants.WOMEN_120_INCHES = "";
            AppConstants.WOMEN_121_INCHES = "";
            AppConstants.WOMEN_122_INCHES = "";
            AppConstants.WOMEN_123_INCHES = "";
            AppConstants.WOMEN_124_INCHES = "";
            AppConstants.WOMEN_125_INCHES = "";
            AppConstants.WOMEN_126_INCHES = "";
            AppConstants.WOMEN_127_INCHES = "";
            AppConstants.WOMEN_128_INCHES = "";
            AppConstants.WOMEN_129_INCHES = "";
            AppConstants.WOMEN_130_INCHES = "";
            AppConstants.WOMEN_131_INCHES = "";
            AppConstants.WOMEN_132_INCHES = "";
            AppConstants.WOMEN_133_INCHES = "";
            AppConstants.WOMEN_134_INCHES = "";
            AppConstants.WOMEN_135_INCHES = "";
            AppConstants.WOMEN_136_INCHES = "";
            AppConstants.WOMEN_137_INCHES = "";
            AppConstants.WOMEN_138_INCHES = "";
            AppConstants.WOMEN_139_INCHES = "";
            AppConstants.WOMEN_140_INCHES = "";
            AppConstants.WOMEN_141_INCHES = "";
            AppConstants.WOMEN_142_INCHES = "";
            AppConstants.WOMEN_143_INCHES = "";
            AppConstants.WOMEN_144_INCHES = "";

            AppConstants.BOY_145_INCHES = "";
            AppConstants.BOY_146_INCHES="";
            AppConstants.BOY_147_INCHES="";
            AppConstants.BOY_148_INCHES="";
            AppConstants.BOY_149_INCHES="";
            AppConstants.BOY_150_INCHES="";
            AppConstants.BOY_151_INCHES="";
            AppConstants.BOY_152_INCHES="";
            AppConstants.BOY_153_INCHES="";
            AppConstants.BOY_154_INCHES="";
            AppConstants.BOY_155_INCHES="";
            AppConstants.BOY_156_INCHES="";
            AppConstants.BOY_157_INCHES="";
            AppConstants.BOY_158_INCHES="";
            AppConstants.BOY_159_INCHES="";
            AppConstants.BOY_160_INCHES="";
            AppConstants.BOY_161_INCHES="";
            AppConstants.BOY_162_INCHES="";
            AppConstants.BOY_163_INCHES="";
            AppConstants.BOY_164_INCHES="";
            AppConstants.BOY_165_INCHES="";
            AppConstants.BOY_166_INCHES="";
            AppConstants.BOY_167_INCHES="";
            AppConstants.BOY_168_INCHES="";
            AppConstants.BOY_169_INCHES="";
            AppConstants.BOY_170_INCHES="";
            AppConstants.BOY_171_INCHES="";
            AppConstants.BOY_172_INCHES="";
            AppConstants.BOY_173_INCHES="";
            AppConstants.BOY_174_INCHES="";
            AppConstants.BOY_175_INCHES="";
            AppConstants.BOY_176_INCHES="";
            AppConstants.BOY_177_INCHES="";
            AppConstants.BOY_178_INCHES="";
            AppConstants.BOY_179_INCHES="";
            AppConstants.BOY_180_INCHES="";
            AppConstants.BOY_181_INCHES="";
            AppConstants.BOY_182_INCHES="";
            AppConstants.BOY_183_INCHES="";
            AppConstants.BOY_184_INCHES="";
            AppConstants.BOY_185_INCHES="";
            AppConstants.BOY_186_INCHES="";
            AppConstants.BOY_187_INCHES="";
            AppConstants.BOY_188_INCHES="";
            AppConstants.BOY_189_INCHES="";
            AppConstants.BOY_190_INCHES="";
            AppConstants.BOY_191_INCHES="";
            AppConstants.BOY_192_INCHES="";
            AppConstants.BOY_193_INCHES="";
            AppConstants.BOY_194_INCHES="";
            AppConstants.BOY_195_INCHES="";
            AppConstants.BOY_196_INCHES="";
            AppConstants.BOY_197_INCHES="";
            AppConstants.BOY_198_INCHES="";
            AppConstants.BOY_199_INCHES="";
            AppConstants.BOY_200_INCHES="";
            AppConstants.BOY_201_INCHES="";
            AppConstants.BOY_202_INCHES="";
            AppConstants.BOY_203_INCHES="";
            AppConstants.BOY_204_INCHES="";
            AppConstants.BOY_205_INCHES="";
            AppConstants.BOY_206_INCHES="";
            AppConstants.BOY_207_INCHES="";
            AppConstants.BOY_208_INCHES="";

            AppConstants.GIRL_209_INCHES="";
            AppConstants.GIRL_210_INCHES="";
            AppConstants.GIRL_211_INCHES="";
            AppConstants.GIRL_212_INCHES="";
            AppConstants.GIRL_213_INCHES="";
            AppConstants.GIRL_214_INCHES="";
            AppConstants.GIRL_215_INCHES="";
            AppConstants.GIRL_216_INCHES="";
            AppConstants.GIRL_217_INCHES="";
            AppConstants.GIRL_218_INCHES="";
            AppConstants.GIRL_219_INCHES="";
            AppConstants.GIRL_220_INCHES="";
            AppConstants.GIRL_221_INCHES="";
            AppConstants.GIRL_222_INCHES="";
            AppConstants.GIRL_223_INCHES="";
            AppConstants.GIRL_224_INCHES="";
            AppConstants.GIRL_225_INCHES="";
            AppConstants.GIRL_226_INCHES="";
            AppConstants.GIRL_227_INCHES="";
            AppConstants.GIRL_228_INCHES="";
            AppConstants.GIRL_229_INCHES="";
            AppConstants.GIRL_230_INCHES="";
            AppConstants.GIRL_231_INCHES="";
            AppConstants.GIRL_232_INCHES="";
            AppConstants.GIRL_233_INCHES="";
            AppConstants.GIRL_234_INCHES="";
            AppConstants.GIRL_235_INCHES="";
            AppConstants.GIRL_236_INCHES="";
            AppConstants.GIRL_237_INCHES="";
            AppConstants.GIRL_238_INCHES="";
            AppConstants.GIRL_239_INCHES="";
            AppConstants.GIRL_240_INCHES="";
            AppConstants.GIRL_241_INCHES="";
            AppConstants.GIRL_242_INCHES="";
            AppConstants.GIRL_243_INCHES="";
            AppConstants.GIRL_244_INCHES="";
            AppConstants.GIRL_245_INCHES="";
            AppConstants.GIRL_246_INCHES="";
            AppConstants.GIRL_247_INCHES="";
            AppConstants.GIRL_248_INCHES="";
            AppConstants.GIRL_249_INCHES="";
            AppConstants.GIRL_250_INCHES="";
            AppConstants.GIRL_251_INCHES="";
            AppConstants.GIRL_252_INCHES="";
            AppConstants.GIRL_253_INCHES="";
            AppConstants.GIRL_254_INCHES="";
            AppConstants.GIRL_255_INCHES="";
            AppConstants.GIRL_256_INCHES="";
            AppConstants.GIRL_257_INCHES="";
            AppConstants.GIRL_258_INCHES="";
            AppConstants.GIRL_259_INCHES="";
            AppConstants.GIRL_260_INCHES="";
            AppConstants.GIRL_261_INCHES="";
            AppConstants.GIRL_262_INCHES="";
            AppConstants.GIRL_263_INCHES="";
            AppConstants.GIRL_264_INCHES="";
            AppConstants.GIRL_265_INCHES="";
            AppConstants.GIRL_266_INCHES="";
            AppConstants.GIRL_267_INCHES="";
            AppConstants.GIRL_268_INCHES="";
            AppConstants.GIRL_269_INCHES="";
            AppConstants.GIRL_270_INCHES="";
            AppConstants.GIRL_271_INCHES="";
            AppConstants.GIRL_272_INCHES="";
            AppConstants.GIRL_273_INCHES="";
            AppConstants.GIRL_274_INCHES="";
            AppConstants.GIRL_275_INCHES="";
            AppConstants.GIRL_276_INCHES="";
            AppConstants.GIRL_277_INCHES="";
            AppConstants.GIRL_278_INCHES="";
            AppConstants.GIRL_279_INCHES="";
            AppConstants.GIRL_280_INCHES="";
            AppConstants.GIRL_281_INCHES="";
            AppConstants.GIRL_282_INCHES="";
            AppConstants.GIRL_283_INCHES="";
            AppConstants.GIRL_284_INCHES="";
            AppConstants.GIRL_285_INCHES="";
            AppConstants.GIRL_286_INCHES="";
            AppConstants.GIRL_287_INCHES="";
            AppConstants.GIRL_288_INCHES="";

            AppConstants.MEASUREMENT_MAP = new HashMap<>();

            this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);
        }
        else {
            parts = new HashMap<>();

            parts = AppConstants.MEASUREMENT_MAP;

            String MapCounts ="0";

            Set keyss = AppConstants.MEASUREMENT_MAP.keySet();
            Iterator itrs = keyss.iterator();

            String keysss;
            String values;
            while(itrs.hasNext())
            {
                keysss = (String)itrs.next();
                values = (String)AppConstants.MEASUREMENT_MAP.get(keysss);
                if (!values.equalsIgnoreCase("")){
                    MapCounts =String.valueOf(Integer.parseInt(MapCounts) + 1);

//                    MapCounts = String.valueOf(Integer.parseInt(MapCounts) - 1);
                }
//                else {
//                }
            }

            int count = Integer.parseInt(MapCounts);

            if (count >=1){
                DialogManager.getInstance().showOptionPopup(MeasurementTwoScreen.this, getResources().getString(R.string.if_you_go_back_value_will_cleared), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                    @Override
                    public void onNegativeClick() {

                    }
                    @Override
                    public void onPositiveClick() {
                        mBackHandleString = "true";

                        getLanguage();
                        onBackPressed();
                    }
                });
            }else {
                mBackHandleString = "true";
                onBackPressed();
            }
        }
    }

    /*Set Adapter for the Recycler view*/
    public void setPartsAdapter(ArrayList<GetMeasurementPartEntity> getMeasurementPartsList) {

        mMeasurementPartsAdapter = new MeasurementTwoPartsAdapter(this,getMeasurementPartsList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mMeasurementTwoPartRecyView.setLayoutManager(layoutManager);
            mMeasurementTwoPartRecyView.setAdapter(mMeasurementPartsAdapter);

            mMeasurementTwoPartRecyView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPosition = layoutManager.findLastVisibleItemPosition();

                    if (lastPosition == getMeasurementPartsList.size()-1){
                        mMeasurementTwoScrollViewImg.setVisibility(View.GONE);
                    }else {
                        mMeasurementTwoScrollViewImg.setVisibility(View.VISIBLE);
                    }
                }
            });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mMeasurementPartsAdapter != null){
            mMeasurementPartsAdapter.notifyDataSetChanged();
        }
        if (mSliderAdapter != null) {
            mSliderAdapter = new SliderImageAdapter(this,mGetMeasurementImgeEntity,mGetMeasurementPartEntity,1);
            mViewPager.setAdapter(mSliderAdapter);
            mViewPager.setCurrentItem(AppConstants.MEASUREMENT_SLIDER_POSITION,true);
//            mMeasurementTwoSliderOne.setBackgroundResource(R.drawable.app_clr_gradient_circle);
//            mMeasurementTwoSliderTwo.setBackgroundResource(R.drawable.grey_clr_circle);
//            mMeasurementTwoSliderThree.setBackgroundResource(R.drawable.grey_clr_circle);
//            mMeasurementTwoSliderFour.setBackgroundResource(R.drawable.grey_clr_circle);
            bottomSliderChanger(AppConstants.MEASUREMENT_SLIDER_POSITION);
        }
        getLanguage();
        }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_two_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            updateResources(MeasurementTwoScreen.this,"ar");

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_two_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            updateResources(MeasurementTwoScreen.this,"en");

        }
    }
    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }
}

