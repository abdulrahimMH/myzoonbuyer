package com.qoltech.mzyoonbuyer.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.GetAppointmentTimeSlotAdapter;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.GetAppointmentDateForScheduleResponse;
import com.qoltech.mzyoonbuyer.modal.GetAppointmentForScheduleTypeResponse;
import com.qoltech.mzyoonbuyer.modal.GetNoOfAppointmentDateForScheduleResponse;
import com.qoltech.mzyoonbuyer.modal.InsertAppointmentForScheduleTypeResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.SelectDateFragment;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class ScheduletDetailScreen extends BaseActivity {


    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.schedule_par_lay)
    LinearLayout mScheduleParLay;

    @BindView(R.id.schedule_inner_par_lay)
    LinearLayout mScheduleInnerParLay;

    @BindView(R.id.schedule_status_txt)
    TextView mScheduleStatusTxt;

    @BindView(R.id.appointment_schedule_type_header_txt)
    TextView mAppointmentScheduleTypeHeaderTxt;

    @BindView(R.id.appointment_schedule_type_body_img)
    ImageView mAppointmentScheduleBodyImg;

    @BindView(R.id.schedule_save_btn_lay)
    RelativeLayout mScheduleSaveBtnLay;

    @BindView(R.id.schedule_continue_btn_lay)
    RelativeLayout mScehduleContinueBtnLay;

    @BindView(R.id.schedule_type_time_slot_txt)
    public TextView mScheduleTypeTimeSlotTxt;

    @BindView(R.id.schedule_type_fromt_date_txt)
    public TextView mScheduleTypeFromDateTxt;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    private UserDetailsEntity mUserDetailsEntityRes;

    Dialog mAppointmentReschedule,mAppointmentTimeDialog;

    private GetAppointmentTimeSlotAdapter mGetAppointmentTimeSlotAdater;

    String mScheduleStatusStr = "",mScheduleTypeFromDateStr = "",mScheduleTimeStr = "",mBackHandleBool = "true";

    private ArrayList<String> mAppointmentTimeSlotList;

    private boolean mScheduleBool = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_schedule_detail_screen);
                initView();

    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mScheduleParLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(ScheduletDetailScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        addTimeSlotList();

        getAppointmentForScheduleTypeApiCall();
        getAppointmentDateForScheduleApiCall();
        getNoOfAppointmentDateForScheduleApiCall();

        getLanguage();

        AppConstants.DATE_FOR_RESTRICT_SCHEDULE = "";

    }

    public void addTimeSlotList(){
        mAppointmentTimeSlotList = new ArrayList<>();
        mAppointmentTimeSlotList.add("08:00 AM TO 02:00 PM");
        mAppointmentTimeSlotList.add("02:00 PM TO 08:00 AM");

    }

    @OnClick({R.id.header_left_side_img,R.id.appointment_schedule_save_btn,R.id.schedule_type_time_slot_par_lay,R.id.schedule_type_calender_from_par_lay,R.id.schedule_status_txt,R.id.appointment_schedule_continue_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                    onBackPressed();
                break;
            case R.id.appointment_schedule_save_btn:
                if (mScheduleTypeFromDateTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.appointment_date_hint))){
                    mScheduleTypeFromDateTxt.clearAnimation();
                    mScheduleTypeFromDateTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mScheduleTypeFromDateTxt.setError(getResources().getString(R.string.please_give_date));
                }else if (mScheduleTypeTimeSlotTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.appointment_time_hint))){
                    mScheduleTypeTimeSlotTxt.clearAnimation();
                    mScheduleTypeTimeSlotTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mScheduleTypeTimeSlotTxt.setError(getResources().getString(R.string.please_give_time));
                }
                else {
                    insertAppointmentForScheduleTypeApiCall();
                }
                break;
            case R.id.schedule_type_time_slot_par_lay:
                    if (mAppointmentTimeSlotList.size() > 0){
                        alertDismiss(mAppointmentTimeDialog);
                        mAppointmentTimeDialog = getDialog(ScheduletDetailScreen.this, R.layout.pop_up_country_alert);

                        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                        Window window = mAppointmentTimeDialog.getWindow();

                        if (window != null) {
                            LayoutParams.copyFrom(window.getAttributes());
                            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(LayoutParams);
                            window.setGravity(Gravity.CENTER);
                        }

                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            ViewCompat.setLayoutDirection(mAppointmentTimeDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                        }else {
                            ViewCompat.setLayoutDirection(mAppointmentTimeDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                        }
                        TextView cancelTxt , headerTxt;

                        RecyclerView countryRecyclerView;

                        /*Init view*/
                        cancelTxt = mAppointmentTimeDialog.findViewById(R.id.country_text_cancel);
                        headerTxt = mAppointmentTimeDialog.findViewById(R.id.country_header_text_cancel);

                        countryRecyclerView = mAppointmentTimeDialog.findViewById(R.id.country_popup_recycler_view);
                        mGetAppointmentTimeSlotAdater = new GetAppointmentTimeSlotAdapter(ScheduletDetailScreen.this, mAppointmentTimeSlotList,mAppointmentTimeDialog,"SCHEDULE_TYPE");

                        countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        countryRecyclerView.setAdapter(mGetAppointmentTimeSlotAdater);

                        /*Set data*/
                        headerTxt.setText(getResources().getString(R.string.choose_your_time_slot));
                        cancelTxt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mAppointmentTimeDialog.dismiss();
                            }
                        });

                        alertShowing(mAppointmentTimeDialog);
                    }else {
                        Toast.makeText(ScheduletDetailScreen.this,getResources().getString(R.string.sorry_no_result_found) , Toast.LENGTH_SHORT).show();
                    }

                break;
            case R.id.schedule_type_calender_from_par_lay:
                    AppConstants.DATE_PICKER_COND = "APPOINTMENT_SCHEDULE_FROM";
                    DialogFragment newFragment = new SelectDateFragment();
                    newFragment.show(getSupportFragmentManager(), "DatePicker");
                break;
            case R.id.schedule_status_txt:
                if (mScheduleStatusStr.equalsIgnoreCase("Rejected")){
                    alertDismiss(mAppointmentReschedule);
                    mAppointmentReschedule = getDialog(ScheduletDetailScreen.this, R.layout.pop_up_appointment_reschedule);

                    WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                    Window window = mAppointmentReschedule.getWindow();

                    if (window != null) {
                        LayoutParams.copyFrom(window.getAttributes());
                        LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(LayoutParams);
                        window.setGravity(Gravity.CENTER);
                    }

                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mAppointmentReschedule.findViewById(R.id.pop_up_appointment_reschedule_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                    } else {
                        ViewCompat.setLayoutDirection(mAppointmentReschedule.findViewById(R.id.pop_up_appointment_reschedule_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }

                    RelativeLayout mOkRal,mResRal;

                    /*Init view*/
                    mOkRal = mAppointmentReschedule.findViewById(R.id.appointment_reschedule_ok_lay);
                    mResRal = mAppointmentReschedule.findViewById(R.id.pop_up_appointment_reschedule_par_lay);

                    mOkRal.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAppointmentReschedule.dismiss();
                        }
                    });
                    mResRal.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAppointmentReschedule.dismiss();

                        }
                    });

                    alertShowing(mAppointmentReschedule);
                }
                break;
            case R.id.appointment_schedule_continue_btn:
                if (mScheduleStatusStr.equalsIgnoreCase("Approved")){
                    if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")){
                        nextScreen(BottomNavigationScreen.class,true);

                    }
                    if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                        if (AppConstants.PAYMENT_STATUS.equalsIgnoreCase("Not Paid")){
                            nextScreen(CheckoutScreen.class,true);

                        }else {
                            onBackPressed();
                        }
                    }
                }else {
                    Toast.makeText(ScheduletDetailScreen.this,getResources().getString(R.string.schedule_continue_btn_txt),Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.appointment));
        mRightSideImg.setVisibility(View.INVISIBLE);

        mScheduleInnerParLay.setVisibility(View.VISIBLE);

        mEmptyListTxt.setText(getResources().getString(R.string.no_result_found));
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);

    }

    public void getAppointmentForScheduleTypeApiCall(){
        if (NetworkUtil.isNetworkAvailable(ScheduletDetailScreen.this)){

            if ( AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")){
                APIRequestHandler.getInstance().getAppointmentForScheduleTypeApiCall(AppConstants.APPOINTMENT_LIST_ID,ScheduletDetailScreen.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().getAppointmentForScheduleTypeApiCall(AppConstants.REQUEST_LIST_ID,ScheduletDetailScreen.this);

            }
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ScheduletDetailScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentForScheduleTypeApiCall();
                }
            });
        }
    }

    public void insertAppointmentForScheduleTypeApiCall(){
        if (NetworkUtil.isNetworkAvailable(ScheduletDetailScreen.this)){

            if ( AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")){
                APIRequestHandler.getInstance().insertAppointmentForScheduleTypeApiCall(AppConstants.APPOINTMENT_LIST_ID,"3",mScheduleTypeTimeSlotTxt.getText().toString(),mScheduleTypeFromDateTxt.getText().toString(),mScheduleTypeFromDateTxt.getText().toString(),"Buyer",mUserDetailsEntityRes.getUSER_ID(),mUserDetailsEntityRes.getUSER_NAME(),ScheduletDetailScreen.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().insertAppointmentForScheduleTypeApiCall(AppConstants.REQUEST_LIST_ID,"3",mScheduleTypeTimeSlotTxt.getText().toString(),mScheduleTypeFromDateTxt.getText().toString(),mScheduleTypeFromDateTxt.getText().toString(),"Buyer",mUserDetailsEntityRes.getUSER_ID(),mUserDetailsEntityRes.getUSER_NAME(),ScheduletDetailScreen.this);

            }

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ScheduletDetailScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    insertAppointmentForScheduleTypeApiCall();
                }
            });
        }
    }
    public void getAppointmentDateForScheduleApiCall(){
        if (NetworkUtil.isNetworkAvailable(ScheduletDetailScreen.this)){

            if ( AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")){
                APIRequestHandler.getInstance().getAppointmentDateForScheduleTypeApiCall(AppConstants.APPOINTMENT_LIST_ID,mUserDetailsEntityRes.getUSER_ID(),"Buyer",ScheduletDetailScreen.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().getAppointmentDateForScheduleTypeApiCall(AppConstants.REQUEST_LIST_ID,mUserDetailsEntityRes.getUSER_ID(),"Buyer",ScheduletDetailScreen.this);

            }
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ScheduletDetailScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentDateForScheduleApiCall();
                }
            });
        }
    }
    public void getNoOfAppointmentDateForScheduleApiCall(){
        if(NetworkUtil.isNetworkAvailable(ScheduletDetailScreen.this)){
            if ( AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")){
                APIRequestHandler.getInstance().getNoOfAppointmentDateForScheduleTypeApiCall(AppConstants.APPOINTMENT_LIST_ID,AppConstants.APPROVED_TAILOR_ID,AppConstants.SUB_DRESS_TYPE_ID,ScheduletDetailScreen.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().getNoOfAppointmentDateForScheduleTypeApiCall(AppConstants.REQUEST_LIST_ID,AppConstants.APPROVED_TAILOR_ID,AppConstants.SUB_DRESS_TYPE_ID,ScheduletDetailScreen.this);

            }

        }else{
            DialogManager.getInstance().showNetworkErrorPopup(ScheduletDetailScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getNoOfAppointmentDateForScheduleApiCall();
                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetAppointmentForScheduleTypeResponse){
            GetAppointmentForScheduleTypeResponse mResponse = (GetAppointmentForScheduleTypeResponse)resObj;
            if (mResponse.getResult().size() > 0){
                try {
                    Glide.with(ScheduletDetailScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/ServiceType/"+mResponse.getResult().get(0).getBodyImage())
                            .apply(new RequestOptions().placeholder(R.drawable.empty_img).error(R.drawable.empty_img))
                            .into(mAppointmentScheduleBodyImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

                mScheduleStatusStr = mResponse.getResult().get(0).getStatus();
                mScheduleStatusTxt.setText(mResponse.getResult().get(0).getStatus());

                if (mResponse.getResult().get(0).getStatus().equalsIgnoreCase("Approved")){
                    mScheduleSaveBtnLay.setVisibility(View.GONE);
                }

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mAppointmentScheduleTypeHeaderTxt.setText(mResponse.getResult().get(0).getHeaderInArabic());

                }else {
                    mAppointmentScheduleTypeHeaderTxt.setText(mResponse.getResult().get(0).getHeaderInEnglish());

                }
            }
        }
        if (resObj instanceof GetAppointmentDateForScheduleResponse){
            GetAppointmentDateForScheduleResponse mResponse = (GetAppointmentDateForScheduleResponse)resObj;
            if (mResponse.getResult().size() > 0){
                AppConstants.DATE_FOR_RESTRICT_SCHEDULE = mResponse.getResult().get(mResponse.getResult().size()-1).getFromDt();
                mScheduleTypeFromDateTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getFromDt());
                mScheduleTypeTimeSlotTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getAppointmentTime());

                mScheduleTypeFromDateStr = mResponse.getResult().get(mResponse.getResult().size()-1).getFromDt();
                mScheduleTimeStr = mResponse.getResult().get(mResponse.getResult().size()-1).getAppointmentTime();
            }
        }
        if (resObj instanceof InsertAppointmentForScheduleTypeResponse){
            InsertAppointmentForScheduleTypeResponse mResponse = (InsertAppointmentForScheduleTypeResponse)resObj;
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")){
                mScheduleSaveBtnLay.setVisibility(View.GONE);
                DialogManager.getInstance().showAlertPopup(ScheduletDetailScreen.this,getResources().getString(R.string.appointment_created_successsfully), new InterfaceBtnCallBack() {
                    @Override
                    public void onPositiveClick() {
                        if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")){
                            nextScreen(BottomNavigationScreen.class,true);

                        }
                        if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                            if (AppConstants.PAYMENT_STATUS.equalsIgnoreCase("Not Paid")){
                                nextScreen(CheckoutScreen.class,true);

                            }else {
                                onBackPressed();
                            }
                        }
                    }
                });
            }
        }
        if (resObj instanceof GetNoOfAppointmentDateForScheduleResponse){
            GetNoOfAppointmentDateForScheduleResponse mResponse = (GetNoOfAppointmentDateForScheduleResponse)resObj;
            if (mResponse.getResult().size() > 0){

                mScheduleBool = mResponse.getResult().get(0).IsSchedule;
//                if (mResponse.getResult().get(0).getNoOfDays()!=0){
                    AppConstants.NO_OF_DAYS_FOR_SCHEDULE = String.valueOf(mResponse.getResult().get(0).getNoOfDays()+1);
//                }else {
//                    AppConstants.NO_OF_DAYS_FOR_SCHEDULE = String.valueOf(mResponse.getResult().get(0).getNoOfDays()+1);
//                }

                if (mResponse.getResult().get(0).isSchedule()){
                    DialogManager.getInstance().showAlertPopup(ScheduletDetailScreen.this, getResources().getString(R.string.schedule_experience), new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {
                            mScheduleTypeTimeSlotTxt.setText("");
                            mScheduleTimeStr = "";
                        }
                    });
                }
                Date currentDate = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(currentDate);
                c.add(Calendar.DATE, Integer.parseInt(AppConstants.NO_OF_DAYS_FOR_SCHEDULE));

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
                Date resultdate = new Date(c.getTimeInMillis());

                mScheduleTypeFromDateTxt.setText(String.valueOf(sdf.format(resultdate)));
            }
        }
    }

    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }

    }
    /*Default dialog init method*/
    public Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }

    public void getLanguage(){
        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.schedule_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.schedule_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
        }
    }

    @Override
    protected void onResume() {
        initView();
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        AppConstants.DATE_FOR_RESTRICT_SCHEDULE = "";

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")||AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            DialogManager.getInstance().showOptionPopup(ScheduletDetailScreen.this, getResources().getString(R.string.sure_want_to_go_back), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                @Override
                public void onNegativeClick() {

                }

                @Override
                public void onPositiveClick() {
                    previousScreen(BottomNavigationScreen.class,true);

                }
            });

        }else {
            super.onBackPressed();

        }
            this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);
    }
}
