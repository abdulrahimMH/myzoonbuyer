package com.qoltech.mzyoonbuyer.ui;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.ProfileGenderAdapter;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.FileUploadResponse;
import com.qoltech.mzyoonbuyer.modal.GetProfileDetailsResponse;
import com.qoltech.mzyoonbuyer.modal.GetReferalCodeResponse;
import com.qoltech.mzyoonbuyer.modal.ProfileIntroResponse;
import com.qoltech.mzyoonbuyer.modal.UpdateProfileResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.SelectDateFragment;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class ProfileScreen extends BaseActivity {

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id. profile_image)
    de.hdodenhof.circleimageview.CircleImageView mProfileImg;

    @BindView(R.id.profile_dob_edt_txt)
    public EditText mProfileDobEdtTxt;

    @BindView(R.id.profile_email_edt_txt)
    EditText mProfileEmailEdtTxt;

    @BindView(R.id.profile_mobile_edt_txt)
    EditText mProfileMobileEdtTxt;

    @BindView(R.id.profile_name_edt_txt)
    EditText mProfileNameEdtTxt;

    @BindView(R.id.profile_par_lay)
    LinearLayout mProfileParLay;

    boolean editableBool = false;
    String IMAGE_PATH = "";
    private static final String IMAGE_DIRECTORY = "/MZYOON";

    public final int REQUEST_CAMERA = 999;
    public final int REQUEST_GALLERY = 888;

    private File mUserImageFile = null;

    @BindView(R.id.profile_update_profile_btn)
    Button mProfileUpdateProfileBtn;

    @BindView(R.id.profile_cancel_save_lay)
    RelativeLayout mProfileCancelSaveLay;

    @BindView(R.id.profile_gender_edt_txt)
    public EditText mProfileGenderEdtTxt;

    private UserDetailsEntity mUserDetailsEntityRes;

    ArrayList<String> mGenderList;

    Dialog mGenderDialog;

    ProfileGenderAdapter mGenderAdapter;

    String mFriendReferalCode = "";

    @BindView(R.id.profile_camera_img)
    ImageView mProfileCameraImg;

    @BindView(R.id.profile_referal_txt)
    TextView mProfileReferalTxt;

    @BindView(R.id.profile_referal_lay)
    RelativeLayout mProfileReferalLay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_profile_screen);

        initView();
    }
    public void initView(){
        ButterKnife.bind(this);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(ProfileScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        editable(false);
        setHeader();

        getReferalCodeApiCall();

        getProfileDetailsApiCall();

        getLanguage();


        mProfileNameEdtTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    mProfileNameEdtTxt.clearFocus();
                    mProfileEmailEdtTxt.requestFocus();
                    mProfileEmailEdtTxt.setCursorVisible(true);
                    return true; // Focus will do whatever you put in the logic.
                }
                return false;  // Focus will change according to the actionId
            }
        });

        mGenderList = new ArrayList<>();
        mGenderList.add(getResources().getString(R.string.male));
        mGenderList.add(getResources().getString(R.string.female));

    }

    @OnClick({R.id.header_left_side_img,R.id.user_profile_pic_lay,R.id.profile_dob_lay,R.id.profile_dob_edt_txt_lay,R.id.profile_update_profile_btn,R.id.profile_cancel_btn,R.id.profile_save_btn,R.id.profile_gender_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.user_profile_pic_lay:
                if (editableBool){
                    if (checkPermission()){
                        uploadImage();
                    }
                }
                break;
            case R.id.profile_dob_lay:
                if (editableBool){
                    AppConstants.DATE_PICKER_COND = "PROFILE";
                    DialogFragment newFragment = new SelectDateFragment();
                    newFragment.show(getSupportFragmentManager(), "DatePicker");
                }
                break;
            case R.id.profile_dob_edt_txt_lay:
                if (editableBool){
                    AppConstants.DATE_PICKER_COND = "PROFILE";
                    DialogFragment newFragment = new SelectDateFragment();
                    newFragment.show(getSupportFragmentManager(), "DatePicker");
                }
                break;
            case R.id.profile_update_profile_btn:
                mProfileCancelSaveLay.setVisibility(View.VISIBLE);
                mProfileUpdateProfileBtn.setVisibility(View.GONE);
                mProfileReferalLay.setVisibility(View.GONE);
                editableBool = true;
                editable(true);
                break;
            case R.id.profile_cancel_btn:
                editableBool = false;
                editable(false);
                setHeader();
                mProfileCancelSaveLay.setVisibility(View.GONE);
                mProfileUpdateProfileBtn.setVisibility(View.VISIBLE);
                mProfileReferalLay.setVisibility(View.VISIBLE);
                break;
            case R.id.profile_save_btn:
                if (mProfileNameEdtTxt.getText().toString().trim().isEmpty()){
                    mProfileNameEdtTxt.clearAnimation();
                    mProfileNameEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mProfileNameEdtTxt.setError(getResources().getString(R.string.please_fill_your_name));
                }
                else if (mProfileGenderEdtTxt.getText().toString().trim().isEmpty()){
                    mProfileGenderEdtTxt.clearAnimation();
                    mProfileGenderEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mProfileGenderEdtTxt.setError(getResources().getString(R.string.please_select_gender));
                }
                else if (mProfileEmailEdtTxt.getText().toString().trim().isEmpty()){
                    mProfileEmailEdtTxt.clearAnimation();
                    mProfileEmailEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mProfileEmailEdtTxt.setError(getResources().getString(R.string.please_give_your_email_address));
                }else if (!isEmailValid(mProfileEmailEdtTxt.getText().toString().trim())){
                    mProfileEmailEdtTxt.clearAnimation();
                    mProfileEmailEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mProfileEmailEdtTxt.setError(getResources().getString(R.string.please_give_valid_email_address));
                }else if (mProfileDobEdtTxt.getText().toString().trim().isEmpty()){
                    mProfileDobEdtTxt.clearAnimation();
                    mProfileDobEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mProfileDobEdtTxt.setError(getResources().getString(R.string.please_give_your_dob));
                }
                else {
                    if (!IMAGE_PATH.equalsIgnoreCase("")){
                        uploadProfileImageApiCall();
                    }else {
                        uploadProfileApiCall(mUserDetailsEntityRes.getUSER_IMAGE());
//                        updateProfileNameApiCall(mUserDetailsEntityRes.getUSER_IMAGE());
                    }

                }
                break;
            case R.id.profile_gender_lay:
                if (editableBool){
                    if (mGenderList.size() > 0){
                        alertDismiss(mGenderDialog);
                        mGenderDialog = getDialog(ProfileScreen.this, R.layout.pop_up_country_alert);

                        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                        Window window = mGenderDialog.getWindow();

                        if (window != null) {
                            LayoutParams.copyFrom(window.getAttributes());
                            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(LayoutParams);
                            window.setGravity(Gravity.CENTER);
                        }

                        TextView cancelTxt , headerTxt;

                        RecyclerView countryRecyclerView;

                        /*Init view*/
                        cancelTxt = mGenderDialog.findViewById(R.id.country_text_cancel);
                        headerTxt = mGenderDialog.findViewById(R.id.country_header_text_cancel);

                        countryRecyclerView = mGenderDialog.findViewById(R.id.country_popup_recycler_view);
                        mGenderAdapter = new ProfileGenderAdapter(ProfileScreen.this, mGenderList,mGenderDialog);

                        countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        countryRecyclerView.setAdapter(mGenderAdapter);

                        /*Set data*/
                        headerTxt.setText(getResources().getString(R.string.choose_your_gender));
                        cancelTxt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mGenderDialog.dismiss();
                            }
                        });

                        alertShowing(mGenderDialog);
                    }else {
                        Toast.makeText(ProfileScreen.this,getResources().getString(R.string.sorry_no_result_found) , Toast.LENGTH_SHORT).show();
                    }
                }

                break;
        }
    }

    public void getProfileDetailsApiCall(){
    if (NetworkUtil.isNetworkAvailable(ProfileScreen.this)){
        APIRequestHandler.getInstance().getProfileDetailsApiCall(mUserDetailsEntityRes.getUSER_ID(),ProfileScreen.this);
    }else {
        DialogManager.getInstance().showNetworkErrorPopup(ProfileScreen.this, new InterfaceBtnCallBack() {
            @Override
            public void onPositiveClick() {
                getProfileDetailsApiCall();

            }
        });
    }
    }

    public void editable(boolean editable){
        if (editable){
            mProfileNameEdtTxt.setEnabled(true);
            mProfileMobileEdtTxt.setEnabled(false);
            mProfileEmailEdtTxt.setEnabled(true);
            mProfileGenderEdtTxt.setEnabled(false);
            mProfileCameraImg.setAlpha(255);
//            mProfileCameraImg.setVisibility(View.VISIBLE);

        }else {
            mProfileNameEdtTxt.setEnabled(false);
            mProfileMobileEdtTxt.setEnabled(false);
            mProfileEmailEdtTxt.setEnabled(false);
            mProfileGenderEdtTxt.setEnabled(false);
            mProfileCameraImg.setAlpha(128);
//            mProfileCameraImg.setVisibility(View.GONE);

        }
    }

    public void uploadProfileImageApiCall(){
        if (NetworkUtil.isNetworkAvailable(ProfileScreen.this)){
            if (!IMAGE_PATH.isEmpty()) {
                mUserImageFile = new File(IMAGE_PATH);

                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), mUserImageFile);

                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("BuyerImages", mUserImageFile.getName(), requestFile);
                APIRequestHandler.getInstance().updateProfile1(body,ProfileScreen.this);
            }
        }else {
            uploadProfileImageApiCall();
        }
    }

    public void uploadProfileApiCall(final String profileImg){

        AppConstants.USER_ID = Integer.parseInt(mUserDetailsEntityRes.getUSER_ID());
        if (NetworkUtil.isNetworkAvailable(ProfileScreen.this)){
            APIRequestHandler.getInstance().updateProfileApiCall(AppConstants.USER_ID,mProfileEmailEdtTxt.getText().toString().trim(),mProfileDobEdtTxt.getText().toString().trim(),mProfileGenderEdtTxt.getText().toString().toLowerCase(),"customer",mUserDetailsEntityRes.getLanguage(),mProfileNameEdtTxt.getText().toString().trim(),profileImg,ProfileScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ProfileScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    uploadProfileApiCall(profileImg);
                }
            });
        }
    }

    public void updateProfileNameApiCall(final String profileImg){
        if (NetworkUtil.isNetworkAvailable(ProfileScreen.this)){
            APIRequestHandler.getInstance().profileIntroAPICall(AppConstants.USER_ID,mProfileNameEdtTxt.getText().toString().trim(),profileImg,mProfileEmailEdtTxt.getText().toString(),mFriendReferalCode,"OldUser ProfileScreen",ProfileScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ProfileScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    updateProfileNameApiCall(profileImg);
                }
            });
        }
    }

    public void getReferalCodeApiCall(){
        if (NetworkUtil.isNetworkAvailable(ProfileScreen.this)){
            APIRequestHandler.getInstance().getReferalCodeApiCall(mUserDetailsEntityRes.getUSER_ID(),ProfileScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ProfileScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getReferalCodeApiCall();

                }
            });
        }
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.profile));
        mRightSideImg.setVisibility(View.INVISIBLE);


        try {
            Glide.with(this)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/BuyerImages/"+mUserDetailsEntityRes.getUSER_IMAGE())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.profile_place_holder_icon).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true))
                    .into(mProfileImg);
        } catch (Exception ex) {
            mProfileImg.setImageResource(R.drawable.empty_img);
        }

        mProfileNameEdtTxt.setText(mUserDetailsEntityRes.getUSER_NAME());
        mProfileMobileEdtTxt.setText(mUserDetailsEntityRes.getCOUNTRY_CODE()+mUserDetailsEntityRes.getMOBILE_NUM());
        mProfileEmailEdtTxt.setText(mUserDetailsEntityRes.getEMAIL());
        mProfileDobEdtTxt.setText(mUserDetailsEntityRes.getDOB());
        mProfileGenderEdtTxt.setText(mUserDetailsEntityRes.getGENDER());

    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.profile_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.profile_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
        }
    }
    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isValid(String s)
    {
        // The given argument to compile() method
        // is regular expression. With the help of
        // regular expression we can validate mobile
        // number.
        // 1) Begins with 0 or 91
        // 2) Then contains 7 or 8 or 9.
        // 3) Then contains 9 digits
        Pattern p = Pattern.compile("(0/91)?[7-9][0-9]{9}");

        // Pattern class contains matcher() method
        // to find matching between given number
        // and regular expression
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }
    /* Ask for permission on Camera access*/
    private boolean checkPermission() {

        boolean addPermission = true;
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            int cameraPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
            int readStoragePermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
            int storagePermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (storagePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            addPermission = askAccessPermission(listPermissionsNeeded, 1, new InterfaceTwoBtnCallBack() {
                @Override
                public void onNegativeClick() {

                }


                @Override
                public void onPositiveClick() {
                    uploadImage();
                }

            });
        }

        return addPermission;

    }


    private void uploadImage() {
        DialogManager.getInstance().showImageUploadPopup(this, getResources().getString(R.string.select_phot_type), getResources().getString(R.string.take_camera), getResources().getString(R.string.open_gallery), new InterfaceTwoBtnCallBack() {
            @Override
            public void onNegativeClick() {
                captureImage();
            }
            @Override
            public void onPositiveClick() {
                galleryImage();
            }
        });
    }

    /*open camera Image*/
    private void captureImage() {

        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);

    }

    /*open gallery Image*/
    private void galleryImage() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, REQUEST_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            /*Open Camera Request Check*/
            case REQUEST_CAMERA:
                if (resultCode == RESULT_OK) {
                    Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//                    String path = saveImage(thumbnail);
                    IMAGE_PATH = compressImage(String.valueOf(getImageUri(getContext(),thumbnail)));
//                    IMAGE_PATH = path;
//                    saveImage(thumbnail);

                    Glide.with(this)
                            .load(IMAGE_PATH)
                            .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.profile_place_holder_icon).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true))
                            .into(mProfileImg);

                } else {
                    if (resultCode == RESULT_CANCELED) {
                        /*Cancelling the image capture process by the user*/
                    } else {
                        /*image capture getting failed due to certail technical issues*/
                        Toast.makeText(getApplicationContext(), "Image capture getting failed due to certail technical issues!", Toast.LENGTH_SHORT).show();

                    }
                }
                break;
            /*Open Photo Gallery Request Check*/
            case REQUEST_GALLERY:
                if (resultCode == RESULT_OK) {

                    Uri contentURI = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), contentURI);
//                        String path = saveImage(bitmap);
                        IMAGE_PATH = compressImage(String.valueOf(contentURI));

                        Glide.with(this)
                                .load(IMAGE_PATH)
                                .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.profile_place_holder_icon).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true))
                                .into(mProfileImg);

                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "I Can't find the location this file!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    if (resultCode == RESULT_CANCELED) {
                        /*Cancelling the image capture process by the user*/

                    } else {
                        /*image capture getting failed due to certail technical issues*/
                    }
                }
                break;

        }
    }
    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            if (checkPermission() == true) {
                wallpaperDirectory.mkdirs();

            }
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getApplicationContext(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {               imgRatio = maxHeight / actualHeight;                actualWidth = (int) (imgRatio * actualWidth);               actualHeight = (int) maxHeight;             } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;

        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height/ (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;      }       final float totalPixels = width * height;       final float totalReqPixelsCap = reqWidth * reqHeight * 2;       while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof UpdateProfileResponse){
            UpdateProfileResponse mResponse = (UpdateProfileResponse)resObj;
            mProfileCancelSaveLay.setVisibility(View.VISIBLE);
            mProfileUpdateProfileBtn.setVisibility(View.GONE);

            mUserDetailsEntityRes.setUSER_NAME(mProfileNameEdtTxt.getText().toString());
            mUserDetailsEntityRes.setMOBILE_NUM(mProfileMobileEdtTxt.getText().toString().trim());
            mUserDetailsEntityRes.setDOB(mProfileDobEdtTxt.getText().toString().trim());
            mUserDetailsEntityRes.setEMAIL(mProfileEmailEdtTxt.getText().toString().trim());
            mUserDetailsEntityRes.setGENDER(mProfileGenderEdtTxt.getText().toString().trim());

            PreferenceUtil.storeUserDetails(ProfileScreen.this,mUserDetailsEntityRes);
//            nextScreen(BottomNavigationScreen.class,true);
            DialogManager.getInstance().showAlertPopup(ProfileScreen.this, getResources().getString(R.string.update_successfully), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    onBackPressed();
                }
            });

        }
        if (resObj instanceof FileUploadResponse){
            FileUploadResponse mResponse = (FileUploadResponse) resObj;
            String[] parts = mResponse.getResult().get(0).split("\\\\");
            String part1 = parts[0]; // 004
            String part2 = parts[1];
            String lastOne = parts[parts.length - 1];
            mUserDetailsEntityRes.setUSER_IMAGE(lastOne);

            uploadProfileApiCall(lastOne);
//            updateProfileNameApiCall(lastOne);
            PreferenceUtil.storeUserDetails(ProfileScreen.this,mUserDetailsEntityRes);
        }
        if (resObj instanceof GetProfileDetailsResponse){
            GetProfileDetailsResponse mResponse = (GetProfileDetailsResponse)resObj;

            if (mResponse.getResult().size() > 0){
                mUserDetailsEntityRes.setUSER_NAME(mResponse.Result.get(0).getName());
                mUserDetailsEntityRes.setUSER_IMAGE(mResponse.Result.get(0).getProfilePicture());
                mUserDetailsEntityRes.setCOUNTRY_CODE("+"+mResponse.getResult().get(0).getCountryCode()+" ");
                mUserDetailsEntityRes.setMOBILE_NUM(mResponse.Result.get(0).getPhoneNumber());
                mUserDetailsEntityRes.setDOB(mResponse.Result.get(0).getDob().replace("T00:00:00",""));
                mUserDetailsEntityRes.setEMAIL(mResponse.Result.get(0).getEmail());
                mUserDetailsEntityRes.setGENDER(mResponse.Result.get(0).getGender());

                PreferenceUtil.storeUserDetails(ProfileScreen.this,mUserDetailsEntityRes);
                setHeader();
            }

        }
        if (resObj instanceof ProfileIntroResponse){
            ProfileIntroResponse mResponse = (ProfileIntroResponse)resObj;
        }
        if (resObj instanceof GetReferalCodeResponse){
            GetReferalCodeResponse mResponse = (GetReferalCodeResponse)resObj;
            mFriendReferalCode = mResponse.getResult().getReferenceCode();
            mProfileReferalTxt.setText(mResponse.getResult().getInvitecode());
        }

    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }

    }
    /*Default dialog init method*/
    public Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
