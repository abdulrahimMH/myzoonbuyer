package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetCouponPriceResponseEntity implements Serializable {
    public int DiscountValue;
    public int DiscountType;
    public double minimumamount;
    public double MaximumDiscount;
    public int CoupanRedemtionmethod;
    public int CoupanType;
    public int CouponAppliesTo;

    public int getDiscountValue() {
        return DiscountValue;
    }

    public void setDiscountValue(int discountValue) {
        DiscountValue = discountValue;
    }

    public int getDiscountType() {
        return DiscountType;
    }

    public void setDiscountType(int discountType) {
        DiscountType = discountType;
    }

    public double getMinimumamount() {
        return minimumamount;
    }

    public void setMinimumamount(double minimumamount) {
        this.minimumamount = minimumamount;
    }

    public double getMaximumDiscount() {
        return MaximumDiscount;
    }

    public void setMaximumDiscount(double maximumDiscount) {
        MaximumDiscount = maximumDiscount;
    }

    public int getCoupanRedemtionmethod() {
        return CoupanRedemtionmethod;
    }

    public void setCoupanRedemtionmethod(int coupanRedemtionmethod) {
        CoupanRedemtionmethod = coupanRedemtionmethod;
    }

    public int getCoupanType() {
        return CoupanType;
    }

    public void setCoupanType(int coupanType) {
        CoupanType = coupanType;
    }

    public int getCouponAppliesTo() {
        return CouponAppliesTo;
    }

    public void setCouponAppliesTo(int couponAppliesTo) {
        CouponAppliesTo = couponAppliesTo;
    }

}
