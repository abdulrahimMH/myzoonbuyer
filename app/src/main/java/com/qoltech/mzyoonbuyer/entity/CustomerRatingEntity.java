package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class CustomerRatingEntity implements Serializable {
    public String Name;
    public String Review;
    public double Rating;
    public String CreatedDt;
    public String ProfilePicture;

    public String getProfilePicture() {
        return ProfilePicture == null ? "" : ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getName() {
        return Name ==  null ? "" : Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getReview() {
        return Review == null ? "" : Review;
    }

    public void setReview(String review) {
        Review = review;
    }

    public double getRating() {
        return Rating;
    }

    public void setRating(double rating) {
        Rating = rating;
    }

    public String getCreateDt() {
        return CreatedDt == null ? "" : CreatedDt;
    }

    public void setCreateDt(String createDt) {
        CreatedDt = createDt;
    }


}
