package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetOrederPaymentStatusEntity implements Serializable {

    public long TransactionId;
    public String PaidOn;
    public double Amount;
    public String cardlast4;
    public int Points;
    public String ReduceAmount;
    public String CoupanReduceAmount;
    public double TotalAmount;
    public long Transactionid;

    public long getTransactionid() {
        return Transactionid;
    }

    public void setTransactionid(long transactionid) {
        Transactionid = transactionid;
    }

    public String getCoupanReduceAmount() {
        return CoupanReduceAmount;
    }

    public void setCoupanReduceAmount(String coupanReduceAmount) {
        CoupanReduceAmount = coupanReduceAmount;
    }

    public double getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        TotalAmount = totalAmount;
    }

    public int getPoints() {
        return Points;
    }

    public void setPoints(int points) {
        Points = points;
    }

    public String getReduceAmount() {
        return ReduceAmount;
    }

    public void setReduceAmount(String reduceAmount) {
        ReduceAmount = reduceAmount;
    }

    public long getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(long transactionId) {
        TransactionId = transactionId;
    }

    public String getPaidOn() {
        return PaidOn == null ? "" : PaidOn;
    }

    public void setPaidOn(String paidOn) {
        PaidOn = paidOn;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public String getCardlast4() {
        return cardlast4 == null ? "" : cardlast4;
    }

    public void setCardlast4(String cardlast4) {
        this.cardlast4 = cardlast4;
    }

}
