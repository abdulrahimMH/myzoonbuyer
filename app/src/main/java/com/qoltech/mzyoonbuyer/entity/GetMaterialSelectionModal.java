package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetMaterialSelectionModal implements Serializable {
    public String Type;
    public String DressSubTypeId;
    public int Tailorid;
    public ArrayList<ApicallidEntity> Thickness;
    public ArrayList<ApicallidEntity> SubColor;
    public ArrayList<ApicallidEntity> PlaceofOrginId;
    public ArrayList<ApicallidEntity> BrandId;
    public ArrayList<ApicallidEntity> MaterialTypeId;

    public ArrayList<ApicallidEntity> getPlaceofOrginId() {
        return PlaceofOrginId == null ? new ArrayList<>() : PlaceofOrginId;
    }

    public void setPlaceofOrginId(ArrayList<ApicallidEntity> placeofOrginId) {
        PlaceofOrginId = placeofOrginId;
    }

    public ArrayList<ApicallidEntity> getBrandId() {
        return BrandId == null ? new ArrayList<>() : BrandId;
    }

    public void setBrandId(ArrayList<ApicallidEntity> brandId) {
        BrandId = brandId;
    }

    public ArrayList<ApicallidEntity> getMaterialTypeId() {
        return MaterialTypeId == null ? new ArrayList<>() : MaterialTypeId;
    }

    public void setMaterialTypeId(ArrayList<ApicallidEntity> materialTypeId) {
        MaterialTypeId = materialTypeId;
    }

    public String getType() {
        return Type == null ? "" : Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getDressSubTypeId() {
        return DressSubTypeId == null ? "" : DressSubTypeId;
    }

    public void setDressSubTypeId(String dressSubTypeId) {
        DressSubTypeId = dressSubTypeId;
    }

    public int getTailorid() {
        return Tailorid;
    }

    public void setTailorid(int tailorid) {
        Tailorid = tailorid;
    }

    public ArrayList<ApicallidEntity> getThickness() {
        return Thickness == null ? new ArrayList<>() : Thickness;
    }

    public void setThickness(ArrayList<ApicallidEntity> thickness) {
        Thickness = thickness;
    }

    public ArrayList<ApicallidEntity> getSubColor() {
        return SubColor == null ? new ArrayList<>() : SubColor;
    }

    public void setSubColor(ArrayList<ApicallidEntity> subColor) {
        SubColor = subColor;
    }

}
