package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class ServiceTypeEntity implements Serializable {
    public int Id;
    public String DeliveryTypeInEnglish;
    public String DeliveryTypeInArabic;
    public String HeaderImage;
    public String DeliveryImage;
    public boolean Status;
    public String TailorMessage;
    public String DeliveryTime;
    public int Express;

    public int getExpress() {
        return Express;
    }

    public void setExpress(int express) {
        Express = express;
    }

    public String getTailorMessage() {
        return TailorMessage == null ? "" : TailorMessage;
    }

    public void setTailorMessage(String tailorMessage) {
        TailorMessage = tailorMessage;
    }

    public String getDeliveryTime() {
        return DeliveryTime == null ? "" : DeliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        DeliveryTime = deliveryTime;
    }


    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDeliveryTypeInEnglish() {
        return DeliveryTypeInEnglish == null ? "" : DeliveryTypeInEnglish;
    }

    public void setDeliveryTypeInEnglish(String deliveryTypeInEnglish) {
        DeliveryTypeInEnglish = deliveryTypeInEnglish;
    }

    public String getDeliveryTypeInArabic() {
        return DeliveryTypeInArabic == null ? "" : DeliveryTypeInArabic;
    }

    public void setDeliveryTypeInArabic(String deliveryTypeInArabic) {
        DeliveryTypeInArabic = deliveryTypeInArabic;
    }

    public String getHeaderImage() {
        return HeaderImage == null ? "" : HeaderImage;
    }

    public void setHeaderImage(String headerImage) {
        HeaderImage = headerImage;
    }

    public String getDeliveryImage() {
        return DeliveryImage == null ? "" : DeliveryImage;
    }

    public void setDeliveryImage(String deliveryImage) {
        DeliveryImage = deliveryImage;
    }

}
