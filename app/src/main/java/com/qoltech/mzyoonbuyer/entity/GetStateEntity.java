package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetStateEntity  implements Serializable {
    public int Id;
    public int CountryId;
    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getCountryId() {
        return CountryId;
    }

    public void setCountryId(int countryId) {
        CountryId = countryId;
    }

    public String getStateName() {
        return StateName == null ? "" : StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public String StateName;
}
