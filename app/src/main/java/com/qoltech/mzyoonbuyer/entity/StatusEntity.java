package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class StatusEntity implements Serializable {
    public String statusInArabic;
    public String Status;

    public String getStatusInArabic() {
        return statusInArabic == null ? "" : statusInArabic;
    }

    public void setStatusInArabic(String statusInArabic) {
        this.statusInArabic = statusInArabic;
    }

    public String getStatus() {
        return Status == null ? "" : Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
