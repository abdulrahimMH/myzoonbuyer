package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class RatingsEntity implements Serializable {
    public int CategoryId;
    public String Value;
    public int CategoryCount;
    public double Rating;
    public String ArabicValue;

    public String getArabicValue() {
        return ArabicValue == null ? "" : ArabicValue;
    }

    public void setArabicValue(String arabicValue) {
        ArabicValue = arabicValue;
    }


    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int categoryId) {
        CategoryId = categoryId;
    }

    public String getValue() {
        return Value == null ? "" : Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public int getCategoryCount() {
        return CategoryCount;
    }

    public void setCategoryCount(int categoryCount) {
        CategoryCount = categoryCount;
    }

    public double getRating() {
        return Rating;
    }

    public void setRating(double rating) {
        Rating = rating;
    }


}
