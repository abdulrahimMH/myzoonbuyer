package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetReferalCodeEntity implements Serializable {
    public String invitecode;
    public String ReferenceCode;

    public String getInvitecode() {
        return invitecode == null ? "" : invitecode;
    }

    public void setInvitecode(String invitecode) {
        this.invitecode = invitecode;
    }

    public String getReferenceCode() {
        return ReferenceCode == null ? "" : ReferenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        ReferenceCode = referenceCode;
    }


}
