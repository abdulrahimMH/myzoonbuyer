package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GenderEntity implements Serializable {

    private int Id;
    private String gender;
    private String ImageURL;
    private String GenderInArabic;
    public boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getGender() {
        return gender == null ? "" : gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImageURL() {
        return ImageURL == null ? "" : ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getGenderInArabic() {
        return GenderInArabic == null ? "" : GenderInArabic;
    }

    public void setGenderInArabic(String genderInArabic) {
        GenderInArabic = genderInArabic;
    }



}
