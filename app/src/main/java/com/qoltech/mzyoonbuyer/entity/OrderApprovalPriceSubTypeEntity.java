package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class OrderApprovalPriceSubTypeEntity implements Serializable {
    public String Image;
    public String NameInEnglish;
    public String NameInArabic;
    public int DressSubTypeId;
    public String OrderTypeId;
    public int DeliveryTypeId;
    public int MeasurementTypeId;

    public String getOrderTypeId() {
        return OrderTypeId == null ? "" : OrderTypeId;
    }

    public void setOrderTypeId(String orderTypeId) {
        OrderTypeId = orderTypeId;
    }

    public int getDeliveryTypeId() {
        return DeliveryTypeId;
    }

    public void setDeliveryTypeId(int deliveryTypeId) {
        DeliveryTypeId = deliveryTypeId;
    }

    public int getMeasurementTypeId() {
        return MeasurementTypeId;
    }

    public void setMeasurementTypeId(int measurementTypeId) {
        MeasurementTypeId = measurementTypeId;
    }

    public String getNameInArabic() {
        return NameInArabic == null ? "" : NameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        NameInArabic = nameInArabic;
    }

    public int getDressSubTypeId() {
        return DressSubTypeId;
    }

    public void setDressSubTypeId(int dressSubTypeId) {
        DressSubTypeId = dressSubTypeId;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getNameInEnglish() {
        return NameInEnglish == null ? "" : NameInEnglish;
    }

    public void setNameInEnglish(String nameInEnglish) {
        NameInEnglish = nameInEnglish;
    }

}
