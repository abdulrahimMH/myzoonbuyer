package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetMaterialSelectionEntity implements Serializable {
    public int Id;
    public String PatternInEnglish;
    public String PatternInArabic;
    public String SoftandThicknessInEnglish;
    public String SoftandThicknessInArabic;
    public String Image;
    public double MaterialCharges;
    public boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getPatternInEnglish() {
        return PatternInEnglish ==  null ? "" : PatternInEnglish;
    }

    public void setPatternInEnglish(String patternInEnglish) {
        PatternInEnglish = patternInEnglish;
    }

    public String getPatternInArabic() {
        return PatternInArabic == null ? "" : PatternInArabic;
    }

    public void setPatternInArabic(String patternInArabic) {
        PatternInArabic = patternInArabic;
    }

    public String getSoftandThicknessInEnglish() {
        return SoftandThicknessInEnglish == null ? "" : SoftandThicknessInEnglish;
    }

    public void setSoftandThicknessInEnglish(String softandThicknessInEnglish) {
        SoftandThicknessInEnglish = softandThicknessInEnglish;
    }

    public String getSoftandThicknessInArabic() {
        return SoftandThicknessInArabic == null ? "" : SoftandThicknessInArabic;
    }

    public void setSoftandThicknessInArabic(String softandThicknessInArabic) {
        SoftandThicknessInArabic = softandThicknessInArabic;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public double getMaterialCharges() {
        return MaterialCharges;
    }

    public void setMaterialCharges(double materialCharges) {
        MaterialCharges = materialCharges;
    }

}
