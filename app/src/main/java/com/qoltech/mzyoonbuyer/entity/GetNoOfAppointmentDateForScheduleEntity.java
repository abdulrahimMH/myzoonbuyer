package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetNoOfAppointmentDateForScheduleEntity implements Serializable {
    public int NoOfDays;
    public boolean IsSchedule;

    public boolean isSchedule() {
        return IsSchedule;
    }

    public void setSchedule(boolean schedule) {
        IsSchedule = schedule;
    }


    public int getNoOfDays() {
        return NoOfDays;
    }

    public void setNoOfDays(int noOfDays) {
        NoOfDays = noOfDays;
    }
}
