package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetStoreReviewEntity implements Serializable {

    public int OrderId;
    public String OrderlineId;
    public String review;
    public double Rating;

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

    public String getOrderlineId() {
        return OrderlineId == null ? "" : OrderlineId;
    }

    public void setOrderlineId(String orderlineId) {
        OrderlineId = orderlineId;
    }

    public String getReview() {
        return review == null ? "" : review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public double getRating() {
        return Rating;
    }

    public void setRating(double rating) {
        Rating = rating;
    }

}
