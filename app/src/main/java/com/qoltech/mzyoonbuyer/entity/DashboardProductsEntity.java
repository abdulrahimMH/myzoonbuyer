package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class DashboardProductsEntity implements Serializable {

    public String Id;
    public String ProductName;
    public String BrandName;
    public String ProductImage;
    public double Amount;
    public int offer;
    public double DiscounAmount;
    public boolean IsProduct;
    public int SellerId;
    public int CategoryId;
    public String DisplayId;
    public String ProductNameInArabic;
    public String ProductDescription;
    public boolean IsActive;
    public int CreatedBy;
    public boolean WishLish;
    public int AvailibilityCount;
    public int Discount;
    public String DiscountType;
    public String ShopNameInEnglish;
    public String BrandId;
    public String ProductDescriptionInArabic;
    public int RatingsCount;
    public double RatingsValue;
    public double NewPrice;
    public String ShareUrl;
    public String BrandNameInArabic;

    public String getBrandNameInArabic() {
        return BrandNameInArabic == null ? "" : BrandNameInArabic;
    }

    public void setBrandNameInArabic(String brandNameInArabic) {
        BrandNameInArabic = brandNameInArabic;
    }

    public String getShareUrl() {
        return ShareUrl == null ? "" : ShareUrl;
    }

    public void setShareUrl(String shareUrl) {
        ShareUrl = shareUrl;
    }

    public String getId() {
        return Id == null ? "" : Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getProductName() {
        return ProductName == null ? "" : ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName == null ? "" : BrandName;
    }

    public String getProductImage() {
        return ProductImage;
    }

    public void setProductImage(String productImage) {
        ProductImage = productImage == null ? "" : ProductImage;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public int getOffer() {
        return offer;
    }

    public void setOffer(int offer) {
        this.offer = offer;
    }

    public double getDiscounAmount() {
        return DiscounAmount;
    }

    public void setDiscounAmount(double discounAmount) {
        DiscounAmount = discounAmount;
    }

    public boolean isProduct() {
        return IsProduct;
    }

    public void setProduct(boolean product) {
        IsProduct = product;
    }

    public int getSellerId() {
        return SellerId;
    }

    public void setSellerId(int sellerId) {
        SellerId = sellerId;
    }
    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int categoryId) {
        CategoryId = categoryId;
    }

    public String getDisplayId() {
        return DisplayId == null ? "" : DisplayId;
    }

    public void setDisplayId(String displayId) {
        DisplayId = displayId;
    }

    public String getProductNameInArabic() {
        return ProductNameInArabic == null ? "" : ProductNameInArabic;
    }

    public void setProductNameInArabic(String productNameInArabic) {
        ProductNameInArabic = productNameInArabic;
    }

    public String getProductDescription() {
        return ProductDescription == null ? "" : ProductDescription;
    }

    public void setProductDescription(String productDescription) {
        ProductDescription = productDescription;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public int getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(int createdBy) {
        CreatedBy = createdBy;
    }

    public boolean isWishLish() {
        return WishLish;
    }

    public void setWishLish(boolean wishLish) {
        WishLish = wishLish;
    }

    public int getAvailibilityCount() {
        return AvailibilityCount;
    }

    public void setAvailibilityCount(int availibilityCount) {
        AvailibilityCount = availibilityCount;
    }

    public int getDiscount() {
        return Discount;
    }

    public void setDiscount(int discount) {
        Discount = discount;
    }

    public String getDiscountType() {
        return DiscountType == null ? "" : DiscountType;
    }

    public void setDiscountType(String discountType) {
        DiscountType = discountType;
    }

    public String getShopNameInEnglish() {
        return ShopNameInEnglish == null ? "" : ShopNameInEnglish;
    }

    public void setShopNameInEnglish(String shopNameInEnglish) {
        ShopNameInEnglish = shopNameInEnglish;
    }

    public String getBrandId() {
        return BrandId == null ? "" : BrandId;
    }

    public void setBrandId(String brandId) {
        BrandId = brandId;
    }

    public String getProductDescriptionInArabic() {
        return ProductDescriptionInArabic == null ? "" : ProductDescriptionInArabic;
    }

    public void setProductDescriptionInArabic(String productDescriptionInArabic) {
        ProductDescriptionInArabic = productDescriptionInArabic;
    }

    public int getRatingsCount() {
        return RatingsCount;
    }

    public void setRatingsCount(int ratingsCount) {
        RatingsCount = ratingsCount;
    }

    public double getRatingsValue() {
        return RatingsValue;
    }

    public void setRatingsValue(double ratingsValue) {
        RatingsValue = ratingsValue;
    }

    public double getNewPrice() {
        return NewPrice;
    }

    public void setNewPrice(double newPrice) {
        NewPrice = newPrice;
    }


}
