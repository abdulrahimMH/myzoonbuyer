package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class TaxAmountEntity implements Serializable {
    public int getTaxAmount() {
        return TaxAmount;
    }

    public void setTaxAmount(int taxAmount) {
        TaxAmount = taxAmount;
    }

    public int TaxAmount;

}
