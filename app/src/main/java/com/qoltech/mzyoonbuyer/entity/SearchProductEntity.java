package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class SearchProductEntity implements Serializable {

    public String ProductName;
    public String ProductNameInArabic;
    public String Title;
    public String Image;
    public boolean checked;
    public boolean searchChecked;
    public String TitleInArabic;

    public String getTitleInArabic() {
        return TitleInArabic == null ? "" : TitleInArabic;
    }

    public void setTitleInArabic(String titleInArabic) {
        TitleInArabic = titleInArabic;
    }

    public boolean isSearchChecked() {
        return searchChecked;
    }

    public void setSearchChecked(boolean searchChecked) {
        this.searchChecked = searchChecked;
    }


    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }


    public String getProductName() {
        return ProductName == null ? "" : ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductNameInArabic() {
        return ProductNameInArabic == null ? "" : ProductNameInArabic;
    }

    public void setProductNameInArabic(String productNameInArabic) {
        ProductNameInArabic = productNameInArabic;
    }

    public String getTitle() {
        return Title == null ? "" : Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

}
