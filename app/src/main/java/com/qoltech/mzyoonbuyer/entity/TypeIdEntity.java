package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class TypeIdEntity implements Serializable {
    public String Type;

    public String getType() {
        return Type == null ? "" : Type;
    }

    public void setType(String type) {
        Type = type;
    }


}
