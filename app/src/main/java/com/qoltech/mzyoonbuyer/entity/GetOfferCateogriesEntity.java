package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetOfferCateogriesEntity implements Serializable {

    public String couponcodeId;
    public int DiscountValue;
    public int DiscountType;
    public double MinimumAmount;
    public double MaximumDiscount;
    public String ValidFrom;
    public String ValidTo;
    public String image;
    public String Message;
    public String TailorNameInEnglish;
    public String ShopNameInEnglish;
    public int Id;
    public String DressTypeId;
    public int TailorId;

    public double getMinimumAmount() {
        return MinimumAmount;
    }

    public void setMinimumAmount(double minimumAmount) {
        MinimumAmount = minimumAmount;
    }

    public double getMaximumDiscount() {
        return MaximumDiscount;
    }

    public void setMaximumDiscount(double maximumDiscount) {
        MaximumDiscount = maximumDiscount;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDressTypeId() {
        return DressTypeId;
    }

    public void setDressTypeId(String dressTypeId) {
        DressTypeId = dressTypeId;
    }

    public int getTailorId() {
        return TailorId;
    }

    public void setTailorId(int tailorId) {
        TailorId = tailorId;
    }

    public String getCouponcodeId() {
        return couponcodeId == null ? "" : couponcodeId;
    }

    public void setCouponcodeId(String couponcodeId) {
        this.couponcodeId = couponcodeId;
    }

    public int getDiscountValue() {
        return DiscountValue;
    }

    public void setDiscountValue(int discountValue) {
        DiscountValue = discountValue;
    }

    public int getDiscountType() {
        return DiscountType;
    }

    public void setDiscountType(int discountType) {
        DiscountType = discountType;
    }

    public String getValidFrom() {
        return ValidFrom == null ? "" : ValidFrom;
    }

    public void setValidFrom(String validFrom) {
        ValidFrom = validFrom;
    }

    public String getValidTo() {
        return ValidTo == null ? "" : ValidTo;
    }

    public void setValidTo(String validTo) {
        ValidTo = validTo;
    }

    public String getImage() {
        return image == null ? "" : image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMessage() {
        return Message == null ? "" : Message;
    }

    public void setMessage(String message) {
        Message = message ;
    }

    public String getTailorNameInEnglish() {
        return TailorNameInEnglish == null ? "" : TailorNameInEnglish;
    }

    public void setTailorNameInEnglish(String tailorNameInEnglish) {
        TailorNameInEnglish = tailorNameInEnglish;
    }

    public String getShopNameInEnglish() {
        return ShopNameInEnglish == null ? "" : ShopNameInEnglish;
     }

    public void setShopNameInEnglish(String shopNameInEnglish) {
        ShopNameInEnglish = shopNameInEnglish;
    }

}
