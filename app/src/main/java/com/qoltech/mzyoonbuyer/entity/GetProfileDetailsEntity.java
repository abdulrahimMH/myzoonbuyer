package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetProfileDetailsEntity implements Serializable {
public int Id;
public String CountryCode;
public String PhoneNumber;
public String Name;
public String Email;
public String LastViewedOn;
public String ProfilePicture;
public String Dob;
public String Gender;
public String CreatedOn;
public String ModifiedOn;
    public String ModifiedBy;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCountryCode() {
        return CountryCode == null ? "" : CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getPhoneNumber() {
        return PhoneNumber == null ? "" : PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getName() {
        return Name == null ? "" : Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email == null ? "" : Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getLastViewedOn() {
        return LastViewedOn ==  null ? "" : LastViewedOn;
    }

    public void setLastViewedOn(String lastViewedOn) {
        LastViewedOn = lastViewedOn;
    }

    public String getProfilePicture() {
        return ProfilePicture == null ? "" : ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getDob() {
        return Dob == null ? "" : Dob;
    }

    public void setDob(String dob) {
        Dob = dob;
    }

    public String getGender() {
        return Gender == null ? "" : Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getCreatedOn() {
        return CreatedOn == null ? "" :  CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedOn() {
        return ModifiedOn == null ? "" : ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String getModifiedBy() {
        return ModifiedBy == null ? "" : ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

}
