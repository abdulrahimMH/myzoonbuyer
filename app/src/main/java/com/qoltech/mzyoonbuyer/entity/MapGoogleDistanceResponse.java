package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class MapGoogleDistanceResponse implements Serializable {
    public ArrayList<MapGoogleDistanceEntity> getLegs() {
        return legs == null ? new ArrayList<>() : legs;
    }

    public void setLegs(ArrayList<MapGoogleDistanceEntity> legs) {
        this.legs = legs;
    }

    ArrayList<MapGoogleDistanceEntity> legs;
}
