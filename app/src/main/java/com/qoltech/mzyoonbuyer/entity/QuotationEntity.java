package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class QuotationEntity implements Serializable {

    public int Id;
    public String TailorNameInEnglish;
    public String TailorNameInArabic;
    public String ShopOwnerImageURL;
    public String ShopNameInEnglish;
    public String StichingTime;
    public int TailorId;
    public int RequestId;
    public String RequestedOn;
    public double TotalAmount;
    public String MaterialType;
    public String MaterialTypeInArabic;
    public String ServiceType;
    public String ServiceTypeInArabic;
    public String NoOfDays;
    public String DeliveryNameInEnglish;
    public String DeliveryNameInArabic;
    public int DeliveryId;
    public int AreaId;

    public int getAreaId() {
        return AreaId;
    }

    public void setAreaId(int areaId) {
        AreaId = areaId;
    }


    public String getDeliveryNameInEnglish() {
        return DeliveryNameInEnglish == null ? "" : DeliveryNameInEnglish;
    }

    public void setDeliveryNameInEnglish(String deliveryNameInEnglish) {
        DeliveryNameInEnglish = deliveryNameInEnglish;
    }

    public String getDeliveryNameInArabic() {
        return DeliveryNameInArabic == null ? "" : DeliveryNameInArabic;
    }

    public void setDeliveryNameInArabic(String deliveryNameInArabic) {
        DeliveryNameInArabic = deliveryNameInArabic;
    }

    public int getDeliveryId() {
        return DeliveryId;
    }

    public void setDeliveryId(int deliveryId) {
        DeliveryId = deliveryId;
    }

    public int getRequestId() {
        return RequestId;
    }

    public void setRequestId(int requestId) {
        RequestId = requestId;
    }

    public String getRequestedOn() {
        return RequestedOn == null ? "" : RequestedOn;
    }

    public void setRequestedOn(String requestedOn) {
        RequestedOn = requestedOn;
    }

    public double getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        TotalAmount = totalAmount;
    }

    public String getMaterialType() {
        return MaterialType == null ? "" : MaterialType;
    }

    public void setMaterialType(String materialType) {
        MaterialType = materialType;
    }

    public String getMaterialTypeInArabic() {
        return MaterialTypeInArabic == null ? "" : MaterialTypeInArabic;
    }

    public void setMaterialTypeInArabic(String materialTypeInArabic) {
        MaterialTypeInArabic = materialTypeInArabic;
    }

    public String getServiceType() {
        return ServiceType == null ? "" : ServiceType;
    }

    public void setServiceType(String serviceType) {
        ServiceType = serviceType;
    }

    public String getServiceTypeInArabic() {
        return ServiceTypeInArabic == null ? "" : ServiceTypeInArabic;
    }

    public void setServiceTypeInArabic(String serviceTypeInArabic) {
        ServiceTypeInArabic = serviceTypeInArabic;
    }

    public String getNoOfDays() {
        return NoOfDays == null ? "0" : NoOfDays;
    }

    public void setNoOfDays(String noOfDays) {
        NoOfDays = noOfDays;
    }
    public int getTailorId() {
        return TailorId;
    }

    public void setTailorId(int tailorId) {
        TailorId = tailorId;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTailorNameInEnglish() {
        return TailorNameInEnglish == null ? "" : TailorNameInEnglish;
    }

    public void setTailorNameInEnglish(String tailorNameInEnglish) {
        TailorNameInEnglish = tailorNameInEnglish;
    }

    public String getTailorNameInArabic() {
        return TailorNameInArabic == null ? "" : TailorNameInArabic;
    }

    public void setTailorNameInArabic(String tailorNameInArabic) {
        TailorNameInArabic = tailorNameInArabic;
    }

    public String getShopOwnerImageURL() {
        return ShopOwnerImageURL == null ? "" : ShopOwnerImageURL;
    }

    public void setShopOwnerImageURL(String shopOwnerImageURL) {
        ShopOwnerImageURL = shopOwnerImageURL;
    }

    public String getShopNameInEnglish() {
        return ShopNameInEnglish == null ?  "" : ShopNameInEnglish;
    }

    public void setShopNameInEnglish(String shopNameInEnglish) {
        ShopNameInEnglish = shopNameInEnglish;
    }

    public String getStichingTime() {
        return StichingTime == null ? "0" : StichingTime;
    }

    public void setStichingTime(String stichingTime) {
        StichingTime = stichingTime;
    }


}
