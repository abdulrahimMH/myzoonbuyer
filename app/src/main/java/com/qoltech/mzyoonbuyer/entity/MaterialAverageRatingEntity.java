package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class MaterialAverageRatingEntity implements Serializable {
    public double getAverageRating() {
        return AvgRating;
    }

    public void setAverageRating(double averageRating) {
        AvgRating = averageRating;
    }

    public double AvgRating;
}
