package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetRewardsEntity implements Serializable {
public String Type;
    public double Points;

    public String getType() {
        return Type == null ? "" : Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public double getPoints() {
        return Points;
    }

    public void setPoints(double points) {
        Points = points;
    }

}
