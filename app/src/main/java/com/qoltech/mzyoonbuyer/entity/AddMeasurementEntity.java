package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class AddMeasurementEntity implements Serializable {

    public String Name;
    public String Image;
    public String NameInEnglish;
    public String NameInArabic;
    public String Gender;
    public String GenderInArabic;
    public int Id;
    public String MeasurementBy;
    public String CreatedOn;

    public String getMeasurementBy() {
        return MeasurementBy == null ? "" : MeasurementBy;
    }

    public void setMeasurementBy(String measurementBy) {
        MeasurementBy = measurementBy;
    }

    public String getCreatedOn() {
        return CreatedOn == null ? "" : CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name == null ? "" : Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getNameInEnglish() {
        return NameInEnglish == null ? "" : NameInEnglish;
    }

    public void setNameInEnglish(String nameInEnglish) {
        NameInEnglish = nameInEnglish;
    }

    public String getNameInArabic() {
        return NameInArabic == null ? "" : NameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        NameInArabic = nameInArabic;
    }

    public String getGender() {
        return Gender == null ? "" : Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getGenderInArabic() {
        return GenderInArabic == null ? "" : GenderInArabic;
    }

    public void setGenderInArabic(String genderInArabic) {
        GenderInArabic = genderInArabic;
    }

}
