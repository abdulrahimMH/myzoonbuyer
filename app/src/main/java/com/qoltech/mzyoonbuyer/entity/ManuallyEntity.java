package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class ManuallyEntity implements Serializable {
    public int Id;
    public String UserName;
    public String Image;
    public String DressSubTypeId;
    public String NameInEnglish;
    public String NameInArabic;
    public String Gender;
    public String GenderInArabic;
    public String UserDate;
    public String Name;
    public String MeasurementBy;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getUserName() {
        return UserName == null ? "" : UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getDressSubTypeId() {
        return DressSubTypeId == null ? "" : DressSubTypeId;
    }

    public void setDressSubTypeId(String dressSubTypeId) {
        DressSubTypeId = dressSubTypeId;
    }

    public String getNameInEnglish() {
        return NameInEnglish == null ? "" : NameInEnglish;
    }

    public void setNameInEnglish(String nameInEnglish) {
        NameInEnglish = nameInEnglish;
    }

    public String getNameInArabic() {
        return NameInArabic == null ? "" : NameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        NameInArabic = nameInArabic;
    }

    public String getGender() {
        return Gender == null ? "" : Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getGenderInArabic() {
        return GenderInArabic == null ? "" : GenderInArabic;
    }

    public void setGenderInArabic(String genderInArabic) {
        GenderInArabic = genderInArabic;
    }

    public String getUserDate() {
        return UserDate == null ? "" : UserDate;
    }

    public void setUserDate(String userDate) {
        UserDate = userDate;
    }

    public String getName() {
        return Name == null ? "" : Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMeasurementBy() {
        return MeasurementBy == null ? "" : MeasurementBy;
    }

    public void setMeasurementBy(String measurementBy) {
        MeasurementBy = measurementBy;
    }



}
