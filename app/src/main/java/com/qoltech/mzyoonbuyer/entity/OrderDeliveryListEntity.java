package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class OrderDeliveryListEntity implements Serializable {
    public int OrderId;
    public String TailorNameInEnglish;
    public String TailorNameInArabic;
    public String Product_Name;
    public String NameInArabic;
    public String ShopNameInEnglish;
    public String ShopNameInArabic;
    public String OrderDate;
    public String Image;
    public int TailorId;
    public String OrderType;

    public String getOrderType() {
        return OrderType == null ? "" : OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public int getTailorId() {
        return TailorId;
    }

    public void setTailorId(int tailorId) {
        TailorId = tailorId;
    }


    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

    public String getTailorNameInEnglish() {
        return TailorNameInEnglish == null ? "" : TailorNameInEnglish;
    }

    public void setTailorNameInEnglish(String tailorNameInEnglish) {
        TailorNameInEnglish = tailorNameInEnglish;
    }

    public String getTailorNameInArabic() {
        return TailorNameInArabic == null ? "" : TailorNameInArabic;
    }

    public void setTailorNameInArabic(String tailorNameInArabic) {
        TailorNameInArabic = tailorNameInArabic;
    }

    public String getProduct_Name() {
        return Product_Name ==  null ? "" : Product_Name;
    }

    public void setProduct_Name(String product_Name) {
        Product_Name = product_Name;
    }

    public String getNameInArabic() {
        return NameInArabic == null ? "" : NameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        NameInArabic = nameInArabic;
    }

    public String getShopNameInEnglish() {
        return ShopNameInEnglish == null ? "" : ShopNameInEnglish;
    }

    public void setShopNameInEnglish(String shopNameInEnglish) {
        ShopNameInEnglish = shopNameInEnglish;
    }

    public String getShopNameInArabic() {
        return ShopNameInArabic == null ? "" : ShopNameInArabic;
    }

    public void setShopNameInArabic(String shopNameInArabic) {
        ShopNameInArabic = shopNameInArabic;
    }

    public String getOrderDate() {
        return OrderDate == null ? "" : OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

}
