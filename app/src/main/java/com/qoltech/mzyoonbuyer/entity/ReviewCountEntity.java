package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class ReviewCountEntity implements Serializable {
    public int getReviewCount() {
        return ReviewCount;
    }

    public void setReviewCount(int reviewCount) {
        ReviewCount = reviewCount;
    }

    public int ReviewCount;
}
