package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetCustomizationThreeEntity implements Serializable {
    public  int Id;
    public String AttributeNameInEnglish;
    public String AttributeNameInArabic;
    public String Images;
    private boolean isChecked;

    public boolean getChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }



    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getAttributeNameInEnglish() {
        return AttributeNameInEnglish == null ? "" : AttributeNameInEnglish;
    }

    public void setAttributeNameInEnglish(String attributeNameInEnglish) {
        AttributeNameInEnglish = attributeNameInEnglish;
    }

    public String getAttributeNameInArabic() {
        return AttributeNameInArabic == null ? "" : AttributeNameInArabic;
    }

    public void setAttributeNameInArabic(String attributeNameInArabic) {
        AttributeNameInArabic = attributeNameInArabic;
    }

    public String getImages() {
        return Images == null ? "" : Images;
    }

    public void setImages(String images) {
        Images = images;
    }

}
