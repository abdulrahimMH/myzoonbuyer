package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class PatternImgEntity implements Serializable {
    public String ImageName;

    public String getImageName() {
        return ImageName == null ? "" : ImageName;
    }

    public void setImageName(String imageName) {
        ImageName = imageName;
    }

}
