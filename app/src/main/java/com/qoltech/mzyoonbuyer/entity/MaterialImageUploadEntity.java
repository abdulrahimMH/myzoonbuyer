package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;


public class MaterialImageUploadEntity implements Serializable {

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String Image;

}
