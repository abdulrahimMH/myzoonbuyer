package com.qoltech.mzyoonbuyer.entity;

public class PaymentGatewayTransactionEntity {
    private String cvv;

    private String ca_valid;

    private String code;

    private String cardcode;

    private String message;

    private String tranref;

    private String cardlast4;

    private String status;

    private String avs;

    public String getCvv ()
    {
        return cvv;
    }

    public void setCvv (String cvv)
    {
        this.cvv = cvv;
    }

    public String getCa_valid ()
    {
        return ca_valid;
    }

    public void setCa_valid (String ca_valid)
    {
        this.ca_valid = ca_valid;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getCardcode ()
    {
        return cardcode;
    }

    public void setCardcode (String cardcode)
    {
        this.cardcode = cardcode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getTranref ()
    {
        return tranref;
    }

    public void setTranref (String tranref)
    {
        this.tranref = tranref;
    }

    public String getCardlast4 ()
    {
        return cardlast4;
    }

    public void setCardlast4 (String cardlast4)
    {
        this.cardlast4 = cardlast4;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getAvs ()
    {
        return avs;
    }

    public void setAvs (String avs)
    {
        this.avs = avs;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [cvv = "+cvv+", ca_valid = "+ca_valid+", code = "+code+", cardcode = "+cardcode+", message = "+message+", tranref = "+tranref+", cardlast4 = "+cardlast4+", status = "+status+", avs = "+avs+"]";
    }
}
