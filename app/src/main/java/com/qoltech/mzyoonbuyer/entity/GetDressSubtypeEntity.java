package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetDressSubtypeEntity implements Serializable {
    public String NameInEnglish;
    public String NameInArabic;
    public String Image;
    public int Qty;

    public String getNameInEnglish() {
        return NameInEnglish == null ? "" : NameInEnglish;
    }

    public void setNameInEnglish(String nameInEnglish) {
        NameInEnglish = nameInEnglish;
    }

    public String getNameInArabic() {
        return NameInArabic == null ? "" : NameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        NameInArabic = nameInArabic;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

}
