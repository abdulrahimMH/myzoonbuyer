package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetMeasurementPartEntity implements Serializable {

    public int Id;
    public String TextInEnglish;
    public String TextInArabic;
    public String ReferenceNumber;
    public int GenderMeasurementId;
    public String MeasurementValue;
    public double Value;
    public String Image;
    public double LengthInAndroid;
    public double LengthInIos;
    public double StartMeasurementValue;
    public double EndMeasurementValue;
    public String PartGifImage;

    public String getPartGifImage() {
        return PartGifImage == null ? "" : PartGifImage;
    }

    public void setPartGifImage(String partGifImage) {
        PartGifImage = partGifImage;
    }


    public double getStartMeasurementValue() {
        return StartMeasurementValue;
    }

    public void setStartMeasurementValue(double startMeasurementValue) {
        StartMeasurementValue = startMeasurementValue;
    }

    public double getEndMeasurementValue() {
        return EndMeasurementValue;
    }

    public void setEndMeasurementValue(double endMeasurementValue) {
        EndMeasurementValue = endMeasurementValue;
    }

    public double getValue() {
        return Value;
    }

    public void setValue(double value) {
        Value = value;
    }

    public double getLengthInAndroid() {
        return LengthInAndroid;
    }

    public void setLengthInAndroid(double lengthInAndroid) {
        LengthInAndroid = lengthInAndroid;
    }

    public double getLengthInIos() {
        return LengthInIos;
    }

    public void setLengthInIos(double lengthInIos) {
        LengthInIos = lengthInIos;
    }

    public String getMeasurementValue() {
        return MeasurementValue == null ? "" : MeasurementValue;
    }

    public void setMeasurementValue(String measurementValue) {
        MeasurementValue = measurementValue;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTextInEnglish() {
        return TextInEnglish == null ? "" : TextInEnglish;
    }

    public void setTextInEnglish(String textInEnglish) {
        TextInEnglish = textInEnglish;
    }

    public String getTextInArabic() {
        return TextInArabic == null ? "" : TextInArabic;
    }

    public void setTextInArabic(String textInArabic) {
        TextInArabic = textInArabic;
    }

    public String getReferenceNumber() {
        return ReferenceNumber == null ? "" : ReferenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        ReferenceNumber = referenceNumber;
    }

    public int getGenderMeasurementId() {
        return GenderMeasurementId;
    }

    public void setGenderMeasurementId(int genderMeasurementId) {
        GenderMeasurementId = genderMeasurementId;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

}
