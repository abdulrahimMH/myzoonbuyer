package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class OrderDetailEntity implements Serializable {
    public  int OrderId;
    public String OrderDt;
    public String Product_Name;
    public String NameInArabic;
    public String Image;
    public int qty;
    public int Amount;
    public String ServiceType;
    public String ServiceTypeInArabic;
    public String MaterialType;
    public String MaterialTypeInArabic;
    public String DressTypeName;
    public int MeasurementType;
    public int CustomerId;
    public int DressType;
    public int DressTypeId;
    public int Gender;
    public double Balance;
    public String PaymentStatus;
    public int PatternId;
    public String MaterialTypeId;
    public String MeasurementInEnglish;
    public String MeasurementInArabic;
    public int Tailorid;
    public int DeliveryTypeId;
    public boolean IsSchedule;
    public String DeliveryNameInEnglish;
    public String DeliveryNameInArabic;
    public int DeliveryId;

    public boolean isSchedule() {
        return IsSchedule;
    }

    public void setSchedule(boolean schedule) {
        IsSchedule = schedule;
    }

    public String getDeliveryNameInEnglish() {
        return DeliveryNameInEnglish == null ? "" : DeliveryNameInEnglish;
    }

    public void setDeliveryNameInEnglish(String deliveryNameInEnglish) {
        DeliveryNameInEnglish = deliveryNameInEnglish;
    }

    public String getDeliveryNameInArabic() {
        return DeliveryNameInArabic == null ? "" : DeliveryNameInArabic;
    }

    public void setDeliveryNameInArabic(String deliveryNameInArabic) {
        DeliveryNameInArabic = deliveryNameInArabic;
    }

    public int getDeliveryId() {
        return DeliveryId;
    }

    public void setDeliveryId(int deliveryId) {
        DeliveryId = deliveryId;
    }


    public int getDeliveryTypeId() {
        return DeliveryTypeId;
    }

    public void setDeliveryTypeId(int deliveryTypeId) {
        DeliveryTypeId = deliveryTypeId;
    }

    public int getDressTypeId() {
        return DressTypeId;
    }

    public void setDressTypeId(int dressTypeId) {
        DressTypeId = dressTypeId;
    }

    public String getMaterialTypeId() {
        return MaterialTypeId == null ? "" : MaterialTypeId;
    }

    public void setMaterialTypeId(String materialTypeId) {
        MaterialTypeId = materialTypeId;
    }

    public String getMeasurementInEnglish() {
        return MeasurementInEnglish == null ? "" : MeasurementInEnglish;
    }

    public void setMeasurementInEnglish(String measurementInEnglish) {
        MeasurementInEnglish = measurementInEnglish;
    }

    public String getMeasurementInArabic() {
        return MeasurementInArabic == null ? "" : MeasurementInArabic;
    }

    public void setMeasurementInArabic(String measurementInArabic) {
        MeasurementInArabic = measurementInArabic;
    }

    public int getTailorid() {
        return Tailorid;
    }

    public void setTailorid(int tailorid) {
        Tailorid = tailorid;
    }
    public String getMaterialType() {
        return MaterialType == null ? "" : MaterialType;
    }

    public void setMaterialType(String materialType) {
        MaterialType = materialType;
    }

    public String getMaterialTypeInArabic() {
        return MaterialTypeInArabic == null ? "" : MaterialTypeInArabic;
    }

    public void setMaterialTypeInArabic(String materialTypeInArabic) {
        MaterialTypeInArabic = materialTypeInArabic;
    }

    public String getDressTypeName() {
        return DressTypeName == null ? "" : DressTypeName;
    }

    public void setDressTypeName(String dressTypeName) {
        DressTypeName = dressTypeName;
    }

    public int getMeasurementType() {
        return MeasurementType;
    }

    public void setMeasurementType(int measurementType) {
        MeasurementType = measurementType;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public int getDressType() {
        return DressType;
    }

    public void setDressType(int dressType) {
        DressType = dressType;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public double getBalance() {
        return Balance;
    }

    public void setBalance(double balance) {
        Balance = balance;
    }

    public String getPaymentStatus() {
        return PaymentStatus == null ? "" : PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public int getPatternId() {
        return PatternId;
    }

    public void setPatternId(int patternId) {
        PatternId = patternId;
    }

    public String getServiceTypeInArabic() {
        return ServiceTypeInArabic == null ? "" : ServiceTypeInArabic;
    }

    public void setServiceTypeInArabic(String serviceTypeInArabic) {
        ServiceTypeInArabic = serviceTypeInArabic;
    }


    public int getAmount() {
        return Amount;
    }

    public void setAmount(int amount) {
        Amount = amount;
    }

    public String getServiceType() {
        return ServiceType == null ? "" : ServiceType;
    }

    public void setServiceType(String serviceType) {
        ServiceType = serviceType;
    }


    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

    public String getOrderDt() {
        return OrderDt == null ? "" : OrderDt;
    }

    public void setOrderDt(String orderDt) {
        OrderDt = orderDt;
    }

    public String getProduct_Name() {
        return Product_Name == null ? "" : Product_Name;
    }

    public void setProduct_Name(String product_Name) {
        Product_Name = product_Name;
    }

    public String getNameInArabic() {
        return NameInArabic == null ? "" : NameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        NameInArabic = nameInArabic;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }


}
