package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetThicknessEntity implements Serializable {
    public int Id;
    public String Thickness;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getThickness() {
        return Thickness == null ? "" : Thickness;
    }

    public void setThickness(String thickness) {
        Thickness = thickness;
    }

}
