package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetShowBuyerAddressEntity implements Serializable {
    public double Lattitude;
     public double Longitude;
     public String FirstName;
     public String LastName;
     public String Building;
     public String Floor;
     public String LandMark;
     public String Area;
     public String StateName;
    public String Name;

    public double getLattitude() {
        return Lattitude;
    }

    public void setLattitude(double lattitude) {
        Lattitude = lattitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public String getFirstName() {
        return FirstName == null ? "" : FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName == null ? "" : LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getBuilding() {
        return Building == null ? "" : Building;
    }

    public void setBuilding(String building) {
        Building = building;
    }

    public String getFloor() {
        return Floor == null ? "" : Floor;
    }

    public void setFloor(String floor) {
        Floor = floor;
    }

    public String getLandMark() {
        return LandMark == null ? "" : LandMark;
    }

    public void setLandMark(String landMark) {
        LandMark = landMark;
    }

    public String getArea() {
        return Area == null ? "" : Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getStateName() {
        return StateName == null ? "" : StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public String getName() {
        return Name == null ? "" : Name;
    }

    public void setName(String name) {
        Name = name;
    }

}
