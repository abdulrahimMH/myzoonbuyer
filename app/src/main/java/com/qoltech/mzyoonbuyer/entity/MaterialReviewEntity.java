package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class MaterialReviewEntity implements Serializable {
    public String getReview() {
        return review == null ? "" : review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String review;
}
