package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class TrackingFieldEntity implements Serializable {
    public int Id;
    public String Status;
    public String StatusInEnglish;
    public String StatusInArabic;
    public String Date;

    public String getStatusInEnglish() {
        return StatusInEnglish == null ? "" : StatusInEnglish;
    }

    public void setStatusInEnglish(String statusInEnglish) {
        StatusInEnglish = statusInEnglish;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getStatus() {
        return Status == null ? "" : Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getStatusInArabic() {
        return StatusInArabic == null ? "" : StatusInArabic;
    }

    public void setStatusInArabic(String statusInArabic) {
        StatusInArabic = statusInArabic;
    }

    public String getDate() {
        return Date == null ? "" : Date;
    }

    public void setDate(String date) {
        Date = date;
    }

}
