package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class ViewDetailsEntity implements Serializable {
public ArrayList<GetpattternByIdEntity> GetpattternById;
    public ArrayList<GetColorByIdEntity> GetColorById;
    public ArrayList<PatternImgEntity> PatternImg;
    public ArrayList<ViewRatingEntity> ReviewCount;
    public ArrayList<AverageRatingEntity> AverageRating;
    public ArrayList<GetThicknessEntity> GetThickness;

    public ArrayList<ViewRatingEntity> getReviewCount() {
        return ReviewCount == null ? new ArrayList<>() : ReviewCount;
    }

    public void setReviewCount(ArrayList<ViewRatingEntity> reviewCount) {
        ReviewCount = reviewCount;
    }

    public ArrayList<AverageRatingEntity> getAverageRating() {
        return AverageRating == null ? new ArrayList<>() : AverageRating;
    }

    public void setAverageRating(ArrayList<AverageRatingEntity> averageRating) {
        AverageRating = averageRating;
    }

    public ArrayList<GetThicknessEntity> getGetThickness() {
        return GetThickness == null ? new ArrayList<>() : GetThickness;
    }

    public void setGetThickness(ArrayList<GetThicknessEntity> getThickness) {
        GetThickness = getThickness;
    }

    public ArrayList<PatternImgEntity> getPatternImg() {
        return PatternImg == null ? new ArrayList<>() : PatternImg;
    }

    public void setPatternImg(ArrayList<PatternImgEntity> patternImg) {
        PatternImg = patternImg;
    }

    public ArrayList<GetpattternByIdEntity> getGetpattternById() {
        return GetpattternById == null ? new ArrayList<>() : GetpattternById;
    }

    public void setGetpattternById(ArrayList<GetpattternByIdEntity> getpattternById) {
        GetpattternById = getpattternById;
    }

    public ArrayList<GetColorByIdEntity> getGetColorById() {
        return GetColorById == null ? new ArrayList<>() : GetColorById;
    }

    public void setGetColorById(ArrayList<GetColorByIdEntity> getColorById) {
        GetColorById = getColorById;
    }

}
