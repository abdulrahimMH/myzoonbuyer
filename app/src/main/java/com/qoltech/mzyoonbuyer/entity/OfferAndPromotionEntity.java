package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class OfferAndPromotionEntity implements Serializable {
    public ArrayList<GetAllCategoriesEntity> GetAllCategories;
    public ArrayList<OfferAndPromotionGetSliderImageEntity> GetSliderImage;
    public ArrayList<OfferAndPromotionGetadvertisementEntity> Getadvertisement;

    public ArrayList<GetAllCategoriesEntity> getGetAllCategories() {
        return GetAllCategories == null ? new ArrayList<>() : GetAllCategories;
    }

    public void setGetAllCategories(ArrayList<GetAllCategoriesEntity> getAllCategories) {
        GetAllCategories = getAllCategories;
    }

    public ArrayList<OfferAndPromotionGetSliderImageEntity> getGetSliderImage() {
        return GetSliderImage == null ? new ArrayList<>() : GetSliderImage;
    }

    public void setGetSliderImage(ArrayList<OfferAndPromotionGetSliderImageEntity> getSliderImage) {
        GetSliderImage = getSliderImage;
    }

    public ArrayList<OfferAndPromotionGetadvertisementEntity> getGetadvertisement() {
        return Getadvertisement == null ? new ArrayList<>() : Getadvertisement;
    }

    public void setGetadvertisement(ArrayList<OfferAndPromotionGetadvertisementEntity> getadvertisement) {
        Getadvertisement = getadvertisement;
    }

}
