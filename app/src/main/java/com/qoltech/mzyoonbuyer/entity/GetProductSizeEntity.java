package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetProductSizeEntity implements Serializable {
    public int Id;
    public String Size;
    public boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getSize() {
        return Size == null ? "" : Size;
    }

    public void setSize(String size) {
        Size = size;
    }


}
