package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class WebOrderEntity implements Serializable {
    public String Email;
    public int OrderId;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

}
