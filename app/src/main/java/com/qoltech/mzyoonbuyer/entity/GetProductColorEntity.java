package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetProductColorEntity implements Serializable {
    public int Id;
    public String colorCode;
    public boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getColorCode() {
        return colorCode == null ? "" : colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }


}
