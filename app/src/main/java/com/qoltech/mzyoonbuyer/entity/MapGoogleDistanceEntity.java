package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class MapGoogleDistanceEntity implements Serializable {

    public DistanceEntity getDistance() {
        return distance ==  null ? new DistanceEntity() : distance;
    }

    public void setDistance(DistanceEntity distance) {
        this.distance = distance;
    }

    public DistanceEntity distance;

}
