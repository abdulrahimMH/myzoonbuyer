package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetMaterialCompaniReferenceImgEntity implements Serializable {

    public int Id;
    public String Image;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

}
