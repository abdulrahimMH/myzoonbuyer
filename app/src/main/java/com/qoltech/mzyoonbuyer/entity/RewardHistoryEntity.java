package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class RewardHistoryEntity implements Serializable {

    public int TotalPoints;
      public int ReedemedPoints;
      public int TotalEarnedPoints;
      public double Points;
      public int Balance;
      public String ReferenceId;
      public String Type;
      public String TransactionDate;
      public String TransactionType;
    public String RewardPointType;
    public String TransactiontypeInArabic;
    public String RewardPointTypeInArabic;
    public boolean checked;
    public int PointType;

    public int getPointType() {
        return PointType;
    }

    public void setPointType(int pointType) {
        PointType = pointType;
    }


    public String getTransactiontypeInArabic() {
        return TransactiontypeInArabic == null ? "" : TransactiontypeInArabic;
    }

    public void setTransactiontypeInArabic(String transactiontypeInArabic) {
        TransactiontypeInArabic = transactiontypeInArabic;
    }

    public String getRewardPointTypeInArabic() {
        return RewardPointTypeInArabic == null ? "" : RewardPointTypeInArabic;
    }

    public void setRewardPointTypeInArabic(String rewardPointTypeInArabic) {
        RewardPointTypeInArabic = rewardPointTypeInArabic;
    }



    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }



    public int getTotalPoints() {
        return TotalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        TotalPoints = totalPoints;
    }

    public int getReedemedPoints() {
        return ReedemedPoints;
    }

    public void setReedemedPoints(int reedemedPoints) {
        ReedemedPoints = reedemedPoints;
    }

    public int getTotalEarnedPoints() {
        return TotalEarnedPoints;
    }

    public void setTotalEarnedPoints(int totalEarnedPoints) {
        TotalEarnedPoints = totalEarnedPoints;
    }

    public double getPoints() {
        return Points;
    }

    public void setPoints(double points) {
        Points = points;
    }

    public int getBalance() {
        return Balance;
    }

    public void setBalance(int balance) {
        Balance = balance;
    }

    public String getReferenceId() {
        return ReferenceId == null ? "" : ReferenceId;
    }

    public void setReferenceId(String referenceId) {
        ReferenceId = referenceId;
    }

    public String getType() {
        return Type == null ? "" : Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getTransactionDate() {
        return TransactionDate == null ? "" : TransactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        TransactionDate = transactionDate;
    }

    public String getTransactionType() {
        return TransactionType == null ? "" : TransactionType;
    }

    public void setTransactionType(String transactionType) {
        TransactionType = transactionType;
    }

    public String getRewardPointType() {
        return RewardPointType == null ? "" : RewardPointType;
    }

    public void setRewardPointType(String rewardPointType) {
        RewardPointType = rewardPointType;
    }


}
