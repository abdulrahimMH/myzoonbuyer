package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetPaymentStatusEntity implements Serializable {

    public long Transactionid;
    public String Amount;
    public String cardlast4;
    public String PaidOn;

    public long getTransactionid() {
        return Transactionid;
    }

    public void setTransactionid(long transactionid) {
        Transactionid = transactionid;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getCardlast4() {
        return cardlast4 == null ? "" : cardlast4;
    }

    public void setCardlast4(String cardlast4) {
        this.cardlast4 = cardlast4;
    }

    public String getPaidOn() {
        return PaidOn;
    }

    public void setPaidOn(String paidOn) {
        PaidOn = paidOn == null ? "" : paidOn;
    }

}
