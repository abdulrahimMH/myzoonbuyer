package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetGrandTotalEntity implements Serializable {
    public String getGrandTotal() {
        return GrandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        GrandTotal = grandTotal;
    }

    public String GrandTotal;
}
