package com.qoltech.mzyoonbuyer.entity;

public class PaymentGatewayTransactionModal {
    private String trace;

    private PaymentGatewayTransactionEntity auth;

    public String getTrace ()
    {
        return trace;
    }

    public void setTrace (String trace)
    {
        this.trace = trace;
    }

    public PaymentGatewayTransactionEntity getAuth ()
    {
        return auth;
    }

    public void setAuth (PaymentGatewayTransactionEntity auth)
    {
        this.auth = auth;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [trace = "+trace+", auth = "+auth+"]";
    }
}
