package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetCountryEntity implements Serializable {

    public int Id;
    public String CountryName;
    public String PhoneCode;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCountryName() {
        return CountryName == null ? "" : CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getPhoneCode() {
        return PhoneCode == null ? "" : PhoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        PhoneCode = phoneCode;
    }

    public String getFlag() {
        return Flag == null ? "" : Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public String Flag;
}
