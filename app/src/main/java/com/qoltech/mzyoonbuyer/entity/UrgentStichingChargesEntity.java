package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class UrgentStichingChargesEntity implements Serializable {
    public String getUrgentStichingCharges() {
        return UrgentStichingCharges;
    }

    public void setUrgentStichingCharges(String urgentStichingCharges) {
        UrgentStichingCharges = urgentStichingCharges;
    }

    public String UrgentStichingCharges;

}
