package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class BuyerAddressEntity implements Serializable {
    public String FirstName;
    public String Floor;
    public String Area;
    public String Country_Name;
    public String StateName;
    public String PhoneNo;

    public String getFirstName() {
        return FirstName == null ? "" : FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getFloor() {
        return Floor == null ? "" : Floor;
    }

    public void setFloor(String floor) {
        Floor = floor;
    }

    public String getArea() {
        return Area == null ? "" : Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getCountry_Name() {
        return Country_Name == null ? "" : Country_Name;
    }

    public void setCountry_Name(String country_Name) {
        Country_Name = country_Name;
    }

    public String getStateName() {
        return StateName == null ? "" : StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public String getPhoneNo() {
        return PhoneNo == null ? "" : PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

}
