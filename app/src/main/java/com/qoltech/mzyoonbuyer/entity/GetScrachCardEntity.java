package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetScrachCardEntity implements Serializable {

    public int Id;
    public double Points;
    public int OrderId;
    public String OrderDt;
    public boolean Isreedemed;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public double getPoints() {
        return Points;
    }

    public void setPoints(double points) {
        Points = points;
    }

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

    public String getOrderDt() {
        return OrderDt;
    }

    public void setOrderDt(String orderDt) {
        OrderDt = orderDt;
    }

    public boolean isIsreedemed() {
        return Isreedemed;
    }

    public void setIsreedemed(boolean isreedemed) {
        Isreedemed = isreedemed;
    }





}
