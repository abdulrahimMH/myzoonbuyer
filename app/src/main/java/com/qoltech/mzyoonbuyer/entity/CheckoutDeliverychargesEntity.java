package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class CheckoutDeliverychargesEntity implements Serializable {
    public String Charges;
    public double Amount;

    public String getCharges() {
        return Charges == null ? "" : Charges;
    }

    public void setCharges(String charges) {
        Charges = charges;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

}
