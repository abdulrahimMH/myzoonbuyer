package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetAreaEntity implements Serializable {
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public  int Id;
    public String getArea() {
        return Area == null ? "" : Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String Area;
}
