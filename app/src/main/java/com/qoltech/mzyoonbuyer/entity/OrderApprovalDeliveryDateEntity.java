package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class OrderApprovalDeliveryDateEntity implements Serializable {
    public String getDeliveryDate() {
        return DeliveryDate == null ? "" : DeliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        DeliveryDate = deliveryDate;
    }

    public String DeliveryDate;

}
