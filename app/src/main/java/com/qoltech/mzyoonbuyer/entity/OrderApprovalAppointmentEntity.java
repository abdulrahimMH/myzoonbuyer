package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class OrderApprovalAppointmentEntity implements Serializable {
    public String getAppoinment() {
        return Appoinment == null ? "" : Appoinment;
    }

    public void setAppoinment(String appoinment) {
        Appoinment = appoinment;
    }

    public String Appoinment;
}
