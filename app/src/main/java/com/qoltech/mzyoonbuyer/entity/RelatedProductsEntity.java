package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class RelatedProductsEntity implements Serializable {
    public String Id;
    public String ProductName;
    public double Amount;
    public String ProductImage;
    public double Rating;
    public String CartId;
    public String ProductId;
    public int Quantity;
    public double UnitTotal;
    public int AvailibilityCount;
    public double NewPrice;
    public double Discount;
    public int ColorId;
    public int SizeId;
    public String ColorCode;
    public String Size;
    public int SellerId;
    public String ShopNameInEnglish;
    public int BrandId;
    public String Product;
    public String ColorName;
    public String Product_Name;
    public String Image;
    public String OrderType;
    public int StichingOrderReferenceNo;

    public String getOrderType() {
        return OrderType == null ? "" : OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public int getStichingOrderReferenceNo() {
        return StichingOrderReferenceNo;
    }

    public void setStichingOrderReferenceNo(int stichingOrderReferenceNo) {
        StichingOrderReferenceNo = stichingOrderReferenceNo;
    }


    public String getProduct_Name() {
        return Product_Name == null ? "" : Product_Name;
    }

    public void setProduct_Name(String product_Name) {
        Product_Name = product_Name;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }


    public String getColorName() {
        return ColorName == null ? "" : ColorName;
    }

    public void setColorName(String colorName) {
        ColorName = colorName;
    }

    public String getBrandName() {
        return BrandName == null ? "" : BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }

    public String BrandName;

    public String getProduct() {
        return Product == null ? "" : Product;
    }

    public void setProduct(String product) {
        Product = product;
    }


    public int getBrandId() {
        return BrandId;
    }

    public void setBrandId(int brandId) {
        BrandId = brandId;
    }


    public String getId() {
        return Id == null ? "" : Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getProductName() {
        return ProductName == null ? "" : ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public String getProductImage() {
        return ProductImage == null ? "" : ProductImage;
    }

    public void setProductImage(String productImage) {
        ProductImage = productImage;
    }

    public double getRating() {
        return Rating;
    }

    public void setRating(double rating) {
        Rating = rating;
    }

    public String getCartId() {
        return CartId == null ? "" : CartId;
    }

    public void setCartId(String cartId) {
        CartId = cartId;
    }

    public String getProductId() {
        return ProductId == null ? "" : ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public double getUnitTotal() {
        return UnitTotal;
    }

    public void setUnitTotal(double unitTotal) {
        UnitTotal = unitTotal;
    }

    public int getAvailibilityCount() {
        return AvailibilityCount;
    }

    public void setAvailibilityCount(int availibilityCount) {
        AvailibilityCount = availibilityCount;
    }

    public double getNewPrice() {
        return NewPrice;
    }

    public void setNewPrice(double newPrice) {
        NewPrice = newPrice;
    }

    public double getDiscount() {
        return Discount;
    }

    public void setDiscount(double discount) {
        Discount = discount;
    }

    public int getColorId() {
        return ColorId;
    }

    public void setColorId(int colorId) {
        ColorId = colorId;
    }

    public int getSizeId() {
        return SizeId;
    }

    public void setSizeId(int sizeId) {
        SizeId = sizeId;
    }

    public String getColorCode() {
        return ColorCode == null ? "" : ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }

    public String getSize() {
        return Size == null ? "" : Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public int getSellerId() {
        return SellerId;
    }

    public void setSellerId(int sellerId) {
        SellerId = sellerId;
    }

    public String getShopNameInEnglish() {
        return ShopNameInEnglish == null ? "" : ShopNameInEnglish;
    }

    public void setShopNameInEnglish(String shopNameInEnglish) {
        ShopNameInEnglish = shopNameInEnglish;
    }
}
