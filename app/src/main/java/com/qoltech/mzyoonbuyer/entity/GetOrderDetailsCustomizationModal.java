package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetOrderDetailsCustomizationModal implements Serializable {
    public ArrayList<GetOrderDetailsCustomizationEntity> Customization;

    public ArrayList<GetOrderDetailsCustomizationEntity> getCustomization() {
        return Customization == null ? new ArrayList<>() : Customization;
    }

    public void setCustomization(ArrayList<GetOrderDetailsCustomizationEntity> customization) {
        Customization = customization;
    }

}
