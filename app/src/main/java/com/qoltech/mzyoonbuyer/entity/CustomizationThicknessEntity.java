package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class CustomizationThicknessEntity implements Serializable {

    public int Id;
    public String Thickness;
    public boolean checked;
    public String ThicknessInArabic;

    public String getThicknessInArabic() {
        return ThicknessInArabic == null ? "" : ThicknessInArabic;
    }

    public void setThicknessInArabic(String thicknessInArabic) {
        ThicknessInArabic = thicknessInArabic;
    }


    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }



    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getThickness() {
        return Thickness == null ? "" : Thickness;
    }

    public void setThickness(String thickness) {
        Thickness = thickness;
    }
}
