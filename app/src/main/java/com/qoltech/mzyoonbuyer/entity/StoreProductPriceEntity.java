package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class StoreProductPriceEntity implements Serializable {
    public double getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(double productPrice) {
        ProductPrice = productPrice;
    }

    public double ProductPrice;

}
