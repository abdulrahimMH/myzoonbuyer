package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class StoreOtherProductPriceEntity implements Serializable {
    public double OtherProductPrice;
    public double StichingPrice;

    public double getOtherProductPrice() {
        return OtherProductPrice;
    }

    public void setOtherProductPrice(double otherProductPrice) {
        OtherProductPrice = otherProductPrice;
    }

    public double getStichingPrice() {
        return StichingPrice;
    }

    public void setStichingPrice(double stichingPrice) {
        StichingPrice = stichingPrice;
    }
}
