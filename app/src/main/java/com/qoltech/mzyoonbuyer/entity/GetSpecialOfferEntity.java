package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetSpecialOfferEntity implements Serializable {
    private  int Id;
    private String DiscountType;
    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDiscountType() {
        return DiscountType == null ? "" : DiscountType;
    }

    public void setDiscountType(String discountType) {
        DiscountType = discountType;
    }


}
