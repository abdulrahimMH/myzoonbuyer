package com.qoltech.mzyoonbuyer.entity;

public class PayementGatewayModal {
    private PayementGatewayEntity webview;

    private String trace;

    public PayementGatewayEntity getWebview ()
    {
        return webview;
    }

    public void setWebview (PayementGatewayEntity webview)
    {
        this.webview = webview;
    }

    public String getTrace ()
    {
        return trace;
    }

    public void setTrace (String trace)
    {
        this.trace = trace;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [webview = "+webview+", trace = "+trace+"]";
    }
}
