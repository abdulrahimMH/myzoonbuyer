package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetAppointmentLeftMaterialEntity implements Serializable {
    public String HeaderInEnglish;
    public String HeaderInArabic;
    public String OrderDt;
    public int AppointmentLeftMeterialCount;
    public String status;
    public boolean MaterialAppoinmentStatus;
    public String AppointmentTime;

    public String getAppointmentTime() {
        return AppointmentTime == null ? "" : AppointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        AppointmentTime = appointmentTime;
    }


    public String getHeaderInEnglish() {
        return HeaderInEnglish == null ? "" : HeaderInEnglish;
    }

    public void setHeaderInEnglish(String headerInEnglish) {
        HeaderInEnglish = headerInEnglish;
    }

    public String getHeaderInArabic() {
        return HeaderInArabic == null ? "" : HeaderInArabic;
    }

    public void setHeaderInArabic(String headerInArabic) {
        HeaderInArabic = headerInArabic;
    }

    public boolean isMaterialAppoinmentStatus() {
        return MaterialAppoinmentStatus;
    }

    public void setMaterialAppoinmentStatus(boolean materialAppoinmentStatus) {
        MaterialAppoinmentStatus = materialAppoinmentStatus;
    }

    public String getOrderDt() {
        return OrderDt == null ? "" : OrderDt;
    }

    public void setOrderDt(String orderDt) {
        OrderDt = orderDt;
    }

    public int getAppointmentLeftMaterialCount() {
        return AppointmentLeftMeterialCount;
    }

    public void setAppointmentLeftMaterialCount(int appointmentLeftMaterialCount) {
        AppointmentLeftMeterialCount = appointmentLeftMaterialCount;
    }

    public String getStatus() {
        return status == null ? "" : status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
