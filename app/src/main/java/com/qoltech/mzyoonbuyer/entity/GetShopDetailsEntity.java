package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetShopDetailsEntity implements Serializable {
    public String TailorNameInEnglish;
    public String TailorNameInArabic;
    public String ShopNameInEnglish;
    public String ShopNameInArabic;
    public String AddressInEnglish;
    public String AddressinArabic;
    public String WebSite;
    public String StateName;
    public String CountryName;
    public String PhoneNumber;
    public double Latitude;
    public double Longitude;

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public String getTailorNameInEnglish() {
        return TailorNameInEnglish == null ? "" : TailorNameInEnglish;
    }

    public void setTailorNameInEnglish(String tailorNameInEnglish) {
        TailorNameInEnglish = tailorNameInEnglish ;
    }

    public String getTailorNameInArabic() {
        return TailorNameInArabic  == null ? "" : TailorNameInArabic;
    }

    public void setTailorNameInArabic(String tailorNameInArabic) {
        TailorNameInArabic = tailorNameInArabic;
    }

    public String getShopNameInEnglish() {
        return ShopNameInEnglish == null ? "" : ShopNameInEnglish;
    }

    public void setShopNameInEnglish(String shopNameInEnglish) {
        ShopNameInEnglish = shopNameInEnglish ;
    }

    public String getShopNameInArabic() {
        return ShopNameInArabic == null ? "" : ShopNameInArabic;
    }

    public void setShopNameInArabic(String shopNameInArabic) {
        ShopNameInArabic = shopNameInArabic ;
    }

    public String getAddressInEnglish() {
        return AddressInEnglish == null ? "" : AddressInEnglish;
    }

    public void setAddressInEnglish(String addressInEnglish) {
        AddressInEnglish = addressInEnglish;
    }

    public String getAddressinArabic() {
        return AddressinArabic == null ? "" : AddressinArabic;
    }

    public void setAddressinArabic(String addressinArabic) {
        AddressinArabic = addressinArabic;
    }

    public String getWebSite() {
        return WebSite == null ? "" : WebSite;
    }

    public void setWebSite(String webSite) {
        WebSite = webSite;
    }

    public String getStateName() {
        return StateName == null ? "" : StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public String getCountryName() {
        return CountryName == null ? "" : CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getPhoneNumber() {
        return PhoneNumber == null ? "" : PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

}
