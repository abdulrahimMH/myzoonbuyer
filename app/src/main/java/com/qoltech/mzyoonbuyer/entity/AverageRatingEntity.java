package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class AverageRatingEntity implements Serializable {
    public double getAvgRating() {
        return AvgRating;
    }

    public void setAvgRating(double avgRating) {
        AvgRating = avgRating;
    }

    public double AvgRating;
}
