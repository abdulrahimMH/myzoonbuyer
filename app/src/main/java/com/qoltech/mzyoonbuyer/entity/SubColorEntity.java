package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class SubColorEntity implements Serializable {

    public int Id;
       public int ColorId;
    public String SubColor;
    public boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getColorId() {
        return ColorId;
    }

    public void setColorId(int colorId) {
        ColorId = colorId;
    }

    public String getSubColor() {
        return SubColor == null ? "" : SubColor;
    }

    public void setSubColor(String subColor) {
        SubColor = subColor;
    }


}
