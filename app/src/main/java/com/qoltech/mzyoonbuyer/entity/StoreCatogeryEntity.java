package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class StoreCatogeryEntity implements Serializable {
    public int Id;
    public String DisplayId;
    public int ParentId;
    public String Name;
    public boolean HasSubCategory;
    public boolean IsActive;
    public String CreatedBy;
    public String Image;
    public int Position;
    public boolean CheckRightBool;
    public boolean CheckBottomBool;
    public String DisplayIdInArabic;
    public String TitleInArabic;

    public String getDisplayIdInArabic() {
        return DisplayIdInArabic == null ? "" : DisplayIdInArabic;
    }

    public void setDisplayIdInArabic(String displayIdInArabic) {
        DisplayIdInArabic = displayIdInArabic;
    }

    public String getTitleInArabic() {
        return TitleInArabic == null ? "" : TitleInArabic;
    }

    public void setTitleInArabic(String titleInArabic) {
        TitleInArabic = titleInArabic;
    }

    public boolean isCheckRightBool() {
        return CheckRightBool;
    }

    public void setCheckRightBool(boolean checkRightBool) {
        CheckRightBool = checkRightBool;
    }

    public boolean isCheckBottomBool() {
        return CheckBottomBool;
    }

    public void setCheckBottomBool(boolean checkBottomBool) {
        CheckBottomBool = checkBottomBool;
    }


    public int getPosition() {
        return Position;
    }

    public void setPosition(int position) {
        Position = position;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDisplayId() {
        return DisplayId == null ? "" : DisplayId;
    }

    public void setDisplayId(String displayId) {
        DisplayId = displayId;
    }

    public int getParentId() {
        return ParentId;
    }

    public void setParentId(int parentId) {
        ParentId = parentId;
    }

    public String getName() {
        return Name == null ? "" : Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public boolean isHasSubCategory() {
        return HasSubCategory;
    }

    public void setHasSubCategory(boolean hasSubCategory) {
        HasSubCategory = hasSubCategory;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public String getCreatedBy() {
        return CreatedBy == null ? "" : CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

}
