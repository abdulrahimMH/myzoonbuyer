package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class SubTotalEntity implements Serializable {
    public int getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(int subTotal) {
        SubTotal = subTotal;
    }

    public int SubTotal;
}
