package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetpattternByIdEntity implements Serializable {
    public int PatternId;
    public String PatternInEnglish;
    public String PatternInArabic;
    public String Image;
    public String MaterialInEnglish;
    public String MaterialInArabic;
    public String MaterialImage;
    public String BrandInEnglish;
    public String BrandInArabic;
    public String BrandImage;
    public String PlaceInEnglish;
    public String PlaceInArabic;
    public String IndustryImage;

    public int getPatternId() {
        return PatternId;
    }

    public void setPatternId(int patternId) {
        PatternId = patternId;
    }

    public String getPatternInEnglish() {
        return PatternInEnglish == null ? "" : PatternInEnglish;
    }

    public void setPatternInEnglish(String patternInEnglish) {
        PatternInEnglish = patternInEnglish;
    }

    public String getPatternInArabic() {
        return PatternInArabic == null ? "" : PatternInArabic;
    }

    public void setPatternInArabic(String patternInArabic) {
        PatternInArabic = patternInArabic;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getMaterialInEnglish() {
        return MaterialInEnglish == null ? "" : MaterialInEnglish;
    }

    public void setMaterialInEnglish(String materialInEnglish) {
        MaterialInEnglish = materialInEnglish;
    }

    public String getMaterialInArabic() {
        return MaterialInArabic == null ? "" : MaterialInArabic;
    }

    public void setMaterialInArabic(String materialInArabic) {
        MaterialInArabic = materialInArabic;
    }

    public String getMaterialImage() {
        return MaterialImage == null ? "" : MaterialImage;
    }

    public void setMaterialImage(String materialImage) {
        MaterialImage = materialImage;
    }

    public String getBrandInEnglish() {
        return BrandInEnglish == null ? "" : BrandInEnglish;
    }

    public void setBrandInEnglish(String brandInEnglish) {
        BrandInEnglish = brandInEnglish;
    }

    public String getBrandInArabic() {
        return BrandInArabic == null ? "" : BrandInArabic;
    }

    public void setBrandInArabic(String brandInArabic) {
        BrandInArabic = brandInArabic;
    }

    public String getBrandImage() {
        return BrandImage == null ? "" : BrandImage;
    }

    public void setBrandImage(String brandImage) {
        BrandImage = brandImage;
    }

    public String getPlaceInEnglish() {
        return PlaceInEnglish == null ? "" : PlaceInEnglish;
    }

    public void setPlaceInEnglish(String placeInEnglish) {
        PlaceInEnglish = placeInEnglish;
    }

    public String getPlaceInArabic() {
        return PlaceInArabic == null ? "" : PlaceInArabic;
    }

    public void setPlaceInArabic(String placeInArabic) {
        PlaceInArabic = placeInArabic;
    }

    public String getIndustryImage() {
        return IndustryImage == null ? "" : IndustryImage;
    }

    public void setIndustryImage(String industryImage) {
        IndustryImage = industryImage;
    }


}
