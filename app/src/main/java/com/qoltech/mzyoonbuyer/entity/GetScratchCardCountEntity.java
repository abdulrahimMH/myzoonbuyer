package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetScratchCardCountEntity implements Serializable {
    public int getScratchCardCount() {
        return ScratchCardCount;
    }

    public void setScratchCardCount(int scratchCardCount) {
        ScratchCardCount = scratchCardCount;
    }

    public int ScratchCardCount;
}
