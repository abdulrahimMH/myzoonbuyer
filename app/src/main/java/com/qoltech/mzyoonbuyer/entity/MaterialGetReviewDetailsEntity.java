package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class MaterialGetReviewDetailsEntity implements Serializable {
    public String Name;
    public double rating;
    public String review;
    public String ReviewDate;

    public String getName() {
        return Name == null ? "" : Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review == null ? "" : review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getReviewDate() {
        return ReviewDate == null ? "" : ReviewDate;
    }

    public void setReviewDate(String reviewDate) {
        ReviewDate = reviewDate;
    }

}
