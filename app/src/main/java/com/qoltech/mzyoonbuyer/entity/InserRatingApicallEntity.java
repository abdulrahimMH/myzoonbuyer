package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class InserRatingApicallEntity implements Serializable {
    public String Id;
    public String Rating;

    public String getId() {
        return Id == null ? "" : Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getRating() {
        return Rating == null ? "" : Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

}
