package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetOrderDetailsCustomizationEntity implements Serializable {
    public int Id;
    public String CustomizationNameInEnglish;
    public String CustomizationNameInArabic;
    public String Images;
    public boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCustomizationNameInEnglish() {
        return CustomizationNameInEnglish == null ? "" : CustomizationNameInEnglish;
    }

    public void setCustomizationNameInEnglish(String customizationNameInEnglish) {
        CustomizationNameInEnglish = customizationNameInEnglish;
    }

    public String getCustomizationNameInArabic() {
        return CustomizationNameInArabic == null ? "" : CustomizationNameInArabic;
    }

    public void setCustomizationNameInArabic(String customizationNameInArabic) {
        CustomizationNameInArabic = customizationNameInArabic;
    }

    public String getImages() {
        return Images == null ? "" : Images;
    }

    public void setImages(String images) {
        Images = images;
    }

}
