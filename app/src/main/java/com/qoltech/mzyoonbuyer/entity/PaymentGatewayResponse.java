package com.qoltech.mzyoonbuyer.entity;

public class PaymentGatewayResponse {
    private PayementGatewayModal mobile;

    public PayementGatewayModal getMobile() {
        return mobile;
    }

    public void setMobile(PayementGatewayModal mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "ClassPojo [mobile = " + mobile + "]";
    }
}
