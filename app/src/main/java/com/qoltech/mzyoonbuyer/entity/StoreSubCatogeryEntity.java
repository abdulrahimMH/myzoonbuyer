package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class StoreSubCatogeryEntity implements Serializable {

    public int Id;
    public String Title;
    public String CategeoriesBannerImage;
    public String TitleInArabic;

    public String getTitleInArabic() {
        return TitleInArabic == null ? "" : TitleInArabic;
    }

    public void setTitleInArabic(String titleInArabic) {
        TitleInArabic = titleInArabic;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return Title == null ? "" : Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getCategeoriesBannerImage() {
        return CategeoriesBannerImage == null ? "" : CategeoriesBannerImage;
    }

    public void setCategeoriesBannerImage(String categeoriesBannerImage) {
        CategeoriesBannerImage = categeoriesBannerImage;
    }

}
