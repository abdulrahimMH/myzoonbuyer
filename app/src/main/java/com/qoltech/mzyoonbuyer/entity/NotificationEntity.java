package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class NotificationEntity implements Serializable {

    public String Type;
    public int OrderId;
    public String Notification;
    public String Header;
    public String OrderType;
    public String CreatedOn;
    public String NotificationInArabic;
    public String HeaderInArabic;

    public String getNotificationInArabic() {
        return NotificationInArabic == null ? "" : NotificationInArabic;
    }

    public void setNotificationInArabic(String notificationInArabic) {
        NotificationInArabic = notificationInArabic;
    }

    public String getHeaderInArabic() {
        return HeaderInArabic == null ? "" : HeaderInArabic;
    }

    public void setHeaderInArabic(String headerInArabic) {
        HeaderInArabic = headerInArabic;
    }

    public String getType() {
        return Type == null ? "" : Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

    public String getNotification() {
        return Notification == null ? "" : Notification;
    }

    public void setNotification(String notification) {
        Notification = notification;
    }

    public String getHeader() {
        return Header == null ? "" : Header;
    }

    public void setHeader(String header) {
        Header = header;
    }

    public String getOrderType() {
        return OrderType == null ? "" : OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public String getCreatedOn() {
        return CreatedOn == null ? "" : CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

}
