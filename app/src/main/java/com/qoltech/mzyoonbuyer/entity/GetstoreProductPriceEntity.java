package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetstoreProductPriceEntity implements Serializable {
    public String getStoreProductPrice() {
        return storeProductPrice;
    }

    public void setStoreProductPrice(String storeProductPrice) {
        this.storeProductPrice = storeProductPrice;
    }

    public String storeProductPrice;
}
