package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class OfferAndPromotionGetadvertisementEntity implements Serializable {
    public String advertisement;
    public String advertisementInArabic;

    public String getAdvertisementInArabic() {
        return advertisementInArabic == null ? "" : advertisementInArabic;
    }

    public void setAdvertisementInArabic(String advertisementInArabic) {
        this.advertisementInArabic = advertisementInArabic;
    }


    public String getAdvertisement() {
        return advertisement == null ? "" : advertisement;
    }

    public void setAdvertisement(String advertisement) {
        this.advertisement = advertisement;
    }


}
