package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetProductSellerEntity implements Serializable {

    public int SellerId;
    public String ShopNameInEnglish;
    public double Amount;
    public double RatingsCount;
    public double RatingsValue;
    public double NewPrice;
    public int Discount;
    public String DiscountType;
    public boolean IsProduct;
    public boolean checked;
    public String Id;
    public String ShopNameInArabic;
    public String ProductName;
    public String ProductNameInArabic;
    public String BrandName;
    public String BrandNameInArabic;

    public String getBrandNameInArabic() {
        return BrandNameInArabic == null ? "" : BrandNameInArabic;
    }

    public void setBrandNameInArabic(String brandNameInArabic) {
        BrandNameInArabic = brandNameInArabic;
    }

    public String getId() {
        return Id == null ? "" : Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getShopNameInArabic() {
        return ShopNameInArabic == null ? "" : ShopNameInArabic;
    }

    public void setShopNameInArabic(String shopNameInArabic) {
        ShopNameInArabic = shopNameInArabic;
    }

    public String getProductName() {
        return ProductName == null ? "" : ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductNameInArabic() {
        return ProductNameInArabic == null ? "" : ProductNameInArabic;
    }

    public void setProductNameInArabic(String productNameInArabic) {
        ProductNameInArabic = productNameInArabic;
    }

    public String getBrandName() {
        return BrandName == null ? "" : BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public int getSellerId() {
        return SellerId;
    }

    public void setSellerId(int sellerId) {
        SellerId = sellerId;
    }

    public String getShopNameInEnglish() {
        return ShopNameInEnglish == null ? "" : ShopNameInEnglish;
    }

    public void setShopNameInEnglish(String shopNameInEnglish) {
        ShopNameInEnglish = shopNameInEnglish;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public double getRatingsCount() {
        return RatingsCount;
    }

    public void setRatingsCount(double ratingsCount) {
        RatingsCount = ratingsCount;
    }

    public double getRatingsValue() {
        return RatingsValue;
    }

    public void setRatingsValue(double ratingsValue) {
        RatingsValue = ratingsValue;
    }

    public double getNewPrice() {
        return NewPrice;
    }

    public void setNewPrice(double newPrice) {
        NewPrice = newPrice;
    }

    public int getDiscount() {
        return Discount;
    }

    public void setDiscount(int discount) {
        Discount = discount;
    }

    public String getDiscountType() {
        return DiscountType == null ? "" : DiscountType;
    }

    public void setDiscountType(String discountType) {
        DiscountType = discountType;
    }

    public boolean isProduct() {
        return IsProduct;
    }

    public void setProduct(boolean product) {
        IsProduct = product;
    }


}
