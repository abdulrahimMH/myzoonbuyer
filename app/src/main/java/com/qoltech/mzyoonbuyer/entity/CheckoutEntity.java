package com.qoltech.mzyoonbuyer.entity;


import java.io.Serializable;
import java.util.ArrayList;

public class CheckoutEntity implements Serializable {
    public ArrayList<GetDressSubtypeEntity> GetDressSubtype;
    public double Price;
    public ArrayList<StoreCartEntity> GetStoreOrderDetails;
    public ArrayList<CheckOutGetStoreAmountEntity> GetStoreOrderAmount;
    public double DeliveryCharges;
    public double GrandTotal;

    public double getGrandTotal() {
        return GrandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        GrandTotal = grandTotal;
    }


    public double getDeliveryCharges() {
        return DeliveryCharges;
    }

    public void setDeliveryCharges(double deliveryCharges) {
        DeliveryCharges = deliveryCharges;
    }


    public ArrayList<StoreCartEntity> getGetStoreOrderDetails() {
        return GetStoreOrderDetails;
    }

    public void setGetStoreOrderDetails(ArrayList<StoreCartEntity> getStoreOrderDetails) {
        GetStoreOrderDetails = getStoreOrderDetails;
    }

    public ArrayList<CheckOutGetStoreAmountEntity> getGetStoreOrderAmount() {
        return GetStoreOrderAmount;
    }

    public void setGetStoreOrderAmount(ArrayList<CheckOutGetStoreAmountEntity> getStoreOrderAmount) {
        GetStoreOrderAmount = getStoreOrderAmount;
    }


    public ArrayList<GetDressSubtypeEntity> getGetDressSubtype() {
        return GetDressSubtype == null ? new ArrayList<>() : GetDressSubtype;
    }

    public void setGetDressSubtype(ArrayList<GetDressSubtypeEntity> getDressSubtype) {
        GetDressSubtype = getDressSubtype;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }


}
