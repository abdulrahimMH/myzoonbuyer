package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetRewardPointsCountEntity  implements Serializable {
    public int getRewardpoints() {
        return Rewardpoints;
    }

    public void setRewardpoints(int rewardpoints) {
        Rewardpoints = rewardpoints;
    }

    public int Rewardpoints;

}
