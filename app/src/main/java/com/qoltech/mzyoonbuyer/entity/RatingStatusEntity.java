package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class RatingStatusEntity implements Serializable {
    public double FullStatus;
    public int Based_On_Reviews;
    public String ReviewCount;

    public String getReviewCount() {
        return ReviewCount == null ? "" : ReviewCount;
    }

    public void setReviewCount(String reviewCount) {
        ReviewCount = reviewCount;
    }


    public double getFullStatus() {
        return FullStatus;
    }

    public void setFullStatus(double fullStatus) {
        FullStatus = fullStatus;
    }

    public int getBased_On_Reviews() {
        return Based_On_Reviews;
    }

    public void setBased_On_Reviews(int based_On_Reviews) {
        Based_On_Reviews = based_On_Reviews;
    }

}
