package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class MeasurementChargesEntity implements Serializable {
    public String getMeasurementCharges() {
        return MeasurementCharges;
    }

    public void setMeasurementCharges(String measurementCharges) {
        MeasurementCharges = measurementCharges;
    }

    public String MeasurementCharges;
}
