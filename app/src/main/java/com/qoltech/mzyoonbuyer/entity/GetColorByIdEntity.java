package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetColorByIdEntity implements Serializable {
    public int PatternId;
    public int ColorId;
    public String ColorInEnglish;
    public String ColorInArabic;
    public String ColorImage;

    public int getPatternId() {
        return PatternId;
    }

    public void setPatternId(int patternId) {
        PatternId = patternId;
    }

    public int getColorId() {
        return ColorId;
    }

    public void setColorId(int colorId) {
        ColorId = colorId;
    }

    public String getColorInEnglish() {
        return ColorInEnglish == null ? "" : ColorInEnglish;
    }

    public void setColorInEnglish(String colorInEnglish) {
        ColorInEnglish = colorInEnglish;
    }

    public String getColorInArabic() {
        return ColorInArabic == null ? "" : ColorInArabic;
    }

    public void setColorInArabic(String colorInArabic) {
        ColorInArabic = colorInArabic;
    }

    public String getColorImage() {
        return ColorImage == null ? "" : ColorImage;
    }

    public void setColorImage(String colorImage) {
        ColorImage = colorImage;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public boolean IsActive;
}
