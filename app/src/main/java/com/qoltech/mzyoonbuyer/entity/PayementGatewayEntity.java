package com.qoltech.mzyoonbuyer.entity;

public class PayementGatewayEntity {
    private String code;

    private String abort;

    private String start;

    private String close;

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getAbort ()
    {
        return abort;
    }

    public void setAbort (String abort)
    {
        this.abort = abort;
    }

    public String getStart ()
    {
        return start;
    }

    public void setStart (String start)
    {
        this.start = start;
    }

    public String getClose ()
    {
        return close;
    }

    public void setClose (String close)
    {
        this.close = close;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [code = "+code+", abort = "+abort+", start = "+start+", close = "+close+"]";
    }
}
