package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetRedeemEntity implements Serializable {
    public double Points;
    public double Amount;
    public int PointPercentage;

    public int getPointPercentage() {
        return PointPercentage;
    }

    public void setPointPercentage(int pointPercentage) {
        PointPercentage = pointPercentage;
    }

    public double getPoints() {
        return Points;
    }

    public void setPoints(double points) {
        Points = points;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }


}
