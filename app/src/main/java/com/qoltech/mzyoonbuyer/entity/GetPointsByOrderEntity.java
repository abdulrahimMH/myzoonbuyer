package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetPointsByOrderEntity implements Serializable {
    public String ScratchCard;
    public String OrderDt;

    public String getScratchCard() {
        return ScratchCard == null ? "" : ScratchCard;
    }
    public void setScratchCard(String scratchCard) {
        ScratchCard = scratchCard;
    }

    public String getOrderDt() {
        return OrderDt == null ? "" : OrderDt;
    }

    public void setOrderDt(String orderDt) {
        OrderDt = orderDt;
    }

}
