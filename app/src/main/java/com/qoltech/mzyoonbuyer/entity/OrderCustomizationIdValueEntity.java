package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class OrderCustomizationIdValueEntity implements Serializable {
    public String CustomizationAttributeId;
    public String AttributeImageId;

    public String getCustomizationAttributeId() {
        return CustomizationAttributeId == null ? "" : CustomizationAttributeId;
    }

    public void setCustomizationAttributeId(String customizationAttributeId) {
        CustomizationAttributeId = customizationAttributeId;
    }

    public String getAttributeImageId() {
        return AttributeImageId == null ? "" : AttributeImageId;
    }

    public void setAttributeImageId(String attributeImageId) {
        AttributeImageId = attributeImageId;
    }

}
