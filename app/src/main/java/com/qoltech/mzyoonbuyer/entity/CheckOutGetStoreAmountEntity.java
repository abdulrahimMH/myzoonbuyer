package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class CheckOutGetStoreAmountEntity implements Serializable {
    public double StoreOrderAmount;

    public double getStoreOrderAmount() {
        return StoreOrderAmount;
    }

    public void setStoreOrderAmount(double storeOrderAmount) {
        StoreOrderAmount = storeOrderAmount;
    }

}
