package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class ProductPriceEntity implements Serializable {
    public int Price;
    public int Appoinment;
    public String Tax;
    public int Total;
    public int balance;

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public int getAppoinment() {
        return Appoinment;
    }

    public void setAppoinment(int appoinment) {
        Appoinment = appoinment;
    }

    public String getTax() {
        return Tax == null ? "" : Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int total) {
        Total = total;
    }

}
