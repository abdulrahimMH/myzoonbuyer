package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class CustomizationPatternsEntity implements Serializable {

    public int Id;
    public String PatternInEnglish;
    public String PatternInArabic;
    public String Image;
    private boolean isChecked;

    public boolean getChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }



    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getPatternInEnglish() {
        return PatternInEnglish == null ? "" : PatternInEnglish;
    }

    public void setPatternInEnglish(String patternInEnglish) {
        PatternInEnglish = patternInEnglish;
    }

    public String getPatternInArabic() {
        return PatternInArabic == null ? "" : PatternInArabic;
    }

    public void setPatternInArabic(String patternInArabic) {
        PatternInArabic = patternInArabic;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

}
