package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class DashboardCategoriesEntity implements Serializable {
    public int Id;
    public String DisplayId;
    public String CategoriesImage;
    public String DisplayIdInArabic;

    public String getDisplayIdInArabic() {
        return DisplayIdInArabic == null ? "" : DisplayIdInArabic;
    }

    public void setDisplayIdInArabic(String displayIdInArabic) {
        DisplayIdInArabic = displayIdInArabic;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDisplayId() {
        return DisplayId;
    }

    public void setDisplayId(String displayId) {
        DisplayId = displayId;
    }

    public String getCategoriesImage() {
        return CategoriesImage == null ? "" : CategoriesImage;
    }

    public void setCategoriesImage(String categoriesImage) {
        CategoriesImage = categoriesImage;
    }

}
