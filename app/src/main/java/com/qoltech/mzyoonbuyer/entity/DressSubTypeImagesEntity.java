package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class DressSubTypeImagesEntity implements Serializable {
    public int DressSubtypeId;
    public String Images;
    public boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }


    public int getDressSubtypeId() {
        return DressSubtypeId;
    }

    public void setDressSubtypeId(int dressSubtypeId) {
        DressSubtypeId = dressSubtypeId;
    }

    public String getImages() {
        return Images == null ? "" : Images;
    }

    public void setImages(String images) {
        Images = images;
    }

}
