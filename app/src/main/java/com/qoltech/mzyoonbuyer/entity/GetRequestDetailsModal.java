package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetRequestDetailsModal implements Serializable {
    public ArrayList<GetRequestDetailsEntity> OrderDetail;
    public ArrayList<GetMaterialCompaniReferenceImgEntity> MaterialImage;
    public ArrayList<GetMaterialCompaniReferenceImgEntity> AdditionalMaterialImage;
    public ArrayList<GetMaterialCompaniReferenceImgEntity> ReferenceImage;
    public ArrayList<BuyerAddressEntity> BuyerAddress;

    public ArrayList<GetRequestDetailsEntity> getOrderDetail() {
        return OrderDetail == null ? new ArrayList<>() : OrderDetail;
    }

    public void setOrderDetail(ArrayList<GetRequestDetailsEntity> orderDetail) {
        OrderDetail = orderDetail;
    }

    public ArrayList<GetMaterialCompaniReferenceImgEntity> getMaterialImage() {
        return MaterialImage == null ? new ArrayList<>() : MaterialImage;
    }

    public void setMaterialImage(ArrayList<GetMaterialCompaniReferenceImgEntity> materialImage) {
        MaterialImage = materialImage;
    }

    public ArrayList<GetMaterialCompaniReferenceImgEntity> getAdditionalMaterialImage() {
        return AdditionalMaterialImage == null ? new ArrayList<>() : AdditionalMaterialImage;
    }

    public void setAdditionalMaterialImage(ArrayList<GetMaterialCompaniReferenceImgEntity> additionalMaterialImage) {
        AdditionalMaterialImage = additionalMaterialImage;
    }

    public ArrayList<GetMaterialCompaniReferenceImgEntity> getReferenceImage() {
        return ReferenceImage == null ? new ArrayList<>() : ReferenceImage;
    }

    public void setReferenceImage(ArrayList<GetMaterialCompaniReferenceImgEntity> referenceImage) {
        ReferenceImage = referenceImage;
    }

    public ArrayList<BuyerAddressEntity> getBuyerAddress() {
        return BuyerAddress == null ? new ArrayList<>() : BuyerAddress;
    }

    public void setBuyerAddress(ArrayList<BuyerAddressEntity> buyerAddress) {
        BuyerAddress = buyerAddress;
    }


}
