package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetAllCategoriesEntity implements Serializable {
    public int Id;
    public String CategoriesNameInEnglis;
    public String CategoriesNameInArabic;
    public String Image;
    public boolean IsChecked;

    public boolean isChecked() {
        return IsChecked;
    }

    public void setChecked(boolean checked) {
        IsChecked = checked;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCategoriesNameInEnglis() {
        return CategoriesNameInEnglis == null ? "" : CategoriesNameInEnglis;
    }

    public void setCategoriesNameInEnglis(String categoriesNameInEnglis) {
        CategoriesNameInEnglis = categoriesNameInEnglis;
    }

    public String getCategoriesNameInArabic() {
        return CategoriesNameInArabic == null ? "" : CategoriesNameInArabic;
    }

    public void setCategoriesNameInArabic(String categoriesNameInArabic) {
        CategoriesNameInArabic = categoriesNameInArabic;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

}
