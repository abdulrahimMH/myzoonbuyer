package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetStoreTrackingDetailsEntity implements Serializable {
    public String Status;
    public String date;

    public String getStatus() {
        return Status == null ? "" : Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDate() {
        return date == null ? "" : date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
