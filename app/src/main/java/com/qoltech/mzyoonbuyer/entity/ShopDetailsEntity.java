package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class ShopDetailsEntity implements Serializable {
public ArrayList<GetShopDetailsEntity> GetShopDetails;
public ArrayList<TailorShopTimeEntity> TailorShopTime;
public ArrayList<OrderCountEntity> OrderCount;
public ArrayList<ReviewCountEntity> ReviewCount;
public ArrayList<ShopImagesEntity> ShopImages;
    public ArrayList<RatingEntity> Rating;

    public ArrayList<RatingEntity> getRating() {
        return Rating == null ? new ArrayList<>() : Rating;
    }

    public void setRating(ArrayList<RatingEntity> rating) {
        Rating = rating;
    }


    public ArrayList<GetShopDetailsEntity> getGetShopDetails() {
        return GetShopDetails == null ? new ArrayList<>() : GetShopDetails;
    }

    public void setGetShopDetails(ArrayList<GetShopDetailsEntity> getShopDetails) {
        GetShopDetails = getShopDetails;
    }

    public ArrayList<TailorShopTimeEntity> getTailorShopTime() {
        return TailorShopTime == null ? new ArrayList<>() : TailorShopTime;
    }

    public void setTailorShopTime(ArrayList<TailorShopTimeEntity> tailorShopTime) {
        TailorShopTime = tailorShopTime;
    }

    public ArrayList<OrderCountEntity> getOrderCount() {
        return OrderCount == null ? new ArrayList<>() : OrderCount;
    }

    public void setOrderCount(ArrayList<OrderCountEntity> orderCount) {
        OrderCount = orderCount;
    }

    public ArrayList<ReviewCountEntity> getReviewCount() {
        return ReviewCount == null ? new ArrayList<>() : ReviewCount;
    }

    public void setReviewCount(ArrayList<ReviewCountEntity> reviewCount) {
        ReviewCount = reviewCount;
    }

    public ArrayList<ShopImagesEntity> getShopImages() {
        return ShopImages == null ? new ArrayList<>() : ShopImages;
    }

    public void setShopImages(ArrayList<ShopImagesEntity> shopImages) {
        ShopImages = shopImages;
    }


}
