package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetRewardHomeResponseEntity implements Serializable {
    public ArrayList<GetRewardAndOfferEntity> GetRewardsAndOffer;
    public ArrayList<GetRewardPointsCountEntity> GetRewardpoints;
    public ArrayList<GetScratchCardCountEntity> GetScratchCardCount;

    public ArrayList<GetRewardAndOfferEntity> getGetRewardsAndOffer() {
        return GetRewardsAndOffer == null ? new ArrayList<>() : GetRewardsAndOffer;
    }

    public void setGetRewardsAndOffer(ArrayList<GetRewardAndOfferEntity> getRewardsAndOffer) {
        GetRewardsAndOffer = getRewardsAndOffer;
    }

    public ArrayList<GetRewardPointsCountEntity> getGetRewardpoints() {
        return GetRewardpoints == null ? new ArrayList<>() : GetRewardpoints;
    }

    public void setGetRewardpoints(ArrayList<GetRewardPointsCountEntity> getRewardpoints) {
        GetRewardpoints = getRewardpoints;
    }

    public ArrayList<GetScratchCardCountEntity> getGetScratchCardCount() {
        return GetScratchCardCount == null ? new ArrayList<>() : GetScratchCardCount;
    }

    public void setGetScratchCardCount(ArrayList<GetScratchCardCountEntity> getScratchCardCount) {
        GetScratchCardCount = getScratchCardCount;
    }
}
