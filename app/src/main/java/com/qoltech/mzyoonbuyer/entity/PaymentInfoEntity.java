package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class PaymentInfoEntity implements Serializable {
    public  String email;
    public  String lineOne;
    public  String lineTwo;
    public  String lineThree;
    public  String region;
    public  String city;
    public  String country;
    public String FirstName;
    public String SecondName;
    public  String zip;

    public String getFirstName() {
        return FirstName == null ? "" : FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getSecondName() {
        return SecondName == null ? "" : SecondName;
    }

    public void setSecondName(String secondName) {
        SecondName = secondName;
    }

    public String getEmail() {
        return email == null ? "" : email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLineOne() {
        return lineOne == null ? "" : lineOne;
    }

    public void setLineOne(String lineOne) {
        this.lineOne = lineOne;
    }

    public String getLineTwo() {
        return lineTwo == null ? "" : lineTwo;
    }

    public void setLineTwo(String lineTwo) {
        this.lineTwo = lineTwo;
    }

    public String getLineThree() {
        return lineThree == null ? "" : lineThree;
    }

    public void setLineThree(String lineThree) {
        this.lineThree = lineThree;
    }

    public String getRegion() {
        return region == null ? "" : region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city == null ? "" : city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country == null ? "" : country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip() {
        return zip == null ? "" : zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }


}
