package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class ShippingChargesEntity implements Serializable {
    public String getShipping_Charges() {
        return Shipping_Charges == null ? "" : Shipping_Charges;
    }

    public void setShipping_Charges(String shipping_Charges) {
        Shipping_Charges = shipping_Charges;
    }

    public String Shipping_Charges;
}
