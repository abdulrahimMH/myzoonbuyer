package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class ShopTimingEntity implements Serializable {
    public String ShopDay;
    public String ShopTime;
    public boolean checked;

    public String getShopDay() {
        return ShopDay == null ? "" : ShopDay;
    }

    public void setShopDay(String shopDay) {
        ShopDay = shopDay;
    }

    public String getShopTime() {
        return ShopTime == null ? "" : ShopTime;
    }

    public void setShopTime(String shopTime) {
        ShopTime = shopTime;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

}
