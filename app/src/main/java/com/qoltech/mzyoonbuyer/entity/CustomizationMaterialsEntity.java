package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class CustomizationMaterialsEntity implements Serializable {

    public int Id;
    public String MaterialInEnglish;
    public String MaterialInArabic;
    public String Image;
    private boolean isChecked;

    public boolean getChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getMaterialInEnglish() {
        return MaterialInEnglish == null ? "" : MaterialInEnglish;
    }

    public void setMaterialInEnglish(String materialInEnglish) {
        MaterialInEnglish = materialInEnglish;
    }

    public String getMaterialInArabic() {
        return MaterialInArabic == null ? "" : MaterialInArabic;
    }

    public void setMaterialInArabic(String materialInArabic) {
        MaterialInArabic = materialInArabic;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }


}

