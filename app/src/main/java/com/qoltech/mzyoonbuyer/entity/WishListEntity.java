package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class WishListEntity implements Serializable {

    public String Id;
     public String CartId;
     public String ProductId;
     public int Quantity;
     public String ProductName;
     public String ProductImage;
     public double Amount;
     public int BrandId;
     public double UnitTotal;
     public int AvailibilityCount;
     public double NewPrice;
     public double Discount;
     public int ColorId;
     public int SizeId;
     public String ColorCode;
     public String Size;
     public int SellerId;
     public String ShopNameInEnglish;
     public String ColorName;
     public String BrandName;
     public String OrderType;
     public String shopNameInArabic;
     public String ProductNameInArabic;
     public String TailorNameInEnglish;
    public String TailorNameInArabic;
    public String BrandNameInArabic;

    public String getBrandNameInArabic() {
        return BrandNameInArabic == null ? "" : BrandNameInArabic;
    }

    public void setBrandNameInArabic(String brandNameInArabic) {
        BrandNameInArabic = brandNameInArabic;
    }

    public String getId() {
        return Id == null ? "" : Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCartId() {
        return CartId == null ? "" : CartId;
    }

    public void setCartId(String cartId) {
        CartId = cartId;
    }

    public String getProductId() {
        return ProductId == null ? "" : ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public String getProductName() {
        return ProductName == null ? "" : ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductImage() {
        return ProductImage == null ? "" : ProductImage;
    }

    public void setProductImage(String productImage) {
        ProductImage = productImage;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public int getBrandId() {
        return BrandId;
    }

    public void setBrandId(int brandId) {
        BrandId = brandId;
    }

    public double getUnitTotal() {
        return UnitTotal;
    }

    public void setUnitTotal(double unitTotal) {
        UnitTotal = unitTotal;
    }

    public int getAvailibilityCount() {
        return AvailibilityCount;
    }

    public void setAvailibilityCount(int availibilityCount) {
        AvailibilityCount = availibilityCount;
    }

    public double getNewPrice() {
        return NewPrice;
    }

    public void setNewPrice(double newPrice) {
        NewPrice = newPrice;
    }

    public double getDiscount() {
        return Discount;
    }

    public void setDiscount(double discount) {
        Discount = discount;
    }

    public int getColorId() {
        return ColorId;
    }

    public void setColorId(int colorId) {
        ColorId = colorId;
    }

    public int getSizeId() {
        return SizeId;
    }

    public void setSizeId(int sizeId) {
        SizeId = sizeId;
    }

    public String getColorCode() {
        return ColorCode == null ? "" :ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }

    public String getSize() {
        return Size == null ? "" : Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public int getSellerId() {
        return SellerId;
    }

    public void setSellerId(int sellerId) {
        SellerId = sellerId;
    }

    public String getShopNameInEnglish() {
        return ShopNameInEnglish == null ? "" : ShopNameInEnglish;
    }

    public void setShopNameInEnglish(String shopNameInEnglish) {
        ShopNameInEnglish = shopNameInEnglish;
    }

    public String getColorName() {
        return ColorName == null ? "" : ColorName;
    }

    public void setColorName(String colorName) {
        ColorName = colorName;
    }

    public String getBrandName() {
        return BrandName == null ? "" : BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }

    public String getOrderType() {
        return OrderType == null ? "" : OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public String getShopNameInArabic() {
        return shopNameInArabic == null ? "" : shopNameInArabic;
    }

    public void setShopNameInArabic(String shopNameInArabic) {
        this.shopNameInArabic = shopNameInArabic;
    }

    public String getProductNameInArabic() {
        return ProductNameInArabic == null ? "" : ProductNameInArabic;
    }

    public void setProductNameInArabic(String productNameInArabic) {
        ProductNameInArabic = productNameInArabic;
    }

    public String getTailorNameInEnglish() {
        return TailorNameInEnglish == null ? "" : TailorNameInEnglish;
    }

    public void setTailorNameInEnglish(String tailorNameInEnglish) {
        TailorNameInEnglish = tailorNameInEnglish;
    }

    public String getTailorNameInArabic() {
        return TailorNameInArabic == null ? "" : TailorNameInArabic;
    }

    public void setTailorNameInArabic(String tailorNameInArabic) {
        TailorNameInArabic = tailorNameInArabic;
    }

}
