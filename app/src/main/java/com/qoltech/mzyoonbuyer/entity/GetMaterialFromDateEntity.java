package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetMaterialFromDateEntity implements Serializable {

    public String FromDt;
     public String ToDt;
     public String AppointmentTime;
    public String Reason;
    public String RejectDt;
    public String NameInEnglish;
    public String NameInArabic;

    public String getReason() {
        return Reason == null ? "" : Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getRejectDt() {
        return RejectDt == null ? "" : RejectDt;
    }

    public void setRejectDt(String rejectDt) {
        RejectDt = rejectDt;
    }

    public String getNameInEnglish() {
        return NameInEnglish == null ? "" : NameInEnglish;
    }

    public void setNameInEnglish(String nameInEnglish) {
        NameInEnglish = nameInEnglish;
    }

    public String getNameInArabic() {
        return NameInArabic == null ? "" : NameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        NameInArabic = nameInArabic;
    }

    public String getFromDt() {
        return FromDt == null ? "" : FromDt;
    }

    public void setFromDt(String fromDt) {
        FromDt = fromDt;
    }

    public String getToDt() {
        return ToDt == null ? "" : ToDt;
    }

    public void setToDt(String toDt) {
        ToDt = toDt;
    }

    public String getAppointmentTime() {
        return AppointmentTime == null ? "" : AppointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        AppointmentTime = appointmentTime;
    }

}

