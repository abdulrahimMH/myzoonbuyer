package com.qoltech.mzyoonbuyer.entity;

public class PaymentGatewayTransactionResponse {
    private PaymentGatewayTransactionModal mobile;

    public PaymentGatewayTransactionModal getMobile() {
        return mobile;
    }

    public void setMobile(PaymentGatewayTransactionModal mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "ClassPojo [mobile = " + mobile + "]";
    }
}
