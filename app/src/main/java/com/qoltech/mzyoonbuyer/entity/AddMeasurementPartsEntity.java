package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class AddMeasurementPartsEntity implements Serializable {



    public String TextInEnglish;
    public String MeasurementValue;
    public String Image;
    public String TextInArabic;
    public String Name;
    public String MeasurementBy;
    public String Gender;
    public String NameInEnglish;
    public String NameInArabic;
    public String CreatedOn;
    public String DressSubTypeImage;
    public String MeasurementParts;
    public String MeasurementPartsInArabic;

    public String getMeasurementParts() {
        return MeasurementParts == null ? "" : MeasurementParts;
    }

    public void setMeasurementParts(String measurementParts) {
        MeasurementParts = measurementParts;
    }

    public String getMeasurementPartsInArabic() {
        return MeasurementPartsInArabic == null ? "" : MeasurementPartsInArabic;
    }

    public void setMeasurementPartsInArabic(String measurementPartsInArabic) {
        MeasurementPartsInArabic = measurementPartsInArabic;
    }

    public String getName() {
        return Name == null ? "" : Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMeasurementBy() {
        return MeasurementBy == null ? "" : MeasurementBy;
    }

    public void setMeasurementBy(String measurementBy) {
        MeasurementBy = measurementBy;
    }

    public String getGender() {
        return Gender == null ? "" : Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getNameInEnglish() {
        return NameInEnglish == null ? "" : NameInEnglish;
    }

    public void setNameInEnglish(String nameInEnglish) {
        NameInEnglish = nameInEnglish;
    }

    public String getNameInArabic() {
        return NameInArabic == null ? "" : NameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        NameInArabic = nameInArabic;
    }

    public String getCreatedOn() {
        return CreatedOn == null ? "" : CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getDressSubTypeImage() {
        return DressSubTypeImage == null ? "" : DressSubTypeImage;
    }

    public void setDressSubTypeImage(String dressSubTypeImage) {
        DressSubTypeImage = dressSubTypeImage;
    }

    public String getTextInEnglish() {
        return TextInEnglish == null ? "" : TextInEnglish;
    }

    public void setTextInEnglish(String textInEnglish) {
        TextInEnglish = textInEnglish;
    }

    public String getMeasurementValue() {
        return MeasurementValue == null ? "" : MeasurementValue;
    }

    public void setMeasurementValue(String measurementValue) {
        MeasurementValue = measurementValue;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getTextInArabic() {
        return TextInArabic == null ? "" : TextInArabic;
    }

    public void setTextInArabic(String textInArabic) {
        TextInArabic = textInArabic;
    }

}
