package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class RecentBrandEntity implements Serializable {

    public int BrandId;
    public String BrandName;
    public String ProductImage;
    public String BrandNameInArabic;

    public String getBrandNameInArabic() {
        return BrandNameInArabic == null ? "" : BrandNameInArabic;
    }

    public void setBrandNameInArabic(String brandNameInArabic) {
        BrandNameInArabic = brandNameInArabic;
    }

    public int getBrandId() {
        return BrandId;
    }

    public void setBrandId(int brandId) {
        BrandId = brandId;
    }

    public String getBrandName() {
        return BrandName == null ? "" : BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }

    public String getProductImage() {
        return ProductImage == null ? "" : ProductImage;
    }

    public void setProductImage(String productImage) {
        ProductImage = productImage;
    }


}
