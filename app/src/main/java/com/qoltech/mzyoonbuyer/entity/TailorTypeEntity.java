package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class TailorTypeEntity implements Serializable {
    public int Id;
    public String OrderTypeNameInEnglish;
    public String OrderTypeInArabic;
    public String Image;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getOrderTypeNameInEnglish() {
        return OrderTypeNameInEnglish == null ? "" : OrderTypeNameInEnglish;
    }

    public void setOrderTypeNameInEnglish(String orderTypeNameInEnglish) {
        OrderTypeNameInEnglish = orderTypeNameInEnglish;
    }

    public String getOrderTypeInArabic() {
        return OrderTypeInArabic == null ? "" : OrderTypeInArabic;
    }

    public void setOrderTypeInArabic(String orderTypeInArabic) {
        OrderTypeInArabic = orderTypeInArabic;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }


}
