package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class StoreOrderDetailsEntity implements Serializable {

    public int Id;
    public String DetailsId;
    public String OrderId;
    public String ShopNameInEnglish;
    public String ShopNameInArabic;
    public String Product_Name;
    public String NameInArabic;
    public int Quantity;
    public double Amount;
    public String CreatedOn;
    public String Image;
    public String ColorName;
    public String Size;
    public double Discount;
    public String BrandName;
    public int SellerId;
    public String OrderType;
    public int StichingOrderReferenceNo;
    public double OrderTotal;

    public double getOrderTotal() {
        return OrderTotal;
    }

    public void setOrderTotal(double orderTotal) {
        OrderTotal = orderTotal;
    }


    public int getStichingOrderReferenceNo() {
        return StichingOrderReferenceNo;
    }

    public void setStichingOrderReferenceNo(int stichingOrderReferenceNo) {
        StichingOrderReferenceNo = stichingOrderReferenceNo;
    }


    public String getOrderType() {
        return OrderType == null ? "" : OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }


    public int getSellerId() {
        return SellerId;
    }

    public void setSellerId(int sellerId) {
        SellerId = sellerId;
    }

    public double getDiscount() {
        return Discount;
    }

    public void setDiscount(double discount) {
        Discount = discount;
    }

    public String getBrandName() {
        return BrandName == null ? "" : BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }


    public String getColorName() {
        return ColorName == null ? "" : ColorName;
    }

    public void setColorName(String colorName) {
        ColorName = colorName;
    }

    public String getSize() {
        return Size == null ? "" : Size;
    }

    public void setSize(String size) {
        Size = size;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDetailsId() {
        return DetailsId == null ? "" : DetailsId;
    }

    public void setDetailsId(String detailsId) {
        DetailsId = detailsId;
    }

    public String getOrderId() {
        return OrderId == null ? "" : OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getShopNameInEnglish() {
        return ShopNameInEnglish == null ? "" : ShopNameInEnglish;
    }

    public void setShopNameInEnglish(String shopNameInEnglish) {
        ShopNameInEnglish = shopNameInEnglish;
    }

    public String getShopNameInArabic() {
        return ShopNameInArabic == null ? "" : ShopNameInArabic;
    }

    public void setShopNameInArabic(String shopNameInArabic) {
        ShopNameInArabic = shopNameInArabic;
    }

    public String getProduct_Name() {
        return Product_Name == null ? "" : Product_Name;
    }

    public void setProduct_Name(String product_Name) {
        Product_Name = product_Name;
    }

    public String getNameInArabic() {
        return NameInArabic == null ? "" : NameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        NameInArabic = nameInArabic;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public String getCreatedOn() {
        return CreatedOn == null ? "" : CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

}
