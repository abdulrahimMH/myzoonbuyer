package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class PerformenceStatusEntity implements Serializable {
    public String Status;

    public String getStatus() {
        return Status == null ? "" : Status;
    }

    public void setStatus(String status) {
        Status = status;
    }


}
