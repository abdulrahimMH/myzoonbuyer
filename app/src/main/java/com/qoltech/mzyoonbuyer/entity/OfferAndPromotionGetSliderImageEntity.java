package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class OfferAndPromotionGetSliderImageEntity implements Serializable {
    public String SliderImage;

    public String getSliderImage() {
        return SliderImage == null ? "" : SliderImage;
    }

    public void setSliderImage(String sliderImage) {
        SliderImage = sliderImage;
    }


}
