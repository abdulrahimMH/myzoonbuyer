package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class MaterialRatingEntity implements Serializable {
    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double rating;
}
