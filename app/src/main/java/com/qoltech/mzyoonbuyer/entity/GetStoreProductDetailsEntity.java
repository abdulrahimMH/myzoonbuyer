package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetStoreProductDetailsEntity implements Serializable {

    public String StoreId;
    public int OrderId;
    public String Image;
    public String ProductName;
    public double Price;
    public String Type;

    public String getStoreId() {
        return StoreId == null ? "" : StoreId;
    }

    public void setStoreId(String storeId) {
        StoreId = storeId;
    }

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getProductName() {
        return ProductName == null ? "" : ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public String getType() {
        return Type == null ? "" : Type;
    }

    public void setType(String type) {
        Type = type;
    }


}
