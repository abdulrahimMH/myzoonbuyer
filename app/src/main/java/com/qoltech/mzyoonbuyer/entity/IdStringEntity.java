package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class IdStringEntity implements Serializable {
    public String getId() {
        return Id == null ? "" : Id;

    }

    public void setId(String id) {
        Id = id;
    }

    public String Id;

}
