package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class FilterTypeEntity implements Serializable {

    private String mfilterTxt;
    private boolean isChecked;

    public String getfilterTxt() {
        return mfilterTxt;
    }

    public void setfilterTxt(String mfilterTxt) {
        this.mfilterTxt = mfilterTxt;
    }



    public FilterTypeEntity(String filterTxt, boolean b) {
        mfilterTxt = filterTxt;
        isChecked = b;
    }




    public boolean getChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

}
