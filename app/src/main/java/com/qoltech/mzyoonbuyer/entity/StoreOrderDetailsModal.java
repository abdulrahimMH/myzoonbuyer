package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class StoreOrderDetailsModal implements Serializable {
    public ArrayList<StoreOrderDetailsEntity> StoreOrderDetails;
    public ArrayList<GetStoreTrackingDetailsEntity> GetStoreTrackingDetails;
    public ArrayList<GetStoreProductDetailsEntity> GetStoreProductDetails;
    public ArrayList<GetShowBuyerAddressEntity> GetShowBuyerAddress;
    public ArrayList<GetStoreReviewEntity> GetStoreReview;
    public ArrayList<GetOrederPaymentStatusEntity> GetStorePaymentStatus;
    public ArrayList<StoreProductPriceEntity> ProductPrice;
    public ArrayList<StoreOtherProductPriceEntity> OtherProductPrice;
    public ArrayList<StoreOrderPaymentstatusEntity> Paymentstatus;

    public ArrayList<StoreOrderPaymentstatusEntity> getPaymentstatus() {
        return Paymentstatus == null ? new ArrayList<>() : Paymentstatus;
    }

    public void setPaymentstatus(ArrayList<StoreOrderPaymentstatusEntity> paymentstatus) {
        Paymentstatus = paymentstatus;
    }


    public ArrayList<StoreProductPriceEntity> getProductPrice() {
        return ProductPrice == null ? new ArrayList<>() : ProductPrice;
    }

    public void setProductPrice(ArrayList<StoreProductPriceEntity> productPrice) {
        ProductPrice = productPrice;
    }

    public ArrayList<StoreOtherProductPriceEntity> getOtherProductPrice() {
        return OtherProductPrice == null ? new ArrayList<>() : OtherProductPrice;
    }

    public void setOtherProductPrice(ArrayList<StoreOtherProductPriceEntity> otherProductPrice) {
        OtherProductPrice = otherProductPrice;
    }

    public ArrayList<GetOrederPaymentStatusEntity> getGetStorePaymentStatus() {
        return GetStorePaymentStatus == null ? new ArrayList<>() : GetStorePaymentStatus;
    }

    public void setGetStorePaymentStatus(ArrayList<GetOrederPaymentStatusEntity> getStorePaymentStatus) {
        GetStorePaymentStatus = getStorePaymentStatus;
    }

    public ArrayList<GetStoreReviewEntity> getGetStoreReview() {
        return GetStoreReview == null ? new ArrayList<>() : GetStoreReview;
    }

    public void setGetStoreReview(ArrayList<GetStoreReviewEntity> getStoreReview) {
        GetStoreReview = getStoreReview;
    }

    public ArrayList<GetShowBuyerAddressEntity> getGetShowBuyerAddress() {
        return GetShowBuyerAddress == null ? new ArrayList<>() : GetShowBuyerAddress;
    }

    public void setGetShowBuyerAddress(ArrayList<GetShowBuyerAddressEntity> getShowBuyerAddress) {
        GetShowBuyerAddress = getShowBuyerAddress;
    }

    public ArrayList<StoreOrderDetailsEntity> getStoreOrderDetails() {
        return StoreOrderDetails == null ? new ArrayList<>() : StoreOrderDetails;
    }

    public void setStoreOrderDetails(ArrayList<StoreOrderDetailsEntity> storeOrderDetails) {
        StoreOrderDetails = storeOrderDetails;
    }

    public ArrayList<GetStoreTrackingDetailsEntity> getGetStoreTrackingDetails() {
        return GetStoreTrackingDetails == null ? new ArrayList<>() : GetStoreTrackingDetails;
    }

    public void setGetStoreTrackingDetails(ArrayList<GetStoreTrackingDetailsEntity> getStoreTrackingDetails) {
        GetStoreTrackingDetails = getStoreTrackingDetails;
    }

    public ArrayList<GetStoreProductDetailsEntity> getGetStoreProductDetails() {
        return GetStoreProductDetails == null ? new ArrayList<>() : GetStoreProductDetails;
    }

    public void setGetStoreProductDetails(ArrayList<GetStoreProductDetailsEntity> getStoreProductDetails) {
        GetStoreProductDetails = getStoreProductDetails;
    }
}
