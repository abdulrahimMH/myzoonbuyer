package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetAppointmentLeftMeasurementEntity implements Serializable {
    public String MeasurementInEnglish;
     public String MeasurementInArabic;
     public String OrderDt;
    public int AppointmentLeftMeasurementCount;
    public String status;
    public boolean MaasuremntAppoinmentStatus;
    public String AppointmentTime;

    public String getAppointmentTime() {
        return AppointmentTime == null ? "" : AppointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        AppointmentTime = appointmentTime;
    }

    public boolean isMaasuremntAppoinmentStatus() {
        return MaasuremntAppoinmentStatus;
    }

    public void setMaasuremntAppoinmentStatus(boolean maasuremntAppoinmentStatus) {
        MaasuremntAppoinmentStatus = maasuremntAppoinmentStatus;
    }
    public int getAppointmentLeftMeasurementCount() {
        return AppointmentLeftMeasurementCount;
    }

    public void setAppointmentLeftMeasurementCount(int appointmentLeftMeasurementCount) {
        AppointmentLeftMeasurementCount = appointmentLeftMeasurementCount;
    }


    public String getMeasurementInEnglish() {
        return MeasurementInEnglish == null ? "" : MeasurementInEnglish;
    }

    public void setMeasurementInEnglish(String measurementInEnglish) {
        MeasurementInEnglish = measurementInEnglish;
    }

    public String getMeasurementInArabic() {
        return MeasurementInArabic == null ? "" : MeasurementInArabic;
    }

    public void setMeasurementInArabic(String measurementInArabic) {
        MeasurementInArabic = measurementInArabic;
    }

    public String getOrderDt() {
        return OrderDt == null ? "" : OrderDt;
    }

    public void setOrderDt(String orderDt) {
        OrderDt = orderDt;
    }



    public String getStatus() {
        return status == null ? "" : status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
