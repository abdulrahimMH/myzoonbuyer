package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class MaterialDeliveryChargesEntity implements Serializable {
    public String getMaterialDeliveryCharges() {
        return MaterialDeliveryCharges;
    }

    public void setMaterialDeliveryCharges(String materialDeliveryCharges) {
        MaterialDeliveryCharges = materialDeliveryCharges;
    }

    public String MaterialDeliveryCharges;
}
