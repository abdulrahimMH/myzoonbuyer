package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class CustomizationThreeClickEntity  implements Serializable {

    public String Id;
    public String CustomizationName;
    public String CustomizationLabelName;
    public String CustomizationSelectedName;
    public String Images;

    public CustomizationThreeClickEntity(String id, String customizationName, String customizationLabelName, String customizationSelectedName,String images) {
        Id = id;
        CustomizationName = customizationName;
        CustomizationLabelName = customizationLabelName;
        CustomizationSelectedName = customizationSelectedName;
        Images = images;
    }

    public String getImages() {
        return Images == null ? "" : Images;
    }

    public void setImages(String images) {
        Images = images;
    }



    public String getId() {
        return Id == null ? "" : Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCustomizationName() {
        return CustomizationName == null ? "" : CustomizationName;
    }

    public void setCustomizationName(String customizationName) {
        CustomizationName = customizationName;
    }

    public String getCustomizationLabelName() {
        return CustomizationLabelName == null ? "" : CustomizationLabelName;
    }

    public void setCustomizationLabelName(String customizationLabelName) {
        CustomizationLabelName = customizationLabelName;
    }

    public String getCustomizationSelectedName() {
        return CustomizationSelectedName == null ? "" : CustomizationSelectedName;
    }

    public void setCustomizationSelectedName(String customizationSelectedName) {
        CustomizationSelectedName = customizationSelectedName;
    }

}
