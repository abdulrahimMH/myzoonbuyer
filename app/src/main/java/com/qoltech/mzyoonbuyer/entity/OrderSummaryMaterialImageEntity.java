package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class OrderSummaryMaterialImageEntity implements Serializable {
    public String Image;

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

}
