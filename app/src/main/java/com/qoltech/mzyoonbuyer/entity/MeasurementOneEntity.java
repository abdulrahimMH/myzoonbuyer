package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class MeasurementOneEntity implements Serializable {

    public int Id;
    public String MeasurementInEnglish;
    public String MeasurementInArabic;
    public String BodyImage;
    public String HeaderImage;
    public String UnSelected;
    public boolean Status;

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }


    public String getUnSelected() {
        return UnSelected == null ? "" : UnSelected;
    }

    public void setUnSelected(String unSelected) {
        UnSelected = unSelected;
    }



    public String getHeaderImage() {
        return HeaderImage;
    }

    public void setHeaderImage(String headerImage) {
        HeaderImage = headerImage;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getMeasurementInEnglish() {
        return MeasurementInEnglish == null ? "" : MeasurementInEnglish;
    }

    public void setMeasurementInEnglish(String measurementInEnglish) {
        MeasurementInEnglish = measurementInEnglish;
    }

    public String getMeasurementInArabic() {
        return MeasurementInArabic == null ? "" : MeasurementInArabic;
    }

    public void setMeasurementInArabic(String measurementInArabic) {
        MeasurementInArabic = measurementInArabic;
    }

    public String getBodyImage() {
        return BodyImage == null ? "" : BodyImage;
    }

    public void setBodyImage(String bodyImage) {
        BodyImage = bodyImage;
    }

}
