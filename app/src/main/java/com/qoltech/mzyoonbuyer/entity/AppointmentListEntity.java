package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class AppointmentListEntity implements Serializable {
    public int Id;
    public String CreatedOn;
    public String NameInEnglish;
    public String NameInArabic;
    public int ApprovedTailorId;
    public String TailorNameInEnglish;
    public String TailorNameInArabic;
    public String ShopNameInEnglish;
    public String ShopNameInArabic;
    public String Image;
    public int TailorId;
    public int DressSubTypeId;
    public int DeliveryTypeId;

    public int getTailorId() {
        return TailorId;
    }

    public void setTailorId(int tailorId) {
        TailorId = tailorId;
    }

    public int getDressSubTypeId() {
        return DressSubTypeId;
    }

    public void setDressSubTypeId(int dressSubTypeId) {
        DressSubTypeId = dressSubTypeId;
    }

    public int getDeliveryTypeId() {
        return DeliveryTypeId;
    }

    public void setDeliveryTypeId(int deliveryTypeId) {
        DeliveryTypeId = deliveryTypeId;
    }


    public int getApprovedTailorId() {
        return ApprovedTailorId;
    }

    public void setApprovedTailorId(int approvedTailorId) {
        ApprovedTailorId = approvedTailorId;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCreatedOn() {
        return CreatedOn == null ? "" : CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getNameInEnglish() {
        return NameInEnglish == null ? "" : NameInEnglish;
    }

    public void setNameInEnglish(String nameInEnglish) {
        NameInEnglish = nameInEnglish;
    }

    public String getNameInArabic() {
        return NameInArabic == null ? "" : NameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        NameInArabic = nameInArabic;
    }

    public String getTailorNameInEnglish() {
        return TailorNameInEnglish == null ? "" : TailorNameInEnglish;
    }

    public void setTailorNameInEnglish(String tailorNameInEnglish) {
        TailorNameInEnglish = tailorNameInEnglish;
    }

    public String getTailorNameInArabic() {
        return TailorNameInArabic == null ? "" : TailorNameInArabic;
    }

    public void setTailorNameInArabic(String tailorNameInArabic) {
        TailorNameInArabic = tailorNameInArabic;
    }

    public String getShopNameInEnglish() {
        return ShopNameInEnglish == null ? "" : ShopNameInEnglish;
    }

    public void setShopNameInEnglish(String shopNameInEnglish) {
        ShopNameInEnglish = shopNameInEnglish;
    }

    public String getShopNameInArabic() {
        return ShopNameInArabic == null ? "" : ShopNameInArabic;
    }

    public void setShopNameInArabic(String shopNameInArabic) {
        ShopNameInArabic = shopNameInArabic;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

}
