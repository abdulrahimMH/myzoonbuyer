package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class TailorShopTimeEntity implements Serializable {
    public String getTailorShopTime() {
        return TailorShopTime == null ? "" : TailorShopTime;
    }

    public void setTailorShopTime(String tailorShopTime) {
        TailorShopTime = tailorShopTime;
    }

    public String TailorShopTime;

}
