package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class DistanceEntity implements Serializable {
    public String getText() {
        return text == null ? "" : text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String text;
}
