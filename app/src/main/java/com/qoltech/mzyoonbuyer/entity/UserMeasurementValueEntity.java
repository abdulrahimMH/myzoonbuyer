package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class UserMeasurementValueEntity implements Serializable {
    public String UserMeasurementId;
    public String Value;

    public String getUserMeasurementId() {
        return UserMeasurementId == null ? "" : UserMeasurementId;
    }

    public void setUserMeasurementId(String userMeasurementId) {
        UserMeasurementId = userMeasurementId;
    }

    public String getValue() {
        return Value == null ? "" : Value;
    }

    public void setValue(String value) {
        Value = value;
    }

}
