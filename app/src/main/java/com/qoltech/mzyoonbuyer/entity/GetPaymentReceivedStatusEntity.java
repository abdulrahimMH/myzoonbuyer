package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetPaymentReceivedStatusEntity implements Serializable {
    public String PaymentReceivedStatus;

    public String getPaymentReceivedStatus() {
        return PaymentReceivedStatus == null ? "" : PaymentReceivedStatus;
    }

    public void setPaymentReceivedStatus(String paymentReceivedStatus) {
        PaymentReceivedStatus = paymentReceivedStatus;
    }

}
