package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetPaymentDetailsEntity implements Serializable {

    public String Amount;
    public String CoupanReduceAmount;
    public String ReduceAmount;

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getCoupanReduceAmount() {
        return CoupanReduceAmount == null ? "" : CoupanReduceAmount;
    }

    public void setCoupanReduceAmount(String coupanReduceAmount) {
        CoupanReduceAmount = coupanReduceAmount;
    }

    public String getReduceAmount() {
        return ReduceAmount == null ? "" : ReduceAmount;
    }

    public void setReduceAmount(String reduceAmount) {
        ReduceAmount = reduceAmount;
    }

}
