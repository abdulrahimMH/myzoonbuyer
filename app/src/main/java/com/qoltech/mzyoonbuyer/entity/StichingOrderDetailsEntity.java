package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class StichingOrderDetailsEntity implements Serializable {
    public int OrderId;
      public String DressSubtypeName;
      public String Image;
      public int Quantity;
      public String price;
      public String UnitTotal;
      public String OrderType;
      public String DeliveryNameInEnglish;
      public String DeliveryNameInArabic;
      public String DeliveryTypeInArabic;
      public String DeliveryTypeInEnglish;
      public String MaterialInEnglish;
      public String MaterialInArabic;
      public int OrderTypeId;
      public int MeasurementTypeId;
      public int DeliveryTypeId;
      public String ShopNameInEnglish;
      public String ShopNameInArabic;
      public String MaterialTypeInEnglish;
      public String MaterialTypeInArabic;
      public String MeasurementInEnglish;
      public String MeasurementInArabic;
      public String Initiatedby;
      public String TailorNameInEnglish;
      public String TailorNameInArabic;
      public int SellerId;
      public String Size;
      public String BrandName;
      public String Discount;
      public String ColorName;
      public String ColorCode;
      public String CreatedOn;
      public String OrderTotal;
      public String StichingOrderReferenceNo;
      public int PatternId;
      public int DressSubTypeId;
      public int DressTypeId;
      public String OrderLineId;
  public int ColorId;
  public int SizeId;
  public double Rating;
  public String status;
  public String LineId;

  public String getLineId() {
    return LineId == null ? "" : LineId;
  }

  public void setLineId(String lineId) {
    LineId = lineId;
  }


  public String getStatus() {
    return status == null ? "" : status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


    public double getRating() {
        return Rating;
    }

    public void setRating(double rating) {
        Rating = rating;
    }

  public int getColorId() {
    return ColorId;
  }

  public void setColorId(int colorId) {
    this.ColorId = colorId;
  }

  public int getSizeId() {
    return SizeId;
  }

  public void setSizeId(int sizeId) {
    SizeId = sizeId;
  }


  public String getOrderLineId() {
    return OrderLineId == null  ? "" : OrderLineId;
  }

  public void setOrderLineId(String orderLineId) {
    OrderLineId = orderLineId;
  }


  public int getPatternId() {
    return PatternId;
  }

  public void setPatternId(int patternId) {
    PatternId = patternId;
  }

  public int getDressSubTypeId() {
    return DressSubTypeId;
  }

  public void setDressSubTypeId(int dressSubTypeId) {
    DressSubTypeId = dressSubTypeId;
  }

  public int getDressTypeId() {
    return DressTypeId;
  }

  public void setDressTypeId(int dressTypeId) {
    DressTypeId = dressTypeId;
  }


  public int getOrderId() {
    return OrderId;
  }

  public void setOrderId(int orderId) {
    OrderId = orderId;
  }

  public String getDressSubtypeName() {
    return DressSubtypeName ==  null ? "" : DressSubtypeName;
  }

  public void setDressSubtypeName(String dressSubtypeName) {
    DressSubtypeName = dressSubtypeName;
  }

  public String getImage() {
    return Image == null ? "" : Image;
  }

  public void setImage(String image) {
    Image = image;
  }

  public int getQuantity() {
    return Quantity;
  }

  public void setQuantity(int quantity) {
    Quantity = quantity;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getUnitTotal() {
    return UnitTotal;
  }

  public void setUnitTotal(String unitTotal) {
    UnitTotal = unitTotal;
  }

  public String getOrderType() {
    return OrderType == null ? "" : OrderType;
  }

  public void setOrderType(String orderType) {
    OrderType = orderType;
  }

  public String getDeliveryNameInEnglish() {
    return DeliveryNameInEnglish == null ? "" : DeliveryNameInEnglish;
  }

  public void setDeliveryNameInEnglish(String deliveryNameInEnglish) {
    DeliveryNameInEnglish = deliveryNameInEnglish;
  }

  public String getDeliveryNameInArabic() {
    return DeliveryNameInArabic == null ? "" : DeliveryNameInArabic;
  }

  public void setDeliveryNameInArabic(String deliveryNameInArabic) {
    DeliveryNameInArabic = deliveryNameInArabic;
  }

  public String getDeliveryTypeInArabic() {
    return DeliveryTypeInArabic == null ? "" : DeliveryTypeInArabic;
  }

  public void setDeliveryTypeInArabic(String deliveryTypeInArabic) {
    DeliveryTypeInArabic = deliveryTypeInArabic;
  }

  public String getDeliveryTypeInEnglish() {
    return DeliveryTypeInEnglish == null ? "" : DeliveryTypeInEnglish;
  }

  public void setDeliveryTypeInEnglish(String deliveryTypeInEnglish) {
    DeliveryTypeInEnglish = deliveryTypeInEnglish;
  }

  public String getMaterialInEnglish() {
    return MaterialInEnglish == null ? "" : MaterialInEnglish;
  }

  public void setMaterialInEnglish(String materialInEnglish) {
    MaterialInEnglish = materialInEnglish;
  }

  public String getMaterialInArabic() {
    return MaterialInArabic == null ? "" : MaterialInArabic;
  }

  public void setMaterialInArabic(String materialInArabic) {
    MaterialInArabic = materialInArabic;
  }

  public int getOrderTypeId() {
    return OrderTypeId;
  }

  public void setOrderTypeId(int orderTypeId) {
    OrderTypeId = orderTypeId;
  }

  public int getMeasurementTypeId() {
    return MeasurementTypeId;
  }

  public void setMeasurementTypeId(int measurementTypeId) {
    MeasurementTypeId = measurementTypeId;
  }

  public int getDeliveryTypeId() {
    return DeliveryTypeId;
  }

  public void setDeliveryTypeId(int deliveryTypeId) {
    DeliveryTypeId = deliveryTypeId;
  }

  public String getShopNameInEnglish() {
    return ShopNameInEnglish == null ? "" : ShopNameInEnglish;
  }

  public void setShopNameInEnglish(String shopNameInEnglish) {
    ShopNameInEnglish = shopNameInEnglish;
  }

  public String getShopNameInArabic() {
    return ShopNameInArabic == null ? "" : ShopNameInArabic;
  }

  public void setShopNameInArabic(String shopNameInArabic) {
    ShopNameInArabic = shopNameInArabic;
  }

  public String getMaterialTypeInEnglish() {
    return MaterialTypeInEnglish == null ? "" : MaterialTypeInEnglish;
  }

  public void setMaterialTypeInEnglish(String materialTypeInEnglish) {
    MaterialTypeInEnglish = materialTypeInEnglish;
  }

  public String getMaterialTypeInArabic() {
    return MaterialTypeInArabic == null ? "" : MaterialTypeInArabic;
  }

  public void setMaterialTypeInArabic(String materialTypeInArabic) {
    MaterialTypeInArabic = materialTypeInArabic;
  }

  public String getMeasurementInEnglish() {
    return MeasurementInEnglish == null ? "" : MeasurementInEnglish;
  }

  public void setMeasurementInEnglish(String measurementInEnglish) {
    MeasurementInEnglish = measurementInEnglish;
  }

  public String getMeasurementInArabic() {
    return MeasurementInArabic == null ? "" : MeasurementInArabic;
  }

  public void setMeasurementInArabic(String measurementInArabic) {
    MeasurementInArabic = measurementInArabic;
  }

  public String getInitiatedby() {
    return Initiatedby == null ? "" : Initiatedby;
  }

  public void setInitiatedby(String initiatedby) {
    Initiatedby = initiatedby;
  }

  public String getTailorNameInEnglish() {
    return TailorNameInEnglish == null ? "" : TailorNameInEnglish;
  }

  public void setTailorNameInEnglish(String tailorNameInEnglish) {
    TailorNameInEnglish = tailorNameInEnglish;
  }

  public String getTailorNameInArabic() {
    return TailorNameInArabic == null ? "" : TailorNameInArabic;
  }

  public void setTailorNameInArabic(String tailorNameInArabic) {
    TailorNameInArabic = tailorNameInArabic;
  }

  public int getSellerId() {
    return SellerId = SellerId;
  }

  public void setSellerId(int sellerId) {
    SellerId = sellerId;
  }

  public String getSize() {
    return Size == null ? "" : Size;
  }

  public void setSize(String size) {
    Size = size;
  }

  public String getBrandName() {
    return BrandName == null ? "" : BrandName;
  }

  public void setBrandName(String brandName) {
    BrandName = brandName;
  }

  public String getDiscount() {
    return Discount == null ? "" : Discount;
  }

  public void setDiscount(String discount) {
    Discount = discount;
  }

  public String getColorName() {
    return ColorName == null ? "" : ColorName;
  }

  public void setColorName(String colorName) {
    ColorName = colorName;
  }

  public String getColorCode() {
    return ColorCode == null ? "" : ColorCode;
  }

  public void setColorCode(String colorCode) {
    ColorCode = colorCode;
  }

  public String getCreatedOn() {
    return CreatedOn == null ? "" : CreatedOn;
  }

  public void setCreatedOn(String createdOn) {
    CreatedOn = createdOn;
  }

  public String getOrderTotal() {
    return OrderTotal == null ? "" : OrderTotal;
  }

  public void setOrderTotal(String orderTotal) {
    OrderTotal = orderTotal;
  }

  public String getStichingOrderReferenceNo() {
    return StichingOrderReferenceNo ==  null ? "" : StichingOrderReferenceNo;
  }

  public void setStichingOrderReferenceNo(String stichingOrderReferenceNo) {
    StichingOrderReferenceNo = stichingOrderReferenceNo;
  }

}
