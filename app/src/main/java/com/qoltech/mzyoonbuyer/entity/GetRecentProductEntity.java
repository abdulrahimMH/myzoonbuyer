package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetRecentProductEntity implements Serializable {

    ArrayList<IdStringEntity> Id;
    public String cartId;
    public String Key;

    public ArrayList<IdStringEntity> getId() {
        return Id == null ? new ArrayList<>() : Id;
    }

    public void setId(ArrayList<IdStringEntity> id) {
        Id = id;
    }

    public String getCartId() {
        return cartId == null ? "" : cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getKey() {
        return Key == null ? "" : Key;
    }

    public void setKey(String key) {
        Key = key;
    }


}
