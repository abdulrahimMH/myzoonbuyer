package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetAppointmentLeftDeliveryEntity implements Serializable {
    public String DeliveryTypeInEnglish;
     public String DeliveryTypeInArabic;
     public String OrderDt;
     public int AppointmentLeftScheduleCount;
    public String status;
    public boolean ScheduleAppoinmentStatus;
    public String AppointmentTime;

    public String getAppointmentTime() {
        return AppointmentTime == null ? "" : AppointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        AppointmentTime = appointmentTime;
    }

    public boolean isScheduleAppoinmentStatus() {
        return ScheduleAppoinmentStatus;
    }

    public void setScheduleAppoinmentStatus(boolean scheduleAppoinmentStatus) {
        ScheduleAppoinmentStatus = scheduleAppoinmentStatus;
    }

    public String getDeliveryTypeInEnglish() {
        return DeliveryTypeInEnglish == null ? "" : DeliveryTypeInEnglish;
    }

    public void setDeliveryTypeInEnglish(String deliveryTypeInEnglish) {
        DeliveryTypeInEnglish = deliveryTypeInEnglish;
    }

    public String getDeliveryTypeInArabic() {
        return DeliveryTypeInArabic == null ? "" : DeliveryTypeInArabic;
    }

    public void setDeliveryTypeInArabic(String deliveryTypeInArabic) {
        DeliveryTypeInArabic = deliveryTypeInArabic;
    }

    public String getOrderDt() {
        return OrderDt == null ? "" : OrderDt;
    }

    public void setOrderDt(String orderDt) {
        OrderDt = orderDt;
    }

    public int getAppointmentLeftScheduleCount() {
        return AppointmentLeftScheduleCount;
    }

    public void setAppointmentLeftScheduleCount(int appointmentLeftScheduleCount) {
        AppointmentLeftScheduleCount = appointmentLeftScheduleCount;
    }

    public String getStatus() {
        return status == null ? "" : status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
