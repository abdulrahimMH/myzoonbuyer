package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetProductDetailsImages implements Serializable {

    public String Small;
    public String Medium;
    public String Large;

    public String getSmall() {
        return Small == null ? "" : Small;
    }

    public void setSmall(String small) {
        Small = small;
    }

    public String getMedium() {
        return Medium == null ? "" : Medium;
    }

    public void setMedium(String medium) {
        Medium = medium;
    }

    public String getLarge() {
        return Large == null ? "" : Large;
    }

    public void setLarge(String large) {
        Large = large;
    }



}
