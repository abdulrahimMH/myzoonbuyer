package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetStoreDashBoardEntity implements Serializable {
    public int Id;
    public String BannerImage;
    public int TargetId;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getBannerImage() {
        return BannerImage == null ? "" : BannerImage;
    }

    public void setBannerImage(String bannerImage) {
        BannerImage = bannerImage;
    }

    public int getTargetId() {
        return TargetId;
    }

    public void setTargetId(int targetId) {
        TargetId = targetId;
    }

}
