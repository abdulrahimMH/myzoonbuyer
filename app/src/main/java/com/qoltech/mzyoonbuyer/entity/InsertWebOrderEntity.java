package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class InsertWebOrderEntity implements Serializable {
    public int UserId;
    public int AddressId;
    public double OrderAmount;
    public int Discount;
    public int Tax;
    public double CouponDiscount;
    public double OrderTotal;
    public String CouponCode;
    public String CartId;
    public int StichingOrderReferenceNo;

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getAddressId() {
        return AddressId;
    }

    public void setAddressId(int addressId) {
        AddressId = addressId;
    }

    public double getOrderAmount() {
        return OrderAmount;
    }

    public void setOrderAmount(double orderAmount) {
        OrderAmount = orderAmount;
    }

    public int getDiscount() {
        return Discount;
    }

    public void setDiscount(int discount) {
        Discount = discount;
    }

    public int getTax() {
        return Tax;
    }

    public void setTax(int tax) {
        Tax = tax;
    }

    public double getCouponDiscount() {
        return CouponDiscount;
    }

    public void setCouponDiscount(double couponDiscount) {
        CouponDiscount = couponDiscount;
    }

    public double getOrderTotal() {
        return OrderTotal;
    }

    public void setOrderTotal(double orderTotal) {
        OrderTotal = orderTotal;
    }

    public String getCouponCode() {
        return CouponCode;
    }

    public void setCouponCode(String couponCode) {
        CouponCode = couponCode;
    }

    public String getCartId() {
        return CartId;
    }

    public void setCartId(String cartId) {
        CartId = cartId;
    }

    public int getStichingOrderReferenceNo() {
        return StichingOrderReferenceNo;
    }

    public void setStichingOrderReferenceNo(int stichingOrderReferenceNo) {
        StichingOrderReferenceNo = stichingOrderReferenceNo;
    }

}
