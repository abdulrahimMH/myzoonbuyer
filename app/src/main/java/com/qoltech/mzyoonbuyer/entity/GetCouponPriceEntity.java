package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetCouponPriceEntity implements Serializable {

    public int DiscountValue;
    public int DiscountType;
    public double minimumamount;
    public double MaximumDiscount;
    public int CoupanRedemtionmethod;
    public String CoupanType;
    public int CouponAppliesTo;
    public int Id;
    public int TailorId;
    public String Result;

    public String getResult() {
        return Result == null ? "" : Result;
    }

    public void setResult(String result) {
        Result = result;
    }


    public int getTailorId() {
        return TailorId;
    }

    public void setTailorId(int tailorId) {
        TailorId = tailorId;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }


    public int getDiscountValue() {
        return DiscountValue;
    }

    public void setDiscountValue(int discountValue) {
        DiscountValue = discountValue;
    }

    public int getDiscountType() {
        return DiscountType;
    }

    public void setDiscountType(int discountType) {
        DiscountType = discountType;
    }

    public double getMinimumamount() {
        return minimumamount;
    }

    public void setMinimumamount(double minimumamount) {
        this.minimumamount = minimumamount;
    }

    public double getMaximumDiscount() {
        return MaximumDiscount;
    }

    public void setMaximumDiscount(double maximumDiscount) {
        MaximumDiscount = maximumDiscount;
    }

    public int getCoupanRedemtionmethod() {
        return CoupanRedemtionmethod;
    }

    public void setCoupanRedemtionmethod(int coupanRedemtionmethod) {
        CoupanRedemtionmethod = coupanRedemtionmethod;
    }

    public String getCoupanType() {
        return CoupanType;
    }

    public void setCoupanType(String coupanType) {
        CoupanType = coupanType;
    }

    public int getCouponAppliesTo() {
        return CouponAppliesTo;
    }

    public void setCouponAppliesTo(int couponAppliesTo) {
        CouponAppliesTo = couponAppliesTo;
    }
}
