package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetBuyerAddressEntity implements Serializable {

    public int Id;
    public int BuyerId;
    public double Lattitude;
    public double Longitude;
    public String FirstName;
    public String LastName;
    public String Name;
    public String StateName;
    public String Area;
    public int AreaId;
    public String Floor;
    public String LandMark;
    public String LocationType;
    public String ShippingNotes;
    public String PhoneNo;
    public String CountryCode;
    public boolean IsDefault;
    public String CreatedBy;
    public String CountryName;
    public String Email;
    public String Landmark;

    public String getLandmark() {
        return Landmark == null ? "" : Landmark;
    }

    public void setLandmark(String landmark) {
        Landmark = landmark;
    }


    public String getEmail() {
        return Email == null ? "" : Email;
    }

    public void setEmail(String email) {
        Email = email;
    }


    public String getCountryName() {
        return CountryName == null ? "" : CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getBuyerId() {
        return BuyerId;
    }

    public void setBuyerId(int buyerId) {
        BuyerId = buyerId;
    }

    public double getLattitude() {
        return Lattitude;
    }

    public void setLattitude(double lattitude) {
        Lattitude = lattitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public String getFirstName() {
        return FirstName == null ? "" : FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName == null ? "" : LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getName() {
        return Name == null ? "" : Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getStateName() {
        return StateName == null ? "" : StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public String getArea() {
        return Area == null ? "" : Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public int getAreaId() {
        return AreaId;
    }

    public void setAreaId(int areaId) {
        AreaId = areaId;
    }

    public String getFloor() {
        return Floor == null ? "" : Floor;
    }

    public void setFloor(String floor) {
        Floor = floor;
    }

    public String getLandMark() {
        return LandMark == null ? "" : LandMark;
    }

    public void setLandMark(String landMark) {
        LandMark = landMark;
    }

    public String getLocationType() {
        return LocationType == null ? "" : LocationType;
    }

    public void setLocationType(String locationType) {
        LocationType = locationType;
    }

    public String getShippingNotes() {
        return ShippingNotes == null ? "" : ShippingNotes;
    }

    public void setShippingNotes(String shippingNotes) {
        ShippingNotes = shippingNotes;
    }

    public String getPhoneNo() {
        return PhoneNo == null ? "" : PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public String getCountryCode() {
        return CountryCode == null ? "" : CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public boolean isDefault() {
        return IsDefault;
    }

    public void setDefault(boolean aDefault) {
        IsDefault = aDefault;
    }

    public String getCreatedBy() {
        return CreatedBy == null ? "" : CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

}
