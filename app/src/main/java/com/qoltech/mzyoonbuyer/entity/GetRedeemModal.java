package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetRedeemModal implements Serializable {
    public double CurrentPoints;
    public ArrayList<GetRedeemEntity> DefaultPointAmount;
    public double SingleAmount;
    public double SinglePoint;

    public double getSingleAmount() {
        return SingleAmount;
    }

    public void setSingleAmount(double singleAmount) {
        SingleAmount = singleAmount;
    }

    public double getSinglePoint() {
        return SinglePoint;
    }

    public void setSinglePoint(double singlePoint) {
        SinglePoint = singlePoint;
    }

    public double getCurrentPoints() {
        return CurrentPoints;
    }

    public void setCurrentPoints(double currentPoints) {
        CurrentPoints = currentPoints;
    }

    public ArrayList<GetRedeemEntity> getDefaultPointAmount() {
        return DefaultPointAmount == null ? new ArrayList<>() : DefaultPointAmount;
    }

    public void setDefaultPointAmount(ArrayList<GetRedeemEntity> defaultPointAmount) {
        DefaultPointAmount = defaultPointAmount;
    }


}
