package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class OrderFlowTypeEntity implements Serializable {
    public int Id;
    public String OrderType;
    public String OrderTypeInArabic;
    public String OrderTypeImage;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getOrderType() {
        return OrderType == null ? "" : OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public String getOrderTypeInArabic() {
        return OrderTypeInArabic == null ? "" : OrderTypeInArabic;
    }

    public void setOrderTypeInArabic(String orderTypeInArabic) {
        OrderTypeInArabic = orderTypeInArabic;
    }

    public String getOrderTypeImage() {
        return OrderTypeImage == null ? "" : OrderTypeImage;
    }

    public void setOrderTypeImage(String orderTypeImage) {
        OrderTypeImage = orderTypeImage;
    }

}
