package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class DressTypeEntity  implements Serializable {

    private int Id;
    private String NameInEnglish;
    private String NameInArabic;
    private String ImageURL;
    private String Gender;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDressTypeInEnglish() {
        return NameInEnglish == null ? "" : NameInEnglish;
    }

    public void setDressTypeInEnglish(String dressTypeInEnglish) {
        NameInEnglish = dressTypeInEnglish;
    }

    public String getDressTypeInArabic() {
        return NameInArabic == null ? "" : NameInArabic;
    }

    public void setDressTypeInArabic(String dressTypeInArabic) {
        NameInArabic = dressTypeInArabic;
    }

    public String getImageURL() {
        return ImageURL == null ? "" : ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getGender() {
        return Gender == null ? "" : Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

}
