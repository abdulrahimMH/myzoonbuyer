package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetNewCustomizationEntity implements Serializable {

    public ArrayList<CustomizationSessionEntity> seasons;
    public ArrayList<CustomizationPlaceEntity> placeofIndustrys;
    public ArrayList<CustomizationBrandEntity> materialBrand;
    public ArrayList<CustomizationThicknessEntity> Thickness;
    public ArrayList<CustomizationMaterialsEntity> Materials;
    public ArrayList<CustomizationColorsEntity> Colors;
    public ArrayList<CustomizationPatternsEntity> Patterns;

    public ArrayList<CustomizationThicknessEntity> getThickness() {
        return Thickness == null ? new ArrayList<>() : Thickness;
    }

    public void setThickness(ArrayList<CustomizationThicknessEntity> thickness) {
        Thickness = thickness;
    }

    public ArrayList<CustomizationSessionEntity> getSeasons() {
        return seasons == null ? new ArrayList<CustomizationSessionEntity>() : seasons;
    }

    public void setSeasons(ArrayList<CustomizationSessionEntity> seasons) {
        this.seasons = seasons;
    }

    public ArrayList<CustomizationPlaceEntity> getPlaceofIndustrys() {
        return placeofIndustrys == null ? new ArrayList<CustomizationPlaceEntity>() : placeofIndustrys;
    }

    public void setPlaceofIndustrys(ArrayList<CustomizationPlaceEntity> placeofIndustrys) {
        this.placeofIndustrys = placeofIndustrys;
    }

    public ArrayList<CustomizationBrandEntity> getMaterialBrand() {
        return materialBrand == null ? new ArrayList<CustomizationBrandEntity>() : materialBrand;
    }

    public void setMaterialBrand(ArrayList<CustomizationBrandEntity> materialBrand) {
        this.materialBrand = materialBrand;
    }


    public ArrayList<CustomizationMaterialsEntity> getMaterials() {
        return Materials == null ? new ArrayList<CustomizationMaterialsEntity>() : Materials;
    }

    public void setMaterials(ArrayList<CustomizationMaterialsEntity> materials) {
        Materials = materials;
    }

    public ArrayList<CustomizationColorsEntity> getColors() {
        return Colors == null ? new ArrayList<CustomizationColorsEntity>() : Colors;
    }

    public void setColors(ArrayList<CustomizationColorsEntity> colors) {
        Colors = colors;
    }

    public ArrayList<CustomizationPatternsEntity> getPatterns() {
        return Patterns == null ? new ArrayList<CustomizationPatternsEntity>() : Patterns;
    }

    public void setPatterns(ArrayList<CustomizationPatternsEntity> patterns) {
        Patterns = patterns;
    }

}
