package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class PaymentAddressEntity implements Serializable {
    public String Building;
    public String Floor;
    public String Landmark;
    public String locationType;
    public String CountryName;
    public String StateName;
    public String Area;
    public String Email;

    public String getBuilding() {
        return Building == null ? "" : Building;
    }

    public void setBuilding(String building) {
        Building = building;
    }

    public String getFloor() {
        return Floor == null ? "" : Floor;
    }

    public void setFloor(String floor) {
        Floor = floor;
    }

    public String getLandmark() {
        return Landmark == null ? "" : Landmark;
    }

    public void setLandmark(String landmark) {
        Landmark = landmark;
    }

    public String getLocationType() {
        return locationType == null ? "" : locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getCountryName() {
        return CountryName == null ? "" : CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getStateName() {
        return StateName == null ? "" : StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public String getArea() {
        return Area == null ? "" : Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getEmail() {
        return Email == null ? "" : Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

}
