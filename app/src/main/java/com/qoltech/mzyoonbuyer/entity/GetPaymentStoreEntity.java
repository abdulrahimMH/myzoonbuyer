package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class GetPaymentStoreEntity implements Serializable {
    public int StoreId;
    public String KeyId;
    public String MerchantId;

    public int getStoreId() {
        return StoreId;
    }

    public void setStoreId(int storeId) {
        StoreId = storeId;
    }

    public String getKeyId() {
        return KeyId;
    }

    public void setKeyId(String keyId) {
        KeyId = keyId;
    }

    public String getMerchantId() {
        return MerchantId;
    }

    public void setMerchantId(String merchantId) {
        MerchantId = merchantId;
    }


}
