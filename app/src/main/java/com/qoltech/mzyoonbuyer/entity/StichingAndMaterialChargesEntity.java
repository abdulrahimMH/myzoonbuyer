package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class StichingAndMaterialChargesEntity implements Serializable {
    public String getStichingAndMaterialCharge() {
        return StichingAndMaterialCharge;
    }

    public void setStichingAndMaterialCharge(String stichingAndMaterialCharge) {
        StichingAndMaterialCharge = stichingAndMaterialCharge;
    }

    public String StichingAndMaterialCharge;

}
