package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class DeliveryChargesEntity implements Serializable {
    public String getDeliveryCharge() {
        return DeliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        DeliveryCharge = deliveryCharge;
    }

    public String DeliveryCharge;

}
