package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class OrderApprovalPriceChargesEntity implements Serializable {
    public String DescInEnglish;
    public double Amount;

    public String getDescInEnglish() {
        return DescInEnglish == null ? "" : DescInEnglish;
    }

    public void setDescInEnglish(String descInEnglish) {
        DescInEnglish = descInEnglish;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

}
