package com.qoltech.mzyoonbuyer.entity;

import java.io.Serializable;

public class FullStatusEntity implements Serializable {

    public double getFullStatus() {
        return FullStatus;
    }

    public void setFullStatus(double fullStatus) {
        FullStatus = fullStatus;
    }

    public double FullStatus;
}
