package com.qoltech.mzyoonbuyer.utils;

import com.qoltech.mzyoonbuyer.entity.CustomizationAttributesEntity;
import com.qoltech.mzyoonbuyer.entity.CustomizationThreeClickEntity;
import com.qoltech.mzyoonbuyer.entity.FilterTypeEntity;
import com.qoltech.mzyoonbuyer.entity.GetAppointmentLeftMeasurementEntity;
import com.qoltech.mzyoonbuyer.entity.GetMeasurementPartEntity;
import com.qoltech.mzyoonbuyer.entity.GetProductSellerEntity;
import com.qoltech.mzyoonbuyer.entity.GetStoreReviewEntity;
import com.qoltech.mzyoonbuyer.entity.IdEntity;
import com.qoltech.mzyoonbuyer.entity.ManuallyEntity;
import com.qoltech.mzyoonbuyer.entity.PaymentInfoEntity;
import com.qoltech.mzyoonbuyer.entity.StoreCartEntity;
import com.qoltech.mzyoonbuyer.entity.TailorListEntity;

import java.util.ArrayList;
import java.util.HashMap;

public class AppConstants {
    /*Notification*/
    public static final String CHANNEL_ID = "my_channel_01";
    public static final String CHANNEL_NAME = "Simplified Coding Notification";
    public static final String CHANNEL_DESCRIPTION = "www.simplifiedcoding.net";
    public static  String TOKEN = "";

    /*PREVIOUS_SCREEN*/
    public static ArrayList<String> PREVIOUS_SCREEN = new ArrayList<>();

    public static final String HOME_ACTIVITY = "HOME_ACTIVITY";

    public static String TAG = "TAG";

    static final String SHARE_PREFERENCE = "SHARE_PREFERENCE";

    public static String LOGIN_STATUS = "LOGIN_STATUS";

    /*OLD BASE URL*/
    public static final String LOCAL_BASE_URL = "http://192.168.0.29/TailorAPI/Api/";

//   /*BASE LIVE URL*/
//    public static final String BASE_URL = "http://appsapi.mzyoon.com/api/";
//
//    /*IMAGE BASE_URL*/
//    public static final String IMAGE_BASE_URL = "http://appsapi.mzyoon.com/";

//    /*STORE BASE URL*/
//    public static final String STORE_BASE_URL = "http://shop.mzyoon.com";

    /*BASE LIVE URL*/
    public static final String BASE_URL = "http://development.mzyoon.com/api/";

    /*IMAGE BASE_URL*/
    public static final String IMAGE_BASE_URL = "http://development.mzyoon.com/";

    /*STORE BASE URL*/
    public static final String STORE_BASE_URL = "http://devstore.mzyoon.com";

    //    http://shop.mzyoon.com

//    /*IMAGE BASE_URL*/
//    public static final String IMAGE_BASE_URL = "http://staging.mzyoon.com/";
//
//    /*BASE DEV URL*/
//    public static final String BASE_URL = "http://staging.mzyoon.com/api/";

//    public static final String IMAGE_LOCAL_BASE_URL = "http://192.168.0.29/TailorAPI/";

    /*Device Id*/
    public static String DEVICE_ID = "DEVICE_ID";

    /*Mobile Num*/
    public static String MOBILE_NUM = "MOBILE_NUM";

    /*Country Code*/
    public static String COUNTRY_CODE = "CountryCode";

    /*User Id*/
    public static int USER_ID = 0;

    /*StringUserId*/
    public static String USER_ID_STR = "USER_ID_STR";

    /*ProfileImg*/
    public static String USER_IMAGE = "";

    /*Gender Id*/
    public static String GENDER_ID = "0";

    /*Dress Type*/
    public static String DRESS_TYPE_ID = "0";

    /*Dress Type*/
    public static String SUB_DRESS_TYPE_ID = "0";

    /*Date Picker*/
    public static String DATE_PICKER_COND = "";

    /*Seasonal Id*/
    public static String SEASONAL_ID = "";

    /*Seasonal Id*/
    public static String SEASONAL_OLD_ID = "";

    /*Place of Industry Id*/
    public static String PLACE_INDUSTRY_ID = "";

    /*Place of Industry Id*/
    public static String PLACE_INDUSTRY_OLD_ID = "";

    /*Brands Id*/
    public static String BRANDS_ID = "";

    /*Brands Id*/
    public static String BRANDS_OLD_ID = "";

    /*Material Id*/
    public static String MATERIAL_ID = "";

    /*Material Id*/
    public static String MATERIAL_OLD_ID = "";

    /*Color Id*/
    public static String COLOR_ID = "";

    /*SubColor Id*/
    public static String SUB_COLOR_ID = "";

    /*Color Id*/
    public static HashMap<String,String> CUST_COLOR_ID = new HashMap<>();

    /*Pattern Id*/
    public static String PATTERN_ID = "";

    /*Order Id*/
    public static String QUOTATION_ORDER_ID = "";

    /*Thickness Id*/
    public static String THICKNESS_ID = "";

    /*Thickness Id*/
    public static String THICKNESS_OLD_ID = "";

    /*Material Selection Available Count*/
    public static String MATERIAL_AVAILABLE_COUNT = "0";

    /*Delivery Type*/
    public static String DELIVERY_TYPE_ENG_NAME = "";

    /*Delivery Type Arabic Name*/
    public static String DELIVERY_TYPE_ARABIC_NAME = "";

    /*Measurement Slider Position*/
    public static int MEASUREMENT_SLIDER_POSITION = 0;

    /*FilterType*/
    public static String FILTER_TYPE = "FILTER_TYPE";

    /*CustomizationTheeDressTypeId*/
    public static String CUSTOMIZATION_DRESS_TYPE_ID = "";

    /*AddressForAddressScree*/
    public static String ADRRESS_STR = "ADRRESS_STR";

    /*Address Country*/
    public static String ADDRESS_COUNTRY = "";

    /*Refresh Badge*/
    public static String BOTTOM_BADGE_REFRESH = "";

    /*Address State*/
    public static String ADDRESS_STATE = "";

    /*Address Area*/
    public static String ADDRESS_AREA = "";
    /*Lat*/
    public static String LAT_STR = "";

    /*Long*/
    public static String LONG_STR = "";

    /*Selected Lat*/
    public static String SELECTED_LAT_STR = "";

    /*Selected Long*/
    public static String SELECTED_LONG_STR = "";

    /*Measurement two*/
    public static String MEASUREMENT_ONE = "";

    /*Order Type*/
    public static String ORDER_TYPE = "";

    /*Order Type Name*/
    public static String ORDER_TYPE_NAME = "";

    /*Order Type Arabic Name*/
    public static String ORDER_TYPE_ARABIC_NAME = "";

    /*Order Summary*/
    public static String ORDER_SUMMARY = "";

    /*Service type*/
    public static String SERVICE_TYPE_NAME = "";
    public static String SERVICE_TYPE_NAME_ENG = "";

    /*Order Details on Back*/
    public static String ORDER_DETAILS_ON_BACK = "";

    /*CountryCode*/
    public static String CountryCode = "CountryCode";

    /*CountryCodeImg*/
    public static String COUNTRY_CODE_ID = "COUNTRY_CODE_ID";

    /*State Code Id*/
    public static String COUNTRY_STATE_ID = "";

    /*Area Code Id*/
    public static String COUNTRY_AREA_ID = "1";

    /*CheckAddAddressScree*/
    public static String CHECK_ADD_ADDRESS = "CHECK_ADD_ADDRESS";

    /*Add Referene and Mateial*/
    public static String ADD_MATERIAL = "";

    /*Address  onBack*/
    public static String ADDRESS_ON_BACK = "";

    /*USER_DETAILS*/
    public static String USER_DETAILS = "USER_DETAILS";

    /*Store Recent Products*/
    public static String STORE_RECENT_PRODUCT = "STORE_RECENT_PRODUCT";

    /*Store Search History*/
    public static String STORE_SEARCH_HISTORY = "STORE_SEARCH_HISTORY";

    /*Store Search Failed History*/
    public static String STORE_SEARCHED_PRODUCTS = "STORE_SEARCHED_PRODUCTS";

    /*PAYMENT DETAILS*/
    public static String PAYMENT_DETAILS = "PAYMENT_DETAILS";

    /*Selected Image*/
    public static String SelectedPartsImage = "SelectedPartsImage";

    /*Selected name*/
    public static String SelectedPartsName = "SelectedPartsName";

    /*Selected Id*/
    public static String SelectedPartsId = "SelectedPartsId";

    /*quotation list Id*/
    public static String QUOTATION_LIST_ID = "";

    /*order request list Id*/
    public static String REQUEST_LIST_ID = "";

    /*appointment list id*/
    public static String APPOINTMENT_LIST_ID = "";

    /*appointment id*/
    public static String APPOINTMENT_MATERIAL_ID = "";

    /*appointment id*/
    public static String APPOINTMENT_MEASUREMENT_ID = "";

    /*Selected Edit Value*/
    public static String SelectedPartsEditValue = "";

    /*Order pending list Id*/
    public static String ORDER_PENDING_ID = "";

    /*Order Details Pending Id*/
    public static String ORDER_DETAILS_PENDING_ID  = "";

    /*Order Line Id*/
    public static String ORDER_PENDING_LINE_ID = "";

    /*Tailor Id*/
    public static String TAILOR_ID = "";

    /*Check Service */
    public static String SERVICE_TYPE  = "";

    /*Edit Address*/
    public static String EDIT_ADDRESS = "";

    /*GET_ADDRESS_ENTITY*/
    public static String GET_ADDRESS_ID = "";

    /*GET ADDRESS*/
    public static String GET_ADDRESS = "";

    /*Filter flag*/
    public static String FILTER_FLAG = "";

    /*Measurement Scale*/
    public static String MEASUREMENT_SCALE = "";

    /*Measurement Scale Meter*/
    public static  String MEASUREMENT_LENGTH = "";

    /*Filter Gender Old*/
    public static String  OLD_FILTER_GENDER = "";

    /*Filter Occasion Old*/
    public static String OLD_FILTER_OCCASION = "";

    /*Filter Region Old*/
    public static String OLD_FILTER_REGION = "";

    /*Filter Gender New*/
    public static String  NEW_FILTER_GENDER = "";

    /*Filter Occasion New*/
    public static String NEW_FILTER_OCCASION = "";

    /*Filter Region New*/
    public static String NEW_FILTER_REGION = "";

    /*Check book appointment*/
     public static String CHECK_BOOK_APPOINTMENT = "";

     /*Approve Reject Button*/
    public static String APPROVE_REJECT_BTN = "";

     /*Approved TailorId*/
    public static String APPROVED_TAILOR_ID = "";

    /*Pending Delivery Click*/
    public static String PENDING_DELIVERY_CLICK = "";

    /*Add New Measurement List*/
    public static String ADD_NEW_MEASUREMENT = "NEW_ORDER";

    /*Direct Order*/
    public static String DIRECT_ORDER = "";

    /*OrderSubType*/
    public static String ORDER_SUB_TYPE = "";

    /*Order Type*/
    public static String DIRECT_ORDER_TYPE = "";

    /*Measurement Type*/
    public static String DIRECT_MEASUREMENT_TYPE = "";

    /*Quantity*/
    public static String UPDATE_QUANTITY = "";

    /*Date and Time Restrict*/
    public static String DATE_FOR_RESTRICT_MEASUREMENT = "";

    /*Date and Time Restrict*/
    public static String DATE_FOR_RESTRICT_MATERIAL = "";

    /*Date and Time Restrict*/
    public static String DATE_FOR_RESTRICT_SCHEDULE = "";
    public static String NO_OF_DAYS_FOR_SCHEDULE = "0";

    /*Order Detail Skill Type*/
    public static String ORDER_TYPE_SKILL = "";

    /*View Details Header*/
    public static String VIEW_DETAILS_HEADER = "";

    /*Load Store or Related*/
    public static String LOAD_STORE_WEB = "";

    /*Add Comment*/
     public static String ADD_COMMENTS  = "";

     /*Get Card Id*/
    public static String CARD_ID = "CARD_ID";

    /*Related Product Id*/
    public static String RELATED_PROD_ID = "";

    /*Related Product Name*/
    public static String RELATED_PROD_NAME = "";

    /*Check Out Store*/
    public static String CHECK_OUT_ORDER = "";

    /*Store Id*/
    public static String STORE_ID = "";

    /*Store Search Txt*/
    public static String STORE_SEARCH_TXT = "";

    /*Store Search*/
    public static String STORE_SEARCH = "";


    /*Measurement Seek Max*/
    public static String SEEK_MAX = "100";

    /*Measurement Seek Min*/
    public static String SEEK_MIN = "0";

    /*Material Review*/
    public static String MATERIAL_REVIEW = "";

    /*View Image Details*/
    public static ArrayList<String> VIEW_IMAGE_LIST = new ArrayList<>();

    /*Store Order Details*/
    public static String STORE_ORDER_DETAILS_PRODUCT_NAME = "";
    public static String STORE_ORDER_DETAILS_PRODUCT_IMG = "";

    /*Customization Selected*/
    public static ArrayList<String> CUSTOMIZATION_CLICKED_ARRAY = new ArrayList<>();

    /*Manully Array*/
    public static ArrayList<ManuallyEntity> MANULLY_LIST = new ArrayList<>();

    /*Filter Occasion Old List*/
    public static ArrayList<FilterTypeEntity> OLD_FILTER_OCCASION_LIST = new ArrayList<>();

    /*Filter Occasion New List*/
    public static ArrayList<FilterTypeEntity> NEW_FILTER_OCCASION_LIST = new ArrayList<>();

    /*Filter Region Old List*/
    public static ArrayList<FilterTypeEntity> OLD_FILTER_REGION_LIST = new ArrayList<>();

    /*Customization click arraylist*/
    public static ArrayList<CustomizationThreeClickEntity> CUSTOMIZATION_CLICK_ARRAY = new ArrayList<>();

    /*MeasurementParts*/
    public static ArrayList<GetMeasurementPartEntity> MEASUREMENT_VALUES = new ArrayList<>();

    /*Measurement Parts*/
    public static HashMap<String,String> MEASUREMENT_MAP = new HashMap<>();

    /*Filter Region New List*/
    public static ArrayList<FilterTypeEntity> NEW_FILTER_REGION_LIST = new ArrayList<>();

    /*Order Approval clicked Tailor list*/
    public static ArrayList<TailorListEntity> ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();

    /*Customization three imgaes*/
    public static ArrayList<CustomizationAttributesEntity> CUSTOMIZATION_ATTRIBUTE_LIST = new ArrayList<>();

    /*Tailor List for map*/
    public static ArrayList<TailorListEntity> TAILOR_LIST = new ArrayList<>();

    /*Offer gender list*/
    public static ArrayList<Integer> OFFER_GENDER_LIST = new ArrayList<>();

    /*Offer location list*/
    public static ArrayList<Integer> OFFER_LOCATION_LIST = new ArrayList<>();

    /*offer selected list*/
    public static ArrayList<Integer> OFFER_SELECTED_LIST = new ArrayList<>();

    /*Filter Occasion List Bool*/
    public static String OCCASTION_LIST_BOOL = "";

    /*Filter Region List Bool*/
    public static String REGION_LIST_BOOL = "";

    /*New Order*/
    public static String NEW_ORDER = "";

    /*lat and long*/
    public static double SHOP_LAT = 0;
    public static double SHOP_LONG = 0;

    public static double SHOP_OWN_LAT = 0;
    public static double SHOP_OWN_LONG = 0;

    public static double FILTER_KM = 0;
    public static int FILTER_NO_OF_ORDER = 0;
    public static double FILTER_RATING =0;
    public static ArrayList<Integer> FILTER_STATE_ID_LIST = new ArrayList<>();

    /*Customization three click*/
    public static String CUSTOMIZATION_NAME = "CUSTOMIZATION_NAME";

    /*Tracking Order Type*/
    public static String TRACKING_ORDER_TYPE = "";

    /*Offer Id*/
    public static String OFFERS_ID = "0";

    /*Offer Categories*/
    public static String OFFER_CATEGORIES_ID = "";

    /*Offer Categories*/
    public static String OFFER_CATEGORIES = "";

    /*Check Out Offer*/
    public static String CHECK_OUT_OFFERS = "";

    /*Offer Apply Code*/
    public static String TAP_TO_APPLY = "";

    /*Offer Code Apply Coupon Id*/
    public static String TAP_TO_APPLY_CODE_ID = "";

    /*Offer Apply Code*/
    public static String APPLY_CODE = "";

    /*Offer Apply Code Id*/
    public static String APPLY_CODE_ID = "";

    /*Redeem Points to cash*/
    public static String REDEEM_POINT_TO_CASH = "0";

    /*Redeem Points to points*/
    public static String REDEEM_POINT_TO_POINTS = "0";

    /*Apply code Amt*/
    public static String APPLY_CODE_AMT = "0";

    /*Sub Total Cash*/
    public static String SUB_TOTAL_CASH = "0";

    /*Redeem convert to points*/
    public static String REDEEM_SELECTED_REDEEM = "";

    /*Stitching Amount*/
    public static String STICTHING_AMT = "0";

    /*Store Amount*/
    public static String STORE_AMT = "0";

    /*Store Address*/
    public static PaymentInfoEntity STORE_USER_DETAILS = new PaymentInfoEntity();

    /*Store Review Rating*/
    public static ArrayList<GetStoreReviewEntity> STORE_REVIEW_RATING = new ArrayList<>();

    /*Store Payment*/
    public static String STORE_PAYMENT = "";

    /*Transaction Amount*/
    public static String TRANSACTION_AMOUNT = "0";

    /*Tailor list click*/
    public static String TAILOR_CLICK_NAME = "";
    public static String TAILOR_CLICK_ID = "";

    /*Get Order Details Customization*/
    public static String ORDER_DETAILS_CUSTOMIZATION = "";

    /*Get Order Details Measurement*/
    public static String ORDER_DETAILS_MEASUREMENT = "";

    /*Get View Details Response*/
    public static String VIEW_DETAILS_RES = "";

    /*Skip Now*/
    public static String SKIP_NOW ="";

    /*Order Details Initiated by*/
    public static String InitiatedBy = "";

    /*Reward And Offers*/
    public static String REWARDS_AND_OFFERS_BACK = "";

    /*Order Id*/
    public static String ORDER_ID = "";

    /*Store Banner Id*/
    public static String STORE_BANNER_ID = "1";

    /*Store Type Id*/
    public static String STORE_TYPE_ID = "1";

    /*Store Product Id*/
    public static String STORE_PRODUCT_ID = "";

    /*Store Seller Id*/
    public static String STORE_SELLER_ID = "";

    /*Store Size Id*/
    public static String STORE_SIZE_ID = "";

    /*Store Color Id*/
    public static String STORE_COLOR_ID = "";

    /*Store SubCatogeryId*/
    public static String STORE_CATOGERY_ID = "";

    /*Store SubCatogery And All Brands*/
    public static String STORE_BRANDS_AND_CATEGORY = "";

    /*Store Product Details Refresh*/
    public static String STORE_PRODUCT_REFRESH = "";

    /*Area Id*/
    public static String DELIVERY_AREA_ID = "";

    /*Stitching or Store Check*/
    public static String ORDER_TYPE_CHECK = "";

    /*CheckOut Screen Check*/
    public static String CART_SCREEN_CHECK = "";

    /*Tailor Id*/
    public static ArrayList<IdEntity> DELIVERY_TAILOR_ID = new ArrayList<>();

    /*Store Seller List*/
    public static ArrayList<GetProductSellerEntity> PRODUCT_SELLER_LIST = new ArrayList<>();

    /*Measurement Parts Measurement Details*/
    public static ArrayList<GetAppointmentLeftMeasurementEntity> GET_APPOINTMENT_LEFT_MEASUREMENT_ENTITY = new ArrayList<>();

    /*Measurement Parts Measurement Type*/
    public static String MEASUREMENT_PARTS_MEASUREMENT_TYPE = "";
    public static String MEASUREMENT_PARTS_MEASUREMENT_NAME_TYPE = "";
    public static String PAYMENT_STATUS = "";

    /*Material Rating Type*/
    public static String MATERIAL_TYPE = "";
    public static String MATERIAL_TYPE_ID = "";

    public static String MATERIAL_ONE_TYPE_NAME ="";
    public static String MATERIAL_ONE_TYPE_ARABIC ="";

    /*Scratch Card api Call count*/
    public static String SCRATCH_CARD_COUNT = "0";

    /*CheckOut Details*/
    public static String CHECK_OUT_MATERIAL_ID = "0";
    public static String CHECK_OUT_SERVICE_TYPE_ID = "0";
    public static String CHECK_OUT_MEASUREMENT_ID = "0";

    /*OrderSummary Details*/
    public static String GENDER_NAME = "";
    public static String GENDER_NAME_ARABIC = "";
    public static String DRESS_TYPE_NAME = "";
    public static String DRESS_SUB_TYPE_NAME = "";
    public static String SEASONAL_NAME = "";
    public static String PLACE_OF_INDUSTRY_NAME = "";
    public static String BRANDS_NAME = "";
    public static String MATERIAL_TYPE_NAME = "";
    public static String COLOUR_NAME = "";
    public static String PATTERN_NAME = "";
    public static String THICKNESS_NAME ="";
    public static String MEASUREMENT_MANUALLY = "";
    public static String ORDER_TYPE_ID = "";
    public static String MEASUREMENT_ID = "";
    public static String DELIVERY_TYPE_ID = "";
    public static String MEASUREMENT_TYPE = "";
    public static String DELIVERY_ID = "";
    public static ArrayList<String> MATERIAL_IMAGES = new ArrayList<>();
    public static ArrayList<String> REFERENCE_IMAGES = new ArrayList<>();
    public static ArrayList<StoreCartEntity> GET_ITEM_CARD_LIST = new ArrayList();

    public static String SEASONAL_OLD_NAME = "";
    public static String PLACE_OF_INDUSTRY_OLD_NAME = "";
    public static String BRANDS_OLD_NAME = "";
    public static String MATERIAL_TYPE_OLD_NAME = "";
    public static String THICKNESS_OLD_NAME ="";

    public static String UNITS = "CM";

    public static String MEN_1 = "";
    public static String MEN_2 = "";
    public static String MEN_3 = "";
    public static String MEN_4 = "";
    public static String MEN_5 = "";
    public static String MEN_6 = "";
    public static String MEN_7 = "";
    public static String MEN_8 = "";
    public static String MEN_9 = "";
    public static String MEN_10 = "";
    public static String MEN_11 = "";
    public static String MEN_12 = "";
    public static String MEN_13 = "";
    public static String MEN_14 = "";
    public static String MEN_15 = "";
    public static String MEN_16 = "";
    public static String MEN_17 = "";
    public static String MEN_18 = "";
    public static String MEN_19 = "";
    public static String MEN_20 = "";
    public static String MEN_21 = "";
    public static String MEN_22 = "";
    public static String MEN_23 = "";
    public static String MEN_24 = "";
    public static String MEN_25 = "";
    public static String MEN_26 = "";
    public static String MEN_27 = "";
    public static String MEN_28 = "";
    public static String MEN_29 = "";
    public static String MEN_30 = "";
    public static String MEN_31 = "";
    public static String MEN_32 = "";
    public static String MEN_33 = "";
    public static String MEN_34 = "";
    public static String MEN_35 = "";
    public static String MEN_36 = "";
    public static String MEN_37 = "";
    public static String MEN_38 = "";
    public static String MEN_39 = "";
    public static String MEN_40 = "";
    public static String MEN_41 = "";
    public static String MEN_42 = "";
    public static String MEN_43 = "";
    public static String MEN_44 = "";
    public static String MEN_45 = "";
    public static String MEN_46 = "";
    public static String MEN_47 = "";
    public static String MEN_48 = "";
    public static String MEN_49 = "";
    public static String MEN_50 = "";
    public static String MEN_51 = "";
    public static String MEN_52 = "";
    public static String MEN_53 = "";
    public static String MEN_54 = "";
    public static String MEN_55 = "";
    public static String MEN_56 = "";
    public static String MEN_57 = "";
    public static String MEN_58 = "";
    public static String MEN_59 = "";
    public static String MEN_60 = "";
    public static String MEN_61 = "";
    public static String MEN_62 = "";
    public static String MEN_63 = "";
    public static String MEN_64 = "";

    public static String WOMEN_65 = "";
    public static String WOMEN_66 = "";
    public static String WOMEN_67 = "";
    public static String WOMEN_68 = "";
    public static String WOMEN_69 = "";
    public static String WOMEN_70 = "";
    public static String WOMEN_71 = "";
    public static String WOMEN_72 = "";
    public static String WOMEN_73 = "";
    public static String WOMEN_74 = "";
    public static String WOMEN_75 = "";
    public static String WOMEN_76 = "";
    public static String WOMEN_77 = "";
    public static String WOMEN_78 = "";
    public static String WOMEN_79 = "";
    public static String WOMEN_80 = "";
    public static String WOMEN_81 = "";
    public static String WOMEN_82 = "";
    public static String WOMEN_83 = "";
    public static String WOMEN_84 = "";
    public static String WOMEN_85 = "";
    public static String WOMEN_86 = "";
    public static String WOMEN_87 = "";
    public static String WOMEN_88 = "";
    public static String WOMEN_89 = "";
    public static String WOMEN_90 = "";
    public static String WOMEN_91 = "";
    public static String WOMEN_92 = "";
    public static String WOMEN_93 = "";
    public static String WOMEN_94 = "";
    public static String WOMEN_95 = "";
    public static String WOMEN_96 = "";
    public static String WOMEN_97 = "";
    public static String WOMEN_98 = "";
    public static String WOMEN_99 = "";
    public static String WOMEN_100 = "";
    public static String WOMEN_101 = "";
    public static String WOMEN_102 = "";
    public static String WOMEN_103 = "";
    public static String WOMEN_104 = "";
    public static String WOMEN_105 = "";
    public static String WOMEN_106 = "";
    public static String WOMEN_107 = "";
    public static String WOMEN_108 = "";
    public static String WOMEN_109 = "";
    public static String WOMEN_110 = "";
    public static String WOMEN_111 = "";
    public static String WOMEN_112 = "";
    public static String WOMEN_113 = "";
    public static String WOMEN_114 = "";
    public static String WOMEN_115 = "";
    public static String WOMEN_116 = "";
    public static String WOMEN_117 = "";
    public static String WOMEN_118 = "";
    public static String WOMEN_119 = "";
    public static String WOMEN_120 = "";
    public static String WOMEN_121 = "";
    public static String WOMEN_122 = "";
    public static String WOMEN_123 = "";
    public static String WOMEN_124 = "";
    public static String WOMEN_125 = "";
    public static String WOMEN_126 = "";
    public static String WOMEN_127 = "";
    public static String WOMEN_128 = "";
    public static String WOMEN_129 = "";
    public static String WOMEN_130 = "";
    public static String WOMEN_131 = "";
    public static String WOMEN_132 = "";
    public static String WOMEN_133 = "";
    public static String WOMEN_134 = "";
    public static String WOMEN_135 = "";
    public static String WOMEN_136 = "";
    public static String WOMEN_137 = "";
    public static String WOMEN_138 = "";
    public static String WOMEN_139 = "";
    public static String WOMEN_140 = "";
    public static String WOMEN_141 = "";
    public static String WOMEN_142 = "";
    public static String WOMEN_143 = "";
    public static String WOMEN_144 = "";

    public static String BOY_145 = "";
    public static String BOY_146 = "";
    public static String BOY_147 = "";
    public static String BOY_148 = "";
    public static String BOY_149 = "";
    public static String BOY_150 = "";
    public static String BOY_151 = "";
    public static String BOY_152 = "";
    public static String BOY_153 = "";
    public static String BOY_154 = "";
    public static String BOY_155 = "";
    public static String BOY_156 = "";
    public static String BOY_157 = "";
    public static String BOY_158 = "";
    public static String BOY_159 = "";
    public static String BOY_160 = "";
    public static String BOY_161 = "";
    public static String BOY_162 = "";
    public static String BOY_163 = "";
    public static String BOY_164 = "";
    public static String BOY_165 = "";
    public static String BOY_166 = "";
    public static String BOY_167 = "";
    public static String BOY_168 = "";
    public static String BOY_169 = "";
    public static String BOY_170 = "";
    public static String BOY_171 = "";
    public static String BOY_172 = "";
    public static String BOY_173 = "";
    public static String BOY_174 = "";
    public static String BOY_175 = "";
    public static String BOY_176 = "";
    public static String BOY_177 = "";
    public static String BOY_178 = "";
    public static String BOY_179 = "";
    public static String BOY_180 = "";
    public static String BOY_181 = "";
    public static String BOY_182 = "";
    public static String BOY_183 = "";
    public static String BOY_184 = "";
    public static String BOY_185 = "";
    public static String BOY_186 = "";
    public static String BOY_187 = "";
    public static String BOY_188 = "";
    public static String BOY_189 = "";
    public static String BOY_190 = "";
    public static String BOY_191 = "";
    public static String BOY_192 = "";
    public static String BOY_193 = "";
    public static String BOY_194 = "";
    public static String BOY_195 = "";
    public static String BOY_196 = "";
    public static String BOY_197 = "";
    public static String BOY_198 = "";
    public static String BOY_199 = "";
    public static String BOY_200 = "";
    public static String BOY_201 = "";
    public static String BOY_202 = "";
    public static String BOY_203 = "";
    public static String BOY_204 = "";
    public static String BOY_205 = "";
    public static String BOY_206 = "";
    public static String BOY_207 = "";
    public static String BOY_208 = "";

    public static String GIRL_209 = "";
    public static String GIRL_210 = "";
    public static String GIRL_211 = "";
    public static String GIRL_212 = "";
    public static String GIRL_213 = "";
    public static String GIRL_214 = "";
    public static String GIRL_215 = "";
    public static String GIRL_216 = "";
    public static String GIRL_217 = "";
    public static String GIRL_218 = "";
    public static String GIRL_219 = "";
    public static String GIRL_220 = "";
    public static String GIRL_221 = "";
    public static String GIRL_222 = "";
    public static String GIRL_223 = "";
    public static String GIRL_224 = "";
    public static String GIRL_225 = "";
    public static String GIRL_226 = "";
    public static String GIRL_227 = "";
    public static String GIRL_228 = "";
    public static String GIRL_229 = "";
    public static String GIRL_230 = "";
    public static String GIRL_231 = "";
    public static String GIRL_232 = "";
    public static String GIRL_233 = "";
    public static String GIRL_234 = "";
    public static String GIRL_235 = "";
    public static String GIRL_236 = "";
    public static String GIRL_237 = "";
    public static String GIRL_238 = "";
    public static String GIRL_239 = "";
    public static String GIRL_240 = "";
    public static String GIRL_241 = "";
    public static String GIRL_242 = "";
    public static String GIRL_243 = "";
    public static String GIRL_244 = "";
    public static String GIRL_245 = "";
    public static String GIRL_246 = "";
    public static String GIRL_247 = "";
    public static String GIRL_248 = "";
    public static String GIRL_249 = "";
    public static String GIRL_250 = "";
    public static String GIRL_251 = "";
    public static String GIRL_252 = "";
    public static String GIRL_253 = "";
    public static String GIRL_254 = "";
    public static String GIRL_255 = "";
    public static String GIRL_256 = "";
    public static String GIRL_257 = "";
    public static String GIRL_258 = "";
    public static String GIRL_259 = "";
    public static String GIRL_260 = "";
    public static String GIRL_261 = "";
    public static String GIRL_262 = "";
    public static String GIRL_263 = "";
    public static String GIRL_264 = "";
    public static String GIRL_265 = "";
    public static String GIRL_266 = "";
    public static String GIRL_267 = "";
    public static String GIRL_268 = "";
    public static String GIRL_269 = "";
    public static String GIRL_270 = "";
    public static String GIRL_271 = "";
    public static String GIRL_272 = "";
    public static String GIRL_273 = "";
    public static String GIRL_274 = "";
    public static String GIRL_275 = "";
    public static String GIRL_276 = "";
    public static String GIRL_277 = "";
    public static String GIRL_278 = "";
    public static String GIRL_279 = "";
    public static String GIRL_280 = "";
    public static String GIRL_281 = "";
    public static String GIRL_282 = "";
    public static String GIRL_283 = "";
    public static String GIRL_284 = "";
    public static String GIRL_285 = "";
    public static String GIRL_286 = "";
    public static String GIRL_287 = "";
    public static String GIRL_288 = "";

    public static String MEN_1_INCHES = "";
    public static String MEN_2_INCHES = "";
    public static String MEN_3_INCHES = "";
    public static String MEN_4_INCHES = "";
    public static String MEN_5_INCHES = "";
    public static String MEN_6_INCHES = "";
    public static String MEN_7_INCHES = "";
    public static String MEN_8_INCHES = "";
    public static String MEN_9_INCHES = "";
    public static String MEN_10_INCHES = "";
    public static String MEN_11_INCHES = "";
    public static String MEN_12_INCHES = "";
    public static String MEN_13_INCHES = "";
    public static String MEN_14_INCHES = "";
    public static String MEN_15_INCHES = "";
    public static String MEN_16_INCHES = "";
    public static String MEN_17_INCHES = "";
    public static String MEN_18_INCHES = "";
    public static String MEN_19_INCHES = "";
    public static String MEN_20_INCHES = "";
    public static String MEN_21_INCHES = "";
    public static String MEN_22_INCHES = "";
    public static String MEN_23_INCHES = "";
    public static String MEN_24_INCHES = "";
    public static String MEN_25_INCHES = "";
    public static String MEN_26_INCHES = "";
    public static String MEN_27_INCHES = "";
    public static String MEN_28_INCHES = "";
    public static String MEN_29_INCHES = "";
    public static String MEN_30_INCHES = "";
    public static String MEN_31_INCHES = "";
    public static String MEN_32_INCHES = "";
    public static String MEN_33_INCHES = "";
    public static String MEN_34_INCHES = "";
    public static String MEN_35_INCHES = "";
    public static String MEN_36_INCHES = "";
    public static String MEN_37_INCHES = "";
    public static String MEN_38_INCHES = "";
    public static String MEN_39_INCHES = "";
    public static String MEN_40_INCHES = "";
    public static String MEN_41_INCHES = "";
    public static String MEN_42_INCHES = "";
    public static String MEN_43_INCHES = "";
    public static String MEN_44_INCHES = "";
    public static String MEN_45_INCHES = "";
    public static String MEN_46_INCHES = "";
    public static String MEN_47_INCHES = "";
    public static String MEN_48_INCHES = "";
    public static String MEN_49_INCHES = "";
    public static String MEN_50_INCHES = "";
    public static String MEN_51_INCHES = "";
    public static String MEN_52_INCHES = "";
    public static String MEN_53_INCHES = "";
    public static String MEN_54_INCHES = "";
    public static String MEN_55_INCHES = "";
    public static String MEN_56_INCHES = "";
    public static String MEN_57_INCHES = "";
    public static String MEN_58_INCHES = "";
    public static String MEN_59_INCHES = "";
    public static String MEN_60_INCHES = "";
    public static String MEN_61_INCHES = "";
    public static String MEN_62_INCHES = "";
    public static String MEN_63_INCHES = "";
    public static String MEN_64_INCHES = "";

    public static String WOMEN_65_INCHES = "";
    public static String WOMEN_66_INCHES = "";
    public static String WOMEN_67_INCHES = "";
    public static String WOMEN_68_INCHES = "";
    public static String WOMEN_69_INCHES = "";
    public static String WOMEN_70_INCHES = "";
    public static String WOMEN_71_INCHES = "";
    public static String WOMEN_72_INCHES = "";
    public static String WOMEN_73_INCHES = "";
    public static String WOMEN_74_INCHES = "";
    public static String WOMEN_75_INCHES = "";
    public static String WOMEN_76_INCHES = "";
    public static String WOMEN_77_INCHES = "";
    public static String WOMEN_78_INCHES = "";
    public static String WOMEN_79_INCHES = "";
    public static String WOMEN_80_INCHES = "";
    public static String WOMEN_81_INCHES = "";
    public static String WOMEN_82_INCHES = "";
    public static String WOMEN_83_INCHES = "";
    public static String WOMEN_84_INCHES = "";
    public static String WOMEN_85_INCHES = "";
    public static String WOMEN_86_INCHES = "";
    public static String WOMEN_87_INCHES = "";
    public static String WOMEN_88_INCHES = "";
    public static String WOMEN_89_INCHES = "";
    public static String WOMEN_90_INCHES = "";
    public static String WOMEN_91_INCHES = "";
    public static String WOMEN_92_INCHES = "";
    public static String WOMEN_93_INCHES = "";
    public static String WOMEN_94_INCHES = "";
    public static String WOMEN_95_INCHES = "";
    public static String WOMEN_96_INCHES = "";
    public static String WOMEN_97_INCHES = "";
    public static String WOMEN_98_INCHES = "";
    public static String WOMEN_99_INCHES = "";
    public static String WOMEN_100_INCHES = "";
    public static String WOMEN_101_INCHES = "";
    public static String WOMEN_102_INCHES = "";
    public static String WOMEN_103_INCHES = "";
    public static String WOMEN_104_INCHES = "";
    public static String WOMEN_105_INCHES = "";
    public static String WOMEN_106_INCHES = "";
    public static String WOMEN_107_INCHES = "";
    public static String WOMEN_108_INCHES = "";
    public static String WOMEN_109_INCHES = "";
    public static String WOMEN_110_INCHES = "";
    public static String WOMEN_111_INCHES = "";
    public static String WOMEN_112_INCHES = "";
    public static String WOMEN_113_INCHES = "";
    public static String WOMEN_114_INCHES = "";
    public static String WOMEN_115_INCHES = "";
    public static String WOMEN_116_INCHES = "";
    public static String WOMEN_117_INCHES = "";
    public static String WOMEN_118_INCHES = "";
    public static String WOMEN_119_INCHES = "";
    public static String WOMEN_120_INCHES = "";
    public static String WOMEN_121_INCHES = "";
    public static String WOMEN_122_INCHES = "";
    public static String WOMEN_123_INCHES = "";
    public static String WOMEN_124_INCHES = "";
    public static String WOMEN_125_INCHES = "";
    public static String WOMEN_126_INCHES = "";
    public static String WOMEN_127_INCHES = "";
    public static String WOMEN_128_INCHES = "";
    public static String WOMEN_129_INCHES = "";
    public static String WOMEN_130_INCHES = "";
    public static String WOMEN_131_INCHES = "";
    public static String WOMEN_132_INCHES = "";
    public static String WOMEN_133_INCHES = "";
    public static String WOMEN_134_INCHES = "";
    public static String WOMEN_135_INCHES = "";
    public static String WOMEN_136_INCHES = "";
    public static String WOMEN_137_INCHES = "";
    public static String WOMEN_138_INCHES = "";
    public static String WOMEN_139_INCHES = "";
    public static String WOMEN_140_INCHES = "";
    public static String WOMEN_141_INCHES = "";
    public static String WOMEN_142_INCHES = "";
    public static String WOMEN_143_INCHES = "";
    public static String WOMEN_144_INCHES = "";

    public static String BOY_145_INCHES = "";
    public static String BOY_146_INCHES = "";
    public static String BOY_147_INCHES = "";
    public static String BOY_148_INCHES = "";
    public static String BOY_149_INCHES = "";
    public static String BOY_150_INCHES = "";
    public static String BOY_151_INCHES = "";
    public static String BOY_152_INCHES = "";
    public static String BOY_153_INCHES = "";
    public static String BOY_154_INCHES = "";
    public static String BOY_155_INCHES = "";
    public static String BOY_156_INCHES = "";
    public static String BOY_157_INCHES = "";
    public static String BOY_158_INCHES = "";
    public static String BOY_159_INCHES = "";
    public static String BOY_160_INCHES = "";
    public static String BOY_161_INCHES = "";
    public static String BOY_162_INCHES = "";
    public static String BOY_163_INCHES = "";
    public static String BOY_164_INCHES = "";
    public static String BOY_165_INCHES = "";
    public static String BOY_166_INCHES = "";
    public static String BOY_167_INCHES = "";
    public static String BOY_168_INCHES = "";
    public static String BOY_169_INCHES = "";
    public static String BOY_170_INCHES = "";
    public static String BOY_171_INCHES = "";
    public static String BOY_172_INCHES = "";
    public static String BOY_173_INCHES = "";
    public static String BOY_174_INCHES = "";
    public static String BOY_175_INCHES = "";
    public static String BOY_176_INCHES = "";
    public static String BOY_177_INCHES = "";
    public static String BOY_178_INCHES = "";
    public static String BOY_179_INCHES = "";
    public static String BOY_180_INCHES = "";
    public static String BOY_181_INCHES = "";
    public static String BOY_182_INCHES = "";
    public static String BOY_183_INCHES = "";
    public static String BOY_184_INCHES = "";
    public static String BOY_185_INCHES = "";
    public static String BOY_186_INCHES = "";
    public static String BOY_187_INCHES = "";
    public static String BOY_188_INCHES = "";
    public static String BOY_189_INCHES = "";
    public static String BOY_190_INCHES = "";
    public static String BOY_191_INCHES = "";
    public static String BOY_192_INCHES = "";
    public static String BOY_193_INCHES = "";
    public static String BOY_194_INCHES = "";
    public static String BOY_195_INCHES = "";
    public static String BOY_196_INCHES = "";
    public static String BOY_197_INCHES = "";
    public static String BOY_198_INCHES = "";
    public static String BOY_199_INCHES = "";
    public static String BOY_200_INCHES = "";
    public static String BOY_201_INCHES = "";
    public static String BOY_202_INCHES = "";
    public static String BOY_203_INCHES = "";
    public static String BOY_204_INCHES = "";
    public static String BOY_205_INCHES = "";
    public static String BOY_206_INCHES = "";
    public static String BOY_207_INCHES = "";
    public static String BOY_208_INCHES = "";

    public static String GIRL_209_INCHES = "";
    public static String GIRL_210_INCHES = "";
    public static String GIRL_211_INCHES = "";
    public static String GIRL_212_INCHES = "";
    public static String GIRL_213_INCHES = "";
    public static String GIRL_214_INCHES = "";
    public static String GIRL_215_INCHES = "";
    public static String GIRL_216_INCHES = "";
    public static String GIRL_217_INCHES = "";
    public static String GIRL_218_INCHES = "";
    public static String GIRL_219_INCHES = "";
    public static String GIRL_220_INCHES = "";
    public static String GIRL_221_INCHES = "";
    public static String GIRL_222_INCHES = "";
    public static String GIRL_223_INCHES = "";
    public static String GIRL_224_INCHES = "";
    public static String GIRL_225_INCHES = "";
    public static String GIRL_226_INCHES = "";
    public static String GIRL_227_INCHES = "";
    public static String GIRL_228_INCHES = "";
    public static String GIRL_229_INCHES = "";
    public static String GIRL_230_INCHES = "";
    public static String GIRL_231_INCHES = "";
    public static String GIRL_232_INCHES = "";
    public static String GIRL_233_INCHES = "";
    public static String GIRL_234_INCHES = "";
    public static String GIRL_235_INCHES = "";
    public static String GIRL_236_INCHES = "";
    public static String GIRL_237_INCHES = "";
    public static String GIRL_238_INCHES = "";
    public static String GIRL_239_INCHES = "";
    public static String GIRL_240_INCHES = "";
    public static String GIRL_241_INCHES = "";
    public static String GIRL_242_INCHES = "";
    public static String GIRL_243_INCHES = "";
    public static String GIRL_244_INCHES = "";
    public static String GIRL_245_INCHES = "";
    public static String GIRL_246_INCHES = "";
    public static String GIRL_247_INCHES = "";
    public static String GIRL_248_INCHES = "";
    public static String GIRL_249_INCHES = "";
    public static String GIRL_250_INCHES = "";
    public static String GIRL_251_INCHES = "";
    public static String GIRL_252_INCHES = "";
    public static String GIRL_253_INCHES = "";
    public static String GIRL_254_INCHES = "";
    public static String GIRL_255_INCHES = "";
    public static String GIRL_256_INCHES = "";
    public static String GIRL_257_INCHES = "";
    public static String GIRL_258_INCHES = "";
    public static String GIRL_259_INCHES = "";
    public static String GIRL_260_INCHES = "";
    public static String GIRL_261_INCHES = "";
    public static String GIRL_262_INCHES = "";
    public static String GIRL_263_INCHES = "";
    public static String GIRL_264_INCHES = "";
    public static String GIRL_265_INCHES = "";
    public static String GIRL_266_INCHES = "";
    public static String GIRL_267_INCHES = "";
    public static String GIRL_268_INCHES = "";
    public static String GIRL_269_INCHES = "";
    public static String GIRL_270_INCHES = "";
    public static String GIRL_271_INCHES = "";
    public static String GIRL_272_INCHES = "";
    public static String GIRL_273_INCHES = "";
    public static String GIRL_274_INCHES = "";
    public static String GIRL_275_INCHES = "";
    public static String GIRL_276_INCHES = "";
    public static String GIRL_277_INCHES = "";
    public static String GIRL_278_INCHES = "";
    public static String GIRL_279_INCHES = "";
    public static String GIRL_280_INCHES = "";
    public static String GIRL_281_INCHES = "";
    public static String GIRL_282_INCHES = "";
    public static String GIRL_283_INCHES = "";
    public static String GIRL_284_INCHES = "";
    public static String GIRL_285_INCHES = "";
    public static String GIRL_286_INCHES = "";
    public static String GIRL_287_INCHES = "";
    public static String GIRL_288_INCHES = "";
}

