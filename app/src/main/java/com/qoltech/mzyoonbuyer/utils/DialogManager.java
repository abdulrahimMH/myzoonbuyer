package com.qoltech.mzyoonbuyer.utils;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.GetCountryAdapter;
import com.qoltech.mzyoonbuyer.entity.GetCountryEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;

import java.util.ArrayList;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;


public class DialogManager {

    /*Init variable*/
    private Dialog mProgressDialog, mNetworkErrorDialog, mAlertDialog,
            mOptionDialog, mImageUploadDialog, mOfferSortDialog,mShowPaymentSuccessDialog,mShowPaymentFailureDialog;
    private Toast mToast;

    /*Init dialog instance*/
    private static final DialogManager sDialogInstance = new DialogManager();

    public static DialogManager getInstance() {
        return sDialogInstance;
    }

    /*Init toast*/
    public void showToast(Context context, String message) {

        try {
            /*To check if the toast is projected, if projected it will be cancelled */
            if (mToast != null && mToast.getView().isShown()) {
                mToast.cancel();
            }

            mToast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
            TextView toastTxt = mToast.getView().findViewById(
                    android.R.id.message);
            toastTxt.setTypeface(Typeface.SANS_SERIF);
            mToast.show();


        } catch (Exception e) {
            Log.e(AppConstants.TAG, e.toString());
        }
    }

    /*Default dialog init method*/
    private Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }

    public void showAlertPopup(Context context, String messageStr, final InterfaceBtnCallBack dialogAlertInterface) {

        alertDismiss(mAlertDialog);
        mAlertDialog = getDialog(context, R.layout.pop_up_basic_alert);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mAlertDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        TextView alertMsgTxt;
        Button alertPositiveBtn;

        /*Init view*/
        alertMsgTxt = mAlertDialog.findViewById(R.id.alert_msg_txt);
        alertPositiveBtn = mAlertDialog.findViewById(R.id.alert_positive_btn);

        /*Set data*/
        alertMsgTxt.setText(messageStr);
        alertPositiveBtn.setText(context.getString(R.string.ok));

        //Check and set button visibility
        alertPositiveBtn.setVisibility(View.VISIBLE);

        alertPositiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialog.dismiss();
                dialogAlertInterface.onPositiveClick();
            }
        });

        alertShowing(mAlertDialog);


    }


    public void showSuccessPopup(Context context, final InterfaceBtnCallBack dialogAlertInterface) {

        alertDismiss(mAlertDialog);
        mAlertDialog = getDialog(context, R.layout.pop_up_sucess);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mAlertDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        Button alertPositiveBtn;

        /*Init view*/
        alertPositiveBtn = mAlertDialog.findViewById(R.id.popup_success_ok);

        /*Set data*/
        alertPositiveBtn.setText(context.getString(R.string.ok));

        //Check and set button visibility
        alertPositiveBtn.setVisibility(View.VISIBLE);

        alertPositiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialog.dismiss();
                dialogAlertInterface.onPositiveClick();
            }
        });

        alertShowing(mAlertDialog);


    }



    public void showNetworkErrorPopup(Context context, final InterfaceBtnCallBack dialogAlertInterface) {

        alertDismiss(mNetworkErrorDialog);
        context = (context == null ? getContext() : context);

        mNetworkErrorDialog = getDialog(context, R.layout.pop_up_network_alert);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = mNetworkErrorDialog.getWindow();

        if (window != null) {
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            window.setGravity(Gravity.BOTTOM);
            window.getAttributes().windowAnimations = R.style.PopupBottomAnimation;
        }

        Button retryBtn;

        //Init View
        retryBtn = mNetworkErrorDialog.findViewById(R.id.retry_btn);


        retryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNetworkErrorDialog.dismiss();
                dialogAlertInterface.onPositiveClick();
            }
        });

        alertShowing(mNetworkErrorDialog);
    }


    public void showOptionPopup(Context context, String message, String firstBtnName, String secondBtnName,String language,
                                final InterfaceTwoBtnCallBack dialogAlertInterface) {
        alertDismiss(mOptionDialog);
        mOptionDialog = getDialog(context, R.layout.pop_up_basic_alert);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mOptionDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (language.equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mOptionDialog.findViewById(R.id.pop_up_basic_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(mOptionDialog.findViewById(R.id.pop_up_basic_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        TextView msgTxt;
        Button firstBtn, secondBtn;
        View view;
//        View view;

        /*Init view*/
        view = mOptionDialog.findViewById(R.id.popup_view);
        msgTxt = mOptionDialog.findViewById(R.id.alert_msg_txt);
        firstBtn = mOptionDialog.findViewById(R.id.alert_negative_btn);
        secondBtn = mOptionDialog.findViewById(R.id.alert_positive_btn);

        /*Set data*/
        msgTxt.setText(message);
        firstBtn.setText(firstBtnName);
        secondBtn.setText(secondBtnName);

        if (language.equalsIgnoreCase("Arabic")) {
            firstBtn.setText("نعم فعلا");
            secondBtn.setText("لا");
            msgTxt.setText("هل أنت متأكد أنك تريد العودة؟");
        }

        view.setVisibility(View.VISIBLE);
        firstBtn.setVisibility(View.VISIBLE);

        firstBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOptionDialog.dismiss();
                dialogAlertInterface.onPositiveClick();
            }
        });
        secondBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOptionDialog.dismiss();
                dialogAlertInterface.onNegativeClick();
            }
        });

        alertShowing(mOptionDialog);

    }


    public void showPaymentSuccessPopup(Context context, String orderIdTxtStr, String amtTxtStr, String transactionIdTxtStr, String dateTxtStr, String paymentSmsTxtStr,String language, final InterfaceBtnCallBack dialogAlertInterface) {

        alertDismiss(mShowPaymentSuccessDialog);
        mShowPaymentSuccessDialog = getDialog(context, R.layout.pop_up_payment_success);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mShowPaymentSuccessDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (language.equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mShowPaymentSuccessDialog.findViewById(R.id.pop_up_payment_success_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(mShowPaymentSuccessDialog.findViewById(R.id.pop_up_payment_success_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        ImageView homeImg;
        pl.droidsonroids.gif.GifImageView mImageGif;
        TextView orderIdTxt,amtTxt,transactionIdTxt,dateTxt,paymentSmsTxt,mAmountPaidByPointsTxt,mDiscountTxt,mTotalPointsDeductionTxt,mAmoutPaidByCardTxt;

        /*Init view*/
        homeImg = mShowPaymentSuccessDialog.findViewById(R.id.pop_up_payment_success_home_img);
        orderIdTxt = mShowPaymentSuccessDialog.findViewById(R.id.pop_up_payment_order_id_txt);
        amtTxt = mShowPaymentSuccessDialog.findViewById(R.id.pop_up_payment_amount_txt);
        transactionIdTxt = mShowPaymentSuccessDialog.findViewById(R.id.pop_up_payment_transaction_id_txt);
        dateTxt = mShowPaymentSuccessDialog.findViewById(R.id.pop_up_payment_date_txt);
        paymentSmsTxt = mShowPaymentSuccessDialog.findViewById(R.id.pop_up_sent_to_sms_txt);
        mAmountPaidByPointsTxt = mShowPaymentSuccessDialog.findViewById(R.id.pop_up_payment_amount_paid_by_points_txt);
        mDiscountTxt = mShowPaymentSuccessDialog.findViewById(R.id.pop_up_payment_discount_txt);
        mTotalPointsDeductionTxt = mShowPaymentSuccessDialog.findViewById(R.id.pop_up_payment_total_point_deduction_txt);
        mAmoutPaidByCardTxt = mShowPaymentSuccessDialog.findViewById(R.id.pop_up_payment_amount_paid_by_card_txt);
        mImageGif = mShowPaymentSuccessDialog.findViewById(R.id.pop_up_success_gif);

        if (language.equalsIgnoreCase("Arabic")){
            try {
                Glide.with(context)
                        .load(R.drawable.payment_success_arabic_gif)
                        .into(mImageGif);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }

        }else {
            try {
                Glide.with(context)
                        .load(R.drawable.payment_success_gif)
                        .into(mImageGif);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
        }

        /*Set data*/
        orderIdTxt.setText(orderIdTxtStr);
        transactionIdTxt.setText(transactionIdTxtStr);
        dateTxt.setText(dateTxtStr);
        paymentSmsTxt.setText(paymentSmsTxtStr);


        if (!AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")&&!AppConstants.APPLY_CODE.equalsIgnoreCase("")){
            amtTxt.setText(AppConstants.SUB_TOTAL_CASH+" "+context.getResources().getString(R.string.aed));
            mAmoutPaidByCardTxt.setText(amtTxtStr+" "+context.getResources().getString(R.string.aed));
            mTotalPointsDeductionTxt.setText(AppConstants.REDEEM_POINT_TO_POINTS+" "+context.getResources().getString(R.string.points));
            mAmountPaidByPointsTxt.setText(AppConstants.REDEEM_POINT_TO_CASH+" "+context.getResources().getString(R.string.aed));
            mDiscountTxt.setText(AppConstants.APPLY_CODE_AMT+" "+context.getResources().getString(R.string.aed));

        }else if (!AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")&&AppConstants.APPLY_CODE.equalsIgnoreCase("")){
            amtTxt.setText(AppConstants.SUB_TOTAL_CASH+" "+context.getResources().getString(R.string.aed));
            mAmoutPaidByCardTxt.setText(amtTxtStr+" "+context.getResources().getString(R.string.aed));
            mTotalPointsDeductionTxt.setText(AppConstants.REDEEM_POINT_TO_POINTS+" "+context.getResources().getString(R.string.points));
            mAmountPaidByPointsTxt.setText(AppConstants.REDEEM_POINT_TO_CASH+" "+context.getResources().getString(R.string.aed));
            mDiscountTxt.setText("0"+" "+context.getResources().getString(R.string.aed));

        }else if (AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")&&!AppConstants.APPLY_CODE.equalsIgnoreCase("")){
            amtTxt.setText(AppConstants.SUB_TOTAL_CASH+" "+context.getResources().getString(R.string.aed));
            mAmoutPaidByCardTxt.setText(amtTxtStr+" "+context.getResources().getString(R.string.aed));
            mTotalPointsDeductionTxt.setText("0"+" "+context.getResources().getString(R.string.points));
            mAmountPaidByPointsTxt.setText(AppConstants.REDEEM_POINT_TO_CASH+" "+context.getResources().getString(R.string.aed));
            mDiscountTxt.setText(AppConstants.APPLY_CODE_AMT+" "+context.getResources().getString(R.string.aed));


        }else if (AppConstants.REDEEM_POINT_TO_CASH.equalsIgnoreCase("0")&&AppConstants.APPLY_CODE.equalsIgnoreCase("")){
            amtTxt.setText(amtTxtStr+" "+context.getResources().getString(R.string.aed));
            mAmoutPaidByCardTxt.setText(amtTxtStr+" "+context.getResources().getString(R.string.aed));
            mTotalPointsDeductionTxt.setText("0"+" "+context.getResources().getString(R.string.points));
            mAmountPaidByPointsTxt.setText(AppConstants.REDEEM_POINT_TO_CASH+" "+context.getResources().getString(R.string.aed));
            mDiscountTxt.setText("0"+" "+context.getResources().getString(R.string.aed));

        }

        homeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mShowPaymentSuccessDialog.dismiss();
                dialogAlertInterface.onPositiveClick();
            }
        });

        alertShowing(mShowPaymentSuccessDialog);

    }

    public void showPaymentFailurePopup(Context context,String language, final InterfaceBtnCallBack dialogAlertInterface) {

        alertDismiss(mShowPaymentFailureDialog);
        mShowPaymentFailureDialog = getDialog(context, R.layout.pop_up_payment_failure);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mShowPaymentFailureDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        ImageView homeImg;
//        TextView paymentFailureTxt;
        pl.droidsonroids.gif.GifImageView mImageFailureGif;

        /*Init view*/
        homeImg = mShowPaymentFailureDialog.findViewById(R.id.pop_up_payment_failure_home_img);
//        paymentFailureTxt = mShowPaymentFailureDialog.findViewById(R.id.pop_up_payment_failure_txt);
        mImageFailureGif = mShowPaymentFailureDialog.findViewById(R.id.pop_up_failure_gif);

        if (language.equalsIgnoreCase("Arabic")){
            try {
                Glide.with(context)
                        .load(R.drawable.payment_failure_arabic_gif)
                        .into(mImageFailureGif);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
        }else {
            try {
                Glide.with(context)
                        .load(R.drawable.payment_failure_gif)
                        .into(mImageFailureGif);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
        }

        homeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mShowPaymentFailureDialog.dismiss();
                dialogAlertInterface.onPositiveClick();
            }
        });

        alertShowing(mShowPaymentFailureDialog);


    }


    public void showProgress(Context context) {

        /*To check if the progressbar is shown, if the progressbar is shown it will be cancelled */
        hideProgress();

        /*Init progress dialog*/
        mProgressDialog = new Dialog(context);
        mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mProgressDialog.getWindow() != null) {
            mProgressDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mProgressDialog.setContentView(R.layout.pop_up_progress);
            mProgressDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            mProgressDialog.getWindow().setGravity(Gravity.CENTER);
            mProgressDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        try {
            if (mProgressDialog != null) {
                mProgressDialog.show();
            }
        } catch (Exception e) {
            Log.e(AppConstants.TAG, e.getMessage());
        }
    }

    /*GetCountryDialog*/
    public void showCountryListPopup(Context context , GetCountryAdapter getCountryAdapter, ArrayList<GetCountryEntity> countryString, Dialog mDialog) {

        alertDismiss(mDialog);
        mDialog = getDialog(context, R.layout.pop_up_country_alert);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        TextView cancelTxt;

        RecyclerView countryRecyclerView;

        /*Init view*/
        cancelTxt = mDialog.findViewById(R.id.country_text_cancel);

        countryRecyclerView = mDialog.findViewById(R.id.country_popup_recycler_view);

        countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        countryRecyclerView.setAdapter(getCountryAdapter);

        /*Set data*/
        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mDialog.dismiss();
            }
        });

        alertShowing(mDialog);


    }

    public void showImageUploadPopup(final Context context, String header, String btn1Name, String btn2Name, final InterfaceTwoBtnCallBack mCallback) {
        alertDismiss(mImageUploadDialog);
        mImageUploadDialog = getDialog(context, R.layout.pop_up_photo_selection);
        TextView msgTxt ,firstTxt, secondTxt;
        Button  cancelBtn;


        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mImageUploadDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
            window.getAttributes().windowAnimations = R.style.PopupBottomAnimation;

        }

        /*Init view*/
        msgTxt = mImageUploadDialog.findViewById(R.id.alertTitle);
        firstTxt = mImageUploadDialog.findViewById(R.id.popup_first_txt);
        secondTxt = mImageUploadDialog.findViewById(R.id.popup_second_txt);
        cancelBtn = mImageUploadDialog.findViewById(R.id.popup_cancel);

        /*Set data*/
        msgTxt.setText(header);
        firstTxt.setText(btn1Name);
        secondTxt.setText(btn2Name);

        firstTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageUploadDialog.dismiss();
                mCallback.onNegativeClick();
            }
        });
        secondTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageUploadDialog.dismiss();
                mCallback.onPositiveClick();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mImageUploadDialog.dismiss();
            }
        });
        mImageUploadDialog.setCanceledOnTouchOutside(true);

        alertShowing(mImageUploadDialog);
    }

    public void showOfferSortPopup(String language, String CatogiresId, final BaseActivity baseActivity) {
        alertDismiss(mOfferSortDialog);
        mOfferSortDialog = getDialog(baseActivity, R.layout.pop_up_offer_sort);
        RelativeLayout popularityLay,priceLowToHighLay,priceHighToLowLay,newestFirstLay;
        ImageView popularityImg,priceLowToHighImg,priceHighToLowImg,newestFirstImg;


        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mOfferSortDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.BOTTOM);
            window.getAttributes().windowAnimations = R.style.PopupBottomAnimation;

        }
        mOfferSortDialog.setCancelable(true);
        mOfferSortDialog.setCanceledOnTouchOutside(true);

        if (language.equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mOfferSortDialog.findViewById(R.id.pop_up_offer_sort_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(mOfferSortDialog.findViewById(R.id.pop_up_offer_sort_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        /*Init view*/
        popularityLay = mOfferSortDialog.findViewById(R.id.pop_up_offer_popular_par_lay);
        priceLowToHighLay = mOfferSortDialog.findViewById(R.id.pop_up_offer_price_low_to_high_par_lay);
        priceHighToLowLay = mOfferSortDialog.findViewById(R.id.pop_up_offer_price_high_to_low_par_lay);
        newestFirstLay = mOfferSortDialog.findViewById(R.id.pop_up_offer_newest_first_par_lay);

        popularityImg = mOfferSortDialog.findViewById(R.id.pop_up_popular_first_img);
        priceLowToHighImg = mOfferSortDialog.findViewById(R.id.pop_up_price_low_to_high_first_img);
        priceHighToLowImg = mOfferSortDialog.findViewById(R.id.pop_up_price_high_to_low_img);
        newestFirstImg = mOfferSortDialog.findViewById(R.id.pop_up_newest_first_img);

        popularityImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));
        priceLowToHighImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));
        priceHighToLowImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));
        newestFirstImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));

        if (AppConstants.OFFER_CATEGORIES.equalsIgnoreCase("GetPopularity")){
            popularityImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_oragne_clr_on));
            popularityImg.setRotation(0);
            priceLowToHighImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));
            priceHighToLowImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));
            newestFirstImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));

        }else if (AppConstants.OFFER_CATEGORIES.equalsIgnoreCase("GetOfferlowtohigh")){
            popularityImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));
            priceLowToHighImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_oragne_clr_on));
            priceLowToHighImg.setRotation(0);
            priceHighToLowImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));
            newestFirstImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));

        }else if (AppConstants.OFFER_CATEGORIES.equalsIgnoreCase("GetOfferHightoLow")){
            popularityImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));
            priceLowToHighImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));
            priceHighToLowImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_oragne_clr_on));
            priceHighToLowImg.setRotation(0);
            newestFirstImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));

        }else if (AppConstants.OFFER_CATEGORIES.equalsIgnoreCase("GetOfferNewestFirst")){
            popularityImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));
            priceLowToHighImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));
            priceHighToLowImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_off_black_icon));
            newestFirstImg.setBackground(baseActivity.getResources().getDrawable(R.drawable.toggle_oragne_clr_on));
            newestFirstImg.setRotation(0);
        }

        popularityLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.OFFER_CATEGORIES = "GetPopularity";
                APIRequestHandler.getInstance().getSortOfferCategoriesApi("GetPopularity",CatogiresId,baseActivity);
                mOfferSortDialog.dismiss();
            }
        });
        priceLowToHighLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.OFFER_CATEGORIES = "GetOfferlowtohigh";
                APIRequestHandler.getInstance().getSortOfferCategoriesApi("GetOfferlowtohigh",CatogiresId,baseActivity);
                mOfferSortDialog.dismiss();

            }
        });

        priceHighToLowLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.OFFER_CATEGORIES = "GetOfferHightoLow";
                APIRequestHandler.getInstance().getSortOfferCategoriesApi("GetOfferHightoLow",CatogiresId,baseActivity);
                mOfferSortDialog.dismiss();

            }
        });

        newestFirstLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.OFFER_CATEGORIES = "GetOfferNewestFirst";
                APIRequestHandler.getInstance().getSortOfferCategoriesApi("GetOfferNewestFirst",CatogiresId,baseActivity);
                mOfferSortDialog.dismiss();

            }
        });

        mOfferSortDialog.setCanceledOnTouchOutside(true);

        alertShowing(mOfferSortDialog);
    }

    public void hideProgress() {
        /*hide progress*/
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            try {
                mProgressDialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }

    }

}
