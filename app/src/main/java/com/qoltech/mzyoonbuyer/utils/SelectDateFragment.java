package com.qoltech.mzyoonbuyer.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.qoltech.mzyoonbuyer.ui.AppointmentDetails;
import com.qoltech.mzyoonbuyer.ui.ProfileScreen;
import com.qoltech.mzyoonbuyer.ui.ScheduletDetailScreen;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, yy, mm, dd);

        if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("PROFILE")){

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

        }else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_MATERIAL_FROM")){

            if ( AppConstants.DATE_FOR_RESTRICT_MATERIAL.equalsIgnoreCase("")){
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            }else {
                String dateString = AppConstants.DATE_FOR_RESTRICT_MATERIAL.replace(" ","/");
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy",Locale.ENGLISH);
                Date date = null;
                try {
                    date = sdf.parse(dateString);
                    long startDate = date.getTime();
                    datePickerDialog.getDatePicker().setMinDate(startDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        } else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_MATERIAL_TO")){

            String dateString = AppConstants.DATE_FOR_RESTRICT_MATERIAL.replace(" ","/");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy", Locale.ENGLISH);
            Date date = null;
            try {
                date = sdf.parse(dateString);
                long startDate = date.getTime();
                datePickerDialog.getDatePicker().setMinDate(startDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_MEASUREMENT_FROM")){
            if (AppConstants.DATE_FOR_RESTRICT_MEASUREMENT.equalsIgnoreCase("")){
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            }else {
                String dateString = AppConstants.DATE_FOR_RESTRICT_MEASUREMENT.replace(" ","/");
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy", Locale.ENGLISH);
                Date date = null;
                try {
                    date = sdf.parse(dateString);
                    long startDate = date.getTime();
                    datePickerDialog.getDatePicker().setMinDate(startDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_MEASUREMENT_TO")){

            String dateString = AppConstants.DATE_FOR_RESTRICT_MEASUREMENT.replace(" ","/");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy", Locale.ENGLISH);
            Date date = null;
            try {
                date = sdf.parse(dateString);
                long startDate = date.getTime();
                datePickerDialog.getDatePicker().setMinDate(startDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_SCHEDULE_FROM")){
            if (AppConstants.DATE_FOR_RESTRICT_SCHEDULE.equalsIgnoreCase("")){

                Date currentDate = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(currentDate);
                c.add(Calendar.DATE, Integer.parseInt(AppConstants.NO_OF_DAYS_FOR_SCHEDULE));

                Date currentDatePlusOne = c.getTime();

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy", Locale.ENGLISH);
                Date date = null;
                try {
                    date = sdf.parse(sdf.format(currentDatePlusOne));
                    long startDate = date.getTime();
                    datePickerDialog.getDatePicker().setMinDate(startDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

//                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            }else {
                String dateString = AppConstants.DATE_FOR_RESTRICT_SCHEDULE.replace(" ","/");
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy", Locale.ENGLISH);
                Date date = null;
                try {
                    date = sdf.parse(dateString);
                    long startDate = date.getTime();
                    datePickerDialog.getDatePicker().setMinDate(startDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
//        else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_SCHEDULE_TO")){
//
//
//            String dateString = AppConstants.DATE_FOR_RESTRICT_SCHEDULE.replace(" ","/");
//            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
//            Date date = null;
//            try {
//                date = sdf.parse(dateString);
//                long startDate = date.getTime();
//                datePickerDialog.getDatePicker().setMinDate(startDate);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
//        }

        return datePickerDialog;
    }

    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(yy, mm, dd);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy", Locale.ENGLISH);

        String dateString = dateFormat.format(calendar.getTime());

        if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("PROFILE")) {
            ((ProfileScreen)getActivity()).mProfileDobEdtTxt.setText(dateString);
        }else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_MATERIAL_FROM")){
            ((AppointmentDetails)getActivity()).mMaterialTypeFromDateTxt.setText(dateString);
        }
        else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_MEASUREMENT_FROM")){
            ((AppointmentDetails)getActivity()).mMeasurementTypeFromDateTxt.setText(dateString);
        }
        else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_SCHEDULE_FROM")){
            ((ScheduletDetailScreen)getActivity()).mScheduleTypeFromDateTxt.setText(dateString);
        }
    }

}