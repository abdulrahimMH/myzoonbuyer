package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.RelatedProductsEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.StoreWebviewScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RelatedProductAdapter extends RecyclerView.Adapter<RelatedProductAdapter.Holder> {

    private Context mContext;
    private ArrayList<RelatedProductsEntity> mRelatedProductsEntity;

    private UserDetailsEntity mUserDetailsEntityRes;
    public RelatedProductAdapter(ArrayList<RelatedProductsEntity> relatedProductsEntities, Context activity) {
        mContext = activity;
        mRelatedProductsEntity = relatedProductsEntities;
    }

    @NonNull
    @Override
    public RelatedProductAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_related_products_list, parent, false);
        return new RelatedProductAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RelatedProductAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){

            holder.mRelatedProductRateTxt.setText(mContext.getResources().getString(R.string.aed)+" "+String.valueOf(mRelatedProductsEntity.get(position).getAmount()));

        }else {
            holder.mRelatedProductRateTxt.setText(String.valueOf(mRelatedProductsEntity.get(position).getAmount())+" "+mContext.getResources().getString(R.string.aed));
        }

        holder.mRelatedProductTxt.setText(mRelatedProductsEntity.get(position).getProductName());
        holder.mRelatedProductRatingBar.setRating(Float.parseFloat(String.valueOf(mRelatedProductsEntity.get(position).getRating())));

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/Products/"+mRelatedProductsEntity.get(position).getProductImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mRelatedProductImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.LOAD_STORE_WEB = "RELATED";
                AppConstants.RELATED_PROD_ID = mRelatedProductsEntity.get(position).getProduct();
                AppConstants.RELATED_PROD_NAME = mRelatedProductsEntity.get(position).getProductName();
                ((BaseActivity)mContext).nextScreen(StoreWebviewScreen.class,true);
            }
        });
    }

    @Override
    public int getItemCount() {
      return mRelatedProductsEntity.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.related_products_par_lay)
        CardView mRelatedParLay;

        @BindView(R.id.related_products_rating_bar)
        RatingBar mRelatedProductRatingBar;

        @BindView(R.id.related_products_rate_txt)
        TextView mRelatedProductRateTxt;

        @BindView(R.id.related_products_txt)
        TextView mRelatedProductTxt;

        @BindView(R.id.related_products_img_lay)
        ImageView mRelatedProductImg;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

