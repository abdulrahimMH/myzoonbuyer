package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.CustomizationMaterialsEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationTwoMaterialAdapter  extends RecyclerView.Adapter<CustomizationTwoMaterialAdapter.Holder> {

    private Context mContext;
    private ArrayList<CustomizationMaterialsEntity> mCustomizeMaterialList;
    int mTotalInt = 1;

    private UserDetailsEntity mUserDetailsEntityRes;
    public CustomizationTwoMaterialAdapter(Context activity, ArrayList<CustomizationMaterialsEntity> customizeMaterialList) {
        mContext = activity;
        mCustomizeMaterialList = customizeMaterialList;
    }

    @NonNull
    @Override
    public CustomizationTwoMaterialAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_thickness_grid_view, parent, false);
        return new CustomizationTwoMaterialAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomizationTwoMaterialAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final CustomizationMaterialsEntity MaterialEntity = mCustomizeMaterialList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mGridViewTxt.setText(MaterialEntity.getMaterialInArabic());

        }else {
            holder.mGridViewTxt.setText(MaterialEntity.getMaterialInEnglish());

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCustomizeMaterialList.get(position).getMaterialInEnglish().equalsIgnoreCase("all material type")){
                    if (mCustomizeMaterialList.get(position).getChecked()){
                        for (int i=0; i<mCustomizeMaterialList.size(); i++) {
                            mCustomizeMaterialList.get(i).setChecked(false);
                        }
                        AppConstants.MATERIAL_ID = "";
                        AppConstants.MATERIAL_TYPE_NAME = "";

                    }else {
                        AppConstants.MATERIAL_ID = "";
                        AppConstants.MATERIAL_TYPE_NAME = "";
                        for (int i=0; i<mCustomizeMaterialList.size(); i++) {
                            mCustomizeMaterialList.get(i).setChecked(true);
                            AppConstants.MATERIAL_ID =String.valueOf(mCustomizeMaterialList.get(position).getId());
                            AppConstants.MATERIAL_TYPE_NAME =String.valueOf(mCustomizeMaterialList.get(position).getMaterialInArabic());
                        }
                    }
//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        ((CustomizationOneScreen)mContext).getNewFlowCustomizationNewApiCall();
//
//                    }else {
//                        ((CustomizationOneScreen)mContext).getNewCustomizationApiCall();
//
//                    }

                    notifyDataSetChanged();

                }else {
                    if (mCustomizeMaterialList.get(position).getChecked()){
                        mCustomizeMaterialList.get(position).setChecked(false);
                        for (int i=0; i<mCustomizeMaterialList.size() ; i++)
                        {
                            if (mCustomizeMaterialList.get(i).getMaterialInEnglish().equalsIgnoreCase("all material type"))
                            {
                                mCustomizeMaterialList.get(i).setChecked(false);

                            }
                        }
                    }else {
                        mTotalInt = 1;
                        for (int i=0; i<mCustomizeMaterialList.size();i++){
                            if (mCustomizeMaterialList.get(i).getChecked()){
                                mTotalInt = mTotalInt + 1;
                            }
                            if (String.valueOf(mTotalInt).equalsIgnoreCase(String.valueOf(mCustomizeMaterialList.size()-1))){
                                for (int j=0; j<mCustomizeMaterialList.size(); j++){
                                    if (mCustomizeMaterialList.get(j).getMaterialInEnglish().equalsIgnoreCase("all material type")){
                                        mCustomizeMaterialList.get(j).setChecked(true);
                                    }
                                }

                            }
                        }
                        mCustomizeMaterialList.get(position).setChecked(true);
                    }
                    AppConstants.MATERIAL_ID = "";
                    for (int i=0; i<mCustomizeMaterialList.size(); i++){
                        if (mCustomizeMaterialList.get(i).getChecked()){
                            AppConstants.MATERIAL_ID = AppConstants.MATERIAL_ID.equalsIgnoreCase("") ? mCustomizeMaterialList.get(i).getId()+"" : AppConstants.MATERIAL_ID + ","+ mCustomizeMaterialList.get(i).getId();

                            AppConstants.MATERIAL_TYPE_NAME =  AppConstants.MATERIAL_TYPE_NAME.equalsIgnoreCase("") ? mCustomizeMaterialList.get(i).getMaterialInArabic() : AppConstants.MATERIAL_TYPE_NAME + ","+ mCustomizeMaterialList.get(i).getMaterialInEnglish();
                        }
                    }
//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        ((CustomizationOneScreen)mContext).getNewFlowCustomizationNewApiCall();
//
//                    }else {
//                        ((CustomizationOneScreen)mContext).getNewCustomizationApiCall();
//
//                    }
                    notifyDataSetChanged();
                }
            }
        });
        holder.mGridViewLay.setBackgroundResource(MaterialEntity.getChecked() ? R.drawable.app_clr_border_with_corner : R.drawable.white_corner);
    }

    @Override
    public int getItemCount() {
        return mCustomizeMaterialList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {


        @BindView(R.id.grid_custimize_par_lay)
        RelativeLayout mGridViewLay;

        @BindView(R.id.grid_custimize_view_txt)
        TextView mGridViewTxt;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


