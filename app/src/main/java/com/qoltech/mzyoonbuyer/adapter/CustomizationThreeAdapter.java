package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.CustomizationThreeClickEntity;
import com.qoltech.mzyoonbuyer.entity.GetCustomizationThreeEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.ui.ChooseCustomizationScreen;
import com.qoltech.mzyoonbuyer.ui.CustomizationThreeScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationThreeAdapter extends RecyclerView.Adapter<CustomizationThreeAdapter.Holder> {

    private Context mContext;
    private ArrayList<GetCustomizationThreeEntity> mCustomizeList;

    private ArrayList<CustomizationThreeClickEntity> mGetTickValue = new ArrayList<>();

    private UserDetailsEntity mUserDetailsEntityRes;
    public CustomizationThreeAdapter(Context activity, ArrayList<GetCustomizationThreeEntity> customizeList) {
        mContext = activity;
        mCustomizeList = customizeList;
    }

    @NonNull
    @Override
    public CustomizationThreeAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_choose_customization_list, parent, false);
        return new CustomizationThreeAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomizationThreeAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final GetCustomizationThreeEntity customizationEntity = mCustomizeList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mGridViewTxt.setText(customizationEntity.getAttributeNameInArabic());

        }else {
            holder.mGridViewTxt.setText(customizationEntity.getAttributeNameInEnglish());
        }

        if (AppConstants.CUSTOMIZATION_CLICK_ARRAY.size() > 0){
          mGetTickValue = AppConstants.CUSTOMIZATION_CLICK_ARRAY ;
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < mCustomizeList.size(); i++) {
                    mCustomizeList.get(i).setChecked(false);
                }
                    mCustomizeList.get(position).setChecked(true);
                if (mGetTickValue.size()>0){
                    for (int i=0; i<mGetTickValue.size(); i++){
                        if (mGetTickValue.get(i).getCustomizationName().equalsIgnoreCase(AppConstants.CUSTOMIZATION_DRESS_TYPE_ID)){
                            mGetTickValue.remove(i);
                        }
                    }
                }

                CustomizationThreeClickEntity list = new CustomizationThreeClickEntity(String.valueOf(mCustomizeList.get(position).getId()),AppConstants.CUSTOMIZATION_DRESS_TYPE_ID,AppConstants.CUSTOMIZATION_NAME,mCustomizeList.get(position).getAttributeNameInEnglish(),mCustomizeList.get(position).getImages());

                mGetTickValue.add(list);

                AppConstants.CUSTOMIZATION_CLICK_ARRAY = mGetTickValue;

                try {
                    Glide.with(mContext)
                            .load(AppConstants.IMAGE_BASE_URL+"images/Customazation3/"+mCustomizeList.get(position).getImages())
                            .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                            .into(((CustomizationThreeScreen)mContext).mCustomizationThreeFullImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

                notifyDataSetChanged();
                AppConstants.CUSTOMIZATION_CLICKED_ARRAY.add(AppConstants.CUSTOMIZATION_DRESS_TYPE_ID);
                ((ChooseCustomizationScreen)mContext).onBackPressed();
            }
        });
                try {
                    holder.mGridViewImgLay.setVisibility(View.VISIBLE);

                    Glide.with(mContext)
                            .load(AppConstants.IMAGE_BASE_URL+"images/Customazation3/"+customizationEntity.getImages())
                            .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                            .into(holder.mGridViewImgLay);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

                if (mGetTickValue.size() > 0){
                    for (int i=0; i<mGetTickValue.size(); i++){
                        if (mGetTickValue.get(i).getCustomizationName().equalsIgnoreCase(AppConstants.CUSTOMIZATION_DRESS_TYPE_ID)){
                            for (int j=0; j<mCustomizeList.size() ; j++){
                                if (mCustomizeList.get(j).getId() == Integer.parseInt(mGetTickValue.get(i).getId())){
                                    mCustomizeList.get(j).setChecked(true);
                                }
                                else {
                                    mCustomizeList.get(j).setChecked(false);

                                }
                            }
                        }
                    }
                }

                holder.mChooseCustomizationZoomImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ((ChooseCustomizationScreen)mContext).mChooseCustomizationListLay.setVisibility(View.GONE);
                        ((ChooseCustomizationScreen)mContext).mChooseCustomizatoinImgLay.setVisibility(View.VISIBLE);

                        Glide.with(mContext).load(AppConstants.IMAGE_BASE_URL+"images/Customazation3/"+mCustomizeList.get(position).getImages())
                                .thumbnail(Glide.with(mContext).load(R.drawable.dress_type_empty_img))
                                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .placeholder(R.drawable.dress_type_empty_img)).into(((ChooseCustomizationScreen)mContext).photo_views);

                    }
                });
                holder.mTickImg.setBackgroundResource(mCustomizeList.get(position).getChecked() ? R.drawable.tick_selected_img : R.drawable.tick_unselected_img);

    }

    @Override
    public int getItemCount() {
        return mCustomizeList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.choose_customization_list_img)
        ImageView mGridViewImgLay;

        @BindView(R.id.choose_customization_list_txt)
        TextView mGridViewTxt;

        @BindView(R.id.choose_customization_tick_img)
        ImageView mTickImg;

        @BindView(R.id.choose_customization_zoom_img)
        ImageView mChooseCustomizationZoomImg;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

