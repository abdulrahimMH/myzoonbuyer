package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.ui.ViewImageScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewImageAdapter extends RecyclerView.Adapter<ViewImageAdapter.Holder> {

    private Context mContext;
    private ArrayList<String> mImageList;
    private UserDetailsEntity mUserDetailsEntityRes;

    public ViewImageAdapter(Context activity, ArrayList<String> imageList) {
        mContext = activity;
        mImageList = imageList;
    }

    @NonNull
    @Override
    public ViewImageAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_image_view, parent, false);
        return new ViewImageAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewImageAdapter.Holder holder, final int position) {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Glide.with(mContext)
                            .load(AppConstants.VIEW_IMAGE_LIST.get(position))
                            .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL).
                                    format(DecodeFormat.PREFER_ARGB_8888).override(Target.SIZE_ORIGINAL)
                                    .placeholder(R.drawable.dress_type_empty_img)).into(ViewImageScreen.photo_view);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
            }
        });

        try {
            Glide.with(mContext)
                    .load(mImageList.get(position))
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

    }

    @Override
    public int getItemCount() {
        return mImageList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_view_only_img_lay)
        CardView mGridViewLay;

        @BindView(R.id.grid_view_only_img)
        ImageView mGridViewImgLay;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


