package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.AppointmentListEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.AppointmentDetails;
import com.qoltech.mzyoonbuyer.ui.ScheduletDetailScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AppointmentListAdapter extends RecyclerView.Adapter<AppointmentListAdapter.Holder> {

    private Context mContext;
    private ArrayList<AppointmentListEntity> mAppointmentList;

    private UserDetailsEntity mUserDetailsEntityRes;

    public AppointmentListAdapter(Context activity,ArrayList<AppointmentListEntity> appointmentList) {
        mContext = activity;
        mAppointmentList = appointmentList;
    }

    @NonNull
    @Override
    public AppointmentListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_appointment_list, parent, false);
        return new AppointmentListAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AppointmentListAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        AppointmentListEntity mList = mAppointmentList.get(position);

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/DressSubType/"+mList.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mAppointmentNameImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
//            holder.mAppointmentTailorNameTxt.setText(mList.getTailorNameInArabic());
            holder.mAppointmentShopNameTxt.setText(mList.getShopNameInArabic());
            holder.mAppointmentProductNameTxt.setText(mList.getNameInArabic());
        }else {
//            holder.mAppointmentTailorNameTxt.setText(mList.getTailorNameInEnglish());
            holder.mAppointmentShopNameTxt.setText(mList.getShopNameInEnglish());
            holder.mAppointmentProductNameTxt.setText(mList.getNameInEnglish());
        }
        holder.mAppointmentListDateTxt.setText(mList.getCreatedOn().replace("T00:00:00",""));
        holder.mAppointmentIdTxt.setText(String.valueOf(mList.getId()));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.CHECK_BOOK_APPOINTMENT = "BookAnAppointment";
                AppConstants.DIRECT_ORDER = "";
                AppConstants.APPROVE_REJECT_BTN = "false";
                AppConstants.APPOINTMENT_LIST_ID =String.valueOf(mAppointmentList.get(position).getId());
                AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(mAppointmentList.get(position).getDressSubTypeId());
                AppConstants.APPROVED_TAILOR_ID = String.valueOf(mAppointmentList.get(position).getApprovedTailorId());
                if (mAppointmentList.get(position).getDeliveryTypeId() == 3){
                    ((BaseActivity)mContext).nextScreen(ScheduletDetailScreen.class,true);

                }else {
                    ((BaseActivity)mContext).nextScreen(AppointmentDetails.class,true);

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mAppointmentList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.appointment_list_img)
        de.hdodenhof.circleimageview.CircleImageView mAppointmentNameImg;

        @BindView(R.id.appointment_list_date_txt)
        TextView mAppointmentListDateTxt;

        @BindView(R.id.appointment_id_txt)
        TextView mAppointmentIdTxt;

//        @BindView(R.id.appointment_tailor_name_txt)
//        TextView mAppointmentTailorNameTxt;

        @BindView(R.id.appointment_shop_name_txt)
        TextView mAppointmentShopNameTxt;

        @BindView(R.id.appointment_product_name_txt)
        TextView mAppointmentProductNameTxt;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

