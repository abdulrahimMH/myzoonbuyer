package com.qoltech.mzyoonbuyer.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetAreaEntity;
import com.qoltech.mzyoonbuyer.ui.AddAddressScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GetAreaAdapter  extends RecyclerView.Adapter<GetAreaAdapter.Holder> {

    private Context mContext;
    private ArrayList<GetAreaEntity> mAreaList;
    private Dialog mDialog;

    public GetAreaAdapter(Context activity, ArrayList<GetAreaEntity> getAreaList, Dialog dialog) {
        mContext = activity;
        mAreaList = getAreaList;
        mDialog = dialog;
    }

    @NonNull
    @Override
    public GetAreaAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_country_code_list, parent, false);
        return new GetAreaAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final GetAreaAdapter.Holder holder, final int position) {
        final GetAreaEntity getAreaEntity = mAreaList.get(position);

        holder.mGetCountryTxtViewTxt.setText(getAreaEntity.getArea());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.COUNTRY_AREA_ID = String.valueOf(mAreaList.get(position).getId());
                ((AddAddressScreen)mContext).mAddressAreaEdtTxt.setText(mAreaList.get(position).getArea());
                mDialog.dismiss();

            }
        });

        holder.mCountryFlagImg.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return mAreaList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.get_country_recycler_view_txt)
        TextView mGetCountryTxtViewTxt;

        @BindView(R.id.get_country_flag_img)
        ImageView mCountryFlagImg;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}



