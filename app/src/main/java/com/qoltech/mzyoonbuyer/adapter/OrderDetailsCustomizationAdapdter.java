package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetOrderDetailsCustomizationEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.ui.ViewImageScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderDetailsCustomizationAdapdter extends RecyclerView.Adapter<OrderDetailsCustomizationAdapdter.Holder> {

    private Context mContext;
    private ArrayList<GetOrderDetailsCustomizationEntity> mCustomizationList;

    private UserDetailsEntity mUserDetailsEntityRes;
    public OrderDetailsCustomizationAdapdter(Context activity, ArrayList<GetOrderDetailsCustomizationEntity> customizeList) {
        mContext = activity;
        mCustomizationList = customizeList;
    }

    @NonNull
    @Override
    public OrderDetailsCustomizationAdapdter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grid_cutomize_corner, parent, false);
        return new OrderDetailsCustomizationAdapdter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderDetailsCustomizationAdapdter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final GetOrderDetailsCustomizationEntity customizationEntity = mCustomizationList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mGridViewTxt.setText(customizationEntity.getCustomizationNameInArabic());

        }else {
            holder.mGridViewTxt.setText(customizationEntity.getCustomizationNameInEnglish());

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    for (int i=0; i<mCustomizationList.size(); i++){
                        mCustomizationList.get(i).setChecked(false);
                    }
                    mCustomizationList.get(position).setChecked(true);
                    holder.mGridViewImgLay.setVisibility(View.VISIBLE);
                    Glide.with(mContext)
                            .load(AppConstants.IMAGE_BASE_URL+"images/Customazation3/"+mCustomizationList.get(position).getImages())
                            .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                            .into(ViewImageScreen.photo_view);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
                notifyDataSetChanged();

            }
        });
        holder.mGridViewImgLay.setVisibility(View.VISIBLE);

        try {
            holder.mGridViewImgLay.setVisibility(View.VISIBLE);

            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"images/Customazation3/"+customizationEntity.getImages())
                    .apply(new RequestOptions().placeholder(R.drawable.empty_img).error(R.drawable.empty_img))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.mTickImg.setVisibility(customizationEntity.isChecked() ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return mCustomizationList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_custimize_view_lay)
        LinearLayout mGridViewLay;

        @BindView(R.id.grid_custimize_view_img_lay)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_custimize_view_bottom_txt)
        TextView mGridViewBottomTxt;

        @BindView(R.id.grid_custimize_view_txt)
        TextView mGridViewTxt;

        @BindView(R.id.grid_custimize_img)
        ImageView mTickImg;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


