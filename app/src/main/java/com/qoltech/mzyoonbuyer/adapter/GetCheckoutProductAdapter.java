package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.StoreCartEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GetCheckoutProductAdapter extends RecyclerView.Adapter<GetCheckoutProductAdapter.Holder> {

    private Context mContext;
    private ArrayList<StoreCartEntity> mRelatedProductsEntity;

    private UserDetailsEntity mUserDetailsEntityRes;
    public GetCheckoutProductAdapter(ArrayList<StoreCartEntity> relatedProductsEntities, Context activity) {
        mContext = activity;
        mRelatedProductsEntity = relatedProductsEntities;
    }

    @NonNull
    @Override
    public GetCheckoutProductAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_checkout_related_amout_list, parent, false);
        return new GetCheckoutProductAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final GetCheckoutProductAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        if (mRelatedProductsEntity.get(position).getProductName().equalsIgnoreCase("")){
            holder.mAdapterProductCheckourNameTxt.setText(mRelatedProductsEntity.get(position).getProduct_Name() + " | " + mRelatedProductsEntity.get(position).getNewPrice()*mRelatedProductsEntity.get(position).getQuantity());

        }else {
            holder.mAdapterProductCheckourNameTxt.setText(mRelatedProductsEntity.get(position).getProductName() + " | " + mRelatedProductsEntity.get(position).getNewPrice()*mRelatedProductsEntity.get(position).getQuantity());

        }

        holder.mProductCheckOutAmtTxt.setText(String.valueOf(mRelatedProductsEntity.get(position).getNewPrice()*mRelatedProductsEntity.get(position).getQuantity()));
    }

    @Override
    public int getItemCount() {
        return mRelatedProductsEntity.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_product_checkout_amt_txt)
        TextView mProductCheckOutAmtTxt;

        @BindView(R.id.adapter_product_checkout_name_txt)
        TextView mAdapterProductCheckourNameTxt;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
