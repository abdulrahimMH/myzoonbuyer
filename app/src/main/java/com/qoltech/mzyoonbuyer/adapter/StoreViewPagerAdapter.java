package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetStoreDashBoardEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.StoreProductListScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;

import java.util.ArrayList;

public class StoreViewPagerAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    ArrayList<String> arrayList;

    ArrayList<GetStoreDashBoardEntity> mDashBoardList;

    public StoreViewPagerAdapter(Context context, ArrayList<String> arrayList, ArrayList<GetStoreDashBoardEntity> storeDashBoard) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.arrayList = arrayList;
        mDashBoardList = storeDashBoard;
    }

    @Override
    public int getCount() {
        if(arrayList != null){
            return arrayList.size();
        }
        return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = layoutInflater.inflate(R.layout.adapter_view_detail, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.viewPagerItem_image1);

        try {
            Glide.with(context)
                    .load(arrayList.get(position))
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(imageView);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.STORE_BANNER_ID = String.valueOf(mDashBoardList.get(position).getId());
                AppConstants.STORE_TYPE_ID = "1";
                AppConstants.STORE_SEARCH = "";
                ((BaseActivity)context).nextScreen(StoreProductListScreen.class,true);
            }
        });

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}