package com.qoltech.mzyoonbuyer.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.ui.ProfileScreen;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileGenderAdapter extends RecyclerView.Adapter<ProfileGenderAdapter.Holder> {

    private Context mContext;
    private ArrayList<String> mLocationList;
    private Dialog mDialog;

    public ProfileGenderAdapter(Context activity, ArrayList<String> locationList, Dialog dialog) {
        mContext = activity;
        mLocationList = locationList;
        mDialog = dialog;
    }

    @NonNull
    @Override
    public ProfileGenderAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_country_code_list, parent, false);
        return new ProfileGenderAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProfileGenderAdapter.Holder holder, final int position) {

        holder.mGetCountryTxtViewTxt.setText(mLocationList.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((ProfileScreen)mContext).mProfileGenderEdtTxt.setText(mLocationList.get(position));
                mDialog.dismiss();
            }
        });

        holder.mCountryFlagImg.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return mLocationList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.get_country_recycler_view_txt)
        TextView mGetCountryTxtViewTxt;

        @BindView(R.id.get_country_flag_img)
        ImageView mCountryFlagImg;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

