package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.StoreCatogeryEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreCatogeryAdapter extends RecyclerView.Adapter<StoreCatogeryAdapter.Holder> {

    ArrayList<StoreCatogeryEntity> mCaotegeryFilteredList;

    Context mContext;

    ArrayList<StoreCatogeryEntity> mCaotegeryList;

    ArrayList<StoreCatogeryEntity> mSubCaotegeryList;

    UserDetailsEntity mUserDetailsEntityRes;

    StoreSubCatogeryAdapter mAdapter;

    public StoreCatogeryAdapter(ArrayList<StoreCatogeryEntity> caotegeryFilteredList, Context context, ArrayList<StoreCatogeryEntity> caotegeryList) {
        mCaotegeryFilteredList = caotegeryFilteredList;
        mContext = context;
        mCaotegeryList = caotegeryList;
    }


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_catogery_list,viewGroup,false);

        return new StoreCatogeryAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        StoreCatogeryEntity list = mCaotegeryFilteredList.get(position);

        mSubCaotegeryList = new ArrayList<>();


        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mAapCatogeryTxt.setText(list.getDisplayIdInArabic());

        }else {
            holder.mAapCatogeryTxt.setText(list.getDisplayId());

        }

        for (int i=0; i<mCaotegeryList.size(); i++){
            if (mCaotegeryList.get(i).getParentId() == list.getId()){
                mSubCaotegeryList.add(mCaotegeryList.get(i));
            }
        }

        mAdapter = new StoreSubCatogeryAdapter(mSubCaotegeryList,mContext);
        holder.mAdapCatogeryRecView.setLayoutManager(new GridLayoutManager(mContext,3));
        holder.mAdapCatogeryRecView.setAdapter(mAdapter);

    }

    @Override
    public int getItemCount() {
        return mCaotegeryFilteredList.size();
    }

    public class Holder extends  RecyclerView.ViewHolder{

        @BindView(R.id.adap_catogery_txt)
        TextView mAapCatogeryTxt;

        @BindView(R.id.adap_catogery_rec_view)
        RecyclerView mAdapCatogeryRecView;

    public Holder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}

}


