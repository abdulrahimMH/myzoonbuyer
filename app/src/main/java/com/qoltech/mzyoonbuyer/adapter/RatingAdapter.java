package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.CustomerRatingEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RatingAdapter extends RecyclerView.Adapter<RatingAdapter.Holder> {

    private Context mContext;
    private ArrayList<CustomerRatingEntity> mCustomerList;

    public RatingAdapter(Context activity, ArrayList<CustomerRatingEntity> getCustomerList ) {
        mContext = activity;
        mCustomerList = getCustomerList;
    }

    @NonNull
    @Override
    public RatingAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_rating_star_list, parent, false);
        return new RatingAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RatingAdapter.Holder holder, final int position) {
        final CustomerRatingEntity getCustomerEntity = mCustomerList.get(position);

        holder.mRatingListProfileNameTxt.setText(getCustomerEntity.getName());
        holder.mRatingListRatingBar.setRating(Float.parseFloat(String.valueOf(getCustomerEntity.getRating())));
        holder.mRatingTimeDateTxt.setText(getCustomerEntity.getCreateDt());
        holder.mRatingListCommentTxt.setText(getCustomerEntity.getReview());
    }

    @Override
    public int getItemCount() {
        return mCustomerList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.rating_list_profile_name_txt)
        TextView mRatingListProfileNameTxt;

        @BindView(R.id.tailor_list_rating_bar)
        RatingBar mRatingListRatingBar;

        @BindView(R.id.rating_time_date_txt)
        TextView mRatingTimeDateTxt;

        @BindView(R.id.rating_list_comment_txt)
        TextView mRatingListCommentTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}



