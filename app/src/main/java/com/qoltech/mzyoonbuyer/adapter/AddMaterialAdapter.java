package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.ui.AddMaterialScreen;
import com.qoltech.mzyoonbuyer.ui.AddReferenceScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddMaterialAdapter extends RecyclerView.Adapter<AddMaterialAdapter.Holder> {

    private Context mContext;
    private ArrayList<String> mImageList;
    private UserDetailsEntity mUserDetailsEntityRes;

    public AddMaterialAdapter(Context activity, ArrayList<String> imageList) {
        mContext = activity;
        mImageList = imageList;
    }

    @NonNull
    @Override
    public AddMaterialAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_only_img_grid, parent, false);
        return new AddMaterialAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AddMaterialAdapter.Holder holder, final int position) {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_MATERIAL")){

                    try {
                        Glide.with(mContext)
                                .load(mImageList.get(position))
                                .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                                .into(((AddMaterialScreen)mContext).mAddMaterialImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }

                }else if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_REFERENCE")){
                    try {
                        Glide.with(mContext)
                                .load(mImageList.get(position))
                                .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                                .into(((AddReferenceScreen)mContext).mAddReferenceImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                }

            }
        });

        if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_MATERIAL")){
            try {
                Glide.with(mContext)
                        .load(mImageList.get(mImageList.size()-1))
                        .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                        .into(((AddMaterialScreen)mContext).mAddMaterialImg);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
        }else if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_REFERENCE")){
            try {
                Glide.with(mContext)
                        .load(mImageList.get(mImageList.size()-1))
                        .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                        .into(((AddReferenceScreen)mContext).mAddReferenceImg);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
        }

        try {
            Glide.with(mContext)
                    .load(mImageList.get(position))
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
//                    holder.mCountryFlagImg.setImageResource(R.drawable.demo_img);
            Log.e(AppConstants.TAG, ex.getMessage());
        }



        holder.mGridViewOnlyDeleteImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.getInstance().showOptionPopup(mContext, mContext.getResources().getString(R.string.delete_img), mContext.getResources().getString(R.string.yes), mContext.getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                    @Override
                    public void onNegativeClick() {

                    }

                    @Override
                    public void onPositiveClick() {
                        mImageList.remove(mImageList.get(position));
                        notifyDataSetChanged();
                        if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_MATERIAL")){
                            if (mImageList.size()>0){
                                ((AddMaterialScreen)mContext).setAdapter(mImageList);
                                ((AddMaterialScreen)mContext).mNoImagesShown.setVisibility(View.GONE);

                            }else {
                                ((AddMaterialScreen)mContext).mNoImagesShown.setVisibility(View.VISIBLE);

                            }
                            int color = Color.parseColor("#FFFFFF");
                            ColorDrawable colorDrawable = new ColorDrawable(color);
                            try {
                                Glide.with(mContext)
                                        .load(colorDrawable)
                                        .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                                        .into(((AddMaterialScreen)mContext).mAddMaterialImg);

                            } catch (Exception ex) {
                                Log.e(AppConstants.TAG, ex.getMessage());
                            }
                        }else if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_REFERENCE")){
                            if (mImageList.size()>0){
                                ((AddReferenceScreen)mContext).setAdapter(mImageList);
                                ((AddReferenceScreen)mContext).mNoImagesTxt.setVisibility(View.GONE);

                            }else {
                                ((AddReferenceScreen)mContext).mNoImagesTxt.setVisibility(View.VISIBLE);

                            }
                            try {
                                int color = Color.parseColor("#FFFFFF");
                                ColorDrawable colorDrawable = new ColorDrawable(color);

                                Glide.with(mContext)
                                        .load(colorDrawable)
                                        .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                                        .into(((AddReferenceScreen)mContext).mAddReferenceImg);

                            } catch (Exception ex) {
                                Log.e(AppConstants.TAG, ex.getMessage());
                            }
                        }
                    }
                });

        }
        });

        if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_MATERIAL")){
            if (mImageList.size()>0){
                ((AddMaterialScreen)mContext).mNoImagesShown.setVisibility(View.GONE);

            }else {
                ((AddMaterialScreen)mContext).mNoImagesShown.setVisibility(View.VISIBLE);

            }
        }else if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_REFERENCE")){
            if (mImageList.size()>0){
                ((AddReferenceScreen)mContext).mNoImagesTxt.setVisibility(View.GONE);

            }else {
                ((AddReferenceScreen)mContext).mNoImagesTxt.setVisibility(View.VISIBLE);

            }
        }
        holder.mGridViewOnlyDeleteImg.setVisibility(View.VISIBLE);

    }

    @Override
    public int getItemCount() {
        return mImageList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_view_only_img_lay)
        CardView mGridViewLay;

        @BindView(R.id.grid_view_only_img)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_view_only_img_close_img)
        ImageView mGridViewOnlyDeleteImg;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

