package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.TailorListEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.DirectOrderPriceScreen;
import com.qoltech.mzyoonbuyer.ui.OrderTypeScreen;
import com.qoltech.mzyoonbuyer.ui.ShopDetailsScreen;
import com.qoltech.mzyoonbuyer.ui.TailorListScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TailorListAdapter extends RecyclerView.Adapter<TailorListAdapter.Holder> {

    private Context mContext;
    private ArrayList<TailorListEntity> mTailorList;

    double mLongi;
    double mLatti;

    private UserDetailsEntity mUserDetailsEntityRes;
    public TailorListAdapter(Context activity, ArrayList<TailorListEntity> tailorList, double latti,double longi) {
        mContext = activity;
        mTailorList = tailorList;
        mLatti = latti;
        mLongi = longi;
    }

    @NonNull
    @Override
    public TailorListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_tailor_list, parent, false);
        return new TailorListAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final TailorListAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final TailorListEntity tailorEntity = mTailorList.get(position);

        holder.mTailorDirectionTxt.setText(tailorEntity.getDistanceText().equalsIgnoreCase("N/A") ? mContext.getResources().getString(R.string.not_available) : tailorEntity.getDistanceText().replace(" km","")+" "+mContext.getResources().getString(R.string.km_from_location));

        holder.mTailorPriceTxt.setText(mTailorList.get(position).getPrice());

        holder.mTailorListCardView.setCardBackgroundColor(Color.TRANSPARENT);
        holder.mTailorListCardView.setCardElevation(0);
        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")){
            holder.mTailorListAdapterLay.setVisibility(View.VISIBLE);
        }else {
            holder.mTailorListAdapterLay.setVisibility(View.GONE);
        }

        holder.mTailorListRatingBar.setRating(tailorEntity.getAvgRating());

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mTailorActShopTxt.setText(tailorEntity.getShopNameInArabic());
        }else {
            holder.mTailorActShopTxt.setText(tailorEntity.getShopNameInEnglish());
        }

        holder.mTailorActOrderNoTxt.setText(String.valueOf(tailorEntity.getNoofOrder()));

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/TailorImages/"+tailorEntity.getShopOwnerImageURL())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mTailorUserImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.mAdapterTailorParLay.setBackgroundResource(tailorEntity.getChecked() ? R.drawable.app_clr_border_with_corner_solid_grey_transparent : R.drawable.silver_clr_border_with_corner);

        holder.mTailorActShopTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    AppConstants.TAILOR_CLICK_NAME = mTailorList.get(position).getShopNameInArabic();
                }else{
                    AppConstants.TAILOR_CLICK_NAME = mTailorList.get(position).getShopNameInEnglish();
                }
                AppConstants.TAILOR_ID = String.valueOf(mTailorList.get(position).getTailorId());
                AppConstants.SHOP_OWN_LAT = Double.parseDouble(mTailorList.get(position).getLatitude());
                AppConstants.SHOP_OWN_LONG = Double.parseDouble(mTailorList.get(position).getLongitude());
                ((BaseActivity)mContext).nextScreen(ShopDetailsScreen.class,true);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER") || AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
                    for (int i = 0; i < mTailorList.size(); i++) {
                        mTailorList.get(i).setChecked(false);
                    }
                    ((TailorListScreen)mContext).mTailorSelectedTxt.setText(String.valueOf(1));
                    AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();
                    mTailorList.get(position).setChecked(true);
                    AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.add(mTailorList.get(position));
                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")){
                        ((BaseActivity)mContext).nextScreen(DirectOrderPriceScreen.class,true);
                    }
                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
                        ((BaseActivity)mContext).nextScreen(OrderTypeScreen.class,true);
                    }
                    notifyDataSetChanged();
                }
                else {
                    if (mTailorList.get(position).getChecked()){
                        mTailorList.get(position).setChecked(false);
                        int count = Integer.parseInt(((TailorListScreen)mContext).mTailorSelectedTxt.getText().toString().trim());
                        int sub = --count;
                        ((TailorListScreen)mContext).mTailorSelectedTxt.setText(String.valueOf(sub));
                        for (int i=0; i<AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.size(); i++){
                            if (mTailorList.get(position).ShopNameInEnglish.equalsIgnoreCase(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(i).ShopNameInEnglish)){
                                AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.remove(i);
                            }
                        }
                    }else {
                        mTailorList.get(position).setChecked(true);
                        int count = Integer.parseInt(((TailorListScreen)mContext).mTailorSelectedTxt.getText().toString().trim());
                        int add = ++count;
                        ((TailorListScreen)mContext).mTailorSelectedTxt.setText(String.valueOf(String.valueOf(add)));
                        AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.add(mTailorList.get(position));
                    }
                    notifyDataSetChanged();
                }
            }
        });

        //        holder.mTailorDirectionTxt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                        Uri.parse("http://maps.google.com/maps?saddr="+String.valueOf(mLatti)+","+String.valueOf(mLongi)+"&daddr="+mTailorList.get(position).getLatitude()+","+mTailorList.get(position).getLongitude()));
//                mContext.startActivity(intent);
//
//            }
//        });
//
//        holder.mTailorListDirPayLay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                        Uri.parse("http://maps.google.com/maps?saddr="+String.valueOf(mLatti)+","+String.valueOf(mLongi)+"&daddr="+mTailorList.get(position).getLatitude()+","+mTailorList.get(position).getLongitude()));
//                mContext.startActivity(intent);
//
//
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return mTailorList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_tailor_par_lay)
        RelativeLayout mAdapterTailorParLay;

        @BindView(R.id.tailor_act_shop_name_txt)
        TextView mTailorActShopTxt;

        @BindView(R.id.tailor_order_count_txt)
        TextView mTailorActOrderNoTxt;

        @BindView(R.id.tailor_user_img)
        ImageView mTailorUserImg;

        @BindView(R.id.tailor_list_rating_bar)
        RatingBar mTailorListRatingBar;

        @BindView(R.id.tailor_direction_txt)
        TextView mTailorDirectionTxt;

        @BindView(R.id.tailor_list_dir_lay)
        RelativeLayout mTailorListDirPayLay;

        @BindView(R.id.tailor_list_rating_bar_lay)
        RelativeLayout mTailorListRatingBarLay;

        @BindView(R.id.tailor_price_txt)
        TextView mTailorPriceTxt;

        @BindView(R.id.tailor_list_adapter_price_lay)
        LinearLayout mTailorListAdapterLay;

        @BindView(R.id.tailor_list_card_view)
        CardView mTailorListCardView;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}

