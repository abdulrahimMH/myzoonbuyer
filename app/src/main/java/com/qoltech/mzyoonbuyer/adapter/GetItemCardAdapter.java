package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.StoreCartEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.ui.OrderSummaryScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GetItemCardAdapter extends RecyclerView.Adapter<GetItemCardAdapter.Holder> {

    private Context mContext;
    private ArrayList<StoreCartEntity> mRelatedProductsEntity;

    private UserDetailsEntity mUserDetailsEntityRes;
    public GetItemCardAdapter(ArrayList<StoreCartEntity> relatedProductsEntities, Context activity) {
        mContext = activity;
        mRelatedProductsEntity = relatedProductsEntities;
    }

    @NonNull
    @Override
    public GetItemCardAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_get_card_items_list, parent, false);
        return new GetItemCardAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final GetItemCardAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

//        if (mRelatedProductsEntity.get(position).getProductName().equalsIgnoreCase("")){
//            holder.mGetCardItemProdNameTxt.setText(mRelatedProductsEntity.get(position).getProduct_Name() + " | " + mRelatedProductsEntity.get(position).getNewPrice()*mRelatedProductsEntity.get(position).getQuantity());
//
//        }else {
//            holder.mGetCardItemProdNameTxt.setText(mRelatedProductsEntity.get(position).getProductName() + " | " + mRelatedProductsEntity.get(position).getNewPrice()*mRelatedProductsEntity.get(position).getQuantity());
//
//        }
        String sizeStr = mRelatedProductsEntity.get(position).getSizeId() == -1 ? mContext.getResources().getString(R.string.not_available) : mRelatedProductsEntity.get(position).getSize();
        String colorStr = mRelatedProductsEntity.get(position).getColorId() == -1 ? mContext.getResources().getString(R.string.not_available) : mRelatedProductsEntity.get(position).getColorName();

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mGetCardItemProdNameTxt.setText(mRelatedProductsEntity.get(position).getProductNameInArabic() + " | " + mContext.getResources().getString(R.string.price) + " : " + mRelatedProductsEntity.get(position).getNewPrice());
            holder.mGetCardItemQtyTxt.setText(mContext.getResources().getString(R.string.size) + " : " + sizeStr + " | " + mContext.getResources().getString(R.string.color) + " : " + colorStr +  " | " + mContext.getResources().getString(R.string.qty) +" : " + mRelatedProductsEntity.get(position).getQuantity()+ " | " + mContext.getResources().getString(R.string.brand) + " : "+mRelatedProductsEntity.get(position).getBrandNameInArabic() + " | " +  mContext.getResources().getString(R.string.tot) + " : " +mRelatedProductsEntity.get(position).getUnitTotal());

        }else {
            if (mRelatedProductsEntity.get(position).getProductName().equalsIgnoreCase("")){
            holder.mGetCardItemProdNameTxt.setText(mRelatedProductsEntity.get(position).getProduct_Name() + " | " + mContext.getResources().getString(R.string.price) + " : " + mRelatedProductsEntity.get(position).getNewPrice());

        }else {
                holder.mGetCardItemProdNameTxt.setText(mRelatedProductsEntity.get(position).getProductName() + " | " + mContext.getResources().getString(R.string.price) + " : " + mRelatedProductsEntity.get(position).getNewPrice());
            }
            holder.mGetCardItemQtyTxt.setText(mContext.getResources().getString(R.string.size) + " : " + sizeStr + " | " + mContext.getResources().getString(R.string.color) + " : " + colorStr +  " | " + mContext.getResources().getString(R.string.qty) +" : " + mRelatedProductsEntity.get(position).getQuantity()+ " | " + mContext.getResources().getString(R.string.brand) + " : "+mRelatedProductsEntity.get(position).getBrandName() + " | " +  mContext.getResources().getString(R.string.tot) + " : " +mRelatedProductsEntity.get(position).getUnitTotal());

        }

        holder.mGetCardItemDeleteImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.getInstance().showOptionPopup(mContext, mContext.getResources().getString(R.string.sure_want_to_delete_item_from_card), mContext.getResources().getString(R.string.yes), mContext.getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                    @Override
                    public void onNegativeClick() {

                    }

                    @Override
                    public void onPositiveClick() {
                        AppConstants.TRANSACTION_AMOUNT = String.valueOf(Double.parseDouble(AppConstants.TRANSACTION_AMOUNT ) - mRelatedProductsEntity.get(position).getNewPrice()*mRelatedProductsEntity.get(position).getQuantity());

                        ((OrderSummaryScreen)mContext).removeFromCart(mRelatedProductsEntity.get(position).getProductId(),String.valueOf(mRelatedProductsEntity.get(position).getSizeId()),String.valueOf(mRelatedProductsEntity.get(position).getColorId()),String.valueOf(mRelatedProductsEntity.get(position).getSellerId()));
//                        if (!AppConstants.CARD_ID.equalsIgnoreCase("")){
//                            ((OrderSummaryScreen)mContext).getCardItemsApiCall(PreferenceUtil.getStringValue(mContext,AppConstants.CARD_ID));
//                        }
                        notifyDataSetChanged();
                    }
                });

            }
        });

        holder.mGetCardItemDeleteImg.setVisibility(AppConstants.ORDER_SUMMARY.equalsIgnoreCase("ORDER_SUMMARY") ? View.VISIBLE : View.GONE);

        if (mRelatedProductsEntity.get(position).getProductImage().equalsIgnoreCase("")){
            try {
                Glide.with(mContext)
                        .load(AppConstants.IMAGE_BASE_URL+"Images/Products/"+mRelatedProductsEntity.get(position).getImage())
                        .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                        .into(holder.mGetCardItemImg);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
        }else {
            try {
                Glide.with(mContext)
                        .load(AppConstants.IMAGE_BASE_URL+"Images/Products/"+mRelatedProductsEntity.get(position).getProductImage())
                        .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                        .into(holder.mGetCardItemImg);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
        }
    }

    @Override
    public int getItemCount() {
        return mRelatedProductsEntity.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.get_card_item_dress_name_txt)
        TextView mGetCardItemProdNameTxt;

        @BindView(R.id.get_card_item_qty_txt)
        TextView mGetCardItemQtyTxt;

        @BindView(R.id.get_card_item_dress_img)
        ImageView mGetCardItemImg;

        @BindView(R.id.get_card_item_delete_img)
        ImageView mGetCardItemDeleteImg;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
