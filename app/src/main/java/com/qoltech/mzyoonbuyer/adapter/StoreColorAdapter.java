package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetProductColorEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreColorAdapter extends RecyclerView.Adapter<StoreColorAdapter.Holder> {

    ArrayList<GetProductColorEntity> mList;
    Context mContext;

    public StoreColorAdapter(ArrayList<GetProductColorEntity> list, Context context) {
        mList = list;
        mContext = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_store_color_list,viewGroup,false);
        return new StoreColorAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        try {
            int color = Color.parseColor(mList.get(position).getColorCode().trim());
            ColorDrawable colorDrawable = new ColorDrawable(color);

            Glide.with(mContext).load(colorDrawable)
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_background_img).error(R.drawable.dress_type_background_img))
                    .into(holder.mAdapterStoreColorImg);

        }catch (Exception e){

            Glide.with(mContext).load(R.drawable.dress_type_background_img)
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_background_img).error(R.drawable.dress_type_background_img))
                    .into(holder.mAdapterStoreColorImg);
        }

        if (mList.size() == 1){
            mList.get(0).setChecked(true);
            AppConstants.STORE_COLOR_ID = String.valueOf(mList.get(0).getId());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0; i<mList.size(); i++){
                    mList.get(i).setChecked(false);
                }
                mList.get(position).setChecked(true);
                AppConstants.STORE_COLOR_ID = String.valueOf(mList.get(position).getId());

                notifyDataSetChanged();
            }
        });

        holder.mAdapterStoreColorParLay.setBackgroundResource(mList.get(position).isChecked() ? R.drawable.border_with_transparent : R.drawable.transaparent);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.adapter_store_color_img)
        ImageView mAdapterStoreColorImg;

        @BindView(R.id.adapter_store_color_par_lay)
        RelativeLayout mAdapterStoreColorParLay;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
