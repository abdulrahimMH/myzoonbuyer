package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetStoreProductDetailsEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.ui.OldOrderDetailsScreen;
import com.qoltech.mzyoonbuyer.ui.StoreOrderDetailScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreOtherItemAdapter extends RecyclerView.Adapter<StoreOtherItemAdapter.Holder> {

    private Context mContext;
    private ArrayList<GetStoreProductDetailsEntity> mStoreOrderDetailsList;
    private UserDetailsEntity mUserDetailsEntityRes;

    public StoreOtherItemAdapter(Context activity, ArrayList<GetStoreProductDetailsEntity> storeOrderDetailsEntities) {
        mContext = activity;
        mStoreOrderDetailsList = storeOrderDetailsEntities;
    }

    @NonNull
    @Override
    public StoreOtherItemAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_old_store_order_details_list, parent, false);
        return new StoreOtherItemAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final StoreOtherItemAdapter.Holder holder, final int position) {

        mUserDetailsEntityRes = new UserDetailsEntity();

        final GetStoreProductDetailsEntity storeProductEntity = mStoreOrderDetailsList.get(position);

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mStoreOrderDetailsList.get(position).getType().equalsIgnoreCase("Store")){
                    AppConstants.ORDER_PENDING_LINE_ID = mStoreOrderDetailsList.get(position).getStoreId();
                    ((StoreOrderDetailScreen)mContext).initView();
                }else {
                    AppConstants.ORDER_PENDING_ID = String.valueOf(mStoreOrderDetailsList.get(position).getOrderId());
                    AppConstants.ORDER_TYPE_SKILL = mStoreOrderDetailsList.get(position).getType();
                    ((StoreOrderDetailScreen)mContext).nextScreen(OldOrderDetailsScreen.class,true);
                }
            }
        });

        holder.mAdapStoreProductNameTxt.setText(mStoreOrderDetailsList.get(position).getProductName());
        holder.mAdapStorePriceTxt.setText(String.valueOf(mStoreOrderDetailsList.get(position).getPrice()));

        if (storeProductEntity.getType().equalsIgnoreCase("Store")){
            try {
                Glide.with(mContext)
                        .load(AppConstants.IMAGE_BASE_URL + "images/Products/"+storeProductEntity.getImage())
                        .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                        .into(holder.mGridViewImgLay);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
        }else {
            try {
                Glide.with(mContext)
                        .load(AppConstants.IMAGE_BASE_URL + "images/DressSubType/"+storeProductEntity.getImage())
                        .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                        .into(holder.mGridViewImgLay);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
        }
    }

    @Override
    public int getItemCount() {
        return mStoreOrderDetailsList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adap_store_details_img)
        ImageView mGridViewImgLay;

        @BindView(R.id.adap_store_product_name_txt)
        TextView mAdapStoreProductNameTxt;

        @BindView(R.id.adap_store_order_price_txt)
        TextView mAdapStorePriceTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

