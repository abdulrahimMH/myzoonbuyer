package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.RecentBrandEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.StoreProductListScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreTopBrandsAdapter extends RecyclerView.Adapter<StoreTopBrandsAdapter.Holder> {

    Context mContext;

    ArrayList<RecentBrandEntity> mTopBrandsList;

    UserDetailsEntity mUserDetailsEntityRes;

    public StoreTopBrandsAdapter(Context context, ArrayList<RecentBrandEntity> topBrandsList) {
        mContext = context;
        mTopBrandsList = topBrandsList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_store_top_brands_list,viewGroup,false);

        return new StoreTopBrandsAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);


        Glide.with(mContext)
                .load(AppConstants.IMAGE_BASE_URL+"Images/Brands/"+mTopBrandsList.get(position).getProductImage())
                .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                .into(holder.mStoreBrandsImg);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.STORE_BANNER_ID = "7";
                AppConstants.STORE_SEARCH = "";
                AppConstants.STORE_TYPE_ID = String.valueOf(mTopBrandsList.get(position).getBrandId());
                ((BaseActivity)mContext).nextScreen(StoreProductListScreen.class,true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTopBrandsList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.adap_store_brands_img)
        ImageView mStoreBrandsImg;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
