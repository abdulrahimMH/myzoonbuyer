package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.DressSubTypeImagesEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.ui.CustomizationThreeScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationDressTypeAdapter extends RecyclerView.Adapter<CustomizationDressTypeAdapter.Holder> {

    private Context mContext;
    private ArrayList<DressSubTypeImagesEntity> mCustomizationDressEntity;

    private UserDetailsEntity mUserDetailsEntityRes;
    public CustomizationDressTypeAdapter(Context activity, ArrayList<DressSubTypeImagesEntity> customizationDressEntity) {
        mContext = activity;
        mCustomizationDressEntity = customizationDressEntity;
    }

    @NonNull
    @Override
    public CustomizationDressTypeAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_customization_dress_type, parent, false);
        return new CustomizationDressTypeAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomizationDressTypeAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final DressSubTypeImagesEntity dressSubTypeImagesEntity = mCustomizationDressEntity.get(position);

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"images/Customazation3/"+dressSubTypeImagesEntity.getImages())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0; i<mCustomizationDressEntity.size(); i++){
                    mCustomizationDressEntity.get(i).setChecked(false);
                }

                mCustomizationDressEntity.get(position).setChecked(true);

                try {
                    Glide.with(mContext)
                            .load(AppConstants.IMAGE_BASE_URL+"images/Customazation3/"+mCustomizationDressEntity.get(position).getImages())
                            .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                            .into(((CustomizationThreeScreen)mContext).mCustomizationThreeFullImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

                notifyDataSetChanged();
            }
        });
        holder.mGridCustimizeImg.setVisibility(dressSubTypeImagesEntity.isChecked() ? View.VISIBLE : View.GONE);

    }

    @Override
    public int getItemCount() {
        return mCustomizationDressEntity.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_custimize_view_lay)
        LinearLayout mGridViewLay;

        @BindView(R.id.grid_custimize_view_img_lay)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_custimize_img)
        ImageView mGridCustimizeImg;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
