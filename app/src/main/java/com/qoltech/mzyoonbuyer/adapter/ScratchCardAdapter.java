package com.qoltech.mzyoonbuyer.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anupkumarpanwar.scratchview.ScratchView;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetScrachCardEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.ui.ScratchListScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScratchCardAdapter extends RecyclerView.Adapter<ScratchCardAdapter.Holder>  {

    private Context mContext;

    private UserDetailsEntity mUserDetailsEntityRes;

    Dialog mDialog;

    ArrayList<GetScrachCardEntity> mGetScrachCardList;

    ScratchListScreen mScratchListScreen;

    public ScratchCardAdapter(Context context, ArrayList<GetScrachCardEntity> getScrachCardEntities,ScratchListScreen scratchListScreen) {
        mContext = context;
        mGetScrachCardList = getScrachCardEntities;
        mScratchListScreen = scratchListScreen;
    }

    @NonNull
    @Override
    public ScratchCardAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_scratch_card, parent, false);
        return new ScratchCardAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ScratchCardAdapter.Holder holder, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    GetScrachCardEntity getScrachCardEntity = new GetScrachCardEntity();
                    getScrachCardEntity = mGetScrachCardList.get(position);
                AppConstants.SCRATCH_CARD_COUNT = "0";

                    scarchDialogue(getScrachCardEntity);

            }
        });

        if(mGetScrachCardList.get(position).getPoints() > 0){
            holder.mScartchCardPointsTxt.setText(mContext.getResources().getString(R.string.have_won_txt) +"\n" +String.valueOf(mGetScrachCardList.get(position).getPoints()) + "\n"+mContext.getResources().getString(R.string.points));

        }else {
            holder.mScartchCardPointsTxt.setText(mContext.getResources().getString(R.string.better_luck_next_time) +"\n" +String.valueOf(mGetScrachCardList.get(position).getPoints()) + "\n"+mContext.getResources().getString(R.string.points));

        }

        holder.mScratchCardClosedImg.setVisibility(mGetScrachCardList.get(position).isIsreedemed() ? View.GONE : View.VISIBLE );

    }

    @Override
    public int getItemCount() {
        return mGetScrachCardList.size();
    }

    public void scarchDialogue(GetScrachCardEntity getScrachCardEntity){
        alertDismiss(mDialog);
        mDialog = getDialog(mContext, R.layout.pop_up_scratch_card_screen);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(mDialog.findViewById(R.id.scratch_card_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(mDialog.findViewById(R.id.scratch_card_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        ImageView mCloseImg;
        TextView mPointsTxt,mCongragulationTxt;
        com.anupkumarpanwar.scratchview.ScratchView mScratchView;
        RelativeLayout mTellYourFriendsLay;
        AppConstants.SCRATCH_CARD_COUNT = "1";

        /*Init view*/
        mCloseImg = mDialog.findViewById(R.id.reward_close_img);
        mScratchView = mDialog.findViewById(R.id.scratch_view);
        mTellYourFriendsLay = mDialog.findViewById(R.id.scracth_tell_your_frd_lay);
        mPointsTxt = mDialog.findViewById(R.id.pop_up_scratch_card_points_txt);
//        mDateTxt = mDialog.findViewById(R.id.pop_up_scratch_card_date_txt);
        mCongragulationTxt = mDialog.findViewById(R.id.scratch_congratulation_card_txt);

        if (getScrachCardEntity.isIsreedemed()){
            mScratchView.setVisibility(View.GONE);
            mPointsTxt.setVisibility(View.VISIBLE);
            if (getScrachCardEntity.getPoints() > 0){
                mTellYourFriendsLay.setVisibility(View.VISIBLE);
                mCongragulationTxt.setText(mContext.getResources().getString(R.string.scratch_card_congratulation) +" " + String.valueOf(getScrachCardEntity.getOrderDt()));
            }else {
                mCongragulationTxt.setVisibility(View.GONE);
            }
        }else {
            mCongragulationTxt.setText(mContext.getResources().getString(R.string.earn_up_to_2000));

        }
        if (getScrachCardEntity.getPoints() > 0){
            mPointsTxt.setText(mContext.getResources().getString(R.string.have_won_txt) +"\n" +String.valueOf(getScrachCardEntity.getPoints()) + "\n"+mContext.getResources().getString(R.string.points));

        }else {
            mPointsTxt.setText(mContext.getResources().getString(R.string.better_luck_next_time) +"\n" +String.valueOf(getScrachCardEntity.getPoints()) + "\n"+mContext.getResources().getString(R.string.points));

        }


        mScratchView.setRevealListener(new ScratchView.IRevealListener() {
            @Override
            public void onRevealed(ScratchView scratchView) {

            }

            @Override
            public void onRevealPercentChangedListener(ScratchView scratchView, float percent) {

                if (percent > 0.3){
                    scratchView.reveal();
                    mPointsTxt.setVisibility(View.VISIBLE);
                    if (getScrachCardEntity.getPoints() > 0){
                        mTellYourFriendsLay.setVisibility(View.VISIBLE);
                        mCongragulationTxt.setText(mContext.getResources().getString(R.string.scratch_card_congratulation) +" " + String.valueOf(getScrachCardEntity.getOrderDt()));
                    }else {
                        mCongragulationTxt.setVisibility(View.GONE);

                    }
                    if (AppConstants.SCRATCH_CARD_COUNT.equalsIgnoreCase("1")){
                        AppConstants.SCRATCH_CARD_COUNT = "0";
                        updateScratchRewardPointsApiCall(getScrachCardEntity);

                    }
                }

            }
        });

        mCloseImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                AppConstants.SCRATCH_CARD_COUNT = "0";
                if (mScratchView.isRevealed()&& !getScrachCardEntity.isIsreedemed()){
                    ((ScratchListScreen)mContext).onResume();
                }
            }
        });
        mTellYourFriendsLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareBodys = mContext.getResources().getString(R.string.hi_earned) + " " +String.valueOf(getScrachCardEntity.getPoints()) + " "+ mContext.getResources().getString(R.string.points) + mContext.getResources().getString(R.string.from_application);
                Intent sharingIntents = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntents.setType("text/plain");
                sharingIntents.putExtra(android.content.Intent.EXTRA_SUBJECT, mContext.getResources().getString(R.string.invite_friend_and_get_point));
                sharingIntents.putExtra(android.content.Intent.EXTRA_TEXT, shareBodys);
                mContext.startActivity(Intent.createChooser(sharingIntents, mContext.getResources().getString(R.string.app_name)));

            }
        });

        alertShowing(mDialog);


    }
    public void updateScratchRewardPointsApiCall(GetScrachCardEntity getScrachCardEntity){
        if (NetworkUtil.isNetworkAvailable(mContext)){
            APIRequestHandler.getInstance().updateScratchCardApiCall(String.valueOf(getScrachCardEntity.getId()),mScratchListScreen);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(mContext, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    updateScratchRewardPointsApiCall(getScrachCardEntity);
                }
            });
        }
    }
    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.scrtch_card_closed_img)
        ImageView mScratchCardClosedImg;

        @BindView(R.id.scartch_card_ponts_txt)
        TextView mScartchCardPointsTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }

    }
    /*Default dialog init method*/
    public Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }
}
