package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetAddressEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.ui.AddAddressScreen;
import com.qoltech.mzyoonbuyer.ui.AddressScreen;
import com.qoltech.mzyoonbuyer.ui.CheckoutScreen;
import com.qoltech.mzyoonbuyer.ui.DeliveryTypeScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GetAddressAdapter extends RecyclerView.Adapter<GetAddressAdapter.Holder> {

    private Context mContext;
    private ArrayList<GetAddressEntity> mAddressList;
    AddressScreen mAddressScreen;
    String mUserId;
    private UserDetailsEntity mUserDetailsEntityRes;

    public GetAddressAdapter(Context activity, ArrayList<GetAddressEntity> addressList,AddressScreen addressScreen,String userId) {
        mContext = activity;
        mAddressList = addressList;
        mAddressScreen = addressScreen;
        mUserId = userId;
    }

    @NonNull
    @Override
    public GetAddressAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_get_address, parent, false);
        return new GetAddressAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final GetAddressAdapter.Holder holder, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final GetAddressEntity getAddressEntity = mAddressList.get(position);

        holder.mGetAddressUserNameTxt.setText(getAddressEntity.getFirstName());

        holder.mGetAddressTxt.setText(getAddressEntity.getArea()+", "+getAddressEntity.getStateName()+", "+getAddressEntity.getName()+".");

        holder.mGetAddressPhoneNumTxt.setText(getAddressEntity.getPhoneNo());
        holder.mGetAddressMakeAsDefaultTxt.setVisibility(getAddressEntity.isDefault() ? View.VISIBLE : View.GONE);
        holder.mGetAddressLocationTxt.setText(getAddressEntity.getLocationType());

        holder.mEditLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.EDIT_ADDRESS = "EDIT_ADDRESS";
                AppConstants.CHECK_ADD_ADDRESS = "ADD_ADDRESS_EDIT";
                AppConstants.GET_ADDRESS_ID = String.valueOf(getAddressEntity.getId());
                ((BaseActivity)mContext).nextScreen(AddAddressScreen.class,true);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.GET_ADDRESS_ID = String.valueOf(getAddressEntity.getId());
                AppConstants.DELIVERY_AREA_ID = String.valueOf(getAddressEntity.getAreaId());
                AppConstants.GET_ADDRESS =getAddressEntity.getFirstName()+", "+ getAddressEntity.getArea()+", "+getAddressEntity.getStateName()+", "+getAddressEntity.getName()+".";
                if (AppConstants.SERVICE_TYPE.equalsIgnoreCase("SERVICE_TYPE")){
                    AppConstants.CHECK_ADD_ADDRESS = "";
                    AppConstants.PENDING_DELIVERY_CLICK = "";
                    AppConstants.COUNTRY_AREA_ID = String.valueOf(getAddressEntity.getAreaId());

                    ((BaseActivity)mContext).nextScreen(DeliveryTypeScreen.class, true);
                }
                if (AppConstants.SERVICE_TYPE.equalsIgnoreCase("CART")){

                    AppConstants.STORE_USER_DETAILS.setFirstName(getAddressEntity.getFirstName());
                    AppConstants.STORE_USER_DETAILS.setSecondName(getAddressEntity.getLastName());
                    AppConstants.STORE_USER_DETAILS.setLineOne(getAddressEntity.getLandMark());
                    AppConstants.STORE_USER_DETAILS.setLineTwo("");
                    AppConstants.STORE_USER_DETAILS.setLineThree("");
                    AppConstants.STORE_USER_DETAILS.setCity(getAddressEntity.getStateName());
                    AppConstants.STORE_USER_DETAILS.setRegion(getAddressEntity.getArea());
                    AppConstants.STORE_USER_DETAILS.setCountry(getAddressEntity.getName());

                    ((BaseActivity)mContext).nextScreen(CheckoutScreen.class, true);
                }
            }
        });

        holder.mDeleteLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            DialogManager.getInstance().showOptionPopup(mContext, mContext.getResources().getString(R.string.address_delete_msg), mContext.getResources().getString(R.string.yes), mContext.getResources().getString(R.string.no),mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                @Override
                public void onNegativeClick() {

                }

                @Override
                public void onPositiveClick() {
                    if (NetworkUtil.isNetworkAvailable(mContext)){
                        deleteAddressApi(mUserId,String.valueOf(getAddressEntity.getId()));

                    }else {
                        DialogManager.getInstance().showNetworkErrorPopup(mContext, new InterfaceBtnCallBack() {
                            @Override
                            public void onPositiveClick() {
                                deleteAddressApi(mUserId,String.valueOf(getAddressEntity.getId()));
                            }
                        });
                    }
                }
            });
            }
        });

    }

    public void deleteAddressApi(String buyerId, String Id){
        APIRequestHandler.getInstance().deleteAddressApiCall(buyerId,Id,mAddressScreen);
    }
    @Override
    public int getItemCount() {
        return mAddressList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.get_address_edit_lay)
        LinearLayout mEditLay;

        @BindView(R.id.get_address_delete_lay)
        LinearLayout mDeleteLay;

        @BindView(R.id.get_address_user_name_txt)
        TextView mGetAddressUserNameTxt;

        @BindView(R.id.get_address_txt)
        TextView mGetAddressTxt;

        @BindView(R.id.get_address_phone_num)
        TextView mGetAddressPhoneNumTxt;

        @BindView(R.id.address_default_address_lay)
        RelativeLayout mGetAddressMakeAsDefaultTxt;

        @BindView(R.id.get_address_location_txt)
        TextView mGetAddressLocationTxt;

        @BindView(R.id.address_adap_par_lay)
        CardView mAddressAdapParLay;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


