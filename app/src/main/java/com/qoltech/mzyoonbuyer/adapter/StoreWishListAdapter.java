package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.entity.WishListEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.StoreProductViewDetailsScreen;
import com.qoltech.mzyoonbuyer.ui.StoreWishListScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreWishListAdapter extends RecyclerView.Adapter<StoreWishListAdapter.Holder> {

    ArrayList<WishListEntity> mWishList;

    Context mContext;

    UserDetailsEntity mUserDetailsEntityRes;

    public StoreWishListAdapter(ArrayList<WishListEntity> wishList, Context context) {
        mWishList = wishList;
        mContext = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_wish_list,viewGroup,false);
        return new StoreWishListAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        WishListEntity list = mWishList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mWishListProdNameTxt.setText(list.getProductNameInArabic());
            holder.mAdapterWishListBrandTxt.setText(list.getBrandNameInArabic());
            holder.mWishListShopNameTxt.setText(list.getShopNameInArabic());
        }else {
            holder.mWishListProdNameTxt.setText(list.getProductName());
            holder.mAdapterWishListBrandTxt.setText(list.getBrandName());
            holder.mWishListShopNameTxt.setText(list.getShopNameInEnglish());

        }

        holder.mWishListSizeTxt.setText(list.getSizeId() == -1 ? mContext.getResources().getString(R.string.not_available) : list.getSize());
        holder.mWishListColorEmptyTxt.setText(mContext.getResources().getString(R.string.not_available));
        holder.mWishListColorEmptyTxt.setVisibility(list.getColorId() == -1 ? View.VISIBLE : View.GONE);
        holder.mWishListColorImgParLay.setVisibility(list.getColorId() == -1 ? View.GONE : View.VISIBLE);
        holder.mWishListAmtTxt.setText(String.valueOf(list.getNewPrice()));

        try {
            int color = Color.parseColor(list.getColorCode().trim());
            ColorDrawable colorDrawable = new ColorDrawable(color);

            Glide.with(mContext).load(colorDrawable)
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_background_img).error(R.drawable.dress_type_background_img))
                    .into(holder.mWishListColorImg);

        }catch (Exception e){

            Glide.with(mContext).load(R.drawable.dress_type_background_img)
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_background_img).error(R.drawable.dress_type_background_img))
                    .into(holder.mWishListColorImg);
        }

        Glide.with(mContext)
                .load(AppConstants.IMAGE_BASE_URL+"Images/Products/"+list.getProductImage())
                .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                .into(holder.mAdapterWishListProdImg);

        holder.mAdapterWishListProdImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.STORE_PRODUCT_ID = mWishList.get(position).getProductId();
                AppConstants.STORE_SELLER_ID = String.valueOf(mWishList.get(position).getSellerId());
                ((BaseActivity)mContext).nextScreen(StoreProductViewDetailsScreen.class,true);
            }
        });

        holder.mWishListDeletImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.getInstance().showOptionPopup(mContext, mContext.getResources().getString(R.string.are_sure_you_want_to_delte_this_product_from_your_wish_list), mContext.getResources().getString(R.string.yes), mContext.getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                    @Override
                    public void onNegativeClick() {

                    }

                    @Override
                    public void onPositiveClick() {
                        ((StoreWishListScreen)mContext).removeToWishListApiCall(mWishList.get(position).getProductId(),String.valueOf(mWishList.get(position).getSizeId()),String.valueOf(mWishList.get(position).getColorId()),String.valueOf(mWishList.get(position).getSellerId()));

                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return mWishList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.adapter_wish_list_prod_name_txt)
        TextView mWishListProdNameTxt;

        @BindView(R.id.adapter_wish_list_delete_icon)
        ImageView mWishListDeletImg;

        @BindView(R.id.adapter_wish_list_brand_txt)
        TextView mAdapterWishListBrandTxt;

        @BindView(R.id.adapter_wish_list_size_txt)
        TextView mWishListSizeTxt;

        @BindView(R.id.adapter_wish_list_color_img)
        ImageView mWishListColorImg;

        @BindView(R.id.adapter_wish_list_color_txt)
        TextView mWishListColorEmptyTxt;

        @BindView(R.id.adapter_wish_list_amount_txt)
        TextView mWishListAmtTxt;

        @BindView(R.id.adapter_wish_list_prod_img)
        ImageView mAdapterWishListProdImg;

        @BindView(R.id.adapter_wish_list_shop_name_txt)
        TextView mWishListShopNameTxt;

        @BindView(R.id.adapter_wish_list_color_img_lay)
        CardView mWishListColorImgParLay;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
