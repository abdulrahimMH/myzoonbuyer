package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.AddMeasurementEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.MeasurementListScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddMeasurementListAdapter  extends RecyclerView.Adapter<AddMeasurementListAdapter.Holder> {

    private Context mContext;
    private ArrayList<AddMeasurementEntity> mMeasurementList;

    private UserDetailsEntity mUserDetailsEntityRes;

    public AddMeasurementListAdapter(Context activity,ArrayList<AddMeasurementEntity> measurementList) {
        mContext = activity;
        mMeasurementList = measurementList;
    }

    @NonNull
    @Override
    public AddMeasurementListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_add_measurement_list, parent, false);
        return new AddMeasurementListAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AddMeasurementListAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        AddMeasurementEntity mList = mMeasurementList.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.ORDER_DETAILS_MEASUREMENT = "ADD_MEASUREMENT";
                AppConstants.NEW_ORDER = "ADD_MEASUREMENT";
                AppConstants.REQUEST_LIST_ID = String.valueOf(mList.getId());
                ((BaseActivity)mContext).nextScreen(MeasurementListScreen.class,true);
            }
        });
        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/DressSubType/"+mList.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mMeasurementImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }
        holder.mAddMeasurementNameTxt.setText(mList.getName());
        holder.mAddMeasurementListMeasurementDateTxt.setText(mList.getCreatedOn());
        holder.mMeasurementListMeasurmentByTxt.setText(mList.getMeasurementBy());
        if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mAddMeasuremenDressTypeTxt.setText(mList.getGender() + " - " + mList.getNameInEnglish());
        }else {
            holder.mAddMeasuremenDressTypeTxt.setText(mList.getNameInEnglish() + " - " + mList.getGender());
        }

    }

    @Override
    public int getItemCount() {
        return mMeasurementList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_image)
        de.hdodenhof.circleimageview.CircleImageView mMeasurementImg;

        @BindView(R.id.add_measurement_list_measurement_name_txt)
        TextView mAddMeasurementNameTxt;

        @BindView(R.id.add_measurement_list_dress_type_txt)
        TextView mAddMeasuremenDressTypeTxt;

        @BindView(R.id.add_measurement_list_measurement_by_txt)
        TextView mMeasurementListMeasurmentByTxt;

        @BindView(R.id.add_measurement_list_measurement_date_txt)
        TextView mAddMeasurementListMeasurementDateTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


