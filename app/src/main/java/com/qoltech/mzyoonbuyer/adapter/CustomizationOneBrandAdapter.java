package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.CustomizationBrandEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationOneBrandAdapter extends RecyclerView.Adapter<CustomizationOneBrandAdapter.Holder> {

    private Context mContext;
    private ArrayList<CustomizationBrandEntity> mCustomizeBrandList;
    int mTotalInt = 1;

    private UserDetailsEntity mUserDetailsEntityRes;
    public CustomizationOneBrandAdapter(Context activity, ArrayList<CustomizationBrandEntity> customizeBrandList) {
        mContext = activity;
        mCustomizeBrandList = customizeBrandList;
    }

    @NonNull
    @Override
    public CustomizationOneBrandAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_brands_grid_view, parent, false);
        return new CustomizationOneBrandAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomizationOneBrandAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        final CustomizationBrandEntity brandEntity = mCustomizeBrandList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mGridViewTxt.setText(brandEntity.getBrandInArabic());

        }else {
            holder.mGridViewTxt.setText(brandEntity.getBrandInEnglish());
        }

        holder.mGridViewTxt.setVisibility(View.GONE);
        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/Brands/"+brandEntity.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mCustomizeBrandList.get(position).getBrandInEnglish().equalsIgnoreCase("All Brands")){
                    if (mCustomizeBrandList.get(position).getChecked()){
                        for (int i=0; i<mCustomizeBrandList.size(); i++) {
                            mCustomizeBrandList.get(i).setChecked(false);
                        }
                        AppConstants.BRANDS_ID = "";
                        AppConstants.BRANDS_NAME = "";

                    }else {
                        AppConstants.BRANDS_ID = "";
                        AppConstants.BRANDS_NAME = "";
                        for (int i=0; i<mCustomizeBrandList.size(); i++) {
                            mCustomizeBrandList.get(i).setChecked(true);
                            AppConstants.BRANDS_ID =String.valueOf(mCustomizeBrandList.get(position).getId());
                            AppConstants.BRANDS_NAME =String.valueOf(mCustomizeBrandList.get(position).getBrandInArabic());
                        }
                    }
//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        ((CustomizationOneScreen)mContext).getNewFlowCustomizationNewApiCall();
//
//                    }else {
//                        ((CustomizationOneScreen)mContext).getNewCustomizationApiCall();
//
//                    }

                    notifyDataSetChanged();

                }else {
                    if (mCustomizeBrandList.get(position).getChecked()){
                        mCustomizeBrandList.get(position).setChecked(false);
                        for (int i=0; i<mCustomizeBrandList.size() ; i++)
                        {
                            if (mCustomizeBrandList.get(i).getBrandInEnglish().equalsIgnoreCase("All Brands"))
                            {
                                mCustomizeBrandList.get(i).setChecked(false);

                            }
                        }
                    }else {
                        mTotalInt = 1;
                        for (int i=0; i<mCustomizeBrandList.size();i++){
                            if (mCustomizeBrandList.get(i).getChecked()){
                                mTotalInt = mTotalInt + 1;
                            }
                            if (String.valueOf(mTotalInt).equalsIgnoreCase(String.valueOf(mCustomizeBrandList.size()-1))){
                                for (int j=0; j<mCustomizeBrandList.size(); j++){
                                    if (mCustomizeBrandList.get(j).getBrandInEnglish().equalsIgnoreCase("All Brands")){
                                        mCustomizeBrandList.get(j).setChecked(true);
                                    }
                                }

                            }
                        }
                        mCustomizeBrandList.get(position).setChecked(true);
                    }
                    AppConstants.BRANDS_ID = "";
                    for (int i=0; i<mCustomizeBrandList.size(); i++){
                        if (mCustomizeBrandList.get(i).getChecked()){
                            AppConstants.BRANDS_ID = AppConstants.BRANDS_ID.equalsIgnoreCase("") ? mCustomizeBrandList.get(i).getId()+"" : AppConstants.BRANDS_ID + ","+ mCustomizeBrandList.get(i).getId();

                            AppConstants.BRANDS_NAME =  AppConstants.BRANDS_NAME.equalsIgnoreCase("") ? mCustomizeBrandList.get(i).getBrandInArabic() : AppConstants.BRANDS_NAME + ","+ mCustomizeBrandList.get(i).getBrandInEnglish();
                        }
                    }
//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        ((CustomizationOneScreen)mContext).getNewFlowCustomizationNewApiCall();
//
//                    }else {
//                        ((CustomizationOneScreen)mContext).getNewCustomizationApiCall();
//
//                    }
                    notifyDataSetChanged();
                }

            }

        });
        holder.mGridCustimizeImg.setVisibility(brandEntity.getChecked() ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return mCustomizeBrandList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_custimize_view_lay)
        LinearLayout mGridViewLay;

        @BindView(R.id.grid_custimize_view_img_lay)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_custimize_view_txt)
        TextView mGridViewTxt;


        @BindView(R.id.grid_custimize_img)
        ImageView mGridCustimizeImg;



        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


