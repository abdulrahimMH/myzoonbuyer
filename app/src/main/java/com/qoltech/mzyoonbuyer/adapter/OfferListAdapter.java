package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetOfferCateogriesEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.ui.OffersScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OfferListAdapter  extends RecyclerView.Adapter<OfferListAdapter.Holder> {

    private Context mContext;

    private UserDetailsEntity mUserDetailsEntityRes;
    ArrayList<GetOfferCateogriesEntity> mGetOfferCategoresEntity;

    public OfferListAdapter(ArrayList<GetOfferCateogriesEntity> getOfferCateogriesEntities,Context activity) {
        mContext = activity;
        mGetOfferCategoresEntity = getOfferCateogriesEntities;
    }

    @NonNull
    @Override
    public OfferListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_offer_list, parent, false);
        return new OfferListAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OfferListAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        GetOfferCateogriesEntity mList = mGetOfferCategoresEntity.get(position);

        if (AppConstants.CHECK_OUT_OFFERS.equalsIgnoreCase("Apply")){
            holder.mOfferApplyTxt.setText(mContext.getResources().getString(R.string.apply));
        }else {
            holder.mOfferApplyTxt.setText(mContext.getResources().getString(R.string.tap_to_select));
        }
        if (mList.getDiscountType() == 1){
            holder.mOfferPriceTxt.setText(String.valueOf(mList.getDiscountValue()) +" "+mContext.getResources().getString(R.string.aed)+" " + mContext.getResources().getString(R.string.off));

        }else {
            holder.mOfferPriceTxt.setText(String.valueOf(mList.getDiscountValue()) +"% " + mContext.getResources().getString(R.string.off));

        }
        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mOfferMinimumValueTxt.setText( String.valueOf(mList.getMinimumAmount()) +" : "+ mContext.getResources().getString(R.string.minimum_order_value) );
            holder.mOfferMaximumTxt.setText(String.valueOf(mList.getMaximumDiscount()) +" : "+ mContext.getResources().getString(R.string.maximum_discount_amount) );
            holder.mOfferShopNameTxt.setText(mList.getShopNameInEnglish() +" : "+ mContext.getResources().getString(R.string.shop_name) );

        }else {
            holder.mOfferMinimumValueTxt.setText(mContext.getResources().getString(R.string.minimum_order_value) +" : "+ String.valueOf(mList.getMinimumAmount()));
            holder.mOfferMaximumTxt.setText(mContext.getResources().getString(R.string.maximum_discount_amount) +" : "+String.valueOf(mList.getMaximumDiscount()));
            holder.mOfferShopNameTxt.setText(mContext.getResources().getString(R.string.shop_name) +" : "+  mList.getShopNameInEnglish());

        }

        holder.mOfferPromoCodeTxt.setText(mContext.getResources().getString(R.string.promo_code)+"\n" + mList.getCouponcodeId());
        holder.mOfferExpirdDateTxt.setText(mContext.getResources().getString(R.string.expires_on) + "\n"+ mList.getValidTo());
        holder.mOfferNoteTxt.setText(mList.getMessage());

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/Offer/"+mList.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mOfferBodyImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        if (mUserDetailsEntityRes.getUSER_ID().equalsIgnoreCase("0")){
            holder.mOfferApplyTxt.setVisibility(View.INVISIBLE);
        }

        holder.mOfferApplyTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppConstants.CHECK_OUT_OFFERS.equalsIgnoreCase("Apply")){
                    AppConstants.APPLY_CODE = mGetOfferCategoresEntity.get(position).getCouponcodeId();
                    AppConstants.APPLY_CODE_ID = String.valueOf(mGetOfferCategoresEntity.get(position).getId());
//                    ((OffersScreen)mContext).onBackPressed();
                    ((OffersScreen)mContext).getCouponPriceApiCall(mGetOfferCategoresEntity.get(position).getCouponcodeId());
                }else {
                        AppConstants.TAP_TO_APPLY = mGetOfferCategoresEntity.get(position).getCouponcodeId();
                        AppConstants.TAP_TO_APPLY_CODE_ID = String.valueOf(mGetOfferCategoresEntity.get(position).getId());
                        Toast.makeText(mContext,mContext.getResources().getString(R.string.selected_coupon_code),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mGetOfferCategoresEntity.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.offer_body_img)
        ImageView mOfferBodyImg;

        @BindView(R.id.offer_bottom_tailor_lay)
        LinearLayout mOfferBottomTailorLay;

        @BindView(R.id.adapter_offer_promo_code_txt)
        TextView mOfferPromoCodeTxt;

        @BindView(R.id.adapter_offer_apply_lay)
        TextView mOfferApplyTxt;

        @BindView(R.id.adapter_offer_shop_name_txt)
        TextView mOfferShopNameTxt;

        @BindView(R.id.adapter_offer_note_txt)
        TextView mOfferNoteTxt;

        @BindView(R.id.adapter_offer_minimum_value_txt)
        TextView mOfferMinimumValueTxt;

        @BindView(R.id.adapter_offer_maximum_txt)
        TextView mOfferMaximumTxt;

        @BindView(R.id.adapter_offer_expird_date_txt)
        TextView mOfferExpirdDateTxt;

        @BindView(R.id.adapter_offer_txt)
        TextView mOfferPriceTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

