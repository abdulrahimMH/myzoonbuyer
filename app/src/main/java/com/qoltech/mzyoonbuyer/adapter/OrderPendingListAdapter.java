package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.OrderListPendingEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.OrderDetailsScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderPendingListAdapter extends RecyclerView.Adapter<OrderPendingListAdapter.Holder> {

    private Context mContext;
    private ArrayList<OrderListPendingEntity> mOrderPendingList;

    private UserDetailsEntity mUserDetailsEntityRes;
    public OrderPendingListAdapter(Context activity ,ArrayList<OrderListPendingEntity> orderPendingList) {
        mContext = activity;
        mOrderPendingList = orderPendingList;
    }

    @NonNull
    @Override
    public OrderPendingListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_order_list, parent, false);
        return new OrderPendingListAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderPendingListAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        OrderListPendingEntity mList = mOrderPendingList.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.ORDER_TYPE_SKILL = mOrderPendingList.get(position).getOrderType();
                AppConstants.APPROVE_REJECT_BTN = "false";
                AppConstants.InitiatedBy = mOrderPendingList.get(position).getInitiatedby();
                if (mList.getOrderType().equalsIgnoreCase("Store")){
                    AppConstants.PENDING_DELIVERY_CLICK = "Pending";

                    String[] part = mOrderPendingList.get(position).getProduct_Name().split(":");

                    AppConstants.ORDER_PENDING_LINE_ID = part[part.length-1];
                    AppConstants.ORDER_DETAILS_PENDING_ID = String.valueOf(mOrderPendingList.get(position).getOrderId());
//                    ((BaseActivity)mContext).nextScreen(StoreOrderDetailScreen.class,true);
                }else {
                    AppConstants.PENDING_DELIVERY_CLICK = "Pending";
                    String[] partOrderType = mOrderPendingList.get(position).getOrderType().split("-");

                    AppConstants.DIRECT_ORDER = partOrderType[partOrderType.length-1];
                    AppConstants.ORDER_SUB_TYPE = mOrderPendingList.get(position).getOrderSubType();
                    AppConstants.ORDER_DETAILS_PENDING_ID = String.valueOf(mOrderPendingList.get(position).getOrderId());
//                    ((BaseActivity)mContext).nextScreen(OldOrderDetailsScreen.class,true);
                }
                ((BaseActivity)mContext).nextScreen(OrderDetailsScreen.class,true);
            }
        });

        if (mList.getOrderType().equalsIgnoreCase("Store")){
            try {
                Glide.with(mContext)
                        .load(AppConstants.IMAGE_BASE_URL+"images/Products/"+mList.getImage())
                        .apply(new RequestOptions().placeholder(R.drawable.empty_img).error(R.drawable.empty_img))
                        .into(holder.mOrderListImg);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
        }else {
            try {
                Glide.with(mContext)
                        .load(AppConstants.IMAGE_BASE_URL+"Images/DressSubType/"+mList.getImage())
                        .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                        .into(holder.mOrderListImg);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
//            holder.mOrderListOrderTypeTxt.setText(mList.getOrderType());
            holder.mOrderListShopNameTxt.setText(mList.getShopNameInArabic());
        }else {
            holder.mOrderListShopNameTxt.setText(mList.getShopNameInEnglish());
        }

        holder.mOrderListOrderTypeTxt.setText(mList.getOrderType().equalsIgnoreCase("StitchingStore") ? mContext.getResources().getString(R.string.stitching_and_store) : mList.getOrderType());

        holder.mOrderListIdTxt.setText(String.valueOf(mList.getOrderId()));
        String[] parts = mOrderPendingList.get(position).getProduct_Name().split(":");

        holder.mOrderListProductNameTxt.setText(String.valueOf(parts[0]));
        holder.mOrderListDateTxt.setText(mList.getOrderDate().replace("T00:00:00",""));
    }

    @Override
    public int getItemCount() {
        return mOrderPendingList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.order_list_img)
        de.hdodenhof.circleimageview.CircleImageView mOrderListImg;

        @BindView(R.id.order_list_id_txt)
        TextView mOrderListIdTxt;

        @BindView(R.id.order_list_tailor_name_txt)
        TextView mOrderListOrderTypeTxt;

        @BindView(R.id.order_list_shop_name_txt)
        TextView mOrderListShopNameTxt;

        @BindView(R.id.order_list_product_name_txt)
        TextView mOrderListProductNameTxt;

        @BindView(R.id.order_list_date_txt)
        TextView mOrderListDateTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

