package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.CustomizationPlaceEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationOnePlaceAdapter extends RecyclerView.Adapter<CustomizationOnePlaceAdapter.Holder> {

    private Context mContext;
    private ArrayList<CustomizationPlaceEntity> mCustomizePlaceList;
    int mTotalInt = 1;

    private UserDetailsEntity mUserDetailsEntityRes;
    public CustomizationOnePlaceAdapter(Context activity, ArrayList<CustomizationPlaceEntity> customizePlaceList) {
        mContext = activity;
        mCustomizePlaceList = customizePlaceList;
    }

    @NonNull
    @Override
    public CustomizationOnePlaceAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_place_of_industry_grid_view, parent, false);
        return new CustomizationOnePlaceAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomizationOnePlaceAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final CustomizationPlaceEntity placeEntity = mCustomizePlaceList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mGridViewTxt.setText(placeEntity.getPlaceInArabic());

        }else {
            holder.mGridViewTxt.setText(placeEntity.getPlaceInEnglish());

        }


        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/PlaceOfIndustry/"+placeEntity.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCustomizePlaceList.get(position).getPlaceInEnglish().equalsIgnoreCase("All Industry")){
                    if (mCustomizePlaceList.get(position).getChecked()){
                        for (int i=0; i<mCustomizePlaceList.size(); i++) {
                            mCustomizePlaceList.get(i).setChecked(false);
                        }
                        AppConstants.PLACE_INDUSTRY_ID = "";
                        AppConstants.PLACE_OF_INDUSTRY_NAME = "";

                    }else {
                        AppConstants.PLACE_INDUSTRY_ID = "";
                        AppConstants.PLACE_OF_INDUSTRY_NAME = "";
                        for (int i=0; i<mCustomizePlaceList.size(); i++) {
                            mCustomizePlaceList.get(i).setChecked(true);
                            AppConstants.PLACE_INDUSTRY_ID =String.valueOf(mCustomizePlaceList.get(position).getId());
                            AppConstants.PLACE_OF_INDUSTRY_NAME =String.valueOf(mCustomizePlaceList.get(position).getPlaceInEnglish());
                        }
                    }
//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        ((CustomizationOneScreen)mContext).getNewFlowCustomizationNewApiCall();
//
//                    }else {
//                        ((CustomizationOneScreen)mContext).getNewCustomizationApiCall();
//
//                    }
                    notifyDataSetChanged();

                }else {
                    if (mCustomizePlaceList.get(position).getChecked()){
                        mCustomizePlaceList.get(position).setChecked(false);
                        for (int i=0; i<mCustomizePlaceList.size() ; i++)
                        {
                            if (mCustomizePlaceList.get(i).getPlaceInEnglish().equalsIgnoreCase("All Industry"))
                            {
                                mCustomizePlaceList.get(i).setChecked(false);

                            }
                        }
                    }else {
                        mTotalInt = 1;
                        for (int i=0; i<mCustomizePlaceList.size();i++){
                            if (mCustomizePlaceList.get(i).getChecked()){
                                mTotalInt = mTotalInt + 1;
                            }
                            if (String.valueOf(mTotalInt).equalsIgnoreCase(String.valueOf(mCustomizePlaceList.size()-1))){
                                for (int j=0; j<mCustomizePlaceList.size(); j++){
                                    if (mCustomizePlaceList.get(j).getPlaceInEnglish().equalsIgnoreCase("All Industry")){
                                        mCustomizePlaceList.get(j).setChecked(true);
                                    }
                                }
                            }
                        }
                        mCustomizePlaceList.get(position).setChecked(true);
                    }
                    AppConstants.PLACE_INDUSTRY_ID = "";
                    for (int i=0; i<mCustomizePlaceList.size(); i++){
                        if (mCustomizePlaceList.get(i).getChecked()){
                            AppConstants.PLACE_INDUSTRY_ID = AppConstants.PLACE_INDUSTRY_ID.equalsIgnoreCase("") ? mCustomizePlaceList.get(i).getId()+"" : AppConstants.PLACE_INDUSTRY_ID + ","+ mCustomizePlaceList.get(i).getId();

                            AppConstants.PLACE_OF_INDUSTRY_NAME =  AppConstants.PLACE_OF_INDUSTRY_NAME.equalsIgnoreCase("") ? mCustomizePlaceList.get(i).getPlaceInEnglish() : AppConstants.PLACE_OF_INDUSTRY_NAME + ","+ mCustomizePlaceList.get(i).getPlaceInEnglish();
                        }
                    }
//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        ((CustomizationOneScreen)mContext).getNewFlowCustomizationNewApiCall();
//
//                    }else {
//                        ((CustomizationOneScreen)mContext).getNewCustomizationApiCall();
//
//                    }
                    notifyDataSetChanged();
                }
            }
        });

        holder.mGridCustimizeImg.setVisibility(placeEntity.getChecked() ? View.VISIBLE : View.GONE);

    }

    @Override
    public int getItemCount() {
        return mCustomizePlaceList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_custimize_view_lay)
        LinearLayout mGridViewLay;

        @BindView(R.id.grid_custimize_view_img_lay)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_custimize_view_txt)
        TextView mGridViewTxt;

        @BindView(R.id.grid_custimize_img)
        ImageView mGridCustimizeImg;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

