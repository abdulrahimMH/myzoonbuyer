package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.StichingOrderDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.OrderDetailsStitchingStoreModal;
import com.qoltech.mzyoonbuyer.ui.StoreOrderTrackingDetailsScreen;
import com.qoltech.mzyoonbuyer.ui.StoreWriteAnReviewScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreOrderDetailsAdapter extends RecyclerView.Adapter<StoreOrderDetailsAdapter.Holder> {

    ArrayList<StichingOrderDetailsEntity> mStoreArrayList;
    Context mContext;
    OrderDetailsStitchingStoreModal mOrderDetailsStitchingStoreModal;
    public StoreOrderDetailsAdapter(ArrayList<StichingOrderDetailsEntity> storeArrayList, Context context,OrderDetailsStitchingStoreModal orderDetailsStitchingStoreModal) {
        mStoreArrayList = storeArrayList;
        mContext = context;
        mOrderDetailsStitchingStoreModal = orderDetailsStitchingStoreModal;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_store_order_details_list,viewGroup,false);
        return new StoreOrderDetailsAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

//        if (holder.getAdapterPosition() == 0){
//            holder.mStoreDetStoreTypeTxtParLay.setVisibility(View.VISIBLE);
//        }else {
//            holder.mStoreDetStoreTypeTxtParLay.setVisibility(View.GONE);
//        }

//        holder.mStoreOrderTypeTxt.setText(mStoreArrayList.get(position).getOrderType());
        holder.mStoreOrderProdNameTxt.setText(mStoreArrayList.get(position).getDressSubtypeName());
        holder.mStoreOrderSizeTxt.setText(mStoreArrayList.get(position).getSizeId() == -1 ? mContext.getResources().getString(R.string.not_available) : mStoreArrayList.get(position).getSize());
        holder.mAdapterStoreOrderDetColorParLay.setVisibility(mStoreArrayList.get(position).getColorId() == -1 ? View.GONE : View.VISIBLE);
        holder.mStoreOrderColorTxt.setVisibility(mStoreArrayList.get(position).getColorId() == -1  ? View.VISIBLE : View.GONE);
        holder.mStoreOrderColorTxt.setText(mStoreArrayList.get(position).getColorId() == -1  ? mContext.getResources().getString(R.string.not_available): "");
        holder.mStoreOrderBrandTxt.setText(mStoreArrayList.get(position).getBrandName());
        holder.mStoreOrderPriceTxt.setText(mStoreArrayList.get(position).getPrice());
        holder.mStoreOrderQtyTxt.setText(String.valueOf(mStoreArrayList.get(position).getQuantity()));
        holder.mStoreOrderTotalTxt.setText(mStoreArrayList.get(position).getUnitTotal());
        holder.mStoreRatingAndReviewTxt.setText(mStoreArrayList.get(position).getRating() > 0 ? mContext.getResources().getString(R.string.view_review) : mContext.getResources().getString(R.string.write_a_review));
        holder.mStoreRatingBar.setRating(Float.parseFloat(String.valueOf(mStoreArrayList.get(position).getRating())));

        if (mOrderDetailsStitchingStoreModal.getGetPaymentReceivedStatus().size() > 0){
            if (mOrderDetailsStitchingStoreModal.getGetPaymentReceivedStatus().get(0).getPaymentReceivedStatus().equalsIgnoreCase("Not paid")){
                holder.mStoreTrackTxt.setVisibility(View.GONE);

            }else {
                holder.mStoreTrackTxt.setVisibility(View.VISIBLE);
            }
        }

        if (AppConstants.PENDING_DELIVERY_CLICK.equalsIgnoreCase("Delivery")){
            holder.mStoreRateAndReviewParlay.setVisibility(View.VISIBLE);
        }
        if (AppConstants.PENDING_DELIVERY_CLICK.equalsIgnoreCase("Pending")){
            holder.mStoreRateAndReviewParlay.setVisibility(View.GONE);

        }

        try {
            int color = Color.parseColor(mStoreArrayList.get(position).getColorCode().trim());
            ColorDrawable colorDrawable = new ColorDrawable(color);

            Glide.with(mContext).load(colorDrawable)
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_background_img).error(R.drawable.dress_type_background_img))
                    .into(holder.mStoreOrdeColorImg);

        }catch (Exception e){

            Glide.with(mContext).load(R.drawable.dress_type_background_img)
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_background_img).error(R.drawable.dress_type_background_img))
                    .into(holder.mStoreOrdeColorImg);
        }

        Glide.with(mContext)
                .load(AppConstants.IMAGE_BASE_URL+"Images/Products/"+mStoreArrayList.get(position).getImage())
                .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                .into(holder.mStoreOrderProdImg);

        holder.mStoreTrackTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.ORDER_PENDING_ID = String.valueOf(mStoreArrayList.get(position).getOrderId());
                AppConstants.ORDER_PENDING_LINE_ID = mStoreArrayList.get(position).getLineId();
                ((BaseActivity)mContext).nextScreen(StoreOrderTrackingDetailsScreen.class,true);
            }
        });

        holder.mStoreRateAndReviewParlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.STORE_ORDER_DETAILS_PRODUCT_NAME = mStoreArrayList.get(position).getDressSubtypeName();
                AppConstants.ORDER_PENDING_ID = String.valueOf(mStoreArrayList.get(position).getOrderId());
                AppConstants.STORE_ORDER_DETAILS_PRODUCT_IMG = mStoreArrayList.get(position).getImage();
                AppConstants.ORDER_PENDING_LINE_ID = mStoreArrayList.get(position).getLineId();
                ((BaseActivity)mContext).nextScreen(StoreWriteAnReviewScreen.class,true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mStoreArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

//        @BindView(R.id.store_det_store_type_txt_lay)
//        LinearLayout mStoreDetStoreTypeTxtParLay;

//        @BindView(R.id.adap_store_order_details_order_type_txt)
//        TextView mStoreOrderTypeTxt;

        @BindView(R.id.adapter_store_order_det_prod_txt)
        TextView mStoreOrderProdNameTxt;

        @BindView(R.id.adapter_adapter_store_order_det_size_txt)
        TextView mStoreOrderSizeTxt;

        @BindView(R.id.adapter_store_order_det_color_empty_txt)
        TextView mStoreOrderColorTxt;

        @BindView(R.id.adapter_store_order_det_color_img)
        ImageView mStoreOrdeColorImg;

        @BindView(R.id.adapter_store_order_det_brand_txt)
        TextView mStoreOrderBrandTxt;

        @BindView(R.id.adapter_store_order_det_price_txt)
        TextView mStoreOrderPriceTxt;

        @BindView(R.id.adapter_store_order_qty_txt)
        TextView mStoreOrderQtyTxt;

        @BindView(R.id.adapter_store_order_det_total_txt)
        TextView mStoreOrderTotalTxt;

        @BindView(R.id.adapter_store_order_prod_img)
        ImageView mStoreOrderProdImg;

        @BindView(R.id.adapter_store_order_tack_txt)
        TextView mStoreTrackTxt;

        @BindView(R.id.adapter_store_rate_and_review_par_lay)
        RelativeLayout mStoreRateAndReviewParlay;

        @BindView(R.id.order_details_rate_and_review_txt)
        TextView mStoreRatingAndReviewTxt;

        @BindView(R.id.order_details_rating_bar)
        RatingBar mStoreRatingBar;

        @BindView(R.id.adapter_store_order_det_color_img_lay)
        CardView mAdapterStoreOrderDetColorParLay;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
