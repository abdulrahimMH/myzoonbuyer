package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.CustomizationThicknessEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationOneThicknessAdapter extends RecyclerView.Adapter<CustomizationOneThicknessAdapter.Holder> {

    private Context mContext;
    private ArrayList<CustomizationThicknessEntity> mCustomizationThicknessList;
    int mTotalInt = 1;

    private UserDetailsEntity mUserDetailsEntityRes;
    public CustomizationOneThicknessAdapter(Context activity, ArrayList<CustomizationThicknessEntity> customizeThicknessList) {
        mContext = activity;
        mCustomizationThicknessList = customizeThicknessList;
    }

    @NonNull
    @Override
    public CustomizationOneThicknessAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_thickness_grid_view, parent, false);
        return new CustomizationOneThicknessAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomizationOneThicknessAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        final CustomizationThicknessEntity thicknessEntity = mCustomizationThicknessList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mGridViewTxt.setText(thicknessEntity.getThicknessInArabic());

        }else {
            holder.mGridViewTxt.setText(thicknessEntity.getThickness());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mCustomizationThicknessList.get(position).isChecked()){
                    mCustomizationThicknessList.get(position).setChecked(false);
                }else {
                    mCustomizationThicknessList.get(position).setChecked(true);

                }

                AppConstants.THICKNESS_ID = "";
                AppConstants.THICKNESS_NAME = "";

                for (int i=0; i<mCustomizationThicknessList.size(); i++){
                        if (mCustomizationThicknessList.get(i).isChecked()){
                            AppConstants.THICKNESS_ID = AppConstants.THICKNESS_ID.equalsIgnoreCase("") ? mCustomizationThicknessList.get(i).getId()+"" :  AppConstants.THICKNESS_ID + ","+ mCustomizationThicknessList.get(i).getId();

                            AppConstants.THICKNESS_NAME =  AppConstants.THICKNESS_NAME.equalsIgnoreCase("") ? mCustomizationThicknessList.get(i).getThickness() :  AppConstants.THICKNESS_NAME + ","+ mCustomizationThicknessList.get(i).getThickness();
                        }
                    }
                    notifyDataSetChanged();
            }

        });

        holder.mGridViewLay.setBackgroundResource(thicknessEntity.isChecked() ? R.drawable.app_clr_border_with_corner : R.drawable.white_corner);


    }

    @Override
    public int getItemCount() {
        return mCustomizationThicknessList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_custimize_par_lay)
        RelativeLayout mGridViewLay;

        @BindView(R.id.grid_custimize_view_txt)
        TextView mGridViewTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

