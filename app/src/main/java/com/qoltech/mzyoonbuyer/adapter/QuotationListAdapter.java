package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.IdEntity;
import com.qoltech.mzyoonbuyer.entity.QuotationEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.OrderApprovalScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuotationListAdapter extends RecyclerView.Adapter<QuotationListAdapter.Holder> {

    private Context mContext;

    ArrayList<QuotationEntity> mQuotationList;

    private UserDetailsEntity mUserDetailsEntityRes;
     public QuotationListAdapter(Context activity , ArrayList<QuotationEntity> quotationList) {
        mContext = activity;
        mQuotationList = quotationList;
    }

    @NonNull
    @Override
    public QuotationListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_quotation_list, parent, false);
        return new QuotationListAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final QuotationListAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        QuotationEntity quotationList = mQuotationList.get(position);

        holder.mQuotationListShopName.setText(quotationList.getShopNameInEnglish());

        if (mQuotationList.get(position).getTotalAmount() == 0.0){
            holder.mQuotationViewImg.setVisibility(View.GONE);
            holder.mQuotationStatusTxt.setText(mContext.getResources().getString(R.string.quotation_pending));
            holder.mQuotationListPricTxt.setText(mContext.getResources().getString(R.string.quotation_staus_pending));
            holder.mQuotationListPrice.setText("");
            holder.mQuotationListDays.setText(mContext.getResources().getString(R.string.pending));


        }else {
            holder.mQuotationViewImg.setVisibility(View.VISIBLE);
            holder.mQuotationStatusTxt.setText(mContext.getResources().getString(R.string.quotation_received));
            holder.mQuotationListPricTxt.setText(mContext.getResources().getString(R.string.quotation_staus_pending));
            holder.mQuotationListPricTxt.setText(mContext.getResources().getString(R.string.offering));
            holder.mQuotationListPrice.setText(String.valueOf(quotationList.getTotalAmount()));
            holder.mQuotationListDays.setText(quotationList.getStichingTime());

        }

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/TailorImages/"+quotationList.getShopOwnerImageURL())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mQuotationListImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.mQuotationViewImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQuotationList.get(position).getTotalAmount() != 0.0) {
                    AppConstants.PAYMENT_STATUS = "Not Paid";
                    IdEntity id = new IdEntity();
                    id.setId(mQuotationList.get(position).getTailorId());
                    AppConstants.DELIVERY_TAILOR_ID.add(id);
                    AppConstants.APPROVED_TAILOR_ID = String.valueOf(mQuotationList.get(position).getTailorId());
                    AppConstants.QUOTATION_LIST_ID = String.valueOf(mQuotationList.get(position).getId());
                    AppConstants.QUOTATION_ORDER_ID = String.valueOf(mQuotationList.get(position).getRequestId());
                    AppConstants.ORDER_ID = String.valueOf(mQuotationList.get(position).getRequestId());
                    AppConstants.DELIVERY_AREA_ID = String.valueOf(mQuotationList.get(position).getAreaId());
                    AppConstants.ORDER_TYPE_SKILL = "Stitching";
                    AppConstants.TAILOR_ID = String.valueOf(mQuotationList.get(position).getTailorId());
                    ((BaseActivity)mContext).nextScreen(OrderApprovalScreen.class,true);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mQuotationList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

         @BindView(R.id.quotation_list_shop_name)
         TextView mQuotationListShopName;

         @BindView(R.id.quotation_list_price)
         TextView mQuotationListPrice;

         @BindView(R.id.quotation_list_status)
         TextView mQuotationStatusTxt;

         @BindView(R.id.quotation_list_days)
         TextView mQuotationListDays;

         @BindView(R.id.quotaion_list_view_img)
         ImageView mQuotationViewImg;

         @BindView(R.id.quotation_list_price_txt)
         TextView mQuotationListPricTxt;

         @BindView(R.id.quotation_list_img)
         de.hdodenhof.circleimageview.CircleImageView mQuotationListImg;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}