package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetAllCategoriesEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.OffersScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReferAndEarnCategoriesAdpater extends RecyclerView.Adapter<ReferAndEarnCategoriesAdpater.Holder> {

    private Context mContext;
    private ArrayList<GetAllCategoriesEntity> mGetAllCategoriesEntityList;

    private UserDetailsEntity mUserDetailsEntityRes;
    public ReferAndEarnCategoriesAdpater(Context activity, ArrayList<GetAllCategoriesEntity> getAllCategoriesEntity) {
        mContext = activity;
        mGetAllCategoriesEntityList = getAllCategoriesEntity;
    }

    @NonNull
    @Override
    public ReferAndEarnCategoriesAdpater.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_refer_and_earn_list, parent, false);
        return new ReferAndEarnCategoriesAdpater.Holder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull final ReferAndEarnCategoriesAdpater.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final GetAllCategoriesEntity getAllCategoriesEntity = mGetAllCategoriesEntityList.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.OFFERS_ID = String.valueOf(position);
                ((BaseActivity)mContext).nextScreen(OffersScreen.class,true);
            }
        });

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/categories/"+getAllCategoriesEntity.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mReferAndEarnBodyImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mReferAndEarnTxt.setText(getAllCategoriesEntity.getCategoriesNameInArabic());

        }else {
            holder.mReferAndEarnTxt.setText(getAllCategoriesEntity.getCategoriesNameInEnglis());

        }

    }

    @Override
    public int getItemCount() {
        return mGetAllCategoriesEntityList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.refer_and_earn_body_img_adap)
        ImageView mReferAndEarnBodyImg;

        @BindView(R.id.refer_and_earn_txt_adap)
        TextView mReferAndEarnTxt;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

