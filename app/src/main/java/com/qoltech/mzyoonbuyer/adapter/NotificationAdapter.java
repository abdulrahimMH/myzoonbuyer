package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.NotificationEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.Holder> {

    private Context mContext;
    private ArrayList<NotificationEntity> mNotificationList;

    private UserDetailsEntity mUserDetailsEntityRes;

    public NotificationAdapter(Context activity,ArrayList<NotificationEntity> notificationEntities) {
        mContext = activity;
        mNotificationList = notificationEntities;
    }

    @NonNull
    @Override
    public NotificationAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_list, parent, false);
        return new NotificationAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final NotificationAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        NotificationEntity mList = mNotificationList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mNotificationTitleTxt.setText(mList.getHeaderInArabic());
            holder.mNotificationMsgTxt.setText(mList.getNotificationInArabic());
        }else {
            holder.mNotificationTitleTxt.setText(mList.getHeader());
            holder.mNotificationMsgTxt.setText(mList.getNotification());
        }

        holder.mNotificationDateTxt.setText(mList.getCreatedOn());
    }

    @Override
    public int getItemCount() {
        return mNotificationList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_notification_title_txt)
        TextView mNotificationTitleTxt;

        @BindView(R.id.adapter_notification_msg_txt)
        TextView mNotificationMsgTxt;

        @BindView(R.id.adapter_notification_date_txt)
        TextView mNotificationDateTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}



