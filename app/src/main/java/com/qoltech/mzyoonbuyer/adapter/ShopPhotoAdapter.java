package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.ShopImagesEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShopPhotoAdapter extends RecyclerView.Adapter<ShopPhotoAdapter.Holder> {

    private Context mContext;
    private ArrayList<ShopImagesEntity> mShopImageList;

    private UserDetailsEntity mUserDetailsEntityRes;
    public ShopPhotoAdapter(Context activity, ArrayList<ShopImagesEntity> shopImageList) {
        mContext = activity;
        mShopImageList = shopImageList;
    }

    @NonNull
    @Override
    public ShopPhotoAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grid_cutomize_corner, parent, false);
        return new ShopPhotoAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ShopPhotoAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final ShopImagesEntity shopImageEntity = mShopImageList.get(position);

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/ShopImages/"+shopImageEntity.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.mGridViewTxt.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return mShopImageList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_custimize_view_lay)
        LinearLayout mGridViewLay;

        @BindView(R.id.grid_custimize_view_img_lay)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_custimize_view_txt)
        TextView mGridViewTxt;

        @BindView(R.id.grid_custimize_img)
        ImageView mGridCustimizeImg;



        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

