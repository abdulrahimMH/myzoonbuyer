package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.OrderDeliveryListEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.OrderDetailsScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderDeliveryListAdapter extends RecyclerView.Adapter<OrderDeliveryListAdapter.Holder> {

    private Context mContext;
    private ArrayList<OrderDeliveryListEntity> mOrderDeliveryList;
    private UserDetailsEntity mUserDetailsEntityRes;

    public OrderDeliveryListAdapter(Context activity ,ArrayList<OrderDeliveryListEntity> orderDeliveryList) {
        mContext = activity;
        mOrderDeliveryList = orderDeliveryList;
    }

    @NonNull
    @Override
    public OrderDeliveryListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_order_list, parent, false);
        return new OrderDeliveryListAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderDeliveryListAdapter.Holder holder, final int position) {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        OrderDeliveryListEntity mList = mOrderDeliveryList.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.ORDER_TYPE_SKILL = mOrderDeliveryList.get(position).getOrderType();
                AppConstants.InitiatedBy = "";
                AppConstants.PENDING_DELIVERY_CLICK = "Delivery";
                AppConstants.APPROVE_REJECT_BTN = "false";
                AppConstants.ORDER_DETAILS_PENDING_ID = String.valueOf(mOrderDeliveryList.get(position).getOrderId());
                AppConstants.TAILOR_CLICK_ID = String.valueOf(mOrderDeliveryList.get(position).getTailorId());
                AppConstants.DIRECT_ORDER = mOrderDeliveryList.get(position).getOrderType();
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    AppConstants.TAILOR_CLICK_NAME = mOrderDeliveryList.get(position).getShopNameInArabic();
                }else{
                    AppConstants.TAILOR_CLICK_NAME = mOrderDeliveryList.get(position).getShopNameInEnglish();
                }

                ((BaseActivity)mContext).nextScreen(OrderDetailsScreen.class,true);
//                if (mList.getOrderType().equalsIgnoreCase("Store")){
//                    ((BaseActivity)mContext).nextScreen(StoreOrderDetailScreen.class,true);
//                }else {
//                    ((BaseActivity)mContext).nextScreen(OldOrderDetailsScreen.class,true);
//                }

            }
        });

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/DressSubType/"+mList.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mOrderListImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
//            holder.mOrderListTailorNameTxt.setText(mList.getTailorNameInArabic());
            holder.mOrderListShopNameTxt.setText(mList.getShopNameInArabic());
        }else {
            holder.mOrderListShopNameTxt.setText(mList.getShopNameInEnglish());
        }
//        holder.mOrderListTailorNameTxt.setText(mList.getOrderType());
        holder.mOrderListTailorNameTxt.setText(mList.getOrderType().equalsIgnoreCase("StitchingStore") ? mContext.getResources().getString(R.string.stitching_and_store) : mList.getOrderType());

        holder.mOrderListIdTxt.setText(String.valueOf(mList.getOrderId()));
        holder.mOrderListProductNameTxt.setText(mList.getProduct_Name());
        holder.mOrderListDateTxt.setText(mList.getOrderDate().replace("T00:00:00",""));

    }

    @Override
    public int getItemCount() {
        return mOrderDeliveryList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.order_list_img)
        de.hdodenhof.circleimageview.CircleImageView mOrderListImg;

        @BindView(R.id.order_list_id_txt)
        TextView mOrderListIdTxt;

        @BindView(R.id.order_list_tailor_name_txt)
        TextView mOrderListTailorNameTxt;

        @BindView(R.id.order_list_shop_name_txt)
        TextView mOrderListShopNameTxt;

        @BindView(R.id.order_list_product_name_txt)
        TextView mOrderListProductNameTxt;

        @BindView(R.id.order_list_date_txt)
        TextView mOrderListDateTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
