package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.ManuallyEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.MeasurementListScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ManuallyAdapter extends RecyclerView.Adapter<ManuallyAdapter.Holder> {

    private Context mContext;
    private ArrayList<ManuallyEntity> mManualList;
    private UserDetailsEntity mUserDetailsEntityRes;

    public ManuallyAdapter(Context activity, ArrayList<ManuallyEntity> manualList) {
        mContext = activity;
        mManualList = manualList;
    }

    @NonNull
    @Override
    public ManuallyAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_add_measurement_list, parent, false);
        return new ManuallyAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ManuallyAdapter.Holder holder, final int position) {
        final ManuallyEntity manualEntity = mManualList.get(position);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_MANUALLY = mManualList.get(position).getUserName();
                AppConstants.MEASUREMENT_ID = String.valueOf(mManualList.get(position).getId());
                AppConstants.ORDER_DETAILS_MEASUREMENT = "ADD_MEASUREMENT";
                AppConstants.NEW_ORDER = "NEW_ORDER";
                AppConstants.REQUEST_LIST_ID = String.valueOf(manualEntity.getId());
                ((BaseActivity)mContext).nextScreen(MeasurementListScreen.class,true);

//                String[] parts = mManualList.get(position).getName().split("-");
//                String part1 = parts[0];
//                String part2 = parts[1];
//                String lastOne = parts[parts.length - 1];
//
//                ((BaseActivity)mContext).nextScreen(AddReferenceScreen.class,true);
            }
        });
        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/DressSubType/"+manualEntity.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mMeasurementImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }
        holder.mAddMeasurementNameTxt.setText(manualEntity.getUserName());
        holder.mAddMeasurementListMeasurementDateTxt.setText(manualEntity.getUserDate());
        holder.mMeasurementListMeasurmentByTxt.setText(manualEntity.getMeasurementBy());
        if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mAddMeasuremenDressTypeTxt.setText(manualEntity.getNameInArabic() + " - " + manualEntity.getGenderInArabic());

        }else {
            holder.mAddMeasuremenDressTypeTxt.setText(manualEntity.getGender() + " - " + manualEntity.getNameInEnglish());

        }

    }

    @Override
    public int getItemCount() {
        return mManualList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.profile_image)
        de.hdodenhof.circleimageview.CircleImageView mMeasurementImg;

        @BindView(R.id.add_measurement_list_measurement_name_txt)
        TextView mAddMeasurementNameTxt;

        @BindView(R.id.add_measurement_list_dress_type_txt)
        TextView mAddMeasuremenDressTypeTxt;

        @BindView(R.id.add_measurement_list_measurement_by_txt)
        TextView mMeasurementListMeasurmentByTxt;

        @BindView(R.id.add_measurement_list_measurement_date_txt)
        TextView mAddMeasurementListMeasurementDateTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

