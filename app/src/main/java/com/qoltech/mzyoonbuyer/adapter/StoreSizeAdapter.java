package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetProductSizeEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreSizeAdapter extends RecyclerView.Adapter<StoreSizeAdapter.Holder> {

    ArrayList<GetProductSizeEntity> mList;
    Context mContext;

    public StoreSizeAdapter(ArrayList<GetProductSizeEntity> list, Context context) {
        mList = list;
        mContext = context;
    }


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_store_size_list,viewGroup,false);
        return new StoreSizeAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        holder.mAdapterStoreSizeTxt.setText(mList.get(position).getSize());

        if (mList.size() == 1){
            mList.get(0).setChecked(true);
            AppConstants.STORE_SIZE_ID = String.valueOf(mList.get(0).getId());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0; i<mList.size(); i++){
                    mList.get(i).setChecked(false);
                }

                mList.get(position).setChecked(true);
                AppConstants.STORE_SIZE_ID = String.valueOf(mList.get(position).getId());
            notifyDataSetChanged();
            }
        });

        holder.mAdapterStoreSizeTxt.setTextColor(mList.get(position).isChecked() ? mContext.getResources().getColor(R.color.white) : mContext.getResources().getColor(R.color.app_blue_clr));
        holder.mAdapterStoreSizeTxt.setBackgroundResource(mList.get(position).isChecked() ? R.drawable.app_clr_wth_lite_corner : R.drawable.app_clr_lite_border_with_corner);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.adapter_store_size_txt)
        TextView mAdapterStoreSizeTxt;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}

