package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetStateEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OfferLocationAdapter extends RecyclerView.Adapter<OfferLocationAdapter.Holder> {

    private Context mContext;
    private ArrayList<GetStateEntity> mLocationEntity;

    private UserDetailsEntity mUserDetailsEntityRes;

    public OfferLocationAdapter(Context activity, ArrayList<GetStateEntity> locationEntity) {
        mContext = activity;
        mLocationEntity = locationEntity;
    }

    @NonNull
    @Override
    public OfferLocationAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_offer_gender, parent, false);
        return new OfferLocationAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OfferLocationAdapter.Holder holder, int position) {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final GetStateEntity getStateEntity = mLocationEntity.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {

            holder.mFilterTypeNameTxt.setText(getStateEntity.getStateName());
            ViewCompat.setLayoutDirection(holder.itemView.findViewById(R.id.filter_check_box_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        } else {
            holder.mFilterTypeNameTxt.setText(getStateEntity.getStateName());
            ViewCompat.setLayoutDirection(holder.itemView.findViewById(R.id.filter_check_box_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mLocationEntity.get(position).isChecked()){
                    mLocationEntity.get(position).setChecked(false);

                }else {
                    mLocationEntity.get(position).setChecked(true);

                }
                notifyDataSetChanged();
            }

        });

        holder.mFilterCheckBoxImg.setBackground(mLocationEntity.get(position).isChecked() ? mContext.getResources().getDrawable(R.drawable.tick_selected_img) : mContext.getResources().getDrawable(R.drawable.tick_unselected_img));

    }

    @Override
    public int getItemCount() {
        return mLocationEntity.size();
    }

    public class Holder extends RecyclerView.ViewHolder {


        @BindView(R.id.filter_type_recycler_view_img)
        ImageView mFilterCheckBoxImg;

        @BindView(R.id.filter_type_recycler_view_txt)
        TextView mFilterTypeNameTxt;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
