package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetProductSellerEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.RatingScreen;
import com.qoltech.mzyoonbuyer.ui.ShopDetailsScreen;
import com.qoltech.mzyoonbuyer.ui.StoreProductViewDetailsScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreSellerAvailableAdapter extends RecyclerView.Adapter<StoreSellerAvailableAdapter.Holder> {

    ArrayList<GetProductSellerEntity> mList;

    Context mContext;

    UserDetailsEntity mUserDetailsEntityRes;

    public StoreSellerAvailableAdapter(ArrayList<GetProductSellerEntity> list, Context context) {
        mList = list;
        mContext = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_store_seller_available_list,viewGroup,false);
        return new StoreSellerAvailableAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        GetProductSellerEntity list = mList.get(position);

        holder.mStoreViewSellerReviewTxt.setText(list.getRatingsCount() > 0 ? String.valueOf(list.getRatingsCount()+" "+mContext.getResources().getString(R.string.review)) : mContext.getResources().getString(R.string.no_reviews));
        holder.mStoreViewSellerRatingTxt.setText(String.valueOf(list.getRatingsValue()));
        holder.mStoreViewSellerAmtTxt.setText(String.valueOf(list.getNewPrice()));
        holder.mStoreViewSellerShopNameTxt.setPaintFlags( holder.mStoreViewSellerShopNameTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.mStoreViewSellerSelectedTxt.setText(list.isChecked() ? mContext.getResources().getString(R.string.selected) : mContext.getResources().getString(R.string.choose_seller));
        holder.mStoreViewSellerSelectedTxt.setBackgroundResource(list.isChecked() ? R.drawable.new_grey_clr_with_lite_corner : R.drawable.app_clr_gradien_with_lite_corner);
        holder.mStoreViewSellerSelectedTxt.setTextColor(list.isChecked() ? mContext.getResources().getColor(R.color.app_blue_clr) : mContext.getResources().getColor(R.color.white));
        holder.mStoreViewSellerRatingLay.setVisibility(list.getRatingsValue() > 0 ? View.VISIBLE : View.GONE);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mStoreViewSellerProdNameTxt.setText(list.getProductNameInArabic());
            holder.mStoreViewSellerShopNameTxt.setText(list.getShopNameInArabic());
            holder.mStoreViewSellerBrandTxt.setText(list.getBrandNameInArabic());

        }else {
            holder.mStoreViewSellerProdNameTxt.setText(list.getProductName());
            holder.mStoreViewSellerShopNameTxt.setText(list.getShopNameInEnglish());
            holder.mStoreViewSellerBrandTxt.setText(list.getBrandName());

        }

        holder.mStoreViewSellerShopNameTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.TAILOR_ID = String.valueOf(mList.get(position).getSellerId());
                ((BaseActivity)mContext).nextScreen(ShopDetailsScreen.class,true);
            }
        });

        holder.mStoreViewSellerRatingParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mList.get(position).getRatingsValue() > 0){
//                    AppConstants.STORE_PRODUCT_ID = mList.get(position).getId();
//                    AppConstants.MATERIAL_REVIEW = "false";
//                    ((BaseActivity)mContext).nextScreen(RatingScreen.class,true);
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        AppConstants.TAILOR_CLICK_NAME = mList.get(position).getShopNameInArabic();

                    }else {
                        AppConstants.TAILOR_CLICK_NAME = mList.get(position).getShopNameInEnglish();

                    }
                    AppConstants.TAILOR_CLICK_ID = String.valueOf(mList.get(position).getSellerId());
                    AppConstants.MATERIAL_REVIEW = "";
                    ((BaseActivity)mContext).nextScreen(RatingScreen.class,true);
                }
            }
        });

        holder.mStoreViewSellerSelectedTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!list.isChecked()){
                    AppConstants.STORE_PRODUCT_ID = mList.get(position).getId();
                    AppConstants.STORE_SELLER_ID = String.valueOf(mList.get(position).getSellerId());
                    ((BaseActivity)mContext).previousScreen(StoreProductViewDetailsScreen.class,true);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.store_view_seller_brand_txt)
        TextView mStoreViewSellerBrandTxt;

        @BindView(R.id.store_view_seller_prod_name_txt)
        TextView mStoreViewSellerProdNameTxt;

        @BindView(R.id.store_view_seller_shop_name_txt)
        TextView mStoreViewSellerShopNameTxt;

        @BindView(R.id.store_view_seller_rating_txt)
        TextView mStoreViewSellerRatingTxt;

        @BindView(R.id.store_view_seller_review_txt)
        TextView mStoreViewSellerReviewTxt;

        @BindView(R.id.store_view_seller_amount_txt)
        TextView mStoreViewSellerAmtTxt;

        @BindView(R.id.store_view_seller_selected_txt)
        TextView mStoreViewSellerSelectedTxt;

        @BindView(R.id.store_view_seller_add_to_cart_txt)
        TextView mStoreVeiwSellerAddToCartTxt;

        @BindView(R.id.store_view_seller_rating_lay)
        CardView mStoreViewSellerRatingLay;

        @BindView(R.id.store_view_seller_rating_par_lay)
        RelativeLayout mStoreViewSellerRatingParLay;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
