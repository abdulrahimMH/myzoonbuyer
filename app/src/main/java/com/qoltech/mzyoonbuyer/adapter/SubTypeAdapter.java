package com.qoltech.mzyoonbuyer.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.SubTypeEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.MeasurementTwoScreen;
import com.qoltech.mzyoonbuyer.ui.OrderFlowType;
import com.qoltech.mzyoonbuyer.ui.SubTypeScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;
import com.qoltech.mzyoonbuyer.utils.ShakeErrorUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubTypeAdapter extends RecyclerView.Adapter<SubTypeAdapter.Holder> {

    private Context mContext;
    private ArrayList<SubTypeEntity> mSubTypeList;
    private UserDetailsEntity mUserDetailsEntityRes;
    private Dialog mEditTxtDialog;
    String mManuallyStr = "false";

    public SubTypeAdapter(Context activity, ArrayList<SubTypeEntity> subTypeList) {
        mContext = activity;
        mSubTypeList = subTypeList;
    }

    @NonNull
    @Override
    public SubTypeAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_gridview, parent, false);
        return new SubTypeAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SubTypeAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        final SubTypeEntity subTypeEntity = mSubTypeList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){

            holder.mGridViewTxt.setText(subTypeEntity.getNameInArabic());
        }else {
            holder.mGridViewTxt.setText(subTypeEntity.getNameInEnglish());

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(mSubTypeList.get(position).getId());
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    AppConstants.DRESS_SUB_TYPE_NAME = mSubTypeList.get(position).getNameInArabic();

                }else {
                    AppConstants.DRESS_SUB_TYPE_NAME = mSubTypeList.get(position).getNameInEnglish();

                }
                if (AppConstants.ADD_NEW_MEASUREMENT.equalsIgnoreCase("ADD_NEW_MEASUREMENT")){
                    ((SubTypeScreen) mContext).getManuallyApiCall();
                    EditTextDialog();

                }else {
//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        ((BaseActivity) mContext).nextScreen(TailorListScreen.class, true);
//
//                    }else {
                        ((BaseActivity) mContext).nextScreen(OrderFlowType.class, true);

//                    }
                }
            }
        });

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"images/DressSubType/"+subTypeEntity.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mGridViewImgLay);
        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

    }

    @Override
    public int getItemCount() {
        return mSubTypeList.size();
    }

    public void filterList(ArrayList<SubTypeEntity> filterdNames) {
        mSubTypeList = filterdNames;
        notifyDataSetChanged();
    }

    public void EditTextDialog(){
        alertDismiss(mEditTxtDialog);
        mEditTxtDialog = getDialog(mContext, R.layout.pop_up_edit_txt);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mEditTxtDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mEditTxtDialog.findViewById(R.id.pop_up_measurement_manually_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(mEditTxtDialog.findViewById(R.id.pop_up_measurement_manually_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
        TextView cancelTxt , okTxt,mHeaderTxt;
        final EditText manuallyEdtTxt;

        /*Init view*/

        cancelTxt = mEditTxtDialog.findViewById(R.id.pop_up_cancel_txt);
        okTxt = mEditTxtDialog.findViewById(R.id.pop_up_ok_txt);
        manuallyEdtTxt = mEditTxtDialog.findViewById(R.id.measurement_manually_edt_txt);
        mHeaderTxt = mEditTxtDialog.findViewById(R.id.popup_measurement_name_manually_txt);

        mHeaderTxt.setText(mContext.getResources().getString(R.string.give_you_measurement_name)+"\n"+AppConstants.DRESS_SUB_TYPE_NAME);

        /*Set data*/
        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditTxtDialog.dismiss();
            }
        });

        okTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!manuallyEdtTxt.getText().toString().trim().equalsIgnoreCase("")){
                    mManuallyStr = "false";

                    for (int i=0; i<AppConstants.MANULLY_LIST.size(); i++){
                        String[] parts = AppConstants.MANULLY_LIST.get(i).getName().split("-");
                        String part1 = parts[0]; // 004
                        String part2 = parts[1];
                        String lastOne = parts[parts.length - 1];

                        if (part2.trim().toLowerCase().equalsIgnoreCase(manuallyEdtTxt.getText().toString().trim().toLowerCase())){
                            mManuallyStr = "true";
                        }
                    }
                    if (mManuallyStr.equalsIgnoreCase("true")){
                        manuallyEdtTxt.clearAnimation();
                        manuallyEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                        manuallyEdtTxt.setError(mContext.getResources().getString(R.string.measurement_name_already_exist));
                    }else if (mManuallyStr.equalsIgnoreCase("false")){
                        mEditTxtDialog.dismiss();
                        AppConstants.MEASUREMENT_MANUALLY = manuallyEdtTxt.getText().toString();
                        AppConstants.MEASUREMENT_ID = "-1";
                        ((BaseActivity) mContext).nextScreen(MeasurementTwoScreen.class, true);

                    }
                }else {
                    manuallyEdtTxt.clearAnimation();
                    manuallyEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    manuallyEdtTxt.setError(mContext.getResources().getString(R.string.please_fill_measurement_name));
                }
            }
        });

        alertShowing(mEditTxtDialog);
    }

    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }

    }

    /*Default dialog init method*/
    public Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_view_lay)
        CardView mGridViewLay;

        @BindView(R.id.grid_view_img_lay)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_view_txt)
        TextView mGridViewTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

