package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetColorByIdEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationColorAdapter extends RecyclerView.Adapter<CustomizationColorAdapter.Holder> {

    private Context mContext;
    private ArrayList<GetColorByIdEntity> mColorList;
    private UserDetailsEntity mUserDetailsEntityRes;

    public CustomizationColorAdapter(Context activity, ArrayList<GetColorByIdEntity> colorList) {
        mContext = activity;
        mColorList = colorList;
    }

    @NonNull
    @Override
    public CustomizationColorAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_customization_image_list, parent, false);
        return new CustomizationColorAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomizationColorAdapter.Holder holder, final int position) {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/SubColor/"+mColorList.get(position).getColorImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

    }

    @Override
    public int getItemCount() {
        return mColorList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_view_only_img)
        ImageView mGridViewImgLay;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


