package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetMeasurementPartEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.ui.MeasurementTwoScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeasurementTwoPartsAdapter extends RecyclerView.Adapter<MeasurementTwoPartsAdapter.Holder> {

    private Context mContext;
    ArrayList<GetMeasurementPartEntity> mMeasuremtnPartsList;
    private UserDetailsEntity mUserDetailsEntityRes;
    ArrayList<String> Values = new ArrayList<>();
    ArrayList<String> Inches = new ArrayList<>();
    HashMap<String,String> parts = new HashMap<>();

    public MeasurementTwoPartsAdapter(Context activity, ArrayList<GetMeasurementPartEntity> measurementPartsList) {
        mContext = activity;
        mMeasuremtnPartsList = measurementPartsList;
    }

    @NonNull
    @Override
    public MeasurementTwoPartsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_measurement_two_part_list, parent, false);
        return new MeasurementTwoPartsAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MeasurementTwoPartsAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

            for (int i=0; i<mMeasuremtnPartsList.size(); i++){
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(""));
            }

        holder.mMeasurementPartsTxt.setText(mContext.getResources().getString(R.string.parts));

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"images/Measurement2/"+mMeasuremtnPartsList.get(position).getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mMeasurementPartsImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        for (int i=0; i<mMeasuremtnPartsList.size(); i++){

            if (mMeasuremtnPartsList.get(i).getId() == 1 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_1);
                Inches.add(i,AppConstants.MEN_1_INCHES);
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));
                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));
                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 2 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_2);
                Inches.add(i,AppConstants.MEN_2_INCHES);
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));
                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));
                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 3 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_3);
                Inches.add(i,AppConstants.MEN_3_INCHES);
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));
                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));
                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 4 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_4);
                Inches.add(i,AppConstants.MEN_4_INCHES);
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));
                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));
                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 5 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_5);
                Inches.add(i,AppConstants.MEN_5_INCHES);
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));
                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));
                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 7 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_7);
                Inches.add(i,AppConstants.MEN_7_INCHES);
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));
                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));
                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 6 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_6);
                Inches.add(i,AppConstants.MEN_6_INCHES);
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));
                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 8 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_8);
                Inches.add(i,AppConstants.MEN_8_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 9 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_9);
                Inches.add(i,AppConstants.MEN_9_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 10 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_10);
                Inches.add(i,AppConstants.MEN_10_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 11 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_11);
                Inches.add(i,AppConstants.MEN_11_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 12 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_12);
                Inches.add(i,AppConstants.MEN_12_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 13 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_13);
                Inches.add(i,AppConstants.MEN_13_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 14 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_14);
                Inches.add(i,AppConstants.MEN_14_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 15 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_15);
                Inches.add(i,AppConstants.MEN_15_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 16 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_16);
                Inches.add(i,AppConstants.MEN_16_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 17 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_17);
                Inches.add(i,AppConstants.MEN_17_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 18 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_18);
                Inches.add(i,AppConstants.MEN_18_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 19 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_19);
                Inches.add(i,AppConstants.MEN_19_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 20 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_20);
                Inches.add(i,AppConstants.MEN_20_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 21 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_21);
                Inches.add(i,AppConstants.MEN_21_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 22 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_22);
                Inches.add(i,AppConstants.MEN_22_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 23 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_23);
                Inches.add(i,AppConstants.MEN_23_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 24 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_24);
                Inches.add(i,AppConstants.MEN_24_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 25 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_25);
                Inches.add(i,AppConstants.MEN_25_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 26 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_26);
                Inches.add(i,AppConstants.MEN_26_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 27 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_27);
                Inches.add(i,AppConstants.MEN_27_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 28 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_28);
                Inches.add(i,AppConstants.MEN_28_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 29 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_29);
                Inches.add(i,AppConstants.MEN_29_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 30 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_30);
                Inches.add(i,AppConstants.MEN_30_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 31 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_31);
                Inches.add(i,AppConstants.MEN_31_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 32 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_32);
                Inches.add(i,AppConstants.MEN_32_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 33 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_33);
                Inches.add(i,AppConstants.MEN_33_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 34 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_34);
                Inches.add(i,AppConstants.MEN_34_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 35 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_35);
                Inches.add(i,AppConstants.MEN_35_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 36 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_36);
                Inches.add(i,AppConstants.MEN_36_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 37 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_37);
                Inches.add(i,AppConstants.MEN_37_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 38 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_38);
                Inches.add(i,AppConstants.MEN_38_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 39 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_39);
                Inches.add(i,AppConstants.MEN_39_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 40 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_40);
                Inches.add(i,AppConstants.MEN_40_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 41 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_41);
                Inches.add(i,AppConstants.MEN_41_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 42 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_42);
                Inches.add(i,AppConstants.MEN_42_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 43 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_43);
                Inches.add(i,AppConstants.MEN_43_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 44 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_44);
                Inches.add(i,AppConstants.MEN_44_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 45 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_45);
                Inches.add(i,AppConstants.MEN_45_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 46 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_46);
                Inches.add(i,AppConstants.MEN_46_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 47 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_47);
                Inches.add(i,AppConstants.MEN_47_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 48 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_48);
                Inches.add(i,AppConstants.MEN_48_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 49 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_49);
                Inches.add(i,AppConstants.MEN_49_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 50 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_50);
                Inches.add(i,AppConstants.MEN_50_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 51 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_51);
                Inches.add(i,AppConstants.MEN_51_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 52 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_52);
                Inches.add(i,AppConstants.MEN_52_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 53 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_53);
                Inches.add(i,AppConstants.MEN_53_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 54 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_54);
                Inches.add(i,AppConstants.MEN_54_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 55 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_55);
                Inches.add(i,AppConstants.MEN_55_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 56 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_56);
                Inches.add(i,AppConstants.MEN_56_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 57 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_57);
                Inches.add(i,AppConstants.MEN_57_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 58 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_58);
                Inches.add(i,AppConstants.MEN_58_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 59 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_59);
                Inches.add(i,AppConstants.MEN_59_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 60 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_60);
                Inches.add(i,AppConstants.MEN_60_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 61 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_61);
                Inches.add(i,AppConstants.MEN_61_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 62 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_62);
                Inches.add(i,AppConstants.MEN_62_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 63 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_63);
                Inches.add(i,AppConstants.MEN_63_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 64 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_64);
                Inches.add(i,AppConstants.MEN_64_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 65 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_65);
                Inches.add(i,AppConstants.WOMEN_65_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 66 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_66);
                Inches.add(i,AppConstants.WOMEN_66_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 67 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_67);
                Inches.add(i,AppConstants.WOMEN_67_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 68 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_68);
                Inches.add(i,AppConstants.WOMEN_68_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 69 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_69);
                Inches.add(i,AppConstants.WOMEN_69_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 70 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_70);
                Inches.add(i,AppConstants.WOMEN_70_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 71 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_71);
                Inches.add(i,AppConstants.WOMEN_71_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 72 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_72);
                Inches.add(i,AppConstants.WOMEN_72_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 73 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_73);
                Inches.add(i,AppConstants.WOMEN_73_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 74 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_74);
                Inches.add(i,AppConstants.WOMEN_74_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 75 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_75);
                Inches.add(i,AppConstants.WOMEN_75_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 76 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_76);
                Inches.add(i,AppConstants.WOMEN_76_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 77 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_77);
                Inches.add(i,AppConstants.WOMEN_77_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 78 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_78);
                Inches.add(i,AppConstants.WOMEN_78_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 79 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_79);
                Inches.add(i,AppConstants.WOMEN_79_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 80 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_80);
                Inches.add(i,AppConstants.WOMEN_80_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 81 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_81);
                Inches.add(i,AppConstants.WOMEN_81_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 82 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_82);
                Inches.add(i,AppConstants.WOMEN_82_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 83 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_83);
                Inches.add(i,AppConstants.WOMEN_83_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 84 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_84);
                Inches.add(i,AppConstants.WOMEN_84_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 85 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_85);
                Inches.add(i,AppConstants.WOMEN_85_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 86 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_86);
                Inches.add(i,AppConstants.WOMEN_86_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 87 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_87);
                Inches.add(i,AppConstants.WOMEN_87_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 88 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_88);
                Inches.add(i,AppConstants.WOMEN_88_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 89 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_89);
                Inches.add(i,AppConstants.WOMEN_89_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 90 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_90);
                Inches.add(i,AppConstants.WOMEN_90_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 91 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_91);
                Inches.add(i,AppConstants.WOMEN_91_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 92 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_92);
                Inches.add(i,AppConstants.WOMEN_92_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 93 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_93);
                Inches.add(i,AppConstants.WOMEN_93_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() ==94 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_94);
                Inches.add(i,AppConstants.WOMEN_94_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 95 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_95);
                Inches.add(i,AppConstants.WOMEN_95_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 96 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_96);
                Inches.add(i,AppConstants.WOMEN_96_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 97 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_97);
                Inches.add(i,AppConstants.WOMEN_97_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 98 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_98);
                Inches.add(i,AppConstants.WOMEN_98_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 99 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_99);
                Inches.add(i,AppConstants.WOMEN_99_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 100 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_100);
                Inches.add(i,AppConstants.WOMEN_100_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 101 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_101);
                Inches.add(i,AppConstants.WOMEN_101_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 102 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_102);
                Inches.add(i,AppConstants.WOMEN_102_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 103 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_103);
                Inches.add(i,AppConstants.WOMEN_103_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 104 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_104);
                Inches.add(i,AppConstants.WOMEN_104_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 105 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_105);
                Inches.add(i,AppConstants.WOMEN_105_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 106 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_106);
                Inches.add(i,AppConstants.WOMEN_106_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 107 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_107);
                Inches.add(i,AppConstants.WOMEN_107_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 108 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_108);
                Inches.add(i,AppConstants.WOMEN_108_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 109 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_109);
                Inches.add(i,AppConstants.WOMEN_109_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 110 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_110);
                Inches.add(i,AppConstants.WOMEN_110_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 111 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_111);
                Inches.add(i,AppConstants.WOMEN_111_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 112 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_112);
                Inches.add(i,AppConstants.WOMEN_112_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 113 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_113);
                Inches.add(i,AppConstants.WOMEN_113_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 114 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_114);
                Inches.add(i,AppConstants.WOMEN_114_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 115 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_115);
                Inches.add(i,AppConstants.WOMEN_115_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 116 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_116);
                Inches.add(i,AppConstants.WOMEN_116_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 117 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_117);
                Inches.add(i,AppConstants.WOMEN_117_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 118 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_118);
                Inches.add(i,AppConstants.WOMEN_118_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 119 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_119);
                Inches.add(i,AppConstants.WOMEN_119_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 120 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_120);
                Inches.add(i,AppConstants.WOMEN_120_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 121 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_121);
                Inches.add(i,AppConstants.WOMEN_121_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 122 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_122);
                Inches.add(i,AppConstants.WOMEN_122_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 123 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_123);
                Inches.add(i,AppConstants.WOMEN_123_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 124 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_124);
                Inches.add(i,AppConstants.WOMEN_124_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 125 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_125);
                Inches.add(i,AppConstants.WOMEN_125_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 126 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_126);
                Inches.add(i,AppConstants.WOMEN_126_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 127 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_127);
                Inches.add(i,AppConstants.WOMEN_127_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 128 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_128);
                Inches.add(i,AppConstants.WOMEN_128_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 129 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_129);
                Inches.add(i,AppConstants.WOMEN_129_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 130 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_130);
                Inches.add(i,AppConstants.WOMEN_130_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 131 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_131);
                Inches.add(i,AppConstants.WOMEN_131_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 132 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_132);
                Inches.add(i,AppConstants.WOMEN_132_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 133 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_133);
                Inches.add(i,AppConstants.WOMEN_133_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 134 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_134);
                Inches.add(i,AppConstants.WOMEN_134_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 135 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_135);
                Inches.add(i,AppConstants.WOMEN_135_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 136 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_136);
                Inches.add(i,AppConstants.WOMEN_136_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 137 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_137);
                Inches.add(i,AppConstants.WOMEN_137_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 138 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_138);
                Inches.add(i,AppConstants.WOMEN_138_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 139 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_139);
                Inches.add(i,AppConstants.WOMEN_139_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 140 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_140);
                Inches.add(i,AppConstants.WOMEN_140_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 141 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_141);
                Inches.add(i,AppConstants.WOMEN_141_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 142 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_142);
                Inches.add(i,AppConstants.WOMEN_142_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 143 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_143);
                Inches.add(i,AppConstants.WOMEN_143_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 144 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_144);
                Inches.add(i,AppConstants.WOMEN_144_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 145 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_145);
                Inches.add(i,AppConstants.BOY_145_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 146 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_146);
                Inches.add(i,AppConstants.BOY_146_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 147 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_147);
                Inches.add(i,AppConstants.BOY_147_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 148 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_148);
                Inches.add(i,AppConstants.BOY_148_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 149 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_149);
                Inches.add(i,AppConstants.BOY_149_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 150 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_150);
                Inches.add(i,AppConstants.BOY_150_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 151 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_151);
                Inches.add(i,AppConstants.BOY_151_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 152 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_152);
                Inches.add(i,AppConstants.BOY_152_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 153 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_153);
                Inches.add(i,AppConstants.BOY_153_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 154 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_154);
                Inches.add(i,AppConstants.BOY_154_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 155 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_155);
                Inches.add(i,AppConstants.BOY_155_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 156 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_156);
                Inches.add(i,AppConstants.BOY_156_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 157 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_157);
                Inches.add(i,AppConstants.BOY_157_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 158 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_158);
                Inches.add(i,AppConstants.BOY_158_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 159 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_159);
                Inches.add(i,AppConstants.BOY_159_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 160 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_160);
                Inches.add(i,AppConstants.BOY_160_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 161 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_161);
                Inches.add(i,AppConstants.BOY_161_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 162 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_162);
                Inches.add(i,AppConstants.BOY_162_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 163 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_163);
                Inches.add(i,AppConstants.BOY_163_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 164 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_164);
                Inches.add(i,AppConstants.BOY_164_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 165 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_165);
                Inches.add(i,AppConstants.BOY_165_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 166 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_166);
                Inches.add(i,AppConstants.BOY_166_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 167 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_167);
                Inches.add(i,AppConstants.BOY_167_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 168 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_168);
                Inches.add(i,AppConstants.BOY_168_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 169 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_169);
                Inches.add(i,AppConstants.BOY_169_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 170 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_170);
                Inches.add(i,AppConstants.BOY_170_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 171 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_171);
                Inches.add(i,AppConstants.BOY_171_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 172 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_172);
                Inches.add(i,AppConstants.BOY_172_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 173 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_173);
                Inches.add(i,AppConstants.BOY_173_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 174 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_174);
                Inches.add(i,AppConstants.BOY_174_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 175 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_175);
                Inches.add(i,AppConstants.BOY_175_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 176 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_176);
                Inches.add(i,AppConstants.BOY_176_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 177 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_177);
                Inches.add(i,AppConstants.BOY_177_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 178 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_178);
                Inches.add(i,AppConstants.BOY_178_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 179 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_179);
                Inches.add(i,AppConstants.BOY_179_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 180 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_180);
                Inches.add(i,AppConstants.BOY_180_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 181 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_181);
                Inches.add(i,AppConstants.BOY_181_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 182 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_182);
                Inches.add(i,AppConstants.BOY_182_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 183 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_183);
                Inches.add(i,AppConstants.BOY_183_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 184 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_184);
                Inches.add(i,AppConstants.BOY_184_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 185 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_185);
                Inches.add(i,AppConstants.BOY_185_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 186 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_186);
                Inches.add(i,AppConstants.BOY_186_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 187 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_187);
                Inches.add(i,AppConstants.BOY_187_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 188 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_188);
                Inches.add(i,AppConstants.BOY_188_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 189 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_189);
                Inches.add(i,AppConstants.BOY_189_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 190 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_190);
                Inches.add(i,AppConstants.BOY_190_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 191 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_191);
                Inches.add(i,AppConstants.BOY_191_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 192 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_192);
                Inches.add(i,AppConstants.BOY_192_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 193 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_193);
                Inches.add(i,AppConstants.BOY_193_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 194 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_194);
                Inches.add(i,AppConstants.BOY_194_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 195 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_195);
                Inches.add(i,AppConstants.BOY_195_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 196 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_196);
                Inches.add(i,AppConstants.BOY_196_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 197 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_197);
                Inches.add(i,AppConstants.BOY_197_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 198 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_198);
                Inches.add(i,AppConstants.BOY_198_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 199 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_199);
                Inches.add(i,AppConstants.BOY_199_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 200 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_200);
                Inches.add(i,AppConstants.BOY_200_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 201 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_201);
                Inches.add(i,AppConstants.BOY_201_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 202 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_202);
                Inches.add(i,AppConstants.BOY_202_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 203 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_203);
                Inches.add(i,AppConstants.BOY_203_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 204 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_204);
                Inches.add(i,AppConstants.BOY_204_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 205 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_205);
                Inches.add(i,AppConstants.BOY_205_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 206 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_206);
                Inches.add(i,AppConstants.BOY_206_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 207 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_207);
                Inches.add(i,AppConstants.BOY_207_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 208 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_208);
                Inches.add(i,AppConstants.BOY_208_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 209 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_209);
                Inches.add(i,AppConstants.GIRL_209_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 210 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_210);
                Inches.add(i,AppConstants.GIRL_210_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 211 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_211);
                Inches.add(i,AppConstants.GIRL_211_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 212 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_212);
                Inches.add(i,AppConstants.GIRL_212_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 213 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_213);
                Inches.add(i,AppConstants.GIRL_213_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 214 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_214);
                Inches.add(i,AppConstants.GIRL_214_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 215 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_215);
                Inches.add(i,AppConstants.GIRL_215_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 216 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_216);
                Inches.add(i,AppConstants.GIRL_216_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 217 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_217);
                Inches.add(i,AppConstants.GIRL_217_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 218 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_218);
                Inches.add(i,AppConstants.GIRL_218_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 219 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_219);
                Inches.add(i,AppConstants.GIRL_219_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 220 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_220);
                Inches.add(i,AppConstants.GIRL_220_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 221 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_221);
                Inches.add(i,AppConstants.GIRL_221_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 222 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_222);
                Inches.add(i,AppConstants.GIRL_222_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 223 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_223);
                Inches.add(i,AppConstants.GIRL_223_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 224 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_224);
                Inches.add(i,AppConstants.GIRL_224_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 225 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_225);
                Inches.add(i,AppConstants.GIRL_225_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 226 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_226);
                Inches.add(i,AppConstants.GIRL_226_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 227 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_227);
                Inches.add(i,AppConstants.GIRL_227_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 228 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_228);
                Inches.add(i,AppConstants.GIRL_228_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 229 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_229);
                Inches.add(i,AppConstants.GIRL_229_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 230 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_230);
                Inches.add(i,AppConstants.GIRL_230_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 231 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_231);
                Inches.add(i,AppConstants.GIRL_231_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 232 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_232);
                Inches.add(i,AppConstants.GIRL_232_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 233 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_233);
                Inches.add(i,AppConstants.GIRL_233_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 234 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_234);
                Inches.add(i,AppConstants.GIRL_234_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 235 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_235);
                Inches.add(i,AppConstants.GIRL_235_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 236 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_236);
                Inches.add(i,AppConstants.GIRL_236_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 237 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_237);
                Inches.add(i,AppConstants.GIRL_237_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 238 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_238);
                Inches.add(i,AppConstants.GIRL_238_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 239 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_239);
                Inches.add(i,AppConstants.GIRL_239_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 240 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_240);
                Inches.add(i,AppConstants.GIRL_240_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 241 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_241);
                Inches.add(i,AppConstants.GIRL_241_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 242 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_242);
                Inches.add(i,AppConstants.GIRL_242_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 243 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_243);
                Inches.add(i,AppConstants.GIRL_243_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 244 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_244);
                Inches.add(i,AppConstants.GIRL_244_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 245 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_245);
                Inches.add(i,AppConstants.GIRL_245_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 246 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_246);
                Inches.add(i,AppConstants.GIRL_246_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 247 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_247);
                Inches.add(i,AppConstants.GIRL_247_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 248 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_248);
                Inches.add(i,AppConstants.GIRL_248_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 249 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_249);
                Inches.add(i,AppConstants.GIRL_249_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 250 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_250);
                Inches.add(i,AppConstants.GIRL_250_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 251 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_251);
                Inches.add(i,AppConstants.GIRL_251_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 252 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_252);
                Inches.add(i,AppConstants.GIRL_252_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 253 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_253);
                Inches.add(i,AppConstants.GIRL_253_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 254 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_254);
                Inches.add(i,AppConstants.GIRL_254_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 255 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_255);
                Inches.add(i,AppConstants.GIRL_255_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 256 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_256);
                Inches.add(i,AppConstants.GIRL_256_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 257 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_257);
                Inches.add(i,AppConstants.GIRL_257_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 258 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_258);
                Inches.add(i,AppConstants.GIRL_258_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 259 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_259);
                Inches.add(i,AppConstants.GIRL_259_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 260 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_260);
                Inches.add(i,AppConstants.GIRL_260_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 261 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_261);
                Inches.add(i,AppConstants.GIRL_261_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 262 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_262);
                Inches.add(i,AppConstants.GIRL_262_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 263 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_263);
                Inches.add(i,AppConstants.GIRL_263_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")) {
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 264 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_264);
                Inches.add(i,AppConstants.GIRL_264_INCHES);

                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                    }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 265 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_265);
                Inches.add(i,AppConstants.GIRL_265_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 266 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_266);
                Inches.add(i,AppConstants.GIRL_266_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 267 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_267);
                Inches.add(i,AppConstants.GIRL_267_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 268 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_268);
                Inches.add(i,AppConstants.GIRL_268_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 269 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_269);
                Inches.add(i,AppConstants.GIRL_269_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 270 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_270);
                Inches.add(i,AppConstants.GIRL_270_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 271 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_271);
                Inches.add(i,AppConstants.GIRL_271_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 272 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_272);
                Inches.add(i,AppConstants.GIRL_272_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 273 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_273);
                Inches.add(i,AppConstants.GIRL_273_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 274 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_274);
                Inches.add(i,AppConstants.GIRL_274_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 275 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_275);
                Inches.add(i,AppConstants.GIRL_275_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 276 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_276);
                Inches.add(i,AppConstants.GIRL_276_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 277 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_277);
                Inches.add(i,AppConstants.GIRL_277_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 278 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_278);
                Inches.add(i,AppConstants.GIRL_278_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 279 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_279);
                Inches.add(i,AppConstants.GIRL_279_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 280 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_280);
                Inches.add(i,AppConstants.GIRL_280_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 281 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_281);
                Inches.add(i,AppConstants.GIRL_281_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 282 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_282);
                Inches.add(i,AppConstants.GIRL_282_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 283 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_283);
                Inches.add(i,AppConstants.GIRL_283_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 284 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_284);
                Inches.add(i,AppConstants.GIRL_284_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 285 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_285);
                Inches.add(i,AppConstants.GIRL_285_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 286 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_286);
                Inches.add(i,AppConstants.GIRL_286_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 287 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_287);
                Inches.add(i,AppConstants.GIRL_287_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 288 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_288);
                Inches.add(i,AppConstants.GIRL_288_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }

                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.SelectedPartsId = String.valueOf(mMeasuremtnPartsList.get(position).getId());
                AppConstants.SelectedPartsImage = mMeasuremtnPartsList.get(position).getPartGifImage();
                AppConstants.SEEK_MIN = String.valueOf(mMeasuremtnPartsList.get(position).getStartMeasurementValue());
                AppConstants.SEEK_MAX = String.valueOf(mMeasuremtnPartsList.get(position).getEndMeasurementValue());
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    AppConstants.SelectedPartsName = mMeasuremtnPartsList.get(position).getTextInArabic();

                }else {
                    AppConstants.SelectedPartsName = mMeasuremtnPartsList.get(position).getTextInEnglish();

                }
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    AppConstants.SelectedPartsEditValue = Values.get(position);

                }else {
                    AppConstants.SelectedPartsEditValue = Inches.get(position);

                }

                ((MeasurementTwoScreen)mContext).getMeasurmentTwoEdtTxtPopUp();
            }
        });
        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
            holder.mMeasuremntPartsCmTxt.setText(Values.get(position));

        }else {
            holder.mMeasuremntPartsCmTxt.setText(Inches.get(position));

        }

    }

    @Override
    public int getItemCount() {
        return mMeasuremtnPartsList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_parts_img)
        ImageView mMeasurementPartsImg;

        @BindView(R.id.adapter_parts_txt)
        TextView mMeasurementPartsTxt;

        @BindView(R.id.adpater_parts_cm_txt)
        TextView mMeasuremntPartsCmTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

