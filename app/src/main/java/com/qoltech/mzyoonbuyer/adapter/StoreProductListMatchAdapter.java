package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.DashboardProductsEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.StoreProductListScreen;
import com.qoltech.mzyoonbuyer.ui.StoreProductViewDetailsScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreProductListMatchAdapter extends RecyclerView.Adapter<StoreProductListMatchAdapter.Holder> {

    Context mContext;

    ArrayList<DashboardProductsEntity> mProductList;

    UserDetailsEntity mUserDetailsEntityRes;

    public StoreProductListMatchAdapter(Context context, ArrayList<DashboardProductsEntity> productList) {
        mContext = context;
        mProductList = productList;
    }

    @NonNull
    @Override
    public StoreProductListMatchAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_store_product_list_match_parent,viewGroup,false);
        return new StoreProductListMatchAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull StoreProductListMatchAdapter.Holder holder, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        DashboardProductsEntity list = mProductList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mStoreProdNameTxt.setText(list.getProductNameInArabic());
            holder.mStoreProdBrandTxt.setText(list.getBrandNameInArabic());
        }else {
            holder.mStoreProdNameTxt.setText(list.getProductName());
            holder.mStoreProdBrandTxt.setText(list.getBrandName());
        }



        if (list.getDiscountType().equalsIgnoreCase("1")){
            holder.mStoreProdTotalAmtTxt.setVisibility(View.VISIBLE);
            holder.mStoreProduListOfferImg.setVisibility(View.VISIBLE);
            holder.mStoreProdOfferTxt.setVisibility(View.VISIBLE);
            holder.mStoreProdOfferTxt.setText(String.valueOf(list.getDiscount())+"\n"+"AED");

        }else if (list.getDiscountType().equalsIgnoreCase("2")){
            holder.mStoreProdTotalAmtTxt.setVisibility(View.VISIBLE);
            holder.mStoreProduListOfferImg.setVisibility(View.VISIBLE);
            holder.mStoreProdOfferTxt.setVisibility(View.VISIBLE);
            holder.mStoreProdOfferTxt.setText(String.valueOf(list.getDiscount())+"%"+"\n"+"OFF");

        }
        else {
            holder.mStoreProdTotalAmtTxt.setVisibility(View.GONE);
            holder.mStoreProduListOfferImg.setVisibility(View.GONE);
            holder.mStoreProdOfferTxt.setVisibility(View.GONE);
        }
        holder.mStoreProdAmtTxt.setText(String.valueOf(list.getNewPrice()));
        holder.mStoreProdTotalAmtTxt.setText(String.valueOf(list.getAmount()));

        Glide.with(mContext)
                .load(AppConstants.IMAGE_BASE_URL+"Images/Products/"+list.getProductImage())
                .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                .into(holder.mStoreProdImg);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.STORE_PRODUCT_ID = mProductList.get(position).getId();
                AppConstants.STORE_SELLER_ID = String.valueOf(mProductList.get(position).getSellerId());
                ((BaseActivity)mContext).nextScreen(StoreProductViewDetailsScreen.class,true);
            }
        });

        holder.mStoreProdHeartImg.setLiked(list.isProduct());

        holder.mStoreProdHeartImg.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                ((StoreProductListScreen)mContext).addToWishListApiCall(mProductList.get(position).getId(),String.valueOf(mProductList.get(position).getSellerId()));
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                ((StoreProductListScreen)mContext).removeToWishListApiCall(mProductList.get(position).getId(),String.valueOf(mProductList.get(position).getSellerId()));

            }
        });

    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.store_product_list_prod_heart_img)
        com.like.LikeButton mStoreProdHeartImg;

        @BindView(R.id.store_product_list_prod_img)
        ImageView mStoreProdImg;

        @BindView(R.id.store_product_list_offer_txt)
        TextView mStoreProdOfferTxt;

        @BindView(R.id.store_product_list_product_name_txt)
        TextView mStoreProdNameTxt;

        @BindView(R.id.store_product_list_product_brands_txt)
        TextView mStoreProdBrandTxt;

        @BindView(R.id.store_product_list_product_offered_amt_txt)
        TextView mStoreProdAmtTxt;

        @BindView(R.id.store_product_list_product_total_amt_txt)
        TextView mStoreProdTotalAmtTxt;

        @BindView(R.id.store_product_list_offer_img)
        ImageView mStoreProduListOfferImg;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
