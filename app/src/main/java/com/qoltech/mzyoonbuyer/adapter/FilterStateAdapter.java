package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.TailorListEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterStateAdapter extends RecyclerView.Adapter<FilterStateAdapter.Holder> {

    private Context mContext;
    private ArrayList<TailorListEntity> mTailorLists;


    private UserDetailsEntity mUserDetailsEntityRes;
    public FilterStateAdapter(Context activity, ArrayList<TailorListEntity> tailorList) {
        mContext = activity;
        mTailorLists = tailorList;
}

    @NonNull
    @Override
    public FilterStateAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_tailor_filter_state_list, parent, false);
        return new FilterStateAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterStateAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final TailorListEntity tailorEntity = mTailorLists.get(position);


        holder.mFilterStateNameTxt.setText(tailorEntity.getStateName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tailorEntity.getChecked()){
                    mTailorLists.get(position).setChecked(false);
                }else {
                    mTailorLists.get(position).setChecked(true);
                }
                notifyDataSetChanged();
            }

        });

        holder.mFilterCheckedImg.setBackground(tailorEntity.getChecked() ? mContext.getResources().getDrawable(R.drawable.tick_selected_img) : mContext.getResources().getDrawable(R.drawable.tick_unselected_img));


    }

    @Override
    public int getItemCount() {
        return mTailorLists.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_filter_par_lay)
        RelativeLayout mAdatperFilterParLay;

        @BindView(R.id.filter_checked_img)
        ImageView mFilterCheckedImg;

        @BindView(R.id.filter_state_name_txt)
        TextView mFilterStateNameTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
