package com.qoltech.mzyoonbuyer.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.StoreCartEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.fragment.CartFragment;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.OrderSummaryScreen;
import com.qoltech.mzyoonbuyer.ui.StoreProductViewDetailsScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.qoltech.mzyoonbuyer.main.MZYOONApplication.getContext;

public class StoreCartInnerAdapter extends RecyclerView.Adapter<StoreCartInnerAdapter.Holder> {

    ArrayList<StoreCartEntity> mStoreCartList ;
    Context mContext;
    CartFragment mCartFragment;
    UserDetailsEntity mUserDetailsEntityRes;
    Dialog mQuantityDialog;
    private GetAppointmentTimeSlotAdapter mGetAppointmentTimeSlotAdater;
    private ArrayList<String> mQuantityList;
    String mScreen;


    public StoreCartInnerAdapter(ArrayList<StoreCartEntity> storeCartList, Context context,CartFragment cartFragment,String Screen) {
        mStoreCartList = storeCartList;
        mContext = context;
        mCartFragment = cartFragment;
        mScreen = Screen;
    }

    public StoreCartInnerAdapter(ArrayList<StoreCartEntity> storeCartList, Context context,String Screen) {
        mStoreCartList = storeCartList;
        mContext = context;
        mScreen = Screen;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_cart_inner_list,viewGroup,false);
        return new StoreCartInnerAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        StoreCartEntity list = mStoreCartList.get(position);

        mQuantityList = new ArrayList<>();

        for (int i=1; i<=mStoreCartList.get(position).getAvailibilityCount(); i++){
            mQuantityList.add(String.valueOf(i));
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mAdapterCartInnerProdTxt.setText(list.getProductNameInArabic());
            holder.mAdapterCartInnerBrandTxt.setText(list.getBrandNameInArabic());
        }else {
            holder.mAdapterCartInnerProdTxt.setText(list.getProductName());
            holder.mAdapterCartInnerBrandTxt.setText(list.getBrandName());

        }

        holder.mAdapterCartInnerSizeTxt.setText(list.getSizeId() == -1 ? mContext.getResources().getString(R.string.not_available) : list.getSize());
        holder.mAdapterCartInnerColorEmptyTxt.setVisibility(list.getColorId() == -1 ? View.VISIBLE : View.GONE);
        holder.mAdapterCartInnerColorEmptyTxt.setText(list.getColorId() == -1 ? mContext.getResources().getString(R.string.not_available) : "");
        holder.mAdapterCartInnerColorImgLay.setVisibility(list.getColorId() == -1 ? View.GONE : View.VISIBLE);
        holder.mAdapterCartInnerPriceTxt.setText(String.valueOf(list.getNewPrice()));
        holder.mAdaptercartInnerQtyTxt.setText(String.valueOf(list.getQuantity()));
        holder.mAdapterCartInnerTotalTxt.setText(String.valueOf(list.getNewPrice()*list.Quantity));

        try {
            int color = Color.parseColor(list.getColorCode().trim());
            ColorDrawable colorDrawable = new ColorDrawable(color);

            Glide.with(mContext)
                    .load(colorDrawable)
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_background_img).error(R.drawable.dress_type_background_img))
                    .into(holder.mAdapterCartInnerColorImg);

        }catch (Exception e){

            Glide.with(mContext).load(R.drawable.dress_type_background_img)
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_background_img).error(R.drawable.dress_type_background_img))
                    .into(holder.mAdapterCartInnerColorImg);
        }

        Glide.with(mContext)
                .load(AppConstants.IMAGE_BASE_URL+"Images/Products/"+list.getProductImage())
                .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                .into(holder.mAdapterCartInnerProdImg);

//        holder.mAdapterCartInnerProdImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AppConstants.STORE_PRODUCT_ID = mStoreCartList.get(position).getProductId();
//                AppConstants.STORE_SELLER_ID = String.valueOf(mStoreCartList.get(position).getSellerId());
//                ((BaseActivity)mContext).nextScreen(StoreProductViewDetailsScreen.class,true);
//            }
//        });

        holder.mAdapterCartInnerProdImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.STORE_PRODUCT_ID = mStoreCartList.get(position).getProductId();
                AppConstants.STORE_SELLER_ID = String.valueOf(mStoreCartList.get(position).getSellerId());
                ((BaseActivity)mContext).nextScreen(StoreProductViewDetailsScreen.class,true);
            }
        });

        holder.mAdapterCartInnerQtyLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mStoreCartList.get(position).getAvailibilityCount() == 0){
                    Toast.makeText(mContext,mContext.getResources().getString(R.string.currently_unavailable),Toast.LENGTH_SHORT).show();
                }else {
                    if (mQuantityList.size() > 0){
                        ((BaseActivity)mContext).alertDismiss(mQuantityDialog);
                        mQuantityDialog = ((BaseActivity)mContext).getDialog(mContext, R.layout.pop_up_country_alert);

                        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                        Window window = mQuantityDialog.getWindow();

                        if (window != null) {
                            LayoutParams.copyFrom(window.getAttributes());
                            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(LayoutParams);
                            window.setGravity(Gravity.CENTER);
                        }

                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            ViewCompat.setLayoutDirection(mQuantityDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                        }else {
                            ViewCompat.setLayoutDirection(mQuantityDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                        }

                        TextView cancelTxt , headerTxt;

                        RecyclerView countryRecyclerView;

                        /*Init view*/
                        cancelTxt = mQuantityDialog.findViewById(R.id.country_text_cancel);
                        headerTxt = mQuantityDialog.findViewById(R.id.country_header_text_cancel);

                        countryRecyclerView = mQuantityDialog.findViewById(R.id.country_popup_recycler_view);
                        if (mScreen.equalsIgnoreCase("OrderSummary")){
                            mGetAppointmentTimeSlotAdater = new GetAppointmentTimeSlotAdapter(mContext, mQuantityList,mQuantityDialog,"OrderSummary",mStoreCartList.get(position).getProductId(),String.valueOf(mStoreCartList.get(position).getSizeId()),String.valueOf(mStoreCartList.get(position).getColorId()),String.valueOf(mStoreCartList.get(position).getSellerId()));

                        }else {
                            mGetAppointmentTimeSlotAdater = new GetAppointmentTimeSlotAdapter(mContext, mQuantityList,mQuantityDialog,"STORE_CART_LIST",mStoreCartList.get(position).getProductId(),String.valueOf(mStoreCartList.get(position).getSizeId()),String.valueOf(mStoreCartList.get(position).getColorId()),String.valueOf(mStoreCartList.get(position).getSellerId()),mCartFragment);

                        }

                        countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        countryRecyclerView.setAdapter(mGetAppointmentTimeSlotAdater);

                        /*Set data*/
                        headerTxt.setText(mContext.getResources().getString(R.string.select_your_qty));
                        cancelTxt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mQuantityDialog.dismiss();
                            }
                        });

                        ((BaseActivity)mContext).alertShowing(mQuantityDialog);
                    }else {
                        Toast.makeText(mContext,mContext.getResources().getString(R.string.loading_plz_wait),Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });

        holder.mAdapterCartInnerDeleteImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.getInstance().showOptionPopup(mContext, mContext.getResources().getString(R.string.are_sure_you_want_to_delte_this_product_from_cart), mContext.getResources().getString(R.string.yes), mContext.getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                    @Override
                    public void onNegativeClick() {

                    }

                    @Override
                    public void onPositiveClick() {
                        if (mScreen.equalsIgnoreCase("OrderSummary")){
                            ((OrderSummaryScreen)mContext).removeFromCart(mStoreCartList.get(position).getProductId(),String.valueOf(mStoreCartList.get(position).getSizeId()),String.valueOf(mStoreCartList.get(position).getColorId()),String.valueOf(mStoreCartList.get(position).getSellerId()));

                        }else {
                            mCartFragment.removeFromCart(mStoreCartList.get(position).getProductId(),String.valueOf(mStoreCartList.get(position).getSizeId()),String.valueOf(mStoreCartList.get(position).getColorId()),String.valueOf(mStoreCartList.get(position).getSellerId()));

                        }
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return mStoreCartList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.adapter_cart_inner_prod_txt)
        TextView mAdapterCartInnerProdTxt;

        @BindView(R.id.adapter_cart_inner_delete_img)
        ImageView mAdapterCartInnerDeleteImg;

        @BindView(R.id.adapter_cart_inner_size_txt)
        TextView mAdapterCartInnerSizeTxt;

        @BindView(R.id.adapter_cart_inner_color_empty_txt)
        TextView mAdapterCartInnerColorEmptyTxt;

        @BindView(R.id.adapter_cart_inner_color_img)
        ImageView mAdapterCartInnerColorImg;

        @BindView(R.id.adapter_cart_inner_price_txt)
        TextView mAdapterCartInnerPriceTxt;

        @BindView(R.id.adapter_cart_inner_qty_txt)
        TextView mAdaptercartInnerQtyTxt;

        @BindView(R.id.adapter_cart_inner_brand_txt)
        TextView mAdapterCartInnerBrandTxt;

        @BindView(R.id.adapter_cart_inner_total_txt)
        TextView mAdapterCartInnerTotalTxt;

        @BindView(R.id.adapter_cart_inner_prod_img)
        ImageView mAdapterCartInnerProdImg;

        @BindView(R.id.adapter_cart_inner_qty_lay)
        RelativeLayout mAdapterCartInnerQtyLay;

        @BindView(R.id.adapter_cart_inner_color_img_lay)
        CardView mAdapterCartInnerColorImgLay;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
