package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetTrackingEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GetTrackingDetailsAdapter  extends RecyclerView.Adapter<GetTrackingDetailsAdapter.Holder> {

    private Context mContext;
    private ArrayList<GetTrackingEntity> mTrackingList;
    private UserDetailsEntity mUserDetailEntity;

    public GetTrackingDetailsAdapter(Context activity, ArrayList<GetTrackingEntity> getTrackingList, UserDetailsEntity userDetailsEntity) {
        mContext = activity;
        mTrackingList = getTrackingList;
        mUserDetailEntity = userDetailsEntity;
    }

    @NonNull
    @Override
    public GetTrackingDetailsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_get_tracking_details_list, parent, false);
        return new GetTrackingDetailsAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final GetTrackingDetailsAdapter.Holder holder, final int position) {
        GetTrackingEntity getTrackingEntity = mTrackingList.get(position);

        if (mUserDetailEntity.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mAdapterTrackingDetailPlacedTxt.setText(getTrackingEntity.getStatusInArabic());

        }else {
            holder.mAdapterTrackingDetailPlacedTxt.setText(getTrackingEntity.getStatus());

        }

//        String[] parts = getTrackingEntity.getDate().split("T");
        holder.mAdapterGetTrackingDatePlaecdTxt.setText(mTrackingList.get(position).getDate());
        holder.mAdapterGetTrackingTimePlacedTxt.setVisibility(View.GONE);
        if (holder.getAdapterPosition() == 0){
            holder.mAdapterTrackingDetailsLineLay.setVisibility(View.GONE);
        }else {
            holder.mAdapterTrackingDetailsLineLay.setVisibility(View.VISIBLE);
        }

//        String string = getTrackingEntity.getDate();
//        String string2 = getTrackingEntity.getDate().replace("T"," ");
//        String[] parts = string.split("T");
//        String[] dates = parts[0].split("-");
//        String part1 = dates[2]+"/"+dates[1]+"/"+dates[0];
//        String input_date= part1;
//        SimpleDateFormat format1=new SimpleDateFormat("dd/MM/yyyy");
//        String startTime = string2.replace("/","-");
//        StringTokenizer tk = new StringTokenizer(startTime);
//        String date = tk.nextToken();
//        String time = tk.nextToken();
//
//        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
//        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
//        Date dt;
//        try {
//            dt = sdf.parse(time);
//            System.out.println("Time Display: " + sdfs.format(dt)); // <-- I got result here
//            Date dt1=format1.parse(input_date);
//            String dayOfTheWeek = (String) DateFormat.format("EE",  dt1);
//            String day          = (String) DateFormat.format("dd",    dt1);
//            String monthString  = (String) DateFormat.format("MMM",   dt1);
//            String monthNumber  = (String) DateFormat.format("MM",    dt1);
//            String year         = (String) DateFormat.format("yyyy",  dt1);
//
//            holder.mAdapterGetTrackingDatePlaecdTxt.setText(dayOfTheWeek+", "+day+" "+monthString+" ");
//            holder.mAdapterGetTrackingTimePlacedTxt.setText(sdfs.format(dt));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

    }

    @Override
    public int getItemCount() {
        return mTrackingList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_get_tracking_date_placed_txt)
        TextView mAdapterGetTrackingDatePlaecdTxt;

        @BindView(R.id.adapter_get_tracking_time_placed_txt)
        TextView mAdapterGetTrackingTimePlacedTxt;

        @BindView(R.id.adapter_tracking_details_placing_txt)
        TextView mAdapterTrackingDetailPlacedTxt;

        @BindView(R.id.adapter_tracking_details_line_lay)
        View mAdapterTrackingDetailsLineLay;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}





