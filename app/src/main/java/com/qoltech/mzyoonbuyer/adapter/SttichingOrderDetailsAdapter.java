package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.StichingOrderDetailsEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.modal.OrderDetailsStitchingStoreModal;
import com.qoltech.mzyoonbuyer.ui.AppointmentDetails;
import com.qoltech.mzyoonbuyer.ui.MeasurementListScreen;
import com.qoltech.mzyoonbuyer.ui.OrderTrackingDetailsScreen;
import com.qoltech.mzyoonbuyer.ui.RatingAndReviewScreen;
import com.qoltech.mzyoonbuyer.ui.ScheduletDetailScreen;
import com.qoltech.mzyoonbuyer.ui.ViewDetailsScreen;
import com.qoltech.mzyoonbuyer.ui.ViewImageScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SttichingOrderDetailsAdapter extends RecyclerView.Adapter<SttichingOrderDetailsAdapter.Holder> {

    ArrayList<StichingOrderDetailsEntity> mStitchingOrderDetailsList;
    Context mContext;
    OrderDetailsStitchingStoreModal mOrderDetailsStitchingStoreModal;
    ArrayList<String> mMaterialAndPatternList;
    ArrayList<String> mReferenceList;
    String mMaterialTypeStr = "", mMaterialTypeNameStr = "", mMeasurementTypeStr = "", mMeasurementTypeNameStr = "", mDeliveryTypeStr = "", mDeliveryTypeNameStr = "";
     UserDetailsEntity mUserDetailsEntityRes;

    public SttichingOrderDetailsAdapter(ArrayList<StichingOrderDetailsEntity> stitchingOrderDetailsList, Context context,OrderDetailsStitchingStoreModal orderDetailsStitchingStoreModal) {
        mStitchingOrderDetailsList = stitchingOrderDetailsList;
        mContext = context;
        mOrderDetailsStitchingStoreModal = orderDetailsStitchingStoreModal;
    }

    @NonNull
    @Override
    public SttichingOrderDetailsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_stitching_order_details_list,viewGroup,false);
        return new SttichingOrderDetailsAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull SttichingOrderDetailsAdapter.Holder holder, int position) {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        mMaterialAndPatternList = new ArrayList<>();
        mReferenceList = new ArrayList<>();
        holder.mStitchingOrderDetailsOrderTypeTxt.setText(mStitchingOrderDetailsList.get(position).getOrderType());
//        holder.mStitchingOrderDetailsDeliveryTypeTxt.setText(mStitchingOrderDetailsList.get(position).getDeliveryNameInEnglish());

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mStitchingOrderDetailsStitchingTimeTxt.setText(mStitchingOrderDetailsList.get(position).getDeliveryTypeInArabic());

        }else {
            holder.mStitchingOrderDetailsStitchingTimeTxt.setText(mStitchingOrderDetailsList.get(position).getDeliveryTypeInEnglish());

        }
        holder.mStitchingOrderDetailsProdNameTxt.setText(mStitchingOrderDetailsList.get(position).getDressSubtypeName());
        holder.mStitchingOrderDetailsPriceTxt.setText(mStitchingOrderDetailsList.get(position).getPrice());
        holder.mStitchingOrderDetailsQtyTxt.setText(String.valueOf(mStitchingOrderDetailsList.get(position).getQuantity()));
        holder.mStitchingOrderDetailsTotalTxt.setText(mStitchingOrderDetailsList.get(position).getUnitTotal()+" "+ mContext.getResources().getString(R.string.aed));
        holder.mStitchingOrderDetailsMaterialTypeTxt.setText(mStitchingOrderDetailsList.get(position).getMaterialTypeInEnglish());
        holder.mStitchingOrderDetailsStatusTxt.setText(mStitchingOrderDetailsList.get(position).getStatus());
        holder.mStitchingRatingAndReviewTxt.setText(mStitchingOrderDetailsList.get(position).getRating() > 0 ? mContext.getResources().getString(R.string.view_review) : mContext.getResources().getString(R.string.write_a_review));
        holder.mStitchingRatingBar.setRating(Float.parseFloat(String.valueOf(mStitchingOrderDetailsList.get(position).getRating())));

        if (mOrderDetailsStitchingStoreModal.getReferenceImage().size() > 0){
            holder.mStitchingReferenceImgTxt.setVisibility(View.VISIBLE);
        }else {
            holder.mStitchingReferenceImgTxt.setVisibility(View.INVISIBLE);
        }

        if (AppConstants.PENDING_DELIVERY_CLICK.equalsIgnoreCase("Delivery")){
            holder.mStitchingRatingAndReviewParLay.setVisibility(View.VISIBLE);
            holder.mStitchingRateAndReviewView.setVisibility(View.VISIBLE);
        }
        if (AppConstants.PENDING_DELIVERY_CLICK.equalsIgnoreCase("Pending")){
            holder.mStitchingRatingAndReviewParLay.setVisibility(View.GONE);
            holder.mStitchingRateAndReviewView.setVisibility(View.GONE);

        }

        if (mOrderDetailsStitchingStoreModal.getStichingOrderDetails().size() > 0){
            mMaterialTypeStr = String.valueOf(mOrderDetailsStitchingStoreModal.getStichingOrderDetails().get(0).getOrderTypeId());
            mMaterialTypeNameStr = mOrderDetailsStitchingStoreModal.getStichingOrderDetails().get(0).getMaterialTypeInEnglish();
            mMeasurementTypeStr = String.valueOf(mOrderDetailsStitchingStoreModal.getStichingOrderDetails().get(0).getMeasurementTypeId());
            mMeasurementTypeNameStr = mOrderDetailsStitchingStoreModal.getStichingOrderDetails().get(0).getMeasurementInEnglish();
            mDeliveryTypeStr =String.valueOf(mOrderDetailsStitchingStoreModal.getStichingOrderDetails().get(0).getDeliveryTypeId());
            mDeliveryTypeNameStr = mOrderDetailsStitchingStoreModal.getStichingOrderDetails().get(0).getDeliveryTypeInEnglish();

        }

        for (int i=0; i<mOrderDetailsStitchingStoreModal.getMaterialImage().size(); i++){
            mMaterialAndPatternList.add(AppConstants.IMAGE_BASE_URL+"Images/MaterialImages/"+mOrderDetailsStitchingStoreModal.getMaterialImage().get(i).getImage());
        }

        for (int i=0; i<mOrderDetailsStitchingStoreModal.getReferenceImage().size(); i++){
            mReferenceList.add(AppConstants.IMAGE_BASE_URL+"Images/ReferenceImages/"+mOrderDetailsStitchingStoreModal.getReferenceImage().get(i).getImage());
        }

        for (int i=0; i<mOrderDetailsStitchingStoreModal.getAdditionalMaterialImage().size(); i++){
            mMaterialAndPatternList.add(AppConstants.IMAGE_BASE_URL+"Images/Pattern/"+mOrderDetailsStitchingStoreModal.getAdditionalMaterialImage().get(i).getImage());
        }

        if (mOrderDetailsStitchingStoreModal.getGetPaymentReceivedStatus().size() > 0){
            if (mOrderDetailsStitchingStoreModal.getGetPaymentReceivedStatus().get(0).getPaymentReceivedStatus().equalsIgnoreCase("Not paid")){
                holder.mStitchingTrackTxt.setVisibility(View.INVISIBLE);
            }else {
                holder.mStitchingTrackTxt.setVisibility(View.VISIBLE);

            }
        }

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"images/DressSubType/"+mStitchingOrderDetailsList.get(0).getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mStitchingOrderDetailsProdImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.mStitchingCustomizationTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.ORDER_DETAILS_CUSTOMIZATION = "CUSTOMIZATION";
                AppConstants.VIEW_DETAILS_HEADER = mContext.getResources().getString(R.string.customization_images);
                ((BaseActivity)mContext).nextScreen(ViewImageScreen.class,true);
            }
        });

        holder.mStitchingMeasurementTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.NEW_ORDER = "ADD_MEASUREMENT";
                AppConstants.ORDER_DETAILS_MEASUREMENT = "ORDER";
                ((BaseActivity)mContext).nextScreen(MeasurementListScreen.class,true);
            }
        });
        holder.mStitchingMaterialImgTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mStitchingOrderDetailsList.get(position).getOrderTypeId() == 3){
                    AppConstants.VIEW_DETAILS_RES = "GONE";
                    ((BaseActivity)mContext).nextScreen(ViewDetailsScreen.class,true);
                }else {
                    AppConstants.VIEW_IMAGE_LIST = new ArrayList<>();
                    AppConstants.VIEW_IMAGE_LIST = mMaterialAndPatternList;
                    AppConstants.ORDER_DETAILS_CUSTOMIZATION = "";

                    AppConstants.VIEW_DETAILS_HEADER = mContext.getResources().getString(R.string.material_images);

                    ((BaseActivity)mContext).nextScreen(ViewImageScreen.class,true);
                }
            }
        });
        holder.mStitchingReferenceImgTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.VIEW_IMAGE_LIST = new ArrayList<>();
                AppConstants.VIEW_IMAGE_LIST = mReferenceList;
                AppConstants.ORDER_DETAILS_CUSTOMIZATION = "";

                AppConstants.VIEW_DETAILS_HEADER = mContext.getResources().getString(R.string.reference_images);

                ((BaseActivity)mContext).nextScreen(ViewImageScreen.class,true);
            }
        });
        holder.mStitchingRatingAndReviewParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.ORDER_PENDING_ID = String.valueOf(mStitchingOrderDetailsList.get(position).getOrderId());
                AppConstants.TAILOR_CLICK_ID = String.valueOf(mStitchingOrderDetailsList.get(position).getSellerId());
                ((BaseActivity)mContext).nextScreen(RatingAndReviewScreen.class,true);

            }
        });
        holder.mStitchingTrackTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AppConstants.TRACKING_ORDER_TYPE = "";
                AppConstants.ORDER_PENDING_ID = String.valueOf(mStitchingOrderDetailsList.get(position).getOrderId());
                ((BaseActivity)mContext).nextScreen(OrderTrackingDetailsScreen.class,true);

            }
        });

        holder.mOrderDetailsMaterialCreateAppointmentTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.CHECK_BOOK_APPOINTMENT = "RequestList";
                ((BaseActivity)mContext).nextScreen(AppointmentDetails.class,true);
            }
        });

        holder.mOrderDetailsMeasurmentCreateAppointmentTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.CHECK_BOOK_APPOINTMENT = "RequestList";
                ((BaseActivity)mContext).nextScreen(AppointmentDetails.class,true);
            }
        });

        holder.mOrderDetailsDeliveryCreateAppointmentTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.CHECK_BOOK_APPOINTMENT = "RequestList";
                ((BaseActivity)mContext).nextScreen(ScheduletDetailScreen.class,true);
            }
        });

        if (mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMateril().size()>0){
            if(AppConstants.InitiatedBy.equalsIgnoreCase("Tailor")){
                holder.mOrderDetailsMaterialAppointmentLay.setVisibility(View.GONE);

            }else {

                holder.mOrderDetailsMaterialAppointmentLay.setVisibility(View.VISIBLE);
                holder.mOrderDetailsMaterialEmptyTxt.setVisibility(View.GONE);
                if (mMaterialTypeStr.equalsIgnoreCase("2")) {
                    holder.mOrderDetailsMaterialCreateAppointmentTxt.setText(mContext.getResources().getString(R.string.create_appointment));
                } else if (mMaterialTypeStr.equalsIgnoreCase("1")) {
                    holder.mOrderDetailsMaterialCreateAppointmentTxt.setText(mContext.getResources().getString(R.string.approve_appointment));
                }
                if (mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMateril().get(0).getStatus().equalsIgnoreCase("Approved") || mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMateril().get(0).getStatus().equalsIgnoreCase("Tailor Approved") || mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMateril().get(0).getStatus().equalsIgnoreCase("Waiting for Tailor Approval") || mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMateril().get(0).getStatus().equalsIgnoreCase("Rejected")) {
                    holder.mOrderDetailsMaterialCreateAppointmentTxt.setVisibility(View.GONE);
                    holder.mOrderDetailsMaterialAppointmentLeftParLay.setVisibility(View.GONE);
                }

                if (mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMateril().get(0).getAppointmentLeftMaterialCount() == 0) {
                    holder.mOrderDetailsMaterialCreateAppointmentTxt.setVisibility(View.GONE);
                    holder.mOrderDetailsMaterialAppointmentLeftParLay.setVisibility(View.GONE);
                }
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    holder.mOrderDetailsMaterialAppointmentTypeTxt.setText(mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMateril().get(0).getHeaderInArabic());

                } else {
                    holder.mOrderDetailsMaterialAppointmentTypeTxt.setText(mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMateril().get(0).getHeaderInEnglish());
                }

                holder.mOrderDetailsMaterialAppointmentTypeTimeTxt.setText(mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMateril().get(0).getAppointmentTime());
                holder.mOrderDetailsMaterialAppointmentTypeDateAndTimeTxt.setText(mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMateril().get(0).getOrderDt());
                holder.mOrderDetailsMaterialAppointmentTypeStatusTxt.setText(mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMateril().get(0).getStatus());
                holder.mOrderDetailsMaterialAppointmentTypeLeftTxt.setText(String.valueOf(mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMateril().get(0).getAppointmentLeftMaterialCount()));
            }
        }else {
            if(AppConstants.InitiatedBy.equalsIgnoreCase("Tailor")){
                holder.mOrderDetailsMaterialAppointmentLay.setVisibility(View.GONE);

            }else {

                holder.mOrderDetailsMaterialAppointmentLay.setVisibility(View.GONE);

                if (mMaterialTypeStr.equalsIgnoreCase("2")){
                    holder.mOrderDetailsMaterialAppointmentLay.setVisibility(View.VISIBLE);
                    holder.mOrderDetailsMaterialAppointmentLeftParLay.setVisibility(View.GONE);
                    holder.mOrderDetailsMaterialOrderDateLay.setVisibility(View.GONE);
                    holder.mOrderDetailsMaterialOrderTimeLay.setVisibility(View.GONE);
                    holder.mOrderDetailsPaymentStatusLay.setVisibility(View.GONE);
                    holder.mOrderDetailsMaterialEmptyTxt.setVisibility(View.VISIBLE);
                    holder.mOrderDetailsMaterialAppointmentTypeTxt.setText(mMaterialTypeNameStr);
                    holder.mOrderDetailsMaterialCreateAppointmentTxt.setText(mContext.getResources().getString(R.string.create_appointment));
                    holder.mOrderDetailsMaterialCreateAppointmentTxt.setVisibility(View.VISIBLE);
                    holder.mOrderDetailsMaterialEmptyTxt.setText(mContext.getResources().getString(R.string.appointment_details_by_yours));

                }else if (mMaterialTypeStr.equalsIgnoreCase("1")){
                    holder.mOrderDetailsMaterialAppointmentLay.setVisibility(View.VISIBLE);
                    holder.mOrderDetailsMaterialAppointmentLeftParLay.setVisibility(View.GONE);
                    holder.mOrderDetailsMaterialOrderDateLay.setVisibility(View.GONE);
                    holder.mOrderDetailsMaterialOrderTimeLay.setVisibility(View.GONE);
                    holder.mOrderDetailsPaymentStatusLay.setVisibility(View.GONE);
                    holder.mOrderDetailsMaterialEmptyTxt.setVisibility(View.VISIBLE);
                    holder.mOrderDetailsMaterialAppointmentTypeTxt.setText(mMaterialTypeNameStr);
                    holder.mOrderDetailsMaterialCreateAppointmentTxt.setVisibility(View.GONE);
                    holder.mOrderDetailsMaterialEmptyTxt.setText(mContext.getResources().getString(R.string.appointment_details_by_tailor));

                }
            }
        }

        if (mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMeasurement().size()>0){
            if(AppConstants.InitiatedBy.equalsIgnoreCase("Tailor")){
               holder.mOrderDetailsMeasurementAppointmentLay.setVisibility(View.GONE);

            }
            else {

                holder.mOrderDetailsMeasurementAppointmentLay.setVisibility(View.VISIBLE);
                holder.mOrderDetailsMeasurementEmtptyTxt.setVisibility(View.GONE);

                if (mMeasurementTypeStr.equalsIgnoreCase("3")){

                    holder.mOrderDetailsMeasurmentCreateAppointmentTxt.setText(mContext.getResources().getString(R.string.create_appointment));
                }else if (mMeasurementTypeStr.equalsIgnoreCase("2")){

                    holder.mOrderDetailsMeasurmentCreateAppointmentTxt.setText(mContext.getResources().getString(R.string.approve_appointment));
                }


                if (mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMeasurement().get(0).getStatus().equalsIgnoreCase("Approved")||mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMeasurement().get(0).getStatus().equalsIgnoreCase("Tailor Approved")||mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMeasurement().get(0).getStatus().equalsIgnoreCase("Waiting for Tailor Approval")||mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMeasurement().get(0).getStatus().equalsIgnoreCase("Rejected")){
                    holder.mOrderDetailsMeasurmentCreateAppointmentTxt.setVisibility(View.GONE);
                    holder.mOrderDetailsMeasurementAppointmentLeftParLay.setVisibility(View.GONE);
                }
                if (mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMeasurement().get(0).getAppointmentLeftMeasurementCount() == 0){
                    holder.mOrderDetailsMeasurmentCreateAppointmentTxt.setVisibility(View.GONE);
                    holder.mOrderDetailsMeasurementAppointmentLeftParLay.setVisibility(View.GONE);
                }
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    holder.mOrderDetailsMeasurementAppointmentTypeTxt.setText(mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMeasurement().get(0).getMeasurementInArabic());

                }else {
                    holder.mOrderDetailsMeasurementAppointmentTypeTxt.setText(mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMeasurement().get(0).getMeasurementInEnglish());
                }

                holder.mOrderDetailsMeasurementAppointmentTypeTimeTxt.setText(mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMeasurement().get(0).getAppointmentTime());
                holder.mOrderDetailsMeasurementAppointmentTypeDateAndTimeTxt.setText(mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMeasurement().get(0).getOrderDt());
                holder.mOrderDetailsMeasurementAppointmentTypeStatusTxt.setText(mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMeasurement().get(0).getStatus());
                holder.mOrderDetailsMeasurementDeliveryAppointmentTypeLeftTxt.setText(String.valueOf(mOrderDetailsStitchingStoreModal.getGetAppoinmentLeftMeasurement().get(0).getAppointmentLeftMeasurementCount()));
            }
        }else {
            if(AppConstants.InitiatedBy.equalsIgnoreCase("Tailor")){
                holder.mOrderDetailsMeasurementAppointmentLay.setVisibility(View.GONE);

            }else {

                holder.mOrderDetailsMeasurementAppointmentLay.setVisibility(View.GONE);
                if (mMeasurementTypeStr.equalsIgnoreCase("3")){
                    holder.mOrderDetailsMeasurementAppointmentLay.setVisibility(View.VISIBLE);
                    holder.mOrderDetailsMeasurementAppointmentLeftParLay.setVisibility(View.GONE);
                    holder.mOrderDetailsMeasurementOrderDetaLay.setVisibility(View.GONE);
                    holder.mOrderDetailsMeasurementOrderTimeLay.setVisibility(View.GONE);
                    holder.mOrderDetailsMeasurementStatusLay.setVisibility(View.GONE);
                    holder.mOrderDetailsMeasurementAppointmentTypeTxt.setText(mMeasurementTypeNameStr);
                    holder.mOrderDetailsMeasurementEmtptyTxt.setVisibility(View.VISIBLE);
                    holder.mOrderDetailsMeasurmentCreateAppointmentTxt.setVisibility(View.VISIBLE);
                    holder.mOrderDetailsMeasurmentCreateAppointmentTxt.setText(mContext.getResources().getString(R.string.create_appointment));
                    holder.mOrderDetailsMeasurementEmtptyTxt.setText(mContext.getResources().getString(R.string.appointment_details_by_yours));

                }else if (mMeasurementTypeStr.equalsIgnoreCase("2")){
                    holder.mOrderDetailsMeasurementAppointmentLay.setVisibility(View.VISIBLE);
                    holder.mOrderDetailsMeasurementAppointmentLeftParLay.setVisibility(View.GONE);
                    holder.mOrderDetailsMeasurementOrderDetaLay.setVisibility(View.GONE);
                    holder.mOrderDetailsMeasurementOrderTimeLay.setVisibility(View.GONE);
                    holder.mOrderDetailsMeasurementStatusLay.setVisibility(View.GONE);
                    holder.mOrderDetailsMeasurementAppointmentTypeTxt.setText(mMeasurementTypeNameStr);
                    holder.mOrderDetailsMeasurementEmtptyTxt.setVisibility(View.VISIBLE);
                    holder.mOrderDetailsMeasurmentCreateAppointmentTxt.setVisibility(View.GONE);
                    holder.mOrderDetailsMeasurementEmtptyTxt.setText(mContext.getResources().getString(R.string.appointment_details_by_tailor));

                }
            }
        }

        if (mOrderDetailsStitchingStoreModal.getGetOrderDetailScheduleType().size()>0){
            if (mDeliveryTypeStr.equalsIgnoreCase("3")){
                holder.mOrderDetailsDeliveryAppointmentLay.setVisibility(View.VISIBLE);
                if (mOrderDetailsStitchingStoreModal.getGetOrderDetailScheduleType().get(0).getStatus().equalsIgnoreCase("Approved")||mOrderDetailsStitchingStoreModal.getGetOrderDetailScheduleType().get(0).getStatus().equalsIgnoreCase("Tailor Approved")){
                    holder.mOrderDetailsDeliveryCreateAppointmentTxt.setVisibility(View.GONE);
                    holder.mOrderDetailsDeliveryAppointmentLeftParLay.setVisibility(View.GONE);
                }
                if (mOrderDetailsStitchingStoreModal.getGetOrderDetailScheduleType().get(0).getAppointmentLeftScheduleCount() == 0){
                    holder.mOrderDetailsDeliveryCreateAppointmentTxt.setVisibility(View.GONE);
                    holder.mOrderDetailsDeliveryAppointmentLeftParLay.setVisibility(View.GONE);
                }
            }
            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                holder.mOrderDetailsDeliveryAppointmentTypeTxt.setText(mOrderDetailsStitchingStoreModal.getGetOrderDetailScheduleType().get(0).getDeliveryTypeInArabic());

            }else {
                holder.mOrderDetailsDeliveryAppointmentTypeTxt.setText(mOrderDetailsStitchingStoreModal.getGetOrderDetailScheduleType().get(0).getDeliveryTypeInEnglish());
            }

            holder.mOrderDetailsDeliveryOrderDateLay.setVisibility(View.VISIBLE);
            holder.mOrderDetailsDeliveryOrderTimeLay.setVisibility(View.VISIBLE);
            holder.mOrderDetailsDeliveryStatusLay.setVisibility(View.VISIBLE);
            holder.mOrderDetailsDeliveryEmptyTxt.setVisibility(View.GONE);
            holder.mOrderDetailsDeliveryAppointmentTypeTimeTxt.setText(mOrderDetailsStitchingStoreModal.getGetOrderDetailScheduleType().get(0).getAppointmentTime());
            holder.mOrderDetailsDeliveryAppointmentTypeDateAndTimeTxt.setText(mOrderDetailsStitchingStoreModal.getGetOrderDetailScheduleType().get(0).getOrderDt());
            holder.mOrderDetailsDeliveryAppointmentTypeStatusTxt.setText(mOrderDetailsStitchingStoreModal.getGetOrderDetailScheduleType().get(0).getStatus());
            holder.mOrderDetailsDeliveryAppointmentTypeLeftTxt.setText(String.valueOf(mOrderDetailsStitchingStoreModal.getGetOrderDetailScheduleType().get(0).getAppointmentLeftScheduleCount()));

        }else {
            if (mDeliveryTypeStr.equalsIgnoreCase("3")){
                holder.mOrderDetailsDeliveryAppointmentLay.setVisibility(View.VISIBLE);
                holder.mOrderDetailsDeliveryCreateAppointmentTxt.setVisibility(View.VISIBLE);
                holder.mOrderDetailsDeliveryAppointmentLeftParLay.setVisibility(View.GONE);
                holder.mOrderDetailsDeliveryOrderDateLay.setVisibility(View.GONE);
                holder.mOrderDetailsDeliveryOrderTimeLay.setVisibility(View.GONE);
                holder.mOrderDetailsDeliveryStatusLay.setVisibility(View.GONE);
                holder.mOrderDetailsDeliveryEmptyTxt.setVisibility(View.VISIBLE);
                holder.mOrderDetailsDeliveryEmptyTxt.setText(mContext.getResources().getString(R.string.appointment_details_by_yours));
                holder.mOrderDetailsDeliveryAppointmentTypeTxt.setText(mDeliveryTypeNameStr);
            }
        }

    }

    @Override
    public int getItemCount() {
        return mStitchingOrderDetailsList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.adap_stitching_order_detail_order_type_txt)
        TextView mStitchingOrderDetailsOrderTypeTxt;

//        @BindView(R.id.adap_stitching_order_detail_delivery_type_txt)
//        TextView mStitchingOrderDetailsDeliveryTypeTxt;

        @BindView(R.id.adap_stitching_order_detail_stitching_time_txt)
        TextView mStitchingOrderDetailsStitchingTimeTxt;

        @BindView(R.id.adap_stitching_order_detail_prod_name_txt)
        TextView mStitchingOrderDetailsProdNameTxt;

        @BindView(R.id.adap_stitching_order_detail_price_txt)
        TextView mStitchingOrderDetailsPriceTxt;

        @BindView(R.id.adap_stitching_order_detail_qty_txt)
        TextView mStitchingOrderDetailsQtyTxt;

        @BindView(R.id.adap_stitching_order_detail_total_txt)
        TextView mStitchingOrderDetailsTotalTxt;

        @BindView(R.id.adap_stitching_order_detail_material_type_txt)
        TextView mStitchingOrderDetailsMaterialTypeTxt;

        @BindView(R.id.adap_stitching_order_detail_status_txt)
        TextView mStitchingOrderDetailsStatusTxt;

        @BindView(R.id.adap_stitching_order_detail_prod_img)
        ImageView mStitchingOrderDetailsProdImg;

        @BindView(R.id.adap_stitching_order_detail_customization_txt)
        TextView mStitchingCustomizationTxt;

        @BindView(R.id.adap_stitching_order_detail_measurement_txt)
        TextView mStitchingMeasurementTxt;

        @BindView(R.id.adap_stitching_order_detail_reference_img_txt)
        TextView mStitchingReferenceImgTxt;

        @BindView(R.id.adap_stitching_order_detail_material_img_txt)
        TextView mStitchingMaterialImgTxt;

        @BindView(R.id.adap_stitching_order_detail_track_txt)
        TextView mStitchingTrackTxt;

        @BindView(R.id.order_details_rate_and_review_par_lay)
        RelativeLayout mStitchingRatingAndReviewParLay;

        @BindView(R.id.order_details_rate_and_review_view)
        View mStitchingRateAndReviewView;

        @BindView(R.id.order_details_material_appointment_lay)
        LinearLayout mOrderDetailsMaterialAppointmentLay;

        @BindView(R.id.order_details_material_order_date_lay)
        LinearLayout mOrderDetailsMaterialOrderDateLay;

        @BindView(R.id.order_details_material_order_time_lay)
        LinearLayout mOrderDetailsMaterialOrderTimeLay;

        @BindView(R.id.order_details_payment_status_lay)
        LinearLayout mOrderDetailsPaymentStatusLay;

        @BindView(R.id.order_details_material_appointment_type_txt)
        TextView mOrderDetailsMaterialAppointmentTypeTxt;

        @BindView(R.id.order_details_material_appointment_type_date_and_time_txt)
        TextView mOrderDetailsMaterialAppointmentTypeDateAndTimeTxt;

        @BindView(R.id.order_details_material_appointment_type_time_txt)
        TextView mOrderDetailsMaterialAppointmentTypeTimeTxt;

        @BindView(R.id.order_details_material_appointment_type_status_txt)
        TextView mOrderDetailsMaterialAppointmentTypeStatusTxt;

        @BindView(R.id.order_details_material_appointment_type_left_txt)
        TextView mOrderDetailsMaterialAppointmentTypeLeftTxt;

        @BindView(R.id.order_deatails_material_create_appointment_txt)
        TextView mOrderDetailsMaterialCreateAppointmentTxt;

        @BindView(R.id.order_details_material_appointment_left_par_lay)
        LinearLayout mOrderDetailsMaterialAppointmentLeftParLay;

        @BindView(R.id.order_deatails_material_empty_txt)
        TextView mOrderDetailsMaterialEmptyTxt;

        @BindView(R.id.order_details_delivery_appointment_lay)
        LinearLayout mOrderDetailsDeliveryAppointmentLay;

        @BindView(R.id.order_details_delivery_order_date_lay)
        LinearLayout mOrderDetailsDeliveryOrderDateLay;

        @BindView(R.id.order_details_delivery_order_time_lay)
        LinearLayout mOrderDetailsDeliveryOrderTimeLay;

        @BindView(R.id.order_details_delivery_status_lay)
        LinearLayout mOrderDetailsDeliveryStatusLay;

        @BindView(R.id.order_details_delivery_appointment_type_txt)
        TextView mOrderDetailsDeliveryAppointmentTypeTxt;

        @BindView(R.id.order_details_delivery_appointment_type_date_and_time_txt)
        TextView mOrderDetailsDeliveryAppointmentTypeDateAndTimeTxt;

        @BindView(R.id.order_details_delivery_appointment_type_time_txt)
        TextView mOrderDetailsDeliveryAppointmentTypeTimeTxt;

        @BindView(R.id.order_details_delivery_appointment_type_status_txt)
        TextView mOrderDetailsDeliveryAppointmentTypeStatusTxt;

        @BindView(R.id.order_details_delivery_appointment_type_left_txt)
        TextView mOrderDetailsDeliveryAppointmentTypeLeftTxt;

        @BindView(R.id.order_deatails_delivery_create_appointment_txt)
        TextView mOrderDetailsDeliveryCreateAppointmentTxt;

        @BindView(R.id.order_details_measurement_appointment_lay)
        LinearLayout mOrderDetailsMeasurementAppointmentLay;

        @BindView(R.id.order_details_measurement_appointment_type_txt)
        TextView mOrderDetailsMeasurementAppointmentTypeTxt;

        @BindView(R.id.order_details_measurement_appointment_type_date_and_time_txt)
        TextView mOrderDetailsMeasurementAppointmentTypeDateAndTimeTxt;

        @BindView(R.id.order_details_measurement_appointment_type_time_txt)
        TextView mOrderDetailsMeasurementAppointmentTypeTimeTxt;

        @BindView(R.id.order_details_measurement_appointment_type_status_txt)
        TextView mOrderDetailsMeasurementAppointmentTypeStatusTxt;

        @BindView(R.id.order_measurement_delivery_appointment_type_left_txt)
        TextView mOrderDetailsMeasurementDeliveryAppointmentTypeLeftTxt;

        @BindView(R.id.order_deatails_measurement_empty_txt)
        TextView mOrderDetailsMeasurementEmtptyTxt;

        @BindView(R.id.order_details_delivery_appointment_left_par_lay)
        LinearLayout mOrderDetailsDeliveryAppointmentLeftParLay;

        @BindView(R.id.order_deatails_delivery_empty_txt)
        TextView mOrderDetailsDeliveryEmptyTxt;

        @BindView(R.id.order_details_measurement_status_lay)
        LinearLayout mOrderDetailsMeasurementStatusLay;

        @BindView(R.id.order_deatails_measurement_create_appointment_txt)
        TextView mOrderDetailsMeasurmentCreateAppointmentTxt;

        @BindView(R.id.order_details_measurement_appointment_left_par_lay)
        LinearLayout mOrderDetailsMeasurementAppointmentLeftParLay;

        @BindView(R.id.order_details_measurement_order_date_lay)
        LinearLayout mOrderDetailsMeasurementOrderDetaLay;

        @BindView(R.id.order_details_measurement_order_time_lay)
        LinearLayout mOrderDetailsMeasurementOrderTimeLay;

        @BindView(R.id.order_details_rate_and_review_txt)
        TextView mStitchingRatingAndReviewTxt;

        @BindView(R.id.order_details_rating_bar)
        RatingBar mStitchingRatingBar;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
