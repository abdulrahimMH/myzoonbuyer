package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.SearchProductEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.StoreProductListScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreSearchAdapter extends RecyclerView.Adapter<StoreSearchAdapter.Holder> {

    ArrayList<SearchProductEntity> mList;
    Context mContext;
    HashMap<String, String> mHistoryList;
    UserDetailsEntity mUserDetailsEntityRes;

    public StoreSearchAdapter(ArrayList<SearchProductEntity> list, HashMap<String, String> historyList, Context context) {
        mList = list;
        mContext = context;
        mHistoryList = historyList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_store_search_list, viewGroup, false);
        return new StoreSearchAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        SearchProductEntity entity = mList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mAdapterStoreProdTxt.setText(entity.getProductNameInArabic());
            holder.mAdapterStoreCatogeryTxt.setText(entity.getTitleInArabic());
        }else {
            holder.mAdapterStoreProdTxt.setText(entity.getProductName());
            holder.mAdapterStoreCatogeryTxt.setText(entity.getTitle());
        }


        if (mList.get(position).isSearchChecked()) {
            holder.mAdapterStoreCatogeryTxt.setVisibility(View.GONE);
        } else{
            holder.mAdapterStoreCatogeryTxt.setVisibility(View.VISIBLE);
        }

        if (mList.get(position).isChecked()){
            holder.mAdapterStoreRecentHistoryImg.setVisibility(View.VISIBLE);
            holder.mAdapterStoreProdImg.setVisibility(View.GONE);
         holder.mAdapterStoreRecentHistoryImg.setBackgroundResource(R.drawable.store_history_img);
        }else if (mList.get(position).isSearchChecked()){
            holder.mAdapterStoreRecentHistoryImg.setVisibility(View.VISIBLE);
            holder.mAdapterStoreProdImg.setVisibility(View.GONE);
            holder.mAdapterStoreRecentHistoryImg.setBackgroundResource(R.drawable.store_search_img);
        }else if (!mList.get(position).isSearchChecked()&&!mList.get(position).isChecked()){
            holder.mAdapterStoreRecentHistoryImg.setVisibility(View.GONE);
            holder.mAdapterStoreProdImg.setVisibility(View.VISIBLE);
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/Products/"+entity.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mAdapterStoreProdImg);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mList.get(position).isSearchChecked()) {
                    mHistoryList.put(mList.get(position).getProductName(), mList.get(position).getTitle());
                    PreferenceUtil.storeSearchHistoryProducts(mContext, mHistoryList);

                }
                    AppConstants.STORE_SEARCH_TXT = mList.get(position).getProductName();
                    AppConstants.STORE_SEARCH = "STORE_SEARCH";
                    ((BaseActivity) mContext).nextScreen(StoreProductListScreen.class, true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.adapter_store_prod_txt)
        TextView mAdapterStoreProdTxt;

        @BindView(R.id.adapter_store_catogery_txt)
        TextView mAdapterStoreCatogeryTxt;

        @BindView(R.id.adapter_store_prod_img)
        ImageView mAdapterStoreProdImg;

        @BindView(R.id.adapter_store_recent_history_img)
        ImageView mAdapterStoreRecentHistoryImg;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
