package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.CustomizationAttributesEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.ui.ChooseCustomizationScreen;
import com.qoltech.mzyoonbuyer.ui.CustomizationThreeScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationThreeDialogAdapter extends RecyclerView.Adapter<CustomizationThreeDialogAdapter.Holder> {

    private Context mContext;
    private ArrayList<CustomizationAttributesEntity> mCustomizationList;

    private UserDetailsEntity mUserDetailsEntityRes;
    boolean mClick = true;

    public CustomizationThreeDialogAdapter(Context activity, ArrayList<CustomizationAttributesEntity> getCustomizationList) {
        mContext = activity;
        mCustomizationList = getCustomizationList;
    }

    @NonNull
    @Override
    public CustomizationThreeDialogAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_customization_three_list_view, parent, false);
        return new CustomizationThreeDialogAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomizationThreeDialogAdapter.Holder holder, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final CustomizationAttributesEntity getCustomizationEntity = mCustomizationList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mCustomizationThreeTxtViewTxt.setText(getCustomizationEntity.getAttributeNameinArabic());

        }else {
            holder.mCustomizationThreeTxtViewTxt.setText(getCustomizationEntity.getAttributeNameInEnglish());

        }

        mClick = true;

        for (int i=0; i<AppConstants.CUSTOMIZATION_CLICKED_ARRAY.size(); i++){
            for (int j=0; j<mCustomizationList.size(); j++){
                if (AppConstants.CUSTOMIZATION_CLICKED_ARRAY.get(i).equalsIgnoreCase(String.valueOf(mCustomizationList.get(j).getId()))){
                    mCustomizationList.get(j).setChecked(true);
                }
            }

        }

        for (int i=0; i<AppConstants.CUSTOMIZATION_CLICK_ARRAY.size(); i++){
            for (int j=0; j<mCustomizationList.size(); j++){
                if (AppConstants.CUSTOMIZATION_CLICK_ARRAY.get(i).getCustomizationName().equalsIgnoreCase(String.valueOf(mCustomizationList.get(j).getId()))){
                    mCustomizationList.get(j).setSelectedImages(AppConstants.CUSTOMIZATION_CLICK_ARRAY.get(i).getImages());
                }
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClick){
                    mClick = false;
                    AppConstants.CUSTOMIZATION_DRESS_TYPE_ID = String.valueOf(getCustomizationEntity.getId());
                    AppConstants.CUSTOMIZATION_NAME = getCustomizationEntity.getAttributeNameInEnglish();

                    ((CustomizationThreeScreen)mContext).nextScreen(ChooseCustomizationScreen.class,true);
                }
            }
        });
        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"images/Customazation3/"+mCustomizationList.get(position).getSelectedImages())
                    .apply(new RequestOptions().placeholder(R.drawable.gender_background_img).error(R.drawable.gender_background_img))
                    .into(holder.mCustimzationThreegImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

//        holder.mCustomizationThreeListLay.setVisibility(mCustomizationList.get(position).getChecked() ? View.VISIBLE: View.GONE);
    }

    @Override
    public int getItemCount() {
        return mCustomizationList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.customization_three_list_txt)
        TextView mCustomizationThreeTxtViewTxt;

        @BindView(R.id.customization_three_list_img)
        ImageView mCustimzationThreegImg;

        @BindView(R.id.customization_three_list_lay)
        CardView mCustomizationThreeListLay;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
