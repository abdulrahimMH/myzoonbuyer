package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.OrderRequestListEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.QuotationListScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestListAdapter extends RecyclerView.Adapter<RequestListAdapter.Holder> {

    private Context mContext;
    private  ArrayList<OrderRequestListEntity> mOrderRequestListEntity;
    private UserDetailsEntity mUserDetailsEntityRes;
    public RequestListAdapter(Context activity, ArrayList<OrderRequestListEntity> orderRequestListEntity) {
        mContext = activity;
        mOrderRequestListEntity = orderRequestListEntity;
    }

    @NonNull
    @Override
    public RequestListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_request_list, parent, false);
        return new RequestListAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RequestListAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        OrderRequestListEntity orderRequestListEntity = mOrderRequestListEntity.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mRequestListProdNameTxt.setText(orderRequestListEntity.getProduct_NameInArabic());

        }else {
            holder.mRequestListProdNameTxt.setText(orderRequestListEntity.getProduct_NameInEnglish());

        }

        holder.mRequestListOrderIdTxt.setText(String.valueOf(orderRequestListEntity.getOrderId()));
        holder.mRequestListDateTxt.setText(orderRequestListEntity.getRequestDt().replace("T00:00:00",""));
        holder.mRequestListNoTailorTxt.setText(String.valueOf(orderRequestListEntity.getNoOfTailors()));

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/DressSubType/"+orderRequestListEntity.getProductImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mRequestDateImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.CHECK_BOOK_APPOINTMENT = "RequestList";
                AppConstants.DIRECT_ORDER = "";
                AppConstants.APPROVE_REJECT_BTN = "true";
                AppConstants.ORDER_ID = String.valueOf(mOrderRequestListEntity.get(position).getOrderId());
                AppConstants.REQUEST_LIST_ID = String.valueOf(mOrderRequestListEntity.get(position).getOrderId());
                ((BaseActivity)mContext).nextScreen(QuotationListScreen.class,true);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mOrderRequestListEntity.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.request_list_img)
        de.hdodenhof.circleimageview.CircleImageView mRequestDateImg;

        @BindView(R.id.request_list_date_txt)
        TextView mRequestListDateTxt;

        @BindView(R.id.request_list_prod_name_txt)
        TextView mRequestListProdNameTxt;

        @BindView(R.id.request_list_no_tailor)
        TextView mRequestListNoTailorTxt;

        @BindView(R.id.request_list_order_id_txt)
        TextView mRequestListOrderIdTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
