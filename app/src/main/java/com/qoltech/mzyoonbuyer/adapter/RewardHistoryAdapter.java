package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.RewardHistoryEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RewardHistoryAdapter  extends RecyclerView.Adapter<RewardHistoryAdapter.Holder> {

    private Context mContext;
    private ArrayList<RewardHistoryEntity> mRewardHistoryList;
    private UserDetailsEntity mUserDetailsEntityRes;

    public RewardHistoryAdapter(Context activity, ArrayList<RewardHistoryEntity> rewardHistoryList) {
        mContext = activity;
        mRewardHistoryList = rewardHistoryList;
    }

    @NonNull
    @Override
    public RewardHistoryAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_reward_history_list, parent, false);
        return new RewardHistoryAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RewardHistoryAdapter.Holder holder, final int position) {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        RewardHistoryEntity mList = mRewardHistoryList.get(position);

        for (int i=0; i<mRewardHistoryList.size(); i++){
//            String[] transactionTypeStr = mRewardHistoryList.get(i).getTransactionType().split("-");
//
//            if (transactionTypeStr[0].equalsIgnoreCase("Reedemed ")){
//                mRewardHistoryList.get(i).setChecked(true);
//            }else {
//                mRewardHistoryList.get(i).setChecked(false);
//
//            }

            if (mRewardHistoryList.get(i).getPointType() == 2){
                mRewardHistoryList.get(i).setChecked(true);
            }else {
                mRewardHistoryList.get(i).setChecked(false);

            }
        }


        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mAdapterRewardTxt.setText(mList.getTransactiontypeInArabic());
            holder.mAdapterRewardMessageTxt.setText(mList.getRewardPointTypeInArabic() +"\n"+mList.getTransactionDate());
        }else {
            holder.mAdapterRewardTxt.setText(mList.getTransactionType());
            holder.mAdapterRewardMessageTxt.setText(mList.getRewardPointType() +"\n"+mList.getTransactionDate());
        }

        holder.mAdapterRewardMinusPlusImg.setBackground(mList.isChecked() ? mContext.getResources().getDrawable(R.drawable.reward_minus_red_img) : mContext.getResources().getDrawable(R.drawable.reward_plus_blue_img));
        holder.mAdapterRewardDownUpImg.setBackground(mList.isChecked() ? mContext.getResources().getDrawable(R.drawable.reward_down_red_img) : mContext.getResources().getDrawable(R.drawable.reward_up_green_img));


        holder.mAdapterRewardPointTxt.setText(String.valueOf(mList.getPoints()));

    }

    @Override
    public int getItemCount() {
        return mRewardHistoryList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_reward_minus_plus_img)
        ImageView mAdapterRewardMinusPlusImg;

        @BindView(R.id.adapter_reward_txt)
        TextView mAdapterRewardTxt;

        @BindView(R.id.adapter_reward_point_txt)
        TextView mAdapterRewardPointTxt;

        @BindView(R.id.adapter_reward_message_txt)
        TextView mAdapterRewardMessageTxt;

        @BindView(R.id.adapter_reward_down_up_img)
        ImageView mAdapterRewardDownUpImg;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

