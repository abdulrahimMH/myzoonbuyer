package com.qoltech.mzyoonbuyer.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetLanguageEntity;
import com.qoltech.mzyoonbuyer.ui.LoginScreen;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GetLanguageAdapter extends RecyclerView.Adapter<GetLanguageAdapter.Holder> {

    private Context mContext;
    private Dialog mDialog;
    private ArrayList<GetLanguageEntity> mLanguageCountryList;

    public GetLanguageAdapter(Context activity, ArrayList<GetLanguageEntity> languageCountryList , Dialog dialog) {
        mContext = activity;
        mLanguageCountryList = languageCountryList;
        mDialog = dialog;
    }

    @NonNull
    @Override
    public GetLanguageAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_country_code_list, parent, false);
        return new GetLanguageAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final GetLanguageAdapter.Holder holder, final int position) {

        holder.mGetCountryTxtViewTxt.setText(mLanguageCountryList.get(position).getLanguage());

        holder.mCountryFlagImg.setVisibility(View.GONE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    ((LoginScreen)mContext).mChooseLanguageTxt.setText(mLanguageCountryList.get(position).getLanguage());
                ((LoginScreen)mContext).getLanguage();
                mDialog.dismiss();
            }
        });

        if(position %2 == 1) {
            holder.GetCountryView.setVisibility(View.GONE);

            }
        else {
            holder.GetCountryView.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public int getItemCount() {
        return mLanguageCountryList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.get_country_recycler_view_txt)
        TextView mGetCountryTxtViewTxt;

        @BindView(R.id.get_country_flag_img)
        ImageView mCountryFlagImg;


        @BindView(R.id.get_country_view)
        View GetCountryView;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


