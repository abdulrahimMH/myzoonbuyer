package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.TailorListEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderApprovalTailorAdapter extends RecyclerView.Adapter<OrderApprovalTailorAdapter.Holder> {

    private Context mContext;
    private ArrayList<TailorListEntity> mOrderApprovalTailorList;

    private UserDetailsEntity mUserDetailsEntityRes;
    public OrderApprovalTailorAdapter(Context activity, ArrayList<TailorListEntity> OrderApprovalTailorList) {
        mContext = activity;
        mOrderApprovalTailorList = OrderApprovalTailorList;
    }

    @NonNull
    @Override
    public OrderApprovalTailorAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_order_approval_fetch_list, parent, false);
        return new OrderApprovalTailorAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderApprovalTailorAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final TailorListEntity orderApprovalTailorEntity = mOrderApprovalTailorList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mOrderApprovalTxt.setText(orderApprovalTailorEntity.getShopNameInArabic());

        }else {
            holder.mOrderApprovalTxt.setText(orderApprovalTailorEntity.getShopNameInEnglish());

        }
    }

    @Override
    public int getItemCount() {
        return mOrderApprovalTailorList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_order_approval_img)
        ImageView mAdapterOrderApprovalImg;

        @BindView(R.id.adapter_order_approval_txt)
        TextView mOrderApprovalTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
