package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.CustomizationColorsEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.ui.CustomizationOneScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationTwoColorAdapter extends RecyclerView.Adapter<CustomizationTwoColorAdapter.Holder> {

    private Context mContext;
    private ArrayList<CustomizationColorsEntity> mCustomizeColorList;
    int mTotalInt = 1;

    private UserDetailsEntity mUserDetailsEntityRes;
    public CustomizationTwoColorAdapter(Context activity, ArrayList<CustomizationColorsEntity> customizeColorList) {
        mContext = activity;
        mCustomizeColorList = customizeColorList;
    }

    @NonNull
    @Override
    public CustomizationTwoColorAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_color_grid_view, parent, false);
        return new CustomizationTwoColorAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomizationTwoColorAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final CustomizationColorsEntity colorEntity = mCustomizeColorList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mGridViewTxt.setText(colorEntity.getColorInArabic());

        }else {
            holder.mGridViewTxt.setText(colorEntity.getColorInEnglish());

        }
        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/Color/"+colorEntity.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CustomizationOneScreen)mContext).getSubColorApiCall(String.valueOf(mCustomizeColorList.get(position).getId()));
            }
        });

        holder.mGridCustomizeColorTickImg.setVisibility(mCustomizeColorList.get(position).getChecked() ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return mCustomizeColorList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_custimize_view_lay)
        LinearLayout mGridViewLay;

        @BindView(R.id.grid_custimize_view_img_lay)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_custimize_view_txt)
        TextView mGridViewTxt;

        @BindView(R.id.grid_customize_tick_img)
        ImageView mGridCustomizeColorTickImg;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

