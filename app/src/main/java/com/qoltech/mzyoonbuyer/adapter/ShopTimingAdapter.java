package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.ShopTimingEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShopTimingAdapter extends RecyclerView.Adapter<ShopTimingAdapter.Holder> {

    private Context mContext;
    private ArrayList<ShopTimingEntity> shopTimingList;

    private UserDetailsEntity mUserDetailsEntityRes;
    public ShopTimingAdapter(Context activity, ArrayList<ShopTimingEntity> shopTimingEntity) {
        mContext = activity;
        shopTimingList = shopTimingEntity;
    }

    @NonNull
    @Override
    public ShopTimingAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_shop_details_timeing, parent, false);
        return new ShopTimingAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ShopTimingAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final ShopTimingEntity shopTimingEntity = shopTimingList.get(position);

        holder.mShopDayTxt.setText(shopTimingEntity.getShopDay());
        holder.mShopTimeingTxt.setText(shopTimingEntity.getShopTime());

        holder.mShopDetailsImg.setBackgroundResource(shopTimingEntity.isChecked() ? R.drawable.tick_selected_img : R.drawable.tick_unselected_img);
    }

    @Override
    public int getItemCount() {
        return shopTimingList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapt_shop_day_txt)
        TextView mShopDayTxt;

        @BindView(R.id.adap_shop_details_timing_img)
        ImageView mShopDetailsImg;

        @BindView(R.id.adapt_shop_timeing_txt)
        TextView mShopTimeingTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


