package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.StoreCatogeryEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.StoreProductListScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreSubCatogeryAdapter extends RecyclerView.Adapter<StoreSubCatogeryAdapter.Holder> {


    Context mContext;

    ArrayList<StoreCatogeryEntity> mFilterList;

    UserDetailsEntity mUserDetailsEntityRes;

    public StoreSubCatogeryAdapter(ArrayList<StoreCatogeryEntity> list, Context context) {
        mFilterList = list;
        mContext = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_sub_catogery_list,viewGroup,false);

        return new StoreSubCatogeryAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mAapterStoreSubCatogeryTxt.setText(mFilterList.get(position).getDisplayIdInArabic());

        }else {
            holder.mAapterStoreSubCatogeryTxt.setText(mFilterList.get(position).getDisplayId());

        }


        for (int i=0; i<mFilterList.size(); i++){
            mFilterList.get(i).setPosition(i+1);
        }

        if (mFilterList.size()%3 == 0){
            mFilterList.get(mFilterList.size()-1).setCheckBottomBool(true);
            mFilterList.get(mFilterList.size()-2).setCheckBottomBool(true);
            mFilterList.get(mFilterList.size()-3).setCheckBottomBool(true);

        }else if (mFilterList.size()%3 == 2){
            mFilterList.get(mFilterList.size()-1).setCheckBottomBool(true);
            mFilterList.get(mFilterList.size()-2).setCheckBottomBool(true);

        }else if (mFilterList.size()%3 == 1){
            mFilterList.get(mFilterList.size()-1).setCheckBottomBool(true);

        }

        for (int i =0 ; i<mFilterList.size(); i++){
            if (mFilterList.get(i).getPosition()%3 == 0){
                mFilterList.get(i).setCheckRightBool(true);
            }
        }

        Glide.with(mContext)
                .load(AppConstants.IMAGE_BASE_URL + "images/CategoriesIconImage/"+mFilterList.get(position).getImage())
                .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                .into(holder.mAdapterStoreSubCatogeryImg);

        holder.mStoreSubCatogeryViewBottom.setVisibility(mFilterList.get(position).isCheckBottomBool() ? View.INVISIBLE : View.VISIBLE);
        holder.mStoreSubCatogeryViewRightSide.setVisibility(mFilterList.get(position).isCheckRightBool() ? View.INVISIBLE : View.VISIBLE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.STORE_BANNER_ID = "6";
                AppConstants.STORE_SEARCH = "";
                AppConstants.STORE_TYPE_ID = String.valueOf(mFilterList.get(position).getId());
                ((BaseActivity)mContext).nextScreen(StoreProductListScreen.class,true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFilterList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.adapter_store_sub_catoger_txt)
        TextView mAapterStoreSubCatogeryTxt;

        @BindView(R.id.adapter_store_sub_catogery_img)
        ImageView mAdapterStoreSubCatogeryImg;

        @BindView(R.id.store_sub_catogery_view_right_side)
        RelativeLayout mStoreSubCatogeryViewRightSide;

        @BindView(R.id.store_sub_catogery_view_bottom)
        RelativeLayout mStoreSubCatogeryViewBottom;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
