package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetMaterialSelectionEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.ViewDetailsScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationTwoPatternAdapter extends RecyclerView.Adapter<CustomizationTwoPatternAdapter.Holder> {

    private Context mContext;
    private ArrayList<GetMaterialSelectionEntity> mCustomizePatternList;

    private UserDetailsEntity mUserDetailsEntityRes;
    public CustomizationTwoPatternAdapter(Context activity, ArrayList<GetMaterialSelectionEntity> customizePatternList) {
        mContext = activity;
        mCustomizePatternList = customizePatternList;
    }

    @NonNull
    @Override
    public CustomizationTwoPatternAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_two_pattern_list, parent, false);
        return new CustomizationTwoPatternAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomizationTwoPatternAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final GetMaterialSelectionEntity PatternEntity = mCustomizePatternList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                holder.mGridViewTxt.setText(PatternEntity.getSoftandThicknessInArabic() + "\n"+ String.valueOf(PatternEntity.getMaterialCharges()));

            }else {
                holder.mGridViewTxt.setText(PatternEntity.getSoftandThicknessInArabic());

            }
        }else {
            if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                holder.mGridViewTxt.setText(PatternEntity.getSoftandThicknessInEnglish() + "\n"+ String.valueOf(PatternEntity.getMaterialCharges()));

            }else {
                holder.mGridViewTxt.setText(PatternEntity.getSoftandThicknessInEnglish());

            }
        }

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/Pattern/"+PatternEntity.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < mCustomizePatternList.size(); i++) {
                    mCustomizePatternList.get(i).setChecked(false);
                }
                mCustomizePatternList.get(position).setChecked(true);
                AppConstants.PATTERN_ID = String.valueOf(mCustomizePatternList.get(position).getId());
                AppConstants.PATTERN_NAME = mCustomizePatternList.get(position).getPatternInEnglish();

                    AppConstants.SEASONAL_NAME = AppConstants.SEASONAL_NAME.equalsIgnoreCase("") ? "None" : AppConstants.SEASONAL_NAME;
                    AppConstants.PLACE_OF_INDUSTRY_NAME = AppConstants.PLACE_OF_INDUSTRY_NAME.equalsIgnoreCase("") ? "None" : AppConstants.PLACE_OF_INDUSTRY_NAME;
                    AppConstants.BRANDS_NAME = AppConstants.BRANDS_NAME.equalsIgnoreCase("") ? "None" : AppConstants.BRANDS_NAME;
                    AppConstants.MATERIAL_TYPE_NAME = AppConstants.MATERIAL_TYPE_NAME.equalsIgnoreCase("") ? "None" : AppConstants.MATERIAL_TYPE_NAME;
                    AppConstants.COLOUR_NAME = AppConstants.COLOUR_NAME.equalsIgnoreCase("") ? "None" : AppConstants.COLOUR_NAME;

                   ((BaseActivity)mContext).nextScreen(ViewDetailsScreen.class,true);

                notifyDataSetChanged();
            }
        });

        holder.mChooseCustomizationZoomImg.setVisibility(View.GONE);
        holder.mGridCustimizeImg.setVisibility(PatternEntity.isChecked() ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return mCustomizePatternList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.choose_customization_list_img)
        ImageView mGridViewImgLay;

        @BindView(R.id.choose_customization_list_txt)
        TextView mGridViewTxt;

        @BindView(R.id.choose_customization_tick_img)
        ImageView mGridCustimizeImg;

        @BindView(R.id.choose_customization_zoom_img)
        ImageView mChooseCustomizationZoomImg;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
