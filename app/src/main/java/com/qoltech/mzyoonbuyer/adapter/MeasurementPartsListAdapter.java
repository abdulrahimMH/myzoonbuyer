package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.MeasurementListGetMeasurementPartsEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeasurementPartsListAdapter extends RecyclerView.Adapter<MeasurementPartsListAdapter.Holder>  {

    private Context mContext;
    private ArrayList<MeasurementListGetMeasurementPartsEntity> mMeasurementPartsList;

    private UserDetailsEntity mUserDetailsEntityRes;

    public MeasurementPartsListAdapter(Context activity, ArrayList<MeasurementListGetMeasurementPartsEntity> measurementPartsList) {
        mContext = activity;
        mMeasurementPartsList = measurementPartsList;
    }

    @NonNull
    @Override
    public MeasurementPartsListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_measurement_grid_list, parent, false);
        return new MeasurementPartsListAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MeasurementPartsListAdapter.Holder holder, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final MeasurementListGetMeasurementPartsEntity measurementList = mMeasurementPartsList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mGridViewTxt.setText(measurementList.getMeasurementPartsInArabic());
        }else {
            holder.mGridViewTxt.setText(measurementList.getMeasurementParts());
        }

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"images/Measurement2/"+measurementList.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return mMeasurementPartsList.size();
    }


    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_view_lay)
        CardView mGridViewLay;

        @BindView(R.id.grid_view_img_lay)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_view_txt)
        TextView mGridViewTxt;



        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}

