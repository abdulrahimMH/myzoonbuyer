package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.OrderApprovalPriceChargesEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderApprovalPriceAdapter extends RecyclerView.Adapter<OrderApprovalPriceAdapter.Holder> {

    private Context mContext;
    ArrayList<OrderApprovalPriceChargesEntity> mOrderApprovalList;

    public OrderApprovalPriceAdapter(Context activity, ArrayList<OrderApprovalPriceChargesEntity> OrderApprovalList) {
        mContext = activity;
        mOrderApprovalList = OrderApprovalList;
    }

    @NonNull
    @Override
    public OrderApprovalPriceAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_order_approval_list, parent, false);
        return new OrderApprovalPriceAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderApprovalPriceAdapter.Holder holder, final int position) {
       final OrderApprovalPriceChargesEntity orderApprovalEntity = mOrderApprovalList.get(position);


        holder.mOrderApprovalChargeNameTxt.setText(mOrderApprovalList.get(position).getDescInEnglish());
        holder.mOrderApprovalChargeTxt.setText(String.valueOf(mOrderApprovalList.get(position).getAmount())+ " AED");


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0; i<mOrderApprovalList.size(); i++){
                    if (mOrderApprovalList.get(i).getDescInEnglish().equalsIgnoreCase("Total")){
               mOrderApprovalList.remove(i);
               notifyDataSetChanged();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mOrderApprovalList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_order_approval_charges_name)
        TextView mOrderApprovalChargeNameTxt;

        @BindView(R.id.adapter_order_price_details_measurement_price)
        TextView mOrderApprovalChargeTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}

