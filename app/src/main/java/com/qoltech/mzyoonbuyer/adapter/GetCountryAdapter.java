package com.qoltech.mzyoonbuyer.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetCountryEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.ui.AddAddressScreen;
import com.qoltech.mzyoonbuyer.ui.LoginScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GetCountryAdapter extends RecyclerView.Adapter<GetCountryAdapter.Holder> {

    private Context mContext;
    private ArrayList<GetCountryEntity> mCountryList;
    private Dialog mDialog;

    private UserDetailsEntity mUserDetailsEntityRes;
    public GetCountryAdapter(Context activity, ArrayList<GetCountryEntity> getCountryList, Dialog dialog) {
        mContext = activity;
        mCountryList = getCountryList;
        mDialog = dialog;
    }

    @NonNull
    @Override
    public GetCountryAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_country_code_list, parent, false);
        return new GetCountryAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final GetCountryAdapter.Holder holder, int position) {
        final GetCountryEntity getCountryEntity = mCountryList.get(position);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mGetCountryTxtViewTxt.setText(getCountryEntity.getCountryName());

        }else {
            holder.mGetCountryTxtViewTxt.setText(getCountryEntity.getCountryName());
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AppConstants.CountryCode.equalsIgnoreCase("ADD_ADDRESS")){
                    ((AddAddressScreen)mContext).mAddAddressCountryCodeTxt.setText(getCountryEntity.getPhoneCode());
                    try {
                        Glide.with(mContext)
                                .load(AppConstants.IMAGE_BASE_URL+"images/flags/"+getCountryEntity.getFlag())
                                .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                                .into(((AddAddressScreen)mContext).mAddAddressFlagImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }

                }else {
                    try {
                        Glide.with(mContext)
                                .load(AppConstants.IMAGE_BASE_URL+"images/flags/"+getCountryEntity.getFlag())
                                .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                                .apply(RequestOptions.skipMemoryCacheOf(true))
                                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                                .into(((LoginScreen)mContext).mFlagImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }

                    ((LoginScreen)mContext).mCountryCodeTxt.setText(getCountryEntity.getPhoneCode());

                }
                        mDialog.dismiss();

        }
        });

                try {
                    Glide.with(mContext)
                            .load(AppConstants.IMAGE_BASE_URL+"images/flags/"+getCountryEntity.getFlag())
                            .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                            .apply(RequestOptions.skipMemoryCacheOf(true))
                            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                            .into(holder.mCountryFlagImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
    }

    @Override
    public int getItemCount() {
        return mCountryList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.get_country_recycler_view_txt)
        TextView mGetCountryTxtViewTxt;

        @BindView(R.id.get_country_flag_img)
        ImageView mCountryFlagImg;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

