package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GenderEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.DressTypeScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GenderAdapter extends RecyclerView.Adapter<GenderAdapter.Holder> {

    private Context mContext;
    private ArrayList<GenderEntity> mGenderList;

    private UserDetailsEntity mUserDetailsEntityRes;

    public GenderAdapter(Context activity, ArrayList<GenderEntity> genderList) {
        mContext = activity;
        mGenderList = genderList;
    }

    @NonNull
    @Override
    public GenderAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_gender_list, parent, false);
        return new GenderAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final GenderAdapter.Holder holder, int position) {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final GenderEntity genderEntity = mGenderList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){

            holder.mGridLeftViewTxt.setText(genderEntity.getGenderInArabic());
            ViewCompat.setLayoutDirection(holder.itemView.findViewById(R.id.gender_list_adapter), ViewCompat.LAYOUT_DIRECTION_LTR);

        }else {
            holder.mGridLeftViewTxt.setText(genderEntity.getGender());
            ViewCompat.setLayoutDirection(holder.itemView.findViewById(R.id.gender_list_adapter), ViewCompat.LAYOUT_DIRECTION_LTR);


        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.GENDER_ID =String.valueOf(genderEntity.getId());
                AppConstants.GENDER_NAME = genderEntity.getGender();
                AppConstants.OLD_FILTER_GENDER = genderEntity.getGender();
                AppConstants.GENDER_NAME_ARABIC = genderEntity.getGenderInArabic();
                AppConstants.NEW_FILTER_GENDER = "";
                AppConstants.OLD_FILTER_OCCASION = "NONE";
                AppConstants.NEW_FILTER_OCCASION = "";
                AppConstants.OLD_FILTER_REGION = "NONE";
                AppConstants.NEW_FILTER_REGION = "";
                AppConstants.OCCASTION_LIST_BOOL = "OLD";
                AppConstants.REGION_LIST_BOOL = "OLD";
                AppConstants.APPROVED_TAILOR_ID = "0";
                ((BaseActivity) mContext).nextScreen(DressTypeScreen.class, true);
            }
        });

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"images/"+genderEntity.getImageURL())
                    .apply(new RequestOptions().placeholder(R.drawable.gender_background_img).error(R.drawable.gender_background_img))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }
        holder.mGridLeftViewTxt.setVisibility(View.VISIBLE);

    }

    @Override
    public int getItemCount() {
        return mGenderList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_gender_img)
        ImageView mGridViewImgLay;

        @BindView(R.id.adapter_left_gender_txt)
        TextView mGridLeftViewTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

