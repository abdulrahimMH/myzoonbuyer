package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.SubColorEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationTwoSubColorAdapter extends RecyclerView.Adapter<CustomizationTwoSubColorAdapter.Holder> {

    private Context mContext;
    private ArrayList<SubColorEntity> mSubColorList;

    private UserDetailsEntity mUserDetailsEntityRes;
    public CustomizationTwoSubColorAdapter(Context activity, ArrayList<SubColorEntity> subColorEntities) {
        mContext = activity;
        mSubColorList = subColorEntities;
    }

    @NonNull
    @Override
    public CustomizationTwoSubColorAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_sub_color_grid_view, parent, false);
        return new CustomizationTwoSubColorAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomizationTwoSubColorAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final SubColorEntity subColorEntity = mSubColorList.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSubColorList.get(position).isChecked()){
                    mSubColorList.get(position).setChecked(false);
                    AppConstants.CUST_COLOR_ID.remove(String.valueOf(mSubColorList.get(position).getId()));
                }else {
                    AppConstants.CUST_COLOR_ID.put(String.valueOf(mSubColorList.get(position).getId()),String.valueOf(mSubColorList.get(position).getColorId()));
                    mSubColorList.get(position).setChecked(true);
                }

//                AppConstants.COLOR_ID = "";
//                AppConstants.CUST_COLOR_ID = new HashMap<>();
//                    for (int i=0; i<mSubColorList.size(); i++){
//                        if (mSubColorList.get(i).isChecked()){
//                            AppConstants.COLOR_ID = AppConstants.COLOR_ID.equalsIgnoreCase("") ? mSubColorList.get(i).getId()+"" : AppConstants.COLOR_ID + ","+ mSubColorList.get(i).getId();
//                        }
//                    }
//                if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        ((CustomizationOneScreen)mContext).getNewFlowCustomizationNewApiCall();
//
//                    }else {
//                        ((CustomizationOneScreen)mContext).getNewCustomizationApiCall();
//
//                    }
                    notifyDataSetChanged();
            }
        });

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/SubColor/"+subColorEntity.getSubColor())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(holder.mSubClrGridImgView);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }
        holder.mGridCustomizeTickImg.setVisibility(mSubColorList.get(position).isChecked() ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return mSubColorList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {


        @BindView(R.id.grid_custimize_view_lay)
        LinearLayout mGridViewLay;

        @BindView(R.id.sub_clr_grid_custimize_view_img_lay)
        ImageView mSubClrGridImgView;

        @BindView(R.id.grid_customize_tick_img)
        ImageView mGridCustomizeTickImg;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


