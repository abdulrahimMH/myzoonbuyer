package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.StoreAllBrandsEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.StoreProductListScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreAllBrandsAdapter extends RecyclerView.Adapter<StoreAllBrandsAdapter.Holder> {

    Context mContext;

    ArrayList<StoreAllBrandsEntity> mList;
    UserDetailsEntity mUserDetailsEntityRes;

    public StoreAllBrandsAdapter(ArrayList<StoreAllBrandsEntity> list, Context context) {
        mList = list;
        mContext = context;
    }


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.store_dashboard_sub_catogery_list,viewGroup,false);

        return new StoreAllBrandsAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        Glide.with(mContext)
                .load(AppConstants.IMAGE_BASE_URL+"Images/Brands/"+mList.get(i).getImage())
                .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                .into(holder.mStoreDashboardSubCatogeryImg);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mStoreDashboardSubCatogeryTxt.setText(mList.get(i).getBrandInArabic());

        }else {
            holder.mStoreDashboardSubCatogeryTxt.setText(mList.get(i).getBrandInEnglish());

        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.STORE_BANNER_ID = "9";
                AppConstants.STORE_SEARCH = "";
                AppConstants.STORE_TYPE_ID = String.valueOf(mList.get(i).getId());
                ((BaseActivity)mContext).nextScreen(StoreProductListScreen.class,true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.store_dashboard_sub_catoger_txt)
        TextView mStoreDashboardSubCatogeryTxt;

        @BindView(R.id.store_dashboard_sub_catoger_img)
        ImageView mStoreDashboardSubCatogeryImg;


        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
