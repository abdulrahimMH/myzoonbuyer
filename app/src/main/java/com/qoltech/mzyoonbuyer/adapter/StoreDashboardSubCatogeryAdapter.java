package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.StoreSubCatogeryEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseActivity;
import com.qoltech.mzyoonbuyer.ui.StoreProductListScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreDashboardSubCatogeryAdapter extends RecyclerView.Adapter<StoreDashboardSubCatogeryAdapter.Holder> {

    ArrayList<StoreSubCatogeryEntity> mList;
    Context mContext;
    UserDetailsEntity mUserDetailsEntityRes;

    public StoreDashboardSubCatogeryAdapter(ArrayList<StoreSubCatogeryEntity> list, Context context) {
        mList = list;
        mContext = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.store_dashboard_sub_catogery_list,viewGroup,false);
        return new StoreDashboardSubCatogeryAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        Glide.with(mContext)
                .load(AppConstants.IMAGE_BASE_URL+"Images/CategoriesBannerImage/"+mList.get(i).getCategeoriesBannerImage())
                .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                .into(holder.mStoreDashboardSubCatogeryImg);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mStoreDashboardSubCatogeryTxt.setText(mList.get(i).getTitleInArabic());

        }else {
            holder.mStoreDashboardSubCatogeryTxt.setText(mList.get(i).getTitle());

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.STORE_BANNER_ID = "6";
                AppConstants.STORE_SEARCH = "";
                AppConstants.STORE_TYPE_ID = String.valueOf(mList.get(i).getId());
                ((BaseActivity)mContext).nextScreen(StoreProductListScreen.class,true);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.store_dashboard_sub_catoger_txt)
        TextView mStoreDashboardSubCatogeryTxt;

        @BindView(R.id.store_dashboard_sub_catoger_img)
        ImageView mStoreDashboardSubCatogeryImg;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
