package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.StoreCartEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.fragment.CartFragment;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreCartAdapter extends RecyclerView.Adapter<StoreCartAdapter.Holder> {

    UserDetailsEntity mUserDetailsEntityRes;
    ArrayList<StoreCartEntity> mStoreCartList ;
    ArrayList<StoreCartEntity> mFilterStoreCartList ;
    Context mContext;
    ArrayList<String> mSellerId;
    ArrayList<String> mTailorName;
    ArrayList<String> mTailorNameInArabic;
    StoreCartInnerAdapter mAdapter;
    CartFragment mCartFragment;

    public StoreCartAdapter(ArrayList<String> sellerId,ArrayList<String> tailorName,ArrayList<String> tailorNameInArabic, ArrayList<StoreCartEntity> storeCartList, Context context,CartFragment cartFragment) {

        mSellerId = sellerId;
        mTailorName = tailorName;
        mTailorNameInArabic = tailorNameInArabic;
        mStoreCartList = storeCartList;
        mContext = context;
        mCartFragment = cartFragment;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_cart_list,viewGroup,false);
        return new StoreCartAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        mFilterStoreCartList = new ArrayList<>();

        for (int i=0 ; i<mStoreCartList.size() ; i++){
                if (mStoreCartList.get(i).getSellerId() == Integer.parseInt(mSellerId.get(position))){
                        mFilterStoreCartList.add(mStoreCartList.get(i));
            }
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mAdapterCartTailorNameTxt.setText(mContext.getResources().getString(R.string.seller) + " : " +mTailorNameInArabic.get(position));

        }else {
            holder.mAdapterCartTailorNameTxt.setText(mContext.getResources().getString(R.string.seller) + " : " +mTailorName.get(position));

        }

        mAdapter = new StoreCartInnerAdapter(mFilterStoreCartList,mContext,mCartFragment,"Cart");
        holder.mAapterCartRecView.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false));
        holder.mAapterCartRecView.setAdapter(mAdapter);

    }

    @Override
    public int getItemCount() {
        return mTailorName.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.adapter_cart_tailor_name_txt)
        TextView mAdapterCartTailorNameTxt;

        @BindView(R.id.adapter_cart_rec_view)
        RecyclerView mAapterCartRecView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
