package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.StichingOrderDetailsEntity;
import com.qoltech.mzyoonbuyer.modal.OrderDetailsStitchingStoreModal;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StitchingStoreOrderDetailsAdapter extends RecyclerView.Adapter<StitchingStoreOrderDetailsAdapter.Holder> {

    ArrayList<StichingOrderDetailsEntity> mStitchingOrderDetailsList;
    Context mContext;
    ArrayList<String> mSellerId ;
    ArrayList<String> mSellerName;
    SttichingOrderDetailsAdapter mStitchingOrderDetailsAdapter;
    StoreOrderDetailsAdapter mStoreOrderDetailsAdapter;
    OrderDetailsStitchingStoreModal mOrderDetailsStitchingStoreModal;

    public StitchingStoreOrderDetailsAdapter(ArrayList<StichingOrderDetailsEntity> stitchingOrderDetailsList, Context context, ArrayList<String> sellerId,ArrayList<String> sellerName,OrderDetailsStitchingStoreModal orderDetailsStitchingStoreModal) {
        mStitchingOrderDetailsList = stitchingOrderDetailsList;
        mContext = context;
        mSellerId = sellerId;
        mSellerName = sellerName;
        mOrderDetailsStitchingStoreModal = orderDetailsStitchingStoreModal;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_stitching_store_order_details_list,viewGroup,false);
        return new StitchingStoreOrderDetailsAdapter.Holder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        holder.mStitchingSellerNameTxt.setText(mContext.getResources().getString(R.string.seller) +" : "+ mSellerName.get(position));

        ArrayList<StichingOrderDetailsEntity> StitchingList = new ArrayList<>();
        ArrayList<StichingOrderDetailsEntity> StoreList = new ArrayList<>();

        for (int i = 0; i < mStitchingOrderDetailsList.size(); i++) {
            if (mStitchingOrderDetailsList.get(i).getSellerId() == Integer.parseInt(mSellerId.get(position))) {
                if (mStitchingOrderDetailsList.get(i).getOrderType().equalsIgnoreCase("store")) {
                    StoreList.add(mStitchingOrderDetailsList.get(i));
                } else if (mStitchingOrderDetailsList.get(i).getOrderType().equalsIgnoreCase("Stitching-Direct")||mStitchingOrderDetailsList.get(i).getOrderType().equalsIgnoreCase("Stitching-Quotation")) {
                    StitchingList.add(mStitchingOrderDetailsList.get(i));
                }
            }
        }

        mStitchingOrderDetailsAdapter = new SttichingOrderDetailsAdapter(StitchingList,mContext,mOrderDetailsStitchingStoreModal);
        holder.mOrderDetailsStitchingRecView.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false));
        holder.mOrderDetailsStitchingRecView.setAdapter(mStitchingOrderDetailsAdapter);

        mStoreOrderDetailsAdapter = new StoreOrderDetailsAdapter(StoreList,mContext,mOrderDetailsStitchingStoreModal);
        holder.mOrderDetailsStoreRecView.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false));
        holder.mOrderDetailsStoreRecView.setAdapter(mStoreOrderDetailsAdapter);

    }

    @Override
    public int getItemCount() {
        return mSellerId.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.adapter_stitching_seller_name_txt)
        TextView mStitchingSellerNameTxt;

        @BindView(R.id.order_details_stitching_rec_view)
        RecyclerView mOrderDetailsStitchingRecView;

        @BindView(R.id.order_details_store_rec_view)
        RecyclerView mOrderDetailsStoreRecView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
