package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetAllCategoriesEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.ui.OffersScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OfferHeaderAdapter  extends RecyclerView.Adapter<OfferHeaderAdapter.Holder> {

    private Context mContext;
    private ArrayList<GetAllCategoriesEntity> mGetAllCategoriesEntityList;

    private UserDetailsEntity mUserDetailsEntityRes;
    public OfferHeaderAdapter(Context activity, ArrayList<GetAllCategoriesEntity> getAllCategoriesEntity) {
        mContext = activity;
        mGetAllCategoriesEntityList = getAllCategoriesEntity;
    }

    @NonNull
    @Override
    public OfferHeaderAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_offer_header_list, parent, false);
        return new OfferHeaderAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OfferHeaderAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final GetAllCategoriesEntity getAllCategoriesEntity = mGetAllCategoriesEntityList.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.OFFER_CATEGORIES = "";
                for (int i=0; i<mGetAllCategoriesEntityList.size(); i++){
                    mGetAllCategoriesEntityList.get(i).setChecked(false);
                }

                mGetAllCategoriesEntityList.get(position).setChecked(true);

                notifyDataSetChanged();

                AppConstants.OFFER_CATEGORIES_ID = String.valueOf(mGetAllCategoriesEntityList.get(position).getId());
                ((OffersScreen)mContext).getOfferProductsApiCall(String.valueOf(mGetAllCategoriesEntityList.get(position).getId()));

//                ((OffersScreen)mContext).getOfferCategoriesApiCall(String.valueOf(mGetAllCategoriesEntityList.get(position).getId()));
            }
        });

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mOfferHeaderTxt.setText(getAllCategoriesEntity.getCategoriesNameInArabic());

        }else {
            holder.mOfferHeaderTxt.setText(getAllCategoriesEntity.getCategoriesNameInEnglis());

        }
        holder.mOfferHeaderTxt.setBackground(getAllCategoriesEntity.isChecked() ? mContext.getResources().getDrawable(R.drawable.bottom_orange_clr) : mContext.getResources().getDrawable(R.drawable.bottom_grey_clr) );

    }

    @Override
    public int getItemCount() {
        return mGetAllCategoriesEntityList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.offer_header_list_txt)
        TextView mOfferHeaderTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


