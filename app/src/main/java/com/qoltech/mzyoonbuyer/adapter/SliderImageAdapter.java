package com.qoltech.mzyoonbuyer.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GetMeasurementImageEntity;
import com.qoltech.mzyoonbuyer.entity.GetMeasurementPartEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.ui.MeasurementTwoScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class SliderImageAdapter extends PagerAdapter {

    private ArrayList<GetMeasurementImageEntity> IMAGES;
    private ArrayList<GetMeasurementPartEntity> mParts;
    private LayoutInflater inflater;
    private Context context;
    private Dialog mSlider;
    double num = 0.0;
    ArrayList<String> Values = new ArrayList<>();
    ArrayList<String> Inches = new ArrayList<>();
    private MeasurementScaleAdapter mMeasurementAdapter;
    int mAnimCount = 0;

    ArrayList<String> mArrayList;
    HashMap<String,String> parts = new HashMap<>();
    String mCheckToAdd = "";

    private UserDetailsEntity mUserDetailsEntityRes;

    public SliderImageAdapter(Context context, ArrayList<GetMeasurementImageEntity> IMAGES , ArrayList<GetMeasurementPartEntity> part,int animCount) {
        this.context = context;
        this.IMAGES = IMAGES;
        mParts = part;
        mAnimCount = animCount;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(context, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

//        updateResources(context,"en");

        View imageLayout = inflater.inflate(R.layout.adapter_measurement_front_one, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image);

        final LinearLayout mMenOneLay = (LinearLayout) imageLayout.findViewById(R.id.measurement_men_one_lay);
        final LinearLayout mMenTwoLay = (LinearLayout) imageLayout.findViewById(R.id.measurement_men_two_lay);
        final LinearLayout mMenThreeLay = (LinearLayout) imageLayout.findViewById(R.id.measurement_men_three_lay);
        final LinearLayout mMenFourLay = (LinearLayout) imageLayout.findViewById(R.id.measurement_men_four_lay);
        final LinearLayout mWomenOneLay = (LinearLayout) imageLayout.findViewById(R.id.measurement_women_one_lay);

        final ImageView mArrowMen1Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_1_img);
        final ImageView mArrowMen2Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_2_img);
        final ImageView mArrowMen3Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_3_img);
        final ImageView mArrowMen4Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_4_img);
        final ImageView mArrowMen5Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_5_img);
        final ImageView mArrowMen6Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_6_img);
        final ImageView mArrowMen7Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_7_img);
        final ImageView mArrowMen8Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_8_img);
        final ImageView mArrowMen9Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_9_img);
        final ImageView mArrowMen10Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_10_img);
        final ImageView mArrowMen11Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_11_img);
        final ImageView mArrowMen12Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_12_img);
        final ImageView mArrowMen13Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_13_img);
        final ImageView mArrowMen14Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_14_img);
        final ImageView mArrowMen15Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_15_img);
        final ImageView mArrowMen16Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_16_img);
        final ImageView mArrowMen17Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_17_img);
        final ImageView mArrowMen18Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_18_img);
        final ImageView mArrowMen19Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_19_img);
        final ImageView mArrowMen20Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_20_img);
        final ImageView mArrowMen21Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_21_img);
        final ImageView mArrowMen22Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_22_img);
        final ImageView mArrowMen23Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_23_img);
        final ImageView mArrowMen24Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_24_img);
        final ImageView mArrowMen25Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_25_img);
        final ImageView mArrowMen26Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_26_img);
        final ImageView mArrowMen27Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_27_img);
        final ImageView mArrowMen28Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_28_img);
        final ImageView mArrowMen29Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_29_img);
        final ImageView mArrowMen30Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_30_img);
        final ImageView mArrowMen31Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_31_img);
        final ImageView mArrowMen32Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_32_img);
        final ImageView mArrowMen33Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_33_img);
        final ImageView mArrowMen34Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_34_img);
        final ImageView mArrowMen35Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_35_img);
        final ImageView mArrowMen36Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_36_img);
        final ImageView mArrowMen37Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_37_img);
        final ImageView mArrowMen38Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_38_img);
        final ImageView mArrowMen39Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_39_img);
        final ImageView mArrowMen40Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_40_img);
        final ImageView mArrowMen41Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_41_img);
        final ImageView mArrowMen42Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_42_img);
        final ImageView mArrowMen43Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_43_img);
        final ImageView mArrowMen44Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_44_img);
        final ImageView mArrowMen45Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_45_img);
        final ImageView mArrowMen46Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_46_img);
        final ImageView mArrowMen47Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_47_img);
        final ImageView mArrowMen48Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_48_img);
        final ImageView mArrowMen49Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_49_img);
        final ImageView mArrowMen50Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_50_img);
        final ImageView mArrowMen51Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_51_img);
        final ImageView mArrowMen52Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_52_img);
        final ImageView mArrowMen53Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_53_img);
        final ImageView mArrowMen54Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_54_img);
        final ImageView mArrowMen55Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_55_img);
        final ImageView mArrowMen56Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_56_img);
        final ImageView mArrowMen57Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_57_img);
        final ImageView mArrowMen58Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_58_img);
        final ImageView mArrowMen59Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_59_img);
        final ImageView mArrowMen60Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_60_img);
        final ImageView mArrowMen61Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_61_img);
        final ImageView mArrowMen62Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_62_img);
        final ImageView mArrowMen63Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_63_img);
        final ImageView mArrowMen64Img = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_64_img);
        final ImageView mArrowWomen65Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_65_img);
        final ImageView mArrowWomen66Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_66_img);
        final ImageView mArrowWomen67Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_67_img);
        final ImageView mArrowWomen68Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_68_img);
        final ImageView mArrowWomen69Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_69_img);
        final ImageView mArrowWomen70Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_70_img);
        final ImageView mArrowWomen71Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_71_img);
        final ImageView mArrowWomen72Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_72_img);
        final ImageView mArrowWomen73Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_73_img);
        final ImageView mArrowWomen74Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_74_img);
        final ImageView mArrowWomen75Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_75_img);
        final ImageView mArrowWomen76Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_76_img);
        final ImageView mArrowWomen77Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_77_img);
        final ImageView mArrowWomen78Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_78_img);
        final ImageView mArrowWomen79Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_79_img);
        final ImageView mArrowWomen80Img = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_80_img);

        final ImageView mMen1StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_1_start_img);
        final ImageView mMen2StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_2_start_img);
        final ImageView mMen3StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_3_start_img);
        final ImageView mMen4StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_4_start_img);
        final ImageView mMen5StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_5_start_img);
        final ImageView mMen6StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_6_start_img);
        final ImageView mMen7StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_7_start_img);
        final ImageView mMen8StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_8_start_img);
        final ImageView mMen9StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_9_start_img);
        final ImageView mMen10StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_10_start_img);
        final ImageView mMen11StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_11_start_img);
        final ImageView mMen12StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_12_start_img);
        final ImageView mMen13StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_13_start_img);
        final ImageView mMen14StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_14_start_img);
        final ImageView mMen15StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_15_start_img);
        final ImageView mMen16StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_16_start_img);
        final ImageView mMen17StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_17_start_img);
        final ImageView mMen18StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_18_start_img);
        final ImageView mMen19StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_19_start_img);
        final ImageView mMen20StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_20_start_img);
        final ImageView mMen21StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_21_start_img);
        final ImageView mMen22StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_22_start_img);
        final ImageView mMen23StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_23_start_img);
        final ImageView mMen24StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_24_start_img);
        final ImageView mMen25StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_25_start_img);
        final ImageView mMen26StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_26_start_img);
        final ImageView mMen27StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_27_start_img);
        final ImageView mMen28StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_28_start_img);
        final ImageView mMen29StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_29_start_img);
        final ImageView mMen30StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_30_start_img);
        final ImageView mMen31StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_31_start_img);
        final ImageView mMen32StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_32_start_img);
        final ImageView mMen33StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_33_start_img);
        final ImageView mMen34StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_34_start_img);
        final ImageView mMen35StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_35_start_img);
        final ImageView mMen36StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_36_start_img);
        final ImageView mMen37StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_37_start_img);
        final ImageView mMen38StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_38_start_img);
        final ImageView mMen39StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_39_start_img);
        final ImageView mMen40StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_40_start_img);
        final ImageView mMen41StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_41_start_img);
        final ImageView mMen42StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_42_start_img);
        final ImageView mMen43StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_43_start_img);
        final ImageView mMen44StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_44_start_img);
        final ImageView mMen45StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_45_start_img);
        final ImageView mMen46StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_46_start_img);
        final ImageView mMen47StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_47_start_img);
        final ImageView mMen48StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_48_start_img);
        final ImageView mMen49StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_49_start_img);
        final ImageView mMen50StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_50_start_img);
        final ImageView mMen51StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_51_start_img);
        final ImageView mMen52StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_52_start_img);
        final ImageView mMen53StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_53_start_img);
        final ImageView mMen54StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_54_start_img);
        final ImageView mMen55StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_55_start_img);
        final ImageView mMen56StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_56_start_img);
        final ImageView mMen57StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_57_start_img);
        final ImageView mMen58StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_58_start_img);
        final ImageView mMen59StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_59_start_img);
        final ImageView mMen60StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_60_start_img);
        final ImageView mMen61StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_61_start_img);
        final ImageView mMen62StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_62_start_img);
        final ImageView mMen63StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_63_start_img);
        final ImageView mMen64StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_men_id_64_start_img);
        final ImageView mWomen65StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_65_start_img);
        final ImageView mWomen66StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_66_start_img);
        final ImageView mWomen67StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_67_start_img);
        final ImageView mWomen68StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_68_start_img);
        final ImageView mWomen69StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_69_start_img);
        final ImageView mWomen70StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_70_start_img);
        final ImageView mWomen71StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_71_start_img);
        final ImageView mWomen72StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_72_start_img);
        final ImageView mWomen73StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_73_start_img);
        final ImageView mWomen74StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_74_start_img);
        final ImageView mWomen75StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_75_start_img);
        final ImageView mWomen76StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_76_start_img);
        final ImageView mWomen77StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_77_start_img);
        final ImageView mWomen78StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_78_start_img);
        final ImageView mWomen79StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_79_start_img);
        final ImageView mWomen80StartImg = (ImageView) imageLayout.findViewById(R.id.measurement_women_id_80_start_img);

        final RelativeLayout mMen1ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_1_lay);
        final RelativeLayout mMen2ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_2_lay);
        final RelativeLayout mMen3ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_3_lay);
        final RelativeLayout mMen4ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_4_lay);
        final RelativeLayout mMen5ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_5_lay);
        final RelativeLayout mMen6ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_6_lay);
        final RelativeLayout mMen7ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_7_lay);
        final RelativeLayout mMen8ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_8_lay);
        final RelativeLayout mMen9ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_9_lay);
        final RelativeLayout mMen10ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_10_lay);
        final RelativeLayout mMen11ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_11_lay);
        final RelativeLayout mMen12ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_12_lay);
        final RelativeLayout mMen13ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_13_lay);
        final RelativeLayout mMen14ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_14_lay);
        final RelativeLayout mMen15ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_15_lay);
        final RelativeLayout mMen16ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_16_lay);
        final RelativeLayout mMen17ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_17_lay);
        final RelativeLayout mMen18ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_18_lay);
        final RelativeLayout mMen19ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_19_lay);
        final RelativeLayout mMen20ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_20_lay);
        final RelativeLayout mMen21ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_21_lay);
        final RelativeLayout mMen22ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_22_lay);
        final RelativeLayout mMen23ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_23_lay);
        final RelativeLayout mMen24ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_24_lay);
        final RelativeLayout mMen25ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_25_lay);
        final RelativeLayout mMen26ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_26_lay);
        final RelativeLayout mMen27ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_27_lay);
        final RelativeLayout mMen28ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_28_lay);
        final RelativeLayout mMen29ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_29_lay);
        final RelativeLayout mMen30ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_30_lay);
        final RelativeLayout mMen31ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_31_lay);
        final RelativeLayout mMen32ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_32_lay);
        final RelativeLayout mMen33ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_33_lay);
        final RelativeLayout mMen34ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_34_lay);
        final RelativeLayout mMen35ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_35_lay);
        final RelativeLayout mMen36ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_36_lay);
        final RelativeLayout mMen37ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_37_lay);
        final RelativeLayout mMen38ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_38_lay);
        final RelativeLayout mMen39ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_39_lay);
        final RelativeLayout mMen40ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_40_lay);
        final RelativeLayout mMen41ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_41_lay);
        final RelativeLayout mMen42ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_42_lay);
        final RelativeLayout mMen43ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_43_lay);
        final RelativeLayout mMen44ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_44_lay);
        final RelativeLayout mMen45ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_45_lay);
        final RelativeLayout mMen46ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_46_lay);
        final RelativeLayout mMen47ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_47_lay);
        final RelativeLayout mMen48ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_48_lay);
        final RelativeLayout mMen49ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_49_lay);
        final RelativeLayout mMen50ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_50_lay);
        final RelativeLayout mMen51ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_51_lay);
        final RelativeLayout mMen52ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_52_lay);
        final RelativeLayout mMen53ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_53_lay);
        final RelativeLayout mMen54ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_54_lay);
        final RelativeLayout mMen55ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_55_lay);
        final RelativeLayout mMen56ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_56_lay);
        final RelativeLayout mMen57ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_57_lay);
        final RelativeLayout mMen58ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_58_lay);
        final RelativeLayout mMen59ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_59_lay);
        final RelativeLayout mMen60ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_60_lay);
        final RelativeLayout mMen61ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_61_lay);
        final RelativeLayout mMen62ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_62_lay);
        final RelativeLayout mMen63ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_63_lay);
        final RelativeLayout mMen64ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_men_id_64_lay);
        final RelativeLayout mWomen65ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_65_lay);
        final RelativeLayout mWomen66ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_66_lay);
        final RelativeLayout mWomen67ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_67_lay);
        final RelativeLayout mWomen68ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_68_lay);
        final RelativeLayout mWomen69ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_69_lay);
        final RelativeLayout mWomen70ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_70_lay);
        final RelativeLayout mWomen71ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_71_lay);
        final RelativeLayout mWomen72ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_72_lay);
        final RelativeLayout mWomen73ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_73_lay);
        final RelativeLayout mWomen74ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_74_lay);
        final RelativeLayout mWomen75ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_75_lay);
        final RelativeLayout mWomen76ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_76_lay);
        final RelativeLayout mWomen77ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_77_lay);
        final RelativeLayout mWomen78ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_78_lay);
        final RelativeLayout mWomen79ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_79_lay);
        final RelativeLayout mWomen80ParLay = (RelativeLayout) imageLayout.findViewById(R.id.measurement_women_id_80_lay);

        final TextView mMen1Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_1_txt);
        final TextView mMen2Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_2_txt);
        final TextView mMen3Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_3_txt);
        final TextView mMen4Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_4_txt);
        final TextView mMen5Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_5_txt);
        final TextView mMen6Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_6_txt);
        final TextView mMen7Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_7_txt);
        final TextView mMen8Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_8_txt);
        final TextView mMen9Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_9_txt);
        final TextView mMen10Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_10_txt);
        final TextView mMen11Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_11_txt);
        final TextView mMen12Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_12_txt);
        final TextView mMen13Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_13_txt);
        final TextView mMen14Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_14_txt);
        final TextView mMen15Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_15_txt);
        final TextView mMen16Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_16_txt);
        final TextView mMen17Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_17_txt);
        final TextView mMen18Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_18_txt);
        final TextView mMen19Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_19_txt);
        final TextView mMen20Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_20_txt);
        final TextView mMen21Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_21_txt);
        final TextView mMen22Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_22_txt);
        final TextView mMen23Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_23_txt);
        final TextView mMen24Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_24_txt);
        final TextView mMen25Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_25_txt);
        final TextView mMen26Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_26_txt);
        final TextView mMen27Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_27_txt);
        final TextView mMen28Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_28_txt);
        final TextView mMen29Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_29_txt);
        final TextView mMen30Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_30_txt);
        final TextView mMen31Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_31_txt);
        final TextView mMen32Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_32_txt);
        final TextView mMen33Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_33_txt);
        final TextView mMen34Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_34_txt);
        final TextView mMen35Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_35_txt);
        final TextView mMen36Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_36_txt);
        final TextView mMen37Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_37_txt);
        final TextView mMen38Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_38_txt);
        final TextView mMen39Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_39_txt);
        final TextView mMen40Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_40_txt);
        final TextView mMen41Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_41_txt);
        final TextView mMen42Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_42_txt);
        final TextView mMen43Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_43_txt);
        final TextView mMen44Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_44_txt);
        final TextView mMen45Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_45_txt);
        final TextView mMen46Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_46_txt);
        final TextView mMen47Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_47_txt);
        final TextView mMen48Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_48_txt);
        final TextView mMen49Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_49_txt);
        final TextView mMen50Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_50_txt);
        final TextView mMen51Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_51_txt);
        final TextView mMen52Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_52_txt);
        final TextView mMen53Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_53_txt);
        final TextView mMen54Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_54_txt);
        final TextView mMen55Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_55_txt);
        final TextView mMen56Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_56_txt);
        final TextView mMen57Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_57_txt);
        final TextView mMen58Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_58_txt);
        final TextView mMen59Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_59_txt);
        final TextView mMen60Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_60_txt);
        final TextView mMen61Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_61_txt);
        final TextView mMen62Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_62_txt);
        final TextView mMen63Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_63_txt);
        final TextView mMen64Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_men_id_64_txt);
        final TextView mWomen65Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_65_txt);
        final TextView mWomen66Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_66_txt);
        final TextView mWomen67Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_67_txt);
        final TextView mWomen68Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_68_txt);
        final TextView mWomen69Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_69_txt);
        final TextView mWomen70Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_70_txt);
        final TextView mWomen71Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_71_txt);
        final TextView mWomen72Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_72_txt);
        final TextView mWomen73Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_73_txt);
        final TextView mWomen74Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_74_txt);
        final TextView mWomen75Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_75_txt);
        final TextView mWomen76Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_76_txt);
        final TextView mWomen77Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_77_txt);
        final TextView mWomen78Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_78_txt);
        final TextView mWomen79Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_79_txt);
        final TextView mWomen80Txt = (TextView)imageLayout.findViewById(R.id.measurement_num_women_id_80_txt);

        if (mAnimCount == 0){
            Animation animSlide = AnimationUtils.loadAnimation(context.getApplicationContext(),
                    R.anim.animation_slow_enter);
            animSlide.setFillAfter(true);
            animSlide.setRepeatCount(1);

            mArrowMen1Img.startAnimation(animSlide);
            mArrowMen2Img.startAnimation(animSlide);
            mArrowMen3Img.startAnimation(animSlide);
            mArrowMen4Img.startAnimation(animSlide);
            mArrowMen5Img.startAnimation(animSlide);
            mArrowMen6Img.startAnimation(animSlide);
            mArrowMen7Img.startAnimation(animSlide);
            mArrowMen8Img.startAnimation(animSlide);
            mArrowMen9Img.startAnimation(animSlide);
            mArrowMen10Img.startAnimation(animSlide);
            mArrowMen11Img.startAnimation(animSlide);
            mArrowMen12Img.startAnimation(animSlide);
            mArrowMen13Img.startAnimation(animSlide);
            mArrowMen14Img.startAnimation(animSlide);
            mArrowMen15Img.startAnimation(animSlide);
            mArrowMen16Img.startAnimation(animSlide);
            mArrowMen17Img.startAnimation(animSlide);
            mArrowMen18Img.startAnimation(animSlide);
            mArrowMen19Img.startAnimation(animSlide);
            mArrowMen20Img.startAnimation(animSlide);
            mArrowMen21Img.startAnimation(animSlide);
            mArrowMen22Img.startAnimation(animSlide);
            mArrowMen23Img.startAnimation(animSlide);
            mArrowMen24Img.startAnimation(animSlide);
            mArrowMen25Img.startAnimation(animSlide);
            mArrowMen26Img.startAnimation(animSlide);
            mArrowMen27Img.startAnimation(animSlide);
            mArrowMen28Img.startAnimation(animSlide);
            mArrowMen29Img.startAnimation(animSlide);
            mArrowMen30Img.startAnimation(animSlide);
            mArrowMen31Img.startAnimation(animSlide);
            mArrowMen32Img.startAnimation(animSlide);
            mArrowMen33Img.startAnimation(animSlide);
            mArrowMen34Img.startAnimation(animSlide);
            mArrowMen35Img.startAnimation(animSlide);
            mArrowMen36Img.startAnimation(animSlide);
            mArrowMen37Img.startAnimation(animSlide);
            mArrowMen38Img.startAnimation(animSlide);
            mArrowMen39Img.startAnimation(animSlide);
            mArrowMen40Img.startAnimation(animSlide);
            mArrowMen41Img.startAnimation(animSlide);
            mArrowMen42Img.startAnimation(animSlide);
            mArrowMen43Img.startAnimation(animSlide);
            mArrowMen44Img.startAnimation(animSlide);
            mArrowMen45Img.startAnimation(animSlide);
            mArrowMen46Img.startAnimation(animSlide);
            mArrowMen47Img.startAnimation(animSlide);
            mArrowMen48Img.startAnimation(animSlide);
            mArrowMen49Img.startAnimation(animSlide);
            mArrowMen50Img.startAnimation(animSlide);
            mArrowMen51Img.startAnimation(animSlide);
            mArrowMen52Img.startAnimation(animSlide);
            mArrowMen53Img.startAnimation(animSlide);
            mArrowMen54Img.startAnimation(animSlide);
            mArrowMen55Img.startAnimation(animSlide);
            mArrowMen56Img.startAnimation(animSlide);
            mArrowMen57Img.startAnimation(animSlide);
            mArrowMen58Img.startAnimation(animSlide);
            mArrowMen59Img.startAnimation(animSlide);
            mArrowMen60Img.startAnimation(animSlide);
            mArrowMen61Img.startAnimation(animSlide);
            mArrowMen62Img.startAnimation(animSlide);
            mArrowMen63Img.startAnimation(animSlide);
            mArrowMen64Img.startAnimation(animSlide);
            mArrowWomen65Img.startAnimation(animSlide);
            mArrowWomen66Img.startAnimation(animSlide);
            mArrowWomen67Img.startAnimation(animSlide);
            mArrowWomen68Img.startAnimation(animSlide);
            mArrowWomen69Img.startAnimation(animSlide);
            mArrowWomen70Img.startAnimation(animSlide);
            mArrowWomen71Img.startAnimation(animSlide);
            mArrowWomen72Img.startAnimation(animSlide);
            mArrowWomen73Img.startAnimation(animSlide);
            mArrowWomen74Img.startAnimation(animSlide);
            mArrowWomen75Img.startAnimation(animSlide);
            mArrowWomen76Img.startAnimation(animSlide);
            mArrowWomen77Img.startAnimation(animSlide);
            mArrowWomen78Img.startAnimation(animSlide);
            mArrowWomen79Img.startAnimation(animSlide);
            mArrowWomen80Img.startAnimation(animSlide);
            mAnimCount = 1;
        }

        for (int i=0; i<mParts.size(); i++){

            if (mParts.get(i).getId() == 1 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")|| mParts.get(i).getId() == 65&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 145&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 209&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen1ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen1StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 2&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 66&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 146&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 210&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen2ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen2StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 3&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 67&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 147&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 211&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen3ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen3StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 4&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 68&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 148&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 212&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen4ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen4StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 5&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 69&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 149&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 213&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen5ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen5StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 6&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 70&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 150&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 214&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen6ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen6StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId()  == 7&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 71&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 151&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 215&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen7ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen7StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 8&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 72&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 152&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 216&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen8ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen8StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 9&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 73&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 153&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 217&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen9ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen9StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 10&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 74&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 154&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 218&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen10ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen10StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 11&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 75&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 155&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 219&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen11ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen11StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 12&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 76&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 156&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 220&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen12ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen12StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 13&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 77&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 157&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 221&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen13ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen13StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 14&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 78&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 158&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 222&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen14ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen14StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 15&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 79&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 159&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 223&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen15ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen15StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 16&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 80&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 160&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 224&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen16ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen16StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 17&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 81&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 161&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 225&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen17ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen17StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 18&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 82&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 162&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 226&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen18ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen18StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 19&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 83&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 163&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 227&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen19ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen19StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 20&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 84&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 164&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 228&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen20ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen20StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 21&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 85&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 165&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 229&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen21ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen21StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 22&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 86&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 166&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 230&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen22ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen22StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 23&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 87&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 167&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 231&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen23ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen23StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 24&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 88&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 168&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 232&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen24ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen24StartImg.setLayoutParams(layoutParams);

            }
            else if (mParts.get(i).getId() == 25&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 89&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 169&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 233&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen25ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen25StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 26&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 90&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 170&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 234&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen26ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen26StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 27&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 91&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 171&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 235&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen27ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen27StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 28&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 92&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 172&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 236&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen28ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen28StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 29&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() ==93&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 173&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 237&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen29ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen29StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 30&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 94&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 174&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 238&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen30ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen30StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 31&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 95&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 175&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 239&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen31ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen31StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 32&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 96&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 176&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 240&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen32ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen32StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 33&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 97&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 177&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 241&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen33ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen33StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 34&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 98&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 178&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 242&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen34ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen34StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 35&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 99&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 179&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 243&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen35ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen35StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 36&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 100&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 180&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 244&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen36ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen36StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 37&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 101&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 181&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 245&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen37ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen37StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 38&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 102&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 182&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 246&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen38ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen38StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 39&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 103&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 183&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 247&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen39ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen39StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 40&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 104&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 184&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 248&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen40ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen40StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 41&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 105&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 185&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 249&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen41ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen41StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 42&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 106&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 186&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 250&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen42ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen42StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 43&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 107&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 187&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 251&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen43ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen43StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 44&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 108&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 188&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 252&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen44ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen44StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 45&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 109&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 189&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 253&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen45ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen45StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 46&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 110&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 190&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 254&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen46ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen46StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 47&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 111&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 191&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 255&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen47ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen47StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 48&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 112&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 192&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 256&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen48ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen48StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 49&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 113&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 193&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 257&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen49ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen49StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 50&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 114&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 194&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 258&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen50ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen50StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 51&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 115&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 195&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 259&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen51ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen51StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 52&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 116&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 196&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 260&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen52ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen52StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 53&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 117&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 197&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 261&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen53ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen53StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 54&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() ==118&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 198&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 262&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen54ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen54StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 55&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 119&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 199&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 263&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen55ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen55StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 56&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 120&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 200&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 264&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen56ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen56StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 57&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 121&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 201&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 265&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen57ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen57StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 58&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 122&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 202&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 266&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen58ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen58StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 59&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 123&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 203&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 267&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen59ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen59StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 60&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 124&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 204&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 268&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen60ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen60StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 61&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 125&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 205&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 269&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen61ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen61StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 62&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 126&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 206&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 270&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen62ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen62StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 63&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 127&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 207&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 271&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen63ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen63StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 64&&AppConstants.GENDER_ID.equalsIgnoreCase("1")||mParts.get(i).getId() == 128&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 208&&AppConstants.GENDER_ID.equalsIgnoreCase("3")||mParts.get(i).getId() == 272&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mMen64ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mMen64StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 129&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 273&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen65ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen65StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 130&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 274&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen66ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen66StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 131&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 275&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen67ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen67StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 132&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 276&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen68ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen68StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 133&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 277&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen69ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen69StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 134&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 278&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen70ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen70StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 135&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 279&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen71ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen71StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 136&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 280&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen72ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen72StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 137&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 281&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen73ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen73StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 138&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 282&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen74ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen74StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 139&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 283&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen75ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen75StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 140&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 284&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen76ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen76StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 141&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 285&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen77ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen77StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 142&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 286&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen78ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen78StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 143&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 287&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen79ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen79StartImg.setLayoutParams(layoutParams);
            }
            else if (mParts.get(i).getId() == 144&&AppConstants.GENDER_ID.equalsIgnoreCase("2")||mParts.get(i).getId() == 288&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                mWomen80ParLay.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        (int) context.getResources().getDimension(R.dimen.size8),
                        (int) context.getResources().getDimension(R.dimen.size8));
                int left =  (int) mParts.get(i).getLengthInAndroid();
                int top = 0;
                int right = 0;
                int bottom = 0;
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
                layoutParams.setMargins(left, top, right, bottom);
                mWomen80StartImg.setLayoutParams(layoutParams);
            }

        }

        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
            if (AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                mMen1Txt.setText(AppConstants.MEN_1);
                mMen2Txt.setText(AppConstants.MEN_2);
                mMen3Txt.setText(AppConstants.MEN_3);
                mMen4Txt.setText(AppConstants.MEN_4);
                mMen5Txt.setText(AppConstants.MEN_5);
                mMen6Txt.setText(AppConstants.MEN_6);
                mMen7Txt.setText(AppConstants.MEN_7);
                mMen8Txt.setText(AppConstants.MEN_8);
                mMen9Txt.setText(AppConstants.MEN_9);
                mMen10Txt.setText(AppConstants.MEN_10);
                mMen11Txt.setText(AppConstants.MEN_11);
                mMen12Txt.setText(AppConstants.MEN_12);
                mMen13Txt.setText(AppConstants.MEN_13);
                mMen14Txt.setText(AppConstants.MEN_14);
                mMen15Txt.setText(AppConstants.MEN_15);
                mMen16Txt.setText(AppConstants.MEN_16);
                mMen17Txt.setText(AppConstants.MEN_17);
                mMen18Txt.setText(AppConstants.MEN_18);
                mMen19Txt.setText(AppConstants.MEN_19);
                mMen20Txt.setText(AppConstants.MEN_20);
                mMen21Txt.setText(AppConstants.MEN_21);
                mMen22Txt.setText(AppConstants.MEN_22);
                mMen23Txt.setText(AppConstants.MEN_23);
                mMen24Txt.setText(AppConstants.MEN_24);
                mMen25Txt.setText(AppConstants.MEN_25);
                mMen26Txt.setText(AppConstants.MEN_26);
                mMen27Txt.setText(AppConstants.MEN_27);
                mMen28Txt.setText(AppConstants.MEN_28);
                mMen29Txt.setText(AppConstants.MEN_29);
                mMen30Txt.setText(AppConstants.MEN_30);
                mMen31Txt.setText(AppConstants.MEN_31);
                mMen32Txt.setText(AppConstants.MEN_32);
                mMen33Txt.setText(AppConstants.MEN_33);
                mMen34Txt.setText(AppConstants.MEN_34);
                mMen35Txt.setText(AppConstants.MEN_35);
                mMen36Txt.setText(AppConstants.MEN_36);
                mMen37Txt.setText(AppConstants.MEN_37);
                mMen38Txt.setText(AppConstants.MEN_38);
                mMen39Txt.setText(AppConstants.MEN_39);
                mMen40Txt.setText(AppConstants.MEN_40);
                mMen41Txt.setText(AppConstants.MEN_41);
                mMen42Txt.setText(AppConstants.MEN_42);
                mMen43Txt.setText(AppConstants.MEN_43);
                mMen44Txt.setText(AppConstants.MEN_44);
                mMen45Txt.setText(AppConstants.MEN_45);
                mMen46Txt.setText(AppConstants.MEN_46);
                mMen47Txt.setText(AppConstants.MEN_47);
                mMen48Txt.setText(AppConstants.MEN_48);
                mMen49Txt.setText(AppConstants.MEN_49);
                mMen50Txt.setText(AppConstants.MEN_50);
                mMen51Txt.setText(AppConstants.MEN_51);
                mMen52Txt.setText(AppConstants.MEN_52);
                mMen53Txt.setText(AppConstants.MEN_53);
                mMen54Txt.setText(AppConstants.MEN_54);
                mMen55Txt.setText(AppConstants.MEN_55);
                mMen56Txt.setText(AppConstants.MEN_56);
                mMen57Txt.setText(AppConstants.MEN_57);
                mMen58Txt.setText(AppConstants.MEN_58);
                mMen59Txt.setText(AppConstants.MEN_59);
                mMen60Txt.setText(AppConstants.MEN_60);
                mMen61Txt.setText(AppConstants.MEN_61);
                mMen62Txt.setText(AppConstants.MEN_62);
                mMen63Txt.setText(AppConstants.MEN_63);
                mMen64Txt.setText(AppConstants.MEN_64);

            }
            else if (AppConstants.GENDER_ID.equalsIgnoreCase("2")){

                mMen1Txt.setText(AppConstants.WOMEN_65);
                mMen2Txt.setText(AppConstants.WOMEN_66);
                mMen3Txt.setText(AppConstants.WOMEN_67);
                mMen4Txt.setText(AppConstants.WOMEN_68);
                mMen5Txt.setText(AppConstants.WOMEN_69);
                mMen6Txt.setText(AppConstants.WOMEN_70);
                mMen7Txt.setText(AppConstants.WOMEN_71);
                mMen8Txt.setText(AppConstants.WOMEN_72);
                mMen9Txt.setText(AppConstants.WOMEN_73);
                mMen10Txt.setText(AppConstants.WOMEN_74);
                mMen11Txt.setText(AppConstants.WOMEN_75);
                mMen12Txt.setText(AppConstants.WOMEN_76);
                mMen13Txt.setText(AppConstants.WOMEN_77);
                mMen14Txt.setText(AppConstants.WOMEN_78);
                mMen15Txt.setText(AppConstants.WOMEN_79);
                mMen16Txt.setText(AppConstants.WOMEN_80);
                mMen17Txt.setText(AppConstants.WOMEN_81);
                mMen18Txt.setText(AppConstants.WOMEN_82);
                mMen19Txt.setText(AppConstants.WOMEN_83);
                mMen20Txt.setText(AppConstants.WOMEN_84);
                mMen21Txt.setText(AppConstants.WOMEN_85);
                mMen22Txt.setText(AppConstants.WOMEN_86);
                mMen23Txt.setText(AppConstants.WOMEN_87);
                mMen24Txt.setText(AppConstants.WOMEN_88);
                mMen25Txt.setText(AppConstants.WOMEN_89);
                mMen26Txt.setText(AppConstants.WOMEN_90);
                mMen27Txt.setText(AppConstants.WOMEN_91);
                mMen28Txt.setText(AppConstants.WOMEN_92);
                mMen29Txt.setText(AppConstants.WOMEN_93);
                mMen30Txt.setText(AppConstants.WOMEN_94);
                mMen31Txt.setText(AppConstants.WOMEN_95);
                mMen32Txt.setText(AppConstants.WOMEN_96);
                mMen33Txt.setText(AppConstants.WOMEN_97);
                mMen34Txt.setText(AppConstants.WOMEN_98);
                mMen35Txt.setText(AppConstants.WOMEN_99);
                mMen36Txt.setText(AppConstants.WOMEN_100);
                mMen37Txt.setText(AppConstants.WOMEN_101);
                mMen38Txt.setText(AppConstants.WOMEN_102);
                mMen39Txt.setText(AppConstants.WOMEN_103);
                mMen40Txt.setText(AppConstants.WOMEN_104);
                mMen41Txt.setText(AppConstants.WOMEN_105);
                mMen42Txt.setText(AppConstants.WOMEN_106);
                mMen43Txt.setText(AppConstants.WOMEN_107);
                mMen44Txt.setText(AppConstants.WOMEN_108);
                mMen45Txt.setText(AppConstants.WOMEN_109);
                mMen46Txt.setText(AppConstants.WOMEN_110);
                mMen47Txt.setText(AppConstants.WOMEN_111);
                mMen48Txt.setText(AppConstants.WOMEN_112);
                mMen49Txt.setText(AppConstants.WOMEN_113);
                mMen50Txt.setText(AppConstants.WOMEN_114);
                mMen51Txt.setText(AppConstants.WOMEN_115);
                mMen52Txt.setText(AppConstants.WOMEN_116);
                mMen53Txt.setText(AppConstants.WOMEN_117);
                mMen54Txt.setText(AppConstants.WOMEN_118);
                mMen55Txt.setText(AppConstants.WOMEN_119);
                mMen56Txt.setText(AppConstants.WOMEN_120);
                mMen57Txt.setText(AppConstants.WOMEN_121);
                mMen58Txt.setText(AppConstants.WOMEN_122);
                mMen59Txt.setText(AppConstants.WOMEN_123);
                mMen60Txt.setText(AppConstants.WOMEN_124);
                mMen61Txt.setText(AppConstants.WOMEN_125);
                mMen62Txt.setText(AppConstants.WOMEN_126);
                mMen63Txt.setText(AppConstants.WOMEN_127);
                mMen64Txt.setText(AppConstants.WOMEN_128);
                mWomen65Txt.setText(AppConstants.WOMEN_129);
                mWomen66Txt.setText(AppConstants.WOMEN_130);
                mWomen67Txt.setText(AppConstants.WOMEN_131);
                mWomen68Txt.setText(AppConstants.WOMEN_132);
                mWomen69Txt.setText(AppConstants.WOMEN_133);
                mWomen70Txt.setText(AppConstants.WOMEN_134);
                mWomen71Txt.setText(AppConstants.WOMEN_135);
                mWomen72Txt.setText(AppConstants.WOMEN_136);
                mWomen73Txt.setText(AppConstants.WOMEN_137);
                mWomen74Txt.setText(AppConstants.WOMEN_138);
                mWomen75Txt.setText(AppConstants.WOMEN_139);
                mWomen76Txt.setText(AppConstants.WOMEN_140);
                mWomen77Txt.setText(AppConstants.WOMEN_141);
                mWomen78Txt.setText(AppConstants.WOMEN_142);
                mWomen79Txt.setText(AppConstants.WOMEN_143);
                mWomen80Txt.setText(AppConstants.WOMEN_144);

            }
            else if (AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                mMen1Txt.setText(AppConstants.BOY_145);
                mMen2Txt.setText(AppConstants.BOY_146);
                mMen3Txt.setText(AppConstants.BOY_147);
                mMen4Txt.setText(AppConstants.BOY_148);
                mMen5Txt.setText(AppConstants.BOY_149);
                mMen6Txt.setText(AppConstants.BOY_150);
                mMen7Txt.setText(AppConstants.BOY_151);
                mMen8Txt.setText(AppConstants.BOY_152);
                mMen9Txt.setText(AppConstants.BOY_153);
                mMen10Txt.setText(AppConstants.BOY_154);
                mMen11Txt.setText(AppConstants.BOY_155);
                mMen12Txt.setText(AppConstants.BOY_156);
                mMen13Txt.setText(AppConstants.BOY_157);
                mMen14Txt.setText(AppConstants.BOY_158);
                mMen15Txt.setText(AppConstants.BOY_159);
                mMen16Txt.setText(AppConstants.BOY_160);
                mMen17Txt.setText(AppConstants.BOY_161);
                mMen18Txt.setText(AppConstants.BOY_162);
                mMen19Txt.setText(AppConstants.BOY_163);
                mMen20Txt.setText(AppConstants.BOY_164);
                mMen21Txt.setText(AppConstants.BOY_165);
                mMen22Txt.setText(AppConstants.BOY_166);
                mMen23Txt.setText(AppConstants.BOY_167);
                mMen24Txt.setText(AppConstants.BOY_168);
                mMen25Txt.setText(AppConstants.BOY_169);
                mMen26Txt.setText(AppConstants.BOY_170);
                mMen27Txt.setText(AppConstants.BOY_171);
                mMen28Txt.setText(AppConstants.BOY_172);
                mMen29Txt.setText(AppConstants.BOY_173);
                mMen30Txt.setText(AppConstants.BOY_174);
                mMen31Txt.setText(AppConstants.BOY_175);
                mMen32Txt.setText(AppConstants.BOY_176);
                mMen33Txt.setText(AppConstants.BOY_177);
                mMen34Txt.setText(AppConstants.BOY_178);
                mMen35Txt.setText(AppConstants.BOY_179);
                mMen36Txt.setText(AppConstants.BOY_180);
                mMen37Txt.setText(AppConstants.BOY_181);
                mMen38Txt.setText(AppConstants.BOY_182);
                mMen39Txt.setText(AppConstants.BOY_183);
                mMen40Txt.setText(AppConstants.BOY_184);
                mMen41Txt.setText(AppConstants.BOY_185);
                mMen42Txt.setText(AppConstants.BOY_186);
                mMen43Txt.setText(AppConstants.BOY_187);
                mMen44Txt.setText(AppConstants.BOY_188);
                mMen45Txt.setText(AppConstants.BOY_189);
                mMen46Txt.setText(AppConstants.BOY_190);
                mMen47Txt.setText(AppConstants.BOY_191);
                mMen48Txt.setText(AppConstants.BOY_192);
                mMen49Txt.setText(AppConstants.BOY_193);
                mMen50Txt.setText(AppConstants.BOY_194);
                mMen51Txt.setText(AppConstants.BOY_195);
                mMen52Txt.setText(AppConstants.BOY_196);
                mMen53Txt.setText(AppConstants.BOY_197);
                mMen54Txt.setText(AppConstants.BOY_198);
                mMen55Txt.setText(AppConstants.BOY_199);
                mMen56Txt.setText(AppConstants.BOY_200);
                mMen57Txt.setText(AppConstants.BOY_201);
                mMen58Txt.setText(AppConstants.BOY_202);
                mMen59Txt.setText(AppConstants.BOY_203);
                mMen60Txt.setText(AppConstants.BOY_204);
                mMen61Txt.setText(AppConstants.BOY_205);
                mMen62Txt.setText(AppConstants.BOY_206);
                mMen63Txt.setText(AppConstants.BOY_207);
                mMen64Txt.setText(AppConstants.BOY_208);


            }
            else if (AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                mMen1Txt.setText(AppConstants.GIRL_209);
                mMen2Txt.setText(AppConstants.GIRL_210);
                mMen3Txt.setText(AppConstants.GIRL_211);
                mMen4Txt.setText(AppConstants.GIRL_212);
                mMen5Txt.setText(AppConstants.GIRL_213);
                mMen6Txt.setText(AppConstants.GIRL_214);
                mMen7Txt.setText(AppConstants.GIRL_215);
                mMen8Txt.setText(AppConstants.GIRL_216);
                mMen9Txt.setText(AppConstants.GIRL_217);
                mMen10Txt.setText(AppConstants.GIRL_218);
                mMen11Txt.setText(AppConstants.GIRL_219);
                mMen12Txt.setText(AppConstants.GIRL_220);
                mMen13Txt.setText(AppConstants.GIRL_221);
                mMen14Txt.setText(AppConstants.GIRL_222);
                mMen15Txt.setText(AppConstants.GIRL_223);
                mMen16Txt.setText(AppConstants.GIRL_224);
                mMen17Txt.setText(AppConstants.GIRL_225);
                mMen18Txt.setText(AppConstants.GIRL_226);
                mMen19Txt.setText(AppConstants.GIRL_227);
                mMen20Txt.setText(AppConstants.GIRL_228);
                mMen21Txt.setText(AppConstants.GIRL_229);
                mMen22Txt.setText(AppConstants.GIRL_230);
                mMen23Txt.setText(AppConstants.GIRL_231);
                mMen24Txt.setText(AppConstants.GIRL_232);
                mMen25Txt.setText(AppConstants.GIRL_233);
                mMen26Txt.setText(AppConstants.GIRL_234);
                mMen27Txt.setText(AppConstants.GIRL_235);
                mMen28Txt.setText(AppConstants.GIRL_236);
                mMen29Txt.setText(AppConstants.GIRL_237);
                mMen30Txt.setText(AppConstants.GIRL_238);
                mMen31Txt.setText(AppConstants.GIRL_239);
                mMen32Txt.setText(AppConstants.GIRL_240);
                mMen33Txt.setText(AppConstants.GIRL_241);
                mMen34Txt.setText(AppConstants.GIRL_242);
                mMen35Txt.setText(AppConstants.GIRL_243);
                mMen36Txt.setText(AppConstants.GIRL_244);
                mMen37Txt.setText(AppConstants.GIRL_245);
                mMen38Txt.setText(AppConstants.GIRL_246);
                mMen39Txt.setText(AppConstants.GIRL_247);
                mMen40Txt.setText(AppConstants.GIRL_248);
                mMen41Txt.setText(AppConstants.GIRL_249);
                mMen42Txt.setText(AppConstants.GIRL_250);
                mMen43Txt.setText(AppConstants.GIRL_251);
                mMen44Txt.setText(AppConstants.GIRL_252);
                mMen45Txt.setText(AppConstants.GIRL_253);
                mMen46Txt.setText(AppConstants.GIRL_254);
                mMen47Txt.setText(AppConstants.GIRL_255);
                mMen48Txt.setText(AppConstants.GIRL_256);
                mMen49Txt.setText(AppConstants.GIRL_257);
                mMen50Txt.setText(AppConstants.GIRL_258);
                mMen51Txt.setText(AppConstants.GIRL_259);
                mMen52Txt.setText(AppConstants.GIRL_260);
                mMen53Txt.setText(AppConstants.GIRL_261);
                mMen54Txt.setText(AppConstants.GIRL_262);
                mMen55Txt.setText(AppConstants.GIRL_263);
                mMen56Txt.setText(AppConstants.GIRL_264);
                mMen57Txt.setText(AppConstants.GIRL_265);
                mMen58Txt.setText(AppConstants.GIRL_266);
                mMen59Txt.setText(AppConstants.GIRL_267);
                mMen60Txt.setText(AppConstants.GIRL_268);
                mMen61Txt.setText(AppConstants.GIRL_269);
                mMen62Txt.setText(AppConstants.GIRL_270);
                mMen63Txt.setText(AppConstants.GIRL_271);
                mMen64Txt.setText(AppConstants.GIRL_272);
                mWomen65Txt.setText(AppConstants.GIRL_273);
                mWomen66Txt.setText(AppConstants.GIRL_274);
                mWomen67Txt.setText(AppConstants.GIRL_275);
                mWomen68Txt.setText(AppConstants.GIRL_276);
                mWomen69Txt.setText(AppConstants.GIRL_277);
                mWomen70Txt.setText(AppConstants.GIRL_278);
                mWomen71Txt.setText(AppConstants.GIRL_279);
                mWomen72Txt.setText(AppConstants.GIRL_280);
                mWomen73Txt.setText(AppConstants.GIRL_281);
                mWomen74Txt.setText(AppConstants.GIRL_282);
                mWomen75Txt.setText(AppConstants.GIRL_283);
                mWomen76Txt.setText(AppConstants.GIRL_284);
                mWomen77Txt.setText(AppConstants.GIRL_285);
                mWomen78Txt.setText(AppConstants.GIRL_286);
                mWomen79Txt.setText(AppConstants.GIRL_287);
                mWomen80Txt.setText(AppConstants.GIRL_288);

            }
        }else if(AppConstants.UNITS.equalsIgnoreCase("IN")) {
            if (AppConstants.GENDER_ID.equalsIgnoreCase("1")) {
                mMen1Txt.setText(AppConstants.MEN_1_INCHES);
                mMen2Txt.setText(AppConstants.MEN_2_INCHES);
                mMen3Txt.setText(AppConstants.MEN_3_INCHES);
                mMen4Txt.setText(AppConstants.MEN_4_INCHES);
                mMen5Txt.setText(AppConstants.MEN_5_INCHES);
                mMen6Txt.setText(AppConstants.MEN_6_INCHES);
                mMen7Txt.setText(AppConstants.MEN_7_INCHES);
                mMen8Txt.setText(AppConstants.MEN_8_INCHES);
                mMen9Txt.setText(AppConstants.MEN_9_INCHES);
                mMen10Txt.setText(AppConstants.MEN_10_INCHES);
                mMen11Txt.setText(AppConstants.MEN_11_INCHES);
                mMen12Txt.setText(AppConstants.MEN_12_INCHES);
                mMen13Txt.setText(AppConstants.MEN_13_INCHES);
                mMen14Txt.setText(AppConstants.MEN_14_INCHES);
                mMen15Txt.setText(AppConstants.MEN_15_INCHES);
                mMen16Txt.setText(AppConstants.MEN_16_INCHES);
                mMen17Txt.setText(AppConstants.MEN_17_INCHES);
                mMen18Txt.setText(AppConstants.MEN_18_INCHES);
                mMen19Txt.setText(AppConstants.MEN_19_INCHES);
                mMen20Txt.setText(AppConstants.MEN_20_INCHES);
                mMen21Txt.setText(AppConstants.MEN_21_INCHES);
                mMen22Txt.setText(AppConstants.MEN_22_INCHES);
                mMen23Txt.setText(AppConstants.MEN_23_INCHES);
                mMen24Txt.setText(AppConstants.MEN_24_INCHES);
                mMen25Txt.setText(AppConstants.MEN_25_INCHES);
                mMen26Txt.setText(AppConstants.MEN_26_INCHES);
                mMen27Txt.setText(AppConstants.MEN_27_INCHES);
                mMen28Txt.setText(AppConstants.MEN_28_INCHES);
                mMen29Txt.setText(AppConstants.MEN_29_INCHES);
                mMen30Txt.setText(AppConstants.MEN_30_INCHES);
                mMen31Txt.setText(AppConstants.MEN_31_INCHES);
                mMen32Txt.setText(AppConstants.MEN_32_INCHES);
                mMen33Txt.setText(AppConstants.MEN_33_INCHES);
                mMen34Txt.setText(AppConstants.MEN_34_INCHES);
                mMen35Txt.setText(AppConstants.MEN_35_INCHES);
                mMen36Txt.setText(AppConstants.MEN_36_INCHES);
                mMen37Txt.setText(AppConstants.MEN_37_INCHES);
                mMen38Txt.setText(AppConstants.MEN_38_INCHES);
                mMen39Txt.setText(AppConstants.MEN_39_INCHES);
                mMen40Txt.setText(AppConstants.MEN_40_INCHES);
                mMen41Txt.setText(AppConstants.MEN_41_INCHES);
                mMen42Txt.setText(AppConstants.MEN_42_INCHES);
                mMen43Txt.setText(AppConstants.MEN_43_INCHES);
                mMen44Txt.setText(AppConstants.MEN_44_INCHES);
                mMen45Txt.setText(AppConstants.MEN_45_INCHES);
                mMen46Txt.setText(AppConstants.MEN_46_INCHES);
                mMen47Txt.setText(AppConstants.MEN_47_INCHES);
                mMen48Txt.setText(AppConstants.MEN_48_INCHES);
                mMen49Txt.setText(AppConstants.MEN_49_INCHES);
                mMen50Txt.setText(AppConstants.MEN_50_INCHES);
                mMen51Txt.setText(AppConstants.MEN_51_INCHES);
                mMen52Txt.setText(AppConstants.MEN_52_INCHES);
                mMen53Txt.setText(AppConstants.MEN_53_INCHES);
                mMen54Txt.setText(AppConstants.MEN_54_INCHES);
                mMen55Txt.setText(AppConstants.MEN_55_INCHES);
                mMen56Txt.setText(AppConstants.MEN_56_INCHES);
                mMen57Txt.setText(AppConstants.MEN_57_INCHES);
                mMen58Txt.setText(AppConstants.MEN_58_INCHES);
                mMen59Txt.setText(AppConstants.MEN_59_INCHES);
                mMen60Txt.setText(AppConstants.MEN_60_INCHES);
                mMen61Txt.setText(AppConstants.MEN_61_INCHES);
                mMen62Txt.setText(AppConstants.MEN_62_INCHES);
                mMen63Txt.setText(AppConstants.MEN_63_INCHES);
                mMen64Txt.setText(AppConstants.MEN_64_INCHES);

            }
            else if (AppConstants.GENDER_ID.equalsIgnoreCase("2")){

                mMen1Txt.setText(AppConstants.WOMEN_65_INCHES);
                mMen2Txt.setText(AppConstants.WOMEN_66_INCHES);
                mMen3Txt.setText(AppConstants.WOMEN_67_INCHES);
                mMen4Txt.setText(AppConstants.WOMEN_68_INCHES);
                mMen5Txt.setText(AppConstants.WOMEN_69_INCHES);
                mMen6Txt.setText(AppConstants.WOMEN_70_INCHES);
                mMen7Txt.setText(AppConstants.WOMEN_71_INCHES);
                mMen8Txt.setText(AppConstants.WOMEN_72_INCHES);
                mMen9Txt.setText(AppConstants.WOMEN_73_INCHES);
                mMen10Txt.setText(AppConstants.WOMEN_74_INCHES);
                mMen11Txt.setText(AppConstants.WOMEN_75_INCHES);
                mMen12Txt.setText(AppConstants.WOMEN_76_INCHES);
                mMen13Txt.setText(AppConstants.WOMEN_77_INCHES);
                mMen14Txt.setText(AppConstants.WOMEN_78_INCHES);
                mMen15Txt.setText(AppConstants.WOMEN_79_INCHES);
                mMen16Txt.setText(AppConstants.WOMEN_80_INCHES);
                mMen17Txt.setText(AppConstants.WOMEN_81_INCHES);
                mMen18Txt.setText(AppConstants.WOMEN_82_INCHES);
                mMen19Txt.setText(AppConstants.WOMEN_83_INCHES);
                mMen20Txt.setText(AppConstants.WOMEN_84_INCHES);
                mMen21Txt.setText(AppConstants.WOMEN_85_INCHES);
                mMen22Txt.setText(AppConstants.WOMEN_86_INCHES);
                mMen23Txt.setText(AppConstants.WOMEN_87_INCHES);
                mMen24Txt.setText(AppConstants.WOMEN_88_INCHES);
                mMen25Txt.setText(AppConstants.WOMEN_89_INCHES);
                mMen26Txt.setText(AppConstants.WOMEN_90_INCHES);
                mMen27Txt.setText(AppConstants.WOMEN_91_INCHES);
                mMen28Txt.setText(AppConstants.WOMEN_92_INCHES);
                mMen29Txt.setText(AppConstants.WOMEN_93_INCHES);
                mMen30Txt.setText(AppConstants.WOMEN_94_INCHES);
                mMen31Txt.setText(AppConstants.WOMEN_95_INCHES);
                mMen32Txt.setText(AppConstants.WOMEN_96_INCHES);
                mMen33Txt.setText(AppConstants.WOMEN_97_INCHES);
                mMen34Txt.setText(AppConstants.WOMEN_98_INCHES);
                mMen35Txt.setText(AppConstants.WOMEN_99_INCHES);
                mMen36Txt.setText(AppConstants.WOMEN_100_INCHES);
                mMen37Txt.setText(AppConstants.WOMEN_101_INCHES);
                mMen38Txt.setText(AppConstants.WOMEN_102_INCHES);
                mMen39Txt.setText(AppConstants.WOMEN_103_INCHES);
                mMen40Txt.setText(AppConstants.WOMEN_104_INCHES);
                mMen41Txt.setText(AppConstants.WOMEN_105_INCHES);
                mMen42Txt.setText(AppConstants.WOMEN_106_INCHES);
                mMen43Txt.setText(AppConstants.WOMEN_107_INCHES);
                mMen44Txt.setText(AppConstants.WOMEN_108_INCHES);
                mMen45Txt.setText(AppConstants.WOMEN_109_INCHES);
                mMen46Txt.setText(AppConstants.WOMEN_110_INCHES);
                mMen47Txt.setText(AppConstants.WOMEN_111_INCHES);
                mMen48Txt.setText(AppConstants.WOMEN_112_INCHES);
                mMen49Txt.setText(AppConstants.WOMEN_113_INCHES);
                mMen50Txt.setText(AppConstants.WOMEN_114_INCHES);
                mMen51Txt.setText(AppConstants.WOMEN_115_INCHES);
                mMen52Txt.setText(AppConstants.WOMEN_116_INCHES);
                mMen53Txt.setText(AppConstants.WOMEN_117_INCHES);
                mMen54Txt.setText(AppConstants.WOMEN_118_INCHES);
                mMen55Txt.setText(AppConstants.WOMEN_119_INCHES);
                mMen56Txt.setText(AppConstants.WOMEN_120_INCHES);
                mMen57Txt.setText(AppConstants.WOMEN_121_INCHES);
                mMen58Txt.setText(AppConstants.WOMEN_122_INCHES);
                mMen59Txt.setText(AppConstants.WOMEN_123_INCHES);
                mMen60Txt.setText(AppConstants.WOMEN_124_INCHES);
                mMen61Txt.setText(AppConstants.WOMEN_125_INCHES);
                mMen62Txt.setText(AppConstants.WOMEN_126_INCHES);
                mMen63Txt.setText(AppConstants.WOMEN_127_INCHES);
                mMen64Txt.setText(AppConstants.WOMEN_128_INCHES);
                mWomen65Txt.setText(AppConstants.WOMEN_129_INCHES);
                mWomen66Txt.setText(AppConstants.WOMEN_130_INCHES);
                mWomen67Txt.setText(AppConstants.WOMEN_131_INCHES);
                mWomen68Txt.setText(AppConstants.WOMEN_132_INCHES);
                mWomen69Txt.setText(AppConstants.WOMEN_133_INCHES);
                mWomen70Txt.setText(AppConstants.WOMEN_134_INCHES);
                mWomen71Txt.setText(AppConstants.WOMEN_135_INCHES);
                mWomen72Txt.setText(AppConstants.WOMEN_136_INCHES);
                mWomen73Txt.setText(AppConstants.WOMEN_137_INCHES);
                mWomen74Txt.setText(AppConstants.WOMEN_138_INCHES);
                mWomen75Txt.setText(AppConstants.WOMEN_139_INCHES);
                mWomen76Txt.setText(AppConstants.WOMEN_140_INCHES);
                mWomen77Txt.setText(AppConstants.WOMEN_141_INCHES);
                mWomen78Txt.setText(AppConstants.WOMEN_142_INCHES);
                mWomen79Txt.setText(AppConstants.WOMEN_143_INCHES);
                mWomen80Txt.setText(AppConstants.WOMEN_144_INCHES);

            }
            else if (AppConstants.GENDER_ID.equalsIgnoreCase("3")) {
                mMen1Txt.setText(AppConstants.BOY_145_INCHES);
                mMen2Txt.setText(AppConstants.BOY_146_INCHES);
                mMen3Txt.setText(AppConstants.BOY_147_INCHES);
                mMen4Txt.setText(AppConstants.BOY_148_INCHES);
                mMen5Txt.setText(AppConstants.BOY_149_INCHES);
                mMen6Txt.setText(AppConstants.BOY_150_INCHES);
                mMen7Txt.setText(AppConstants.BOY_151_INCHES);
                mMen8Txt.setText(AppConstants.BOY_152_INCHES);
                mMen9Txt.setText(AppConstants.BOY_153_INCHES);
                mMen10Txt.setText(AppConstants.BOY_154_INCHES);
                mMen11Txt.setText(AppConstants.BOY_155_INCHES);
                mMen12Txt.setText(AppConstants.BOY_156_INCHES);
                mMen13Txt.setText(AppConstants.BOY_157_INCHES);
                mMen14Txt.setText(AppConstants.BOY_158_INCHES);
                mMen15Txt.setText(AppConstants.BOY_159_INCHES);
                mMen16Txt.setText(AppConstants.BOY_160_INCHES);
                mMen17Txt.setText(AppConstants.BOY_161_INCHES);
                mMen18Txt.setText(AppConstants.BOY_162_INCHES);
                mMen19Txt.setText(AppConstants.BOY_163_INCHES);
                mMen20Txt.setText(AppConstants.BOY_164_INCHES);
                mMen21Txt.setText(AppConstants.BOY_165_INCHES);
                mMen22Txt.setText(AppConstants.BOY_166_INCHES);
                mMen23Txt.setText(AppConstants.BOY_167_INCHES);
                mMen24Txt.setText(AppConstants.BOY_168_INCHES);
                mMen25Txt.setText(AppConstants.BOY_169_INCHES);
                mMen26Txt.setText(AppConstants.BOY_170_INCHES);
                mMen27Txt.setText(AppConstants.BOY_171_INCHES);
                mMen28Txt.setText(AppConstants.BOY_172_INCHES);
                mMen29Txt.setText(AppConstants.BOY_173_INCHES);
                mMen30Txt.setText(AppConstants.BOY_174_INCHES);
                mMen31Txt.setText(AppConstants.BOY_175_INCHES);
                mMen32Txt.setText(AppConstants.BOY_176_INCHES);
                mMen33Txt.setText(AppConstants.BOY_177_INCHES);
                mMen34Txt.setText(AppConstants.BOY_178_INCHES);
                mMen35Txt.setText(AppConstants.BOY_179_INCHES);
                mMen36Txt.setText(AppConstants.BOY_180_INCHES);
                mMen37Txt.setText(AppConstants.BOY_181_INCHES);
                mMen38Txt.setText(AppConstants.BOY_182_INCHES);
                mMen39Txt.setText(AppConstants.BOY_183_INCHES);
                mMen40Txt.setText(AppConstants.BOY_184_INCHES);
                mMen41Txt.setText(AppConstants.BOY_185_INCHES);
                mMen42Txt.setText(AppConstants.BOY_186_INCHES);
                mMen43Txt.setText(AppConstants.BOY_187_INCHES);
                mMen44Txt.setText(AppConstants.BOY_188_INCHES);
                mMen45Txt.setText(AppConstants.BOY_189_INCHES);
                mMen46Txt.setText(AppConstants.BOY_190_INCHES);
                mMen47Txt.setText(AppConstants.BOY_191_INCHES);
                mMen48Txt.setText(AppConstants.BOY_192_INCHES);
                mMen49Txt.setText(AppConstants.BOY_193_INCHES);
                mMen50Txt.setText(AppConstants.BOY_194_INCHES);
                mMen51Txt.setText(AppConstants.BOY_195_INCHES);
                mMen52Txt.setText(AppConstants.BOY_196_INCHES);
                mMen53Txt.setText(AppConstants.BOY_197_INCHES);
                mMen54Txt.setText(AppConstants.BOY_198_INCHES);
                mMen55Txt.setText(AppConstants.BOY_199_INCHES);
                mMen56Txt.setText(AppConstants.BOY_200_INCHES);
                mMen57Txt.setText(AppConstants.BOY_201_INCHES);
                mMen58Txt.setText(AppConstants.BOY_202_INCHES);
                mMen59Txt.setText(AppConstants.BOY_203_INCHES);
                mMen60Txt.setText(AppConstants.BOY_204_INCHES);
                mMen61Txt.setText(AppConstants.BOY_205_INCHES);
                mMen62Txt.setText(AppConstants.BOY_206_INCHES);
                mMen63Txt.setText(AppConstants.BOY_207_INCHES);
                mMen64Txt.setText(AppConstants.BOY_208_INCHES);

            }
            else if (AppConstants.GENDER_ID.equalsIgnoreCase("4")) {
                mMen1Txt.setText(AppConstants.GIRL_209_INCHES);
                mMen2Txt.setText(AppConstants.GIRL_210_INCHES);
                mMen3Txt.setText(AppConstants.GIRL_211_INCHES);
                mMen4Txt.setText(AppConstants.GIRL_212_INCHES);
                mMen5Txt.setText(AppConstants.GIRL_213_INCHES);
                mMen6Txt.setText(AppConstants.GIRL_214_INCHES);
                mMen7Txt.setText(AppConstants.GIRL_215_INCHES);
                mMen8Txt.setText(AppConstants.GIRL_216_INCHES);
                mMen9Txt.setText(AppConstants.GIRL_217_INCHES);
                mMen10Txt.setText(AppConstants.GIRL_218_INCHES);
                mMen11Txt.setText(AppConstants.GIRL_219_INCHES);
                mMen12Txt.setText(AppConstants.GIRL_220_INCHES);
                mMen13Txt.setText(AppConstants.GIRL_221_INCHES);
                mMen14Txt.setText(AppConstants.GIRL_222_INCHES);
                mMen15Txt.setText(AppConstants.GIRL_223_INCHES);
                mMen16Txt.setText(AppConstants.GIRL_224_INCHES);
                mMen17Txt.setText(AppConstants.GIRL_225_INCHES);
                mMen18Txt.setText(AppConstants.GIRL_226_INCHES);
                mMen19Txt.setText(AppConstants.GIRL_227_INCHES);
                mMen20Txt.setText(AppConstants.GIRL_228_INCHES);
                mMen21Txt.setText(AppConstants.GIRL_229_INCHES);
                mMen22Txt.setText(AppConstants.GIRL_230_INCHES);
                mMen23Txt.setText(AppConstants.GIRL_231_INCHES);
                mMen24Txt.setText(AppConstants.GIRL_232_INCHES);
                mMen25Txt.setText(AppConstants.GIRL_233_INCHES);
                mMen26Txt.setText(AppConstants.GIRL_234_INCHES);
                mMen27Txt.setText(AppConstants.GIRL_235_INCHES);
                mMen28Txt.setText(AppConstants.GIRL_236_INCHES);
                mMen29Txt.setText(AppConstants.GIRL_237_INCHES);
                mMen30Txt.setText(AppConstants.GIRL_238_INCHES);
                mMen31Txt.setText(AppConstants.GIRL_239_INCHES);
                mMen32Txt.setText(AppConstants.GIRL_240_INCHES);
                mMen33Txt.setText(AppConstants.GIRL_241_INCHES);
                mMen34Txt.setText(AppConstants.GIRL_242_INCHES);
                mMen35Txt.setText(AppConstants.GIRL_243_INCHES);
                mMen36Txt.setText(AppConstants.GIRL_244_INCHES);
                mMen37Txt.setText(AppConstants.GIRL_245_INCHES);
                mMen38Txt.setText(AppConstants.GIRL_246_INCHES);
                mMen39Txt.setText(AppConstants.GIRL_247_INCHES);
                mMen40Txt.setText(AppConstants.GIRL_248_INCHES);
                mMen41Txt.setText(AppConstants.GIRL_249_INCHES);
                mMen42Txt.setText(AppConstants.GIRL_250_INCHES);
                mMen43Txt.setText(AppConstants.GIRL_251_INCHES);
                mMen44Txt.setText(AppConstants.GIRL_252_INCHES);
                mMen45Txt.setText(AppConstants.GIRL_253_INCHES);
                mMen46Txt.setText(AppConstants.GIRL_254_INCHES);
                mMen47Txt.setText(AppConstants.GIRL_255_INCHES);
                mMen48Txt.setText(AppConstants.GIRL_256_INCHES);
                mMen49Txt.setText(AppConstants.GIRL_257_INCHES);
                mMen50Txt.setText(AppConstants.GIRL_258_INCHES);
                mMen51Txt.setText(AppConstants.GIRL_259_INCHES);
                mMen52Txt.setText(AppConstants.GIRL_260_INCHES);
                mMen53Txt.setText(AppConstants.GIRL_261_INCHES);
                mMen54Txt.setText(AppConstants.GIRL_262_INCHES);
                mMen55Txt.setText(AppConstants.GIRL_263_INCHES);
                mMen56Txt.setText(AppConstants.GIRL_264_INCHES);
                mMen57Txt.setText(AppConstants.GIRL_265_INCHES);
                mMen58Txt.setText(AppConstants.GIRL_266_INCHES);
                mMen59Txt.setText(AppConstants.GIRL_267_INCHES);
                mMen60Txt.setText(AppConstants.GIRL_268_INCHES);
                mMen61Txt.setText(AppConstants.GIRL_269_INCHES);
                mMen62Txt.setText(AppConstants.GIRL_270_INCHES);
                mMen63Txt.setText(AppConstants.GIRL_271_INCHES);
                mMen64Txt.setText(AppConstants.GIRL_272_INCHES);
                mWomen65Txt.setText(AppConstants.GIRL_273_INCHES);
                mWomen66Txt.setText(AppConstants.GIRL_274_INCHES);
                mWomen67Txt.setText(AppConstants.GIRL_275_INCHES);
                mWomen68Txt.setText(AppConstants.GIRL_276_INCHES);
                mWomen69Txt.setText(AppConstants.GIRL_277_INCHES);
                mWomen70Txt.setText(AppConstants.GIRL_278_INCHES);
                mWomen71Txt.setText(AppConstants.GIRL_279_INCHES);
                mWomen72Txt.setText(AppConstants.GIRL_280_INCHES);
                mWomen73Txt.setText(AppConstants.GIRL_281_INCHES);
                mWomen74Txt.setText(AppConstants.GIRL_282_INCHES);
                mWomen75Txt.setText(AppConstants.GIRL_283_INCHES);
                mWomen76Txt.setText(AppConstants.GIRL_284_INCHES);
                mWomen77Txt.setText(AppConstants.GIRL_285_INCHES);
                mWomen78Txt.setText(AppConstants.GIRL_286_INCHES);
                mWomen79Txt.setText(AppConstants.GIRL_287_INCHES);
                mWomen80Txt.setText(AppConstants.GIRL_288_INCHES);
            }
        }

        mMen1ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 1||mParts.get(i).getId() == 65||mParts.get(i).getId() == 145||mParts.get(i).getId() == 209) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen1Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 1){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_1;
                            }else if(mParts.get(i).getId() == 65){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_65;
                            }else if (mParts.get(i).getId() == 145){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_145;
                            }else if (mParts.get(i).getId() == 209){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_209;
                            }

                        }else {
                            if(mParts.get(i).getId() == 1){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_1_INCHES;
                            }else if(mParts.get(i).getId() == 65){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_65_INCHES;
                            }else if (mParts.get(i).getId() == 145){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_145_INCHES;
                            }else if (mParts.get(i).getId() == 209){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_209_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });

        mMen2ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 2||mParts.get(i).getId() == 66||mParts.get(i).getId() == 146||mParts.get(i).getId() == 210) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen2Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                          if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 2){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_2;
                            }else if(mParts.get(i).getId() == 66){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_66;
                            }else if (mParts.get(i).getId() == 146){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_146;
                            }else if (mParts.get(i).getId() == 210){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_210;
                            }

                        }else {
                            if(mParts.get(i).getId() == 2){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_2_INCHES;
                            }else if(mParts.get(i).getId() == 66){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_66_INCHES;
                            }else if (mParts.get(i).getId() == 146){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_146_INCHES;
                            }else if (mParts.get(i).getId() == 210){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_210_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen3ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 3||mParts.get(i).getId() == 67||mParts.get(i).getId() == 147||mParts.get(i).getId() == 211) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen3Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 3){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_3;
                            }else if(mParts.get(i).getId() == 67){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_67;
                            }else if (mParts.get(i).getId() == 147){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_147;
                            }else if (mParts.get(i).getId() == 211){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_211;
                            }

                        }else {
                            if(mParts.get(i).getId() == 3){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_3_INCHES;
                            }else if(mParts.get(i).getId() == 67){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_67_INCHES;
                            }else if (mParts.get(i).getId() == 147){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_147_INCHES;
                            }else if (mParts.get(i).getId() == 211){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_211_INCHES;
                            }

                        }
                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen4ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 4||mParts.get(i).getId() == 68||mParts.get(i).getId() == 148||mParts.get(i).getId() == 212) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen4Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 4){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_4;
                            }else if(mParts.get(i).getId() == 68){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_68;
                            }else if (mParts.get(i).getId() == 148){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_148;
                            }else if (mParts.get(i).getId() == 212){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_212;
                            }

                        }else {
                            if(mParts.get(i).getId() == 4){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_4_INCHES;
                            }else if(mParts.get(i).getId() == 68){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_68_INCHES;
                            }else if (mParts.get(i).getId() == 148){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_148_INCHES;
                            }else if (mParts.get(i).getId() == 212){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_212_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen5ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 5||mParts.get(i).getId() == 69||mParts.get(i).getId() == 149||mParts.get(i).getId() == 213) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen5Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 5){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_5;
                            }else if(mParts.get(i).getId() == 69){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_69;
                            }else if (mParts.get(i).getId() == 149){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_149;
                            }else if (mParts.get(i).getId() == 213){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_213;
                            }

                        }else {
                            if(mParts.get(i).getId() == 5){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_5_INCHES;
                            }else if(mParts.get(i).getId() == 69){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_69_INCHES;
                            }else if (mParts.get(i).getId() == 149){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_149_INCHES;
                            }else if (mParts.get(i).getId() == 213){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_213_INCHES;
                            }

                        }
                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen6ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 6||mParts.get(i).getId() == 70||mParts.get(i).getId() == 150||mParts.get(i).getId() == 214) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen6Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 6){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_6;
                            }else if(mParts.get(i).getId() == 70){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_70;
                            }else if (mParts.get(i).getId() == 150){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_150;
                            }else if (mParts.get(i).getId() == 214){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_214;
                            }

                        }else {
                            if(mParts.get(i).getId() == 6){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_6_INCHES;
                            }else if(mParts.get(i).getId() == 70){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_70_INCHES;
                            }else if (mParts.get(i).getId() == 150){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_150_INCHES;
                            }else if (mParts.get(i).getId() == 214){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_214_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen7ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 7||mParts.get(i).getId() == 71||mParts.get(i).getId() == 151||mParts.get(i).getId() == 215) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen7Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 7){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_7;
                            }else if(mParts.get(i).getId() == 71){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_71;
                            }else if (mParts.get(i).getId() == 151){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_151;
                            }else if (mParts.get(i).getId() == 215){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_215;
                            }

                        }else {
                            if(mParts.get(i).getId() == 7){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_7_INCHES;
                            }else if(mParts.get(i).getId() == 71){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_71_INCHES;
                            }else if (mParts.get(i).getId() == 151){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_151_INCHES;
                            }else if (mParts.get(i).getId() == 215){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_215_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen8ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 8||mParts.get(i).getId() == 72||mParts.get(i).getId() == 152||mParts.get(i).getId() == 216) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen8Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 8){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_8;
                            }else if(mParts.get(i).getId() == 72){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_72;
                            }else if (mParts.get(i).getId() == 152){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_152;
                            }else if (mParts.get(i).getId() == 216){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_216;
                            }

                        }else {
                            if(mParts.get(i).getId() == 8){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_8_INCHES;
                            }else if(mParts.get(i).getId() == 72){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_72_INCHES;
                            }else if (mParts.get(i).getId() == 152){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_152_INCHES;
                            }else if (mParts.get(i).getId() == 216){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_216_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen9ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 9||mParts.get(i).getId() == 73||mParts.get(i).getId() == 153||mParts.get(i).getId() == 217) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen9Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 9){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_9;
                            }else if(mParts.get(i).getId() == 73){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_73;
                            }else if (mParts.get(i).getId() == 153){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_153;
                            }else if (mParts.get(i).getId() == 217){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_217;
                            }

                        }else {
                            if(mParts.get(i).getId() == 9){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_9_INCHES;
                            }else if(mParts.get(i).getId() == 73){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_73_INCHES;
                            }else if (mParts.get(i).getId() == 153){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_153_INCHES;
                            }else if (mParts.get(i).getId() == 217){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_217_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen10ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 10||mParts.get(i).getId() == 74||mParts.get(i).getId() == 154||mParts.get(i).getId() == 218) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen10Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 10){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_10;
                            }else if(mParts.get(i).getId() == 74){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_74;
                            }else if (mParts.get(i).getId() == 154){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_154;
                            }else if (mParts.get(i).getId() == 218){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_218;
                            }

                        }else {
                            if(mParts.get(i).getId() == 10){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_10_INCHES;
                            }else if(mParts.get(i).getId() == 74){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_74_INCHES;
                            }else if (mParts.get(i).getId() == 154){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_154_INCHES;
                            }else if (mParts.get(i).getId() == 218){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_218_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen11ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 11||mParts.get(i).getId() == 75||mParts.get(i).getId() == 155||mParts.get(i).getId() == 219) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen11Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 11){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_11;
                            }else if(mParts.get(i).getId() == 75){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_75;
                            }else if (mParts.get(i).getId() == 155){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_155;
                            }else if (mParts.get(i).getId() == 219){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_219;
                            }

                        }else {
                            if(mParts.get(i).getId() == 11){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_11_INCHES;
                            }else if(mParts.get(i).getId() == 75){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_75_INCHES;
                            }else if (mParts.get(i).getId() == 155){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_155_INCHES;
                            }else if (mParts.get(i).getId() == 219){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_219_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen12ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 12||mParts.get(i).getId() == 76||mParts.get(i).getId() == 156||mParts.get(i).getId() == 220) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen12Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 12){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_12;
                            }else if(mParts.get(i).getId() == 76){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_76;
                            }else if (mParts.get(i).getId() == 156){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_156;
                            }else if (mParts.get(i).getId() == 220){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_220;
                            }

                        }else {
                            if(mParts.get(i).getId() == 12){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_12_INCHES;
                            }else if(mParts.get(i).getId() == 76){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_76_INCHES;
                            }else if (mParts.get(i).getId() == 156){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_156_INCHES;
                            }else if (mParts.get(i).getId() == 220){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_220_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen13ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 13||mParts.get(i).getId() == 77||mParts.get(i).getId() == 157||mParts.get(i).getId() == 221) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen13Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 13){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_13;
                            }else if(mParts.get(i).getId() == 77){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_77;
                            }else if (mParts.get(i).getId() == 157){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_157;
                            }else if (mParts.get(i).getId() == 221){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_221;
                            }

                        }else {
                            if(mParts.get(i).getId() == 13){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_13_INCHES;
                            }else if(mParts.get(i).getId() == 77){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_77_INCHES;
                            }else if (mParts.get(i).getId() == 157){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_157_INCHES;
                            }else if (mParts.get(i).getId() == 221){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_221_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen14ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 14||mParts.get(i).getId() == 78||mParts.get(i).getId() == 158||mParts.get(i).getId() == 222) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen14Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 14){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_14;
                            }else if(mParts.get(i).getId() == 78){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_78;
                            }else if (mParts.get(i).getId() == 158){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_158;
                            }else if (mParts.get(i).getId() == 222){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_222;
                            }

                        }else {
                            if(mParts.get(i).getId() == 14){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_14_INCHES;
                            }else if(mParts.get(i).getId() == 78){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_78_INCHES;
                            }else if (mParts.get(i).getId() == 158){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_158_INCHES;
                            }else if (mParts.get(i).getId() == 222){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_222_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen15ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 15||mParts.get(i).getId() == 79||mParts.get(i).getId() == 159||mParts.get(i).getId() == 223) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen15Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 15){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_15;
                            }else if(mParts.get(i).getId() == 79){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_79;
                            }else if (mParts.get(i).getId() == 159){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_159;
                            }else if (mParts.get(i).getId() == 223){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_223;
                            }

                        }else {
                            if(mParts.get(i).getId() == 15){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_15_INCHES;
                            }else if(mParts.get(i).getId() == 79){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_79_INCHES;
                            }else if (mParts.get(i).getId() == 159){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_159_INCHES;
                            }else if (mParts.get(i).getId() == 223){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_223_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen16ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 16||mParts.get(i).getId() == 80||mParts.get(i).getId() == 160||mParts.get(i).getId() == 224) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen16Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 16){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_16;
                            }else if(mParts.get(i).getId() == 80){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_80;
                            }else if (mParts.get(i).getId() == 160){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_160;
                            }else if (mParts.get(i).getId() == 224){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_224;
                            }

                        }else {
                            if(mParts.get(i).getId() == 16){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_16_INCHES;
                            }else if(mParts.get(i).getId() == 80){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_80_INCHES;
                            }else if (mParts.get(i).getId() == 160){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_160_INCHES;
                            }else if (mParts.get(i).getId() == 224){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_224_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen17ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 17||mParts.get(i).getId() == 81||mParts.get(i).getId() == 161||mParts.get(i).getId() == 225) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen17Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
//
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 17){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_17;
                            }else if(mParts.get(i).getId() == 81){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_81;
                            }else if (mParts.get(i).getId() == 161){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_161;
                            }else if (mParts.get(i).getId() == 225){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_225;
                            }

                        }else {
                            if(mParts.get(i).getId() == 17){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_17_INCHES;
                            }else if(mParts.get(i).getId() == 81){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_81_INCHES;
                            }else if (mParts.get(i).getId() == 161){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_161_INCHES;
                            }else if (mParts.get(i).getId() == 225){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_225_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen18ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 18||mParts.get(i).getId() == 82||mParts.get(i).getId() == 162||mParts.get(i).getId() == 226) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen18Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
//
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 18){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_18;
                            }else if(mParts.get(i).getId() == 82){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_82;
                            }else if (mParts.get(i).getId() == 162){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_162;
                            }else if (mParts.get(i).getId() == 226){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_226;
                            }

                        }else {
                            if(mParts.get(i).getId() == 18){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_18_INCHES;
                            }else if(mParts.get(i).getId() == 82){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_82_INCHES;
                            }else if (mParts.get(i).getId() == 162){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_162_INCHES;
                            }else if (mParts.get(i).getId() == 226){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_226_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen19ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 19||mParts.get(i).getId() == 83||mParts.get(i).getId() == 163||mParts.get(i).getId() == 227) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen19Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 19){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_19;
                            }else if(mParts.get(i).getId() == 83){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_83;
                            }else if (mParts.get(i).getId() == 163){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_163;
                            }else if (mParts.get(i).getId() == 227){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_227;
                            }

                        }else {
                            if(mParts.get(i).getId() == 19){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_19_INCHES;
                            }else if(mParts.get(i).getId() == 83){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_83_INCHES;
                            }else if (mParts.get(i).getId() == 163){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_163_INCHES;
                            }else if (mParts.get(i).getId() == 227){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_227_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen20ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 20||mParts.get(i).getId() == 84||mParts.get(i).getId() == 164||mParts.get(i).getId() == 228) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen20Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 20){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_20;
                            }else if(mParts.get(i).getId() == 84){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_84;
                            }else if (mParts.get(i).getId() == 164){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_164;
                            }else if (mParts.get(i).getId() == 228){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_228;
                            }

                        }else {
                            if(mParts.get(i).getId() == 20){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_20_INCHES;
                            }else if(mParts.get(i).getId() == 84){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_84_INCHES;
                            }else if (mParts.get(i).getId() == 164){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_164_INCHES;
                            }else if (mParts.get(i).getId() == 228){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_228_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen21ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 21||mParts.get(i).getId() == 85||mParts.get(i).getId() == 165||mParts.get(i).getId() == 229) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen21Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 21){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_21;
                            }else if(mParts.get(i).getId() == 85){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_85;
                            }else if (mParts.get(i).getId() == 165){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_165;
                            }else if (mParts.get(i).getId() == 229){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_229;
                            }

                        }else {
                            if(mParts.get(i).getId() == 21){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_21_INCHES;
                            }else if(mParts.get(i).getId() == 85){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_85_INCHES;
                            }else if (mParts.get(i).getId() == 165){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_165_INCHES;
                            }else if (mParts.get(i).getId() == 229){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_229_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen22ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 22||mParts.get(i).getId() == 86||mParts.get(i).getId() == 166||mParts.get(i).getId() == 230) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen22Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 22){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_22;
                            }else if(mParts.get(i).getId() == 86){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_86;
                            }else if (mParts.get(i).getId() == 166){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_166;
                            }else if (mParts.get(i).getId() == 230){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_230;
                            }

                        }else {
                            if(mParts.get(i).getId() == 22){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_22_INCHES;
                            }else if(mParts.get(i).getId() == 86){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_86_INCHES;
                            }else if (mParts.get(i).getId() == 166){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_166_INCHES;
                            }else if (mParts.get(i).getId() == 230){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_230_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen23ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 23||mParts.get(i).getId() == 87||mParts.get(i).getId() == 167||mParts.get(i).getId() == 231) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen23Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 23){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_23;
                            }else if(mParts.get(i).getId() == 87){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_87;
                            }else if (mParts.get(i).getId() == 167){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_167;
                            }else if (mParts.get(i).getId() == 231){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_231;
                            }

                        }else {
                            if(mParts.get(i).getId() == 23){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_23_INCHES;
                            }else if(mParts.get(i).getId() == 87){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_87_INCHES;
                            }else if (mParts.get(i).getId() == 167){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_167_INCHES;
                            }else if (mParts.get(i).getId() == 231){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_231_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen24ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 24||mParts.get(i).getId() == 88||mParts.get(i).getId() == 168||mParts.get(i).getId() == 232) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen24Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 24){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_24;
                            }else if(mParts.get(i).getId() == 88){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_88;
                            }else if (mParts.get(i).getId() == 168){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_168;
                            }else if (mParts.get(i).getId() == 232){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_232;
                            }

                        }else {
                            if(mParts.get(i).getId() == 24){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_24_INCHES;
                            }else if(mParts.get(i).getId() == 88){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_88_INCHES;
                            }else if (mParts.get(i).getId() == 168){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_168_INCHES;
                            }else if (mParts.get(i).getId() == 232){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_232_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen25ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 25||mParts.get(i).getId() == 89||mParts.get(i).getId() == 169||mParts.get(i).getId() == 233) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen25Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 25){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_25;
                            }else if(mParts.get(i).getId() == 89){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_89;
                            }else if (mParts.get(i).getId() == 169){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_169;
                            }else if (mParts.get(i).getId() == 233){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_233;
                            }

                        }else {
                            if(mParts.get(i).getId() == 25){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_25_INCHES;
                            }else if(mParts.get(i).getId() == 89){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_89_INCHES;
                            }else if (mParts.get(i).getId() == 169){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_169_INCHES;
                            }else if (mParts.get(i).getId() == 233){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_233_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen26ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 26||mParts.get(i).getId() == 90||mParts.get(i).getId() == 170||mParts.get(i).getId() == 234) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen26Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 26){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_26;
                            }else if(mParts.get(i).getId() == 90){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_90;
                            }else if (mParts.get(i).getId() == 170){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_170;
                            }else if (mParts.get(i).getId() == 234){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_234;
                            }

                        }else {
                            if(mParts.get(i).getId() == 26){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_26_INCHES;
                            }else if(mParts.get(i).getId() == 90){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_90_INCHES;
                            }else if (mParts.get(i).getId() == 170){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_170_INCHES;
                            }else if (mParts.get(i).getId() == 234){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_234_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen27ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 27||mParts.get(i).getId() == 91||mParts.get(i).getId() == 171||mParts.get(i).getId() == 235) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen27Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 27){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_27;
                            }else if(mParts.get(i).getId() ==91){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_91;
                            }else if (mParts.get(i).getId() == 171){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_171;
                            }else if (mParts.get(i).getId() == 235){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_235;
                            }

                        }else {
                            if(mParts.get(i).getId() == 27){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_27_INCHES;
                            }else if(mParts.get(i).getId() == 91){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_91_INCHES;
                            }else if (mParts.get(i).getId() == 171){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_171_INCHES;
                            }else if (mParts.get(i).getId() == 235){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_235_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen28ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 28||mParts.get(i).getId() == 92||mParts.get(i).getId() == 172||mParts.get(i).getId() == 236) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen28Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 28){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_28;
                            }else if(mParts.get(i).getId() == 92){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_92;
                            }else if (mParts.get(i).getId() == 172){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_172;
                            }else if (mParts.get(i).getId() == 236){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_236;
                            }

                        }else {
                            if(mParts.get(i).getId() == 28){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_28_INCHES;
                            }else if(mParts.get(i).getId() == 92){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_92_INCHES;
                            }else if (mParts.get(i).getId() == 172){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_172_INCHES;
                            }else if (mParts.get(i).getId() == 236){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_236_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen29ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 29||mParts.get(i).getId() == 93||mParts.get(i).getId() == 173||mParts.get(i).getId() == 237) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen29Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 29){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_29;
                            }else if(mParts.get(i).getId() == 93){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_93;
                            }else if (mParts.get(i).getId() == 173){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_173;
                            }else if (mParts.get(i).getId() == 237){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_237;
                            }

                        }else {
                            if(mParts.get(i).getId() == 29){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_29_INCHES;
                            }else if(mParts.get(i).getId() == 93){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_93_INCHES;
                            }else if (mParts.get(i).getId() == 173){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_173_INCHES;
                            }else if (mParts.get(i).getId() == 237){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_237_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen30ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 30||mParts.get(i).getId() == 94||mParts.get(i).getId() == 174||mParts.get(i).getId() == 238) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen30Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 30){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_30;
                            }else if(mParts.get(i).getId() == 94){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_94;
                            }else if (mParts.get(i).getId() == 174){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_174;
                            }else if (mParts.get(i).getId() == 238){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_238;
                            }

                        }else {
                            if(mParts.get(i).getId() == 30){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_30_INCHES;
                            }else if(mParts.get(i).getId() == 94){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_94_INCHES;
                            }else if (mParts.get(i).getId() == 174){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_174_INCHES;
                            }else if (mParts.get(i).getId() == 238){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_238_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen31ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 31||mParts.get(i).getId() == 95||mParts.get(i).getId() == 175||mParts.get(i).getId() == 239) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen31Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 31){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_31;
                            }else if(mParts.get(i).getId() == 95){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_95;
                            }else if (mParts.get(i).getId() == 175){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_175;
                            }else if (mParts.get(i).getId() == 239){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_239;
                            }

                        }else {
                            if(mParts.get(i).getId() == 31){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_31_INCHES;
                            }else if(mParts.get(i).getId() == 95){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_95_INCHES;
                            }else if (mParts.get(i).getId() == 175){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_175_INCHES;
                            }else if (mParts.get(i).getId() == 239){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_239_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen32ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 32||mParts.get(i).getId() == 96||mParts.get(i).getId() == 176||mParts.get(i).getId() == 240) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen32Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 32){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_32;
                            }else if(mParts.get(i).getId() == 96){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_96;
                            }else if (mParts.get(i).getId() == 176){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_176;
                            }else if (mParts.get(i).getId() == 240){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_240;
                            }

                        }else {
                            if(mParts.get(i).getId() == 32){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_32_INCHES;
                            }else if(mParts.get(i).getId() == 96){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_96_INCHES;
                            }else if (mParts.get(i).getId() == 176){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_176_INCHES;
                            }else if (mParts.get(i).getId() == 240){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_240_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen33ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 33||mParts.get(i).getId() == 97||mParts.get(i).getId() == 177||mParts.get(i).getId() == 241) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen33Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 33){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_33;
                            }else if(mParts.get(i).getId() == 97){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_97;
                            }else if (mParts.get(i).getId() == 177){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_177;
                            }else if (mParts.get(i).getId() == 241){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_241;
                            }

                        }else {
                            if(mParts.get(i).getId() == 33){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_33_INCHES;
                            }else if(mParts.get(i).getId() == 97){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_97_INCHES;
                            }else if (mParts.get(i).getId() == 177){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_177_INCHES;
                            }else if (mParts.get(i).getId() == 241){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_241_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen34ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 34||mParts.get(i).getId() == 98||mParts.get(i).getId() == 178||mParts.get(i).getId() == 242) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen34Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 34){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_34;
                            }else if(mParts.get(i).getId() == 98){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_98;
                            }else if (mParts.get(i).getId() == 178){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_178;
                            }else if (mParts.get(i).getId() == 242){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_242;
                            }

                        }else {
                            if(mParts.get(i).getId() == 34){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_34_INCHES;
                            }else if(mParts.get(i).getId() == 98){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_98_INCHES;
                            }else if (mParts.get(i).getId() == 178){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_178_INCHES;
                            }else if (mParts.get(i).getId() == 242){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_242_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen35ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 35||mParts.get(i).getId() == 99||mParts.get(i).getId() == 179||mParts.get(i).getId() == 243) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen35Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 35){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_35;
                            }else if(mParts.get(i).getId() == 99){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_99;
                            }else if (mParts.get(i).getId() == 179){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_179;
                            }else if (mParts.get(i).getId() == 243){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_243;
                            }

                        }else {
                            if(mParts.get(i).getId() == 35){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_35_INCHES;
                            }else if(mParts.get(i).getId() == 99){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_99_INCHES;
                            }else if (mParts.get(i).getId() == 179){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_179_INCHES;
                            }else if (mParts.get(i).getId() == 243){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_243_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen36ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 36||mParts.get(i).getId() == 100||mParts.get(i).getId() == 180||mParts.get(i).getId() == 244) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen36Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 36){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_36;
                            }else if(mParts.get(i).getId() == 100){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_100;
                            }else if (mParts.get(i).getId() == 180){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_180;
                            }else if (mParts.get(i).getId() == 244){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_244;
                            }

                        }else {
                            if(mParts.get(i).getId() == 36){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_36_INCHES;
                            }else if(mParts.get(i).getId() == 100){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_100_INCHES;
                            }else if (mParts.get(i).getId() == 180){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_180_INCHES;
                            }else if (mParts.get(i).getId() == 244){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_244_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen37ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 37||mParts.get(i).getId() == 101||mParts.get(i).getId() == 181||mParts.get(i).getId() == 245) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen37Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 37){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_37;
                            }else if(mParts.get(i).getId() == 101){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_101;
                            }else if (mParts.get(i).getId() == 181){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_181;
                            }else if (mParts.get(i).getId() == 245){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_245;
                            }

                        }else {
                            if(mParts.get(i).getId() == 37){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_37_INCHES;
                            }else if(mParts.get(i).getId() == 101){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_101_INCHES;
                            }else if (mParts.get(i).getId() == 181){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_181_INCHES;
                            }else if (mParts.get(i).getId() == 245){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_245_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen38ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 38||mParts.get(i).getId() == 102||mParts.get(i).getId() == 182||mParts.get(i).getId() == 246) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen38Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 38){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_38;
                            }else if(mParts.get(i).getId() == 102){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_102;
                            }else if (mParts.get(i).getId() == 182){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_182;
                            }else if (mParts.get(i).getId() == 246){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_246;
                            }

                        }else {
                            if(mParts.get(i).getId() == 38){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_38_INCHES;
                            }else if(mParts.get(i).getId() == 102){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_102_INCHES;
                            }else if (mParts.get(i).getId() == 182){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_182_INCHES;
                            }else if (mParts.get(i).getId() == 246){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_246_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen39ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 39||mParts.get(i).getId() == 103||mParts.get(i).getId() == 183||mParts.get(i).getId() == 247) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen39Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 39){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_39;
                            }else if(mParts.get(i).getId() == 103){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_103;
                            }else if (mParts.get(i).getId() == 183){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_183;
                            }else if (mParts.get(i).getId() == 247){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_247;
                            }

                        }else {
                            if(mParts.get(i).getId() == 39){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_39_INCHES;
                            }else if(mParts.get(i).getId() == 103){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_103_INCHES;
                            }else if (mParts.get(i).getId() == 183){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_183_INCHES;
                            }else if (mParts.get(i).getId() == 247){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_247_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen40ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 40||mParts.get(i).getId() == 104||mParts.get(i).getId() == 184||mParts.get(i).getId() == 248) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen40Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 40){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_40;
                            }else if(mParts.get(i).getId() == 104){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_104;
                            }else if (mParts.get(i).getId() == 184){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_184;
                            }else if (mParts.get(i).getId() == 248){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_248;
                            }

                        }else {
                            if(mParts.get(i).getId() == 40){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_40_INCHES;
                            }else if(mParts.get(i).getId() == 104){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_104_INCHES;
                            }else if (mParts.get(i).getId() == 184){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_184_INCHES;
                            }else if (mParts.get(i).getId() == 248){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_248_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen41ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 41||mParts.get(i).getId() == 105||mParts.get(i).getId() == 185||mParts.get(i).getId() == 249) {
//                        AppConstants.MEASUREMENT_SCALE = String.valueOf(mParts.get(i).getId());
//                        setAdapter(mMen41Txt, mParts.get(i).getImage(), "");

                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }

                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 41){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_41;
                            }else if(mParts.get(i).getId() == 105){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_105;
                            }else if (mParts.get(i).getId() == 185){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_185;
                            }else if (mParts.get(i).getId() == 249){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_249;
                            }

                        }else {
                            if(mParts.get(i).getId() == 41){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_41_INCHES;
                            }else if(mParts.get(i).getId() == 105){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_105_INCHES;
                            }else if (mParts.get(i).getId() == 185){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_185_INCHES;
                            }else if (mParts.get(i).getId() == 249){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_249_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen42ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 42||mParts.get(i).getId() == 106||mParts.get(i).getId() == 186||mParts.get(i).getId() == 250) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 42){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_42;
                            }else if(mParts.get(i).getId() == 106){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_106;
                            }else if (mParts.get(i).getId() == 186){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_186;
                            }else if (mParts.get(i).getId() == 250){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_250;
                            }

                        }else {
                            if(mParts.get(i).getId() == 42){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_42_INCHES;
                            }else if(mParts.get(i).getId() == 106){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_106_INCHES;
                            }else if (mParts.get(i).getId() == 186){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_186_INCHES;
                            }else if (mParts.get(i).getId() == 250){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_250_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen43ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 43||mParts.get(i).getId() == 107||mParts.get(i).getId() == 187||mParts.get(i).getId() == 251) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 43){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_43;
                            }else if(mParts.get(i).getId() == 107){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_107;
                            }else if (mParts.get(i).getId() == 187){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_187;
                            }else if (mParts.get(i).getId() == 251){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_251;
                            }

                        }else {
                            if(mParts.get(i).getId() == 43){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_43_INCHES;
                            }else if(mParts.get(i).getId() == 107){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_107_INCHES;
                            }else if (mParts.get(i).getId() == 187){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_187_INCHES;
                            }else if (mParts.get(i).getId() == 251){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_251_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen44ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 44||mParts.get(i).getId() == 108||mParts.get(i).getId() == 188||mParts.get(i).getId() == 252) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 44){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_44;
                            }else if(mParts.get(i).getId() == 108){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_108;
                            }else if (mParts.get(i).getId() == 188){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_188;
                            }else if (mParts.get(i).getId() == 252){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_252;
                            }

                        }else {
                            if(mParts.get(i).getId() == 44){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_44_INCHES;
                            }else if(mParts.get(i).getId() == 108){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_108_INCHES;
                            }else if (mParts.get(i).getId() == 188){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_188_INCHES;
                            }else if (mParts.get(i).getId() == 252){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_252_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen45ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 45||mParts.get(i).getId() == 109||mParts.get(i).getId() == 189||mParts.get(i).getId() == 253) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 45){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_45;
                            }else if(mParts.get(i).getId() == 109){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_109;
                            }else if (mParts.get(i).getId() == 189){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_189;
                            }else if (mParts.get(i).getId() == 253){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_253;
                            }

                        }else {
                            if(mParts.get(i).getId() == 45){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_45_INCHES;
                            }else if(mParts.get(i).getId() == 109){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_109_INCHES;
                            }else if (mParts.get(i).getId() == 189){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_189_INCHES;
                            }else if (mParts.get(i).getId() == 253){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_253_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen46ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 46||mParts.get(i).getId() == 110||mParts.get(i).getId() == 190||mParts.get(i).getId() == 254) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 46){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_46;
                            }else if(mParts.get(i).getId() == 110){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_110;
                            }else if (mParts.get(i).getId() == 190){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_190;
                            }else if (mParts.get(i).getId() == 254){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_254;
                            }

                        }else {
                            if(mParts.get(i).getId() == 46){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_46_INCHES;
                            }else if(mParts.get(i).getId() == 110){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_110_INCHES;
                            }else if (mParts.get(i).getId() == 190){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_190_INCHES;
                            }else if (mParts.get(i).getId() == 254){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_254_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen47ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 47||mParts.get(i).getId() == 111||mParts.get(i).getId() == 191||mParts.get(i).getId() == 255) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 47){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_47;
                            }else if(mParts.get(i).getId() == 111){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_111;
                            }else if (mParts.get(i).getId() == 191){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_191;
                            }else if (mParts.get(i).getId() == 255){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_255;
                            }

                        }else {
                            if(mParts.get(i).getId() == 47){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_47_INCHES;
                            }else if(mParts.get(i).getId() == 111){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_111_INCHES;
                            }else if (mParts.get(i).getId() == 191){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_191_INCHES;
                            }else if (mParts.get(i).getId() == 255){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_255_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen48ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 48||mParts.get(i).getId() == 112||mParts.get(i).getId() == 192||mParts.get(i).getId() == 256) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 48){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_48;
                            }else if(mParts.get(i).getId() == 112){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_112;
                            }else if (mParts.get(i).getId() == 192){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_192;
                            }else if (mParts.get(i).getId() == 256){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_256;
                            }

                        }else {
                            if(mParts.get(i).getId() == 48){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_48_INCHES;
                            }else if(mParts.get(i).getId() == 112){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_112_INCHES;
                            }else if (mParts.get(i).getId() == 192){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_192_INCHES;
                            }else if (mParts.get(i).getId() == 256){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_256_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen49ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 49||mParts.get(i).getId() == 113||mParts.get(i).getId() == 193||mParts.get(i).getId() == 257) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 49){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_49;
                            }else if(mParts.get(i).getId() ==113){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_113;
                            }else if (mParts.get(i).getId() == 193){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_193;
                            }else if (mParts.get(i).getId() == 257){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_257;
                            }

                        }else {
                            if(mParts.get(i).getId() == 49){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_49_INCHES;
                            }else if(mParts.get(i).getId() == 113){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_113_INCHES;
                            }else if (mParts.get(i).getId() == 193){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_193_INCHES;
                            }else if (mParts.get(i).getId() == 257){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_257_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen50ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 50||mParts.get(i).getId() == 114||mParts.get(i).getId() == 194||mParts.get(i).getId() == 258) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        } if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 50){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_50;
                            }else if(mParts.get(i).getId() == 114){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_114;
                            }else if (mParts.get(i).getId() == 194){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_194;
                            }else if (mParts.get(i).getId() == 258){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_258;
                            }

                        }else {
                            if(mParts.get(i).getId() == 50){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_50_INCHES;
                            }else if(mParts.get(i).getId() == 114){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_114_INCHES;
                            }else if (mParts.get(i).getId() == 194){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_194_INCHES;
                            }else if (mParts.get(i).getId() == 258){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_258_INCHES;
                            }

                        }



                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen51ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 51||mParts.get(i).getId() == 115||mParts.get(i).getId() == 195||mParts.get(i).getId() == 259) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 51){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_51;
                            }else if(mParts.get(i).getId() == 115){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_115;
                            }else if (mParts.get(i).getId() == 195){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_195;
                            }else if (mParts.get(i).getId() == 259){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_259;
                            }

                        }else {
                            if(mParts.get(i).getId() == 51){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_51_INCHES;
                            }else if(mParts.get(i).getId() == 115){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_115_INCHES;
                            }else if (mParts.get(i).getId() == 195){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_195_INCHES;
                            }else if (mParts.get(i).getId() == 259){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_259_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen52ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 52||mParts.get(i).getId() == 116||mParts.get(i).getId() == 196||mParts.get(i).getId() == 260) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 52){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_52;
                            }else if(mParts.get(i).getId() == 116){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_116;
                            }else if (mParts.get(i).getId() == 196){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_196;
                            }else if (mParts.get(i).getId() == 260){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_260;
                            }

                        }else {
                            if(mParts.get(i).getId() == 52){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_52_INCHES;
                            }else if(mParts.get(i).getId() == 116){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_116_INCHES;
                            }else if (mParts.get(i).getId() == 196){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_196_INCHES;
                            }else if (mParts.get(i).getId() == 260){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_260_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen53ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 53||mParts.get(i).getId() == 117||mParts.get(i).getId() == 197||mParts.get(i).getId() == 261) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 53){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_53;
                            }else if(mParts.get(i).getId() == 117){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_117;
                            }else if (mParts.get(i).getId() == 197){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_197;
                            }else if (mParts.get(i).getId() == 261){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_261;
                            }

                        }else {
                            if(mParts.get(i).getId() == 53){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_53_INCHES;
                            }else if(mParts.get(i).getId() == 117){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_117_INCHES;
                            }else if (mParts.get(i).getId() == 197){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_197_INCHES;
                            }else if (mParts.get(i).getId() == 261){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_261_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen54ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 54||mParts.get(i).getId() == 118||mParts.get(i).getId() == 198||mParts.get(i).getId() == 262) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 54){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_54;
                            }else if(mParts.get(i).getId() == 118){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_118;
                            }else if (mParts.get(i).getId() == 198){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_198;
                            }else if (mParts.get(i).getId() == 262){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_262;
                            }

                        }else {
                            if(mParts.get(i).getId() == 54){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_54_INCHES;
                            }else if(mParts.get(i).getId() == 118){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_118_INCHES;
                            }else if (mParts.get(i).getId() == 198){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_198_INCHES;
                            }else if (mParts.get(i).getId() == 262){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_262_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen55ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 55||mParts.get(i).getId() == 119||mParts.get(i).getId() == 199||mParts.get(i).getId() == 263) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 55){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_55;
                            }else if(mParts.get(i).getId() == 119){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_119;
                            }else if (mParts.get(i).getId() == 199){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_199;
                            }else if (mParts.get(i).getId() == 263){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_263;
                            }

                        }else {
                            if(mParts.get(i).getId() == 55){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_55_INCHES;
                            }else if(mParts.get(i).getId() == 119){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_119_INCHES;
                            }else if (mParts.get(i).getId() == 199){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_199_INCHES;
                            }else if (mParts.get(i).getId() == 263){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_263_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen56ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 56||mParts.get(i).getId() == 120||mParts.get(i).getId() == 200||mParts.get(i).getId() == 264) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 56){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_56;
                            }else if(mParts.get(i).getId() == 120){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_120;
                            }else if (mParts.get(i).getId() == 200){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_200;
                            }else if (mParts.get(i).getId() == 264){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_264;
                            }

                        }else {
                            if(mParts.get(i).getId() == 56){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_56_INCHES;
                            }else if(mParts.get(i).getId() == 120){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_120_INCHES;
                            }else if (mParts.get(i).getId() == 200){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_200_INCHES;
                            }else if (mParts.get(i).getId() == 264){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_264_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen57ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 57||mParts.get(i).getId() == 121||mParts.get(i).getId() == 201||mParts.get(i).getId() == 265) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 57){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_57;
                            }else if(mParts.get(i).getId() == 121){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_121;
                            }else if (mParts.get(i).getId() == 201){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_201;
                            }else if (mParts.get(i).getId() == 265){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_265;
                            }

                        }else {
                            if(mParts.get(i).getId() == 57){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_57_INCHES;
                            }else if(mParts.get(i).getId() == 121){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_121_INCHES;
                            }else if (mParts.get(i).getId() == 201){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_201_INCHES;
                            }else if (mParts.get(i).getId() == 265){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_265_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen58ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 58||mParts.get(i).getId() == 122||mParts.get(i).getId() == 202||mParts.get(i).getId() == 266) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 58){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_58;
                            }else if(mParts.get(i).getId() == 122){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_122;
                            }else if (mParts.get(i).getId() == 202){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_202;
                            }else if (mParts.get(i).getId() == 266){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_266;
                            }

                        }else {
                            if(mParts.get(i).getId() == 58){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_58_INCHES;
                            }else if(mParts.get(i).getId() == 122){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_122_INCHES;
                            }else if (mParts.get(i).getId() == 202){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_202_INCHES;
                            }else if (mParts.get(i).getId() == 266){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_266_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen59ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 59||mParts.get(i).getId() == 123||mParts.get(i).getId() == 203||mParts.get(i).getId() == 267) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 59){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_59;
                            }else if(mParts.get(i).getId() == 123){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_123;
                            }else if (mParts.get(i).getId() == 203){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_203;
                            }else if (mParts.get(i).getId() == 267){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_267;
                            }

                        }else {
                            if(mParts.get(i).getId() == 59){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_59_INCHES;
                            }else if(mParts.get(i).getId() == 123){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_123_INCHES;
                            }else if (mParts.get(i).getId() == 203){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_203_INCHES;
                            }else if (mParts.get(i).getId() == 267){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_267_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen60ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 60||mParts.get(i).getId() == 124||mParts.get(i).getId() == 204||mParts.get(i).getId() == 268) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 60){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_60;
                            }else if(mParts.get(i).getId() == 124){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_124;
                            }else if (mParts.get(i).getId() == 204){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_204;
                            }else if (mParts.get(i).getId() == 268){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_268;
                            }

                        }else {
                            if(mParts.get(i).getId() == 60){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_60_INCHES;
                            }else if(mParts.get(i).getId() == 124){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_124_INCHES;
                            }else if (mParts.get(i).getId() == 204){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_204_INCHES;
                            }else if (mParts.get(i).getId() == 268){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_268_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen61ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 61||mParts.get(i).getId() == 125||mParts.get(i).getId() == 205||mParts.get(i).getId() == 269) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 61){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_61;
                            }else if(mParts.get(i).getId() == 125){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_125;
                            }else if (mParts.get(i).getId() == 205){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_205;
                            }else if (mParts.get(i).getId() == 269){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_269;
                            }

                        }else {
                            if(mParts.get(i).getId() == 61){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_61_INCHES;
                            }else if(mParts.get(i).getId() == 125){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_125_INCHES;
                            }else if (mParts.get(i).getId() == 205){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_205_INCHES;
                            }else if (mParts.get(i).getId() == 269){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_269_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen62ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 62||mParts.get(i).getId() == 126||mParts.get(i).getId() == 206||mParts.get(i).getId() == 270) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 62){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_62;
                            }else if(mParts.get(i).getId() == 126){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_126;
                            }else if (mParts.get(i).getId() == 206){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_206;
                            }else if (mParts.get(i).getId() == 270){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_270;
                            }

                        }else {
                            if(mParts.get(i).getId() == 62){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_62_INCHES;
                            }else if(mParts.get(i).getId() == 126){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_126_INCHES;
                            }else if (mParts.get(i).getId() == 206){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_206_INCHES;
                            }else if (mParts.get(i).getId() == 270){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_270_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen63ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 63||mParts.get(i).getId() == 127||mParts.get(i).getId() == 207||mParts.get(i).getId() == 271) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 63){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_63;
                            }else if(mParts.get(i).getId() == 127){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_127;
                            }else if (mParts.get(i).getId() == 207){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_207;
                            }else if (mParts.get(i).getId() == 271){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_271;
                            }

                        }else {
                            if(mParts.get(i).getId() == 63){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_63_INCHES;
                            }else if(mParts.get(i).getId() == 127){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_127_INCHES;
                            }else if (mParts.get(i).getId() == 207){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_207_INCHES;
                            }else if (mParts.get(i).getId() == 271){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_271_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mMen64ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 64||mParts.get(i).getId() == 128||mParts.get(i).getId() == 208||mParts.get(i).getId() == 272) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if(mParts.get(i).getId() == 64){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_64;
                            }else if(mParts.get(i).getId() == 128){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_128;
                            }else if (mParts.get(i).getId() == 208){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_208;
                            }else if (mParts.get(i).getId() == 272){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_272;
                            }

                        }else {
                            if(mParts.get(i).getId() == 64){
                                AppConstants.SelectedPartsEditValue = AppConstants.MEN_64_INCHES;
                            }else if(mParts.get(i).getId() == 128){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_128_INCHES;
                            }else if (mParts.get(i).getId() == 208){
                                AppConstants.SelectedPartsEditValue = AppConstants.BOY_208_INCHES;
                            }else if (mParts.get(i).getId() == 272){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_272_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen65ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 129||mParts.get(i).getId() == 273) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                             if (mParts.get(i).getId() == 129){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_129;
                            }else if (mParts.get(i).getId() == 273){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_273;
                            }

                        }else {
                           if (mParts.get(i).getId() == 129){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_129_INCHES;
                            }else if (mParts.get(i).getId() == 273){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_273_INCHES;
                            }

                        }


                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen66ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 130||mParts.get(i).getId() == 274) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if (mParts.get(i).getId() == 130){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_130;
                            }else if (mParts.get(i).getId() == 274){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_274;
                            }

                        }else {
                            if (mParts.get(i).getId() == 130){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_130_INCHES;
                            }else if (mParts.get(i).getId() == 274){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_274_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen67ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 131||mParts.get(i).getId() == 275) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if (mParts.get(i).getId() == 131){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_131;
                            }else if (mParts.get(i).getId() == 275){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_275;
                            }

                        }else {
                            if (mParts.get(i).getId() == 131){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_131_INCHES;
                            }else if (mParts.get(i).getId() == 275){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_275_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen68ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 132||mParts.get(i).getId() == 276) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if (mParts.get(i).getId() == 132){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_132;
                            }else if (mParts.get(i).getId() == 276){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_276;
                            }

                        }else {
                            if (mParts.get(i).getId() == 132){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_132_INCHES;
                            }else if (mParts.get(i).getId() == 276){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_276_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen69ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 133||mParts.get(i).getId() ==277) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if (mParts.get(i).getId() == 133){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_133;
                            }else if (mParts.get(i).getId() == 277){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_277;
                            }

                        }else {
                            if (mParts.get(i).getId() == 133){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_133_INCHES;
                            }else if (mParts.get(i).getId() == 277){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_277_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen70ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 134||mParts.get(i).getId() == 278) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if (mParts.get(i).getId() == 134){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_134;
                            }else if (mParts.get(i).getId() == 278){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_278;
                            }

                        }else {
                            if (mParts.get(i).getId() == 134){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_134_INCHES;
                            }else if (mParts.get(i).getId() == 278){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_278_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen71ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 135||mParts.get(i).getId() == 279) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if (mParts.get(i).getId() == 135){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_135;
                            }else if (mParts.get(i).getId() == 279){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_279;
                            }

                        }else {
                            if (mParts.get(i).getId() == 135){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_135_INCHES;
                            }else if (mParts.get(i).getId() == 279){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_279_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen72ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 136||mParts.get(i).getId() == 280) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if (mParts.get(i).getId() == 136){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_136;
                            }else if (mParts.get(i).getId() == 280){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_280;
                            }

                        }else {
                            if (mParts.get(i).getId() == 136){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_136_INCHES;
                            }else if (mParts.get(i).getId() == 280){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_280_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen73ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 137||mParts.get(i).getId() == 281) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if (mParts.get(i).getId() == 137){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_137;
                            }else if (mParts.get(i).getId() == 281){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_281;
                            }

                        }else {
                            if (mParts.get(i).getId() == 137){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_137_INCHES;
                            }else if (mParts.get(i).getId() == 281){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_281_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen74ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 138||mParts.get(i).getId() == 282) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if (mParts.get(i).getId() == 138){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_138;
                            }else if (mParts.get(i).getId() == 282){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_282;
                            }

                        }else {
                            if (mParts.get(i).getId() == 138){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_138_INCHES;
                            }else if (mParts.get(i).getId() == 282){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_282_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen75ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 139||mParts.get(i).getId() == 283) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if (mParts.get(i).getId() == 139){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_139;
                            }else if (mParts.get(i).getId() == 283){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_283;
                            }

                        }else {
                            if (mParts.get(i).getId() == 139){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_139_INCHES;
                            }else if (mParts.get(i).getId() == 283){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_283_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen76ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 140||mParts.get(i).getId() == 284) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if (mParts.get(i).getId() == 140){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_140;
                            }else if (mParts.get(i).getId() == 284){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_284;
                            }

                        }else {
                            if (mParts.get(i).getId() == 140){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_140_INCHES;
                            }else if (mParts.get(i).getId() == 284){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_284_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen77ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 141||mParts.get(i).getId() == 285) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if (mParts.get(i).getId() == 141){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_141;
                            }else if (mParts.get(i).getId() == 285){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_285;
                            }

                        }else {
                            if (mParts.get(i).getId() == 141){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_141_INCHES;
                            }else if (mParts.get(i).getId() == 285){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_285_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen78ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 142||mParts.get(i).getId() == 286) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if (mParts.get(i).getId() == 142){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_142;
                            }else if (mParts.get(i).getId() == 286){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_286;
                            }

                        }else {
                            if (mParts.get(i).getId() == 142){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_142_INCHES;
                            }else if (mParts.get(i).getId() == 286){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_286_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen79ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 143||mParts.get(i).getId() == 287) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if (mParts.get(i).getId() == 143){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_143;
                            }else if (mParts.get(i).getId() == 287){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_287;
                            }

                        }else {
                            if (mParts.get(i).getId() == 143){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_143_INCHES;
                            }else if (mParts.get(i).getId() == 287){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_287_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });
        mWomen80ParLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<mParts.size(); i++) {
                    if (mParts.get(i).getId() == 144||mParts.get(i).getId() == 288) {
                        AppConstants.SelectedPartsId = String.valueOf(mParts.get(i).getId());
                        AppConstants.SelectedPartsImage = mParts.get(i).getPartGifImage();
                        AppConstants.SEEK_MIN = String.valueOf(mParts.get(i).getStartMeasurementValue());
                        AppConstants.SEEK_MAX = String.valueOf(mParts.get(i).getEndMeasurementValue());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInArabic();

                        }else {
                            AppConstants.SelectedPartsName = mParts.get(i).getTextInEnglish();

                        }
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            if (mParts.get(i).getId() == 144){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_144;
                            }else if (mParts.get(i).getId() == 288){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_288;
                            }

                        }else {
                            if (mParts.get(i).getId() == 144){
                                AppConstants.SelectedPartsEditValue = AppConstants.WOMEN_144_INCHES;
                            }else if (mParts.get(i).getId() == 288){
                                AppConstants.SelectedPartsEditValue = AppConstants.GIRL_288_INCHES;
                            }

                        }

                        ((MeasurementTwoScreen)context).getMeasurmentTwoEdtTxtPopUp();
                    }
                }
            }
        });

        if (position == 0) {
            mMenOneLay.setVisibility(View.VISIBLE);
            mMenTwoLay.setVisibility(View.GONE);
            mMenThreeLay.setVisibility(View.GONE);
            mMenFourLay.setVisibility(View.GONE);
            mWomenOneLay.setVisibility(View.GONE);
        }
        if (position == 1) {
            mMenOneLay.setVisibility(View.GONE);
            mMenTwoLay.setVisibility(View.VISIBLE);
            mMenThreeLay.setVisibility(View.GONE);
            mMenFourLay.setVisibility(View.GONE);
            mWomenOneLay.setVisibility(View.GONE);
        }
        if (position == 2) {
            mMenOneLay.setVisibility(View.GONE);
            mMenTwoLay.setVisibility(View.GONE);
            mMenThreeLay.setVisibility(View.VISIBLE);
            mMenFourLay.setVisibility(View.GONE);
            mWomenOneLay.setVisibility(View.GONE);
        }
        if (position == 3) {
            mMenOneLay.setVisibility(View.GONE);
            mMenTwoLay.setVisibility(View.GONE);
            mMenThreeLay.setVisibility(View.GONE);
            mMenFourLay.setVisibility(View.VISIBLE);
            mWomenOneLay.setVisibility(View.GONE);
        }
        if (position == 4){
            mMenOneLay.setVisibility(View.GONE);
            mMenTwoLay.setVisibility(View.GONE);
            mMenThreeLay.setVisibility(View.GONE);
            mMenFourLay.setVisibility(View.GONE);
            mWomenOneLay.setVisibility(View.VISIBLE);
        }

        try {
            Glide.with(context)
                    .load(AppConstants.IMAGE_BASE_URL+"images/Measurement2/"+IMAGES.get(position).getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(imageView);
        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    /*Default dialog init method*/
    public Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }


    public void getArraylist() {
        num = 0.0;

        mArrayList = new ArrayList<>();
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("0.0");

        for (int i = 0; i < 35; i++) {
            num = num + 0.1;
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat formatter = (DecimalFormat)nf;
            formatter.applyPattern("######.#");
            mArrayList.add(String.valueOf(Double.valueOf(formatter.format(num))));
        }

    }

    public void setAdapter(final TextView mText,String partImage,String titleTxtStr) {
        getArraylist();
        alertDismiss(mSlider);
        mSlider = getDialog(context, R.layout.pop_up_measurement_scale);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mSlider.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mSlider.findViewById(R.id.pop_up_measurement_scale_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(mSlider.findViewById(R.id.pop_up_measurement_scale_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
        final TextView scaleTxt;
        Button mOkBtn,mCancelBtn;
        ImageView partsImageView;

        RecyclerView recyclerView;
        final LinearLayoutManager layoutManager;
        /*Init view*/
        mOkBtn = mSlider.findViewById(R.id.scale_measurement_ok_btn);
        mCancelBtn = mSlider.findViewById(R.id.scale_measurement_cancel_btn);
        scaleTxt = mSlider.findViewById(R.id.scale_cm_txt);
        partsImageView = mSlider.findViewById(R.id.measurement_scale_body_img);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            mCancelBtn.setText("إلغاء");
            mOkBtn.setText("حسنا");
        }

        try {
            Glide.with(context)
                    .load(AppConstants.IMAGE_BASE_URL + "images/Measurement2/" + partImage)
                    .apply(new RequestOptions().placeholder(R.drawable.dress_type_empty_img).error(R.drawable.dress_type_empty_img))
                    .into(partsImageView);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }
        recyclerView = mSlider.findViewById(R.id.measurmentScallingSampleRecyclerview);
        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);

            mMeasurementAdapter = new MeasurementScaleAdapter(context, mArrayList);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(mMeasurementAdapter);

        DialogManager.getInstance().hideProgress();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstPos = layoutManager.findFirstVisibleItemPosition();
                int lastPos = layoutManager.findLastVisibleItemPosition();
                int middle = Math.abs(lastPos - firstPos) / 2 + firstPos;

                if (mCheckToAdd.equalsIgnoreCase("")) {

                for (int i = 0; i < mArrayList.size(); i++) {
                    if (mArrayList.get(i).equalsIgnoreCase("99.0")) {
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");
                        mArrayList.add("");

                        mMeasurementAdapter.notifyDataSetChanged();

                        mCheckToAdd = "Added";
                    } else {
                        if (String.valueOf(lastPos).equalsIgnoreCase(String.valueOf(mArrayList.size() - 1))) {

                            for (int j = 0; j < 10; j++) {
                                num = num + 0.1;
                                NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                                DecimalFormat formatter = (DecimalFormat)nf;
                                formatter.applyPattern("######.#");
                                mArrayList.add(String.valueOf(Double.valueOf(formatter.format(num))));

                            }
                            mMeasurementAdapter.notifyDataSetChanged();

                        }
                    }
                }
            }

                    for (int k = 0; k < mMeasurementAdapter.getItemCount(); k++) {
                    if (k == middle) {
                        scaleTxt.setText(mArrayList.get(k).equalsIgnoreCase("") ? "0.0CM" : mArrayList.get(k));
                        AppConstants.MEASUREMENT_LENGTH = mArrayList.get(k).equalsIgnoreCase("") ? "0.0" : mArrayList.get(k);

                    }
                }
            }
        });
        /*Set data*/
        mOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                DecimalFormat formatter = (DecimalFormat)nf;
                formatter.applyPattern("######.#");
                Float values = Float.parseFloat(AppConstants.MEASUREMENT_LENGTH);
                Float multiplyValue = Float.parseFloat("2.5");

                if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("1")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){

                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_1 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_1_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_1 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_1_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;

                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("2")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){

                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_2 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_2_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_2 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_2_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("3")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_3 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_3_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_3 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_3_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("4")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_4 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_4_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_4 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_4_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("5")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_5 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_5_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_5 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_5_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }

                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("6")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_6 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_6_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_6 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_6_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("7")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_7 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_7_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_7 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_7_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("8")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_8 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_8_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_8 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_8_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("9")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_9 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_9_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_9 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_9_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("10")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_10 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_10_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_10 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_10_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("11")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_11 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_11_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_11 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_11_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("12")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_12 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_12_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_12 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_12_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("13")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_13 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_13_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_13 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_13_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("14")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_14 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_14_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_14 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_14_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("15")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_15 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_15_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_15 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_15_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("16")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_16 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_16_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_16 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_16_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("17")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_17 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_17_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_17 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_17_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("18")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_18 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_18_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_18 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_18_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("19")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_19 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_19_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_19= String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_19_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("20")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_20 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_20_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_20 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_20_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("21")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_21 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_21_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_21= String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_21_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("22")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_22 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_22_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_22 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_22_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("23")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_23 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_23_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_23 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_23_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("24")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_24 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_24_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_24 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_24_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("25")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_25 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_25_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_25 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_25_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("26")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_26 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_26_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_26 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_26_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("27")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_27 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_27_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_27 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_27_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("28")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_28 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_28_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_28 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_28_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("29")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_29 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_29_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_29 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_29_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("30")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_30 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_30_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_30 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_30_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("31")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_31 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_31_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_31 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_31_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("32")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_32 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_32_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_32 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_32_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("33")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_33 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_33_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_33 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_33_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("34")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_34 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_34_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_34 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_34_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("35")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_35 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_35_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_35 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_35_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("36")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_36 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_36_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_36 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_36_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("37")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_37 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_37_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_37 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_37_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("38")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_38 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_38_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_38 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_38_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("39")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_39 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_39_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_39 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_39_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("40")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_40 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_40_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_40 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_40_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("41")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_41 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_41_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_41 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_41_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("42")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_42 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_42_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_42 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_42_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("43")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_43 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_43_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_43 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_43_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("44")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_44 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_44_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_44 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_44_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("45")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_45 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_45_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_45 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_45_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("46")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_46 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_46_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_46 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_46_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("47")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_47 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_47_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_47 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_47_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("48")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_48 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_48_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_48 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_48_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("49")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_49 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_49_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_49 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_49_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("50")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_50 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_50_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_50 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_50_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("51")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_51 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_51_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_51 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_51_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("52")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_52 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_52_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_52 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_52_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("53")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_53 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_53_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_53 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_53_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("54")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_54 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_54_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_54 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_54_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("55")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_55 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_55_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_55 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_55_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("56")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_56 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_56_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_56 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_56_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("57")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_57 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_57_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_57 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_57_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("58")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_58 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_58_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_58 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_58_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("59")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_59 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_59_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_59 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_59_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("60")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_60 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_60_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_60 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_60_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("61")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_61 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_61_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_61 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_61_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("62")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_62 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_62_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_62 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_62_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("63")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_63 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_63_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_63 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_63_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("64")&&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.MEN_64 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.MEN_64_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.MEN_64 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.MEN_64_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("65")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_65 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_65_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_65 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_65_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("66")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_66 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_66_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_66 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_66_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("67")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_67 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_67_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_67 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_67_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("68")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_68 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_68_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_68 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_68_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("69")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_69 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_69_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_69 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_69_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("70")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_70 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_70_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_70 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_70_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("71")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_71 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_71_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_71 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_71_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("72")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_72 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_72_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_72 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_72_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("73")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_73 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_73_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_73 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_73_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("74")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_74 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_74_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_74 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_74_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("75")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_75 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_75_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_75 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_75_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("76")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_76 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_76_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_76 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_76_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("77")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_77 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_77_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_77 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_77_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("78")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_78 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_78_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_78 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_78_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("79")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_79 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_79_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_79 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_79_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("80")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_80 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_80_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_80 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_80_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("81")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_81 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_81_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_81 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_81_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("82")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_82 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_82_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_82 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_82_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("83")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_83 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_83_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_83 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_83_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("84")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_84 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_84_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_84 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_84_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("86")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_86 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_86_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_86 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_86_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("87")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_87 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_87_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_87 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_87_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("88")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_88 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_88_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_88 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_88_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("89")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_89 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_89_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_89 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_89_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("90")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_90 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_90_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_90 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_90_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("91")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_91 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_91_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_91 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_91_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("92")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_92 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_92_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_92 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_92_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("93")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_93 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_93_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_93 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_93_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("94")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_94 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_94_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_94 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_94_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("95")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_95 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_95_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_95 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_95_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("96")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_96 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_96_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_96 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_96_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("97")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_97 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_97_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_97 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_97_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("98")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_98 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_98_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_98 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_98_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("99")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_99 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_99_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_99 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_99_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("100")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_100 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_100_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_100 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_100_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("101")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_101 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_101_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_101 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_101_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("102")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_102 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_102_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_102 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_102_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("103")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_103 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_103_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_103 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_103_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("104")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_104 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_104_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_104 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_104_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("105")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_105 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_105_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_105 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_105_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("106")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_106 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_106_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_106 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_106_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("107")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_107 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_107_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_107 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_107_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("108")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_108 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_108_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_108 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_108_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("109")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_109 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_109_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_109 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_109_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("110")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_110 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_110_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_110 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_110_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("111")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_111 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_111_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_111 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_111_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("112")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_112 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_112_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_112 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_112_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("113")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_113 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_113_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_113 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_113_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("114")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_114 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_114_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_114 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_114_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("115")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_115 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_115_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_115 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_115_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("116")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_116 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_116_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_116 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_116_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("117")&&AppConstants.GENDER_ID.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_117 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_117_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_117 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_117_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("118")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_118 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_118_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_118 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_118_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("119")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_119 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_119_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_119 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_119_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("120")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_120 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_120_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_120 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_120_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("121")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_121 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_121_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_121 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_121_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("122")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_122 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_122_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_122 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_122_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("123")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_123 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_123_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_123 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_123_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("124")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_124 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_124_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_124 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_124_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("125")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_125 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_125_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_125 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_125_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("126")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_126 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_126_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_126 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_126_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("127")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_127 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_127_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_127 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_127_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("128")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_128 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_128_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_128 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_128_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("129")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_129 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_129_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_129 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_129_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("130")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_130 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_130_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_130 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_130_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("131")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_131 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_131_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_131 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_131_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("132")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_132 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_132_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_132 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_132_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("133")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_133 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_133_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_133 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_133_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("134")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_134 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_134_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_134 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_134_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("135")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_135 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_135_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_135 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_135_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("136")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_136 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_136_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_136 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_136_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("137")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_137 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_137_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_137 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_137_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("138")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_138 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_138_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_138 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_138_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("139")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_139 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_139_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_139 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_139_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("140")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_140 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_140_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_140 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_140_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("141")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_141 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_141_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_141 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_141_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("142")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_142 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_142_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_142 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_142_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("143")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_143 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_143_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_143 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_143_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("144")&&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_144 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_144_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_144 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.WOMEN_144_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("145")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_145 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_145_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_145 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_145_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("146")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_146 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_146_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_146 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_146_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("147")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_147 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_147_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_147 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_147_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("148")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_148 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_148_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_148 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_148_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("149")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_149 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_149_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_149 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_149_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("150")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_150 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_150_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_150 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_150_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("151")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_151 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_151_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_151 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_151_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("152")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_152 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_152_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_152 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_152_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("153")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_153 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_153_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_153 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_153_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("154")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_154 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_154_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_154 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_154_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("155")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_155 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_155_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_155 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_155_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("156")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_156 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_156_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_156 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_156_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("157")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_157 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_157_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_157 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_157_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("158")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_158 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_158_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_158 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_158_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("159")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_159 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_159_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_159 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_159_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("160")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_160 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_160_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_160 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_160_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("161")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_161 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_161_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_161 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_161_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("162")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_162 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_162_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_162 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_162_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("163")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_163 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_163_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_163 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_163_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("164")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_164 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_164_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_164 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_164_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("165")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_165 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_165_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_165 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_165_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("166")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_166 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_166_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_166 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_166_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("167")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_167 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_167_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_167 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_167_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("168")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_168 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_168_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_168 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_168_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("169")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_169 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_169_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_169 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_169_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("170")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_170 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_170_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_170 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_170_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("171")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_171 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_171_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_171 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_171_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("172")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_172 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_172_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_172 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_172_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("173")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_173 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_173_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_173 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_173_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("174")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_174 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_174_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_174 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_174_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("175")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_175 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_175_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_175 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_175_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("176")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_176 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_176_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_176 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_176_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("177")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_177 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_177_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_177 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_177_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("178")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_178 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_178_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_178 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_178_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("179")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_179 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_179_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_179 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_179_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("180")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_180 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_180_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_180 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_180_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("181")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_181 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_181_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_181 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_181_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("182")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_182 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_182_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_182 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_182_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("183")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_183 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_183_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_183 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_183_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("184")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_184 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_184_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_184 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_184_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("185")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_185 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_185_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_185 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_185_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("186")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_186 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_186_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_186 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_186_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("187")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_187 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_187_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_187 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_187_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("188")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_188 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_188_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_188 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_188_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("189")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_189 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_189_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_189 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_189_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("190")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_190 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_190_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_190 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_190_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("191")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_191 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_191_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_191 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_191_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("192")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_192 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_192_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_192 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_192_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("193")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_193 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_193_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_193 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_193_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("194")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_194 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_194_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_194 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_194_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("195")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_195 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_195_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_195 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_195_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("196")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_196 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_196_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_196 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_196_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("197")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_197 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_197_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_197 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_197_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("198")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_198 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_198_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_198 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_198_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("199")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_199 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_199_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_199 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_199_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("200")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_200 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_200_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_200 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_200_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("201")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_201 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_201_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_201 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_201_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("202")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_202 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_202_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_202 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_202_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("203")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_203 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_203_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_203 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_203_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("204")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_204 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_204_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_204 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_204_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("205")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_205 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_205_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_205 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_205_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("206")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_206 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_206_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_206 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_206_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("207")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_207 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_207_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_207 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_207_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("208")&&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_208 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_208_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_208 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.BOY_208_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("209")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_209 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_209_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_209 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_209_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("210")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_210 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_210_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_210 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_210_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("211")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_211 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_211_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_211 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_211_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("212")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_212 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_212_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_212 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_212_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("213")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_213 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_213_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_213 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_213_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("214")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_214 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_214_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_214 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_214_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("215")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_215 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_215_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_215 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_215_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("216")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_216 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_216_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_216 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_216_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("217")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_217 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_217_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_217 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_217_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("218")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_218 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_218_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_218 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_218_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("219")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_219 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_219_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_219 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_219_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("220")&&AppConstants.GENDER_ID.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_220 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_220_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_220 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_220_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("221")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_221 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_221_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_221 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_221_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("222")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_222 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_222_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_222 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_222_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("223")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_223 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_223_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_223 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_223_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("224")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_224 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_224_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_224 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_224_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("225")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_225 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_225_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_225 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_225_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("226")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_226 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_226_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_226 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_226_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("227")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_227 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_227_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_227 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_227_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("228")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_228 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_228_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_228 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_228_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("229")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_229 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_229_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_229 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_229_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("230")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_230 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_230_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_230 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_230_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("231")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_231 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_231_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_231 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_231_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("232")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_232 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_232_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_232 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_232_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("233")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_233 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_233_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_233 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_233_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("234")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_234 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_234_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_234 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_234_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("235")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_235 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_235_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_235 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_235_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("236")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_236 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_236_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_236 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_236_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("237")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_237 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_237_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_237 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_237_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("238")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_238 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_238_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_238 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_238_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("239")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_239 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_239_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_239 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_239_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("240")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_240 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_240_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_240 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_240_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("241")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_241 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_241_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_241 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_241_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("242")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_242 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_242_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_242 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_242_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("243")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_243 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_243_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_243 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_243_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("244")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_244 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_244_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_244 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_244_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("245")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_245 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_245_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_245 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_245_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("246")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_246 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_246_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_246 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_246_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("247")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_247 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_247_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_247 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_247_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("248")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_248 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_248_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_248 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_248_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("249")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_249 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_249_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_249 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_249_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("250")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_250 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_250_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_250 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_250_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("251")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_251 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_251_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_251 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_251_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("252")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_252 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_252_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_252 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_252_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("253")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_253 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_253_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_253 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_253_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("254")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_254 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_254_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_254 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_254_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("255")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_255 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_255_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_255 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_255_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("256")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_256 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_256_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_256 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_256_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("257")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_257 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_257_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_257 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_257_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("258")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_258 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_258_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_258 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_258_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("259")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_259 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_259_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_259 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_259_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("260")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_260 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_260_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_260 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_260_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("261")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_261 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_261_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_261 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_261_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("262")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_262 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_262_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_262 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_262_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("263")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_263 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_263_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_263 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_263_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("264")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_264 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_264_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_264 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_264_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("265")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_265 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_265_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_265 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_265_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("266")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_266 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_266_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_266 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_266_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("267")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_267 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_267_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_267 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_267_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("268")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_268 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_268_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_268 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_268_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("269")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_269 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_269_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_269 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_269_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("270")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_270 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_270_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_270 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_270_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("271")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_271 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_271_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_271 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_271_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("272")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_272 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_272_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_272 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_272_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("273")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_273 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_273_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_273 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_273_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("274")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_274 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_274_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_274 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_274_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("275")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_275 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_275_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_275 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_275_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("276")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_276 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_276_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_276 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_276_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("277")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_277 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_277_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_277 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_277_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("278")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_278 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_278_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_278 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_278_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("279")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_279 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_279_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_279 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_279_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("280")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_280 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_280_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_280 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_280_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("281")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_281 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_281_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_281 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_281_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("282")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_282 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_282_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_282 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_282_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("283")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_283 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_283_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_283 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_283_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("284")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_284 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_284_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_284 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_284_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("285")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_285 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_285_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_285 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_285_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("286")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_286 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_286_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_286 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_286_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("287")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_287 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_287_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_287 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_287_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("288")&&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_288 = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_288_INCHES = String.valueOf(formatter.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_288 = String.valueOf(formatter.format(values * multiplyValue));
                        AppConstants.GIRL_288_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }

                mText.setText(AppConstants.MEASUREMENT_LENGTH);
                mSlider.dismiss();
                reloadArrayList();
            }
        });
        mCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSlider.dismiss();
            }
        });

        alertShowing(mSlider);

    }

    public void reloadArrayList(){
        for (int i=0; i<mParts.size(); i++){

            if (mParts.get(i).getId() == 1 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_1);
                Inches.add(i,AppConstants.MEN_1_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 2 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_2);
                Inches.add(i,AppConstants.MEN_2_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 3 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_3);
                Inches.add(i,AppConstants.MEN_3_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 4 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_4);
                Inches.add(i,AppConstants.MEN_4_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 5 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_5);
                Inches.add(i,AppConstants.MEN_5_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 7 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_7);
                Inches.add(i,AppConstants.MEN_7_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 6 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_6);
                Inches.add(i,AppConstants.MEN_6_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 8 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_8);
                Inches.add(i,AppConstants.MEN_8_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 9 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_9);
                Inches.add(i,AppConstants.MEN_9_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 10 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_10);
                Inches.add(i,AppConstants.MEN_10_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 11 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_11);
                Inches.add(i,AppConstants.MEN_11_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 12 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_12);
                Inches.add(i,AppConstants.MEN_12_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 13 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_13);
                Inches.add(i,AppConstants.MEN_13_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 14 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_14);
                Inches.add(i,AppConstants.MEN_14_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 15 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_15);
                Inches.add(i,AppConstants.MEN_15_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 16 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_16);
                Inches.add(i,AppConstants.MEN_16_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 17 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_17);
                Inches.add(i,AppConstants.MEN_17_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 18 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_18);
                Inches.add(i,AppConstants.MEN_18_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 19 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_19);
                Inches.add(i,AppConstants.MEN_19_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 20 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_20);
                Inches.add(i,AppConstants.MEN_20_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 21 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_21);
                Inches.add(i,AppConstants.MEN_21_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 22 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_22);
                Inches.add(i,AppConstants.MEN_22_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 23 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_23);
                Inches.add(i,AppConstants.MEN_23_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 24 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_24);
                Inches.add(i,AppConstants.MEN_24_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 25 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_25);
                Inches.add(i,AppConstants.MEN_25_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 26 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_26);
                Inches.add(i,AppConstants.MEN_26_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 27 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_27);
                Inches.add(i,AppConstants.MEN_27_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 28 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_28);
                Inches.add(i,AppConstants.MEN_28_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 29 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_29);
                Inches.add(i,AppConstants.MEN_29_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 30 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_30);
                Inches.add(i,AppConstants.MEN_30_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 31 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_31);
                Inches.add(i,AppConstants.MEN_31_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 32 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_32);
                Inches.add(i,AppConstants.MEN_32_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 33 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_33);
                Inches.add(i,AppConstants.MEN_33_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 34 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_34);
                Inches.add(i,AppConstants.MEN_34_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 35 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_35);
                Inches.add(i,AppConstants.MEN_35_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 36 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_36);
                Inches.add(i,AppConstants.MEN_36_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 37 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_37);
                Inches.add(i,AppConstants.MEN_37_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 38 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_38);
                Inches.add(i,AppConstants.MEN_38_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 39 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_39);
                Inches.add(i,AppConstants.MEN_39_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 40 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_40);
                Inches.add(i,AppConstants.MEN_40_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 41 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_41);
                Inches.add(i,AppConstants.MEN_41_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 42 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_42);
                Inches.add(i,AppConstants.MEN_42_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 43 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_43);
                Inches.add(i,AppConstants.MEN_43_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 44 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_44);
                Inches.add(i,AppConstants.MEN_44_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 45 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_45);
                Inches.add(i,AppConstants.MEN_45_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 46 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_46);
                Inches.add(i,AppConstants.MEN_46_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 47 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_47);
                Inches.add(i,AppConstants.MEN_47_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 48 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_48);
                Inches.add(i,AppConstants.MEN_48_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 49 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_49);
                Inches.add(i,AppConstants.MEN_49_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 50 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_50);
                Inches.add(i,AppConstants.MEN_50_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 51 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_51);
                Inches.add(i,AppConstants.MEN_51_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 52 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_52);
                Inches.add(i,AppConstants.MEN_52_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 53 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_53);
                Inches.add(i,AppConstants.MEN_53_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 54 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_54);
                Inches.add(i,AppConstants.MEN_54_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 55 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_55);
                Inches.add(i,AppConstants.MEN_55_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 56 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_56);
                Inches.add(i,AppConstants.MEN_56_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 57 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_57);
                Inches.add(i,AppConstants.MEN_57_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 58 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_58);
                Inches.add(i,AppConstants.MEN_58_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 59 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_59);
                Inches.add(i,AppConstants.MEN_59_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 60 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_60);
                Inches.add(i,AppConstants.MEN_60_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 61 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_61);
                Inches.add(i,AppConstants.MEN_61_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 62 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_62);
                Inches.add(i,AppConstants.MEN_62_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 63 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_63);
                Inches.add(i,AppConstants.MEN_63_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 64 &&AppConstants.GENDER_ID.equalsIgnoreCase("1")){
                Values.add(i,AppConstants.MEN_64);
                Inches.add(i,AppConstants.MEN_64_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 65 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_65);
                Inches.add(i,AppConstants.WOMEN_65_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 66 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_66);
                Inches.add(i,AppConstants.WOMEN_66_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 67 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_67);
                Inches.add(i,AppConstants.WOMEN_67_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 68 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_68);
                Inches.add(i,AppConstants.WOMEN_68_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 69 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_69);
                Inches.add(i,AppConstants.WOMEN_69_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 70 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_70);
                Inches.add(i,AppConstants.WOMEN_70_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 71 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_71);
                Inches.add(i,AppConstants.WOMEN_71_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 72 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_72);
                Inches.add(i,AppConstants.WOMEN_72_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 73 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_73);
                Inches.add(i,AppConstants.WOMEN_73_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 74 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_74);
                Inches.add(i,AppConstants.WOMEN_74_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 75 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_75);
                Inches.add(i,AppConstants.WOMEN_75_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 76 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_76);
                Inches.add(i,AppConstants.WOMEN_76_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 77 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_77);
                Inches.add(i,AppConstants.WOMEN_77_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 78 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_78);
                Inches.add(i,AppConstants.WOMEN_78_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 79 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_79);
                Inches.add(i,AppConstants.WOMEN_79_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 80 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_80);
                Inches.add(i,AppConstants.WOMEN_80_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 81 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_81);
                Inches.add(i,AppConstants.WOMEN_81_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 82 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_82);
                Inches.add(i,AppConstants.WOMEN_82_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 83 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_83);
                Inches.add(i,AppConstants.WOMEN_83_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 84 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_84);
                Inches.add(i,AppConstants.WOMEN_84_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 85 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_85);
                Inches.add(i,AppConstants.WOMEN_85_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 86 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_86);
                Inches.add(i,AppConstants.WOMEN_86_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 87 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_87);
                Inches.add(i,AppConstants.WOMEN_87_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 88 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_88);
                Inches.add(i,AppConstants.WOMEN_88_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 89 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_89);
                Inches.add(i,AppConstants.WOMEN_89_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 90 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_90);
                Inches.add(i,AppConstants.WOMEN_90_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 91 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_91);
                Inches.add(i,AppConstants.WOMEN_91_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 92 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_92);
                Inches.add(i,AppConstants.WOMEN_92_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 93 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_93);
                Inches.add(i,AppConstants.WOMEN_93_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 94 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_94);
                Inches.add(i,AppConstants.WOMEN_94_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 95 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_95);
                Inches.add(i,AppConstants.WOMEN_95_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 96 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_96);
                Inches.add(i,AppConstants.WOMEN_96_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 97 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_97);
                Inches.add(i,AppConstants.WOMEN_97_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 98 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_98);
                Inches.add(i,AppConstants.WOMEN_98_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 99 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_99);
                Inches.add(i,AppConstants.WOMEN_99_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 100 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_100);
                Inches.add(i,AppConstants.WOMEN_100_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 101 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_101);
                Inches.add(i,AppConstants.WOMEN_101_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 102 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_102);
                Inches.add(i,AppConstants.WOMEN_102_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 103 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_103);
                Inches.add(i,AppConstants.WOMEN_103_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 104 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_104);
                Inches.add(i,AppConstants.WOMEN_104_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 105 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_105);
                Inches.add(i,AppConstants.WOMEN_105_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 106 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_106);
                Inches.add(i,AppConstants.WOMEN_106_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 107 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_107);
                Inches.add(i,AppConstants.WOMEN_107_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 108 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_108);
                Inches.add(i,AppConstants.WOMEN_108_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 109 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_109);
                Inches.add(i,AppConstants.WOMEN_109_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 110 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_110);
                Inches.add(i,AppConstants.WOMEN_110_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 111 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_111);
                Inches.add(i,AppConstants.WOMEN_111_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 112 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_112);
                Inches.add(i,AppConstants.WOMEN_112_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 113 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_113);
                Inches.add(i,AppConstants.WOMEN_113_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 114 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_114);
                Inches.add(i,AppConstants.WOMEN_114_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 115 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_115);
                Inches.add(i,AppConstants.WOMEN_115_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 116 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_116);
                Inches.add(i,AppConstants.WOMEN_116_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 117 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_117);
                Inches.add(i,AppConstants.WOMEN_117_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 118 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_118);
                Inches.add(i,AppConstants.WOMEN_118_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 119 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_119);
                Inches.add(i,AppConstants.WOMEN_119_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 120 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_120);
                Inches.add(i,AppConstants.WOMEN_120_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 121 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_121);
                Inches.add(i,AppConstants.WOMEN_121_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 122 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_122);
                Inches.add(i,AppConstants.WOMEN_122_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 123 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_123);
                Inches.add(i,AppConstants.WOMEN_123_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 124 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_124);
                Inches.add(i,AppConstants.WOMEN_124_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 125 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_125);
                Inches.add(i,AppConstants.WOMEN_125_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 126 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_126);
                Inches.add(i,AppConstants.WOMEN_126_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 127 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_127);
                Inches.add(i,AppConstants.WOMEN_127_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 128 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_128);
                Inches.add(i,AppConstants.WOMEN_128_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 129 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_129);
                Inches.add(i,AppConstants.WOMEN_129_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 130 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_130);
                Inches.add(i,AppConstants.WOMEN_130_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 131 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_131);
                Inches.add(i,AppConstants.WOMEN_131_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 132 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_132);
                Inches.add(i,AppConstants.WOMEN_132_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 133 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_133);
                Inches.add(i,AppConstants.WOMEN_133_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 134 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_134);
                Inches.add(i,AppConstants.WOMEN_134_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 135 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_135);
                Inches.add(i,AppConstants.WOMEN_135_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 136 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_136);
                Inches.add(i,AppConstants.WOMEN_136_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 137 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_137);
                Inches.add(i,AppConstants.WOMEN_137_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 138 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_138);
                Inches.add(i,AppConstants.WOMEN_138_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 139 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_139);
                Inches.add(i,AppConstants.WOMEN_139_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 140 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_140);
                Inches.add(i,AppConstants.WOMEN_140_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 141 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_141);
                Inches.add(i,AppConstants.WOMEN_141_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 142 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_142);
                Inches.add(i,AppConstants.WOMEN_142_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 143 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_143);
                Inches.add(i,AppConstants.WOMEN_143_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }

            else if (mParts.get(i).getId() == 144 &&AppConstants.GENDER_ID.equalsIgnoreCase("2")){
                Values.add(i,AppConstants.WOMEN_144);
                Inches.add(i,AppConstants.WOMEN_144_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 145 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_145);
                Inches.add(i,AppConstants.BOY_145_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 146 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_146);
                Inches.add(i,AppConstants.BOY_146_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 147 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_147);
                Inches.add(i,AppConstants.BOY_147_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 148 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_148);
                Inches.add(i,AppConstants.BOY_148_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 149 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_149);
                Inches.add(i,AppConstants.BOY_149_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 150 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_150);
                Inches.add(i,AppConstants.BOY_150_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 151 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_151);
                Inches.add(i,AppConstants.BOY_151_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 152 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_152);
                Inches.add(i,AppConstants.BOY_152_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 153 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_153);
                Inches.add(i,AppConstants.BOY_153_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 154 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_154);
                Inches.add(i,AppConstants.BOY_154_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 155 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_155);
                Inches.add(i,AppConstants.BOY_155_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 156 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_156);
                Inches.add(i,AppConstants.BOY_156_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 157 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_157);
                Inches.add(i,AppConstants.BOY_157_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 158 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_158);
                Inches.add(i,AppConstants.BOY_158_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 159 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_159);
                Inches.add(i,AppConstants.BOY_159_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 160 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_160);
                Inches.add(i,AppConstants.BOY_160_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 161 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_161);
                Inches.add(i,AppConstants.BOY_161_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 162 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_162);
                Inches.add(i,AppConstants.BOY_162_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 163 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_163);
                Inches.add(i,AppConstants.BOY_163_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 164 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_164);
                Inches.add(i,AppConstants.BOY_164_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 165 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_165);
                Inches.add(i,AppConstants.BOY_165_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 166 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_166);
                Inches.add(i,AppConstants.BOY_166_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 167 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_167);
                Inches.add(i,AppConstants.BOY_167_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 168 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_168);
                Inches.add(i,AppConstants.BOY_168_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 169 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_169);
                Inches.add(i,AppConstants.BOY_169_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 170 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_170);
                Inches.add(i,AppConstants.BOY_170_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 171 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_171);
                Inches.add(i,AppConstants.BOY_171_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 172 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_172);
                Inches.add(i,AppConstants.BOY_172_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 173 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_173);
                Inches.add(i,AppConstants.BOY_173_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 174 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_174);
                Inches.add(i,AppConstants.BOY_174_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 175 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_175);
                Inches.add(i,AppConstants.BOY_175_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 176 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_176);
                Inches.add(i,AppConstants.BOY_176_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 177 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_177);
                Inches.add(i,AppConstants.BOY_177_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 178 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_178);
                Inches.add(i,AppConstants.BOY_178_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 179 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_179);
                Inches.add(i,AppConstants.BOY_179_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 180 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_180);
                Inches.add(i,AppConstants.BOY_180_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 181 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_181);
                Inches.add(i,AppConstants.BOY_181_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 182 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_182);
                Inches.add(i,AppConstants.BOY_182_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 183 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_183);
                Inches.add(i,AppConstants.BOY_183_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 184 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_184);
                Inches.add(i,AppConstants.BOY_184_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 185 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_185);
                Inches.add(i,AppConstants.BOY_185_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 186 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_186);
                Inches.add(i,AppConstants.BOY_186_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 187 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_187);
                Inches.add(i,AppConstants.BOY_187_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 188 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_188);
                Inches.add(i,AppConstants.BOY_188_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 189 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_189);
                Inches.add(i,AppConstants.BOY_189_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 190 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_190);
                Inches.add(i,AppConstants.BOY_190_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 191 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_191);
                Inches.add(i,AppConstants.BOY_191_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 192 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_192);
                Inches.add(i,AppConstants.BOY_192_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 193 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_193);
                Inches.add(i,AppConstants.BOY_193_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 194 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_194);
                Inches.add(i,AppConstants.BOY_194_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 195 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_195);
                Inches.add(i,AppConstants.BOY_195_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 196 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_196);
                Inches.add(i,AppConstants.BOY_196_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 197 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_197);
                Inches.add(i,AppConstants.BOY_197_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 198 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_198);
                Inches.add(i,AppConstants.BOY_198_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 199 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_199);
                Inches.add(i,AppConstants.BOY_199_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 200 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_200);
                Inches.add(i,AppConstants.BOY_200_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 201 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_201);
                Inches.add(i,AppConstants.BOY_201_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 202 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_202);
                Inches.add(i,AppConstants.BOY_202_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 203 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_203);
                Inches.add(i,AppConstants.BOY_203_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 204 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_204);
                Inches.add(i,AppConstants.BOY_204_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 205 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_205);
                Inches.add(i,AppConstants.BOY_205_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 206 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_206);
                Inches.add(i,AppConstants.BOY_206_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 207 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_207);
                Inches.add(i,AppConstants.BOY_207_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 208 &&AppConstants.GENDER_ID.equalsIgnoreCase("3")){
                Values.add(i,AppConstants.BOY_208);
                Inches.add(i,AppConstants.BOY_208_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 209 &&AppConstants.GENDER_ID.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_209);
                Inches.add(i,AppConstants.GIRL_209_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 210 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_210);
                Inches.add(i,AppConstants.GIRL_210_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 211 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_211);
                Inches.add(i,AppConstants.GIRL_211_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 212 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_212);
                Inches.add(i,AppConstants.GIRL_212_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 213 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_213);
                Inches.add(i,AppConstants.GIRL_213_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 214 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_214);
                Inches.add(i,AppConstants.GIRL_214_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 215 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_215);
                Inches.add(i,AppConstants.GIRL_215_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 216 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_216);
                Inches.add(i,AppConstants.GIRL_216_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 217 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_217);
                Inches.add(i,AppConstants.GIRL_217_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 218 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_218);
                Inches.add(i,AppConstants.GIRL_218_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 219 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_219);
                Inches.add(i,AppConstants.GIRL_219_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 220 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_220);
                Inches.add(i,AppConstants.GIRL_220_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 221 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_221);
                Inches.add(i,AppConstants.GIRL_221_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 222 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_222);
                Inches.add(i,AppConstants.GIRL_222_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 223 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_223);
                Inches.add(i,AppConstants.GIRL_223_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 224 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_224);
                Inches.add(i,AppConstants.GIRL_224_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 225 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_225);
                Inches.add(i,AppConstants.GIRL_225_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 226 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_226);
                Inches.add(i,AppConstants.GIRL_226_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 227 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_227);
                Inches.add(i,AppConstants.GIRL_227_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 228 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_228);
                Inches.add(i,AppConstants.GIRL_228_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 229 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_229);
                Inches.add(i,AppConstants.GIRL_229_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 230 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_230);
                Inches.add(i,AppConstants.GIRL_230_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 231 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_231);
                Inches.add(i,AppConstants.GIRL_231_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 232 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_232);
                Inches.add(i,AppConstants.GIRL_232_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 233 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_233);
                Inches.add(i,AppConstants.GIRL_233_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 234 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_234);
                Inches.add(i,AppConstants.GIRL_234_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 235 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_235);
                Inches.add(i,AppConstants.GIRL_235_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 236 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_236);
                Inches.add(i,AppConstants.GIRL_236_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 237 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_237);
                Inches.add(i,AppConstants.GIRL_237_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 238 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_238);
                Inches.add(i,AppConstants.GIRL_238_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 239 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_239);
                Inches.add(i,AppConstants.GIRL_239_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 240 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_240);
                Inches.add(i,AppConstants.GIRL_240_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 241 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_241);
                Inches.add(i,AppConstants.GIRL_241_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 242 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_242);
                Inches.add(i,AppConstants.GIRL_242_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 243 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_243);
                Inches.add(i,AppConstants.GIRL_243_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 244 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_244);
                Inches.add(i,AppConstants.GIRL_244_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 245 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_245);
                Inches.add(i,AppConstants.GIRL_245_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 246 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_246);
                Inches.add(i,AppConstants.GIRL_246_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 247 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_247);
                Inches.add(i,AppConstants.GIRL_247_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 248 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_248);
                Inches.add(i,AppConstants.GIRL_248_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 249 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_249);
                Inches.add(i,AppConstants.GIRL_249_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 250 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_250);
                Inches.add(i,AppConstants.GIRL_250_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 251 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_251);
                Inches.add(i,AppConstants.GIRL_251_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 252 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_252);
                Inches.add(i,AppConstants.GIRL_252_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 253 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_253);
                Inches.add(i,AppConstants.GIRL_253_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 254 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_254);
                Inches.add(i,AppConstants.GIRL_254_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 255 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_255);
                Inches.add(i,AppConstants.GIRL_255_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 256 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_256);
                Inches.add(i,AppConstants.GIRL_256_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 257 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_257);
                Inches.add(i,AppConstants.GIRL_257_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 258 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_258);
                Inches.add(i,AppConstants.GIRL_258_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 259 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_259);
                Inches.add(i,AppConstants.GIRL_259_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 260 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_260);
                Inches.add(i,AppConstants.GIRL_260_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 261 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_261);
                Inches.add(i,AppConstants.GIRL_261_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 262 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_262);
                Inches.add(i,AppConstants.GIRL_262_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 263 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_263);
                Inches.add(i,AppConstants.GIRL_263_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 264 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_264);
                Inches.add(i,AppConstants.GIRL_264_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 265 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_265);
                Inches.add(i,AppConstants.GIRL_265_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 266 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_266);
                Inches.add(i,AppConstants.GIRL_266_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 267 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_267);
                Inches.add(i,AppConstants.GIRL_267_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 268 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_268);
                Inches.add(i,AppConstants.GIRL_268_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 269 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_269);
                Inches.add(i,AppConstants.GIRL_269_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 270 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_270);
                Inches.add(i,AppConstants.GIRL_270_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 271 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_271);
                Inches.add(i,AppConstants.GIRL_271_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 272 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_272);
                Inches.add(i,AppConstants.GIRL_272_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 273 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_273);
                Inches.add(i,AppConstants.GIRL_273_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 274 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_274);
                Inches.add(i,AppConstants.GIRL_274_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 275 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_275);
                Inches.add(i,AppConstants.GIRL_275_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 276 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_276);
                Inches.add(i,AppConstants.GIRL_276_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 277 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_277);
                Inches.add(i,AppConstants.GIRL_277_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 278 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_278);
                Inches.add(i,AppConstants.GIRL_278_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 279 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_279);
                Inches.add(i,AppConstants.GIRL_279_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 280 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_280);
                Inches.add(i,AppConstants.GIRL_280_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 281 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_281);
                Inches.add(i,AppConstants.GIRL_281_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 282 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_282);
                Inches.add(i,AppConstants.GIRL_282_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 283 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_283);
                Inches.add(i,AppConstants.GIRL_283_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 284 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_284);
                Inches.add(i,AppConstants.GIRL_284_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 285 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_285);
                Inches.add(i,AppConstants.GIRL_285_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 286 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_286);
                Inches.add(i,AppConstants.GIRL_286_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 287 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_287);
                Inches.add(i,AppConstants.GIRL_287_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 288 &&AppConstants.GENDER_ID.equalsIgnoreCase("4")){
                Values.add(i,AppConstants.GIRL_288);
                Inches.add(i,AppConstants.GIRL_288_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
        }
    }
    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }
}