package com.qoltech.mzyoonbuyer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.GenderEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OfferGenderAdapter extends RecyclerView.Adapter<OfferGenderAdapter.Holder> {

    private Context mContext;
    private ArrayList<GenderEntity> mGenderList;

    private UserDetailsEntity mUserDetailsEntityRes;

    public OfferGenderAdapter(Context activity, ArrayList<GenderEntity> genderList) {
        mContext = activity;
        mGenderList = genderList;
    }

    @NonNull
    @Override
    public OfferGenderAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_offer_gender, parent, false);
        return new OfferGenderAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OfferGenderAdapter.Holder holder, int position) {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final GenderEntity genderEntity = mGenderList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {

            holder.mFilterTypeNameTxt.setText(genderEntity.getGenderInArabic());
            ViewCompat.setLayoutDirection(holder.itemView.findViewById(R.id.filter_check_box_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        } else {
            holder.mFilterTypeNameTxt.setText(genderEntity.getGender());
            ViewCompat.setLayoutDirection(holder.itemView.findViewById(R.id.filter_check_box_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);


        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mGenderList.get(position).isChecked()){
                    mGenderList.get(position).setChecked(false);

                }else {
                    mGenderList.get(position).setChecked(true);

                }
                notifyDataSetChanged();
            }

        });
        holder.mFilterCheckBoxImg.setBackground(mGenderList.get(position).isChecked() ? mContext.getResources().getDrawable(R.drawable.tick_selected_img) : mContext.getResources().getDrawable(R.drawable.tick_unselected_img));
    }

    @Override
    public int getItemCount() {
        return mGenderList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {


        @BindView(R.id.filter_type_recycler_view_img)
        ImageView mFilterCheckBoxImg;

        @BindView(R.id.filter_type_recycler_view_txt)
        TextView mFilterTypeNameTxt;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
