package com.qoltech.mzyoonbuyer.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.OrderDeliveryListAdapter;
import com.qoltech.mzyoonbuyer.adapter.OrderPendingListAdapter;
import com.qoltech.mzyoonbuyer.entity.OrderDeliveryListEntity;
import com.qoltech.mzyoonbuyer.entity.OrderListPendingEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseFragment;
import com.qoltech.mzyoonbuyer.modal.OrderDeliveryListResponse;
import com.qoltech.mzyoonbuyer.modal.OrderListPendingResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderListFragment extends BaseFragment {

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.profile_image_circle_img)
    de.hdodenhof.circleimageview.CircleImageView mHomeCircleImg;

    @BindView(R.id.order_list_delivery_recycler_view)
    RecyclerView mOrderListDeliveryRecyclerView;

    @BindView(R.id.order_list_pending_recycler_view)
    RecyclerView mOrderListPendingRecyclerView;

    @BindView(R.id.order_delivery_list_scroll_view_img)
    ImageView mOrderDeliveryListScrollViewImg;

    @BindView(R.id.order_pending_list_par_lay)
    RelativeLayout mOrderPendingListParLay;

    @BindView(R.id.order_delivery_list_par_lay)
    RelativeLayout mOrderDeliveryListParLay;

    @BindView(R.id.order_delivered_lay)
    RelativeLayout mOrderDeliveredLay;

    @BindView(R.id.order_pending_lay)
    RelativeLayout mOrderPendingLay;

    @BindView(R.id.order_delivered_txt)
    TextView mOrderDeliveredTxt;

    @BindView(R.id.order_pending_txt)
    TextView mOrderPendingTxt;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    @BindView(R.id.order_pending_list_scroll_view_img)
    ImageView mOrderListScrollViewImg;

    private OrderDeliveryListAdapter mOrderListAdapter;

    private OrderPendingListAdapter mOrderPendingListAdapter;

    private UserDetailsEntity mUserDetailsEntityRes;

    private ArrayList<OrderListPendingEntity> mOrderListPendingList;

    private ArrayList<OrderDeliveryListEntity> mOrderListDeliveryList;

    boolean mPendingLoader = false, mDeliveryLoader = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = layoutInflater.inflate(R.layout.frag_order_list, container, false);

        ButterKnife.bind(this, rootView);

        initView();

        return rootView;
    }

    /*InitViews*/
    private void initView() {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(getContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        mOrderListPendingList = new ArrayList<>();
        getOrderPendingListApiCall();
        getOrderDeliveryDetailsList();

        setHeader();

        if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            mOrderPendingLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
            mOrderPendingTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mOrderDeliveredLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
            mOrderDeliveredTxt.setTextColor(getResources().getColor(R.color.white));
        }else {
            mOrderPendingLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
            mOrderPendingTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
            mOrderDeliveredLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
            mOrderDeliveredTxt.setTextColor(getResources().getColor(R.color.white));
        }

    }
    @OnClick({R.id.order_pending_lay,R.id.order_delivered_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.order_pending_lay:
                mEmptyListTxt.setText(getResources().getString(R.string.no_result_for_pending_order));

                mOrderPendingListParLay.setVisibility(mOrderListPendingList.size()>0 ? View.VISIBLE : View.GONE);
                mEmptyListLay.setVisibility(mOrderListPendingList.size() > 0 ? View.GONE : View.VISIBLE);
                mOrderDeliveryListParLay.setVisibility(View.GONE);

                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mOrderPendingLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                    mOrderPendingTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                    mOrderDeliveredLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                    mOrderDeliveredTxt.setTextColor(getResources().getColor(R.color.white));
                }else {
                    mOrderPendingLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
                    mOrderPendingTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                    mOrderDeliveredLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
                    mOrderDeliveredTxt.setTextColor(getResources().getColor(R.color.white));
                }

//                mOrderDeliveredLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
//                mOrderPendingLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
//
//                mOrderDeliveredTxt.setTextColor(this.getResources().getColor(R.color.white));
//                mOrderPendingTxt.setTextColor(this.getResources().getColor(R.color.app_blue_clr));

                break;
            case R.id.order_delivered_lay:
                mEmptyListTxt.setText(getResources().getString(R.string.no_result_for_delivered_order));

                mOrderDeliveryListParLay.setVisibility(mOrderListDeliveryList.size()>0 ? View.VISIBLE : View.GONE);
                mEmptyListLay.setVisibility(mOrderListDeliveryList.size() > 0 ? View.GONE : View.VISIBLE);
                mOrderPendingListParLay.setVisibility(View.GONE);

                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mOrderPendingLay.setBackgroundResource(R.drawable.grey_clr_with_right_corner);
                    mOrderPendingTxt.setTextColor(getResources().getColor(R.color.white));
                    mOrderDeliveredLay.setBackgroundResource(R.drawable.yellow_gradient_with_left_corner);
                    mOrderDeliveredTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                }else {
                    mOrderPendingLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
                    mOrderPendingTxt.setTextColor(getResources().getColor(R.color.white));
                    mOrderDeliveredLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
                    mOrderDeliveredTxt.setTextColor(getResources().getColor(R.color.app_blue_clr));
                }

//                mOrderDeliveredLay.setBackgroundResource(R.drawable.yellow_gradient_with_right_corner);
//                mOrderPendingLay.setBackgroundResource(R.drawable.grey_clr_with_left_corner);
//
//                mOrderDeliveredTxt.setTextColor(this.getResources().getColor(R.color.app_blue_clr));
//                mOrderPendingTxt.setTextColor(this.getResources().getColor(R.color.white));

                break;
        }
    }

    public void getOrderPendingListApiCall(){
        if (NetworkUtil.isNetworkAvailable(getContext())){
            APIRequestHandler.getInstance().getOrderPendingListApi(mUserDetailsEntityRes.getUSER_ID(),OrderListFragment.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(getContext(), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getOrderPendingListApiCall();
                }
            });
        }
    }
    public void getOrderDeliveryDetailsList(){
        if (NetworkUtil.isNetworkAvailable(getContext())){
            APIRequestHandler.getInstance().getOrderDeliveryListApiCall(mUserDetailsEntityRes.getUSER_ID(),OrderListFragment.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(getContext(), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getOrderDeliveryDetailsList();
                }
            });
        }
    }
    public void setHeader() {
        if (!mUserDetailsEntityRes.getUSER_ID().equalsIgnoreCase("0")) {
            mHomeCircleImg.setVisibility(View.VISIBLE);

        }
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.list_or_orders));
        mEmptyListTxt.setText(getResources().getString(R.string.no_result_for_pending_order));

        try {
            Glide.with(this)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/BuyerImages/"+ mUserDetailsEntityRes.getUSER_IMAGE())
                    .apply(new RequestOptions().placeholder(R.drawable.profile_place_holder_icon).error(R.drawable.profile_place_holder_icon).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true))
                    .into(mHomeCircleImg);
        } catch (Exception ex) {
        }
    }
    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<OrderDeliveryListEntity> orderDeliveryList) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        mOrderDeliveryListParLay.setVisibility(orderDeliveryList.size()>0 ? View.VISIBLE : View.GONE);
            mEmptyListLay.setVisibility(orderDeliveryList.size() > 0 ? View.GONE : View.VISIBLE);

            mOrderListAdapter = new OrderDeliveryListAdapter(getContext(),orderDeliveryList);
            mOrderListDeliveryRecyclerView.setLayoutManager(layoutManager);
            mOrderListDeliveryRecyclerView.setAdapter(mOrderListAdapter);

        mOrderListDeliveryRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPosition = layoutManager.findLastVisibleItemPosition();

                    if (lastPosition == orderDeliveryList.size()-1){
                        mOrderDeliveryListScrollViewImg.setVisibility(View.GONE);
                    }else {
                        mOrderDeliveryListScrollViewImg.setVisibility(View.VISIBLE);
                    }
                }
        });
        mDeliveryLoader = true;
        if (mPendingLoader&&mDeliveryLoader){
            DialogManager.getInstance().hideProgress();
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof OrderListPendingResponse){
            OrderListPendingResponse mResponse = (OrderListPendingResponse)resObj;
            mOrderListPendingList = mResponse.getResult();
            setPendingAdapter(mOrderListPendingList);
        }
        if (resObj instanceof OrderDeliveryListResponse){
            OrderDeliveryListResponse mResponse = (OrderDeliveryListResponse)resObj;
            mOrderListDeliveryList = mResponse.getResult();
            setAdapter(mResponse.getResult());

        }
    }

    public void setPendingAdapter(ArrayList<OrderListPendingEntity> orderPendingEntity) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        mOrderPendingListParLay.setVisibility(orderPendingEntity.size()>0 ? View.VISIBLE : View.GONE);
        mEmptyListLay.setVisibility(orderPendingEntity.size() > 0 ? View.GONE : View.VISIBLE);

        mOrderPendingListAdapter = new OrderPendingListAdapter(getContext(),orderPendingEntity);
        mOrderListPendingRecyclerView.setLayoutManager(layoutManager);
        mOrderListPendingRecyclerView.setAdapter(mOrderPendingListAdapter);

        mOrderListPendingRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = layoutManager.findLastVisibleItemPosition();

                if (lastPosition == orderPendingEntity.size()-1){
                    mOrderListScrollViewImg.setVisibility(View.GONE);
                }else {
                    mOrderListScrollViewImg.setVisibility(View.VISIBLE);
                }
            }
        });
        mPendingLoader = true;
        if (mPendingLoader&&mDeliveryLoader){
            DialogManager.getInstance().hideProgress();
        }
    }
}