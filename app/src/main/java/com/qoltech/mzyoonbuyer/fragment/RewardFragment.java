package com.qoltech.mzyoonbuyer.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseFragment;
import com.qoltech.mzyoonbuyer.modal.GetReferalCodeResponse;
import com.qoltech.mzyoonbuyer.modal.GetRewardsHomeResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.ui.LoginScreen;
import com.qoltech.mzyoonbuyer.ui.OfferAndPromotionScreen;
import com.qoltech.mzyoonbuyer.ui.RewardHistoryScreen;
import com.qoltech.mzyoonbuyer.ui.RewardScreen;
import com.qoltech.mzyoonbuyer.ui.ScratchListScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RewardFragment extends BaseFragment {

    @BindView(R.id.reward_par_lay)
    LinearLayout mRewardsParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.profile_image_circle_img)
    de.hdodenhof.circleimageview.CircleImageView mHomeCircleImg;

    @BindView(R.id.reward_and_offer_reward_txt)
    TextView mRewardOfferRewardTxt;

    @BindView(R.id.reward_and_offer_scratch_txt)
    TextView mRewardOfferScratchTxt;

    @BindView(R.id.reward_and_offer_offers_txt)
    TextView mRewardOfferOfferTxt;

    @BindView(R.id.reward_and_offer_invite_txt)
    TextView mRewardOfferInviteTxt;

    @BindView(R.id.reward_and_offer_reward_img)
    ImageView mRewardOfferRewardImg;

    @BindView(R.id.reward_and_offer_scratch_img)
    ImageView mRewardOfferScratchImg;

    @BindView(R.id.reward_and_offer_offers_img)
    ImageView mRewardOfferOfferImg;

    @BindView(R.id.reward_and_offer_invite_img)
    ImageView mRewardOfferInviteImg;

    @BindView(R.id.reward_and_offer_reward_count_txt)
    TextView mRewardAndOfferRewardCountTxt;

    @BindView(R.id.reward_and_offer_scratch_count_txt)
    TextView mRewardAndOfferScratchCountTxt;

    private UserDetailsEntity mUserDetailsEntityRes;

    String mInviteCodeStr = "";

    @Override
    public View onCreateView(@NonNull LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = layoutInflater.inflate(R.layout.frag_reward_screen, container, false);
        ButterKnife.bind(this, rootView);

        initView();

        return rootView;
    }

    public void initView(){

        setupUI(mRewardsParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(getContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        setHeader();

        if (!mUserDetailsEntityRes.getUSER_ID().equalsIgnoreCase("0")){
            getReferalCodeApiCall();

        }
        getRewardHomeApiCall(mUserDetailsEntityRes.getUSER_ID());

    }

    public void setHeader() {

        if (AppConstants.REWARDS_AND_OFFERS_BACK.equalsIgnoreCase("BACK_BTN")){
            mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
                mHomeCircleImg.setVisibility(View.GONE);

        }else {
            mHeaderLeftBackBtn.setVisibility(View.GONE);
            if (!mUserDetailsEntityRes.getUSER_ID().equalsIgnoreCase("0")) {
                mHomeCircleImg.setVisibility(View.VISIBLE);

            }
        }
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.refer_and_earn));

        try {
            Glide.with(this)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/BuyerImages/"+ mUserDetailsEntityRes.getUSER_IMAGE())
                    .apply(new RequestOptions().placeholder(R.drawable.profile_place_holder_icon).error(R.drawable.profile_place_holder_icon).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true))
                    .into(mHomeCircleImg);
        } catch (Exception ex) {
        }
    }
    public void getRewardHomeApiCall(String UserId){
        if (NetworkUtil.isNetworkAvailable(getContext())){
            APIRequestHandler.getInstance().getRewarHomeApi(UserId,RewardFragment.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(getContext(), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getRewardHomeApiCall(UserId);
                }
            });
        }
    }
    public void getReferalCodeApiCall(){
        if (NetworkUtil.isNetworkAvailable(getActivity())){
            APIRequestHandler.getInstance().getReferalCodeApiCall(mUserDetailsEntityRes.getUSER_ID(),RewardFragment.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(getActivity(), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getReferalCodeApiCall();

                }
            });
        }
    }

    @OnClick({R.id.header_left_side_img,R.id.reward_reward_history_card_view,R.id.reward_scratch_card_view,R.id.reward_inviter_buyer_lay,R.id.reward_invite_tailor_lay,R.id.reward_offer_and_promotion_card_view})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                ((RewardScreen)getContext()).onBackPressed();
                break;
            case R.id.reward_reward_history_card_view:
                if (mUserDetailsEntityRes.getUSER_ID().equalsIgnoreCase("0")) {
                    DialogManager.getInstance().showOptionPopup(getContext(), getResources().getString(R.string.please_login_to_proceed), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                        @Override
                        public void onNegativeClick() {

                        }
                        @Override
                        public void onPositiveClick() {
                            logOut();
                            HashMap<String,String> productIdist = new HashMap<>();
                            PreferenceUtil.storeRecentProducts(getContext(),productIdist);
                            PreferenceUtil.storeBoolPreferenceValue(getContext(),AppConstants.LOGIN_STATUS,false);
//                            PreferenceUtil.clearPreference(BottomNavigationScreen.this);
                            nextScreenWithFinish(LoginScreen.class,false);

                        }
                    });
                }else {
                    nextScreenWithOutFinish(RewardHistoryScreen.class,true);
                }
                break;
            case R.id.reward_scratch_card_view:
                if (mUserDetailsEntityRes.getUSER_ID().equalsIgnoreCase("0")) {
                    DialogManager.getInstance().showOptionPopup(getContext(), getResources().getString(R.string.please_login_to_proceed), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                        @Override
                        public void onNegativeClick() {

                        }
                        @Override
                        public void onPositiveClick() {
                            logOut();
                            HashMap<String,String> productIdist = new HashMap<>();
                            PreferenceUtil.storeRecentProducts(getContext(),productIdist);
                            PreferenceUtil.storeBoolPreferenceValue(getContext(),AppConstants.LOGIN_STATUS,false);
//                            PreferenceUtil.clearPreference(BottomNavigationScreen.this);
                            nextScreenWithFinish(LoginScreen.class,false);

                        }
                    });
                }else {
                    nextScreenWithOutFinish(ScratchListScreen.class,true);
                }
                break;
            case R.id.reward_invite_tailor_lay:
                if (mUserDetailsEntityRes.getUSER_ID().equalsIgnoreCase("0")) {
                    DialogManager.getInstance().showOptionPopup(getContext(), getResources().getString(R.string.please_login_to_proceed), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                        @Override
                        public void onNegativeClick() {

                        }
                        @Override
                        public void onPositiveClick() {
                            logOut();
                            HashMap<String,String> productIdist = new HashMap<>();
                            PreferenceUtil.storeRecentProducts(getContext(),productIdist);
                            PreferenceUtil.storeBoolPreferenceValue(getContext(),AppConstants.LOGIN_STATUS,false);
//                            PreferenceUtil.clearPreference(BottomNavigationScreen.this);
                            nextScreenWithFinish(LoginScreen.class,false);

                        }
                    });
                }else {
                    String shareBody = getResources().getString(R.string.share_link_buyer)+ mInviteCodeStr + "\n\n" + "http://onelink.to/mzyoon" +"\n";
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.invite_friend_and_get_point));
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.app_name)));
                }
                break;
            case R.id.reward_inviter_buyer_lay:
                if (mUserDetailsEntityRes.getUSER_ID().equalsIgnoreCase("0")) {
                    DialogManager.getInstance().showOptionPopup(getContext(), getResources().getString(R.string.please_login_to_proceed), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                        @Override
                        public void onNegativeClick() {

                        }
                        @Override
                        public void onPositiveClick() {
                            logOut();
                            HashMap<String,String> productIdist = new HashMap<>();
                            PreferenceUtil.storeRecentProducts(getContext(),productIdist);
                            PreferenceUtil.storeBoolPreferenceValue(getContext(),AppConstants.LOGIN_STATUS,false);
//                            PreferenceUtil.clearPreference(BottomNavigationScreen.this);
                            nextScreenWithFinish(LoginScreen.class,false);

                        }
                    });
                }else {
                    String shareBodys = getResources().getString(R.string.share_link_tailor)+ "\n\n" + "http://onelink.to/mzyoon" +"\n";
                    Intent sharingIntents = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntents.setType("text/plain");
                    sharingIntents.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.invite_friend_and_get_point));
                    sharingIntents.putExtra(android.content.Intent.EXTRA_TEXT, shareBodys);
                    startActivity(Intent.createChooser(sharingIntents, getResources().getString(R.string.app_name)));
                }
                break;
            case R.id.reward_offer_and_promotion_card_view:
                AppConstants.CHECK_OUT_OFFERS = "Tap to apply";
                nextScreenWithOutFinish(OfferAndPromotionScreen.class,true);
                break;
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetReferalCodeResponse){
            GetReferalCodeResponse mResponse = (GetReferalCodeResponse)resObj;
            mInviteCodeStr = mResponse.getResult().getInvitecode();
        }
        if (resObj instanceof GetRewardsHomeResponse){
            GetRewardsHomeResponse mResponse = (GetRewardsHomeResponse)resObj;

            if (mResponse.getResult().getGetRewardpoints().size() > 0){
                try {
                    mRewardAndOfferRewardCountTxt.setText(String.valueOf(mResponse.getResult().getGetRewardpoints().get(0).getRewardpoints())+ " "+ getResources().getString(R.string.points));

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
            }
            if (mResponse.getResult().getGetScratchCardCount().size() > 0){
                try {
                    mRewardAndOfferScratchCountTxt.setText(String.valueOf(mResponse.getResult().getGetScratchCardCount().get(0).getScratchCardCount()) + " "+ getResources().getString(R.string.cards));

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
            }
            for (int i=0; i<mResponse.getResult().getGetRewardsAndOffer().size(); i++){
                if (i == 0){
                    try {
                        Glide.with(getContext())
                                .load(AppConstants.IMAGE_BASE_URL+"Images/RewardsAndOfferHome/"+mResponse.getResult().getGetRewardsAndOffer().get(0).getImage())
                                .apply(new RequestOptions().placeholder(R.drawable.empty_img).error(R.drawable.empty_img))
                                .into(mRewardOfferRewardImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        mRewardOfferRewardTxt.setText(mResponse.getResult().getGetRewardsAndOffer().get(0).getNameInArabic());
                        mRewardOfferRewardImg.setRotationY(180);

                    }else {
                        mRewardOfferRewardTxt.setText(mResponse.getResult().getGetRewardsAndOffer().get(0).getName());

                    }

                }else if (i == 1){
                    try {
                        Glide.with(getContext())
                                .load(AppConstants.IMAGE_BASE_URL+"Images/RewardsAndOfferHome/"+mResponse.getResult().getGetRewardsAndOffer().get(1).getImage())
                                .apply(new RequestOptions().placeholder(R.drawable.empty_img).error(R.drawable.empty_img))
                                .into(mRewardOfferScratchImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        mRewardOfferScratchTxt.setText(mResponse.getResult().getGetRewardsAndOffer().get(1).getNameInArabic());
                        mRewardOfferScratchImg.setRotationY(180);

                    }else {
                        mRewardOfferScratchTxt.setText(mResponse.getResult().getGetRewardsAndOffer().get(1).getName());

                    }

                }else if (i == 2){
                    try {
                        Glide.with(getContext())
                                .load(AppConstants.IMAGE_BASE_URL+"Images/RewardsAndOfferHome/"+mResponse.getResult().getGetRewardsAndOffer().get(2).getImage())
                                .apply(new RequestOptions().placeholder(R.drawable.empty_img).error(R.drawable.empty_img))
                                .into(mRewardOfferOfferImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        mRewardOfferOfferTxt.setText(mResponse.getResult().getGetRewardsAndOffer().get(2).getNameInArabic());
                        mRewardOfferOfferImg.setRotationY(180);
                    }else {
                        mRewardOfferOfferTxt.setText(mResponse.getResult().getGetRewardsAndOffer().get(2).getName());

                    }

                }else if (i == 3){
                    try {
                        Glide.with(getContext())
                                .load(AppConstants.IMAGE_BASE_URL+"Images/RewardsAndOfferHome/"+mResponse.getResult().getGetRewardsAndOffer().get(3).getImage())
                                .apply(new RequestOptions().placeholder(R.drawable.empty_img).error(R.drawable.empty_img))
                                .into(mRewardOfferInviteImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        mRewardOfferInviteTxt.setText(mResponse.getResult().getGetRewardsAndOffer().get(3).getNameInArabic());
                        mRewardOfferInviteImg.setRotationY(180);

                    }else {
                        mRewardOfferInviteTxt.setText(mResponse.getResult().getGetRewardsAndOffer().get(3).getName());

                    }
                }
            }
        }
    }
    public void logOut(){

        AppConstants.MEN_1 = "";
        AppConstants.MEN_2 = "";
        AppConstants.MEN_3 = "";
        AppConstants.MEN_4 = "";
        AppConstants.MEN_5 = "";
        AppConstants.MEN_6 = "";
        AppConstants.MEN_7 = "";
        AppConstants.MEN_8 = "";
        AppConstants.MEN_9 = "";
        AppConstants.MEN_10 = "";
        AppConstants.MEN_11 = "";
        AppConstants.MEN_12 = "";
        AppConstants.MEN_13 = "";
        AppConstants.MEN_14 = "";
        AppConstants.MEN_15 = "";
        AppConstants.MEN_16 = "";
        AppConstants.MEN_17 = "";
        AppConstants.MEN_18 = "";
        AppConstants.MEN_19 = "";
        AppConstants.MEN_20 = "";
        AppConstants.MEN_21 = "";
        AppConstants.MEN_22 = "";
        AppConstants.MEN_23 = "";
        AppConstants.MEN_24 = "";
        AppConstants.MEN_25 = "";
        AppConstants.MEN_26 = "";
        AppConstants.MEN_27 = "";
        AppConstants.MEN_28 = "";
        AppConstants.MEN_29 = "";
        AppConstants.MEN_30 = "";
        AppConstants.MEN_31 = "";
        AppConstants.MEN_32 = "";
        AppConstants.MEN_33 = "";
        AppConstants.MEN_34 = "";
        AppConstants.MEN_35 = "";
        AppConstants.MEN_36 = "";
        AppConstants.MEN_37 = "";
        AppConstants.MEN_38 = "";
        AppConstants.MEN_39 = "";
        AppConstants.MEN_40 = "";
        AppConstants.MEN_41 = "";
        AppConstants.MEN_42 = "";
        AppConstants.MEN_43 = "";
        AppConstants.MEN_44 = "";
        AppConstants.MEN_45 = "";
        AppConstants.MEN_46 = "";
        AppConstants.MEN_47 = "";
        AppConstants.MEN_48 = "";
        AppConstants.MEN_49 = "";
        AppConstants.MEN_50 = "";
        AppConstants.MEN_51 = "";
        AppConstants.MEN_52 = "";
        AppConstants.MEN_53 = "";
        AppConstants.MEN_54 = "";
        AppConstants.MEN_55 = "";
        AppConstants.MEN_56 = "";
        AppConstants.MEN_57 = "";
        AppConstants.MEN_58 = "";
        AppConstants.MEN_59 = "";
        AppConstants.MEN_60 = "";
        AppConstants.MEN_61 = "";
        AppConstants.MEN_62 = "";
        AppConstants.MEN_63 = "";
        AppConstants.MEN_64 = "";

        AppConstants.WOMEN_65 = "";
        AppConstants.WOMEN_66 = "";
        AppConstants.WOMEN_67 = "";
        AppConstants.WOMEN_68 = "";
        AppConstants.WOMEN_69 = "";
        AppConstants.WOMEN_70 = "";
        AppConstants.WOMEN_71 = "";
        AppConstants.WOMEN_72 = "";
        AppConstants.WOMEN_73 = "";
        AppConstants.WOMEN_74 = "";
        AppConstants.WOMEN_75 = "";
        AppConstants.WOMEN_76 = "";
        AppConstants.WOMEN_77 = "";
        AppConstants.WOMEN_78 = "";
        AppConstants.WOMEN_79 = "";
        AppConstants.WOMEN_80 = "";
        AppConstants.WOMEN_81 = "";
        AppConstants.WOMEN_82 = "";
        AppConstants.WOMEN_83 = "";
        AppConstants.WOMEN_84 = "";
        AppConstants.WOMEN_85 = "";
        AppConstants.WOMEN_86 = "";
        AppConstants.WOMEN_87 = "";
        AppConstants.WOMEN_88 = "";
        AppConstants.WOMEN_89 = "";
        AppConstants.WOMEN_90 = "";
        AppConstants.WOMEN_91 = "";
        AppConstants.WOMEN_92 = "";
        AppConstants.WOMEN_93 = "";
        AppConstants.WOMEN_94 = "";
        AppConstants.WOMEN_95 = "";
        AppConstants.WOMEN_96 = "";
        AppConstants.WOMEN_97 = "";
        AppConstants.WOMEN_98 = "";
        AppConstants.WOMEN_99 = "";
        AppConstants.WOMEN_100 = "";
        AppConstants.WOMEN_101 = "";
        AppConstants.WOMEN_102 = "";
        AppConstants.WOMEN_103 = "";
        AppConstants.WOMEN_104 = "";
        AppConstants.WOMEN_105 = "";
        AppConstants.WOMEN_106 = "";
        AppConstants.WOMEN_107 = "";
        AppConstants.WOMEN_108 = "";
        AppConstants.WOMEN_109 = "";
        AppConstants.WOMEN_110 = "";
        AppConstants.WOMEN_111 = "";
        AppConstants.WOMEN_112 = "";
        AppConstants.WOMEN_113 = "";
        AppConstants.WOMEN_114 = "";
        AppConstants.WOMEN_115 = "";
        AppConstants.WOMEN_116 = "";
        AppConstants.WOMEN_117 = "";
        AppConstants.WOMEN_118 = "";
        AppConstants.WOMEN_119 = "";
        AppConstants.WOMEN_120 = "";
        AppConstants.WOMEN_121 = "";
        AppConstants.WOMEN_122 = "";
        AppConstants.WOMEN_123 = "";
        AppConstants.WOMEN_124 = "";
        AppConstants.WOMEN_125 = "";
        AppConstants.WOMEN_126 = "";
        AppConstants.WOMEN_127 = "";
        AppConstants.WOMEN_128 = "";
        AppConstants.WOMEN_129 = "";
        AppConstants.WOMEN_130 = "";
        AppConstants.WOMEN_131 = "";
        AppConstants.WOMEN_132 = "";
        AppConstants.WOMEN_133 = "";
        AppConstants.WOMEN_134 = "";
        AppConstants.WOMEN_135 = "";
        AppConstants.WOMEN_136 = "";
        AppConstants.WOMEN_137 = "";
        AppConstants.WOMEN_138 = "";
        AppConstants.WOMEN_139 = "";
        AppConstants.WOMEN_140 = "";
        AppConstants.WOMEN_141 = "";
        AppConstants.WOMEN_142 = "";
        AppConstants.WOMEN_143 = "";
        AppConstants.WOMEN_144 = "";

        AppConstants.BOY_145 = "";
        AppConstants.BOY_146 = "";
        AppConstants.BOY_147 = "";
        AppConstants.BOY_148 = "";
        AppConstants.BOY_149 = "";
        AppConstants.BOY_150 = "";
        AppConstants.BOY_151 = "";
        AppConstants.BOY_152 = "";
        AppConstants.BOY_153 = "";
        AppConstants.BOY_154 = "";
        AppConstants.BOY_155 = "";
        AppConstants.BOY_156 = "";
        AppConstants.BOY_157 = "";
        AppConstants.BOY_158 = "";
        AppConstants.BOY_159 = "";
        AppConstants.BOY_160 = "";
        AppConstants.BOY_161 = "";
        AppConstants.BOY_162 = "";
        AppConstants.BOY_163 = "";
        AppConstants.BOY_164 = "";
        AppConstants.BOY_165 = "";
        AppConstants.BOY_166 = "";
        AppConstants.BOY_167 = "";
        AppConstants.BOY_168 = "";
        AppConstants.BOY_169 = "";
        AppConstants.BOY_170 = "";
        AppConstants.BOY_171 = "";
        AppConstants.BOY_172 = "";
        AppConstants.BOY_173 = "";
        AppConstants.BOY_174 = "";
        AppConstants.BOY_175 = "";
        AppConstants.BOY_176 = "";
        AppConstants.BOY_177 = "";
        AppConstants.BOY_178 = "";
        AppConstants.BOY_179 = "";
        AppConstants.BOY_180 = "";
        AppConstants.BOY_181 = "";
        AppConstants.BOY_182 = "";
        AppConstants.BOY_183 = "";
        AppConstants.BOY_184 = "";
        AppConstants.BOY_185 = "";
        AppConstants.BOY_186 = "";
        AppConstants.BOY_187 = "";
        AppConstants.BOY_188 = "";
        AppConstants.BOY_189 = "";
        AppConstants.BOY_190 = "";
        AppConstants.BOY_191 = "";
        AppConstants.BOY_192 = "";
        AppConstants.BOY_193 = "";
        AppConstants.BOY_194 = "";
        AppConstants.BOY_195 = "";
        AppConstants.BOY_196 = "";
        AppConstants.BOY_197 = "";
        AppConstants.BOY_198 = "";
        AppConstants.BOY_199 = "";
        AppConstants.BOY_200 = "";
        AppConstants.BOY_201 = "";
        AppConstants.BOY_202 = "";
        AppConstants.BOY_203 = "";
        AppConstants.BOY_204 = "";
        AppConstants.BOY_205 = "";
        AppConstants.BOY_206 = "";
        AppConstants.BOY_207 = "";
        AppConstants.BOY_208 = "";

        AppConstants.GIRL_209 = "";
        AppConstants.GIRL_210 = "";
        AppConstants.GIRL_211 = "";
        AppConstants.GIRL_212 = "";
        AppConstants.GIRL_213 = "";
        AppConstants.GIRL_214 = "";
        AppConstants.GIRL_215 = "";
        AppConstants.GIRL_216 = "";
        AppConstants.GIRL_217 = "";
        AppConstants.GIRL_218 = "";
        AppConstants.GIRL_219 = "";
        AppConstants.GIRL_220 = "";
        AppConstants.GIRL_221 = "";
        AppConstants.GIRL_222 = "";
        AppConstants.GIRL_223 = "";
        AppConstants.GIRL_224 = "";
        AppConstants.GIRL_225 = "";
        AppConstants.GIRL_226 = "";
        AppConstants.GIRL_227 = "";
        AppConstants.GIRL_228 = "";
        AppConstants.GIRL_229 = "";
        AppConstants.GIRL_230 = "";
        AppConstants.GIRL_231 = "";
        AppConstants.GIRL_232 = "";
        AppConstants.GIRL_233 = "";
        AppConstants.GIRL_234 = "";
        AppConstants.GIRL_235 = "";
        AppConstants.GIRL_236 = "";
        AppConstants.GIRL_237 = "";
        AppConstants.GIRL_238 = "";
        AppConstants.GIRL_239 = "";
        AppConstants.GIRL_240 = "";
        AppConstants.GIRL_241 = "";
        AppConstants.GIRL_242 = "";
        AppConstants.GIRL_243 = "";
        AppConstants.GIRL_244 = "";
        AppConstants.GIRL_245 = "";
        AppConstants.GIRL_246 = "";
        AppConstants.GIRL_247 = "";
        AppConstants.GIRL_248 = "";
        AppConstants.GIRL_249 = "";
        AppConstants.GIRL_250 = "";
        AppConstants.GIRL_251 = "";
        AppConstants.GIRL_252 = "";
        AppConstants.GIRL_253 = "";
        AppConstants.GIRL_254 = "";
        AppConstants.GIRL_255 = "";
        AppConstants.GIRL_256 = "";
        AppConstants.GIRL_257 = "";
        AppConstants.GIRL_258 = "";
        AppConstants.GIRL_259 = "";
        AppConstants.GIRL_260 = "";
        AppConstants.GIRL_261 = "";
        AppConstants.GIRL_262 = "";
        AppConstants.GIRL_263 = "";
        AppConstants.GIRL_264 = "";
        AppConstants.GIRL_265 = "";
        AppConstants.GIRL_266 = "";
        AppConstants.GIRL_267 = "";
        AppConstants.GIRL_268 = "";
        AppConstants.GIRL_269 = "";
        AppConstants.GIRL_270 = "";
        AppConstants.GIRL_271 = "";
        AppConstants.GIRL_272 = "";
        AppConstants.GIRL_273 = "";
        AppConstants.GIRL_274 = "";
        AppConstants.GIRL_275 = "";
        AppConstants.GIRL_276 = "";
        AppConstants.GIRL_277 = "";
        AppConstants.GIRL_278 = "";
        AppConstants.GIRL_279 = "";
        AppConstants.GIRL_280 = "";
        AppConstants.GIRL_281 = "";
        AppConstants.GIRL_282 = "";
        AppConstants.GIRL_283 = "";
        AppConstants.GIRL_284 = "";
        AppConstants.GIRL_285 = "";
        AppConstants.GIRL_286 = "";
        AppConstants.GIRL_287 = "";
        AppConstants.GIRL_288 = "";

        AppConstants.MEASUREMENT_VALUES = new ArrayList<>();

        AppConstants.MEASUREMENT_ID = "";

        AppConstants.MEASUREMENT_MANUALLY = "";

        AppConstants.MEASUREMENT_MAP = new HashMap<>();

        AppConstants.PATTERN_ID = "";

        AppConstants.SEASONAL_NAME = "";
        AppConstants.PLACE_OF_INDUSTRY_NAME = "";
        AppConstants.BRANDS_NAME = "";
        AppConstants.MATERIAL_TYPE_NAME = "";
        AppConstants.COLOUR_NAME = "";
        AppConstants.PATTERN_NAME = "";

        AppConstants.GET_ADDRESS_ID = "";

        AppConstants.PATTERN_ID = "";

        AppConstants.ORDER_TYPE_ID  = "" ;

        AppConstants.MEASUREMENT_ID = "";

        AppConstants.MATERIAL_IMAGES = new ArrayList<>();

        AppConstants.REFERENCE_IMAGES = new ArrayList<>();

        AppConstants.CUSTOMIZATION_CLICK_ARRAY = new ArrayList<>();

        AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();

        AppConstants.CUSTOMIZATION_CLICKED_ARRAY = new ArrayList<>();

        AppConstants.DELIVERY_TYPE_ID = "";

        AppConstants.MEASUREMENT_TYPE = "";

        AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST = new ArrayList<>();
    }

    @Override
    public void onResume() {
        super.onResume();
        initView();
    }

}
