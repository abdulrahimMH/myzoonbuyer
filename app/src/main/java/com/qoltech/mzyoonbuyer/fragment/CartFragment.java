package com.qoltech.mzyoonbuyer.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.StoreCartAdapter;
import com.qoltech.mzyoonbuyer.entity.DashboardProductsEntity;
import com.qoltech.mzyoonbuyer.entity.IdEntity;
import com.qoltech.mzyoonbuyer.entity.StoreCartEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseFragment;
import com.qoltech.mzyoonbuyer.modal.StoreCartResponse;
import com.qoltech.mzyoonbuyer.service.APICommonInterface;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.ui.AddressScreen;
import com.qoltech.mzyoonbuyer.ui.BottomNavigationScreen;
import com.qoltech.mzyoonbuyer.ui.CartScreen;
import com.qoltech.mzyoonbuyer.ui.LoginScreen;
import com.qoltech.mzyoonbuyer.ui.OrderSummaryScreen;
import com.qoltech.mzyoonbuyer.ui.StoreDashboardScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartFragment extends BaseFragment{

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    private UserDetailsEntity mUserDetailsEntityRes;

    @BindView(R.id.profile_image_circle_img)
    de.hdodenhof.circleimageview.CircleImageView mHomeCircleImg;

    @BindView(R.id.cart_rec_view)
    RecyclerView mCartRecView;

    @BindView(R.id.store_prod_amt_txt)
    TextView mStoreProdAmtTxt;

    @BindView(R.id.grand_amt_txt)
    TextView mStoreGrandAmtTxt;

    StoreCartAdapter mStoreCartAdapter;

    HashMap<String,String> mFilter = new HashMap<>();
    HashMap<String,String> mFilterArabic = new HashMap<>();

    ArrayList<StoreCartEntity> mFilterList = new ArrayList<>();

    ArrayList<String> mSellerId = new ArrayList<>();
    ArrayList<String> mTailorName = new ArrayList<>();
    ArrayList<String> mTailorNameInArabic = new ArrayList<>();

    double mStoreTotalDouble = 0;

    APICommonInterface mApiCommonInterface;

    @BindView(R.id.cart_list_empty_lay)
    LinearLayout mCartEmptyLay;

    @BindView(R.id.cart_list_lay)
    ScrollView mCartListLay;

    @BindView(R.id.cart_shop_now_txt)
    TextView mCartShopNowTxt;

    @BindView(R.id.cart_checkout_txt)
    TextView mCartCheckOutTxt;

    @BindView(R.id.cart_continue_shopping_txt)
    TextView mCartContineShoppingTxt;

    @Override
    public View onCreateView(@NonNull LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = layoutInflater.inflate(R.layout.ui_cart_fragment, container, false);

        ButterKnife.bind(this, rootView);

        initView();

        return rootView;
    }

    /*InitViews*/
    private void initView() {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(getContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        mApiCommonInterface = APIRequestHandler.getClient().create(APICommonInterface.class);

        setHeader();

        getStoreCartApiCall();

        AppConstants.CHECK_OUT_ORDER = "store";

        mCartContineShoppingTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextScreenWithOutFinish(StoreDashboardScreen.class,true);
            }
        });

        mCartShopNowTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextScreenWithOutFinish(StoreDashboardScreen.class,true);
            }
        });

        mCartCheckOutTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUserDetailsEntityRes.getUSER_ID().equalsIgnoreCase("0")){
                    DialogManager.getInstance().showOptionPopup(getContext(), getResources().getString(R.string.please_login_to_proceed), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                        @Override
                        public void onNegativeClick() {

                        }
                        @Override
                        public void onPositiveClick() {
//                            logOut();
                            AppConstants.SKIP_NOW = "CART";
                            HashMap<String,String> productIdist = new HashMap<>();
                            PreferenceUtil.storeRecentProducts(getContext(),productIdist);
                            PreferenceUtil.storeBoolPreferenceValue(getContext(),AppConstants.LOGIN_STATUS,false);
//                            PreferenceUtil.clearPreference(getContext());
                            nextScreenWithFinish(LoginScreen.class,false);
                        }
                    });
                }else {
                    if (AppConstants.ORDER_TYPE_CHECK.equalsIgnoreCase("Stitching")){
                        previousScreenWithFinish(OrderSummaryScreen.class,true);
                    }else {
                        AppConstants.CART_SCREEN_CHECK = "CART";
                        AppConstants.SERVICE_TYPE = "CART";
                        AppConstants.ADDRESS_ON_BACK = "true";
                        nextScreenWithOutFinish(AddressScreen.class,true);
                        AppConstants.TRANSACTION_AMOUNT = mStoreGrandAmtTxt.getText().toString();
                    }
                }
            }
        });
    }

    public void getStoreCartApiCall() {
        if (NetworkUtil.isNetworkAvailable(getContext())){
            APIRequestHandler.getInstance().getStoreCartListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),"List",this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(getContext(), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStoreCartApiCall();
                }
            });
        }
    }

    public void setHeader() {
        if (!mUserDetailsEntityRes.getUSER_ID().equalsIgnoreCase("0")) {
            mHomeCircleImg.setVisibility(View.VISIBLE);

        }
        mHeaderTxt.setVisibility(View.VISIBLE);

        mHeaderTxt.setText(getResources().getString(R.string.cart));

        try {
            Glide.with(this)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/BuyerImages/"+ mUserDetailsEntityRes.getUSER_IMAGE())
                    .apply(new RequestOptions().placeholder(R.drawable.profile_place_holder_icon).error(R.drawable.profile_place_holder_icon).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true))
                    .into(mHomeCircleImg);
        } catch (Exception ex) {
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof StoreCartResponse){
            StoreCartResponse mRespones = (StoreCartResponse)resObj;
            mFilter = new HashMap<>();
            mFilterArabic = new HashMap<>();
            mFilterList= new ArrayList<>();
            mSellerId = new ArrayList<>();
            mTailorName = new ArrayList<>();
            mTailorNameInArabic = new ArrayList<>();

            AppConstants.GET_ITEM_CARD_LIST  = new ArrayList<>();
            AppConstants.GET_ITEM_CARD_LIST = mRespones.getResult();
            if (mRespones.getResult().size() > 0){
                mCartEmptyLay.setVisibility(View.GONE);
                mCartListLay.setVisibility(View.VISIBLE);
                mStoreTotalDouble = 0;
                AppConstants.DELIVERY_TAILOR_ID = new ArrayList<>();
                for (int i=0 ; i<mRespones.getResult().size() ; i++){
                    IdEntity idEntity = new IdEntity();
                    idEntity.setId(mRespones.getResult().get(i).getSellerId());
                    AppConstants.DELIVERY_TAILOR_ID.add(idEntity);
                    mStoreTotalDouble = mStoreTotalDouble + mRespones.getResult().get(i).getNewPrice()*mRespones.getResult().get(i).getQuantity();
                    mFilter.put(String.valueOf(mRespones.getResult().get(i).getSellerId()),mRespones.getResult().get(i).getShopNameInEnglish());
                    mFilterArabic.put(String.valueOf(mRespones.getResult().get(i).getSellerId()),mRespones.getResult().get(i).getShopNameInArabic());
                }

                Set keys = mFilter.keySet();
                Iterator itr = keys.iterator();

                String key;
                String value;
                while(itr.hasNext())
                {
                    key = (String) itr.next();
                    value = (String) mFilter.get(key);
                    mSellerId.add(key);
                    mTailorName.add(value);
                }

                Set keyss = mFilterArabic.keySet();
                Iterator itrs = keyss.iterator();

                String keyy;
                String values;
                while(itrs.hasNext())
                {
                    keyy = (String) itrs.next();
                    values = (String) mFilterArabic.get(keyy);
                    mTailorNameInArabic.add(values);
                }

                setCartAdapter(mSellerId,mTailorName,mTailorNameInArabic,mRespones.getResult());
                mStoreProdAmtTxt.setText(String.valueOf(mStoreTotalDouble));
                mStoreGrandAmtTxt.setText(String.valueOf(mStoreTotalDouble));
            }
            else {
                mCartListLay.setVisibility(View.GONE);
                mCartEmptyLay.setVisibility(View.VISIBLE);
            }
        }
    }

    public void setCartAdapter(ArrayList<String> sellerId,ArrayList<String> TailorName,ArrayList<String> TailorNameInArabic, ArrayList<StoreCartEntity> CartEntity){

        mStoreCartAdapter = new StoreCartAdapter(sellerId,TailorName,TailorNameInArabic,CartEntity,getContext(),CartFragment.this);
        mCartRecView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        mCartRecView.setAdapter(mStoreCartAdapter);
    }

    public void removeFromCart(String productId,String sizeId,String colorId,String sellerId){
       DialogManager.getInstance().showProgress(getContext());
        if (NetworkUtil.isNetworkAvailable(getContext())){
            mApiCommonInterface.removeFromCartListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),productId,sizeId,colorId,sellerId).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {
                    DialogManager.getInstance().hideProgress();
                    if (response.body() != null && response.isSuccessful()){
                            if (AppConstants.BOTTOM_BADGE_REFRESH.equalsIgnoreCase("CART")){
                                ((CartScreen)getContext()).getStoreCartApiCall();

                            }else if (AppConstants.BOTTOM_BADGE_REFRESH.equalsIgnoreCase("HOME")){
                                ((BottomNavigationScreen)getContext()).getStoreCartApiCall();

                            }
                        initView();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {
                    DialogManager.getInstance().hideProgress();

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(getContext(), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    removeFromCart(productId,sizeId,colorId,sellerId);
                }
            });
        }

    }

    public void addQtyFromCart(String productId,String sizeId,String colorId,String sellerId,String qty){
        DialogManager.getInstance().showProgress(getContext());
        if (NetworkUtil.isNetworkAvailable(getContext())){
            mApiCommonInterface.changeQtyFromCartListApi(PreferenceUtil.getStringValue(getContext(),AppConstants.CARD_ID),productId,sizeId,colorId,sellerId,qty).enqueue(new Callback<ArrayList<DashboardProductsEntity>>() {
                @Override
                public void onResponse(Call<ArrayList<DashboardProductsEntity>> call, Response<ArrayList<DashboardProductsEntity>> response) {
                    DialogManager.getInstance().hideProgress();
                    if (response.body() != null && response.isSuccessful()){
                        if (AppConstants.BOTTOM_BADGE_REFRESH.equalsIgnoreCase("CART")){
                            ((CartScreen)getContext()).getStoreCartApiCall();

                        }else if (AppConstants.BOTTOM_BADGE_REFRESH.equalsIgnoreCase("HOME")){
                            ((BottomNavigationScreen)getContext()).getStoreCartApiCall();

                        }
                        initView();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<DashboardProductsEntity>> call, Throwable t) {
                    DialogManager.getInstance().hideProgress();

                }
            });
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(getContext(), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    addQtyFromCart(productId,sizeId,colorId,sellerId,qty);
                }
            });
        }

    }
    public void logOut(){

        AppConstants.MEN_1 = "";
        AppConstants.MEN_2 = "";
        AppConstants.MEN_3 = "";
        AppConstants.MEN_4 = "";
        AppConstants.MEN_5 = "";
        AppConstants.MEN_6 = "";
        AppConstants.MEN_7 = "";
        AppConstants.MEN_8 = "";
        AppConstants.MEN_9 = "";
        AppConstants.MEN_10 = "";
        AppConstants.MEN_11 = "";
        AppConstants.MEN_12 = "";
        AppConstants.MEN_13 = "";
        AppConstants.MEN_14 = "";
        AppConstants.MEN_15 = "";
        AppConstants.MEN_16 = "";
        AppConstants.MEN_17 = "";
        AppConstants.MEN_18 = "";
        AppConstants.MEN_19 = "";
        AppConstants.MEN_20 = "";
        AppConstants.MEN_21 = "";
        AppConstants.MEN_22 = "";
        AppConstants.MEN_23 = "";
        AppConstants.MEN_24 = "";
        AppConstants.MEN_25 = "";
        AppConstants.MEN_26 = "";
        AppConstants.MEN_27 = "";
        AppConstants.MEN_28 = "";
        AppConstants.MEN_29 = "";
        AppConstants.MEN_30 = "";
        AppConstants.MEN_31 = "";
        AppConstants.MEN_32 = "";
        AppConstants.MEN_33 = "";
        AppConstants.MEN_34 = "";
        AppConstants.MEN_35 = "";
        AppConstants.MEN_36 = "";
        AppConstants.MEN_37 = "";
        AppConstants.MEN_38 = "";
        AppConstants.MEN_39 = "";
        AppConstants.MEN_40 = "";
        AppConstants.MEN_41 = "";
        AppConstants.MEN_42 = "";
        AppConstants.MEN_43 = "";
        AppConstants.MEN_44 = "";
        AppConstants.MEN_45 = "";
        AppConstants.MEN_46 = "";
        AppConstants.MEN_47 = "";
        AppConstants.MEN_48 = "";
        AppConstants.MEN_49 = "";
        AppConstants.MEN_50 = "";
        AppConstants.MEN_51 = "";
        AppConstants.MEN_52 = "";
        AppConstants.MEN_53 = "";
        AppConstants.MEN_54 = "";
        AppConstants.MEN_55 = "";
        AppConstants.MEN_56 = "";
        AppConstants.MEN_57 = "";
        AppConstants.MEN_58 = "";
        AppConstants.MEN_59 = "";
        AppConstants.MEN_60 = "";
        AppConstants.MEN_61 = "";
        AppConstants.MEN_62 = "";
        AppConstants.MEN_63 = "";
        AppConstants.MEN_64 = "";

        AppConstants.WOMEN_65 = "";
        AppConstants.WOMEN_66 = "";
        AppConstants.WOMEN_67 = "";
        AppConstants.WOMEN_68 = "";
        AppConstants.WOMEN_69 = "";
        AppConstants.WOMEN_70 = "";
        AppConstants.WOMEN_71 = "";
        AppConstants.WOMEN_72 = "";
        AppConstants.WOMEN_73 = "";
        AppConstants.WOMEN_74 = "";
        AppConstants.WOMEN_75 = "";
        AppConstants.WOMEN_76 = "";
        AppConstants.WOMEN_77 = "";
        AppConstants.WOMEN_78 = "";
        AppConstants.WOMEN_79 = "";
        AppConstants.WOMEN_80 = "";
        AppConstants.WOMEN_81 = "";
        AppConstants.WOMEN_82 = "";
        AppConstants.WOMEN_83 = "";
        AppConstants.WOMEN_84 = "";
        AppConstants.WOMEN_85 = "";
        AppConstants.WOMEN_86 = "";
        AppConstants.WOMEN_87 = "";
        AppConstants.WOMEN_88 = "";
        AppConstants.WOMEN_89 = "";
        AppConstants.WOMEN_90 = "";
        AppConstants.WOMEN_91 = "";
        AppConstants.WOMEN_92 = "";
        AppConstants.WOMEN_93 = "";
        AppConstants.WOMEN_94 = "";
        AppConstants.WOMEN_95 = "";
        AppConstants.WOMEN_96 = "";
        AppConstants.WOMEN_97 = "";
        AppConstants.WOMEN_98 = "";
        AppConstants.WOMEN_99 = "";
        AppConstants.WOMEN_100 = "";
        AppConstants.WOMEN_101 = "";
        AppConstants.WOMEN_102 = "";
        AppConstants.WOMEN_103 = "";
        AppConstants.WOMEN_104 = "";
        AppConstants.WOMEN_105 = "";
        AppConstants.WOMEN_106 = "";
        AppConstants.WOMEN_107 = "";
        AppConstants.WOMEN_108 = "";
        AppConstants.WOMEN_109 = "";
        AppConstants.WOMEN_110 = "";
        AppConstants.WOMEN_111 = "";
        AppConstants.WOMEN_112 = "";
        AppConstants.WOMEN_113 = "";
        AppConstants.WOMEN_114 = "";
        AppConstants.WOMEN_115 = "";
        AppConstants.WOMEN_116 = "";
        AppConstants.WOMEN_117 = "";
        AppConstants.WOMEN_118 = "";
        AppConstants.WOMEN_119 = "";
        AppConstants.WOMEN_120 = "";
        AppConstants.WOMEN_121 = "";
        AppConstants.WOMEN_122 = "";
        AppConstants.WOMEN_123 = "";
        AppConstants.WOMEN_124 = "";
        AppConstants.WOMEN_125 = "";
        AppConstants.WOMEN_126 = "";
        AppConstants.WOMEN_127 = "";
        AppConstants.WOMEN_128 = "";
        AppConstants.WOMEN_129 = "";
        AppConstants.WOMEN_130 = "";
        AppConstants.WOMEN_131 = "";
        AppConstants.WOMEN_132 = "";
        AppConstants.WOMEN_133 = "";
        AppConstants.WOMEN_134 = "";
        AppConstants.WOMEN_135 = "";
        AppConstants.WOMEN_136 = "";
        AppConstants.WOMEN_137 = "";
        AppConstants.WOMEN_138 = "";
        AppConstants.WOMEN_139 = "";
        AppConstants.WOMEN_140 = "";
        AppConstants.WOMEN_141 = "";
        AppConstants.WOMEN_142 = "";
        AppConstants.WOMEN_143 = "";
        AppConstants.WOMEN_144 = "";

        AppConstants.BOY_145 = "";
        AppConstants.BOY_146 = "";
        AppConstants.BOY_147 = "";
        AppConstants.BOY_148 = "";
        AppConstants.BOY_149 = "";
        AppConstants.BOY_150 = "";
        AppConstants.BOY_151 = "";
        AppConstants.BOY_152 = "";
        AppConstants.BOY_153 = "";
        AppConstants.BOY_154 = "";
        AppConstants.BOY_155 = "";
        AppConstants.BOY_156 = "";
        AppConstants.BOY_157 = "";
        AppConstants.BOY_158 = "";
        AppConstants.BOY_159 = "";
        AppConstants.BOY_160 = "";
        AppConstants.BOY_161 = "";
        AppConstants.BOY_162 = "";
        AppConstants.BOY_163 = "";
        AppConstants.BOY_164 = "";
        AppConstants.BOY_165 = "";
        AppConstants.BOY_166 = "";
        AppConstants.BOY_167 = "";
        AppConstants.BOY_168 = "";
        AppConstants.BOY_169 = "";
        AppConstants.BOY_170 = "";
        AppConstants.BOY_171 = "";
        AppConstants.BOY_172 = "";
        AppConstants.BOY_173 = "";
        AppConstants.BOY_174 = "";
        AppConstants.BOY_175 = "";
        AppConstants.BOY_176 = "";
        AppConstants.BOY_177 = "";
        AppConstants.BOY_178 = "";
        AppConstants.BOY_179 = "";
        AppConstants.BOY_180 = "";
        AppConstants.BOY_181 = "";
        AppConstants.BOY_182 = "";
        AppConstants.BOY_183 = "";
        AppConstants.BOY_184 = "";
        AppConstants.BOY_185 = "";
        AppConstants.BOY_186 = "";
        AppConstants.BOY_187 = "";
        AppConstants.BOY_188 = "";
        AppConstants.BOY_189 = "";
        AppConstants.BOY_190 = "";
        AppConstants.BOY_191 = "";
        AppConstants.BOY_192 = "";
        AppConstants.BOY_193 = "";
        AppConstants.BOY_194 = "";
        AppConstants.BOY_195 = "";
        AppConstants.BOY_196 = "";
        AppConstants.BOY_197 = "";
        AppConstants.BOY_198 = "";
        AppConstants.BOY_199 = "";
        AppConstants.BOY_200 = "";
        AppConstants.BOY_201 = "";
        AppConstants.BOY_202 = "";
        AppConstants.BOY_203 = "";
        AppConstants.BOY_204 = "";
        AppConstants.BOY_205 = "";
        AppConstants.BOY_206 = "";
        AppConstants.BOY_207 = "";
        AppConstants.BOY_208 = "";

        AppConstants.GIRL_209 = "";
        AppConstants.GIRL_210 = "";
        AppConstants.GIRL_211 = "";
        AppConstants.GIRL_212 = "";
        AppConstants.GIRL_213 = "";
        AppConstants.GIRL_214 = "";
        AppConstants.GIRL_215 = "";
        AppConstants.GIRL_216 = "";
        AppConstants.GIRL_217 = "";
        AppConstants.GIRL_218 = "";
        AppConstants.GIRL_219 = "";
        AppConstants.GIRL_220 = "";
        AppConstants.GIRL_221 = "";
        AppConstants.GIRL_222 = "";
        AppConstants.GIRL_223 = "";
        AppConstants.GIRL_224 = "";
        AppConstants.GIRL_225 = "";
        AppConstants.GIRL_226 = "";
        AppConstants.GIRL_227 = "";
        AppConstants.GIRL_228 = "";
        AppConstants.GIRL_229 = "";
        AppConstants.GIRL_230 = "";
        AppConstants.GIRL_231 = "";
        AppConstants.GIRL_232 = "";
        AppConstants.GIRL_233 = "";
        AppConstants.GIRL_234 = "";
        AppConstants.GIRL_235 = "";
        AppConstants.GIRL_236 = "";
        AppConstants.GIRL_237 = "";
        AppConstants.GIRL_238 = "";
        AppConstants.GIRL_239 = "";
        AppConstants.GIRL_240 = "";
        AppConstants.GIRL_241 = "";
        AppConstants.GIRL_242 = "";
        AppConstants.GIRL_243 = "";
        AppConstants.GIRL_244 = "";
        AppConstants.GIRL_245 = "";
        AppConstants.GIRL_246 = "";
        AppConstants.GIRL_247 = "";
        AppConstants.GIRL_248 = "";
        AppConstants.GIRL_249 = "";
        AppConstants.GIRL_250 = "";
        AppConstants.GIRL_251 = "";
        AppConstants.GIRL_252 = "";
        AppConstants.GIRL_253 = "";
        AppConstants.GIRL_254 = "";
        AppConstants.GIRL_255 = "";
        AppConstants.GIRL_256 = "";
        AppConstants.GIRL_257 = "";
        AppConstants.GIRL_258 = "";
        AppConstants.GIRL_259 = "";
        AppConstants.GIRL_260 = "";
        AppConstants.GIRL_261 = "";
        AppConstants.GIRL_262 = "";
        AppConstants.GIRL_263 = "";
        AppConstants.GIRL_264 = "";
        AppConstants.GIRL_265 = "";
        AppConstants.GIRL_266 = "";
        AppConstants.GIRL_267 = "";
        AppConstants.GIRL_268 = "";
        AppConstants.GIRL_269 = "";
        AppConstants.GIRL_270 = "";
        AppConstants.GIRL_271 = "";
        AppConstants.GIRL_272 = "";
        AppConstants.GIRL_273 = "";
        AppConstants.GIRL_274 = "";
        AppConstants.GIRL_275 = "";
        AppConstants.GIRL_276 = "";
        AppConstants.GIRL_277 = "";
        AppConstants.GIRL_278 = "";
        AppConstants.GIRL_279 = "";
        AppConstants.GIRL_280 = "";
        AppConstants.GIRL_281 = "";
        AppConstants.GIRL_282 = "";
        AppConstants.GIRL_283 = "";
        AppConstants.GIRL_284 = "";
        AppConstants.GIRL_285 = "";
        AppConstants.GIRL_286 = "";
        AppConstants.GIRL_287 = "";
        AppConstants.GIRL_288 = "";

        AppConstants.MEASUREMENT_VALUES = new ArrayList<>();

        AppConstants.MEASUREMENT_ID = "";

        AppConstants.MEASUREMENT_MANUALLY = "";

        AppConstants.MEASUREMENT_MAP = new HashMap<>();

        AppConstants.PATTERN_ID = "";

        AppConstants.SEASONAL_NAME = "";
        AppConstants.PLACE_OF_INDUSTRY_NAME = "";
        AppConstants.BRANDS_NAME = "";
        AppConstants.MATERIAL_TYPE_NAME = "";
        AppConstants.COLOUR_NAME = "";
        AppConstants.PATTERN_NAME = "";

        AppConstants.GET_ADDRESS_ID = "";

        AppConstants.PATTERN_ID = "";

        AppConstants.ORDER_TYPE_ID  = "" ;

        AppConstants.MEASUREMENT_ID = "";

        AppConstants.MATERIAL_IMAGES = new ArrayList<>();

        AppConstants.REFERENCE_IMAGES = new ArrayList<>();

        AppConstants.CUSTOMIZATION_CLICK_ARRAY = new ArrayList<>();

        AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();

        AppConstants.CUSTOMIZATION_CLICKED_ARRAY = new ArrayList<>();

        AppConstants.DELIVERY_TYPE_ID = "";

        AppConstants.MEASUREMENT_TYPE = "";

        AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST = new ArrayList<>();
    }

    @Override
    public void onResume() {
        super.onResume();
        AppConstants.SKIP_NOW = "";
        initView();
    }
}
