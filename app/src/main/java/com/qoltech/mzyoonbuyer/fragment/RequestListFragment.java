package com.qoltech.mzyoonbuyer.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoonbuyer.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.adapter.RequestListAdapter;
import com.qoltech.mzyoonbuyer.entity.OrderRequestListEntity;
import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
import com.qoltech.mzyoonbuyer.main.BaseFragment;
import com.qoltech.mzyoonbuyer.modal.OrderRequestListResponse;
import com.qoltech.mzyoonbuyer.service.APIRequestHandler;
import com.qoltech.mzyoonbuyer.utils.AppConstants;
import com.qoltech.mzyoonbuyer.utils.DialogManager;
import com.qoltech.mzyoonbuyer.utils.NetworkUtil;
import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestListFragment extends BaseFragment {

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.profile_image_circle_img)
    de.hdodenhof.circleimageview.CircleImageView mHomeCircleImg;

    @BindView(R.id.order_request_list_scroll_view_img)
    ImageView mOrderRequestListScrollViewImg;

    private RequestListAdapter mRequestListAdapter;

    @BindView(R.id.request_list_recycler_view)
    RecyclerView mRequestListRecyclerView;

    @BindView(R.id.order_request_list_par_lay)
    RelativeLayout mOrderRequestListParLay;

    private UserDetailsEntity mUserDetailsEntityRes;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    String sample = "sample";

    @Override
    public View onCreateView(@NonNull LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = layoutInflater.inflate(R.layout.frag_request_list, container, false);

        ButterKnife.bind(this, rootView);

        initView();

        return rootView;
    }

    /*InitViews*/
    private void initView() {


        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(getContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        setHeader();

        getRequestOrderList();
    }
    public void setHeader() {
        if (!mUserDetailsEntityRes.getUSER_ID().equalsIgnoreCase("0")) {
            mHomeCircleImg.setVisibility(View.VISIBLE);

        }
        mHeaderTxt.setVisibility(View.VISIBLE);

        mHeaderTxt.setText(getResources().getString(R.string.order_request_list));
        mEmptyListTxt.setText(getResources().getString(R.string.no_result_for_request_list));

        try {
            Glide.with(this)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/BuyerImages/"+ mUserDetailsEntityRes.getUSER_IMAGE())
                    .apply(new RequestOptions().placeholder(R.drawable.profile_place_holder_icon).error(R.drawable.profile_place_holder_icon).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true))
                    .into(mHomeCircleImg);
        } catch (Exception ex) {
        }
    }

    public void getRequestOrderList(){
        if (NetworkUtil.isNetworkAvailable(getContext())){
            APIRequestHandler.getInstance().getOrderRequestListApi(mUserDetailsEntityRes.getUSER_ID(),RequestListFragment.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(getContext(), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getRequestOrderList();
                }
            });
        }
        }


    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof OrderRequestListResponse){
            OrderRequestListResponse mResponse = (OrderRequestListResponse)resObj;
            setAdapter(mResponse.getResult());
        }

    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<OrderRequestListEntity> orderRequestListEntities) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        mOrderRequestListParLay.setVisibility(orderRequestListEntities.size() > 0 ? View.VISIBLE : View.GONE);
        mEmptyListLay.setVisibility(orderRequestListEntities.size() > 0 ? View.GONE : View.VISIBLE);

        mRequestListAdapter = new RequestListAdapter(getContext(), orderRequestListEntities);
        mRequestListRecyclerView.setLayoutManager(layoutManager);
        mRequestListRecyclerView.setAdapter(mRequestListAdapter);


        mRequestListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = layoutManager.findLastVisibleItemPosition();

                if (lastPosition == orderRequestListEntities.size()-1){
                    mOrderRequestListScrollViewImg.setVisibility(View.GONE);
                }else {
                    mOrderRequestListScrollViewImg.setVisibility(View.VISIBLE);
                }
            }
        });
        DialogManager.getInstance().hideProgress();
    }

}