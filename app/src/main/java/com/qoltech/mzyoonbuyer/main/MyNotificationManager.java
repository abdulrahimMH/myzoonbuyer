package com.qoltech.mzyoonbuyer.main;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.qoltech.mzyoonbuyer.R;
import com.qoltech.mzyoonbuyer.ui.BottomNavigationScreen;
import com.qoltech.mzyoonbuyer.utils.AppConstants;

import static android.content.Context.NOTIFICATION_SERVICE;

public class MyNotificationManager {

    private Context mCtx;
    private static MyNotificationManager mInstance;

    private MyNotificationManager(Context context) {
        mCtx = context;
    }

    public static synchronized MyNotificationManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MyNotificationManager(context);
        }
        return mInstance;
    }

    public void displayNotification(String title, String body) {
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mCtx, AppConstants.CHANNEL_ID)
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setContentTitle(title)
                        .setSound(uri)
                        .setSmallIcon(R.mipmap.notification_icon)
                        .setColor(mCtx.getResources().getColor(R.color.white))
                        .setChannelId(AppConstants.CHANNEL_ID)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setContentText(body);

//        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            mBuilder.setSmallIcon(R.drawable.logo);
//            mBuilder.setColor(mCtx.getResources().getColor(R.color.black));
//            mBuilder.setChannelId(AppConstants.CHANNEL_ID);
//            mBuilder.setSound(uri);
//        } else {
//            mBuilder.setSmallIcon(R.mipmap.app_icon);
//        }

        /*
         *  Clicking on the notification will take us to this intent
         *  Right now we are using the MainActivity as this is the only activity we have in our application
         *  But for your project you can customize it as you want
         * */

        Intent resultIntent = new Intent(mCtx, BottomNavigationScreen.class);

        /*
         *  Now we will create a pending intent
         *  The method getActivity is taking 4 parameters
         *  All paramters are describing themselves
         *  0 is the request code (the second parameter)
         *  We can detect this code in the activity that will open by this we can get
         *  Which notification opened the activity
         * */
        PendingIntent pendingIntent = PendingIntent.getActivity(mCtx, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        /*
         *  Setting the pending intent to notification builder
         * */

        mBuilder.setContentIntent(pendingIntent);

        NotificationManager mNotifyMgr =
                (NotificationManager) mCtx.getSystemService(NOTIFICATION_SERVICE);

        /*
         * The first parameter is the notification id
         * better don't give a literal here (right now we are giving a int literal)
         * because using this id we can modify it later
         * */
        if (mNotifyMgr != null) {
            mNotifyMgr.notify(1, mBuilder.build());
        }
    }


}
