package com.qoltech.mzyoonbuyer.main;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService  extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        //if the message contains data payload
        //It is a map of custom keyvalues
        //we can read it easily

        //getting the title and the body
        if(remoteMessage.getData().size() > 0){
            //handle the data message here
            String title = remoteMessage.getData().get("title");
            String body = remoteMessage.getData().get("body");

            MyNotificationManager.getInstance(this).displayNotification(title , body);
        }

        if (remoteMessage.getNotification() != null){
            String title = remoteMessage.getNotification().getTitle();
            String body = remoteMessage.getNotification().getBody();

            MyNotificationManager.getInstance(this).displayNotification(title , body);
        }

        //then here we can use the title and body to build a notification
    }
}
