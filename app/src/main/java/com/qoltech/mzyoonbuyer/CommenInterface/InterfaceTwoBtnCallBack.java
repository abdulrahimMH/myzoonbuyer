package com.qoltech.mzyoonbuyer.CommenInterface;


public interface InterfaceTwoBtnCallBack extends InterfaceBtnCallBack {

    void onNegativeClick();
}
