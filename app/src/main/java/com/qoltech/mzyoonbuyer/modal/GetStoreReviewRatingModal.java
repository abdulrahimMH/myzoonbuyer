package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetStoreReviewEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetStoreReviewRatingModal implements Serializable {
    public String ResponseMsg;
    public ArrayList<GetStoreReviewEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetStoreReviewEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<GetStoreReviewEntity> result) {
        Result = result;
    }

}
