package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetAreaEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetAreaResponse implements Serializable {
    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg == null ? "" : ResponseMsg;
    }

    public ArrayList<GetAreaEntity> getResult() {
        return Result;
    }

    public void setResult(ArrayList<GetAreaEntity> result) {
        Result = result == null ? new ArrayList<>() : Result;
    }

    public ArrayList<GetAreaEntity> Result;
}
