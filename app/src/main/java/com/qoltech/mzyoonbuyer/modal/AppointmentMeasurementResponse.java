package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.AppointmentMeasurementEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class AppointmentMeasurementResponse implements Serializable {
public String ResponseMsg;
    public ArrayList<AppointmentMeasurementEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<AppointmentMeasurementEntity> getResult() {
        return Result == null ? new ArrayList<AppointmentMeasurementEntity>() : Result;
    }

    public void setResult(ArrayList<AppointmentMeasurementEntity> result) {
        Result = result;
    }

}
