package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.RewardHistoryEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class RewardHistoryResponse implements Serializable {

    public String ResponseMsg;
    public ArrayList<RewardHistoryEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<RewardHistoryEntity> getResult() {
        return Result;
    }

    public void setResult(ArrayList<RewardHistoryEntity> result) {
        Result = result;
    }

}
