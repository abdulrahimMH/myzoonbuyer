package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class OrderDetailsStitchingStoreResponse implements Serializable {

    public String ResponseMsg;
    public OrderDetailsStitchingStoreModal Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public OrderDetailsStitchingStoreModal getResult() {
        return Result == null ? new OrderDetailsStitchingStoreModal() : Result;
    }

    public void setResult(OrderDetailsStitchingStoreModal result) {
        Result = result;
    }

}
