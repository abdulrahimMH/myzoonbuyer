package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class LoginOTPResponse implements Serializable {
    private String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public String getResult() {
        return Result == null ? "" : Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    private String Result;


}
