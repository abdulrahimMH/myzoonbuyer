package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetMeasurementPartsEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetMeasurementPartsResponse implements Serializable {
    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetMeasurementPartsEntity> getResult() {
        return Result == null ? new ArrayList<GetMeasurementPartsEntity>() : Result;
    }

    public void setResult(ArrayList<GetMeasurementPartsEntity> result) {
        Result = result;
    }

    public ArrayList<GetMeasurementPartsEntity> Result;

}
