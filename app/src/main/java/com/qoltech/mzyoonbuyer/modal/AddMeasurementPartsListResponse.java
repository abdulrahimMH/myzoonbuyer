package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.AddMeasurementPartsEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class AddMeasurementPartsListResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<AddMeasurementPartsEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<AddMeasurementPartsEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<AddMeasurementPartsEntity> result) {
        Result = result;
    }

}
