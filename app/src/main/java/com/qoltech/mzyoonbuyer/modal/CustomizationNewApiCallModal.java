package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.ApicallidEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class CustomizationNewApiCallModal implements Serializable {
    public ArrayList<ApicallidEntity> SeasonId;
    public ArrayList<ApicallidEntity> PlaceofOrginId;
    public ArrayList<ApicallidEntity> BrandId;
    public ArrayList<ApicallidEntity> MaterialTypeId;
    public ArrayList<ApicallidEntity> colorId;
    public ArrayList<ApicallidEntity> ThicknessId;
    public String DressType;

    public ArrayList<ApicallidEntity> getSeasonId() {
        return SeasonId == null ? new ArrayList<>() : SeasonId;
    }

    public void setSeasonId(ArrayList<ApicallidEntity> seasonId) {
        SeasonId = seasonId;
    }

    public ArrayList<ApicallidEntity> getPlaceofOrginId() {
        return PlaceofOrginId == null ? new ArrayList<>() : PlaceofOrginId;
    }

    public void setPlaceofOrginId(ArrayList<ApicallidEntity> placeofOrginId) {
        PlaceofOrginId = placeofOrginId;
    }

    public ArrayList<ApicallidEntity> getBrandId() {
        return BrandId == null ? new ArrayList<>() : BrandId;
    }

    public void setBrandId(ArrayList<ApicallidEntity> brandId) {
        BrandId = brandId;
    }

    public ArrayList<ApicallidEntity> getMaterialTypeId() {
        return MaterialTypeId == null ? new ArrayList<>() : MaterialTypeId;
    }

    public void setMaterialTypeId(ArrayList<ApicallidEntity> materialTypeId) {
        MaterialTypeId = materialTypeId;
    }

    public ArrayList<ApicallidEntity> getColorId() {
        return colorId == null ? new ArrayList<>() : colorId;
    }

    public void setColorId(ArrayList<ApicallidEntity> colorId) {
        this.colorId = colorId;
    }

    public ArrayList<ApicallidEntity> getThicknessId() {
        return ThicknessId == null ? new ArrayList<>() : ThicknessId;
    }

    public void setThicknessId(ArrayList<ApicallidEntity> thicknessId) {
        ThicknessId = thicknessId;
    }

    public String getDressType() {
        return DressType == null ? "" : DressType;
    }

    public void setDressType(String dressType) {
        DressType = dressType;
    }


}
