package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class InsertRatingAndReviewResponse implements Serializable {
    public  String Result;
    public  String ResponseMsg;

    public String getResult() {
        return Result == null ? "" : Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }
}
