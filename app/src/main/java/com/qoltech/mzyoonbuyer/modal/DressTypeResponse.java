package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.DressTypeEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class DressTypeResponse implements Serializable {

    public String ResponseMsg;
    public ArrayList<DressTypeEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg ==  null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<DressTypeEntity> getResult() {
        return Result == null ? new ArrayList<DressTypeEntity>() : Result;
    }

    public void setResult(ArrayList<DressTypeEntity> result) {
        Result = result;
    }

}
