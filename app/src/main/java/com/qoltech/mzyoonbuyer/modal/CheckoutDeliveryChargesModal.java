package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.IdEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class CheckoutDeliveryChargesModal implements Serializable {
    public ArrayList<IdEntity> TailorId;
    public int AreaId;
    public int OrderId;
    public int DeliveryId;

    public int getDeliveryId() {
        return DeliveryId;
    }

    public void setDeliveryId(int deliveryId) {
        DeliveryId = deliveryId;
    }

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

    public ArrayList<IdEntity> getTailorId() {
        return TailorId == null ? new ArrayList<>() : TailorId;
    }

    public void setTailorId(ArrayList<IdEntity> tailorId) {
        TailorId = tailorId;
    }

    public int getAreaId() {
        return AreaId;
    }

    public void setAreaId(int areaId) {
        AreaId = areaId;
    }
}
