package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.DeliveryChargesEntity;
import com.qoltech.mzyoonbuyer.entity.GetAppointmentLeftDeliveryEntity;
import com.qoltech.mzyoonbuyer.entity.GetAppointmentLeftMaterialEntity;
import com.qoltech.mzyoonbuyer.entity.GetAppointmentLeftMeasurementEntity;
import com.qoltech.mzyoonbuyer.entity.GetBuyerAddressEntity;
import com.qoltech.mzyoonbuyer.entity.GetGrandTotalEntity;
import com.qoltech.mzyoonbuyer.entity.GetMaterialCompaniReferenceImgEntity;
import com.qoltech.mzyoonbuyer.entity.GetPaymentDetailsEntity;
import com.qoltech.mzyoonbuyer.entity.GetPaymentReceivedStatusEntity;
import com.qoltech.mzyoonbuyer.entity.GetPaymentStatusEntity;
import com.qoltech.mzyoonbuyer.entity.GetstoreProductPriceEntity;
import com.qoltech.mzyoonbuyer.entity.MaterialDeliveryChargesEntity;
import com.qoltech.mzyoonbuyer.entity.MeasurementChargesEntity;
import com.qoltech.mzyoonbuyer.entity.StichingAndMaterialChargesEntity;
import com.qoltech.mzyoonbuyer.entity.StichingOrderDetailsEntity;
import com.qoltech.mzyoonbuyer.entity.TotalEntity;
import com.qoltech.mzyoonbuyer.entity.UrgentStichingChargesEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderDetailsStitchingStoreModal implements Serializable {

    public ArrayList<StichingOrderDetailsEntity> StichingOrderDetails;
    public ArrayList<GetAppointmentLeftMaterialEntity> GetAppoinmentLeftMateril;
    public ArrayList<GetAppointmentLeftMeasurementEntity> GetAppoinmentLeftMeasurement;
    public ArrayList<GetAppointmentLeftDeliveryEntity> GetOrderDetailScheduleType;
    public ArrayList<StichingAndMaterialChargesEntity> StichingAndMaterialCharge;
    public ArrayList<MeasurementChargesEntity> MeasurementCharges;
    public ArrayList<UrgentStichingChargesEntity> UrgentStichingCharges;
    public ArrayList<DeliveryChargesEntity> DeliveryCharge;
    public ArrayList<MaterialDeliveryChargesEntity> MaterialDeliveryCharges;
    public ArrayList<TotalEntity> Total;
    public ArrayList<GetstoreProductPriceEntity> GetstoreProductPrice;
    public ArrayList<GetGrandTotalEntity> GetGrandTotal;
    public ArrayList<GetPaymentDetailsEntity> GetPaymentDetails;
    public ArrayList<GetPaymentStatusEntity> GetPaymentStatus;
    public ArrayList<GetPaymentReceivedStatusEntity> GetPaymentReceivedStatus;
    public ArrayList<GetBuyerAddressEntity> GetBuyerAddress;
    public ArrayList<GetMaterialCompaniReferenceImgEntity> MaterialImage;
    public ArrayList<GetMaterialCompaniReferenceImgEntity> AdditionalMaterialImage;
    public ArrayList<GetMaterialCompaniReferenceImgEntity> ReferenceImage;

    public ArrayList<GetMaterialCompaniReferenceImgEntity> getMaterialImage() {
        return MaterialImage;
    }

    public void setMaterialImage(ArrayList<GetMaterialCompaniReferenceImgEntity> materialImage) {
        MaterialImage = materialImage;
    }

    public ArrayList<GetMaterialCompaniReferenceImgEntity> getAdditionalMaterialImage() {
        return AdditionalMaterialImage;
    }

    public void setAdditionalMaterialImage(ArrayList<GetMaterialCompaniReferenceImgEntity> additionalMaterialImage) {
        AdditionalMaterialImage = additionalMaterialImage;
    }

    public ArrayList<GetMaterialCompaniReferenceImgEntity> getReferenceImage() {
        return ReferenceImage;
    }

    public void setReferenceImage(ArrayList<GetMaterialCompaniReferenceImgEntity> referenceImage) {
        ReferenceImage = referenceImage;
    }

    public ArrayList<GetPaymentReceivedStatusEntity> getGetPaymentReceivedStatus() {
        return GetPaymentReceivedStatus == null ? new ArrayList<>() : GetPaymentReceivedStatus;
    }

    public void setGetPaymentReceivedStatus(ArrayList<GetPaymentReceivedStatusEntity> getPaymentReceivedStatus) {
        GetPaymentReceivedStatus = getPaymentReceivedStatus;
    }

    public ArrayList<StichingOrderDetailsEntity> getStichingOrderDetails() {
        return StichingOrderDetails == null ? new ArrayList<>() : StichingOrderDetails;
    }

    public void setStichingOrderDetails(ArrayList<StichingOrderDetailsEntity> stichingOrderDetails) {
        StichingOrderDetails = stichingOrderDetails;
    }

    public ArrayList<GetAppointmentLeftMaterialEntity> getGetAppoinmentLeftMateril() {
        return GetAppoinmentLeftMateril == null ? new ArrayList<>() : GetAppoinmentLeftMateril;
    }

    public void setGetAppoinmentLeftMateril(ArrayList<GetAppointmentLeftMaterialEntity> getAppoinmentLeftMateril) {
        GetAppoinmentLeftMateril = getAppoinmentLeftMateril;
    }

    public ArrayList<GetAppointmentLeftMeasurementEntity> getGetAppoinmentLeftMeasurement() {
        return GetAppoinmentLeftMeasurement == null ? new ArrayList<>() : GetAppoinmentLeftMeasurement;
    }

    public void setGetAppoinmentLeftMeasurement(ArrayList<GetAppointmentLeftMeasurementEntity> getAppoinmentLeftMeasurement) {
        GetAppoinmentLeftMeasurement = getAppoinmentLeftMeasurement;
    }

    public ArrayList<GetAppointmentLeftDeliveryEntity> getGetOrderDetailScheduleType() {
        return GetOrderDetailScheduleType == null ? new ArrayList<>() : GetOrderDetailScheduleType;
    }

    public void setGetOrderDetailScheduleType(ArrayList<GetAppointmentLeftDeliveryEntity> getOrderDetailScheduleType) {
        GetOrderDetailScheduleType = getOrderDetailScheduleType;
    }

    public ArrayList<StichingAndMaterialChargesEntity> getStichingAndMaterialCharge() {
        return StichingAndMaterialCharge == null ? new ArrayList<>() : StichingAndMaterialCharge;
    }

    public void setStichingAndMaterialCharge(ArrayList<StichingAndMaterialChargesEntity> stichingAndMaterialCharge) {
        StichingAndMaterialCharge = stichingAndMaterialCharge;
    }

    public ArrayList<MeasurementChargesEntity> getMeasurementCharges() {
        return MeasurementCharges == null ? new ArrayList<>() : MeasurementCharges;
    }

    public void setMeasurementCharges(ArrayList<MeasurementChargesEntity> measurementCharges) {
        MeasurementCharges = measurementCharges;
    }

    public ArrayList<UrgentStichingChargesEntity> getUrgentStichingCharges() {
        return UrgentStichingCharges == null ? new ArrayList<>() : UrgentStichingCharges;
    }

    public void setUrgentStichingCharges(ArrayList<UrgentStichingChargesEntity> urgentStichingCharges) {
        UrgentStichingCharges = urgentStichingCharges;
    }

    public ArrayList<DeliveryChargesEntity> getDeliveryCharge() {
        return DeliveryCharge == null ? new ArrayList<>() : DeliveryCharge;
    }

    public void setDeliveryCharge(ArrayList<DeliveryChargesEntity> deliveryCharge) {
        DeliveryCharge = deliveryCharge;
    }

    public ArrayList<MaterialDeliveryChargesEntity> getMaterialDeliveryCharges() {
        return MaterialDeliveryCharges == null ? new ArrayList<>() : MaterialDeliveryCharges;
    }

    public void setMaterialDeliveryCharges(ArrayList<MaterialDeliveryChargesEntity> materialDeliveryCharges) {
        MaterialDeliveryCharges = materialDeliveryCharges;
    }

    public ArrayList<TotalEntity> getTotal() {
        return Total == null ? new ArrayList<>() : Total;
    }

    public void setTotal(ArrayList<TotalEntity> total) {
        Total = total;
    }

    public ArrayList<GetstoreProductPriceEntity> getGetstoreProductPrice() {
        return GetstoreProductPrice == null ? new ArrayList<>() : GetstoreProductPrice;
    }

    public void setGetstoreProductPrice(ArrayList<GetstoreProductPriceEntity> getstoreProductPrice) {
        GetstoreProductPrice = getstoreProductPrice;
    }

    public ArrayList<GetGrandTotalEntity> getGetGrandTotal() {
        return GetGrandTotal == null ? new ArrayList<>() : GetGrandTotal;
    }

    public void setGetGrandTotal(ArrayList<GetGrandTotalEntity> getGrandTotal) {
        GetGrandTotal = getGrandTotal;
    }

    public ArrayList<GetPaymentDetailsEntity> getGetPaymentDetails() {
        return GetPaymentDetails == null ? new ArrayList<>() : GetPaymentDetails;
    }

    public void setGetPaymentDetails(ArrayList<GetPaymentDetailsEntity> getPaymentDetails) {
        GetPaymentDetails = getPaymentDetails;
    }

    public ArrayList<GetPaymentStatusEntity> getGetPaymentStatus() {
        return GetPaymentStatus == null ? new ArrayList<>() : GetPaymentStatus;
    }

    public void setGetPaymentStatus(ArrayList<GetPaymentStatusEntity> getPaymentStatus) {
        GetPaymentStatus = getPaymentStatus;
    }

    public ArrayList<GetBuyerAddressEntity> getGetBuyerAddress() {
        return GetBuyerAddress == null ? new ArrayList<>() : GetBuyerAddress;
    }

    public void setGetBuyerAddress(ArrayList<GetBuyerAddressEntity> getBuyerAddress) {
        GetBuyerAddress = getBuyerAddress;
    }


}
