package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.ApicallidEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class NewFlowCustomizationTwoApiCall implements Serializable {
    public int TailorId;
    public ArrayList<ApicallidEntity> BrandId;
    public ArrayList<ApicallidEntity> MaterialTypeId;
    public ArrayList<ApicallidEntity> ColorId;
    public ArrayList<ApicallidEntity> ThicknessId;
    public String DressType;

    public String getDressType() {
        return DressType == null ? "" : DressType;
    }

    public void setDressType(String dressType) {
        DressType = dressType;
    }

    public ArrayList<ApicallidEntity> getThicknessId() {
        return ThicknessId;
    }

    public void setThicknessId(ArrayList<ApicallidEntity> thicknessId) {
        ThicknessId = thicknessId;
    }

    public int getTailorId() {
        return TailorId;
    }

    public void setTailorId(int tailorId) {
        TailorId = tailorId;
    }
    public ArrayList<ApicallidEntity> getColorId() {
        return ColorId == null ? new ArrayList<ApicallidEntity>() : ColorId;
    }

    public void setColorId(ArrayList<ApicallidEntity> colorId) {
        ColorId = colorId;
    }

    public ArrayList<ApicallidEntity> getBrandId() {
        return BrandId == null ? new ArrayList<ApicallidEntity>() : BrandId;
    }

    public void setBrandId(ArrayList<ApicallidEntity> brandId) {
        BrandId = brandId;
    }

    public ArrayList<ApicallidEntity> getMaterialTypeId() {
        return MaterialTypeId == null ? new ArrayList<ApicallidEntity>() : MaterialTypeId;
    }

    public void setMaterialTypeId(ArrayList<ApicallidEntity> materialTypeId) {
        MaterialTypeId = materialTypeId;
    }

}
