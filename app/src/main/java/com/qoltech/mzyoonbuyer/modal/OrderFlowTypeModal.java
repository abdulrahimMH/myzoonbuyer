package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.OrderFlowTypeEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderFlowTypeModal implements Serializable {
    public String ResponseMsg;
    public ArrayList<OrderFlowTypeEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<OrderFlowTypeEntity> getResult() {
        return Result == null ? new ArrayList<>(): Result;
    }

    public void setResult(ArrayList<OrderFlowTypeEntity> result) {
        Result = result;
    }

}
