package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetRedeemModal;

import java.io.Serializable;

public class GetRedeemResponse implements Serializable {

    public String ResponseMsg;
    public GetRedeemModal Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public GetRedeemModal getResult() {
        return Result == null ? new GetRedeemModal() : Result;
    }

    public void setResult(GetRedeemModal result) {
        Result = result;
    }


}
