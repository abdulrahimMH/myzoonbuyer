package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetRequestDetailsModal;

import java.io.Serializable;

public class GetRequestDetailsResponse implements Serializable {
    public String ResponseMsg;
    public GetRequestDetailsModal Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public GetRequestDetailsModal getResult() {
        return Result == null ? new GetRequestDetailsModal() : Result;
    }

    public void setResult(GetRequestDetailsModal result) {
        Result = result;
    }

}
