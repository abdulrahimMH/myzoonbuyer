package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.MeasurementListGetMeasurementPartsEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class MeasurementListGetMeasurementPartsResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<MeasurementListGetMeasurementPartsEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<MeasurementListGetMeasurementPartsEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<MeasurementListGetMeasurementPartsEntity> result) {
        Result = result;
    }

}
