package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.MaterialAverageRatingEntity;
import com.qoltech.mzyoonbuyer.entity.MaterialGetReviewDetailsEntity;
import com.qoltech.mzyoonbuyer.entity.MaterialReviewCountEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class MaterialReviewResponse implements Serializable {
    public ArrayList<MaterialReviewCountEntity> ReviewCount;
    public ArrayList<MaterialAverageRatingEntity> AverageRating;
    public ArrayList<MaterialGetReviewDetailsEntity> GetReviewDetails;

    public ArrayList<MaterialReviewCountEntity> getReviewCount() {
        return ReviewCount == null ? new ArrayList<>() : ReviewCount;
    }

    public void setReviewCount(ArrayList<MaterialReviewCountEntity> reviewCount) {
        ReviewCount = reviewCount;
    }

    public ArrayList<MaterialAverageRatingEntity> getAverageRating() {
        return AverageRating == null ? new ArrayList<>() : AverageRating;
    }

    public void setAverageRating(ArrayList<MaterialAverageRatingEntity> averageRating) {
        AverageRating = averageRating;
    }

    public ArrayList<MaterialGetReviewDetailsEntity> getGetReviewDetails() {
        return GetReviewDetails == null ? new ArrayList<>() : GetReviewDetails;
    }

    public void setGetReviewDetails(ArrayList<MaterialGetReviewDetailsEntity> getReviewDetails) {
        GetReviewDetails = getReviewDetails;
    }


}

