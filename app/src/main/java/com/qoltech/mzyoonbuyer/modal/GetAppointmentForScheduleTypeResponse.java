package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetAppointmentForScheduleTypeEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetAppointmentForScheduleTypeResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<GetAppointmentForScheduleTypeEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetAppointmentForScheduleTypeEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<GetAppointmentForScheduleTypeEntity> result) {
        Result = result;
    }

}
