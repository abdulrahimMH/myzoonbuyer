package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.CheckoutEntity;

import java.io.Serializable;

public class CheckoutResponse implements Serializable {
    public String ResponseMsg;
    public CheckoutEntity Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public CheckoutEntity getResult() {
        return Result == null ? new CheckoutEntity() : Result;
    }

    public void setResult(CheckoutEntity result) {
        Result = result;
    }

}
