package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetNewCustomizationEntity;

import java.io.Serializable;

public class GetNewCustomizationResponse implements Serializable {

    public String ResponseMsg;
    public GetNewCustomizationEntity Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public GetNewCustomizationEntity getResult() {
        return Result == null ? new GetNewCustomizationEntity() : Result;
    }

    public void setResult(GetNewCustomizationEntity result) {
        Result = result;
    }

}
