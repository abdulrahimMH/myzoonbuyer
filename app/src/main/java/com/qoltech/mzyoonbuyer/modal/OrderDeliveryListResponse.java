package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.OrderDeliveryListEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderDeliveryListResponse implements Serializable {
    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<OrderDeliveryListEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<OrderDeliveryListEntity> result) {
        Result = result;
    }

    public ArrayList<OrderDeliveryListEntity> Result;
}
