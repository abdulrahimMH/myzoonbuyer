package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.OrderRequestListEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderRequestListResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<OrderRequestListEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<OrderRequestListEntity> getResult() {
        return Result == null ? new ArrayList<OrderRequestListEntity>() : Result;
    }

    public void setResult(ArrayList<OrderRequestListEntity> result) {
        Result = result;
    }


}
