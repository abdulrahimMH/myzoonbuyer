package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetScrachCardEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetScrachCardResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<GetScrachCardEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetScrachCardEntity> getResult() {
        return Result;
    }

    public void setResult(ArrayList<GetScrachCardEntity> result) {
        Result = result;
    }


}
