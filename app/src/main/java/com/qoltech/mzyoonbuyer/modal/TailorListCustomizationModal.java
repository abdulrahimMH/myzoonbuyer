package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class TailorListCustomizationModal implements Serializable {
    public String getCustomizationId() {
        return CustomizationId == null ? "" : CustomizationId;
    }

    public  void setCustomizationId(String customizationId) {
        CustomizationId = customizationId;
    }

    public String CustomizationId;
}
