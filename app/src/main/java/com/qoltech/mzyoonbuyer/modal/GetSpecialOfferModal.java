package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetSpecialOfferEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetSpecialOfferModal implements Serializable {
    private String ResponseMsg;
    private ArrayList<GetSpecialOfferEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetSpecialOfferEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<GetSpecialOfferEntity> result) {
        Result = result;
    }

}
