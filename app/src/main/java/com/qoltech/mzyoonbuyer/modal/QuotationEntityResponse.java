package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.QuotationEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class QuotationEntityResponse implements Serializable {
    public ArrayList<QuotationEntity> getQuotationList() {
        return QuotationList;
    }

    public void setQuotationList(ArrayList<QuotationEntity> quotationList) {
        QuotationList = quotationList == null ? new ArrayList<QuotationEntity>() : QuotationList;
    }

    public ArrayList<QuotationEntity> QuotationList;

}
