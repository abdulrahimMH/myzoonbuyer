package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.OrderTypeEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderTypeResponse implements Serializable {

    public String ResponseMsg;
    public ArrayList<OrderTypeEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg ==  null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<OrderTypeEntity> getResult() {
        return Result == null ? new ArrayList<OrderTypeEntity>() : Result;
    }

    public void setResult(ArrayList<OrderTypeEntity> result) {
        Result = result;
    }

}

