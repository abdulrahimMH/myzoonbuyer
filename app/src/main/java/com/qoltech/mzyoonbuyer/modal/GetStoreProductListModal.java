package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.DashboardProductsEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetStoreProductListModal implements Serializable {
    public ArrayList<DashboardProductsEntity> products;

    public ArrayList<DashboardProductsEntity> getProducts() {
        return products == null ? new ArrayList<>() : products;
    }

    public void setProducts(ArrayList<DashboardProductsEntity> products) {
        this.products = products;
    }

}
