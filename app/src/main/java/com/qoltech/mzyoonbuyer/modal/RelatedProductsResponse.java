package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.DashboardProductsEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class RelatedProductsResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<DashboardProductsEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<DashboardProductsEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<DashboardProductsEntity> result) {
        Result = result;
    }

}
