package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.MeasurementOneEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class MeasurementOneResponse implements Serializable {

    public String ResponseMsg;
    public ArrayList<MeasurementOneEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg ==  null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<MeasurementOneEntity> getResult() {
        return Result == null ? new ArrayList<MeasurementOneEntity>() : Result;
    }

    public void setResult(ArrayList<MeasurementOneEntity> result) {
        Result = result;
    }

}
