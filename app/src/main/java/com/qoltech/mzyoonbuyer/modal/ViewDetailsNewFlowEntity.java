package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.AverageRatingEntity;
import com.qoltech.mzyoonbuyer.entity.GetColorByIdEntity;
import com.qoltech.mzyoonbuyer.entity.GetMaterialNameEntity;
import com.qoltech.mzyoonbuyer.entity.GetThicknessEntity;
import com.qoltech.mzyoonbuyer.entity.GetpattternByIdEntity;
import com.qoltech.mzyoonbuyer.entity.PatternImgEntity;
import com.qoltech.mzyoonbuyer.entity.ViewRatingEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class ViewDetailsNewFlowEntity implements Serializable {
    public ArrayList<GetpattternByIdEntity> GetPattern;
    public ArrayList<GetColorByIdEntity> GetColors;
    public ArrayList<PatternImgEntity> PatternImg;
    public ArrayList<GetMaterialNameEntity> GetMaterialName;
    public ArrayList<ViewRatingEntity> ReviewCount;
    public ArrayList<AverageRatingEntity> AverageRating;
    public ArrayList<GetThicknessEntity> GetThickness;

    public ArrayList<ViewRatingEntity> getReviewCount() {
        return ReviewCount == null ? new ArrayList<>() : ReviewCount;
    }

    public void setReviewCount(ArrayList<ViewRatingEntity> reviewCount) {
        ReviewCount = reviewCount;
    }

    public ArrayList<AverageRatingEntity> getAverageRating() {
        return AverageRating == null ? new ArrayList<>() : AverageRating;
    }

    public void setAverageRating(ArrayList<AverageRatingEntity> averageRating) {
        AverageRating = averageRating;
    }

    public ArrayList<GetThicknessEntity> getGetThickness() {
        return GetThickness == null ? new ArrayList<>() : GetThickness;
    }

    public void setGetThickness(ArrayList<GetThicknessEntity> getThickness) {
        GetThickness = getThickness;
    }


    public ArrayList<GetMaterialNameEntity> getGetMaterialName() {
        return GetMaterialName == null ? new ArrayList<>() : GetMaterialName;
    }

    public void setGetMaterialName(ArrayList<GetMaterialNameEntity> getMaterialName) {
        GetMaterialName = getMaterialName;
    }


    public ArrayList<PatternImgEntity> getPatternImg() {
        return PatternImg == null ? new ArrayList<>() : PatternImg;
    }

    public void setPatternImg(ArrayList<PatternImgEntity> patternImg) {
        PatternImg = patternImg;
    }

    public ArrayList<GetpattternByIdEntity> getGetpattternById() {
        return GetPattern == null ? new ArrayList<>() : GetPattern;
    }

    public void setGetpattternById(ArrayList<GetpattternByIdEntity> getpattternById) {
        GetPattern = getpattternById;
    }

    public ArrayList<GetColorByIdEntity> getGetColorById() {
        return GetColors == null ? new ArrayList<>() : GetColors;
    }

    public void setGetColorById(ArrayList<GetColorByIdEntity> getColorById) {
        GetColors = getColorById;
    }
}
