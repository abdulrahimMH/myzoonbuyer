package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class OrderDetailsResponse implements Serializable {
    public String ResponseMsg;
    public OrderDetailsModal Result;

    public OrderDetailsModal getResult() {
        return Result == null  ? new OrderDetailsModal() : Result;
    }

    public void setResult(OrderDetailsModal result) {
        Result = result;
    }

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

}
