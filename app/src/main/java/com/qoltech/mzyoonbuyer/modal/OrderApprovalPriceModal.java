package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.OrderApprovalPriceChargesEntity;
import com.qoltech.mzyoonbuyer.entity.OrderApprovalPriceSubTypeEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderApprovalPriceModal implements Serializable {
    public ArrayList<OrderApprovalPriceSubTypeEntity> DressSubType;
    public ArrayList<OrderApprovalPriceChargesEntity> TailorCharges;
    public double StichingAndMaterialCharge;
    public double MeasurementCharges;
    public double UrgentStichingCharges;
    public double DeliveryCharge;
    public double MaterialDeliveryCharges;

    public double getStichingAndMaterialCharge() {
        return StichingAndMaterialCharge;
    }

    public void setStichingAndMaterialCharge(double stichingAndMaterialCharge) {
        StichingAndMaterialCharge = stichingAndMaterialCharge;
    }

    public double getMeasurementCharges() {
        return MeasurementCharges;
    }

    public void setMeasurementCharges(double measurementCharges) {
        MeasurementCharges = measurementCharges;
    }

    public double getUrgentStichingCharges() {
        return UrgentStichingCharges;
    }

    public void setUrgentStichingCharges(double urgentStichingCharges) {
        UrgentStichingCharges = urgentStichingCharges;
    }

    public double getDeliveryCharge() {
        return DeliveryCharge;
    }

    public void setDeliveryCharge(double deliveryCharge) {
        DeliveryCharge = deliveryCharge;
    }

    public double getMaterialDeliveryCharges() {
        return MaterialDeliveryCharges;
    }

    public void setMaterialDeliveryCharges(double materialDeliveryCharges) {
        MaterialDeliveryCharges = materialDeliveryCharges;
    }


    public ArrayList<OrderApprovalPriceSubTypeEntity> getDressSubType() {
        return DressSubType == null ? new ArrayList<OrderApprovalPriceSubTypeEntity>() : DressSubType;
    }

    public void setDressSubType(ArrayList<OrderApprovalPriceSubTypeEntity> dressSubType) {
        DressSubType = dressSubType;
    }

    public ArrayList<OrderApprovalPriceChargesEntity> getTailorCharges() {
        return TailorCharges == null ? new ArrayList<OrderApprovalPriceChargesEntity>() : TailorCharges;
    }

    public void setTailorCharges(ArrayList<OrderApprovalPriceChargesEntity> tailorCharges) {
        TailorCharges = tailorCharges;
    }

}
