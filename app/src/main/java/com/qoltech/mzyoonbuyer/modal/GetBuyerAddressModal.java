package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetBuyerAddressEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetBuyerAddressModal implements Serializable {
    public String ResponseMsg;
    public ArrayList<GetBuyerAddressEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetBuyerAddressEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<GetBuyerAddressEntity> result) {
        Result = result;
    }


}
