package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.SubColorEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class SubColorResponse implements Serializable {

    public String ResponseMsg;
    public ArrayList<SubColorEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<SubColorEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<SubColorEntity> result) {
        Result = result;
    }

}
