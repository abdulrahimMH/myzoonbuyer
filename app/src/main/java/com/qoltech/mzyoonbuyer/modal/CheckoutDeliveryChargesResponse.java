package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.CheckoutDeliverychargesEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class CheckoutDeliveryChargesResponse implements Serializable{
    public String ResponseMsg;
    public ArrayList<CheckoutDeliverychargesEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<CheckoutDeliverychargesEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<CheckoutDeliverychargesEntity> result) {
        Result = result;
    }


}
