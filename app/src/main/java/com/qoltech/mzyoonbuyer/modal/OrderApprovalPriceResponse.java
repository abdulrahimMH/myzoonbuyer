package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class OrderApprovalPriceResponse implements Serializable {
    public String ResponseMsg;
    public OrderApprovalPriceModal Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public OrderApprovalPriceModal getResult() {
        return Result == null ? new OrderApprovalPriceModal() : Result;
    }

    public void setResult(OrderApprovalPriceModal result) {
        Result = result;
    }
}
