package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.DeliveryTypeEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class DeliveryTypeResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<DeliveryTypeEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<DeliveryTypeEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;

    }

    public void setResult(ArrayList<DeliveryTypeEntity> result) {
        Result = result;
    }

}
