package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetRewardHomeResponseEntity;

import java.io.Serializable;

public class GetRewardsHomeResponse implements Serializable {

  public String ResponseMsg;
    public GetRewardHomeResponseEntity Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : getResponseMsg();
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public GetRewardHomeResponseEntity getResult() {
        return Result == null ? new GetRewardHomeResponseEntity() : Result;
    }

    public void setResult(GetRewardHomeResponseEntity result) {
        Result = result;
    }


}
