package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetTrackingEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetStoreTrackingResponse implements Serializable {
    public ArrayList<GetTrackingEntity> GetTracking;

    public ArrayList<GetTrackingEntity> getGetTracking() {
        return GetTracking == null ? new ArrayList<>() : GetTracking;
    }

    public void setGetTracking(ArrayList<GetTrackingEntity> getTracking) {
        GetTracking = getTracking;
    }


}
