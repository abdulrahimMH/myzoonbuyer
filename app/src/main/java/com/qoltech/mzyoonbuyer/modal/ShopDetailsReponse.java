package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.ShopDetailsEntity;

import java.io.Serializable;

public class ShopDetailsReponse implements Serializable {
    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ShopDetailsEntity getResult() {
        return Result == null ? new ShopDetailsEntity() : Result;
    }

    public void setResult(ShopDetailsEntity result) {
        Result = result;
    }

    public ShopDetailsEntity Result;
}
