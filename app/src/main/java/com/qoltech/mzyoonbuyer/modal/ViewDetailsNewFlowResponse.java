package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class ViewDetailsNewFlowResponse implements Serializable {
    public String ResponseMsg;
    public ViewDetailsNewFlowEntity Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ViewDetailsNewFlowEntity getResult() {
        return Result == null ? new ViewDetailsNewFlowEntity() : Result;
    }

    public void setResult(ViewDetailsNewFlowEntity result) {
        Result = result;
    }


}
