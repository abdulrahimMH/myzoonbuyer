package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class OrderApprovalDeliveryResponse implements Serializable {
    public String ResponseMsg;
    public OrderApprovalDeliveryModal Result;

    public String getResponseMsg() {
        return ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg == null ? "" : ResponseMsg;
    }

    public OrderApprovalDeliveryModal getResult() {
        return Result == null ? new OrderApprovalDeliveryModal() : Result;
    }

    public void setResult(OrderApprovalDeliveryModal result) {
        Result = result;
    }

}
