package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;
import java.util.ArrayList;

public class RulerResponse implements Serializable {
    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg == null ? "" : ResponseMsg;
    }

    public ArrayList<Double> getResult() {
        return Result == null ? new ArrayList<Double>() : Result;
    }

    public void setResult(ArrayList<Double> result) {
        Result = result;
    }

    public ArrayList<Double> Result;
}


