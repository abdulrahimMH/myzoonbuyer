package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.DashboardCategoriesEntity;
import com.qoltech.mzyoonbuyer.entity.DashboardProductsEntity;
import com.qoltech.mzyoonbuyer.entity.GetStoreDashBoardEntity;
import com.qoltech.mzyoonbuyer.entity.RecentBrandEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetStoreDashBoardResponse implements Serializable {

public ArrayList<GetStoreDashBoardEntity> GetStoreDashBoard;
public ArrayList<DashboardCategoriesEntity> DashboardCategories;
public ArrayList<DashboardProductsEntity> DashboardProducts;
public ArrayList<GetStoreDashBoardEntity> StoreDashBoard2;
public ArrayList<GetStoreDashBoardEntity> StoreDashBoard3;
public ArrayList<GetStoreDashBoardEntity> StoreDashBoard4;
public ArrayList<RecentBrandEntity> RecentBrand;

    public ArrayList<GetStoreDashBoardEntity> getGetStoreDashBoard() {
        return GetStoreDashBoard == null ? new ArrayList<>() : GetStoreDashBoard;
    }

    public void setGetStoreDashBoard(ArrayList<GetStoreDashBoardEntity> getStoreDashBoard) {
        GetStoreDashBoard = getStoreDashBoard;
    }

    public ArrayList<DashboardCategoriesEntity> getDashboardCategories() {
        return DashboardCategories == null ? new ArrayList<>() : DashboardCategories;
    }

    public void setDashboardCategories(ArrayList<DashboardCategoriesEntity> dashboardCategories) {
        DashboardCategories = dashboardCategories;
    }

    public ArrayList<DashboardProductsEntity> getDashboardProducts() {
        return DashboardProducts == null ? new ArrayList<>() : DashboardProducts;
    }

    public void setDashboardProducts(ArrayList<DashboardProductsEntity> dashboardProducts) {
        DashboardProducts = dashboardProducts;
    }

    public ArrayList<GetStoreDashBoardEntity> getStoreDashBoard2() {
        return StoreDashBoard2 == null ? new ArrayList<>() : StoreDashBoard2;
    }

    public void setStoreDashBoard2(ArrayList<GetStoreDashBoardEntity> storeDashBoard2) {
        StoreDashBoard2 = storeDashBoard2;
    }

    public ArrayList<GetStoreDashBoardEntity> getStoreDashBoard3() {
        return StoreDashBoard3 == null ? new ArrayList<>() : StoreDashBoard3;
    }

    public void setStoreDashBoard3(ArrayList<GetStoreDashBoardEntity> storeDashBoard3) {
        StoreDashBoard3 = storeDashBoard3;
    }

    public ArrayList<GetStoreDashBoardEntity> getStoreDashBoard4() {
        return StoreDashBoard4 == null ? new ArrayList<>() : StoreDashBoard4;
    }

    public void setStoreDashBoard4(ArrayList<GetStoreDashBoardEntity> storeDashBoard4) {
        StoreDashBoard4 = storeDashBoard4;
    }

    public ArrayList<RecentBrandEntity> getRecentBrand() {
        return RecentBrand == null ? new ArrayList<>() : RecentBrand;
    }

    public void setRecentBrand(ArrayList<RecentBrandEntity> recentBrand) {
        RecentBrand = recentBrand;
    }


}
