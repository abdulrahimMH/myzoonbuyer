package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class GetRatingResponse implements Serializable {
    public String ResponseMsg;
    public GetRatingModal Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public GetRatingModal getResult() {
        return Result == null ? new GetRatingModal() : Result;
    }

    public void setResult(GetRatingModal result) {
        Result = result;
    }

}
