package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class RemoveCardItemResponse implements Serializable {
    public  String getResponseMsg() {
        return ResponseMsg;
    }
    public  void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }
    public  String ResponseMsg;

}
