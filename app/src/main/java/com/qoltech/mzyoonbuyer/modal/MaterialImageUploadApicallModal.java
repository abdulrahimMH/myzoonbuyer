package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.MaterialImageUploadEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class MaterialImageUploadApicallModal implements Serializable {
    public ArrayList<MaterialImageUploadEntity> getMaterialImages() {
        return MaterialImages == null ? new ArrayList<MaterialImageUploadEntity>() : MaterialImages;
    }

    public void setMaterialImages(ArrayList<MaterialImageUploadEntity> materialImages) {
        MaterialImages = materialImages;
    }

    public ArrayList<MaterialImageUploadEntity> MaterialImages;
}
