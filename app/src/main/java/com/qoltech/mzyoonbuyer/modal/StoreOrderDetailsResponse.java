package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.StoreOrderDetailsModal;

import java.io.Serializable;

public class StoreOrderDetailsResponse implements Serializable {
    public String ResponseMsg;
    public StoreOrderDetailsModal Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public StoreOrderDetailsModal getResult() {
        return Result == null ? new StoreOrderDetailsModal() : Result;
    }

    public void setResult(StoreOrderDetailsModal result) {
        Result = result;
    }

}
