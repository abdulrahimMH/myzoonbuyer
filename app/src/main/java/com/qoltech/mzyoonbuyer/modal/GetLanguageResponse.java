package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetLanguageEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetLanguageResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<GetLanguageEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetLanguageEntity> getResult() {
        return Result == null ? new ArrayList<GetLanguageEntity>() : Result;
    }

    public void setResult(ArrayList<GetLanguageEntity> result) {
        Result = result;
    }

}
