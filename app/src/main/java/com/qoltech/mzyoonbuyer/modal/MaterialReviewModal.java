package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class MaterialReviewModal implements Serializable {
    public String ResponseMsg;
    public MaterialReviewResponse Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public MaterialReviewResponse getResult() {
        return Result == null ? new MaterialReviewResponse() : Result;
    }

    public void setResult(MaterialReviewResponse result) {
        Result = result;
    }
}
