package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetMeasurementTwoEntity;

import java.io.Serializable;

public class GetMeasurementTwoResponse implements Serializable {

    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public GetMeasurementTwoEntity getResult() {
        return Result == null ? new GetMeasurementTwoEntity() : Result;
    }

    public void setResult(GetMeasurementTwoEntity result) {
        Result = result;
    }

    public GetMeasurementTwoEntity Result;
}
