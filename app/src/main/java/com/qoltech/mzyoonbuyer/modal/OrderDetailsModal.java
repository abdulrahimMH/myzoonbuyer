package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.BuyerAddressEntity;
import com.qoltech.mzyoonbuyer.entity.CheckOutGetStoreAmountEntity;
import com.qoltech.mzyoonbuyer.entity.DeliveryChargesEntity;
import com.qoltech.mzyoonbuyer.entity.GetAppointmentLeftDeliveryEntity;
import com.qoltech.mzyoonbuyer.entity.GetAppointmentLeftMaterialEntity;
import com.qoltech.mzyoonbuyer.entity.GetAppointmentLeftMeasurementEntity;
import com.qoltech.mzyoonbuyer.entity.GetMaterialCompaniReferenceImgEntity;
import com.qoltech.mzyoonbuyer.entity.GetOrederPaymentStatusEntity;
import com.qoltech.mzyoonbuyer.entity.GetTailorDetailsEntity;
import com.qoltech.mzyoonbuyer.entity.MaterialDeliveryChargesEntity;
import com.qoltech.mzyoonbuyer.entity.MeasurementChargesEntity;
import com.qoltech.mzyoonbuyer.entity.OrderDetailEntity;
import com.qoltech.mzyoonbuyer.entity.ProductPriceEntity;
import com.qoltech.mzyoonbuyer.entity.StatusEntity;
import com.qoltech.mzyoonbuyer.entity.StichingAndMaterialChargesEntity;
import com.qoltech.mzyoonbuyer.entity.StoreCartEntity;
import com.qoltech.mzyoonbuyer.entity.TotalEntity;
import com.qoltech.mzyoonbuyer.entity.UrgentStichingChargesEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderDetailsModal implements Serializable {
    public ArrayList<StatusEntity> Status;
    public ArrayList<OrderDetailEntity> OrderDetail;
    public ArrayList<ProductPriceEntity> Price;
    public ArrayList<GetAppointmentLeftMaterialEntity> GetAppoinmentLeftMateril;
    public ArrayList<GetAppointmentLeftMeasurementEntity> GetAppoinmentLeftMeasurement;
    public ArrayList<GetAppointmentLeftDeliveryEntity> GetOrderDetailScheduleType;
    public ArrayList<GetMaterialCompaniReferenceImgEntity> MaterialImage;
    public ArrayList<GetMaterialCompaniReferenceImgEntity> AdditionalMaterialImage;
    public ArrayList<GetMaterialCompaniReferenceImgEntity> ReferenceImage;
    public ArrayList<StichingAndMaterialChargesEntity> StichingAndMaterialCharge;
    public ArrayList<MeasurementChargesEntity> MeasurementCharges;
    public ArrayList<UrgentStichingChargesEntity> UrgentStichingCharges;
    public ArrayList<DeliveryChargesEntity> DeliveryCharge;
    public ArrayList<MaterialDeliveryChargesEntity> MaterialDeliveryCharges;
    public ArrayList<TotalEntity> Total;
    public ArrayList<GetTailorDetailsEntity> GetTailorDetails;
    public ArrayList<BuyerAddressEntity> BuyerAddress;
    public ArrayList<GetOrederPaymentStatusEntity> GetOrederPaymentStatus;
    public ArrayList<StoreCartEntity> GetStoreOrderDetails;
    public ArrayList<CheckOutGetStoreAmountEntity> GetStoreOrderAmount;

    public ArrayList<StoreCartEntity> getGetStoreOrderDetails() {
        return GetStoreOrderDetails == null ? new ArrayList<>() : GetStoreOrderDetails;
    }

    public void setGetStoreOrderDetails(ArrayList<StoreCartEntity> getStoreOrderDetails) {
        GetStoreOrderDetails = getStoreOrderDetails;
    }

    public ArrayList<CheckOutGetStoreAmountEntity> getGetStoreOrderAmount() {
        return GetStoreOrderAmount == null ? new ArrayList<>() : GetStoreOrderAmount;
    }

    public void setGetStoreOrderAmount(ArrayList<CheckOutGetStoreAmountEntity> getStoreOrderAmount) {
        GetStoreOrderAmount = getStoreOrderAmount;
    }

    public ArrayList<GetOrederPaymentStatusEntity> getGetOrederPaymentStatus() {
        return GetOrederPaymentStatus == null ? new ArrayList<>() : GetOrederPaymentStatus;
    }

    public void setGetOrederPaymentStatus(ArrayList<GetOrederPaymentStatusEntity> getOrederPaymentStatus) {
        GetOrederPaymentStatus = getOrederPaymentStatus;
    }

    public ArrayList<StatusEntity> getStatus() {
        return Status == null ? new ArrayList<>() : Status;
    }

    public void setStatus(ArrayList<StatusEntity> status) {
        Status = status;
    }

    public ArrayList<OrderDetailEntity> getOrderDetail() {
        return OrderDetail == null ? new ArrayList<>() : OrderDetail;
    }

    public void setOrderDetail(ArrayList<OrderDetailEntity> orderDetail) {
        OrderDetail = orderDetail;
    }

    public ArrayList<ProductPriceEntity> getPrice() {
        return Price == null ? new ArrayList<>() : Price;
    }

    public void setPrice(ArrayList<ProductPriceEntity> price) {
        Price = price;
    }

    public ArrayList<GetAppointmentLeftMaterialEntity> getGetAppoinmentLeftMateril() {
        return GetAppoinmentLeftMateril == null ? new ArrayList<>() : GetAppoinmentLeftMateril;
    }

    public void setGetAppoinmentLeftMateril(ArrayList<GetAppointmentLeftMaterialEntity> getAppoinmentLeftMateril) {
        GetAppoinmentLeftMateril = getAppoinmentLeftMateril;
    }

    public ArrayList<GetAppointmentLeftMeasurementEntity> getGetAppoinmentLeftMeasurement() {
        return GetAppoinmentLeftMeasurement == null ? new ArrayList<>() : GetAppoinmentLeftMeasurement;
    }

    public void setGetAppoinmentLeftMeasurement(ArrayList<GetAppointmentLeftMeasurementEntity> getAppoinmentLeftMeasurement) {
        GetAppoinmentLeftMeasurement = getAppoinmentLeftMeasurement;
    }

    public ArrayList<GetAppointmentLeftDeliveryEntity> getGetOrderDetailScheduleType() {
        return GetOrderDetailScheduleType == null ? new ArrayList<>() : GetOrderDetailScheduleType;
    }

    public void setGetOrderDetailScheduleType(ArrayList<GetAppointmentLeftDeliveryEntity> getOrderDetailScheduleType) {
        GetOrderDetailScheduleType = getOrderDetailScheduleType;
    }

    public ArrayList<GetMaterialCompaniReferenceImgEntity> getMaterialImage() {
        return MaterialImage == null ? new ArrayList<>() : MaterialImage;
    }

    public void setMaterialImage(ArrayList<GetMaterialCompaniReferenceImgEntity> materialImage) {
        MaterialImage = materialImage;
    }

    public ArrayList<GetMaterialCompaniReferenceImgEntity> getAdditionalMaterialImage() {
        return AdditionalMaterialImage == null ? new ArrayList<>() : AdditionalMaterialImage;
    }

    public void setAdditionalMaterialImage(ArrayList<GetMaterialCompaniReferenceImgEntity> additionalMaterialImage) {
        AdditionalMaterialImage = additionalMaterialImage;
    }

    public ArrayList<GetMaterialCompaniReferenceImgEntity> getReferenceImage() {
        return ReferenceImage == null ? new ArrayList<>() : ReferenceImage;
    }

    public void setReferenceImage(ArrayList<GetMaterialCompaniReferenceImgEntity> referenceImage) {
        ReferenceImage = referenceImage;
    }

    public ArrayList<StichingAndMaterialChargesEntity> getStichingAndMaterialCharge() {
        return StichingAndMaterialCharge == null ? new ArrayList<>() : StichingAndMaterialCharge;
    }

    public void setStichingAndMaterialCharge(ArrayList<StichingAndMaterialChargesEntity> stichingAndMaterialCharge) {
        StichingAndMaterialCharge = stichingAndMaterialCharge;
    }

    public ArrayList<MeasurementChargesEntity> getMeasurementCharges() {
        return MeasurementCharges == null ? new ArrayList<>() : MeasurementCharges ;
    }

    public void setMeasurementCharges(ArrayList<MeasurementChargesEntity> measurementCharges) {
        MeasurementCharges = measurementCharges;
    }

    public ArrayList<UrgentStichingChargesEntity> getUrgentStichingCharges() {
        return UrgentStichingCharges == null ? new ArrayList<>() : UrgentStichingCharges;
    }

    public void setUrgentStichingCharges(ArrayList<UrgentStichingChargesEntity> urgentStichingCharges) {
        UrgentStichingCharges = urgentStichingCharges;
    }

    public ArrayList<DeliveryChargesEntity> getDeliveryCharge() {
        return DeliveryCharge == null ? new ArrayList<>(): DeliveryCharge;
    }

    public void setDeliveryCharge(ArrayList<DeliveryChargesEntity> deliveryCharge) {
        DeliveryCharge = deliveryCharge;
    }

    public ArrayList<MaterialDeliveryChargesEntity> getMaterialDeliveryCharges() {
        return MaterialDeliveryCharges == null ? new ArrayList<>() : MaterialDeliveryCharges;
    }

    public void setMaterialDeliveryCharges(ArrayList<MaterialDeliveryChargesEntity> materialDeliveryCharges) {
        MaterialDeliveryCharges = materialDeliveryCharges;
    }

    public ArrayList<TotalEntity> getTotal() {
        return Total == null ? new ArrayList<>() :  Total;
    }

    public void setTotal(ArrayList<TotalEntity> total) {
        Total = total;
    }

    public ArrayList<GetTailorDetailsEntity> getGetTailorDetails() {
        return GetTailorDetails == null ? new ArrayList<>() : GetTailorDetails;
    }

    public void setGetTailorDetails(ArrayList<GetTailorDetailsEntity> getTailorDetails) {
        GetTailorDetails = getTailorDetails;
    }

    public ArrayList<BuyerAddressEntity> getBuyerAddress() {
        return BuyerAddress == null ? new ArrayList<>() : BuyerAddress;
    }

    public void setBuyerAddress(ArrayList<BuyerAddressEntity> buyerAddress) {
        BuyerAddress = buyerAddress;
    }
}
