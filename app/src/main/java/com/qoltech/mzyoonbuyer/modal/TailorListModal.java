package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;
import java.util.ArrayList;

public class TailorListModal implements Serializable {
    public  String DressSubType;
    public  String OrderType;
    public  String MeasuremenType;
    public  String ServiceType;
    public  String AreaId;
    public ArrayList<TailorListCustomizationModal> Customization;
    public String MaterialId;
    public double latitude;
    public double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getServiceType() {
        return ServiceType == null ? "" : ServiceType;
    }

    public void setServiceType(String serviceType) {
        ServiceType = serviceType;
    }

    public String getMaterialId() {
        return MaterialId == null ? "" : MaterialId;
    }

    public void setMaterialId(String materialId) {
        MaterialId = materialId;
    }


    public ArrayList<TailorListCustomizationModal> getCustomization() {
        return Customization == null ? new ArrayList<>() : Customization;
    }

    public void setCustomization(ArrayList<TailorListCustomizationModal> customization) {
        Customization = customization;
    }


    public  String getDressSubType() {
        return DressSubType == null ? "" : DressSubType;
    }

    public  void setDressSubType(String dressSubType) {
        DressSubType = dressSubType;
    }

    public  String getOrderType() {
        return OrderType == null ? "" : OrderType;
    }

    public  void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public  String getMeasuremenType() {
        return MeasuremenType == null ? "" : MeasuremenType;
    }

    public  void setMeasuremenType(String measuremenType) {
        MeasuremenType = measuremenType;
    }


    public  String getAreaId() {
        return AreaId == null ? "" : AreaId;
    }

    public  void setAreaId(String areaId) {
        AreaId = areaId;
    }
}
