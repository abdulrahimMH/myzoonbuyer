package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.DeliveryChargesEntity;
import com.qoltech.mzyoonbuyer.entity.MaterialDeliveryChargesEntity;
import com.qoltech.mzyoonbuyer.entity.MeasurementChargesEntity;
import com.qoltech.mzyoonbuyer.entity.StichingAndMaterialChargesEntity;
import com.qoltech.mzyoonbuyer.entity.TotalEntity;
import com.qoltech.mzyoonbuyer.entity.UrgentStichingChargesEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetPriceDetailsModal implements Serializable {

    public ArrayList<StichingAndMaterialChargesEntity> StichingAndMaterialCharges;
    public ArrayList<MeasurementChargesEntity> MeasurementCharges;
    public ArrayList<UrgentStichingChargesEntity> UrgentStichingCharges;
    public ArrayList<DeliveryChargesEntity> DeliveryCharges;
    public ArrayList<MaterialDeliveryChargesEntity> MaterialDeliveryCharges;
    public ArrayList<TotalEntity> Total;

    public ArrayList<StichingAndMaterialChargesEntity> getStichingAndMaterialCharges() {
        return StichingAndMaterialCharges == null ? new ArrayList<>() : StichingAndMaterialCharges;
    }

    public void setStichingAndMaterialCharges(ArrayList<StichingAndMaterialChargesEntity> stichingAndMaterialCharges) {
        StichingAndMaterialCharges = stichingAndMaterialCharges;
    }

    public ArrayList<DeliveryChargesEntity> getDeliveryCharges() {
        return DeliveryCharges == null ? new ArrayList<>() : DeliveryCharges;
    }

    public void setDeliveryCharges(ArrayList<DeliveryChargesEntity> deliveryCharges) {
        DeliveryCharges = deliveryCharges;
    }

    public ArrayList<MaterialDeliveryChargesEntity> getMaterialDeliveryCharges() {
        return MaterialDeliveryCharges == null ? new ArrayList<>() : MaterialDeliveryCharges;
    }

    public void setMaterialDeliveryCharges(ArrayList<MaterialDeliveryChargesEntity> materialDeliveryCharges) {
        MaterialDeliveryCharges = materialDeliveryCharges;
    }

    public ArrayList<MeasurementChargesEntity> getMeasurementCharges() {
        return MeasurementCharges == null ? new ArrayList<>() : MeasurementCharges;
    }

    public void setMeasurementCharges(ArrayList<MeasurementChargesEntity> measurementCharges) {
        MeasurementCharges = measurementCharges;
    }

    public ArrayList<UrgentStichingChargesEntity> getUrgentStichingCharges() {
        return UrgentStichingCharges == null ? new ArrayList<>() : UrgentStichingCharges;
    }

    public void setUrgentStichingCharges(ArrayList<UrgentStichingChargesEntity> urgentStichingCharges) {
        UrgentStichingCharges = urgentStichingCharges;
    }

    public ArrayList<TotalEntity> getTotal() {
        return Total == null ? new ArrayList<>() : Total;
    }

    public void setTotal(ArrayList<TotalEntity> total) {
        Total = total;
    }


}
