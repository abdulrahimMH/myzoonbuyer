package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetMeasurementFromDateEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetMeasurementFromDateResponse implements Serializable {
    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetMeasurementFromDateEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<GetMeasurementFromDateEntity> result) {
        Result = result;
    }

    public ArrayList<GetMeasurementFromDateEntity> Result;
}
