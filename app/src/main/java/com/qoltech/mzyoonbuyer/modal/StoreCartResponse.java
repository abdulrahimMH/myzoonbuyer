package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.StoreCartEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class StoreCartResponse implements Serializable {

    public String ResponseMsg;
    public ArrayList<StoreCartEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<StoreCartEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<StoreCartEntity> result) {
        Result = result;
    }

}
