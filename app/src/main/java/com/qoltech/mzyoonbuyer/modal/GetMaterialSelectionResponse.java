package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetMaterialSelectionEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetMaterialSelectionResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<GetMaterialSelectionEntity> Result;

    public ArrayList<GetMaterialSelectionEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<GetMaterialSelectionEntity> result) {
        Result = result;
    }


    public String getResponseMsg() {
        return ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

}
