package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class QuotationListResponse implements Serializable {
    public String ResponseMsg;
    public QuotationEntityResponse Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public QuotationEntityResponse getResult() {
        return Result == null ? new QuotationEntityResponse() : Result;
    }

    public void setResult(QuotationEntityResponse result) {
        Result = result;
    }

}
