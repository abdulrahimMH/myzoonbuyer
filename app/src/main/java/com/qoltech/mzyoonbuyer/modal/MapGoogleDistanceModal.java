package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.MapGoogleDistanceResponse;

import java.io.Serializable;
import java.util.ArrayList;

public class MapGoogleDistanceModal implements Serializable {

    public ArrayList<MapGoogleDistanceResponse> getRoutes() {
        return routes == null ? new ArrayList<>() : routes;
    }

    public void setRoutes(ArrayList<MapGoogleDistanceResponse> routes) {
        this.routes = routes;
    }

    ArrayList<MapGoogleDistanceResponse> routes;
}
