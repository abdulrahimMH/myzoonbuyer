package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.InsertWebOrderDetailsEntity;
import com.qoltech.mzyoonbuyer.entity.InsertWebOrderEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class InsertWebOrderBody implements Serializable {
    public InsertWebOrderEntity order;
    public ArrayList<InsertWebOrderDetailsEntity> OrderDetails;

    public InsertWebOrderEntity getOrder() {
        return order == null ? new InsertWebOrderEntity() : order;
    }

    public void setOrder(InsertWebOrderEntity order) {
        this.order = order;
    }

    public ArrayList<InsertWebOrderDetailsEntity> getOrderDetails() {
        return OrderDetails == null ? new ArrayList<>() : OrderDetails;
    }

    public void setOrderDetails(ArrayList<InsertWebOrderDetailsEntity> orderDetails) {
        OrderDetails = orderDetails;
    }

 }
