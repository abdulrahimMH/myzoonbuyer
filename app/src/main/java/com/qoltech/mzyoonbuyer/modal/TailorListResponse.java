package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class TailorListResponse implements Serializable {
    public String ResponseMsg;
    public GetTailorListEntity Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

//    public ArrayList<TailorListEntity> getResult() {
//        return Result == null ? new ArrayList<TailorListEntity>() :Result;
//    }
//
//    public void setResult(ArrayList<TailorListEntity> result) {
//        Result = result;
//    }
//
//    public ArrayList<TailorListEntity> Result;

    public GetTailorListEntity getResult() {
        return Result == null ? new GetTailorListEntity() : Result;
    }

    public void setResult(GetTailorListEntity result) {
        Result = result;
    }


}
