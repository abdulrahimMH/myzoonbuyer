package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.AddMeasurementEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class AddMeasurementResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<AddMeasurementEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<AddMeasurementEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<AddMeasurementEntity> result) {
        Result = result;
    }

}
