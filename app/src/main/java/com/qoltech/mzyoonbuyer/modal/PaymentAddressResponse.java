package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.PaymentAddressEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class PaymentAddressResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<PaymentAddressEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<PaymentAddressEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<PaymentAddressEntity> result) {
        Result = result;
    }

}
