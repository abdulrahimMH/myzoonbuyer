package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class AfterPayementResponse implements Serializable {
    public static String Result;
    public static String ResponseMsg;

    public static String getResult() {
        return Result;
    }

    public static void setResult(String result) {
        Result = result;
    }

    public static String getResponseMsg() {
        return ResponseMsg;
    }

    public static void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }
}
