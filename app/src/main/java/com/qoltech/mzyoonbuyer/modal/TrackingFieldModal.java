package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.TrackingFieldEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class TrackingFieldModal implements Serializable {
    public String ResponseMsg;
    public ArrayList<TrackingFieldEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<TrackingFieldEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<TrackingFieldEntity> result) {
        Result = result;
    }

}
