package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class GetRewardsResponse implements Serializable {
    public String ResponseMsg;
    public GetRewardsModal Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public GetRewardsModal getResult() {
        return Result == null ? new GetRewardsModal() : Result;
    }

    public void setResult(GetRewardsModal result) {
        Result = result;
    }


}
