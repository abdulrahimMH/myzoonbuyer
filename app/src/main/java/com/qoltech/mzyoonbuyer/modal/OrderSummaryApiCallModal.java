package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.ApicallidEntity;
import com.qoltech.mzyoonbuyer.entity.OrderCustomizationIdValueEntity;
import com.qoltech.mzyoonbuyer.entity.OrderSummaryMaterialImageEntity;
import com.qoltech.mzyoonbuyer.entity.UserMeasurementValueEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderSummaryApiCallModal implements Serializable {
    public String dressType;
    public String CustomerId;
    public String AddressId;
    public String PatternId;
    public String Ordertype;
    public String MeasurementId;
    public ArrayList<OrderSummaryMaterialImageEntity> MaterialImages;
    public ArrayList<OrderSummaryMaterialImageEntity> ReferenceImages;
    public ArrayList<OrderCustomizationIdValueEntity> OrderCustomization;
    public ArrayList<UserMeasurementValueEntity> UserMeasurementValues;
    public String MeasurementBy;
    public String CreatedBy;
    public String MeasurementName;
    public ArrayList<ApicallidEntity> TailorId;
    public String DeliveryTypeId;
    public String MeasurmentType;
    public String Units;
    public String Type;
    public String Comments;
    public String OrderSubType;
    public String DeliveryId;

    public String getDeliveryId() {
        return DeliveryId;
    }

    public void setDeliveryId(String deliveryId) {
        DeliveryId = deliveryId;
    }

    public String getOrderSubType() {
        return OrderSubType == null ? "" : OrderSubType;
    }

    public void setOrderSubType(String orderSubType) {
        OrderSubType = orderSubType;
    }


    public String getComments() {
        return Comments == null ? "" : Comments;
    }

    public void setComments(String comments) {
        Comments = comments;
    }

    public String getType() {
        return Type == null ? "" : Type;
    }

    public void setType(String type) {
        Type = type;
    }
    public String getDressType() {
        return dressType == null ? "" : dressType;
    }

    public void setDressType(String dressType) {
        this.dressType = dressType;
    }

    public String getCustomerId() {
        return CustomerId == null ? "" : CustomerId;
    }

    public void setCustomerId(String customerId) {
        CustomerId = customerId;
    }

    public String getAddressId() {
        return AddressId == null ? "" : AddressId;
    }

    public void setAddressId(String addressId) {
        AddressId = addressId;
    }

    public String getPatternId() {
        return PatternId == null ? "" : PatternId;
    }

    public void setPatternId(String patternId) {
        PatternId = patternId;
    }

    public String getOrdertype() {
        return Ordertype == null ? "" : Ordertype;
    }

    public void setOrdertype(String ordertype) {
        Ordertype = ordertype;
    }

    public String getMeasurementId() {
        return MeasurementId == null ? "" : MeasurementId;
    }

    public void setMeasurementId(String measurementId) {
        MeasurementId = measurementId;
    }

    public ArrayList<OrderSummaryMaterialImageEntity> getMaterialImages() {
        return MaterialImages == null ? new ArrayList<OrderSummaryMaterialImageEntity>() : MaterialImages;
    }

    public void setMaterialImages(ArrayList<OrderSummaryMaterialImageEntity> materialImages) {
        MaterialImages = materialImages;
    }

    public ArrayList<OrderSummaryMaterialImageEntity> getReferenceImages() {
        return ReferenceImages == null ? new ArrayList<OrderSummaryMaterialImageEntity>() : ReferenceImages;
    }

    public void setReferenceImages(ArrayList<OrderSummaryMaterialImageEntity> referenceImages) {
        ReferenceImages = referenceImages;
    }

    public ArrayList<OrderCustomizationIdValueEntity> getOrderCustomization() {
        return OrderCustomization == null ? new ArrayList<OrderCustomizationIdValueEntity>() : OrderCustomization;
    }

    public void setOrderCustomization(ArrayList<OrderCustomizationIdValueEntity> orderCustomization) {
        OrderCustomization = orderCustomization;
    }

    public ArrayList<UserMeasurementValueEntity> getUserMeasurementValue() {
        return UserMeasurementValues == null ? new ArrayList<UserMeasurementValueEntity>() : UserMeasurementValues;
    }

    public void setUserMeasurementValue(ArrayList<UserMeasurementValueEntity> userMeasurementValue) {
        UserMeasurementValues = userMeasurementValue;
    }

    public String getMeasurementBy() {
        return MeasurementBy == null ? "" : MeasurementBy;
    }

    public void setMeasurementBy(String measurementBy) {
        MeasurementBy = measurementBy;
    }

    public String getCreatedBy() {
        return CreatedBy == null ? "" : CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getMeasurementName() {
        return MeasurementName == null ? "" : MeasurementName;
    }

    public void setMeasurementName(String measurementName) {
        MeasurementName = measurementName;
    }

    public ArrayList<ApicallidEntity> getTailorId() {
        return TailorId == null ? new ArrayList<ApicallidEntity>() : TailorId;
    }

    public void setTailorId(ArrayList<ApicallidEntity> tailorId) {
        TailorId = tailorId;
    }

    public String getDeliveryTypeId() {
        return DeliveryTypeId == null ? "" : DeliveryTypeId;
    }

    public void setDeliveryTypeId(String deliveryTypeId) {
        DeliveryTypeId = deliveryTypeId;
    }

    public String getMeasurmentType() {
        return MeasurmentType == null ? "" : MeasurmentType;
    }

    public void setMeasurmentType(String measurmentType) {
        MeasurmentType = measurmentType;
    }

    public String getUnits() {
        return Units == null ? "" : Units;
    }

    public void setUnits(String units) {
        Units = units;
    }

}
