package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetCouponPriceEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetCouponPriceResponse implements Serializable {

    public String ResponseMsg;
    public ArrayList<GetCouponPriceEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetCouponPriceEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<GetCouponPriceEntity> result) {
        Result = result;
    }

}
