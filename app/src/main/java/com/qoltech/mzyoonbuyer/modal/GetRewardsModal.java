package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetRewardsEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetRewardsModal implements Serializable {
public double TotalPoints;
public double CurrentBalance;
    public ArrayList<GetRewardsEntity> GetRewardPoints;

    public double getTotalPoints() {
        return TotalPoints;
    }

    public void setTotalPoints(double totalPoints) {
        TotalPoints = totalPoints;
    }

    public double getCurrentBalance() {
        return CurrentBalance;
    }

    public void setCurrentBalance(double currentBalance) {
        CurrentBalance = currentBalance;
    }

    public ArrayList<GetRewardsEntity> getGetRewardPoints() {
        return GetRewardPoints == null ? new ArrayList<>() : GetRewardPoints;
    }

    public void setGetRewardPoints(ArrayList<GetRewardsEntity> getRewardPoints) {
        GetRewardPoints = getRewardPoints;
    }

}
