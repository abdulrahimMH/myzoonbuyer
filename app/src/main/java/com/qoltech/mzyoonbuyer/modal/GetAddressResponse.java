package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetAddressEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetAddressResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<GetAddressEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetAddressEntity> getResult() {
        return Result == null ? new ArrayList<GetAddressEntity>() : Result;
    }

    public void setResult(ArrayList<GetAddressEntity> result) {
        Result = result;
    }

}
