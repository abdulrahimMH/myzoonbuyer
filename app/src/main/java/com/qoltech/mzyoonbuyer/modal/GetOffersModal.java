package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.OfferEntity;
import com.qoltech.mzyoonbuyer.entity.OfferGenderEntity;
import com.qoltech.mzyoonbuyer.entity.OfferLocationEntity;
import com.qoltech.mzyoonbuyer.entity.TypeIdEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetOffersModal implements Serializable {
    public String CoupanType;
    public ArrayList<OfferGenderEntity> Gender;
    public ArrayList<OfferLocationEntity> location;
    public ArrayList<OfferEntity> OfferType;
    public String TailorId;
    public ArrayList<TypeIdEntity> TypeId;

    public void setTailorId(String tailorId) {
        TailorId = tailorId;
    }

    public ArrayList<TypeIdEntity> getTypeId() {
        return TypeId == null ? new ArrayList<>() : TypeId;
    }

    public void setTypeId(ArrayList<TypeIdEntity> typeId) {
        TypeId = typeId;
    }


    public String getTailorId() {
        return TailorId == null ? "" : TailorId;
    }




    public String getCoupanType() {
        return CoupanType == null ? "" : CoupanType;
    }

    public void setCoupanType(String coupanType) {
        CoupanType = coupanType;
    }

    public ArrayList<OfferGenderEntity> getGender() {
        return Gender == null ? new ArrayList<>() : Gender;
    }

    public void setGender(ArrayList<OfferGenderEntity> gender) {
        Gender = gender;
    }

    public ArrayList<OfferLocationEntity> getLocation() {
        return location == null ? new ArrayList<>() : location;
    }

    public void setLocation(ArrayList<OfferLocationEntity> location) {
        this.location = location;
    }

    public ArrayList<OfferEntity> getOfferType() {
        return OfferType == null ? new ArrayList<>() : OfferType;
    }

    public void setOfferType(ArrayList<OfferEntity> offerType) {
        OfferType = offerType;
    }

}
