package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.OrderApprovalAppointmentEntity;
import com.qoltech.mzyoonbuyer.entity.OrderApprovalDeliveryDateEntity;
import com.qoltech.mzyoonbuyer.entity.OrderApprovalDeliveryTypesEntity;
import com.qoltech.mzyoonbuyer.entity.OrderApprovalPriceSubTypeEntity;
import com.qoltech.mzyoonbuyer.entity.OrderApprovalStichingTimeEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderApprovalDeliveryModal implements Serializable {
    public ArrayList<OrderApprovalPriceSubTypeEntity> DressSubType;
    public ArrayList<OrderApprovalAppointmentEntity> Appoinments;
    public ArrayList<OrderApprovalDeliveryTypesEntity> DeliveryTypes;
    public ArrayList<OrderApprovalDeliveryDateEntity> DeliveryDate;
    public ArrayList<OrderApprovalStichingTimeEntity> StichingTime;
    public ArrayList<OrderApprovalDeliveryEntity> Delivery;

    public ArrayList<OrderApprovalDeliveryEntity> getDelivery() {
        return Delivery == null ? new ArrayList<>() : Delivery;
    }

    public void setDelivery(ArrayList<OrderApprovalDeliveryEntity> delivery) {
        Delivery = delivery;
    }

    public ArrayList<OrderApprovalPriceSubTypeEntity> getDressSubType() {
        return DressSubType ==  null ? new ArrayList<OrderApprovalPriceSubTypeEntity>() : DressSubType;
    }

    public void setDressSubType(ArrayList<OrderApprovalPriceSubTypeEntity> dressSubType) {
        DressSubType = dressSubType;
    }

    public ArrayList<OrderApprovalAppointmentEntity> getAppoinments() {
        return Appoinments == null ? new ArrayList<OrderApprovalAppointmentEntity>() : Appoinments;
    }

    public void setAppoinments(ArrayList<OrderApprovalAppointmentEntity> appoinments) {
        Appoinments = appoinments;
    }

    public ArrayList<OrderApprovalDeliveryTypesEntity> getDeliveryTypes() {
        return DeliveryTypes == null ? new ArrayList<OrderApprovalDeliveryTypesEntity>() : DeliveryTypes;
    }

    public void setDeliveryTypes(ArrayList<OrderApprovalDeliveryTypesEntity> deliveryTypes) {
        DeliveryTypes = deliveryTypes;
    }

    public ArrayList<OrderApprovalDeliveryDateEntity> getDeliveryDate() {
        return DeliveryDate == null ? new ArrayList<OrderApprovalDeliveryDateEntity>() : DeliveryDate;
    }

    public void setDeliveryDate(ArrayList<OrderApprovalDeliveryDateEntity> deliveryDate) {
        DeliveryDate = deliveryDate;
    }

    public ArrayList<OrderApprovalStichingTimeEntity> getStichingTime() {
        return StichingTime == null ? new ArrayList<OrderApprovalStichingTimeEntity>() : StichingTime;
    }

    public void setStichingTime(ArrayList<OrderApprovalStichingTimeEntity> stichingTime) {
        StichingTime = stichingTime;
    }


}
