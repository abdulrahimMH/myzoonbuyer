package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.TailorListEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetTailorListEntity implements Serializable {

    public ArrayList<TailorListEntity> getGetTailorLists() {
        return GetTailorLists == null ? new ArrayList<>() : GetTailorLists;
    }

    public void setGetTailorLists(ArrayList<TailorListEntity> getTailorLists) {
        GetTailorLists = getTailorLists;
    }

    public ArrayList<TailorListEntity> GetTailorLists;
}
