package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetPaymentStoreEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetPaymentStoreModal implements Serializable {
    public String ResponseMsg;
    public ArrayList<GetPaymentStoreEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetPaymentStoreEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<GetPaymentStoreEntity> result) {
        Result = result;
    }

}
