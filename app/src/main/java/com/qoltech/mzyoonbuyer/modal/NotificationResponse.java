package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.NotificationEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class NotificationResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<NotificationEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<NotificationEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<NotificationEntity> result) {
        Result = result;
    }

}
