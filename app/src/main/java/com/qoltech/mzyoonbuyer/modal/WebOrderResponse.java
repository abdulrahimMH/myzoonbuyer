package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.WebOrderEntity;

import java.io.Serializable;

public class WebOrderResponse implements Serializable {
    public String ResponseMsg;
    public WebOrderEntity Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public WebOrderEntity getResult() {
        return Result == null ? new WebOrderEntity(): Result;
    }

    public void setResult(WebOrderEntity result) {
        Result = result;
    }

}
