package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.DashboardProductsEntity;
import com.qoltech.mzyoonbuyer.entity.GetProductColorEntity;
import com.qoltech.mzyoonbuyer.entity.GetProductDetailsImages;
import com.qoltech.mzyoonbuyer.entity.GetProductSellerEntity;
import com.qoltech.mzyoonbuyer.entity.GetProductSizeEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetProductDetailsResponse implements Serializable {

    public DashboardProductsEntity products;
    public ArrayList<GetProductDetailsImages> productImages;
    public ArrayList<GetProductColorEntity> colors;
    public ArrayList<GetProductSizeEntity> sizes;
    public ArrayList<DashboardProductsEntity> realtedProducts;
    public ArrayList<GetProductSellerEntity> sellers;

    public DashboardProductsEntity getProducts() {
        return products;
    }

    public void setProducts(DashboardProductsEntity products) {
        this.products = products;
    }

    public ArrayList<GetProductDetailsImages> getProductImages() {
        return productImages == null ? new ArrayList<>() : productImages;
    }

    public void setProductImages(ArrayList<GetProductDetailsImages> productImages) {
        this.productImages = productImages;
    }

    public ArrayList<GetProductColorEntity> getColors() {
        return colors == null ? new ArrayList<>() : colors;
    }

    public void setColors(ArrayList<GetProductColorEntity> colors) {
        this.colors = colors;
    }

    public ArrayList<GetProductSizeEntity> getSizes() {
        return sizes == null ? new ArrayList<>() : sizes;
    }

    public void setSizes(ArrayList<GetProductSizeEntity> sizes) {
        this.sizes = sizes;
    }

    public ArrayList<DashboardProductsEntity> getRealtedProducts() {
        return realtedProducts == null ? new ArrayList<>() : realtedProducts;
    }

    public void setRealtedProducts(ArrayList<DashboardProductsEntity> realtedProducts) {
        this.realtedProducts = realtedProducts;
    }

    public ArrayList<GetProductSellerEntity> getSellers() {
        return sellers == null ? new ArrayList<>() : sellers;
    }

    public void setSellers(ArrayList<GetProductSellerEntity> sellers) {
        this.sellers = sellers;
    }


}
