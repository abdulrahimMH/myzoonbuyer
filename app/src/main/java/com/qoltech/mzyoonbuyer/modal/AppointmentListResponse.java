package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.AppointmentListEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class AppointmentListResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<AppointmentListEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<AppointmentListEntity> getResult() {
        return Result == null ? new ArrayList<AppointmentListEntity>() : Result;
    }

    public void setResult(ArrayList<AppointmentListEntity> result) {
        Result = result;
    }


}
