package com.qoltech.mzyoonbuyer.modal;

import java.io.Serializable;

public class GetPriceDetailsResponse implements Serializable {
    public String ResponseMsg;
    public GetPriceDetailsModal Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public GetPriceDetailsModal getResult() {
        return Result == null ? new GetPriceDetailsModal() : Result;
    }

    public void setResult(GetPriceDetailsModal result) {
        Result = result;
    }


}
