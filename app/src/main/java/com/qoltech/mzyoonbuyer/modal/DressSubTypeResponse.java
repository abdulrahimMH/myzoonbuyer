package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.DressSubTypeEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class DressSubTypeResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<DressSubTypeEntity> Result;


    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<DressSubTypeEntity> getResult() {
        return Result == null ? new ArrayList<DressSubTypeEntity>() : Result;
    }

    public void setResult(ArrayList<DressSubTypeEntity> result) {
        Result = result;
    }


}
