package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetOfferCateogriesEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetOfferCategoriesResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<GetOfferCateogriesEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetOfferCateogriesEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<GetOfferCateogriesEntity> result) {
        Result = result;
    }

}
