package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetReferalCodeEntity;

import java.io.Serializable;

public class GetReferalCodeResponse implements Serializable {
    public String ResponseMsg;
    public GetReferalCodeEntity Result;

    public GetReferalCodeEntity getResult() {
        return Result == null ? new GetReferalCodeEntity() : Result;
    }

    public void setResult(GetReferalCodeEntity result) {
        Result = result;
    }


    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }


}
