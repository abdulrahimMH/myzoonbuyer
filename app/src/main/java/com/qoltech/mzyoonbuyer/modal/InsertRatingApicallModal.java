package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.InserRatingApicallEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class InsertRatingApicallModal implements Serializable {
public String OrderId;
public String Review;
public String TailorId;
    public ArrayList<InserRatingApicallEntity> Category;

    public String getOrderId() {
        return OrderId == null ? "" : OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getReview() {
        return Review == null ? "" : Review;

    }

    public void setReview(String review) {
        Review = review;
    }

    public String getTailorId() {
        return TailorId == null ? "" : TailorId;
    }

    public void setTailorId(String tailorId) {
        TailorId = tailorId;
    }

    public ArrayList<InserRatingApicallEntity> getCategory() {
        return Category == null ? new ArrayList<InserRatingApicallEntity>() : Category;
    }

    public void setCategory(ArrayList<InserRatingApicallEntity> category) {
        Category = category;
    }

}
