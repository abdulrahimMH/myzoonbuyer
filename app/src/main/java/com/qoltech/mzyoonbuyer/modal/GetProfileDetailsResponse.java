package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetProfileDetailsEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetProfileDetailsResponse implements Serializable {
    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetProfileDetailsEntity> getResult() {
        return Result == null ? new ArrayList<GetProfileDetailsEntity>() : Result;
    }

    public void setResult(ArrayList<GetProfileDetailsEntity> result) {
        Result = result;
    }

    public ArrayList<GetProfileDetailsEntity> Result;
}
