package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetAppointmentDateForScheduleEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetAppointmentDateForScheduleResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<GetAppointmentDateForScheduleEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetAppointmentDateForScheduleEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<GetAppointmentDateForScheduleEntity> result) {
        Result = result;
    }

}
