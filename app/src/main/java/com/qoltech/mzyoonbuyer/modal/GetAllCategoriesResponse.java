package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.OfferAndPromotionEntity;

import java.io.Serializable;

public class GetAllCategoriesResponse implements Serializable {
    public String ResponseMsg;
    public OfferAndPromotionEntity Result;

    public OfferAndPromotionEntity getResult() {
        return Result  == null ?new OfferAndPromotionEntity() : Result;
    }

    public void setResult(OfferAndPromotionEntity result) {
        Result = result;
    }


    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }



}
