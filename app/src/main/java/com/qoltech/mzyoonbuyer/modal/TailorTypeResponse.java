package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.TailorTypeEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class TailorTypeResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<TailorTypeEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<TailorTypeEntity> getResult() {
        return Result == null ? new ArrayList<>() : Result;
    }

    public void setResult(ArrayList<TailorTypeEntity> result) {
        Result = result;
    }

}
