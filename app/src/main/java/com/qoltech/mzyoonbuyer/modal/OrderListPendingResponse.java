package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.OrderListPendingEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderListPendingResponse implements Serializable{
    public String ResponseMsg;
    public ArrayList<OrderListPendingEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<OrderListPendingEntity> getResult() {
        return Result == null ? new ArrayList<OrderListPendingEntity>() : Result;
    }

    public void setResult(ArrayList<OrderListPendingEntity> result) {
        Result = result;
    }

}
