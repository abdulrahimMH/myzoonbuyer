package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.GetOrderDetailsCustomizationModal;

import java.io.Serializable;

public class GetOrderDetailsCustomizationResponse implements Serializable {
    public String ResponseMsg;
    public GetOrderDetailsCustomizationModal Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public GetOrderDetailsCustomizationModal getResult() {
        return Result == null ? new GetOrderDetailsCustomizationModal() : Result;
    }

    public void setResult(GetOrderDetailsCustomizationModal result) {
        Result = result;
    }


}
