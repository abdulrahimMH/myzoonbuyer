package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.CustomerRatingEntity;
import com.qoltech.mzyoonbuyer.entity.FullStatusEntity;
import com.qoltech.mzyoonbuyer.entity.MaterialRatingEntity;
import com.qoltech.mzyoonbuyer.entity.MaterialReviewEntity;
import com.qoltech.mzyoonbuyer.entity.PerformenceStatusEntity;
import com.qoltech.mzyoonbuyer.entity.RatingStatusEntity;
import com.qoltech.mzyoonbuyer.entity.RatingsEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetRatingModal implements Serializable {
    public ArrayList<RatingsEntity> Ratings;
    public ArrayList<RatingStatusEntity> RatingStatus;
    public ArrayList<PerformenceStatusEntity> PerformenceStatus;
    public ArrayList<CustomerRatingEntity> CustomerRating;
    public ArrayList<FullStatusEntity> FullStatus;
    public ArrayList<MaterialReviewEntity> MaterialReview;
    public ArrayList<MaterialRatingEntity> MaterialRating;

    public ArrayList<MaterialReviewEntity> getMaterialReview() {
        return MaterialReview == null ? new ArrayList<>() : MaterialReview;
    }

    public void setMaterialReview(ArrayList<MaterialReviewEntity> materialReview) {
        MaterialReview = materialReview;
    }

    public ArrayList<MaterialRatingEntity> getMaterialRating() {
        return MaterialRating == null ? new ArrayList<>() : MaterialRating;
    }

    public void setMaterialRating(ArrayList<MaterialRatingEntity> materialRating) {
        MaterialRating = materialRating;
    }


    public ArrayList<FullStatusEntity> getFullStatus() {
        return FullStatus == null ? new ArrayList<>() : FullStatus;
    }

    public void setFullStatus(ArrayList<FullStatusEntity> fullStatus) {
        FullStatus = fullStatus;
    }

    public ArrayList<RatingsEntity> getRatings() {
        return Ratings == null ? new ArrayList<RatingsEntity>() : Ratings;
    }

    public void setRatings(ArrayList<RatingsEntity> ratings) {
        Ratings = ratings;
    }

    public ArrayList<RatingStatusEntity> getRatingStatus() {
        return RatingStatus == null ? new ArrayList<RatingStatusEntity>() : RatingStatus;
    }

    public void setRatingStatus(ArrayList<RatingStatusEntity> ratingStatus) {
        RatingStatus = ratingStatus;
    }

    public ArrayList<PerformenceStatusEntity> getPerformenceStatus() {
        return PerformenceStatus == null ? new ArrayList<PerformenceStatusEntity>() : PerformenceStatus;
    }

    public void setPerformenceStatus(ArrayList<PerformenceStatusEntity> performenceStatus) {
        PerformenceStatus = performenceStatus;
    }

    public ArrayList<CustomerRatingEntity> getCustomerRating() {
        return CustomerRating == null ? new ArrayList<CustomerRatingEntity>() : CustomerRating;
    }

    public void setCustomerRating(ArrayList<CustomerRatingEntity> customerRating) {
        CustomerRating = customerRating;
    }

 }
