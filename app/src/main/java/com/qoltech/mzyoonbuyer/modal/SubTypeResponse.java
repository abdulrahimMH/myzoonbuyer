package com.qoltech.mzyoonbuyer.modal;

import com.qoltech.mzyoonbuyer.entity.SubTypeEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class SubTypeResponse implements Serializable {

    public String ResponseMsg;
    public ArrayList<SubTypeEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg ==  null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<SubTypeEntity> getResult() {
        return Result == null ? new ArrayList<SubTypeEntity>() : Result;
    }

    public void setResult(ArrayList<SubTypeEntity> result) {
        Result = result;
    }
}
