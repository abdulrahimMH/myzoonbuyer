package com.qoltech.mzyoonbuyer.floatingmarker;

import com.google.android.gms.maps.model.LatLng;
import com.qoltech.mzyoonbuyer.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class SampleMarkerData {
    public static List<MarkerInfo> getSampleMarkersInfo() {
        final ArrayList<MarkerInfo> res = new ArrayList<>();
        for (int i = 0; i < AppConstants.TAILOR_LIST.size(); i++) {

                final LatLng latLng = new LatLng(//
                        Double.parseDouble(AppConstants.TAILOR_LIST.get(i).getLatitude()),//
                        Double.parseDouble(AppConstants.TAILOR_LIST.get(i).getLongitude())//
                );
                final String title = AppConstants.TAILOR_LIST.get(i).getPrice();
                final int color = 0xFFf44336;
                res.add(new MarkerInfo(latLng, title, color));

        }
        return res;
    }
}
